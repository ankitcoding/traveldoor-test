<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomSeasons extends Model
{
   protected $table="hotel_room_seasons";
    protected $primaryKey = 'hotel_room_season_id';
}
