<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedItinerary extends Model
{
    protected $table="saved_itineraries_new";
}
