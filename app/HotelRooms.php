<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRooms extends Model
{
   protected $table="hotel_rooms";
   protected $primaryKey = 'hotel_room_id';
}
