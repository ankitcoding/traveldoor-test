<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiries extends Model
{
    protected $table="enquiries";

     public function getEnquiryType()
    {
        return $this->belongsTo('App\EnquiryType', 'enquiry_type', 'enquiry_type_id');
    }
}
