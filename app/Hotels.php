<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hotels extends Model
{
   protected $table="hotels";

    public function getHotelType()
   {
   	 return $this->belongsTo('App\HotelType', 'hotel_type','hotel_type_id');
   }
   public function getCity()
   {
   	return $this->belongsTo('App\Cities', 'hotel_city','id');
   }
   public function getCountry()
   {
   	return $this->belongsTo('App\Countries', 'hotel_country','country_id');
   }
    public function getSupplier()
   {
   	return $this->belongsTo('App\Suppliers', 'supplier_id','supplier_id');
   }
}
