<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Income extends Model
{
    protected $table="incomes";

    public function get_income_category()
    {
    	return $this->belongsTo('App\IncomeExpenseCategory','incomes_category_id','expense_category_id');
    }
}
