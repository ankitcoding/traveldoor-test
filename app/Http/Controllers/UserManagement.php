<?php

namespace App\Http\Controllers;

use App\Users;

use App\Menus;

use App\UserRights;

use App\Countries;

use App\SettingTargetCommission;

use App\UserCommissions;

use App\Income;

use App\Expense;

use App\Bookings;

use App\BookingInstallment;

use Session;

use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMailable;



class UserManagement extends Controller

{
     public function __construct()
    {
        date_default_timezone_set('Asia/Dubai');
    }

    private function rights($menu)
    {
        $emp_id=session()->get('travel_users_id');
        $right_array=array();
        $employees=Users::where('users_id',$emp_id)->where('users_pid',0)->where('users_status',1)->first();
        if($employees)
        {
            $right_array['add']=1;
            $right_array['view']=1;
            $right_array['edit_delete']=1;
            $right_array['report']=1;
            $right_array['admin']=1;
            $right_array['admin_which']="add,view,edit_delete,report";
        }
        else
        {

            $employees=Users::where('users_id',$emp_id)->where('users_status',1)->first();
            if(!empty($employees))
            {
                $user_rights=UserRights::where('emp_id',$emp_id)->where('menu',$menu)->first();
                if(!empty($user_rights))
                {
                    $right_array['add']=$user_rights->add_status;
                    $right_array['view']=$user_rights->view_status;
                    $right_array['edit_delete']=$user_rights->edit_del_status;
                    $right_array['report']=$user_rights->report_status;
                    $right_array['admin']=$user_rights->admin_status;
                    if($user_rights->admin_which_status!="")
                        $right_array['admin_which']=$user_rights->admin_which_status;
                    else
                        $right_array['admin_which']="No";
                }
                else
                {
                    $right_array['add']=0;
                    $right_array['view']=0;
                    $right_array['edit_delete']=0;
                    $right_array['report']=0;
                    $right_array['admin']=0;
                    $right_array['admin_which']="No";
                }
            }
            else
            {
                $right_array['add']=0;
                $right_array['view']=0;
                $right_array['edit_delete']=0;
                $right_array['report']=0;
                $right_array['admin']=0;
                $right_array['admin_which']="No";
            }

        }

        return $right_array;

    }

	 public function user_management(Request $request)

	 {

	 	if(session()->has('travel_users_id'))

	 	{
            $rights=$this->rights('user-management');

	 		$emp_id=session()->get('travel_users_id');

	 		$get_users=Users::where("users_pid",$emp_id)->get();


	 		return view('mains.user-management')->with(compact('get_users','rights'));

	 	}

	 	else

	 	{

	 		return redirect()->route('index');

	 	}



    }



    public function create_user(Request $request)

    {

    	if(session()->has('travel_users_id'))

    	{
            $rights=$this->rights('user-management');
            $countries=Countries::get();
    		return view('mains.create-user')->with(compact('rights','countries'));

    	}

    	else

    	{

    		return redirect()->route('index');

    	}



    	

    }



    public function insert_user(Request $request)

    {

    	$users_pid=session()->get('travel_users_id');

    	$users_empcode=$request->get('employee_code');

    	$users_assigned_role=$request->get('select_role');

    	$users_role=$request->get('select_role');

    	$users_username=$request->get('username');

    	$users_fname=$request->get('first_name');

    	$users_lname=$request->get('last_name');

    	$users_contact=$request->get('contact_number');

    	$users_email=$request->get('employee_email');

    	$users_password_hint="123456";

    	$users_password=md5($users_password_hint);

        if($users_assigned_role=="Partner")
        {
             $partner_country=$request->get('partner_country');
        }
        else
        {
           $partner_country=""; 
        }
       

    	$get_users=Users::where('users_username',$users_username)->orWhere('users_email',$users_email)->first();

        if($get_users)

        {   

            echo "exist";

        }

        else

        {


            $insert_user=new Users;

            $insert_user->users_pid=$users_pid;

            $insert_user->users_empcode=$users_empcode;

            $insert_user->users_fname=$users_fname;

            $insert_user->users_lname=$users_lname;

            $insert_user->users_username=$users_username;

            $insert_user->users_contact=$users_contact;

            $insert_user->users_email=$users_email;

            $insert_user->users_password=$users_password;

            $insert_user->users_password_hint=$users_password_hint;

            if($partner_country!="")
            {
            $insert_user->users_partner_country=$partner_country;
            }

            $insert_user->users_assigned_role=$users_assigned_role;

            $insert_user->users_role=$users_role;

            $insert_user->users_status="1";

            if($insert_user->save())

            {

                echo "success";

            }

            else

            {

                echo "fail";

            }

        }

    }

     public function edit_user($users_id)

    {

    	if(session()->has('travel_users_id'))

    	{
            $rights=$this->rights('user-management');
    		$get_users=Users::where('users_id',$users_id)->first();
   $countries=Countries::get();
    		$users_id=base64_encode(base64_encode($users_id));

    		if($get_users)

    		{

    			return view('mains.edit-user')->with(compact('get_users','rights','countries'))->with('users_id',$users_id);  

    		}

    		else

    		{

    			return redirect()->back();

    		}

    	}

    	else

    	{

    		return redirect()->route('index');

    	}

       

     

    }



      public function update_user(Request $request)

    {

        $users_id=base64_decode(base64_decode($request->get('users_id')));

        $users_empcode=$request->get('employee_code');

        $users_assigned_role=$request->get('select_role');

        $users_username=$request->get('username');

        $users_fname=$request->get('first_name');

        $users_lname=$request->get('last_name');

        $users_contact=$request->get('contact_number');
        $service_type=$request->get('service_type');
        $users_email=$request->get('employee_email');
        $service_type=implode(",",$service_type);
        //agent markup new code
               if($request->has('service_name'))
               {
                $service_name=$request->get('service_name');

                $service_cost=$request->get('service_cost');

                $user_service_markup="";
                for($markup_count=0;$markup_count<count($service_name);$markup_count++)

                {

                  $user_service_markup.=$service_name[$markup_count]."---".$service_cost[$markup_count]."///";
              }


          }
          else
          {
              $user_service_markup="";
          }

        if($users_assigned_role=="Partner")
        {
             $partner_country=$request->get('partner_country');
        }
        else
        {
           $partner_country=""; 
        }
       

         $users_status=$request->get('select_status');

        $get_users=Users::where('users_id',"!=",$users_id)->where('users_username','!=','admin')->where(function ($query) use($users_username,$users_email)

            {

                $query->where('users_username',$users_username)->orWhere('users_email',$users_email);



            })->first();

        if($get_users)

        {   

            echo "exist";

        }

        else

        {


            $update_data=array("users_empcode"=>$users_empcode,

                "users_assigned_role"=>$users_assigned_role,

                "users_username"=>$users_username,

                "users_fname"=>$users_fname,

                "users_lname"=>$users_lname,

                "users_contact"=>$users_contact,

                "users_partner_country"=>$partner_country,

                "users_email"=>$users_email,

                "users_status"=>$users_status,
                "user_service_type"=>$service_type,
                "user_service_markup"=>$user_service_markup,
            );



            $update_user=Users::where('users_id',$users_id)->update($update_data);

            if($update_data)

            {

                echo "success";



            }

            else

            {

                echo "fail";

            }

          

        }

    }

    public static function fetch_user($users_id)
    {
        $get_users=Users::where('users_id',$users_id)->first();
        
        return $get_users;
  
    }

     public function user_details($users_id)

    {

    	if(session()->has('travel_users_id'))

    	{
                 $rights=$this->rights('user-management');
    		$get_users=Users::where('users_id',$users_id)->first();

    		$users_id=base64_encode(base64_encode($users_id));

    		if($get_users)

    		{

    			return view('mains.user-details-view')->with(compact('get_users','rights'))->with('users_id',$users_id);

    		}

    		else

    		{

    			return redirect()->back();

    		}

    	}

    	else

    	{

    		return redirect()->route('index');

    	}

    	

    	

    }



    public function user_rights(Request $request)

{

    if(session()->has('travel_users_id'))

    {
         $rights=$this->rights('user-rights');

        $get_users=Users::where("users_pid",'!=',0)->get();

        return view('mains.user-rights')->with(compact('get_users','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

        

}



public function user_rights_insert(Request $request)

{

    $employee=$request->get('employees');

    $total_menus=$request->get('total_menus');





    $check_existing=UserRights::where('emp_id',$employee)->get();

    if(count($check_existing)<=0)

    {

        $success=0;

        for($rights=1;$rights<=$total_menus;$rights++)

        {

            $date=date('d/m/Y');

            $time=date('H:i:s');

            $menus=$request->get('rights_menu'.$rights);



            if($request->has('add'.$rights))

                $add_status=1;

            else

                $add_status=0;



            if($request->has('view'.$rights))

                $view_status=1;

            else

                $view_status=0;



            if($request->has('edit_delete'.$rights))

                $edit_del_status=1;

            else

                $edit_del_status=0;



            if($request->has('report'.$rights))

                $report_status=1;

            else

                $report_status=0;





            if($request->has('sadmin'.$rights))

                $sadmin_status=1;

            else

                $sadmin_status=0;



            if($request->has('admin_which'.$rights))

            {

                $admin_which_array=$request->get('admin_which'.$rights);

                if(count($admin_which_array)>0)

                    $admin_which=implode(',',$admin_which_array);

                else

                    $admin_which="";



            }

            else

            {

                $admin_which="";

            }



            $user_rights=new UserRights;

            $user_rights->emp_id=$employee;

            $user_rights->menu=$menus;

            $user_rights->add_status=$add_status;

            $user_rights->view_status=$view_status;



            $user_rights->edit_del_status=$edit_del_status;

            $user_rights->report_status=$report_status;

            $user_rights->admin_status=$sadmin_status;

            $user_rights->admin_which_status=$admin_which;

            $user_rights->rights_status=1;

            $user_rights->rights_date=$date;

            $user_rights->rights_time=$time;



            if($user_rights->save())

            {

                $success++;

            }

        }



        if($success==$total_menus)

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

    else

    {

        $updations=0;

        for($rights=1;$rights<=$total_menus;$rights++)

        {

            $menus=$request->get('rights_menu'.$rights);



            if($request->has('add'.$rights))

                $add_status=1;

            else

                $add_status=0;



            if($request->has('view'.$rights))

                $view_status=1;

            else

                $view_status=0;



            if($request->has('edit_delete'.$rights))

                $edit_del_status=1;

            else

                $edit_del_status=0;



            if($request->has('report'.$rights))

                $report_status=1;

            else

                $report_status=0;





            if($request->has('sadmin'.$rights))

                $sadmin_status=1;

            else

                $sadmin_status=0;



            if($request->has('admin_which'.$rights))

            {

                $admin_which_array=$request->get('admin_which'.$rights);

                if(count($admin_which_array)>0)

                    $admin_which=implode(',',$admin_which_array);

                else

                    $admin_which="";



            }

            else

            {

                $admin_which="";

            }







            $check_existing_menu=UserRights::where('emp_id',$employee)->where('menu',$menus)->get();



            if(count($check_existing_menu)>0)

            {

                $updatearray=array(

                    'menu'=>$menus,

                    'add_status'=>$add_status,

                    'view_status'=>$view_status,

                    'edit_del_status'=>$edit_del_status,

                    'report_status'=>$report_status,

                    'admin_status'=>$sadmin_status,

                    'admin_which_status'=>$admin_which,

                    'rights_status'=>1

                );

                $update_rights=UserRights::where('emp_id',$employee)->where('menu',$menus)->update($updatearray);



                if($update_rights)

                {

                    $updations++;

                }

            }

            else

            {

                $date=date('d/m/Y');

                $time=date('H:i:s');

                $user_rights=new UserRights;

                $user_rights->emp_id=$employee;

                $user_rights->menu=$menus;

                $user_rights->add_status=$add_status;

                $user_rights->view_status=$view_status;

                $user_rights->edit_del_status=$edit_del_status;

                $user_rights->report_status=$report_status;

                $user_rights->admin_status=$sadmin_status;

                $user_rights->admin_which_status=$admin_which;

                $user_rights->rights_status=1;

                $user_rights->rights_date=$date;

                $user_rights->rights_time=$time;



                if($user_rights->save())

                {

                    $updations++;

                }

            }



        }



        if($updations==$total_menus)

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }



}

public function user_rights_fetch(Request $request)

{

    $employee=$request->get('emp_id');

    $fetch_menu=Menus::where('menu_pid',0)->get();

    $fetch_sub_menu=Menus::where('menu_pid','!=',0)->get();

    $dataarray=array();

    $nor_display_content=array();

    $data="<input type='hidden' name='_token' value='".csrf_token()."'>";





    $check_existing=UserRights::where('emp_id',$employee)->get();
    // print_r($check_existing);

    if(count($check_existing)>0)

    {

        $new_array=array();

        $count=0;

        foreach($check_existing as $array_values)

        {

            $new_array[$count]['menu']=$array_values->menu;

            $new_array[$count]['add_status']=$array_values->add_status;

            $new_array[$count]['add_status']=$array_values->add_status;

            $new_array[$count]['view_status']=$array_values->view_status;

            $new_array[$count]['edit_del_status']=$array_values->edit_del_status;

            $new_array[$count]['report_status']=$array_values->report_status;

            $new_array[$count]['admin_status']=$array_values->admin_status;

            $new_array[$count]['admin_which_status']=$array_values->admin_which_status;



            $count++;

        }

        $inputcount=1;

        $check_menu=0;

        $count_menus=0;

        foreach($fetch_menu as $main_menu)

        {



                $check_none=0;  //for not displaying the div if no rights has been active from that div

                $check_sub_menus=0;

                $data.='<div class="row">

                <div class="col-md-12">

                <div class="row">

                <div class="col-md-3">

                <h4>'.strtoupper($main_menu->menu_name).'</h4>

                </div>

                <div class="col-md-1">

                <span class="plus_minus minus" id="show_rights_data'.$main_menu->menu_id.'" onclick="showRights(\''.$main_menu->menu_id.'\')"><i class="fa fa-minus-square-o plus" aria-hidden="true"></i></span>

                </div>

                </div>

                <br>

                <div class="row" id="divRightData'.$main_menu->menu_id.'"  style="border:1px solid lightgrey;padding:10px">

                <div class="col-md-12">

                <div class="row">

                <div class="col-md-3">

                <br>

                <input type="radio" id="radiobtn_partial'.$main_menu->menu_id.'" name="radio'.$main_menu->menu_id.'" class="with-gap radio-col-primary radio'.$main_menu->menu_id.'" value="Partial" onchange="fillOutAllCheck(\''.$main_menu->menu_id.'\')">

                <label for="radiobtn_partial'.$main_menu->menu_id.'">PARTIAL RIGHTS </label>

                </div>

                <div class="col-md-3">

                <br>

                <input type="radio" id="radiobtn_full'.$main_menu->menu_id.'" name="radio'.$main_menu->menu_id.'" class="with-gap radio-col-primary radio'.$main_menu->menu_id.'" value="Full" onchange="fillOutAllCheck(\''.$main_menu->menu_id.'\')">

                <label for="radiobtn_full'.$main_menu->menu_id.'">FULL RIGHTS </label>

                </div>

                </div>

                </div>

                <br>

                 <div class="col-md-12">

                <div class="row">

                <div class="col-md-4">

                </div>

                <div class="col-md-1  text-center">
                 <label>All</label>

                </div>

                <div class="col-md-1 text-center">

                <label>Add</label>

                </div>

                <div class="col-md-1 text-center">

                <label>View</label>

                </div>

                <div class="col-md-1 text-center">

                <label>Edit/Delete</label>

                </div>

                <div class="col-md-1 text-center">

                <label>Pdf/Excel</label>

                </div>

                <div class="col-md-1 text-center">

                <label>Admin</label>

                </div>

                </div>

                </div> <div class="col-md-12">';



                foreach($fetch_sub_menu as $submenu)

                {

                    if($submenu->menu_pid==$main_menu->menu_id)

                    {

                        $array_key="";

                        foreach($new_array as $key => $data_values)

                        {

                            if($data_values['menu']==$submenu->menu_file)

                                $array_key=$key;

                            

                        }



                        $check_sub_menus++;



                        $data.='<div class="row">

                        <div class="col-md-4">

                        <label class="rights_menu" id="rights_menu'.$inputcount.'">'.$submenu->menu_name.'</label>

                        <input type="hidden" name="rights_menu'.$inputcount.'" value="'.$submenu->menu_file.'">

                        </div>';





                        if($array_key!="" && $new_array[$array_key]['add_status']==1 && $new_array[$array_key]['view_status']==1 && $new_array[$array_key]['edit_del_status']==1 && $new_array[$array_key]['report_status']==1 && $new_array[$array_key]['admin_status']==1)

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="single_row_rights'.$inputcount.'" class="single_row_rights'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="fillOutSingleCheck(\''.$inputcount.'\')" checked>

                            <label for="single_row_rights'.$inputcount.'"></label>

                            </div>';

                        }

                        else

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="single_row_rights'.$inputcount.'" class="single_row_rights'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="fillOutSingleCheck(\''.$inputcount.'\')">

                             <label for="single_row_rights'.$inputcount.'"></label>

                            </div>';

                        }



                        if($array_key!="" && $new_array[$array_key]['add_status']==1)

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="add'.$inputcount.'" name="add'.$inputcount.'" class="add'.$inputcount.' rights'.$main_menu->menu_id.'"  onchange="removeCheck(\''.$inputcount.'\')" checked>

                            <label for="add'.$inputcount.'"></label>

                            </div>';

                            $check_none++;

                        }

                        else

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="add'.$inputcount.'" name="add'.$inputcount.'" class="add'.$inputcount.' rights'.$main_menu->menu_id.'"  onchange="removeCheck(\''.$inputcount.'\')">

                             <label for="add'.$inputcount.'"></label>

                            </div>';

                        }



                        if($array_key!="" && $new_array[$array_key]['view_status']==1)

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="view'.$inputcount.'"  name="view'.$inputcount.'" class="view'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')" checked>

                            <label for="view'.$inputcount.'"></label>

                            </div>';

                            $check_none++;

                        }

                        else

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="view'.$inputcount.'" name="view'.$inputcount.'" class="view'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                             <label for="view'.$inputcount.'"></label>

                            </div>';

                        }



                        if($array_key!="" && $new_array[$array_key]['edit_del_status']==1)

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="edit_delete'.$inputcount.'" name="edit_delete'.$inputcount.'" class="edit_delete'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')" checked>

                             <label for="edit_delete'.$inputcount.'"></label>

                            </div>';

                            $check_none++;

                        }

                        else

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="edit_delete'.$inputcount.'" name="edit_delete'.$inputcount.'" class="edit_delete'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                             <label for="edit_delete'.$inputcount.'"></label>

                            </div>';

                        }



                        if($array_key!="" && $new_array[$array_key]['report_status']==1)

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="report'.$inputcount.'" name="report'.$inputcount.'" class="report'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')" checked>

                            <label for="report'.$inputcount.'"></label>

                            </div>';

                            $check_none++;

                        }

                        else

                        {

                            $data.='<div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="report'.$inputcount.'" name="report'.$inputcount.'" class="report'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                            <label for="report'.$inputcount.'"></label>

                            </div>';

                        }



                        if($array_key!="" && $new_array[$array_key]['admin_status']==1)

                        {

                            $data.='<!--    <div class="col-md-1 text-center checkbox">

                            <input type="checkbox" name="admin'.$inputcount.'" class="admin'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                            </div> -->

                            <div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="sadmin'.$inputcount.'"  name="sadmin'.$inputcount.'"  class="sadmin'.$inputcount.' rights'.$main_menu->menu_id.' single_row_rights'.$inputcount.' " onchange="activeSelect(\''.$inputcount.'\')" checked>

                              <label for="sadmin'.$inputcount.'"></label>

                            </div>';

                            $check_none++;

                        }

                        else

                        {

                            $data.='<!--    <div class="col-md-1 text-center checkbox">

                            <input type="checkbox" name="admin'.$inputcount.'" class="admin'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                            </div> -->

                            <div class="col-md-1 text-center checkbox">

                            <input type="checkbox" id="sadmin'.$inputcount.'"  name="sadmin'.$inputcount.'"  class="sadmin'.$inputcount.' rights'.$main_menu->menu_id.' single_row_rights'.$inputcount.' " onchange="activeSelect(\''.$inputcount.'\')">

                             <label for="sadmin'.$inputcount.'"></label>

                            </div>';

                        }



                        if($array_key!="" && $new_array[$array_key]['admin_which_status']!="")

                        {

                            $which_status=explode(',',$new_array[$array_key]['admin_which_status']);



                            $data.='<div class="col-md-2 text-center">

                            <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" onchange="checkIndividual(\''.$inputcount.'\')" >';

                            if(in_array("add",$which_status))

                                $data.='<option value="add" selected>Add</option>';

                            else

                                $data.='<option value="add" >Add</option>';



                            if(in_array("view",$which_status))

                                $data.='<option value="view" selected>View</option>';

                            else

                                $data.='<option value="view">View</option>';



                            if(in_array("edit_delete",$which_status))

                                $data.='<option value="edit_delete" selected>Edit/Delete</option>';

                            else

                                $data.='<option value="edit_delete">Edit/Delete</option>';

                            

                            if(in_array("report",$which_status))

                                $data.='<option value="report" selected>Pdf/Excel</option>';

                            else

                                $data.='<option value="report">Pdf/Excel</option>';



                            $data.='</select>

                            </div>';


                        }

                        else if($array_key!="" && $new_array[$array_key]['admin_status']==1)

                        {

                            $data.='<div class="col-md-2 text-center">

                            <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" onchange="checkIndividual(\''.$inputcount.'\')" >

                            <option value="add">Add</option>

                            <option value="view">View</option>

                            <option value="edit_delete">Edit/Delete</option>

                            <option value="report">Pdf/Excel</option>

                            </select>

                            </div>';

                        }

                        else

                        {

                            $data.='<div class="col-md-2 text-center">

                            <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" disabled="disabled" onchange="checkIndividual(\''.$inputcount.'\')" >

                            <option value="add">Add</option>

                            <option value="view">View</option>

                            <option value="edit_delete">Edit/Delete</option>

                            <option value="report">Pdf/Excel</option>

                            </select>

                            </div>';

                        }

                        



                        $data.='</div><br>';



                        $inputcount++;

                        $check_menu++;

                    }



                }



                if($check_sub_menus==0)

                {
                    $array_key="";
                     foreach($new_array as $key => $data_values)
                    {
                            if($data_values['menu']==$main_menu->menu_file)

                                $array_key=$key;
                    }

                    $data.='<div class="row">

                    <div class="col-md-4">

                    <label class="rights_menu" id="rights_menu'.$inputcount.'">'.$main_menu->menu_name.'</label>

                    <input type="hidden" name="rights_menu'.$inputcount.'" value="'.$main_menu->menu_file.'">

                    </div>';





                    if($array_key!="" && $new_array[$array_key]['add_status']==1 && $new_array[$array_key]['view_status']==1 && $new_array[$array_key]['edit_del_status']==1 && $new_array[$array_key]['report_status']==1 && $new_array[$array_key]['admin_status']==1)

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="single_row_rights'.$inputcount.'" class="single_row_rights'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="fillOutSingleCheck(\''.$inputcount.'\')" checked>

                        <label for="single_row_rights'.$inputcount.'"></label>

                        </div>';

                    }

                    else

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="single_row_rights'.$inputcount.'" class="single_row_rights'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="fillOutSingleCheck(\''.$inputcount.'\')">

                          <label for="single_row_rights'.$inputcount.'"></label>

                        </div>';

                    }



                    if($array_key!="" && $new_array[$array_key]['add_status']==1)

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="add'.$inputcount.'" name="add'.$inputcount.'" class="add'.$inputcount.' rights'.$main_menu->menu_id.'"  onchange="removeCheck(\''.$inputcount.'\')" checked>

                          <label for="add'.$inputcount.'"></label>

                        </div>';

                        $check_none++;

                    }

                    else

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="add'.$inputcount.'" name="add'.$inputcount.'" class="add'.$inputcount.' rights'.$main_menu->menu_id.'"  onchange="removeCheck(\''.$inputcount.'\')">

                          <label for="add'.$inputcount.'"></label>

                        </div>';

                        $check_none++;

                    }



                    if($array_key!="" && $new_array[$array_key]['view_status']==1)

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="view'.$inputcount.'" name="view'.$inputcount.'" class="view'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')" checked>

                          <label for="view'.$inputcount.'"></label>

                        </div>';

                        $check_none++;

                    }

                    else

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="view'.$inputcount.'" name="view'.$inputcount.'" class="view'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                          <label for="view'.$inputcount.'"></label>

                        </div>';

                    }



                    if($array_key!="" &&  $new_array[$array_key]['edit_del_status']==1)

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="edit_delete'.$inputcount.'"  name="edit_delete'.$inputcount.'" class="edit_delete'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')" checked>

                        <label for="edit_delete'.$inputcount.'"></label>

                        </div>';

                        $check_none++;

                    }

                    else

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="edit_delete'.$inputcount.'"  name="edit_delete'.$inputcount.'" class="edit_delete'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                        <label for="edit_delete'.$inputcount.'"></label>

                        </div>';

                    }



                    if($array_key!="" &&  $new_array[$array_key]['report_status']==1)

                    {

                        $data.='<div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="report'.$inputcount.'" name="report'.$inputcount.'" class="report'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')" checked>

                         <label for="report'.$inputcount.'"></label>

                        </div>';

                        $check_none++;

                    }

                    else

                    {

                        $data.='<div class="col-md-1 text-center">

                        <input type="checkbox" id="report'.$inputcount.'" name="report'.$inputcount.'" class="report'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                         <label for="report'.$inputcount.'"></label>

                        </div>';

                    }



                    if($array_key!="" &&  $new_array[$array_key]['admin_status']==1)

                    {

                        $data.='<!--    <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" name="admin'.$inputcount.'" class="admin'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                        </div> -->

                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox"  id="sadmin'.$inputcount.'" name="sadmin'.$inputcount.'"  class="sadmin'.$inputcount.' rights'.$main_menu->menu_id.' single_row_rights'.$inputcount.' " onchange="activeSelect(\''.$inputcount.'\')" checked>

                         <label for="sadmin'.$inputcount.'"></label>

                        </div>';

                        $check_none++;

                    }

                    else

                    {

                        $data.='<!--    <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" name="admin'.$inputcount.'" class="admin'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                        </div> -->

                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox"  id="sadmin'.$inputcount.'" name="sadmin'.$inputcount.'"  class="sadmin'.$inputcount.' rights'.$main_menu->menu_id.' single_row_rights'.$inputcount.' " onchange="activeSelect(\''.$inputcount.'\')">

                         <label for="sadmin'.$inputcount.'"></label>

                        </div>';

                    }



                    if( $array_key!="" &&  $new_array[$array_key]['admin_which_status']!="")

                    {

                        $which_status=explode(',',$new_array[$array_key]['admin_which_status']);



                        $data.='<div class="col-md-2 text-center">

                        <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" onchange="checkIndividual(\''.$inputcount.'\')" >';

                        if(in_array("add",$which_status))

                            $data.='<option value="add" selected>Add</option>';

                        else

                            $data.='<option value="add" >Add</option>';



                        if(in_array("view",$which_status))

                            $data.='<option value="view" selected>View</option>';

                        else

                            $data.='<option value="view">View</option>';



                        if(in_array("edit_delete",$which_status))

                            $data.='<option value="edit_delete" selected>Edit/Delete</option>';

                        else

                            $data.='<option value="edit_delete">Edit/Delete</option>';



                        if(in_array("report",$which_status))

                            $data.='<option value="report" selected>Pdf/Excel</option>';

                        else

                            $data.='<option value="report">Pdf/Excel</option>';



                        $data.='</select>

                        </div>';


                    }

                    else if($array_key!="" && $new_array[$array_key]['admin_status']==1)

                    {

                        $data.='<div class="col-md-2 text-center">

                        <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" onchange="checkIndividual(\''.$inputcount.'\')" >

                        <option value="add">Add</option>

                        <option value="view">View</option>

                        <option value="edit_delete">Edit/Delete</option>

                        <option value="report">Pdf/Excel</option>

                        </select>

                        </div>';



                    }

                    else

                    {

                        $data.='<div class="col-md-2 text-center">

                        <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" disabled="disabled" onchange="checkIndividual(\''.$inputcount.'\')" >

                        <option value="add">Add</option>

                        <option value="view">View</option>

                        <option value="edit_delete">Edit/Delete</option>

                        <option value="report">Pdf/Excel</option>

                        </select>

                        </div>';

                    }



                    $data.='</div>';

                    $inputcount++;

                    $check_menu++;



                }





                $data.='</div></div></div></div><br>';
                if($check_none==0)
                {

                    $nor_display_content[]="divRightData$main_menu->menu_id";

                }



                

            }

            $data.='<div class="row">

            <div class="col-md-9">

            </div>

            <div class="col-md-3 pull-right">

            <input type="hidden" name="total_menus" value="'.($inputcount-1).'">

            <button id="submit" type="button" class="btn btn-rounded btn-primary mr-10">Submit</button>

            </div>

            </div>';



            if(count($nor_display_content)>0)

            {

                $dataarray['nor_display_content']=implode(',',$nor_display_content);

            }

            else

            {

                $dataarray['nor_display_content']="";

            }



            $dataarray['result']="success";

            $dataarray['data']=$data;



        }

        else

        {



            $inputcount=1;

            foreach($fetch_menu as $main_menu)

            {



                $check_sub_menus=0;

                $data.='<div class="row">

                <div class="col-md-12">

                <div class="row">

                <div class="col-md-3">

                <h4>'.strtoupper($main_menu->menu_name).'</h4>

                </div>

                <div class="col-md-1">

                <span class="plus_minus plus" id="show_rights_data'.$main_menu->menu_id.'" onclick="showRights(\''.$main_menu->menu_id.'\')"><i class="fa fa-plus-square-o plus" aria-hidden="true"></i></span>

                </div>

                </div>

                <br>



                <div class="row" id="divRightData'.$main_menu->menu_id.'"  style="border:1px solid lightgrey;padding:10px;display:none">

                <div class="col-md-12">

                <div class="row">

                <div class="col-md-3">

                <br>

                <input type="radio" id="radiobtn_partial'.$main_menu->menu_id.'" name="radio'.$main_menu->menu_id.'" class="with-gap radio-col-primary radio'.$main_menu->menu_id.'" value="Partial" onchange="fillOutAllCheck(\''.$main_menu->menu_id.'\')">

                <label for="radiobtn_partial'.$main_menu->menu_id.'">PARTIAL RIGHTS </label>

                </div>

                <div class="col-md-3">

                <br>

                <input type="radio" id="radiobtn_full'.$main_menu->menu_id.'" name="radio'.$main_menu->menu_id.'" class="with-gap radio-col-primary radio'.$main_menu->menu_id.'" value="Full" onchange="fillOutAllCheck(\''.$main_menu->menu_id.'\')">

                <label for="radiobtn_full'.$main_menu->menu_id.'">FULL RIGHTS </label>

                </div>

                </div>

                </div>

                <br>

                 <div class="col-md-12">

                <div class="row">

                <div class="col-md-4">

                </div>

                <div class="col-md-1 text-center">
                 <label>All</label>

                </div>

                <div class="col-md-1 text-center">

                <label>Add</label>

                </div>

                <div class="col-md-1 text-center">

                <label>View</label>

                </div>

                <div class="col-md-1 text-center">

                <label>Edit/Delete</label>

                </div>



                <div class="col-md-1 text-center">

                <label>Pdf/Excel</label>

                </div>

                <!--   <div class="col-md-1 text-center">

                <label>Admin</label>

                </div> -->

                <div class="col-md-1 text-center">

                <label>Admin</label>

                </div>

                </div>

                </div>

                   <div class="col-md-12">';

                foreach($fetch_sub_menu as $submenu)

                {

                    if($submenu->menu_pid==$main_menu->menu_id)

                    {

                        $check_sub_menus++;



                        $data.='<div class="row">

                        <div class="col-md-4">

                        <label class="rights_menu" id="rights_menu'.$inputcount.'">'.$submenu->menu_name.'</label>

                        <input type="hidden" name="rights_menu'.$inputcount.'" value="'.$submenu->menu_file.'">

                        </div>

                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="single_row_rights'.$inputcount.'" class="single_row_rights'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="fillOutSingleCheck(\''.$inputcount.'\')">

                         <label for="single_row_rights'.$inputcount.'"></label>

                        </div>

                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="add'.$inputcount.'" name="add'.$inputcount.'" class="add'.$inputcount.' rights'.$main_menu->menu_id.'"  onchange="removeCheck(\''.$inputcount.'\')">

                         <label for="add'.$inputcount.'"></label>

                        </div>

                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="view'.$inputcount.'" name="view'.$inputcount.'" class="view'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                         <label for="view'.$inputcount.'"></label>

                        </div>

                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="edit_delete'.$inputcount.'" name="edit_delete'.$inputcount.'" class="edit_delete'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                         <label for="edit_delete'.$inputcount.'"></label>

                        </div>



                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="report'.$inputcount.'" name="report'.$inputcount.'" class="report'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                         <label for="report'.$inputcount.'"></label>

                        </div>

                        <!--    <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" name="admin'.$inputcount.'" class="admin'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                         <label for="admin'.$inputcount.'"></label>

                        </div> -->

                        <div class="col-md-1 text-center checkbox">

                        <input type="checkbox" id="sadmin'.$inputcount.'" name="sadmin'.$inputcount.'" class="sadmin'.$inputcount.' rights'.$main_menu->menu_id.' single_row_rights'.$inputcount.' " onchange="activeSelect(\''.$inputcount.'\')">

                         <label for="sadmin'.$inputcount.'"></label>

                        </div>

                        <div class="col-md-2 text-center">

                        <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" onchange="checkIndividual(\''.$inputcount.'\')">

                        <option value="add">Add</option>

                        <option value="view">View</option>

                        <option value="edit_delete">Edit/Delete</option>

                        <option value="report">Pdf/Excel</option>

                        </select>

                        </div>

                        </div>

                        <br>';



                        $inputcount++;



                    }



                }



                if($check_sub_menus==0)

                {

                    $data.='<div class="row">

                    <div class="col-md-4">

                    <label class="rights_menu" id="rights_menu'.$inputcount.'">'.$main_menu->menu_name.'</label>

                    <input type="hidden" name="rights_menu'.$inputcount.'" value="'.$main_menu->menu_file.'">

                    </div>

                    <div class="col-md-1 text-center checkbox">

                    <input type="checkbox" id="single_row_rights'.$inputcount.'" class="single_row_rights'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="fillOutSingleCheck(\''.$inputcount.'\')">

                      <label for="single_row_rights'.$inputcount.'"></label>

                    </div>

                    <div class="col-md-1 text-center checkbox">

                    <input type="checkbox" id="add'.$inputcount.'" name="add'.$inputcount.'" class="add'.$inputcount.' rights'.$main_menu->menu_id.'"  onchange="removeCheck(\''.$inputcount.'\')">

                    <label for="add'.$inputcount.'"></label>

                    </div>

                    <div class="col-md-1 text-center checkbox">

                    <input type="checkbox" id="view'.$inputcount.'" name="view'.$inputcount.'" class="view'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                    <label for="view'.$inputcount.'"></label>

                    </div>

                    <div class="col-md-1 text-center checkbox">

                    <input type="checkbox" id="edit_delete'.$inputcount.'" name="edit_delete'.$inputcount.'" class="edit_delete'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                    <label for="edit_delete'.$inputcount.'"></label>

                    </div>



                    <div class="col-md-1 text-center checkbox">

                    <input type="checkbox" id="report'.$inputcount.'"  name="report'.$inputcount.'" class="report'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                    <label for="report'.$inputcount.'"></label>

                    </div>

                    <!--    <div class="col-md-1 text-center checkbox">

                    <input type="checkbox" id="admin'.$inputcount.'" name="admin'.$inputcount.'" class="admin'.$inputcount.' rights'.$main_menu->menu_id.'" onchange="removeCheck(\''.$inputcount.'\')">

                    <label for="admin'.$inputcount.'"></label>

                    </div> -->

                    <div class="col-md-1 text-center checkbox">

                    <input type="checkbox" id="sadmin'.$inputcount.'" name="sadmin'.$inputcount.'" class="sadmin'.$inputcount.' rights'.$main_menu->menu_id.' single_row_rights'.$inputcount.' " onchange="activeSelect(\''.$inputcount.'\')">

                    <label for="sadmin'.$inputcount.'"></label>

                    </div>

                    <div class="col-md-2 text-center">

                    <select style="width:100% !important" name="admin_which'.$inputcount.'[]" id="admin_which'.$inputcount.'" class="select-multiple  admin_which'.$inputcount.' rights1'.$main_menu->menu_id.'" multiple="multiple" onchange="checkIndividual(\''.$inputcount.'\')">

                    <option value="add">Add</option>

                    <option value="view">View</option>

                    <option value="edit_delete">Edit/Delete</option>

                    <option value="report">Pdf/Excel</option>

                    </select>

                    </div>

                    </div>';



                    $inputcount++;



                }





                $data.='</div>

                </div>

                </div></div><br>';



            }



            $data.='  <div class="row">

            <div class="col-md-9">

            </div>

            <div class="col-md-3 pull-right">

            <input type="hidden" name="total_menus" value="'.($inputcount-1).'">

            <button id="submit" type="button" class="btn btn-rounded btn-primary mr-10">Submit</button>

            </div>

            </div>';



            $dataarray['result']="notavailable";

            $dataarray['data']=$data;

        }

        echo json_encode($dataarray);

    }

    public function user_commissions(Request $request)
{
  if(session()->has('travel_users_id'))
  {
     $rights=$this->rights('user-commissions');
     $users=Users::where('users_pid','!=',0)->where('users_status',1)->where('users_assigned_role','Sub-User')->get();

     $target_commission=SettingTargetCommission::where('st_status',1)->orderBy('st_amount','asc')->get();
     $month=date('m');

      $monthname=date('F');
      $yearname=date('Y');

      $user_commissions_data=array();
      $count=0;
     // foreach($users as $user)
     // {

     //  $user_commissions_data[$count]['user_id']=$user->users_id;
     //  $user_commissions_data[$count]['users_empcode']=$user->users_empcode;
     //  $user_commissions_data[$count]['users_fullname']=$user->users_fname." ".$user->users_lname;

     //  $get_bookings=Bookings::join('agents','bookings.booking_agent_id','=','agents.agent_id')->where('agents.agent_ref_id',$user->users_id)->where('bookings.itinerary_status',null)->where('bookings.booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%m")'),$month)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%Y")'),$yearname)->get();
     //   $actual_profit=0;
     //    foreach($get_bookings as $bookings)
     //    {
     //      if($bookings->booking_final_amount_status==1)
     //      {
     //        $expenses_amt=0;
     //         $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

     //           foreach($fetch_booking_expenses as $expenses)
     //           {
     //            $expenses_amt+=$expenses->expense_amount;
     //           }

     //        $get_profit=$bookings->booking_agent_amount-$bookings->booking_supplier_amount;

     //        $actual_profit+=($get_profit-$expenses_amt);
     //      }

     //    }
     //    $commission_per=0;
     //    if($actual_profit>=0)
     //    {
     //       foreach($target_commission as $tc)
     //    {
     //      if($tc->st_amount<=$actual_profit)
     //      {
     //        $commission_per=$tc->st_commission_per;
     //      }
     //    }

     //    $total_commission=($actual_profit*$commission_per)/100;
     //    }
     //    else
     //    {
     //      $total_commission=0;
     //    }
       

     //     $user_commissions_data[$count]['total_profit']=$actual_profit;
     //     $user_commissions_data[$count]["commission_per"]=$commission_per;
     //     $user_commissions_data[$count]["commission_earned"]=$total_commission;
     //     $user_commissions_data[$count]["commission_paid"]=$total_commission;
     //     $user_commissions_data[$count]["commission_pending"]=$total_commission;
     //     $count++;
     // }
    return view('mains.user-commissions')->with(compact('rights'));
  }
  else
  {
    return redirect()->route('index');
  }

  

}

public function user_commissions_filter(Request $request)
{
  if(session()->has('travel_users_id'))
  {
     $users=Users::where('users_pid','!=',0)->where('users_status',1)->where('users_assigned_role','Sub-User')->get();

     $target_commission=SettingTargetCommission::where('st_status',1)->orderBy('st_amount','asc')->get();
     $month_full=$request->get('month');
     $month_full=explode("-",$month_full);
     $month_numeric=$month_full[0];
      $month_name=$month_full[1];
      $yearname=$request->get('year');



      $html='<p>Commissions Data for '.$month_name.', '.$yearname.'</p>
                                                <table class="table table-bordered datatable">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sr. no</th>
                                                                    <th>Empcode</th>
                                                                    <th>Employee Name</th>
                                                                    <th>Profit Earned (GEL)</th>
                                                                    <th>Commission %</th>
                                                                    <th>Commission Earned</th>
                                                                     <th>Commission Credited</th>
                                                                     <th>Commission Pending</th>
                                                                    <th>Action</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>';

      $user_commissions_data=array();
      $count=0;
     foreach($users as $user)
     {
       $html.='<tr>
       <td>'.($count+1).'</td>
       <td>'.$user->users_empcode.'</td>
       <td>'.$user->users_fname.' '.$user->users_lname.'</td>
       ';
      $get_bookings=Bookings::join('agents','bookings.booking_agent_id','=','agents.agent_id')->where('agents.agent_ref_id',$user->users_id)->where('bookings.itinerary_status',null)->where('bookings.booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%Y")'),$yearname)->where('booking_role','!=','OPERATOR')->get();
       $actual_profit=0;
        foreach($get_bookings as $bookings)
        {
          if($bookings->booking_final_amount_status==1)
          {
            $expenses_amt=0;
            $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

            foreach($fetch_booking_expenses as $expenses)
            {
                $expenses_amt+=$expenses->expense_amount;
            }
            $incomes_amt=0;
            $fetch_booking_incomes=Income::where('incomes_booking_id',$bookings->booking_sep_id)->get();

            foreach($fetch_booking_incomes as $incomes)
            {
                $incomes_amt+=$incomes->incomes_amount;
            }


             if($bookings->booking_type=="sightseeing")
               {
                 $booking_subject_name=unserialize($bookings->booking_subject_name);

                 if($booking_subject_name['tour_type']=="private")
                 {
                   $guide_cost=$booking_subject_name['guide_supplier_cost'];
                   $driver_cost=$booking_subject_name['driver_supplier_cost'];

                   $booking_supplier_amount=($guide_cost+$driver_cost);
                 }
                 else if($booking_subject_name['tour_type']=="group")
                 {
                    $booking_supplier_amount=0;
                 }

               }
               else
               {
                $booking_supplier_amount=$bookings->booking_supplier_amount;
               }

           $received_amount=0;
               $get_booking_installment=BookingInstallment::where('booking_id',$bookings->booking_sep_id)->get();

               if(count($get_booking_installment)>0)
               {


                $counter=1;
                foreach($get_booking_installment as $installments)
                {

                  if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                  {
                   $received_amount+=$installments->booking_install_amount;

                 }
               }
             }

            $get_profit=$received_amount-$booking_supplier_amount;

             $actual_profit+=(($get_profit+$incomes_amt)-$expenses_amt);
          }

        }

        $get_operator_bookings=Bookings::where('booking_agent_id',$user->users_id)->where('itinerary_status',null)->where('booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%Y")'),$yearname)->where('booking_role','OPERATOR')->get();
        foreach($get_operator_bookings as $bookings)
        {
          if($bookings->booking_final_amount_status==1)
          {
            $expenses_amt=0;
             $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_expenses as $expenses)
               {
                $expenses_amt+=$expenses->expense_amount;
               }
                 $incomes_amt=0;
            $fetch_booking_incomes=Income::where('incomes_booking_id',$bookings->booking_sep_id)->get();

            foreach($fetch_booking_incomes as $incomes)
            {
                $incomes_amt+=$incomes->incomes_amount;
            }



               if($bookings->booking_type=="sightseeing")
               {
                 $booking_subject_name=unserialize($bookings->booking_subject_name);

                 if($booking_subject_name['tour_type']=="private")
                 {
                   $guide_cost=$booking_subject_name['guide_supplier_cost'];
                   $driver_cost=$booking_subject_name['driver_supplier_cost'];

                   $booking_supplier_amount=($guide_cost+$driver_cost);
                 }
                 else if($booking_subject_name['tour_type']=="group")
                 {
                    $booking_supplier_amount=0;
                 }

               }
               else
               {
                $booking_supplier_amount=$bookings->booking_supplier_amount;
               }

            
           $received_amount=0;
               $get_booking_installment=BookingInstallment::where('booking_id',$bookings->booking_sep_id)->get();

               if(count($get_booking_installment)>0)
               {


                $counter=1;
                foreach($get_booking_installment as $installments)
                {

                  if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                  {
                   $received_amount+=$installments->booking_install_amount;

                 }
               }
             }

            $get_profit=$received_amount-$booking_supplier_amount;

             $actual_profit+=(($get_profit+$incomes_amt)-$expenses_amt);
          }

        }
        $commission_per=0;
        if($actual_profit>=0)
        {
           foreach($target_commission as $tc)
        {
          if($tc->st_amount<=$actual_profit)
          {
            $commission_per=$tc->st_commission_per;
          }
        }

        $total_commission=($actual_profit*$commission_per)/100;
        }
        else
        {
          $total_commission=0;
        }
        $total_commission=round($total_commission);
        $get_current_credited_commissions=UserCommissions::where('comm_user_id',$user->users_id)->where('comm_month',$month_numeric)->where('comm_year',$yearname)->get();

         $credited_amt=0;
              foreach($get_current_credited_commissions as $credited_commissions)
               {
                $credited_amt+=$credited_commissions->comm_credit_amount;
               }
               $credited_amt=round($credited_amt);

        $total_commission_pending=$total_commission-$credited_amt;
        $html.='
       <td>'.$actual_profit.'</td>
       <td>'.$commission_per.'</td>
       <td>'.$total_commission.'</td>  
        <td>'.$credited_amt.'</td>
         <td>'.round($total_commission_pending).'</td>';
         if($total_commission_pending>0)
         {
           $html.='<td style="white-space: nowrap"><button class="btn btn-sm btn-primary pay_commission" id="pay_commission_'.$user->users_id.'">Pay</button> &nbsp; <a class="btn btn-sm btn-primary" href="'.route('user-commissions-details',["user_id"=>$user->users_id,"month"=>$month_numeric."-".$month_name,"year"=>$yearname]).'" target="_blank">View Details</a></td>';
         }
         else
         {
           $html.='<td style="white-space: nowrap"><a class="btn btn-sm btn-primary" href="'.route('user-commissions-details',["user_id"=>$user->users_id,"month"=>$month_numeric."-".$month_name,"year"=>$yearname]).'" target="_blank">View Details</a></td>';
         }
        
       $html.='</tr>
       ';
         $count++;
     }
    $html.="</tbody>
    </table>";
    echo $html;
  }
  else
  {
    $html="Session Expired";
    echo $html;
  }
}

public function user_commissions_details(Request $request)
{
    if(session()->has('travel_users_id'))
  {
     $user_id=$request->get('user_id');
     $rights=$this->rights('user-commissions');
     $user=Users::where('users_pid','!=',0)->where('users_id',$user_id)->where('users_status',1)->where('users_assigned_role','Sub-User')->first();
     if(!empty($user))
     {


     $target_commission=SettingTargetCommission::where('st_status',1)->orderBy('st_amount','asc')->get();

     $month_full=$request->get('month');
     $month_full=explode("-",$month_full);
     $month_numeric=$month_full[0];
      $month_name=$month_full[1];
      $yearname=$request->get('year');

      $user_commissions_details=array();
      $count=0;
      $user_name="";
        $user_name=$user->users_fname." ".$user->users_lname;

      $get_bookings=Bookings::join('agents','bookings.booking_agent_id','=','agents.agent_id')->where('agents.agent_ref_id',$user->users_id)->where('bookings.itinerary_status',null)->where('bookings.booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%Y")'),$yearname)->where('booking_role','!=','OPERATOR')->get();
       $actual_profit=0;
        foreach($get_bookings as $bookings)
        {

          if($bookings->booking_final_amount_status==1)
          {

            $user_commissions_details[$count]["booking_id"]=$bookings->booking_sep_id;
            $user_commissions_details[$count]["booking_type"]=$bookings->booking_type;
            $expenses_amt=0;
             $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_expenses as $expenses)
               {
                $expenses_amt+=$expenses->expense_amount;
               }

                 $incomes_amt=0;
             $fetch_booking_incomes=Income::where('incomes_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_incomes as $incomes)
               {
                $incomes_amt+=$incomes->incomes_amount;
               }

           if($bookings->booking_type=="sightseeing")
               {
                 $booking_subject_name=unserialize($bookings->booking_subject_name);

                 if($booking_subject_name['tour_type']=="private")
                 {
                   $guide_cost=$booking_subject_name['guide_supplier_cost'];
                   $driver_cost=$booking_subject_name['driver_supplier_cost'];

                   $booking_supplier_amount=($guide_cost+$driver_cost);
                 }
                 else if($booking_subject_name['tour_type']=="group")
                 {
                    $booking_supplier_amount=0;
                 }

               }
               else
               {
                $booking_supplier_amount=$bookings->booking_supplier_amount;
               }

             $received_amount=0;
               $get_booking_installment=BookingInstallment::where('booking_id',$bookings->booking_sep_id)->get();

               if(count($get_booking_installment)>0)
               {


                $counter=1;
                foreach($get_booking_installment as $installments)
                {

                  if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                  {
                   $received_amount+=$installments->booking_install_amount;

                 }
               }
             }

            $get_profit=$received_amount-$booking_supplier_amount;

            $actual_profit+=(($get_profit+$incomes_amt)-$expenses_amt);
              $user_commissions_details[$count]["booking_profit"]=$get_profit;
              $user_commissions_details[$count]["booking_expenses"]=$expenses_amt;
              $user_commissions_details[$count]["booking_actual_profit"]=(($get_profit+$incomes_amt)-$expenses_amt);
               $user_commissions_details[$count]["booking_role"]=$bookings->booking_role;
               $count++;
          }



        }

        $get_operator_bookings=Bookings::where('booking_agent_id',$user->users_id)->where('itinerary_status',null)->where('booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%Y")'),$yearname)->where('booking_role','OPERATOR')->get();
        foreach($get_operator_bookings as $bookings)
        {
          if($bookings->booking_final_amount_status==1)
          {
              $user_commissions_details[$count]["booking_id"]=$bookings->booking_sep_id;
            $user_commissions_details[$count]["booking_type"]=$bookings->booking_type;
            $expenses_amt=0;
             $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_expenses as $expenses)
               {
                $expenses_amt+=$expenses->expense_amount;
               }

                 $incomes_amt=0;
             $fetch_booking_incomes=Income::where('incomes_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_incomes as $incomes)
               {
                $incomes_amt+=$incomes->incomes_amount;
               }


            if($bookings->booking_type=="sightseeing")
               {
                 $booking_subject_name=unserialize($bookings->booking_subject_name);

                 if($booking_subject_name['tour_type']=="private")
                 {
                   $guide_cost=$booking_subject_name['guide_supplier_cost'];
                   $driver_cost=$booking_subject_name['driver_supplier_cost'];

                   $booking_supplier_amount=($guide_cost+$driver_cost);
                 }
                 else if($booking_subject_name['tour_type']=="group")
                 {
                    $booking_supplier_amount=0;
                 }

               }
               else
               {
                $booking_supplier_amount=$bookings->booking_supplier_amount;
               }

          $received_amount=0;
               $get_booking_installment=BookingInstallment::where('booking_id',$bookings->booking_sep_id)->get();

               if(count($get_booking_installment)>0)
               {


                $counter=1;
                foreach($get_booking_installment as $installments)
                {

                  if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                  {
                   $received_amount+=$installments->booking_install_amount;

                 }
               }
             }

            $get_profit=$received_amount-$booking_supplier_amount;

            $actual_profit+=(($get_profit+$incomes_amt)-$expenses_amt);
              $user_commissions_details[$count]["booking_profit"]=$get_profit;
              $user_commissions_details[$count]["booking_expenses"]=$expenses_amt;
              $user_commissions_details[$count]["booking_actual_profit"]=(($get_profit+$incomes_amt)-$expenses_amt);
               $user_commissions_details[$count]["booking_role"]=$bookings->booking_role;
               $count++;
          }

        }

     return view('mains.user-commissions-details')->with(compact('user_commissions_details','rights'))->with('month_name',$month_name)->with('yearname',$yearname)->with('user_name',$user_name);
 }
 else
 {
    return redirect()->back();
 }
    
  }
  else
  {
     return redirect()->route('index');
  }

}

public function pay_commissions(Request $request)
{
  $user_id=$request->get('user_id');
  $user=Users::where('users_pid','!=',0)->where('users_id',$user_id)->where('users_status',1)->where('users_assigned_role','Sub-User')->first();

     $target_commission=SettingTargetCommission::where('st_status',1)->orderBy('st_amount','asc')->get();
     $month_full=$request->get('month');
     $month_full=explode("-",$month_full);
     $month_numeric=$month_full[0];
      $month_name=$month_full[1];
      $yearname=$request->get('year');

      $user_commissions_data=array();
      $count=0;
     if(!empty($user))
     {
      $get_bookings=Bookings::join('agents','bookings.booking_agent_id','=','agents.agent_id')->where('agents.agent_ref_id',$user->users_id)->where('bookings.itinerary_status',null)->where('bookings.booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(bookings.booking_complete_timestamp, "%Y")'),$yearname)->where('booking_role','!=','OPERATOR')->get();
       $actual_profit=0;
        foreach($get_bookings as $bookings)
        {
          if($bookings->booking_final_amount_status==1)
          {
            $expenses_amt=0;
             $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_expenses as $expenses)
               {
                $expenses_amt+=$expenses->expense_amount;
               }

                    $incomes_amt=0;
             $fetch_booking_incomes=Income::where('incomes_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_incomes as $incomes)
               {
                $incomes_amt+=$incomes->incomes_amount;
               }

            if($bookings->booking_type=="sightseeing")
               {
                 $booking_subject_name=unserialize($bookings->booking_subject_name);

                 if($booking_subject_name['tour_type']=="private")
                 {
                   $guide_cost=$booking_subject_name['guide_supplier_cost'];
                   $driver_cost=$booking_subject_name['driver_supplier_cost'];

                   $booking_supplier_amount=($guide_cost+$driver_cost);
                 }
                 else if($booking_subject_name['tour_type']=="group")
                 {
                    $booking_supplier_amount=0;
                 }

               }
               else
               {
                $booking_supplier_amount=$bookings->booking_supplier_amount;
               }

          $received_amount=0;
               $get_booking_installment=BookingInstallment::where('booking_id',$bookings->booking_sep_id)->get();

               if(count($get_booking_installment)>0)
               {


                $counter=1;
                foreach($get_booking_installment as $installments)
                {

                  if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                  {
                   $received_amount+=$installments->booking_install_amount;

                 }
               }
             }

            $get_profit=$received_amount-$booking_supplier_amount;

            $actual_profit+=(($get_profit+$incomes_amt)-$expenses_amt);
          }

        }

        $get_operator_bookings=Bookings::where('booking_agent_id',$user->users_id)->where('itinerary_status',null)->where('booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%Y")'),$yearname)->where('booking_role','OPERATOR')->get();
        foreach($get_operator_bookings as $bookings)
        {
          if($bookings->booking_final_amount_status==1)
          {
            $expenses_amt=0;
             $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_expenses as $expenses)
               {
                $expenses_amt+=$expenses->expense_amount;
               }

                $incomes_amt=0;
             $fetch_booking_incomes=Income::where('incomes_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_incomes as $incomes)
               {
                $incomes_amt+=$incomes->incomes_amount;
               }

            if($bookings->booking_type=="sightseeing")
               {
                 $booking_subject_name=unserialize($bookings->booking_subject_name);

                 if($booking_subject_name['tour_type']=="private")
                 {
                   $guide_cost=$booking_subject_name['guide_supplier_cost'];
                   $driver_cost=$booking_subject_name['driver_supplier_cost'];

                   $booking_supplier_amount=($guide_cost+$driver_cost);
                 }
                 else if($booking_subject_name['tour_type']=="group")
                 {
                    $booking_supplier_amount=0;
                 }

               }
               else
               {
                $booking_supplier_amount=$bookings->booking_supplier_amount;
               }

            $received_amount=0;
               $get_booking_installment=BookingInstallment::where('booking_id',$bookings->booking_sep_id)->get();

               if(count($get_booking_installment)>0)
               {


                $counter=1;
                foreach($get_booking_installment as $installments)
                {

                  if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                  {
                   $received_amount+=$installments->booking_install_amount;

                 }
               }
             }

            $get_profit=$received_amount-$booking_supplier_amount;

            $actual_profit+=(($get_profit+$incomes_amt)-$expenses_amt);
          }

        }

        $commission_per=0;
        if($actual_profit>=0)
        {
           foreach($target_commission as $tc)
        {
          if($tc->st_amount<=$actual_profit)
          {
            $commission_per=$tc->st_commission_per;
          }
        }

        $total_commission=($actual_profit*$commission_per)/100;
        }
        else
        {
          $total_commission=0;
        }

        $get_current_credited_commissions=UserCommissions::where('comm_user_id',$user->users_id)->where('comm_month',$month_numeric)->where('comm_year',$yearname)->get();

         $credited_amt=0;
              foreach($get_current_credited_commissions as $credited_commissions)
               {
                $credited_amt+=$credited_commissions->comm_credit_amount;
               }


        $total_commission_pending=$total_commission-$credited_amt;



        if($total_commission_pending>0)
        {
          $insert_commission=new UserCommissions;
          $insert_commission->comm_user_id=$user->users_id;
          $insert_commission->comm_credit_amount=round($total_commission_pending);
          $insert_commission->comm_month=$month_numeric;
          $insert_commission->comm_year=$yearname;
          $insert_commission->comm_date=date('Y-m-d');
          $insert_commission->comm_date=date('H:i:s');
          $insert_commission->comm_remarks="Commission Earned";
          if($insert_commission->save())
          {
            echo "success";
          }
          else
          {
            echo "fail";
          }
        }
        else
        {
            echo "fail";
        }

     }
     else
     {
      echo "invalid_user";
     }
}

    public function user_wallet(Request $request)
    {
    if(session()->has('travel_users_id'))
    {
     $rights=$this->rights('users-wallet');

    $user_id=session()->get('travel_users_id');
    $check_user=Users::where('users_pid',0)->where('users_id',$user_id)->first();
    if(!empty($check_user) || strpos($rights['admin_which'],'view')!==false)
    {
      $users=Users::where('users_pid','!=',0)->where('users_status',1)->where('users_assigned_role','Sub-User')->get();
     // $month=date('m');

     //  $monthname=date('F');
     //  $yearname=date('Y');

      $user_wallet_data=array();
      $count=0;
     foreach($users as $user)
     {

      $user_wallet_data[$count]['user_id']=$user->users_id;
      $user_wallet_data[$count]['users_empcode']=$user->users_empcode;
      $user_wallet_data[$count]['users_fullname']=$user->users_fname." ".$user->users_lname;

    $get_commission_total=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_credit_amount), 0)-COALESCE(sum(comm_debit_amount), 0)) as total_wallet_amount"))->where('comm_user_id',$user->users_id)->where('comm_status',1)->groupBy('comm_user_id')->first();



     $get_commission_withdrawals=UserCommissions::where('comm_debit_amount','!=',null)->where('comm_user_id',$user->users_id)->where('comm_status',0)->get();

    if(!empty($get_commission_total))
    {
    $user_wallet_data[$count]['users_total_wallet_amount']="GEL ".$get_commission_total->total_wallet_amount;
    }
    else
    {
     $user_wallet_data[$count]['users_total_wallet_amount']="GEL 0";   
    }

    $user_wallet_data[$count]['get_commission_withdrawals_count']=count($get_commission_withdrawals);
    
    $count++;
     
     }
    return view('mains.my-wallet-users')->with(compact('rights','user_wallet_data'));   
    }
    else
    {
        return redirect()->route('my-wallet');
    }
    
  }
  else
  {
    return redirect()->route('index');
  }


    }

    public function user_own_wallet(Request $request)
    {
       if(session()->has('travel_users_id'))
       {
        $user_id=$request->get('user_id');
        $withdaw_yes="0";
        if($user_id=="")
        {
            $withdaw_yes="1";
             $user_id=session()->get('travel_users_id');
         }
            $rights=$this->rights('users-wallet');
            
            $month_numeric=date('m');
            $yearname=date('Y');
             $get_wallet=UserCommissions::where('comm_user_id',$user_id)->orderBy('comm_id','desc')->paginate(10);
            $get_commission_total=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_credit_amount), 0)-COALESCE(sum(comm_debit_amount), 0)) as total_wallet_amount"))->where('comm_user_id',$user_id)->where('comm_status',1)->groupBy('comm_user_id')->first();

            $get_commission_total_withdraw=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_credit_amount), 0)-COALESCE(sum(comm_debit_amount), 0)) as total_wallet_amount"))->where('comm_user_id',$user_id)->where('comm_status','!=',2)->groupBy('comm_user_id')->first();


              $get_commission_credited_all=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_credit_amount), 0)) as amount_credited"))->where('comm_user_id',$user_id)->where('comm_status',1)->groupBy('comm_user_id')->first();

             $get_commission_withdraw_all=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_debit_amount), 0)) as amount_withdrawn"))->where('comm_user_id',$user_id)->where('comm_remarks','Money Withdrawn')->where('comm_status',1)->groupBy('comm_user_id')->first();

             $get_commission_deducted_all=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_debit_amount), 0)) as amount_deducted"))->where('comm_user_id',$user_id)->where('comm_remarks','Money Deducted')->where('comm_status',1)->groupBy('comm_user_id')->first();



               $get_commission_credited_month=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_credit_amount), 0)) as amount_credited"))->where('comm_user_id',$user_id)->where('comm_month',$month_numeric)->where('comm_year',$yearname)->where('comm_status',1)->groupBy('comm_user_id')->first();

             $get_commission_withdraw_month=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_debit_amount), 0)) as amount_withdrawn"))->where('comm_user_id',$user_id)->where('comm_month',$month_numeric)->where('comm_year',$yearname)->where('comm_remarks','Money Withdrawn')->where('comm_status',1)->groupBy('comm_user_id')->first();

             $get_commission_deducted_month=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_debit_amount), 0)) as amount_deducted"))->where('comm_user_id',$user_id)->where('comm_month',$month_numeric)->where('comm_year',$yearname)->where('comm_remarks','Money Deducted')->where('comm_status',1)->groupBy('comm_user_id')->first();


            if(!empty($get_commission_total))
                $total_amount=$get_commission_total->total_wallet_amount;
            else
               $total_amount=0;  

               if(!empty($get_commission_total_withdraw))
                $total_amount_withdraw=$get_commission_total_withdraw->total_wallet_amount;
            else
               $total_amount_withdraw=0;  
                
            if(!empty($get_commission_credited_all))
                $total_amount_credited_all=$get_commission_credited_all->amount_credited;
            else
               $total_amount_credited_all=0;

           if(!empty($get_commission_withdraw_all))
                $total_amount_withdraw_all=$get_commission_withdraw_all->amount_withdrawn;
            else
               $total_amount_withdraw_all=0;

           if(!empty($get_commission_deducted_all))
                $total_amount_deducted_all=$get_commission_deducted_all->amount_deducted;
            else
               $total_amount_deducted_all=0;


           if(!empty($get_commission_credited_month))
                $total_amount_credited_month=$get_commission_credited_month->amount_credited;
            else
               $total_amount_credited_month=0;

           if(!empty($get_commission_withdraw_month))
                $total_amount_withdraw_month=$get_commission_withdraw_month->amount_withdrawn;
            else
               $total_amount_withdraw_month=0;

           if(!empty($get_commission_deducted_month))
                $total_amount_deducted_month=$get_commission_deducted_month->amount_deducted;
            else
               $total_amount_deducted_month=0;




            
        return view('mains.my-wallet')->with(compact('rights','get_wallet','total_amount','total_amount_credited_all','total_amount_withdraw_all','total_amount_deducted_all','total_amount_credited_month','total_amount_withdraw_month','total_amount_deducted_month','total_amount_withdraw','withdaw_yes'));
    }
    else
    {
        return redirect()->route('index');
    }
}


public function withdraw_wallet(Request $request)
{
    if(session()->has('travel_users_id'))
       {
             $user_id=session()->get('travel_users_id');
             $withdraw_amount=$request->get('withdraw_amount');
                 $month_numeric=date('m');
            $yearname=date('Y');
           
             $get_wallet=UserCommissions::where('comm_user_id',$user_id)->orderBy('comm_id','desc')->paginate(10);
            $get_commission_total=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_credit_amount), 0)-COALESCE(sum(comm_debit_amount), 0)) as total_wallet_amount"))->where('comm_user_id',$user_id)->where('comm_status','!=',2)->groupBy('comm_user_id')->first();

            if(!empty($get_commission_total))
                $total_amount=$get_commission_total->total_wallet_amount;
            else
               $total_amount=0;   
           if($withdraw_amount>$total_amount)
           {
            echo "exceed_amount";
           }
           else if($withdraw_amount<=0)
           {
             echo "less_amount";
           }
            else if($withdraw_amount<=$total_amount)
           {
              $insert_commission=new UserCommissions;
              $insert_commission->comm_user_id=$user_id;
              $insert_commission->comm_debit_amount=round($withdraw_amount);
              $insert_commission->comm_month=$month_numeric;
              $insert_commission->comm_year=$yearname;
              $insert_commission->comm_date=date('Y-m-d');
              $insert_commission->comm_time=date('H:i:s');
              $insert_commission->comm_remarks="Money Withdrawn";
              $insert_commission->comm_status=0;
              if($insert_commission->save())
              {
                echo "success";
            }
            else
            {
                echo "fail";
            }
        }
    }
    else
    {
          echo "invalid_user";
    }

}



public function get_withdrawals_detail(Request $request)
{
     $user_id=$request->get('user_id');
  $data=array();
  $get_commissions_withdrawable=UserCommissions::where('comm_debit_amount','!=',null)->where('comm_user_id',$user_id)->where('comm_remarks','Money Withdrawn')->where('comm_status',0)->get();
 $table='<div class="table-responsive">
                        <table  id="withdrawals_pending_table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S. No.</th>
                              <th>Requested At</th>
                              <th>Amount to be paid</th>
                               <th>Action</th>
                             </tr>
                          </thead>
                          <tbody>';
                          $count=1;
                foreach($get_commissions_withdrawable as $commissions_withdrawable)
                {
                   $table.='<tr>
                   <td>'.$count.'</td>
                   <td>'.date('d/m/Y h:i a',strtotime($commissions_withdrawable->created_at)).'</td>
                    <td>GEL '.$commissions_withdrawable->comm_debit_amount.'</td>
                     <td>
                     <button class="btn btn-sm btn-primary approve" id="approve_'.$commissions_withdrawable->comm_id.'">Approve</button>
                     <button class="btn btn-sm btn-danger reject" id="reject_'.$commissions_withdrawable->comm_id.'">Reject</button>
                     </td>
                   </tr>';

                          $count++;
                }

                if(count($get_commissions_withdrawable)<=0)
                {
                    $table.='<tr><td colspan=4>No Requests Found</td></tr>';
                }

   $table.='</tbody>
     </table>';

     $get_commissions_withdrawable_history=UserCommissions::where('comm_debit_amount','!=',null)->where('comm_debit_amount','!=',0)->where('comm_user_id',$user_id)->where('comm_remarks','Money Withdrawn')->where('comm_status','!=',0)->get();

     $table1='<div class="table-responsive">
                        <table id="history_table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S. No.</th>
                              <th>Requested At</th>
                              <th>Amount</th>
                               <th>Approved/Rejected At</th>
                               <th>Status</th>
                             </tr>
                          </thead>
                          <tbody>';
                           $count=1;
                foreach($get_commissions_withdrawable_history as $commissions_withdrawable)
                {
                   $table1.='<tr>
                   <td>'.$count.'</td>
                   <td>'.date('d/m/Y h:i a',strtotime($commissions_withdrawable->created_at)).'</td>
                    <td>GEL '.$commissions_withdrawable->comm_debit_amount.'</td>
                     <td>'.date('d/m/Y h:i a',strtotime($commissions_withdrawable->comm_approve_reject_at)).'</td>
                     <td>';
                      if($commissions_withdrawable->comm_status==1)
                      {
                           $table1.='<button class="btn btn-sm btn-primary approve" disabled>Approved</button>';
                      }
                      else
                      {
                         $table1.='<button class="btn btn-sm btn-danger reject" disabled>Rejected</button>';
                      }
                                    
                        
                     $table1.='</td>
                   </tr>';

                          $count++;
                }

   $table1.='</tbody>
     </table>';



   $data["withdrawals_pending"]=$table;
   $data["history_table"]=$table1;

  echo json_encode($data);


}

public function withdrawals_approval(Request $request)
{
    if(session()->has('travel_users_id'))
       {
    $comm_id=$request->get('comm_id');
    $comm_answer=$request->get('comm_answer');
     $remarks=$request->get('remarks');



    if($comm_answer=="approve")
    {
    $get_commission=UserCommissions::where('comm_id',$comm_id)->where('comm_status',1)->first();
    if(!empty($get_commission))
    {
        echo "already";
    }
    else
    {
        $approve_update_array=array('comm_status'=>1,"comm_approve_reject_at"=>date('Y-m-d H:i:s'),"comm_approve_reject_remarks"=>$remarks);

        $update_commissions=UserCommissions::where('comm_id',$comm_id)->update($approve_update_array);

        if($update_commissions)
        {
            echo "success";
        }
        else
        {
            echo "fail";
        }
    }


    }
    else if($comm_answer=="reject")
    {
         $get_commission=UserCommissions::where('comm_id',$comm_id)->where('comm_status',2)->first();
    if(!empty($get_commission))
    {
        echo "already";
    }
    else
    {
        $approve_update_array=array('comm_status'=>2,"comm_approve_reject_at"=>date('Y-m-d H:i:s'),"comm_approve_reject_remarks"=>$remarks);

        $update_commissions=UserCommissions::where('comm_id',$comm_id)->update($approve_update_array);

        if($update_commissions)
        {
            echo "success";
        }
        else
        {
            echo "fail";
        }
    }

    }
    else
    {
        echo "invalid_answer";
    }
     }
    else
    {
          echo "invalid_user";
    }


}

public function users_operation(Request $request)
{
  $users_id=$request->get('user_action_id');
  $operation=$request->get('operation');
  $operation_remarks=$request->get('remarks');
  $operation_amount=$request->get('operation_amount');

  if($operation=="credit" || $operation=="debit")
  {
     $operation_performed="";
          $insert_comm=new UserCommissions;
          $insert_comm->comm_user_id=$users_id;
          if($operation=="credit")
          {
             $insert_comm->comm_credit_amount=round($operation_amount);
             $insert_comm->comm_remarks="Money Added"; 
              $operation_performed='credited';
          }
          else
          {
             $insert_comm->comm_debit_amount=round($operation_amount);
             $insert_comm->comm_remarks="Money Deducted";  
             $operation_performed='debited';   
          }
            $insert_comm->comm_month=date('m');
            $insert_comm->comm_year=date('Y');
          $insert_comm->comm_date=date('Y-m-d');
          $insert_comm->comm_time=date('H:i:s');
          $insert_comm->comm_approve_reject_remarks=$operation_remarks;
          if($insert_comm->save())
          {
               //SEND EMAIL TO USER
           $payment_amount=$operation_amount;
           $fetch_user=Users::where('users_id',$users_id)->first();
           $htmldata='<p>Dear '.$fetch_user->users_fname.' '.$fetch_user->users_lname.',</p><p>Congratulations!</p><p>Traveldoor admin has successfully '.$operation_performed.' <b>GEL '.$payment_amount.'</b> into your wallet.</p>
           ';
           $data = array(
              'name' => $fetch_user->users_fname.' '.$fetch_user->users_lname,
              'email' =>$fetch_user->users_email
          );
           Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data['email'], $data['name'])->subject('MONEY ADDED INTO WALLET');
          });

            //SEND EMAIL TO ADMIN
           $fetch_admin=Users::where('users_pid',0)->first();
           $htmldata='<p>Dear Admin,</p><p>You have successfully '.$operation_performed.' <b>GEL '.$payment_amount.'</b> into user '.$fetch_user->users_fname.' '.$fetch_user->users_lname.' \'s wallet. </p>';
           $data_admin= array(
              'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
              'email' =>$fetch_admin->users_email
          );
           Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_admin['email'], $data_admin['name'])->subject('MONEY ADDED TO WALLET');
          });






             $get_commission_total=UserCommissions::select(\DB::raw("(COALESCE(sum(comm_credit_amount), 0)-COALESCE(sum(comm_debit_amount), 0)) as total_wallet_amount"))->where('comm_user_id',$users_id)->where('comm_status',1)->groupBy('comm_user_id')->first();

               if(!empty($get_commission_total))
                $total_amount="GEL ".$get_commission_total->total_wallet_amount;
            else
               $total_amount="GEL 0";

            echo "success_".$total_amount;
          }
          else
          {
            echo "fail";
          }

  }
  else
  {
    echo "fail";
  }

}

}

