<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\States;
use App\Cities;
use App\Currency;
use App\Suppliers;
use App\UserRights;
use App\Restaurants;
use App\Restaurants_log;
use App\RestaurantType;
use App\RestaurantMenuCategory;
use App\RestaurantFood;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use PDF;
use Session;
use Cookie;
use Mail;
class RestaurantController extends Controller
{
	private function rights($menu)
    {
        $emp_id=session()->get('travel_users_id');
        $right_array=array();
        $employees=Users::where('users_id',$emp_id)->where('users_pid',0)->where('users_status',1)->first();
        if(!empty($employees))
        {
            $right_array['add']=1;
            $right_array['view']=1;
            $right_array['edit_delete']=1;
            $right_array['report']=1;
            $right_array['admin']=1;
            $right_array['admin_which']="add,view,edit_delete,report";
        }
        else
        {

            $employees=Users::where('users_id',$emp_id)->where('users_status',1)->first();
            if(!empty($employees))
            {
                $user_rights=UserRights::where('emp_id',$emp_id)->where('menu',$menu)->first();
                if(!empty($user_rights))
                {
                    $right_array['add']=$user_rights->add_status;
                    $right_array['view']=$user_rights->view_status;
                    $right_array['edit_delete']=$user_rights->edit_del_status;
                    $right_array['report']=$user_rights->report_status;
                    $right_array['admin']=$user_rights->admin_status;
                    if($user_rights->admin_which_status!="")
                        $right_array['admin_which']=$user_rights->admin_which_status;
                    else
                        $right_array['admin_which']="No";
                }
                else
                {
                    $right_array['add']=0;
                    $right_array['view']=0;
                    $right_array['edit_delete']=0;
                    $right_array['report']=0;
                    $right_array['admin']=0;
                    $right_array['admin_which']="No";
                }
            }
            else
            {
                $right_array['add']=0;
                $right_array['view']=0;
                $right_array['edit_delete']=0;
                $right_array['report']=0;
                $right_array['admin']=0;
                $right_array['admin_which']="No";
            }

        }

        return $right_array;

    }



    public function restaurant_type(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('restaurant-type');
        $fetch_restaurant_type=RestaurantType::get();
        return view('mains.restaurant-type')->with(compact('fetch_restaurant_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function restaurant_type_insert(Request $request)

{

    $restaurant_type_name=$request->get('restaurant_type_name');

    $check_restaurant_type=RestaurantType::where('restaurant_type_name',$restaurant_type_name)->first();

    if(!empty($check_restaurant_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $restaurant_type_insert=new RestaurantType;

        $restaurant_type_insert->restaurant_type_name=$restaurant_type_name;

        $restaurant_type_insert->restaurant_type_created_by=$emp_id;

        if($restaurant_type_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function restaurant_type_update(Request $request)

{

    $restaurant_type_id=$request->get('restaurant_type_id');

   $restaurant_type_name=$request->get('restaurant_type_name');

    $check_restaurant_type=RestaurantType::where('restaurant_type_name',$restaurant_type_name)->where('restaurant_type_id','!=',$restaurant_type_id)->first();

    if(!empty($check_restaurant_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_restaurant_type_array=array("restaurant_type_name"=>$restaurant_type_name,);
        $update_restaurant_type=RestaurantType::where('restaurant_type_id',$restaurant_type_id)->update($update_restaurant_type_array);

        if($update_restaurant_type)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}


 public function restaurant_menu_category(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('restaurant-menu-category');
        $fetch_restaurant_menu_category=RestaurantMenuCategory::get();
        return view('mains.restaurant-menu-category')->with(compact('fetch_restaurant_menu_category','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function restaurant_menu_category_insert(Request $request)

{

    $restaurant_menu_category_name=$request->get('restaurant_menu_category_name');
    $restaurant_menu_category_description=$request->get('restaurant_menu_category_description');

    $check_restaurant_menu_category=RestaurantMenuCategory::where('restaurant_menu_category_name',$restaurant_menu_category_name)->first();

    if(!empty($check_restaurant_menu_category))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $restaurant_menu_category_insert=new RestaurantMenuCategory;

        $restaurant_menu_category_insert->restaurant_menu_category_name=$restaurant_menu_category_name;
        $restaurant_menu_category_insert->restaurant_menu_category_description=$restaurant_menu_category_description;

        $restaurant_menu_category_insert->restaurant_menu_category_created_by=$emp_id;

        if($restaurant_menu_category_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function restaurant_menu_category_update(Request $request)

{

    $restaurant_menu_category_id=$request->get('restaurant_menu_category_id');

   $restaurant_menu_category_name=$request->get('restaurant_menu_category_name');
$restaurant_menu_category_description=$request->get('restaurant_menu_category_description');
    $check_restaurant_menu_category=RestaurantMenuCategory::where('restaurant_menu_category_name',$restaurant_menu_category_name)->where('restaurant_menu_category_id','!=',$restaurant_menu_category_id)->first();

    if(!empty($check_restaurant_menu_category))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_restaurant_menu_category_array=array("restaurant_menu_category_name"=>$restaurant_menu_category_name,"restaurant_menu_category_description"=>$restaurant_menu_category_description);
        $update_restaurant_menu_category=RestaurantMenuCategory::where('restaurant_menu_category_id',$restaurant_menu_category_id)->update($update_restaurant_menu_category_array);

        if($update_restaurant_menu_category)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}
public function restaurant_management(Request $request)
{
    if(session()->has('travel_users_id'))
  {
    $emp_id=session()->get('travel_users_id');
    $rights=$this->rights('restaurant-management');
    if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
    {
      $get_restaurants=Restaurants::orderBy(\DB::raw('-`restaurant_show_order`'), 'desc')->orderBy('restaurant_id','asc')->get();
    }
    else
    {
      $get_restaurants=Restaurants::where('restaurant_created_by',$emp_id)->where('restaurant_create_role','!=','Supplier')->orderBy(\DB::raw('-`restaurant_show_order`'), 'desc')->orderBy('restaurant_id','asc')->get();
    }
    return view('mains.restaurant-management')->with(compact('get_restaurants','rights'));
  }
  else
  {
    return redirect()->route('index');
  }
}
public function create_restaurant(Request $request)
{
  if(session()->has('travel_users_id'))
      {
    $rights=$this->rights('restaurant-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $fetch_restaurant_type=RestaurantType::where('restaurant_type_status',1)->get();
    return view('mains.create-restaurant')->with(compact('countries','currency','suppliers','fetch_restaurant_type','rights'));
  }
  else
  {
    return redirect()->route('index');
  }
}

public function insert_restaurant(Request $request)
{
    // echo "<pre>";
    // print_r($request->all());
    // die();
  $restaurant_role=$request->get('restaurant_role');
  if($restaurant_role=='supplier')
  {
    $user_id=$request->get('supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $restaurant_type=$request->get('restaurant_type');
  $restaurant_name=$request->get('restaurant_name');
  $restaurant_owner_name=$request->get('restaurant_owner_name');
   $restaurant_email_address=$request->get('restaurant_email_address');
    $restaurant_contact_number=$request->get('restaurant_contact_number');
  $supplier_id=$request->get('supplier_name');
  $restaurant_address=$request->get('restaurant_address');
  $restaurant_country=$request->get('restaurant_country');
  $restaurant_city=$request->get('restaurant_city');
  $validity_fromdate=$request->get('validity_operation_from');
  $validity_todate=$request->get('validity_operation_to');
  $validity_fromtime=$request->get('restaurant_time_from');
  $validity_totime=$request->get('restaurant_time_to');
$no_of_tables=$request->get('no_of_tables');
  $week_monday=$request->get('week_monday');
  $week_tuesday=$request->get('week_tuesday');
  $week_wednesday=$request->get('week_wednesday');
  $week_thursday=$request->get('week_thursday');
  $week_friday=$request->get('week_friday');
  $week_saturday=$request->get('week_saturday');
  $week_sunday=$request->get('week_sunday');
  $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);
  $operating_weekdays=serialize($operating_weekdays);
    $restaurant_available_for_delivery=$request->get('restaurant_available_for_delivery');
  $restaurant_currency=$request->get('restaurant_currency');
  $restaurant_blackout_dates=$request->get('blackout_days');

  
  $restaurant_inclusions=$request->get('restaurant_inclusions');
  $restaurant_exclusions=$request->get('restaurant_exclusions');
  $restaurant_description=$request->get('restaurant_description');
  $restaurant_cancel_policy=$request->get('restaurant_cancellation');
  $restaurant_terms_conditions=$request->get('restaurant_terms_conditions');
  $restaurant_created_by=$user_id;
  $restaurant_images=array();
//multifile uploading
  if($request->hasFile('upload_ativity_images'))
  {
    foreach($request->file('upload_ativity_images') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_restaurant = "restaurant-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/restaurant_images/';
        $file->move($dir1, $image_restaurant);
        $restaurant_images[]=$image_restaurant;
      }
    }
  }
  $restaurant_images=serialize($restaurant_images);
  $insert_restaurant=new Restaurants;
  $insert_restaurant->restaurant_name=$restaurant_name; 
  $insert_restaurant->restaurant_owner_name=$restaurant_owner_name;
  $insert_restaurant->restaurant_email=$restaurant_email_address;
  $insert_restaurant->restaurant_contact=$restaurant_contact_number;
  $insert_restaurant->restaurant_type=$restaurant_type;
  $insert_restaurant->supplier_id=$supplier_id;
  $insert_restaurant->restaurant_address=$restaurant_address;
  $insert_restaurant->restaurant_country=$restaurant_country;
  $insert_restaurant->restaurant_city=$restaurant_city;
  $insert_restaurant->validity_fromdate=$validity_fromdate;
  $insert_restaurant->validity_todate=$validity_todate;
  $insert_restaurant->validity_fromtime=$validity_fromtime;
  $insert_restaurant->validity_totime=$validity_totime;
  $insert_restaurant->no_of_tables=$no_of_tables;
  $insert_restaurant->operating_weekdays=$operating_weekdays;
  $insert_restaurant->restaurant_currency=$restaurant_currency;
  $insert_restaurant->restaurant_blackout_dates=$restaurant_blackout_dates;
  $insert_restaurant->restaurant_inclusions=$restaurant_inclusions;
  $insert_restaurant->restaurant_exclusions=$restaurant_exclusions;
  $insert_restaurant->restaurant_description=$restaurant_description;
  $insert_restaurant->restaurant_cancel_policy=$restaurant_cancel_policy;
  $insert_restaurant->restaurant_terms_conditions=$restaurant_terms_conditions;
  $insert_restaurant->restaurant_images=$restaurant_images;
   $insert_restaurant->restaurant_available_for_delivery=$restaurant_available_for_delivery;
  $insert_restaurant->restaurant_created_by=$restaurant_created_by;
  $insert_restaurant->restaurant_role=$user_role;
  if($user_role=="Supplier")
  {
    $insert_restaurant->restaurant_approve_status=0;
  }
  if($insert_restaurant->save())
  {

    $last_id=$insert_restaurant->id;
    $insert_restaurant_log=new Restaurants_log;
    $insert_restaurant_log->restaurant_id=$last_id;
    $insert_restaurant_log->restaurant_name=$restaurant_name; 
    $insert_restaurant_log->restaurant_owner_name=$restaurant_owner_name;
    $insert_restaurant_log->restaurant_email=$restaurant_email_address;
    $insert_restaurant_log->restaurant_contact=$restaurant_contact_number;
    $insert_restaurant_log->restaurant_type=$restaurant_type;
    $insert_restaurant_log->supplier_id=$supplier_id;
    $insert_restaurant_log->restaurant_address=$restaurant_address;
    $insert_restaurant_log->restaurant_country=$restaurant_country;
    $insert_restaurant_log->restaurant_city=$restaurant_city;
    $insert_restaurant_log->validity_fromdate=$validity_fromdate;
    $insert_restaurant_log->validity_todate=$validity_todate;
    $insert_restaurant_log->validity_fromtime=$validity_fromtime;
    $insert_restaurant_log->validity_totime=$validity_totime;
    $insert_restaurant_log->no_of_tables=$no_of_tables;
    $insert_restaurant_log->operating_weekdays=$operating_weekdays;
    $insert_restaurant_log->restaurant_currency=$restaurant_currency;
    $insert_restaurant_log->restaurant_blackout_dates=$restaurant_blackout_dates;
    $insert_restaurant_log->restaurant_inclusions=$restaurant_inclusions;
    $insert_restaurant_log->restaurant_exclusions=$restaurant_exclusions;
    $insert_restaurant_log->restaurant_cancel_policy=$restaurant_cancel_policy;
    $insert_restaurant_log->restaurant_terms_conditions=$restaurant_terms_conditions;
    $insert_restaurant_log->restaurant_images=$restaurant_images;
    $insert_restaurant_log->restaurant_available_for_delivery=$restaurant_available_for_delivery;
    $insert_restaurant_log->restaurant_created_by=$restaurant_created_by;
    $insert_restaurant_log->restaurant_role=$user_role;
    $insert_restaurant_log->restaurant_operation_performed="INSERT";
    $insert_restaurant_log->save();
    echo "success";
  }
  else
  {
    echo "fail";
  }
}


public function edit_restaurant($restaurant_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('restaurant-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    $fetch_restaurant_type=RestaurantType::where('restaurant_type_status',1)->get();
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->first();
    }
    else
    {
      $get_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->where('restaurant_created_by',$emp_id)->where('restaurant_role','!=','Supplier')->first();
    }

    if(!empty($get_restaurant))
    {
      $supplier_id=$get_restaurant->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_restaurant->restaurant_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.edit-restaurant')->with(compact('countries','currency','cities','suppliers','get_restaurant','countries_data','fetch_restaurant_type','rights'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}

public function update_restaurant(Request $request)
{
  // echo "<pre>";
    // print_r($request->all());
    // die();
   $restaurant_id=urldecode(base64_decode(base64_decode($request->get('restaurant_id'))));
  $restaurant_role=$request->get('restaurant_role');
  if($restaurant_role=='supplier')
  {
    $user_id=$request->get('supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
   $check_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->get();
  if(count($check_restaurant)>0)
  {
  $restaurant_type=$request->get('restaurant_type');
  $restaurant_name=$request->get('restaurant_name');
  $restaurant_owner_name=$request->get('restaurant_owner_name');
   $restaurant_email_address=$request->get('restaurant_email_address');
    $restaurant_contact_number=$request->get('restaurant_contact_number');
  $supplier_id=$request->get('supplier_name');
  $restaurant_address=$request->get('restaurant_address');
  $restaurant_country=$request->get('restaurant_country');
  $restaurant_city=$request->get('restaurant_city');
  $validity_fromdate=$request->get('validity_operation_from');
  $validity_todate=$request->get('validity_operation_to');
  $validity_fromtime=$request->get('restaurant_time_from');
  $validity_totime=$request->get('restaurant_time_to');
$no_of_tables=$request->get('no_of_tables');
  $week_monday=$request->get('week_monday');
  $week_tuesday=$request->get('week_tuesday');
  $week_wednesday=$request->get('week_wednesday');
  $week_thursday=$request->get('week_thursday');
  $week_friday=$request->get('week_friday');
  $week_saturday=$request->get('week_saturday');
  $week_sunday=$request->get('week_sunday');
  $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);
  $operating_weekdays=serialize($operating_weekdays);
  $restaurant_currency=$request->get('restaurant_currency');
  $restaurant_available_for_delivery=$request->get('restaurant_available_for_delivery');
  $restaurant_blackout_dates=$request->get('blackout_days');

  
  $restaurant_inclusions=$request->get('restaurant_inclusions');
  $restaurant_exclusions=$request->get('restaurant_exclusions');
  $restaurant_description=$request->get('restaurant_description');
  $restaurant_cancel_policy=$request->get('restaurant_cancellation');
  $restaurant_terms_conditions=$request->get('restaurant_terms_conditions');
  $restaurant_created_by=$user_id;
    if(!empty($request->get('upload_ativity_already_images')))
    {
     $restaurant_images=$request->get('upload_ativity_already_images');
   }
   else
   {
    $restaurant_images=array();
  }
//multifile uploading
  if($request->hasFile('upload_ativity_images'))
  {
    foreach($request->file('upload_ativity_images') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_restaurant = "restaurant-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/restaurant_images/';
        $file->move($dir1, $image_restaurant);
        $restaurant_images[]=$image_restaurant;
      }
    }
  }
  $restaurant_images=serialize($restaurant_images);
 $update_restaurant_array=array(
  "restaurant_name"=>$restaurant_name, 
  "restaurant_owner_name"=>$restaurant_owner_name,
  "restaurant_email"=>$restaurant_email_address,
  "restaurant_contact"=>$restaurant_contact_number,
  "restaurant_type"=>$restaurant_type,
  "supplier_id"=>$supplier_id,
  "restaurant_address"=>$restaurant_address,
  "restaurant_country"=>$restaurant_country,
  "restaurant_city"=>$restaurant_city,
  "validity_fromdate"=>$validity_fromdate,
  "validity_todate"=>$validity_todate,
  "validity_fromtime"=>$validity_fromtime,
  "validity_totime"=>$validity_totime,
  "no_of_tables"=>$no_of_tables,
  "operating_weekdays"=>$operating_weekdays,
  "restaurant_currency"=>$restaurant_currency,
  "restaurant_blackout_dates"=>$restaurant_blackout_dates,
  "restaurant_inclusions"=>$restaurant_inclusions,
  "restaurant_exclusions"=>$restaurant_exclusions,
  "restaurant_description"=>$restaurant_description,
  "restaurant_cancel_policy"=>$restaurant_cancel_policy,
  "restaurant_terms_conditions"=>$restaurant_terms_conditions,
  "restaurant_images"=>$restaurant_images,
"restaurant_available_for_delivery"=>$restaurant_available_for_delivery);
 $update_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->update($update_restaurant_array);
  if($update_restaurant)
  {

    $last_id=$restaurant_id;
    $insert_restaurant_log=new Restaurants_log;
    $insert_restaurant_log->restaurant_id=$last_id;
    $insert_restaurant_log->restaurant_name=$restaurant_name; 
    $insert_restaurant_log->restaurant_owner_name=$restaurant_owner_name;
    $insert_restaurant_log->restaurant_email=$restaurant_email_address;
    $insert_restaurant_log->restaurant_contact=$restaurant_contact_number;
    $insert_restaurant_log->restaurant_type=$restaurant_type;
    $insert_restaurant_log->supplier_id=$supplier_id;
    $insert_restaurant_log->restaurant_address=$restaurant_address;
    $insert_restaurant_log->restaurant_country=$restaurant_country;
    $insert_restaurant_log->restaurant_city=$restaurant_city;
    $insert_restaurant_log->validity_fromdate=$validity_fromdate;
    $insert_restaurant_log->validity_todate=$validity_todate;
    $insert_restaurant_log->validity_fromtime=$validity_fromtime;
    $insert_restaurant_log->validity_totime=$validity_totime;
    $insert_restaurant_log->no_of_tables=$no_of_tables;
    $insert_restaurant_log->operating_weekdays=$operating_weekdays;
    $insert_restaurant_log->restaurant_currency=$restaurant_currency;
    $insert_restaurant_log->restaurant_blackout_dates=$restaurant_blackout_dates;
    $insert_restaurant_log->restaurant_inclusions=$restaurant_inclusions;
    $insert_restaurant_log->restaurant_exclusions=$restaurant_exclusions;
    $insert_restaurant_log->restaurant_cancel_policy=$restaurant_cancel_policy;
    $insert_restaurant_log->restaurant_terms_conditions=$restaurant_terms_conditions;
    $insert_restaurant_log->restaurant_images=$restaurant_images;
    $insert_restaurant_log->restaurant_available_for_delivery=$restaurant_available_for_delivery;
    $insert_restaurant_log->restaurant_created_by=$restaurant_created_by;
    $insert_restaurant_log->restaurant_role=$user_role;
    $insert_restaurant_log->restaurant_operation_performed="UPDATE";
    $insert_restaurant_log->save();
    echo "success";
  }
  else
  {
    echo "fail";
  }
  }
else
{
  echo "fail";
}

}


public function restaurant_details($restaurant_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('restaurant-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    $fetch_restaurant_type=RestaurantType::where('restaurant_type_status',1)->get();
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->first();
    }
    else
    {
      $get_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->where('restaurant_created_by',$emp_id)->where('restaurant_role','!=','Supplier')->first();
    }

    if(!empty($get_restaurant))
    {
      $supplier_id=$get_restaurant->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_restaurant->restaurant_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.restaurant-detail-view')->with(compact('countries','currency','cities','suppliers','get_restaurant','countries_data','fetch_restaurant_type','rights'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}


public function update_restaurant_approval(Request $request)
{
  $id=$request->get('restaurant_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="approve")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_approve_status"=>1]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="reject")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_approve_status"=>2]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}

  public function update_restaurant_active_inactive(Request $request)
{
  $id=$request->get('restaurant_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_status"=>1]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_status"=>0]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}
public function update_restaurant_bestseller(Request $request)
{
  $id=$request->get('restaurant_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="bestselleryes")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_best_status"=>1]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="bestsellerno")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_best_status"=>0]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}
public function update_restaurant_popular(Request $request)
{
  $id=$request->get('restaurant_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="popularyes")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_popular_status"=>1]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="popularno")
  {
    $update_restaurant=Restaurants::where('restaurant_id',$id)->update(["restaurant_popular_status"=>0]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}
public function sort_restaurant(Request $request)
{
  $sort_restaurant_array=$request->get('new_data');
  $success_array=array();
  for($count=0;$count<count($sort_restaurant_array);$count++)
  {
    $restaurant_id=$sort_restaurant_array[$count]['restaurant_id'];
    $new_order=$sort_restaurant_array[$count]['new_order'];
    $update_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->update(["restaurant_show_order"=>$new_order]);
    if($update_restaurant)
    {
      $success_array[]="success";
    }
    else
    {
      $success_array[]="not_success";
    }
  }
  echo json_encode($success_array);
}

 public function itinerary_get_restaurant_details(Request $request)
  {
    $mainid=$request->restaurant_id;
    $data=array();
     $get_restaurant=Restaurants::where('restaurant_id', $mainid)->first();
    if(!empty($get_restaurant))
  {
     $data["status"]='200';
     $get_menu_categories=RestaurantMenuCategory::where('restaurant_menu_category_status',1)->get();
      $data["body"]["restaurant_id"]=$get_restaurant->restaurant_id;
     $data["body"]["restaurant_name"]=$get_restaurant->restaurant_name;
      $data["body"]["restaurant_address"]=$get_restaurant->restaurant_address;
       $data["body"]["restaurant_description"]=$get_restaurant->restaurant_description;
       $data["body"]["restaurant_type"]=$get_restaurant->getRestaurantType->restaurant_type_name." Restaurant";
        $data["body"]["restaurant_timing"]= $get_restaurant->validity_fromtime." - ".$get_restaurant->validity_totime;
        $restaurant_images_array=array();
        if($get_restaurant->restaurant_images!=null && $get_restaurant->restaurant_images!="")
        {
          $restaurant_images=unserialize($get_restaurant->restaurant_images);
          if(!empty($restaurant_images[0]))
          {
            foreach($restaurant_images as $restaurant_image)
            {
              $restaurant_images_array[]=asset("assets/uploads/restaurant_images")."/".$restaurant_image;
            }
          }
          else
          {
            $restaurant_images_array[]=asset("assets/images/no-photo.png");
          }
        }
        else
        {
          $restaurant_images_array[]=asset("assets/images/no-photo.png");
        }
        $data["body"]["restaurant_images"]=$restaurant_images_array;

        $data["body"]["restaurant_details_link"]=route("restaurant-details-view",["restaurant_id"=>$get_restaurant->restaurant_id]);

       

        $menu_html="<style>
        .Tab-para
{
  font-size: 21px;
  display: flex;
  justify-content: flex-start;
  align-items: end;
  color: #5c6975;
  font-style: normal;
  font-weight: 400;
  float:left;
}
.trad
{
 position: absolute;
 top: 0;
 right: 13px;
 background: #ffffff;
 color: #5cb660;
 border: 1px solid #5cb660;
 padding: 4px 10px;
 border-radius: 8px;
 min-width: 99px;
 width: auto;
 text-align: center;
}
p.address {
  margin-bottom: 25px;
}
.collapse-btn
{
 width: 100%;
 text-align: left;
 border-radius: 0px;
 /* background: gray; */
 border: none;
 color:white;
 background:#ec407a;
 cursor:pointer;
 padding:10px 10px;
 font-weight:600;
}
.sight-seeing
{
  padding: 10px 10px;
  margin-top: 16px;
}
.btn-heading
{
  font-family: 'Roboto Condensed',sans-serif;
  font-size: 16px;
  text-transform: uppercase;
  font-weight:600;
}
.cat-text
{
  color: #000;
  padding-right: 4px;
}
.break-text
{
  color: #e53932;
}
.para-text
{
  font-size: 14px;
  line-height: 20px;
  color: #888;;
}
.rate
{
  color: #e53932;
  font-size:18px;
}
.col-div
{
  padding:20px 10px;
}
.row-border
{
  border-color: gray;
  border-bottom: 1px dashed;
  padding: 14px 10px;
}
.fur-p
{
  font-family: 'Roboto Condensed',sans-serif;
  font-size: 16px;
  text-transform: uppercase;
  padding-bottom: 9px;
}
.right-class
{
  position: absolute;
  right: 26px;
}
        </style>
        <div class='row'>";
        $food_counter=0; 
                      foreach($get_menu_categories as $menu_category_key =>$menu_category_value)
                      {
                      $get_menu_foods=RestaurantFood::where('food_status',1)->where('food_approval_status',1)->where('restaurant_id_fk',$get_restaurant->restaurant_id)->where('menu_category_id_fk',$menu_category_value->restaurant_menu_category_id)->get();

                      if(count($get_menu_foods)>0)
                      {
                      
                        $menu_html.='<div class="col-md-12 mt-1">
                          <button type="button" class="collapse-btn" data-toggle="collapse" data-target="#'.strtolower(str_replace(' ','_',$menu_category_value->restaurant_menu_category_name)).'_'.$menu_category_value->restaurant_menu_category_id.'">'.$menu_category_value->restaurant_menu_category_name.'<span class="right-class"><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                          <div id="'.strtolower(str_replace(' ','_',$menu_category_value->restaurant_menu_category_name)).'_'.$menu_category_value->restaurant_menu_category_id.'" class="collapse col-div">';

                            foreach($get_menu_foods as $menu_food_key=>$menu_foods)
                            {

                            $menu_html.='<input type="hidden" name="food_id['.$food_counter.']"  id="food_id__'.$food_counter.'" value="'.$menu_foods->restaurant_food_id.'">
                            <input type="hidden" name="food_category_id['.$food_counter.']"  id="food_category_id__'.$food_counter.'" value="'.$menu_category_value->restaurant_menu_category_id.'">
                             <input type="hidden" name="food_name['.$food_counter.']" id="food_name__'.$food_counter.'"  value="'.$menu_foods->food_name.'">
                            <div class="row row-border">';
                              
                              $get_food_images=unserialize($menu_foods->food_images);

                              if($menu_foods->food_images!=null && $menu_foods->food_images!="" && count($get_food_images)>0)
                              {
                               $menu_html.='<div class="col-md-3">
                                <img src='.asset("assets/uploads/food_images").'/'.$get_food_images[0].'>
                             </div>
                             <div class="col-md-7">';
                              }
                              else
                              {
                                $menu_html.='<div class="col-md-10">';
                               }
                                 
                                  $menu_html.='<p class="btn-heading">'.$menu_foods->food_name.'</p>
                                  <span class="cat-text">Category:</span>&nbsp;<span class="break-text">'.$menu_category_value->restaurant_menu_category_name.'</span>
                                  <p class="para-text"><b>Ingredients : </b>'.$menu_foods->food_ingredients.'</p>
                                </div>
                                <div class="col-md-2">';
                                
                                  if($menu_foods->food_discounted_price!=null && $menu_foods->food_discounted_price<=$menu_foods->food_price)
                                  {
                                    $price=$menu_foods->food_discounted_price;
                                  }
                                  else
                                  {
                                    $price=$menu_foods->food_price;
                                  }
                                  
                                    $total_cost=round($price);
                                 
                                     $menu_html.='<p class="rate">GEL '.$total_cost.'</p>
                                   <input type="hidden" name="food_price['.$food_counter.']" id="food_price__'.$food_counter.'"  value="'.$total_cost.'">
                                  <div class="form-group">
                                     <input type="hidden" name="food_unit['.$food_counter.']" id="food_unit__'.$food_counter.'"  value="'.$menu_foods->food_unit.'">
                                    <select class="form-control food_qty" name="food_qty['.$food_counter.']" id="food_qty__'.$food_counter.'">
                                      <option value="0">Quantity</option>';

                                      for($op_count=1;$op_count<=50;$op_count++)
                                      {
                                        $menu_html.='<option value="'.$op_count.'">'.$op_count.'</option>';
                                    }
                                      $menu_html.='</select> / '.$menu_foods->food_unit.'
                                  </div>
                                </div>
                              </div>';
                         
                              $food_counter++;
                            
                             }

                              $menu_html.='</div>
                          </div>';
                          }
                        }

        $data["body"]["restaurant_menu_html"]=$menu_html;                

  }
  else
  {
    $data["status"]='404';
     $data["body"]="";
  }
  

  echo json_encode($data);
}

}
