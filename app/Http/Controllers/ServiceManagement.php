<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\States;
use App\Cities;
use App\Currency;
use App\Suppliers;
use App\Activities;
use App\Activities_log;
use App\Transport;
use App\Transport_log;
use App\Hotels;
use App\HotelRooms;
use App\HotelRoomSeasons;
use App\HotelRoomSeasonOccupancy;
use App\Hotels_log;
use App\HotelMeal;
use App\Drivers;
use App\Drivers_log;
use App\Guides;
use App\Guides_log;
use App\SightSeeing;
use App\SightSeeing_log;
use App\Languages;
use App\FuelType;
use App\Vehicles;
use App\VehicleType;
use App\UserRights;
use App\Agents;
use App\SavedItinerary;
use Session;
use App\Bookings;
use App\BookingCustomer;
use App\BookingInstallment;
use App\Amenities;
use App\SubAmenities;
use App\AirportMaster;
use App\Transfers;
use App\TransferDetails;
use App\TourType;
use App\HotelType;
use App\ActivityType;
use App\Restaurants;
use App\Expense;
use App\Income;
use App\IncomeExpenseCategory;
use App\SettingTargetCommission;
use App\UserCommissions;
use PDF;
use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Schema;

class ServiceManagement extends Controller
{

   private $currencyApiKey;
  private $base_currency;
  public function __construct()
  {
    date_default_timezone_set('Asia/Dubai');
    $this->currencyApiKey="f4f2d4f26341429dcf7e";
     $this->base_currency="GEL";
  }
private function rights($menu)
{
  $emp_id=session()->get('travel_users_id');
  $right_array=array();
  $employees=Users::where('users_id',$emp_id)->where('users_pid',0)->where('users_status',1)->first();
  if(!empty($employees))
  {
    $right_array['add']=1;
    $right_array['view']=1;
    $right_array['edit_delete']=1;
    $right_array['report']=1;
    $right_array['admin']=1;
    $right_array['admin_which']="add,view,edit_delete,report";
  }
  else
  {
    $employees=Users::where('users_id',$emp_id)->where('users_status',1)->first();
    if(!empty($employees))
    {
      $user_rights=UserRights::where('emp_id',$emp_id)->where('menu',$menu)->first();
      if(!empty($user_rights))
      {
        $right_array['add']=$user_rights->add_status;
        $right_array['view']=$user_rights->view_status;
        $right_array['edit_delete']=$user_rights->edit_del_status;
        $right_array['report']=$user_rights->report_status;
        $right_array['admin']=$user_rights->admin_status;
        if($user_rights->admin_which_status!="")
          $right_array['admin_which']=$user_rights->admin_which_status;
        else
          $right_array['admin_which']="No";
      }
      else
      {
        $right_array['add']=0;
        $right_array['view']=0;
        $right_array['edit_delete']=0;
        $right_array['report']=0;
        $right_array['admin']=0;
        $right_array['admin_which']="No";
      }
    }
    else
    {
      $right_array['add']=0;
      $right_array['view']=0;
      $right_array['edit_delete']=0;
      $right_array['report']=0;
      $right_array['admin']=0;
      $right_array['admin_which']="No";
    }
  }
  return $right_array;
}
public function service_management(Request $request)
{
  if(session()->has('travel_users_id'))
  {
    $emp_id=session()->get('travel_users_id');
    $rights=$this->rights('service-management');
    if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
    {
      // $get_activites=Activities::orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','asc')->get();
      // $get_transport=Transport::get();
      $get_hotels=Hotels::orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','asc')->get();
      // $get_guides=Guides::orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','asc')->get();
      // $get_drivers=Drivers::orderBy(\DB::raw('-`driver_show_order`'), 'desc')->orderBy('driver_id','asc')->get();
      // $get_sightseeing=SightSeeing::orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->orderBy('sightseeing_id','asc')->get();
      // $get_transfer=Transfers::orderBy(\DB::raw('-`transfer_show_order`'), 'desc')->orderBy('transfer_id','asc')->get();
    }
    else
    {
      // $get_activites=Activities::where('activity_created_by',$emp_id)->where('activity_role','!=','Supplier')->orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','asc')->get();
      // $get_transport=Transport::where('transfer_created_by',$emp_id)->where('transfer_create_role','!=','Supplier')->get();
      $get_hotels=Hotels::where('hotel_created_by',$emp_id)->where('hotel_create_role','!=','Supplier')->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','asc')->get();
      // $get_guides=Guides::where('guide_created_by',$emp_id)->where('guide_role',"!=","Supplier")->orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','asc')->get();
      // $get_drivers=Drivers::where('driver_created_by',$emp_id)->where('driver_role',"!=","Supplier")->orderBy(\DB::raw('-`driver_show_order`'), 'desc')->orderBy('driver_id','asc')->get();
      // $get_sightseeing=SightSeeing::where('sightseeing_created_by',$emp_id)->orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->orderBy('sightseeing_id','asc')->get();
      // $get_transfer=Transfers::where('transfer_created_by',$emp_id)->where('transfer_role','!=','Supplier')->orderBy(\DB::raw('-`transfer_show_order`'), 'desc')->orderBy('transfer_id','asc')->get();
    }
    // return view('mains.service-management')->with(compact('countries','cities','get_activites','get_suppliers','get_hotels','get_transport','get_guides','get_drivers','get_sightseeing','get_transfer','rights'));
    return view('mains.service-management')->with(compact('get_hotels','rights'));
  }
  else
  {
    return redirect()->route('index');
  }
}

public function fetch_service_management_tab(Request $request)
{
   $booking_type=$request->get('booking_type');
    $emp_id=session()->get('travel_users_id');
    $rights=$this->rights('service-management');
    $html="";
    if($booking_type=="hotel")
    {
         if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
      {
        $get_hotels=Hotels::orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','asc')->get();
      }
      else
      {
        $get_hotels=Hotels::where('hotel_created_by',$emp_id)->where('hotel_create_role','!=','Supplier')->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','asc')->get();
      }

      $html.='<div class="row">';
                  if($rights['add']==1)
                  {
                     $html.='<div class="col-sm-6 col-md-3">
                  <div class="form-group">

                    <a href="'.route('create-hotel').'"><button type="button"
                        class="btn btn-rounded btn-success">Create New Hotel</button></a>
                  </div>
                </div>';

                  }
               

                if($rights['view']==1)
                {   
                $html.='<div class="col-12">
                  <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="table-responsive">
                        <table  id="hotel_table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S. No.</th>
                              <th style="display:none">Hotel ID</th>
                              <th>Hotel Name</th>
                              <th>Hotel Address</th>
                              <th>City</th>
                              <th>Country</th>
                              <th>Supplier Name</th>';
                              if($rights['edit_delete']==1)
                              {
                              $html.='<th>Status</th>
                              <th>Approval</th>
                                <th>Best Seller</th>';
                              }
                              if($rights['edit_delete']==1 || $rights['view']==1)
                              {
                                 $html.='<th>Action</th>';
                              }
                              
                            $html.='</tr>
                          </thead>
                          <tbody>';
                            
                            $srno=1;
                        foreach($get_hotels as $hotels)
                        {
                        $html.='<tr id="tr_'.$hotels->hotel_id.'">
                          <td style="cursor: all-scroll;">'.$srno.'</td>
                          <td style="display:none">'.$hotels->hotel_id.'</td>
                          <td>'.$hotels->hotel_name.'</td>
                          <td>'.$hotels->hotel_address.'</td>
                          <td>'.$hotels->getCity->name.'</td>
                          <td>'.$hotels->getCountry->country_name.'</td>
                          <td>'.$hotels->getSupplier->supplier_name.'</td>';
                          if($rights['edit_delete']==1)
                          {
                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                              $html.='<td>';
                              if($hotels->hotel_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_'.$hotels->hotel_id.'">Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_'.$hotels->hotel_id.'">InActive</button>';

                              $html.='</td>
                              <td style="white-space: nowrap;">';
                              if($hotels->hotel_approve_status==1)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              }
                              else if($hotels->hotel_approve_status==0)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_'.$hotels->hotel_id.'">Approve</button>
                                <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_'.$hotels->hotel_id.'">Reject</button>';
                              }
                              else if($hotels->hotel_approve_status==2)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              }

                              $html.='</td>
                              <td>';
                              if($hotels->hotel_best_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_'.$hotels->hotel_id.'" >Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_'.$hotels->hotel_id.'">InActive</button>
                              </td>';
                            }
                            else if($hotels->hotel_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                              $html.='<td>';
                              if($hotels->hotel_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_'.$hotels->hotel_id.'">Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_'.$hotels->hotel_id.'">InActive</button>

                              </td>
                              <td style="white-space: nowrap;">';
                              if($hotels->hotel_approve_status==1)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              }
                              else if($hotels->hotel_approve_status==0)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_'.$hotels->hotel_id.'">Approve</button>
                                <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_'.$hotels->hotel_id.'">Reject</button>';
                              }
                              else if($hotels->hotel_approve_status==2)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              }
                              $html.='</td>
                              <td>';
                              if($hotels->hotel_best_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_'.$hotels->hotel_id.'" >Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_'.$hotels->hotel_id.'">InActive</button>
                              </td>';
                            }
                            else if($hotels->hotel_created_by==Session::get('travel_users_id') && $hotels->hotel_create_role!="Supplier" && $rights['edit_delete']==1)
                            {
                              $html.='<td>';
                              if($hotels->hotel_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_'.$hotels->hotel_id.'">Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_'.$hotels->hotel_id.'">InActive</button>';
                              $html.='</td>

                              <td style="white-space: nowrap;">';
                              if($hotels->hotel_approve_status==1)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              }
                              else if($hotels->hotel_approve_status==0)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_'.$hotels->hotel_id.'">Approve</button>
                                <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_'.$hotels->hotel_id.'">Reject</button>';
                              }
                              else if($hotels->hotel_approve_status==2)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              }
                              $html.='</td>
                              <td>';
                              if($hotels->hotel_best_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_'.$hotels->hotel_id.'" >Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_'.$hotels->hotel_id.'">InActive</button>
                              </td>';
                            }
                            else
                            {
                              $html.='<td>';
                              if($hotels->hotel_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_'.$hotels->hotel_id.'" disabled="disabled">Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_'.$hotels->hotel_id.'" disabled="disabled">InActive</button>';
                              $html.='</td>

                              <td style="white-space: nowrap;">';
                              if($hotels->hotel_approve_status==1)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              }
                              else if($hotels->hotel_approve_status==0)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_'.$hotels->hotel_id.'">Approve</button>
                                <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_'.$hotels->hotel_id.'">Reject</button>';
                              }
                              else if($hotels->hotel_approve_status==2)
                              {
                                $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              }
                              $html.='</td>
                              <td>';
                              if($hotels->hotel_best_status==1)
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_'.$hotels->hotel_id.'"  disabled="disabled">Active</button>';
                              else
                                $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_'.$hotels->hotel_id.' disabled="disabled"">InActive</button>
                              </td>';
                            }

                          }

                              if($rights['edit_delete']==1 || $rights['view']==1)
                                {
                                                          $html.='<td>';
                            if(strpos($rights['admin_which'],'view')!==false)
                            {
                             $html.='<a href="'.route('hotel-details',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"><i class="fa fa-eye"></i></a>'; 
                            }
                            else if($hotels->hotel_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'view')!==false)
                            {
                               $html.='<a href="'.route('hotel-details',['hotel_id'=>$hotels->hotel_id]).'"><i class="fa fa-eye"></i></a>';
                            }
                            else if($hotels->hotel_created_by==Session::get('travel_users_id') && $hotels->hotel_create_role!="Supplier" && $rights['view']==1)
                            {
                               $html.='<a href="'.route('hotel-details',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            }
                          

                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                            $html.='<a href="'.route('edit-hotel',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>
                            <a href="'.route('edit-hotel',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"  style="color:red"><i class="fa fa-pencil"></i></a>';
                            }
                            else if($hotels->hotel_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                                $html.='  <a href="'.route('edit-hotel',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>
                                 <a href="'.route('edit-hotel',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"  style="color:red"><i class="fa fa-pencil"></i></a>
                                ';
                            }
                            else if($hotels->hotel_created_by==Session::get('travel_users_id') && $hotels->hotel_create_role!="Supplier" && $rights['edit_delete']==1)
                            {
                               $html.='<a href="'.route('edit-hotel',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>
                               <a href="'.route('edit-hotel',['hotel_id'=>$hotels->hotel_id]).'" target="_blank"  style="color:red"><i class="fa fa-pencil"></i></a>';
                            }
                           

                         $html.='</td>';
                          }

                        $html.='</tr>';
                            $srno++;

                        }

                         $html.=' </tbody>

                        </table>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>';
              }
              $html.='</div>';



    }
    else if($booking_type=="sightseeing")
    {
           if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
        {
           $get_sightseeing=SightSeeing::orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->orderBy('sightseeing_id','asc')->get();
        }
        else
        {
           $get_sightseeing=SightSeeing::orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->orderBy('sightseeing_id','asc')->get();
        }

        $html.='<div class="row">';
                if($rights['add']==1)
                {
                $html.='<div class="col-sm-6 col-md-3">
                  <div class="form-group">

                    <a href="'.route('create-sightseeing').'"><button type="button"
                        class="btn btn-rounded btn-success">Create New SightSeeing</button></a>
                  </div>
                </div>';
               }
                if($rights['view']==1) 
                {
                $html.='<div class="col-12">
                  <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="table-responsive">
                        <table id="sightseeing_table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S. No.</th>
                              <th style="display:none">Sightseeing ID</th>
                              <th>Tour Name</th>
                              <th>Distance Covered</th>
                              <th>Duration</th>
                              <th>From City</th>
                              <th>In Between Cities</th>
                              <th>To City</th>
                              <th>Country</th>';

                              if($rights['edit_delete']==1)
                              {
                              $html.='<th>Status</th>
                              <th>Best Seller</th>
                              <th>Popular</th>';
                              }
                              if($rights['edit_delete']==1 || $rights['view']==1)
                              {
                              $html.='<th>Action</th>';
                              }

                            $html.='</tr>
                          </thead>
                          <tbody>';
                            $srno=1;
                            foreach($get_sightseeing as $sightseeing)
                            {

                        $html.='<tr id="tr_'.$sightseeing->sightseeing_id.'">

                          <td style="cursor: all-scroll;">'.$srno.'</td>
                          <td style="display:none">'.$sightseeing->sightseeing_id.'</td>

                          <td>'.$sightseeing->sightseeing_tour_name.'</td>
                          <td>'.$sightseeing->sightseeing_distance_covered.' KMS</td>
                          <td>';
                          if($sightseeing->sightseeing_duration!=null || $sightseeing->sightseeing_duration!="")
                          {
                            $html.=$sightseeing->sightseeing_duration;
                          }
                          else
                          {
                            $html.='NA';
                          }
                         $html.='</td>
                          <td>';
                          if(!empty($sightseeing->getFromCity->name))
                          {
                            $html.=$sightseeing->getFromCity->name;
                          }
                          $html.='</td>
                          <td>';
                            if($sightseeing->sightseeing_city_between!="")
                            {
                            $all_between_cities=explode(",",$sightseeing->sightseeing_city_between);
                            for($cities=0;$cities< count($all_between_cities);$cities++)
                            {
                                $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$sightseeing->sightseeing_country);
                            $html.=$fetch_city['name'];

                            if($cities<count($all_between_cities)-1)
                              $html.=" ,";

                            }
                            }
                            else
                             $html.=' NA ';
                            
                          $html.='</td>
                           <td>';
                           if(!empty($sightseeing->getToCity->name))
                           {
                            $html.=$sightseeing->getToCity->name;
                           }
                           else
                           {
                            $html.='NA';
                           }

                           $html.='</td>
                          <td>';
                            if(!empty($sightseeing->getCountry->country_name))
                           {
                            $html.=$sightseeing->getCountry->country_name;
                           }
                           else
                           {
                            $html.='NA';
                           }
                           $html.='</td>';
                            if($rights['edit_delete']==1)
                            {
                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                              $html.='<td>';
                              if($sightseeing->sightseeing_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';
                              
                            $html.='</td>

                            <td>';
                              if($sightseeing->sightseeing_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';
                              
                            $html.='</td>
                            <td>';
                              if($sightseeing->sightseeing_popular_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded popular_no btn-primary" id="popularno_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded popular_yes btn-default" id="popularyes_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';
                              
                            $html.='</td>';
                            }
                            else if($sightseeing->sightseeing_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                              $html.='<td>';
                              if($sightseeing->sightseeing_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';

                            $html.='</td>
                            <td>';
                              if($sightseeing->sightseeing_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';
                              
                            $html.='</td>
                            <td>';
                              if($sightseeing->sightseeing_popular_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded popular_no btn-primary" id="popularno_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded popular_yes btn-default" id="popularyes_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';
                             
                            $html.='</td>';
                          }
                            else if($sightseeing->sightseeing_created_by==Session::get('travel_users_id') && $activities->transfer_create_role!="Supplier" && $rights['edit_delete']==1)
                            {
                            $html.='<td>';
                              if($sightseeing->sightseeing_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';

                            $html.='</td>
                            <td>';
                              if($sightseeing->sightseeing_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';
                            
                            $html.='</td>
                            <td>';
                              if($sightseeing->sightseeing_popular_status==1)
                             $html.=' <button type="button" class="btn btn-sm btn-rounded popular_no btn-primary" id="popularno_sightseeing_'.$sightseeing->sightseeing_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded popular_yes btn-default" id="popularyes_sightseeing_'.$sightseeing->sightseeing_id.'">InActive</button>';
                              
                            $html.='</td>';
                          }
                            else
                            {
                            $html.='<td>';
                              if($sightseeing->sightseeing_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_sightseeing_'.$sightseeing->sightseeing_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_sightseeing_'.$sightseeing->sightseeing_id.'" disabled="disabled">InActive</button>';
                              
                            $html.='</td>
                            <td>';
                              if($sightseeing->sightseeing_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_sightseeing_'.$sightseeing->sightseeing_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_sightseeing_'.$sightseeing->sightseeing_id.'" disabled="disabled">InActive</button>';
                              
                            $html.='</td>
                            <td>';
                              if($sightseeing->sightseeing_popular_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded popular_no btn-primary" id="popularno_sightseeing_'.$sightseeing->sightseeing_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded popular_yes btn-default" id="popularyes_sightseeing_'.$sightseeing->sightseeing_id.'" disabled="disabled">InActive</button>';
                             
                           $html.=' </td>';
                            
                            
                            
                            }

                            
                            }

                            if($rights['edit_delete']==1 || $rights['view']==1)
                            {

                         $html.='<td>';
                            if(strpos($rights['admin_which'],'view')!==false)
                              $html.='<a href="'.route('sightseeing-details',['sightseeing_id'=>$sightseeing->sightseeing_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($sightseeing->sightseeing_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'view')!==false)
                              $html.='<a href="'.route('sightseeing-details',['sightseeing_id'=>$sightseeing->sightseeing_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($sightseeing->sightseeing_created_by==Session::get('travel_users_id') && $sightseeing->sightseeing_create_role!="Supplier" && $rights['view']==1)
                              $html.='<a href="'.route('sightseeing-details',['sightseeing_id'=>$sightseeing->sightseeing_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            

                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                              $html.='<a href="'.route('edit-sightseeing',['sightseeing_id'=>$sightseeing->sightseeing_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($sightseeing->sightseeing_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                              $html.='<a href="'.route('edit-sightseeing',['sightseeing_id'=>$sightseeing->sightseeing_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                          else if($sightseeing->sightseeing_created_by==Session::get('travel_users_id') && $sightseeing->sightseeing_create_role!="Supplier" && $rights['edit_delete']==1)
                            $html.='<a href="'.route('edit-sightseeing',['sightseeing_id'=>$sightseeing->sightseeing_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';


                          $html.='</td>';
                          }

                        $html.='</tr>';
                        $srno++;
                        }
                          $html.='</tbody>
                        </table>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>';
                }


              $html.='</div>';



    }
    else if($booking_type=="activity")
    {
      if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
    {
      $get_activites=Activities::orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','asc')->get();
    }
    else
    {
      $get_activites=Activities::where('activity_created_by',$emp_id)->where('activity_role','!=','Supplier')->orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','asc')->get();
    }

    $html.='<div class="row">';

              if($rights['add']==1)
              {
                 $html.='<div class="col-sm-6 col-md-3">
                  <div class="form-group">

                    <a href="'.route('create-activity').'"><button type="button"
                        class="btn btn-rounded btn-success">Create New Activity</button></a>
                  </div>
                </div>';
              }
              

                if($rights['view']==1) 
                {
              
                $html.='<div class="col-12">
                  <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="table-responsive">
                        <table id="activity_table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S. No.</th>
                              <th style="display:none">Activity ID</th>
                              <th>Activity Name</th>
                              <th>Supplier Name</th>
                              <th>Location</th>
                              <th>City</th>
                              <th>Country</th>';

                              if($rights['edit_delete']==1)
                              {

                              $html.='<th>Status</th>
                              <th>Approval</th>
                              <th>Best Seller</th>';

                              }
                            
                              if($rights['edit_delete']==1 || $rights['view']==1)
                              {

                             $html.='<th>Action</th>';

                              }

                            $html.='</tr>
                          </thead>
                          <tbody>';

                            $srno=1;
                            foreach($get_activites as $activities)
{
                       $html.=' <tr id="tr_'.$activities->activity_id.'" >
                          <td style="cursor: all-scroll;">'.$srno.'</td>
                          <td style="display:none">'.$activities->activity_id.'</td>

                          <td>'.$activities->activity_name.'</td>

                          <td>'.$activities->getSupplier->supplier_name.'</td>

                          <td>'.$activities->activity_location.'</td>

                          <td>'.$activities->getCity->name.'</td>

                          <td>'.$activities->getCountry->country_name.'</td>';

                            if($rights['edit_delete']==1)
                            {
                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            {

                            $html.='<td>';
                              
                              if($activities->activity_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_activity_'.$activities->activity_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_activity_'.$activities->activity_id.'">InActive</button>';
                              
                            $html.='</td>

                            <td style="white-space: nowrap;">';
                              if($activities->activity_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($activities->activity_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_activity_'.$activities->activity_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_activity_'.$activities->activity_id.'">Reject</button>';
                              else if($activities->activity_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              
                            $html.='</td>
                            <td>';
                              
                              if($activities->activity_best_status==1)
                             $html.=' <button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_activity_'.$activities->activity_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_activity_'.$activities->activity_id.'" >InActive</button>';
                              

                            $html.='</td>';

                          }
                            else if($activities->activity_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                           $html.='<td>';
                              
                              if($activities->activity_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_activity_'.$activities->activity_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_activity_'.$activities->activity_id.'">InActive</button>';
                              
                            $html.='</td>

                            <td style="white-space: nowrap;">';
                              if($activities->activity_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($activities->activity_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_activity_'.$activities->activity_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_activity_'.$activities->activity_id.'">Reject</button>';
                              else if($activities->activity_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              
                            $html.='</td>
                            <td>';
                              
                              if($activities->activity_best_status==1)
                             $html.=' <button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_activity_'.$activities->activity_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_activity_'.$activities->activity_id.'" >InActive</button>';
                              

                            $html.='</td>';
                          }
                            else if($activities->activity_created_by==Session::get('travel_users_id') && $activities->activity_role!="Supplier" && $rights['edit_delete']==1)
                            {
                            $html.='<td>';
                              
                              if($activities->activity_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_activity_'.$activities->activity_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_activity_'.$activities->activity_id.'">InActive</button>';
                              
                            $html.='</td>

                            <td style="white-space: nowrap;">';
                              if($activities->activity_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($activities->activity_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_activity_'.$activities->activity_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_activity_'.$activities->activity_id.'">Reject</button>';
                              else if($activities->activity_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              
                            $html.='</td>
                            <td>';
                              
                              if($activities->activity_best_status==1)
                             $html.=' <button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_activity_'.$activities->activity_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_activity_'.$activities->activity_id.'" >InActive</button>';
                              

                            $html.='</td>';
                          }
                            else
                            {
                             $html.='<td>';
                              
                              if($activities->activity_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_activity_'.$activities->activity_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_activity_'.$activities->activity_id.'" disabled="disabled">InActive</button>';
                              
                            $html.='</td>

                            <td style="white-space: nowrap;">';
                              if($activities->activity_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($activities->activity_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_activity_'.$activities->activity_id.'" disabled="disabled">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_activity_'.$activities->activity_id.'" disabled="disabled">Reject</button>';
                              else if($activities->activity_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                              
                            $html.='</td>
                            <td>';
                              
                              if($activities->activity_best_status==1)
                             $html.=' <button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_activity_'.$activities->activity_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_activity_'.$activities->activity_id.'" disabled="disabled">InActive</button>';
                              

                            $html.='</td>';
                           }

                         }

                            if($rights['edit_delete']==1 || $rights['view']==1)
                            {

                          $html.=' <td>';

                            if(strpos($rights['admin_which'],'view')!==false)
                            $html.='<a href="'.route('activity-details',['activity_id'=>$activities->activity_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($activities->activity_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'view')!==false)
                            $html.='<a href="'.route('activity-details',['activity_id'=>$activities->activity_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($activities->activity_created_by==Session::get('travel_users_id') && $activities->activity_role!="Supplier" && $rights['view']==1)
                           $html.='<a href="'.route('activity-details',['activity_id'=>$activities->activity_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';

                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                              $html.='<a href="'.route('edit-activity',['activity_id'=>$activities->activity_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($activities->activity_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                              $html.='<a href="'.route('edit-activity',['activity_id'=>$activities->activity_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($activities->activity_created_by==Session::get('travel_users_id') && $activities->activity_role!="Supplier" && $rights['edit_delete']==1)
                              $html.='<a href="'.route('edit-activity',['activity_id'=>$activities->activity_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                           

                          

                          $html.=' </td>';
                          }

                        $html.=' </tr>';
                          $srno++;

                       }

                          $html.=' </tbody>

                        </table>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>';
                }


              $html.=' </div>';

    }
    else if($booking_type=="guide")
    {
      if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
    {
      $get_guides=Guides::orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','asc')->get();
    }
    else
    {
      $get_guides=Guides::where('guide_created_by',$emp_id)->where('guide_role',"!=","Supplier")->orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','asc')->get();
    }

    $html.='<div class="row">';

                if($rights['add']==1)
                {

                $html.='<div class="col-sm-6 col-md-3">
                  <div class="form-group">

                    <a href="'.route('admin-create-guide').'"><button type="button"
                        class="btn btn-rounded btn-success">Create New Guide</button></a>
                  </div>
                </div>';
                }
              if($rights['view']==1) 
              {
               $html.=' <div class="col-12">
              <div class="box">
                <!-- /.box-header -->
                <div class="box-body" style="padding:0">
                  <div class="table-responsive">
                                        <div class="row">
                                        <table id="guide_table" class="table table-bordered table-striped">

                      <thead>
                        <tr>
                          <th>S. No.</th>
                          <th style="display:none">Guide ID</th>

                          <th>Name</th>

                          <th>Contact</th>

                          <th>Address</th>

                          <th>Supplier</th>

                          <th>City</th>

                          <th>Country</th>';
                        
                          if($rights['edit_delete']==1)
                          {
                             $html.='  <th>Status</th>
                          <th>Approval</th>
                          <th>Best Seller</th>';
                          }
                        
                          if($rights['edit_delete']==1 || $rights['view']==1)
                          {
                              $html.='<th>Action</th>';
                          }

                        $html.='</tr>
                      </thead>
                      <tbody>';
                        $srno=1;
                        foreach($get_guides as $guide)
                        {

                        $html.='<tr id="tr_'.$guide->guide_id.'">

                          <td style="cursor: all-scroll;">'.$srno.'</td>
                          <td style="display:none">'.$guide->guide_id.'</td>

                          <td>'.$guide->guide_first_name.' '.$guide->guide_last_name.'</td>

                          <td>'.$guide->guide_contact.'</td>

                          <td>'.$guide->guide_address.'</td>

                          <td>'.$guide->getSupplier->supplier_name.'</td>

                          <td>'.$guide->getCity->name.'</td>

                          <td>'.$guide->getCountry->country_name.'</td>';
                            if($rights['edit_delete']==1)
                            {
                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                            
                            $html.='<td>';
                              if($guide->guide_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_guide_'.$guide->guide_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_guide_'.$guide->guide_id.'">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($guide->guide_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              elseif($guide->guide_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_guide_'.$guide->guide_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_guide_'.$guide->guide_id.'">Reject</button>';
                              elseif($guide->guide_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                            
                           $html.=' </td>
                            <td>';
                              
                              if($guide->guide_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_guide_'.$guide->guide_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_guide_'.$guide->guide_id.'" >InActive</button>';
                             
                            $html.='</td>';
                          }
                            else if($guide->guide_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                             $html.='<td>';
                              if($guide->guide_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_guide_'.$guide->guide_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_guide_'.$guide->guide_id.'">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($guide->guide_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              elseif($guide->guide_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_guide_'.$guide->guide_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_guide_'.$guide->guide_id.'">Reject</button>';
                              elseif($guide->guide_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                            
                           $html.=' </td>
                            <td>';
                              
                              if($guide->guide_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_guide_'.$guide->guide_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_guide_'.$guide->guide_id.'" >InActive</button>';
                             
                            $html.='</td>';
                          }
                            else if($guide->guide_created_by==Session::get('travel_users_id') && $guide->guide_role!="Supplier" && $rights['edit_delete']==1)
                            {
                             $html.='<td>';
                              if($guide->guide_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_guide_'.$guide->guide_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_guide_'.$guide->guide_id.'">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($guide->guide_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              elseif($guide->guide_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_guide_'.$guide->guide_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_guide_'.$guide->guide_id.'">Reject</button>';
                              elseif($guide->guide_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                            
                           $html.=' </td>
                            <td>';
                              
                              if($guide->guide_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_guide_'.$guide->guide_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_guide_'.$guide->guide_id.'" >InActive</button>';
                             
                            $html.='</td>';
                          }
                            else
                            {

                            $html.='<td>';
                              if($guide->guide_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_guide_'.$guide->guide_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_guide_'.$guide->guide_id.'" disabled="disabled">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($guide->guide_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              elseif($guide->guide_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_guide_'.$guide->guide_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_guide_'.$guide->guide_id.'">Reject</button>';
                              elseif($guide->guide_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                            
                           $html.=' </td>
                            <td>';
                              
                              if($guide->guide_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_guide_'.$guide->guide_id.'"  disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_guide_'.$guide->guide_id.'"  disabled="disabled">InActive</button>';
                             
                            $html.='</td>';
                             }
                      
                            }
                            if($rights['edit_delete']==1 || $rights['view']==1)
                            {
                         $html.='<td>';
                            if(strpos($rights['admin_which'],'view')!==false)
                            $html.='<a href="'.route('admin-guide-details',['guide_id'=>$guide->guide_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($guide->guide_created_by!=Session::get('travel_users_id') && $guide->guide_role!="Supplier" && strpos($rights['admin_which'],'view')!==false)
                            $html.='<a href="'.route('admin-guide-details',['guide_id'=>$guide->guide_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($guide->guide_created_by==Session::get('travel_users_id') && $guide->guide_role!="Supplier" && $rights['view']==1)
                            $html.='<a href="'.route('admin-guide-details',['guide_id'=>$guide->guide_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            

                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            $html.='<a href="'.route('admin-edit-guide',['guide_id'=>$guide->guide_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($guide->guide_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            $html.='<a href="'.route('admin-edit-guide',['guide_id'=>$guide->guide_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($guide->guide_created_by==Session::get('travel_users_id') && $guide->guide_role!="Supplier" && $rights['edit_delete']==1)
                            $html.='<a href="'.route('admin-edit-guide',['guide_id'=>$guide->guide_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                             
                          $html.='</td>';
                          }

                        $html.='</tr>';
                            $srno++;

                      }

                      $html.='</tbody>
                    </table>
                                        </div>
                  </div>

                </div>

                <!-- /.box-body -->

              </div>

            </div>';
               }


              $html.='</div>';


    }
    else if($booking_type=="driver")
    {
      if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
    {
      $get_drivers=Drivers::orderBy(\DB::raw('-`driver_show_order`'), 'desc')->orderBy('driver_id','asc')->get();
    }
    else
    {
      $get_drivers=Drivers::where('driver_created_by',$emp_id)->where('driver_role',"!=","Supplier")->orderBy(\DB::raw('-`driver_show_order`'), 'desc')->orderBy('driver_id','asc')->get();
    }


    $html.='<div class="row">';
  
              if($rights['add']==1)
              {

                 $html.='<div class="col-sm-6 col-md-3">
                  <div class="form-group">

                    <a href="'.route('create-driver').'"><button type="button"
                        class="btn btn-rounded btn-success">Create New Driver</button></a>
                  </div>
                </div>';
              }
              if($rights['view']==1)
              { 
                 $html.='<div class="col-12">

                  <div class="box">

                <!-- /.box-header -->

                <div class="box-body" style="padding:0">

                  <div class="table-responsive">

                                        <div class="row">

                                        <table id="driver_table" class="table table-bordered table-striped">

                      <thead>

                        <tr>
                          <th>S. No.</th>
                          <th style="display:none">Driver ID</th>

                          <th>Name</th>

                          <th>Contact</th>

                          <th>Address</th>

                          <th>Supplier</th>

                          <th>City</th>

                          <th>Country</th>';
                        
                          if($rights['edit_delete']==1)
                          {
                           $html.='<th>Status</th>
                          <th>Approval</th>
                          <th>Best Seller</th>';
                          }
                          if($rights['edit_delete']==1 || $rights['view']==1)
                          {
                             $html.='<th>Action</th>';
                          }
                        

                        $html.='</tr>

                      </thead>

                      <tbody>';
                        $srno=1;
                        foreach($get_drivers as $driver)
                        {
                        $html.='<tr id="tr_'.$driver->driver_id.'">

                          <td style="cursor: all-scroll;">'.$srno.'</td>
                          <td style="display:none">'.$driver->driver_id.'</td>

                          <td>'.$driver->driver_first_name.' '.$driver->driver_last_name.'</td>

                          <td>'.$driver->driver_contact.'</td>

                          <td>'.$driver->driver_address.'</td>

                          <td>'.$driver->getSupplier->supplier_name.'</td>

                          <td>'.$driver->getCity->name.'</td>

                          <td>'.$driver->getCountry->country_name.'</td>';
                            if($rights['edit_delete']==1)
                            {
                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                            $html.='<td>';
                              if($driver->driver_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_driver_'.$driver->driver_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_driver_'.$driver->driver_id.'">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($driver->driver_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($driver->driver_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_driver_'.$driver->driver_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_driver_'.$driver->driver_id.'">Reject</button>';
                              else if($driver->driver_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             
                           $html.=' </td>
                            <td>';
                              
                              if($driver->driver_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_driver_'.$driver->driver_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_driver_'.$driver->driver_id.'" >InActive</button>';
                              
                            $html.='</td>';
                          }
                            else if($driver->driver_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                            $html.='<td>';
                              if($driver->driver_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_driver_'.$driver->driver_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_driver_'.$driver->driver_id.'">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($driver->driver_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($driver->driver_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_driver_'.$driver->driver_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_driver_'.$driver->driver_id.'">Reject</button>';
                              else if($driver->driver_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             
                           $html.=' </td>
                            <td>';
                              
                              if($driver->driver_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_driver_'.$driver->driver_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_driver_'.$driver->driver_id.'" >InActive</button>';
                              
                            $html.='</td>';
                          }
                            else if($driver->driver_created_by==Session::get('travel_users_id') && $driver->driver_role!="Supplier" && $rights['edit_delete']==1)
                            {
                            $html.='<td>';
                              if($driver->driver_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_driver_'.$driver->driver_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_driver_'.$driver->driver_id.'">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($driver->driver_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($driver->driver_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_driver_'.$driver->driver_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_driver_'.$driver->driver_id.'">Reject</button>';
                              else if($driver->driver_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             
                           $html.=' </td>
                            <td>';
                              
                              if($driver->driver_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_driver_'.$driver->driver_id.'" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_driver_'.$driver->driver_id.'" >InActive</button>';
                              
                            $html.='</td>';
                          }
                            else
                            {
                            $html.='<td>';
                              if($driver->driver_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_driver_'.$driver->driver_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_driver_'.$driver->driver_id.'" disabled="disabled">InActive</button>';
                             
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($driver->driver_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($driver->driver_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_driver_'.$driver->driver_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_driver_'.$driver->driver_id.'">Reject</button>';
                              else if($driver->driver_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             
                           $html.=' </td>
                            <td>';
                              
                              if($driver->driver_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_driver_'.$driver->driver_id.'" disabled="disabled" >Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_driver_'.$driver->driver_id.'" disabled="disabled" >InActive</button>';
                              
                            $html.='</td>';
                          }
                      
                            }

                            if($rights['edit_delete']==1 || $rights['view']==1)
                            {
                          $html.='<td>';
                            if(strpos($rights['admin_which'],'view')!==false)
                            $html.='<a href="'.route('driver-details',['driver_id'=>$driver->driver_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($driver->driver_created_by!=Session::get('travel_users_id') && $driver->driver_role!="Supplier" && strpos($rights['admin_which'],'view')!==false)
                            $html.='<a href="'.route('driver-details',['driver_id'=>$driver->driver_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            elseif($driver->driver_created_by==Session::get('travel_users_id') && $driver->driver_role!="Supplier" && $rights['view']==1)
                            $html.='<a href="'.route('driver-details',['driver_id'=>$driver->driver_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                          

                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                           $html.=' <a href="'.route('edit-driver',['driver_id'=>$driver->driver_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($driver->driver_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                           $html.=' <a href="'.route('edit-driver',['driver_id'=>$driver->driver_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($driver->driver_created_by==Session::get('travel_users_id') && $driver->driver_role!="Supplier" && $rights['edit_delete']==1)
                           $html.=' <a href="'.route('edit-driver',['driver_id'=>$driver->driver_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            
                          $html.='</td>';
                          }

                        $html.='</tr>';
                      
                        $srno++;

                        }

                     $html.=' </tbody>



                    </table>

                                        </div>

                  

                  </div>

                </div>

                <!-- /.box-body -->

              </div>

            </div>';
               }


             $html.=' </div>';

    }
    else if($booking_type=="transfer")
    {
      if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
    {
      $get_transfer=Transfers::orderBy(\DB::raw('-`transfer_show_order`'), 'desc')->orderBy('transfer_id','asc')->get();
    }
    else
    {
      $get_transfer=Transfers::where('transfer_created_by',$emp_id)->where('transfer_role','!=','Supplier')->orderBy(\DB::raw('-`transfer_show_order`'), 'desc')->orderBy('transfer_id','asc')->get();
    }

    $html.=' <div class="row">';
                if($rights['add']==1)
                {
                $html.='<div class="col-sm-6 col-md-3">
                  <div class="form-group">

                    <a href="'.route('create-transfer').'"><button type="button"
                        class="btn btn-rounded btn-success">Create New Transfer</button></a>
                  </div>
                </div>';
                }
                if($rights['view']==1) 
                {
               $html.=' <div class="col-12">
                  <div class="box">

                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="table-responsive">
                        <table id="transfer_table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S. No.</th>
                              <th style="display:none">Transfer ID</th>
                              <th>Transfer Name</th>
                              <th>Supplier</th>
                              <th>Description</th>
                              <th>Transfer Type</th>
                              <th>No. of Transfers</th>
                              <th>Country</th>';
                              if($rights['edit_delete']==1)
                              {
                          $html.='<th>Status</th>
                              <th>Approval</th>
                              <th>Best Seller</th>';
                              }
                              if($rights['edit_delete']==1 || $rights['view']==1)
                              {
                               $html.='<th>Action</th>';
                              }

                            $html.='</tr>
                          </thead>
                          <tbody>';
                        
                            $srno=1;
                        
                            foreach($get_transfer as $transfer)
                            {
                        $html.='<tr id="tr_'.$transfer->transfer_id.'">

                          <td style="cursor: all-scroll;">'.$srno.'</td>
                          <td style="display:none">'.$transfer->transfer_id.'</td>
                          <td>'.$transfer->transfer_name.'</td>
                          <td>'.$transfer->getSupplier->supplier_name.'</td>
                          <td>'.$transfer->transfer_description.'</td>
                          <td>';
                            if($transfer->transfer_type=="from-airport")
                             $html.='From Airport to City Transfer';
                            else if($transfer->transfer_type=="to-airport")
                            $html.='From City to Airport Transfer';
                            else
                            $html.='City Transfer';
                            
                            $html.='</td>
                          <td>'.$transfer->no_of_transfer_available.'</td>
                          <td>'.$transfer->country->country_name.'</td>';
                            if($rights['edit_delete']==1)
                            {
                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                              $html.='<td>';
                              if($transfer->transfer_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_transfer_'.$transfer->transfer_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_transfer_'.$transfer->transfer_id.'">InActive</button>';
                              
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($transfer->transfer_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($transfer->transfer_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_transfer_'.$transfer->transfer_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_transfer_'.$transfer->transfer_id.'">Reject</button>';
                              else if($transfer->transfer_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             


                              $html.='</td>
                            <td>';
                              if($transfer->transfer_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_transfer_'.$transfer->transfer_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_transfer_'.$transfer->transfer_id.'">InActive</button>';
                              
                            $html.='</td>';
                          }
                            else if($transfer->transfer_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                            {
                               $html.='<td>';

                              if($transfer->transfer_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_transfer_'.$transfer->transfer_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_transfer_'.$transfer->transfer_id.'">InActive</button>';
                              
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($transfer->transfer_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($transfer->transfer_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_transfer_'.$transfer->transfer_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_transfer_'.$transfer->transfer_id.'">Reject</button>';
                              else if($transfer->transfer_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             


                              $html.='</td>
                            <td>';
                              if($transfer->transfer_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_transfer_'.$transfer->transfer_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_transfer_'.$transfer->transfer_id.'">InActive</button>';
                              
                            $html.='</td>';
                          }
                            
                            elseif($transfer->transfer_created_by==Session::get('travel_users_id') && $transfer->transfer_create_role!="Supplier" && $rights['edit_delete']==1)
                            {
                            $html.='<td>';
                              if($transfer->transfer_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_transfer_'.$transfer->transfer_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_transfer_'.$transfer->transfer_id.'">InActive</button>';
                              
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($transfer->transfer_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($transfer->transfer_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_transfer_'.$transfer->transfer_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_transfer_'.$transfer->transfer_id.'">Reject</button>';
                              else if($transfer->transfer_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             


                              $html.='</td>
                            <td>';
                              if($transfer->transfer_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_transfer_'.$transfer->transfer_id.'">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_transfer_'.$transfer->transfer_id.'">InActive</button>';
                              
                            $html.='</td>';
                          }
                            else
                            {
                             $html.='<td>';
                              if($transfer->transfer_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_transfer_'.$transfer->transfer_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_transfer_'.$transfer->transfer_id.'" disabled="disabled">InActive</button>';
                              
                            $html.='</td>
                            <td style="white-space: nowrap;">';
                              if($transfer->transfer_approve_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';
                              else if($transfer->transfer_approve_status==0)
                              $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_transfer_'.$transfer->transfer_id.'">Approve</button>
                              <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_transfer_'.$transfer->transfer_id.'">Reject</button>';
                              else if($transfer->transfer_approve_status==2)
                              $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';
                             


                              $html.='</td>
                            <td>';
                              if($transfer->transfer_best_status==1)
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_transfer_'.$transfer->transfer_id.'" disabled="disabled">Active</button>';
                              else
                              $html.='<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_transfer_'.$transfer->transfer_id.'" disabled="disabled">InActive</button>';
                              
                            $html.='</td>';
                          }

                            
                            }

                            if($rights['edit_delete']==1 || $rights['view']==1)
                            {
                          $html.='<td>';
                            if(strpos($rights['admin_which'],'view')!==false)
                              $html.='<a href="'.route('transfer-details',['transfer_id'=>$transfer->transfer_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($transfer->transfer_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'view')!==false)
                              $html.='<a href="'.route('transfer-details',['transfer_id'=>$transfer->transfer_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                            else if($transfer->transfer_created_by==Session::get('travel_users_id') && $transfer->transfer_create_role!="Supplier" && $rights['view']==1)
                              $html.='<a href="'.route('transfer-details',['transfer_id'=>$transfer->transfer_id]).'" target="_blank"><i class="fa fa-eye"></i></a>';
                           

                            if(strpos($rights['admin_which'],'edit_delete')!==false)
                              $html.='<a href="'.route('edit-transfer',['transfer_id'=>$transfer->transfer_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($transfer->transfer_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
                              $html.='<a href="'.route('edit-transfer',['transfer_id'=>$transfer->transfer_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                            else if($transfer->transfer_created_by==Session::get('travel_users_id') && $transfer->transfer_create_role!="Supplier" && $rights['edit_delete']==1)
                            $html.='<a href="'.route('edit-transfer',['transfer_id'=>$transfer->transfer_id]).'" target="_blank"><i class="fa fa-pencil"></i></a>';
                          


                          $html.='</td>';
                          }

                        $html.='</tr>';
                         $srno++;
                        }
                          $html.='</tbody>
                        </table>
                      </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>';
                }


              $html.='</div>';

    }


    echo $html;

}

public static function searchCities($cityid,$countryid)
{
  $city_id=$cityid;
  $country_id=$countryid;
  $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $country_id)->where('cities.id',$city_id)->select("cities.*")->orderBy('cities.name','asc')->first();
  return $fetch_cities;
}
public static function searchSightseeingTour(Request $request)
{
  $city_id=$request->get('city_id');
  $country_id=$request->get('country_id');
  $src=$request->get('src');
  $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
  // $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function($query) use($city_id){
  //   $query->where('sightseeing_city_from',$city_id)->orWhereRaw('FIND_IN_SET(?,sightseeing_city_between)', [$city_id])->orWhere('sightseeing_city_to',$city_id);
  // })->where('sightseeing_status',1)->get();
  $cities=Cities::where('id',$city_id)->first();
if($cities->name=="Tbilisi")
{
 $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where('sightseeing_status',1)->get();
}
else if($cities->name=="Kutaisi" || $cities->name=="Batumi")
{
   $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function($query) use($city_id){
    $query->where('sightseeing_city_from',$city_id)->OrWhere('sightseeing_city_to',$city_id);
  })->where('sightseeing_status',1)->get();
  
}
else
{
   $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function($query) use($city_id){
    $query->where('sightseeing_city_from',$city_id)->where('sightseeing_city_to',$city_id);
  })->where('sightseeing_status',1)->get();
}
 

  $html="<div class='col-md-12'>
  <div class='row'>
  <div class='col-md-4'>
  <p >SightSeeing Tours</p>
  </div>
  <div class='col-md-5'>
  <div class='row'>
  <div class='col-md-8'>
  <p>Vehicle Type</p>
  </div>
  <div class='col-md-4'>
  <p>".ucfirst($src)." Cost</p>
  </div>
  </div>


  </div>
  </div>";
  foreach($fetch_sightseeing as $sightseeing)
  {
    $html.="<div class='row'>
    <div class='col-md-4'>
    <input type='hidden' name='tour_name[]' value='".$sightseeing->sightseeing_id."'>
    <input type='text' class='form-control' value='".$sightseeing->sightseeing_tour_name."' readonly='readonly'>
    </div>
    <div class='col-md-5'>
    ";
    foreach($fetch_vehicle_type as $vehcile_type)
    {
      $html.=" <div class='row'>
      <div class='col-md-8'><input type='hidden' name='tour_vehiclename[".($sightseeing->sightseeing_id-1)."][]' value='".$vehcile_type->vehicle_type_id."'>
      <input type='text' class='form-control' value='".$vehcile_type->vehicle_type_name."' readonly='readonly'>  </div>  <div class='col-md-4'>
      <input type='text' class='form-control' name='tour_guide_cost[".($sightseeing->sightseeing_id-1)."][]' value='0' onkeypress='javascript:return validateNumber(event)'  onpaste='javascript:return validateNumber(event)'> </div>
      </div>";
    }
    $html.="
    <br></div>
    </div>
    ";
  }
  if(count($fetch_sightseeing)<=0)
  {
    $html.="<div class='row'><p class='text-center'><b>No Tours Available</b></p></div>";
  }
  $html.="</div>";
  echo $html;
}

public function searchAirportsCost(Request $request)
{

  $city_id=$request->get('city_id');
  $country_id=$request->get('country_id');
  $src=$request->get('src');
  $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
$fetch_airports=AirportMaster::where('airport_master_status',1)->get();
 

  $html="<div class='col-md-12'>
  <div class='row'>
  <div class='col-md-4'>
  <p>Airports</p>
  </div>
  <div class='col-md-8'>
  <div class='row'>
  <div class='col-md-5'>
  <p>Vehicle Type</p>
  </div>
  <div class='col-md-3'>
  <p>".ucfirst($src)." Inside City Cost</p>
  </div>
  <div class='col-md-3'>
  <p>".ucfirst($src)." Outside City Cost</p>
  </div>
  </div>


  </div>
  </div>";
  foreach($fetch_airports as $airports)
  {
    $html.="<div class='row'>
    <div class='col-md-4'>
    <input type='hidden' name='airport_name[]' value='".$airports->airport_master_id."'>
    <input type='text' class='form-control' value='".$airports->airport_master_name."' readonly='readonly'>
    </div>
    <div class='col-md-8'>
    ";
    foreach($fetch_vehicle_type as $vehcile_type)
    {
      $html.=" <div class='row'>
      <div class='col-md-5'><input type='hidden' name='airport_vehicle_name[".($airports->airport_master_id-1)."][]' value='".$vehcile_type->vehicle_type_id."'>
      <input type='text' class='form-control' value='".$vehcile_type->vehicle_type_name."' readonly='readonly'>  </div>  <div class='col-md-3'>
      <input type='text' class='form-control' name='airport_guide_inside_cost[".($airports->airport_master_id-1)."][]' value='0' onkeypress='javascript:return validateNumber(event)'  onpaste='javascript:return validateNumber(event)'> </div>
      <div class='col-md-3'>
      <input type='text' class='form-control' name='airport_guide_outside_cost[".($airports->airport_master_id-1)."][]' value='0' onkeypress='javascript:return validateNumber(event)'  onpaste='javascript:return validateNumber(event)'> </div>
      </div>";
    }
    $html.="
    <br></div>
    </div>
    ";
  }
  if(count($fetch_airports)<=0)
  {
    $html.="<div class='row'><p class='text-center'><b>No Airports Available</b></p></div>";
  }
  $html.="</div>";
  echo $html;

}
public static function searchSightseeingTourName($sightseeing_id)
{
  $fetch_sightseeing=SightSeeing::where('sightseeing_id',$sightseeing_id)->first();
  return $fetch_sightseeing;
}
public static function searchTransfers($transfer_id)
{
  $fetch_transfer=Transfers::where('transfer_id',$transfer_id)->first();
  return $fetch_transfer;
}
public static function searchAirports($airport_id)
{
  $fetch_airports=AirportMaster::where('airport_master_id',$airport_id)->first();
  return $fetch_airports;
}
public static function searchFuelCost(Request $request)
{
  $fuel_type_id=$request->get('fuel_type_id');
  $fetch_fuel_type=FuelType::where("fuel_type_id", $fuel_type_id)->first();
  $fuel_cost=$fetch_fuel_type->fuel_type_cost;
  return $fuel_cost;
}
public static function searchActivity($activity_id)
{
  $fetch_activity=Activities::where('activity_id',$activity_id)->first();
  return $fetch_activity;
}
public static function searchHotel($hotel_id)
{
  $fetch_hotel=Hotels::where('hotel_id',$hotel_id)->first();
  return $fetch_hotel;
}
public static function searchGuide($guide_id)
{
  $fetch_guide=Guides::where('guide_id',$guide_id)->first();
  return $fetch_guide;
}
public static function searchDriver($driver_id)
{
  $fetch_driver=Drivers::where('driver_id',$driver_id)->first();
  return $fetch_driver;
}
public static function searchItinerary($itinerary_id)
{
  $fetch_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->first();
  return $fetch_itinerary;
}
public static function searchRestaurant($restaurant_id)
{
  $fetch_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->first();
  return $fetch_restaurant;
}
public static function searchSupplier($supplier_id)
{
  $fetch_supplier=Suppliers::where('supplier_id',$supplier_id)->first();
  return $fetch_supplier;
}
public static function searchAgent($agent_id)
{
  $fetch_agent=Agents::where('agent_id',$agent_id)->first();
  return $fetch_agent;
}
public static function searchUser($user_id)
{
  $fetch_user=Users::where('users_id',$user_id)->first();
  return $fetch_user;
}
public function create_activity(Request $request)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $fetch_activity_type=ActivityType::where('activity_type_status',1)->get();
    return view('mains.create-activity')->with(compact('countries','currency','suppliers','fetch_activity_type','rights'));
  }
  else
  {
    return redirect()->route('index');
  }
}
public function search_supplier_country(Request $request)
{
  $supplier_id=$request->get('supplier_id');
  $countries=Countries::where('country_status',1)->get();
  $get_supplier=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
  if(!empty($get_supplier))
  {
    $countrydata="<option value='0' hidden>SELECT COUNTRY</option>";
    $supplier_countries=$get_supplier->supplier_opr_countries;
    $supplier_countries_array=explode(',',$supplier_countries);
    for($count=0;$count<count($supplier_countries_array);$count++)
    {
      foreach($countries as $country)
      {
        if($country->country_id==$supplier_countries_array[$count])
        {
          $countrydata.="<option value='".$country->country_id."'>".$country->country_name."</option>";
        }
      }
    }
    echo $countrydata;
  }
  else
  {
    echo "fail";
  }
}
public function search_country_cities(Request $request)
{
  $country_id=$request->get('country_id');
  $get_countries=Countries::where('country_id',$country_id)->first();
  if(!empty($get_countries))
  {
    $citydata="<option value='0' hidden>SELECT CITY</option>";
    $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->select("cities.*")->orderBy('cities.name','asc')->get();
    foreach($fetch_cities as $cities)
    {
      $citydata.="<option value='".$cities->id."'>".$cities->name."</option>";
    }
    echo $citydata;
  }
  else
  {
  }
}
public function search_country_itinerary_cities(Request $request)
{
  $country_id=$request->get('country_id');
  $get_countries=Countries::where('country_id',$country_id)->first();
  if(!empty($get_countries))
  {
    $citydata="<option value='0' hidden>SELECT CITY</option>";
    $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->select("cities.*");
    if($get_countries->country_id=="81" || $get_countries->country_name=="Georgia")
    {
 $fetch_cities=$fetch_cities->whereIn('cities.name',["Tbilisi","Batumi","Kutaisi"]);
    }
    $fetch_cities=$fetch_cities->orderBy('cities.name','asc')->get();
    foreach($fetch_cities as $cities)
    {
      $citydata.="<option value='".$cities->id."'>".$cities->name."</option>";
    }
    echo $citydata;
  }
  else
  {
  }
}
public static function search_country_cities_array($country_id,$city_id)
{
  $country_id=$country_id;
  $get_countries=Countries::where('country_id',$country_id)->first();
  if(!empty($get_countries))
  {
    $citydata="<option value='0' hidden>SELECT CITY</option>";
    $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->select("cities.*")->orderBy('cities.name','asc')->get();
    return $fetch_cities;
  }
  else
  {
    $fetch_cities=array();
    return $fetch_cities;
  }
}
public function fetch_services(Request $request)
{
  $supplier_id=$request->get('supplier');
  $country_id=$request->get('country');
  $city_id=$request->get('city');
  $services_type=$request->get('services_type');
  $service_data="<option value='0' selected >SELECT SERVICE</option>";
  if($services_type=="activity")
  {
    $fetch_activity=Activities::where('activity_country',$country_id)->where('activity_city',$city_id)->where("supplier_id",$supplier_id)->where("activity_status",1)->get();
    foreach($fetch_activity as $activites)
    {
      $service_data.="<option value='$activites->activity_id'>".$activites->activity_name."</option>";
    }
  }
  else if($services_type=="hotel")
  {
    $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id)->where("supplier_id",$supplier_id)->where("hotel_status",1)->get();
    foreach($fetch_hotels as $hotels)
    {
      $service_data.="<option value='$hotels->hotel_id'>".$hotels->hotel_name."</option>";
    }
  }
  else if($services_type=="transportation")
  {
    $fetch_transportation=Transport::where('transfer_country',$country_id)->where('transfer_city',$city_id)->where("supplier_id",$supplier_id)->where("transfer_status",1)->get();
    foreach($fetch_transportation as $transport)
    {
      $service_data.="<option value='$transport->transport_id'>".$transport->transfer_name."</option>";
    }
  }
  echo $service_data;
}
public static function fetch_services_array($supplier,$country,$city,$services_type)
{
  $supplier_id=$supplier;
  $country_id=$country;
  $city_id=$city;
  $services_type=$services_type;
  $service_data="<option value='0' selected >SELECT SERVICE</option>";
  if($services_type=="activity")
  {
    $fetch_activity=Activities::where('activity_country',$country_id)->where('activity_city',$city_id)->where("supplier_id",$supplier_id)->where("activity_status",1)->get();
    return $fetch_activity;
  }
  else if($services_type=="hotel")
  {
    $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id)->where("supplier_id",$supplier_id)->where("hotel_status",1)->get();
    return $fetch_hotels;
  }
  else if($services_type=="transportation")
  {
    $fetch_transportation=Transport::where('transfer_country',$country_id)->where('transfer_city',$city_id)->where("supplier_id",$supplier_id)->where("transfer_status",1)->get();
    return $fetch_transportation;
  }
}
public function insert_activity(Request $request)
{
    // echo "<pre>";
    // print_r($request->all());
    // die();
  $activity_role=$request->get('activity_role');
  if($activity_role=='supplier')
  {
    $user_id=$request->get('supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $activity_type=$request->get('activity_type');
  $activity_name=$request->get('activity_name');
  $supplier_id=$request->get('supplier_name');
  $activity_location=$request->get('activity_location');
  $activity_country=$request->get('activity_country');
  $activity_city=$request->get('activity_city');
  $activity_duration=$request->get('activity_duration');
  $operation_period_fromdate=$request->get('period_operation_from');
  $operation_period_todate=$request->get('period_operation_to');
  $validity_fromdate=$request->get('validity_operation_from');
  $validity_todate=$request->get('validity_operation_to');
  $validity_fromtime=$request->get('activity_time_from');
  $validity_totime=$request->get('activity_time_to');
  $week_monday=$request->get('week_monday');
  $week_tuesday=$request->get('week_tuesday');
  $week_wednesday=$request->get('week_wednesday');
  $week_thursday=$request->get('week_thursday');
  $week_friday=$request->get('week_friday');
  $week_saturday=$request->get('week_saturday');
  $week_sunday=$request->get('week_sunday');
  $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);
  $operating_weekdays=serialize($operating_weekdays);
  $activity_currency=$request->get('activity_currency');
  $adult_price=$request->get('activity_adult_cost');
  $child_price=$request->get('activity_child_cost');
  $for_all_ages=$request->get('for_all_ages');
  $child_allowed=$request->get('child_allowed');
  $child_age=$request->get('child_age');
  $adult_allowed=$request->get('adult_allowed');
  $adult_age=$request->get('adult_age');
  $child_adult_age_details=array();
  $child_adult_age_details[0][]="child";
  $child_adult_age_details[0][]=$child_allowed;
  $child_adult_age_details[0][]=$child_age;
  $child_adult_age_details[1][]="adult";
  $child_adult_age_details[1][]=$adult_allowed;
  $child_adult_age_details[1][]=$adult_age;

  $child_adult_age_details=serialize($child_adult_age_details);
  $activity_blackout_dates=$request->get('blackout_days');
  $activity_nationality=$request->get('activity_nationality');
  $activity_markup=$request->get('activity_markup');
  $activity_amount=$request->get('activity_amount');
  $nationality_markup_details=array();
  for($nation_count=0;$nation_count<count($activity_nationality);$nation_count++)
  {
    $nationality_markup_details[$nation_count]['activity_nationality']=$activity_nationality[$nation_count];
    $nationality_markup_details[$nation_count]['activity_markup']=$activity_markup[$nation_count];
    $nationality_markup_details[$nation_count]['activity_amount']=$activity_amount[$nation_count];
  }
  $nationality_markup_details=serialize($nationality_markup_details);
  $activity_transport_currency=$request->get('activity_transport_currency');
  $activity_transport_desc=$request->get('activity_transport_desc');
  $activity_transport_cost=$request->get('activity_transport_cost');
  $activity_transport_pricing=array();
  for($transport_count=0;$transport_count<count($activity_transport_currency);$transport_count++)
  {
    $activity_transport_pricing[$transport_count]['transport_currency']=$activity_transport_currency[$transport_count];
    $activity_transport_pricing[$transport_count]['transport_desc']=$activity_transport_desc[$transport_count];
    $activity_transport_pricing[$transport_count]['transport_cost']=$activity_transport_cost[$transport_count];
  }

  $activity_transport_pricing=serialize($activity_transport_pricing);

  $adults_age=$request->get('adults_age');
  $adults_min_age=$request->get('adults_min_age');
  $adults_max_age=$request->get('adults_max_age');

  $children_age=$request->get('children_age');
  $child_min_age=$request->get('child_min_age');
  $child_max_age=$request->get('child_max_age');

  $infant_age=$request->get('infant_age');
  $infant_min_age=$request->get('infant_min_age');
  $infant_max_age=$request->get('infant_max_age');

  $age_group_details=array();

  if($request->has('adults_age'))
    $age_group_details['adults']['allowed']="yes";
  else
    $age_group_details['adults']['allowed']="no";

  $age_group_details['adults']['min_age']=$adults_min_age;
  $age_group_details['adults']['max_age']=$adults_max_age;

  if($request->has('children_age'))
    $age_group_details['child']['allowed']="yes";
  else
    $age_group_details['child']['allowed']="no";

  $age_group_details['child']['min_age']=$child_min_age;
  $age_group_details['child']['max_age']=$child_max_age;

  if($request->has('infant_age'))
    $age_group_details['infant']['allowed']="yes";
  else
    $age_group_details['infant']['allowed']="no";

  $age_group_details['infant']['min_age']=$infant_min_age;
  $age_group_details['infant']['max_age']=$infant_max_age;

    //age group array serialize
  $age_group_details=serialize($age_group_details);


  $adult_min_pax=$request->get('adult_min_pax');
  $adult_max_pax=$request->get('adult_max_pax');
  $adult_pax_price=$request->get('adult_pax_price');

  $adult_price_details=array();

  for($adultpax_count=0;$adultpax_count<count($adult_min_pax);$adultpax_count++)
  {
    $adult_price_details[$adultpax_count]['adult_min_pax']=$adult_min_pax[$adultpax_count];
    $adult_price_details[$adultpax_count]['adult_max_pax']=$adult_max_pax[$adultpax_count];
    $adult_price_details[$adultpax_count]['adult_pax_price']=$adult_pax_price[$adultpax_count];
  }
  $adult_price_details=serialize($adult_price_details);



  $infant_min_pax=$request->get('infant_min_pax');
  $infant_max_pax=$request->get('infant_max_pax');
  $infant_pax_price=$request->get('infant_pax_price');

  $infant_price_details=array();

  for($infantpax_count=0;$infantpax_count<count($infant_min_pax);$infantpax_count++)
  {
    $infant_price_details[$infantpax_count]['infant_min_pax']=$infant_min_pax[$infantpax_count];
    $infant_price_details[$infantpax_count]['infant_max_pax']=$infant_max_pax[$infantpax_count];
    $infant_price_details[$infantpax_count]['infant_pax_price']=$infant_pax_price[$infantpax_count];
  }
  $infant_price_details=serialize($infant_price_details);

  $child_min_pax=$request->get('child_min_pax');
  $child_max_pax=$request->get('child_max_pax');
  $child_pax_price=$request->get('child_pax_price');


  $child_price_details=array();

  for($childpax_count=0;$childpax_count<count($child_min_pax);$childpax_count++)
  {
    $child_price_details[$childpax_count]['child_min_pax']=$child_min_pax[$childpax_count];
    $child_price_details[$childpax_count]['child_max_pax']=$child_max_pax[$childpax_count];
    $child_price_details[$childpax_count]['child_pax_price']=$child_pax_price[$childpax_count];
  }
  $child_price_details=serialize($child_price_details);


  $availability_from=$request->get('availability_from');
  $availability_to=$request->get('availability_to');
  $availability_time_from=$request->get('availability_time_from');
  $availability_time_to=$request->get('availability_time_to');
  $no_of_bookings=$request->get('no_of_bookings');

  $availability_qty_details=array();
  for($avail_count=0;$avail_count<count($availability_from);$avail_count++)
  {
    $availability_qty_details[$avail_count]['availability_from']=$availability_from[$avail_count];
    $availability_qty_details[$avail_count]['availability_to']=$availability_to[$avail_count];
    $availability_qty_details[$avail_count]['availability_time_from']=$availability_time_from[$avail_count];
    $availability_qty_details[$avail_count]['availability_time_to']=$availability_time_to[$avail_count];
    $availability_qty_details[$avail_count]['availability_no_of_bookings']=$no_of_bookings[$avail_count];
  }

  $availability_qty_details=serialize($availability_qty_details);

  
  $activity_inclusions=$request->get('activity_inclusions');
  $activity_exclusions=$request->get('activity_exclusions');
  $activity_description=$request->get('activity_description');
  $activity_cancel_policy=$request->get('activity_cancellation');
  $activity_terms_conditions=$request->get('activity_terms_conditions');
  $activity_created_by=$user_id;
  $activity_images=array();
//multifile uploading
  if($request->hasFile('upload_ativity_images'))
  {
    foreach($request->file('upload_ativity_images') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_activity = "activity-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/activities_images/';
        $file->move($dir1, $image_activity);
        $activity_images[]=$image_activity;
      }
    }
  }
  $activity_images=serialize($activity_images);
  $insert_activity=new Activities;
  $insert_activity->activity_name=$activity_name;
  $insert_activity->activity_type=$activity_type;
  $insert_activity->supplier_id=$supplier_id;
  $insert_activity->activity_location=$activity_location;
  $insert_activity->activity_country=$activity_country;
  $insert_activity->activity_city=$activity_city;
  $insert_activity->activity_duration=$activity_duration;
  $insert_activity->operation_period_fromdate=$operation_period_fromdate;
  $insert_activity->operation_period_todate=$operation_period_todate;
  $insert_activity->validity_fromdate=$validity_fromdate;
  $insert_activity->validity_todate=$validity_todate;
  $insert_activity->validity_fromtime=$validity_fromtime;
  $insert_activity->validity_totime=$validity_totime;
  $insert_activity->operating_weekdays=$operating_weekdays;
  $insert_activity->availability_qty_details=$availability_qty_details;
  $insert_activity->age_group_details=$age_group_details;
  $insert_activity->adult_price_details=$adult_price_details;
  $insert_activity->child_price_details=$child_price_details;
  $insert_activity->infant_price_details=$infant_price_details;
  $insert_activity->activity_currency=$activity_currency;
  $insert_activity->adult_price=$adult_price;
  $insert_activity->child_price=$child_price;
  $insert_activity->activity_blackout_dates=$activity_blackout_dates;
  $insert_activity->nationality_markup_details=$nationality_markup_details;
  $insert_activity->activity_transport_pricing=$activity_transport_pricing;
  $insert_activity->for_all_ages=$for_all_ages;
  $insert_activity->child_adult_age_details=$child_adult_age_details;
  $insert_activity->activity_inclusions=$activity_inclusions;
  $insert_activity->activity_exclusions=$activity_exclusions;
  $insert_activity->activity_description=$activity_description;
  $insert_activity->activity_cancel_policy=$activity_cancel_policy;
  $insert_activity->activity_terms_conditions=$activity_terms_conditions;
  $insert_activity->activity_images=$activity_images;
  $insert_activity->activity_created_by=$activity_created_by;
  $insert_activity->activity_role=$user_role;
  if($user_role=="Supplier")
  {
    $insert_activity->activity_approve_status=0;
  }
  if($insert_activity->save())
  {

    $last_id=$insert_activity->id;
    $insert_activity_log=new Activities_log;
    $insert_activity_log->activity_id=$last_id;
    $insert_activity_log->activity_name=$activity_name;
    $insert_activity_log->activity_type=$activity_type;
    $insert_activity_log->supplier_id=$supplier_id;
    $insert_activity_log->activity_location=$activity_location;
    $insert_activity_log->activity_country=$activity_country;
    $insert_activity_log->activity_city=$activity_city;
    $insert_activity_log->activity_duration=$activity_duration;
    $insert_activity_log->operation_period_fromdate=$operation_period_fromdate;
    $insert_activity_log->operation_period_todate=$operation_period_todate;
    $insert_activity_log->validity_fromdate=$validity_fromdate;
    $insert_activity_log->validity_todate=$validity_todate;
    $insert_activity_log->validity_fromtime=$validity_fromtime;
    $insert_activity_log->validity_totime=$validity_totime;
    $insert_activity_log->operating_weekdays=$operating_weekdays;
    $insert_activity_log->availability_qty_details=$availability_qty_details;
    $insert_activity_log->activity_currency=$activity_currency;
    $insert_activity_log->adult_price=$adult_price;
    $insert_activity_log->child_price=$child_price;
    $insert_activity_log->activity_blackout_dates=$activity_blackout_dates;
    $insert_activity_log->nationality_markup_details=$nationality_markup_details;
    $insert_activity_log->activity_transport_pricing=$activity_transport_pricing;
    $insert_activity_log->for_all_ages=$for_all_ages;
    $insert_activity_log->child_adult_age_details=$child_adult_age_details;
    $insert_activity_log->activity_inclusions=$activity_inclusions;
    $insert_activity_log->activity_exclusions=$activity_exclusions;
    $insert_activity_log->activity_description=$activity_description;
    $insert_activity_log->activity_cancel_policy=$activity_cancel_policy;
    $insert_activity_log->activity_terms_conditions=$activity_terms_conditions;
    $insert_activity_log->activity_images=$activity_images;
    $insert_activity_log->activity_created_by=$activity_created_by;
    $insert_activity_log->activity_role=$user_role;
    $insert_activity_log->activity_operation_performed="INSERT";
    $insert_activity_log->save();
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
public function edit_activity($activity_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    $fetch_activity_type=ActivityType::where('activity_type_status',1)->get();
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_activity=Activities::where('activity_id',$activity_id)->first();
    }
    else
    {
      $get_activity=Activities::where('activity_id',$activity_id)->where('activity_created_by',$emp_id)->where('activity_role','!=','Supplier')->first();
    }

    if(!empty($get_activity))
    {
      $supplier_id=$get_activity->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_activity->activity_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.edit-activity')->with(compact('countries','currency','cities','suppliers','get_activity','countries_data','fetch_activity_type','rights'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}
public function update_activity(Request $request)
{
  $activity_id=urldecode(base64_decode(base64_decode($request->get('activity_id'))));
  $activity_role=$request->get('activity_role');
  if($activity_role=='supplier')
  {
    $user_id=$request->get('supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $check_activity=Activities::where('activity_id',$activity_id)->get();
  if(count($check_activity)>0)
  {
    $activity_type=$request->get('activity_type');
    $activity_name=$request->get('activity_name');
    $supplier_id=$request->get('supplier_name');
    $activity_location=$request->get('activity_location');
    $activity_country=$request->get('activity_country');
    $activity_city=$request->get('activity_city');
    $activity_duration=$request->get('activity_duration');

    $operation_period_fromdate=$request->get('period_operation_from');
    $operation_period_todate=$request->get('period_operation_to');
    $validity_fromdate=$request->get('validity_operation_from');
    $validity_todate=$request->get('validity_operation_to');
    $validity_fromtime=$request->get('activity_time_from');
    $validity_totime=$request->get('activity_time_to');
    $week_monday=$request->get('week_monday');
    $week_tuesday=$request->get('week_tuesday');
    $week_wednesday=$request->get('week_wednesday');
    $week_thursday=$request->get('week_thursday');
    $week_friday=$request->get('week_friday');
    $week_saturday=$request->get('week_saturday');
    $week_sunday=$request->get('week_sunday');
    $operating_weekdays=array("monday"=>$week_monday,
      "tuesday"=>$week_tuesday,
      "wednesday"=>$week_wednesday,
      "thursday"=>$week_thursday,
      "friday"=>$week_friday,
      "saturday"=>$week_saturday,
      "sunday"=>$week_sunday);
    $operating_weekdays=serialize($operating_weekdays);
    $activity_currency=$request->get('activity_currency');
    $adult_price=$request->get('activity_adult_cost');
    $child_price=$request->get('activity_child_cost');
    $for_all_ages=$request->get('for_all_ages');
    $child_allowed=$request->get('child_allowed');
    $child_age=$request->get('child_age');
    $adult_allowed=$request->get('adult_allowed');
    $adult_age=$request->get('adult_age');
    $child_adult_age_details=array();
    $child_adult_age_details[0][]="child";
    $child_adult_age_details[0][]=$child_allowed;
    $child_adult_age_details[0][]=$child_age;
    $child_adult_age_details[1][]="adult";
    $child_adult_age_details[1][]=$adult_allowed;
    $child_adult_age_details[1][]=$adult_age;

    $child_adult_age_details=serialize($child_adult_age_details);
    $activity_blackout_dates=$request->get('blackout_days');
    $activity_nationality=$request->get('activity_nationality');
    $activity_markup=$request->get('activity_markup');
    $activity_amount=$request->get('activity_amount');
    $nationality_markup_details=array();
    for($nation_count=0;$nation_count<count($activity_nationality);$nation_count++)
    {
      $nationality_markup_details[$nation_count]['activity_nationality']=$activity_nationality[$nation_count];
      $nationality_markup_details[$nation_count]['activity_markup']=$activity_markup[$nation_count];
      $nationality_markup_details[$nation_count]['activity_amount']=$activity_amount[$nation_count];
    }
    $nationality_markup_details=serialize($nationality_markup_details);
    $activity_transport_currency=$request->get('activity_transport_currency');
    $activity_transport_desc=$request->get('activity_transport_desc');
    $activity_transport_cost=$request->get('activity_transport_cost');
    $activity_transport_pricing=array();
    for($transport_count=0;$transport_count<count($activity_transport_currency);$transport_count++)
    {
      $activity_transport_pricing[$transport_count]['transport_currency']=$activity_transport_currency[$transport_count];
      $activity_transport_pricing[$transport_count]['transport_desc']=$activity_transport_desc[$transport_count];
      $activity_transport_pricing[$transport_count]['transport_cost']=$activity_transport_cost[$transport_count];
    }
    $activity_transport_pricing=serialize($activity_transport_pricing);

    $adults_age=$request->get('adults_age');
    $adults_min_age=$request->get('adults_min_age');
    $adults_max_age=$request->get('adults_max_age');

    $children_age=$request->get('children_age');
    $child_min_age=$request->get('child_min_age');
    $child_max_age=$request->get('child_max_age');

    $infant_age=$request->get('infant_age');
    $infant_min_age=$request->get('infant_min_age');
    $infant_max_age=$request->get('infant_max_age');

    $age_group_details=array();

    if($request->has('adults_age'))
      $age_group_details['adults']['allowed']="yes";
    else
      $age_group_details['adults']['allowed']="no";

    $age_group_details['adults']['min_age']=$adults_min_age;
    $age_group_details['adults']['max_age']=$adults_max_age;

    if($request->has('children_age'))
      $age_group_details['child']['allowed']="yes";
    else
      $age_group_details['child']['allowed']="no";

    $age_group_details['child']['min_age']=$child_min_age;
    $age_group_details['child']['max_age']=$child_max_age;

    if($request->has('infant_age'))
      $age_group_details['infant']['allowed']="yes";
    else
      $age_group_details['infant']['allowed']="no";

    $age_group_details['infant']['min_age']=$infant_min_age;
    $age_group_details['infant']['max_age']=$infant_max_age;

    //age group array serialize
    $age_group_details=serialize($age_group_details);


    $adult_min_pax=$request->get('adult_min_pax');
    $adult_max_pax=$request->get('adult_max_pax');
    $adult_pax_price=$request->get('adult_pax_price');

    $adult_price_details=array();

    for($adultpax_count=0;$adultpax_count<count($adult_min_pax);$adultpax_count++)
    {
      $adult_price_details[$adultpax_count]['adult_min_pax']=$adult_min_pax[$adultpax_count];
      $adult_price_details[$adultpax_count]['adult_max_pax']=$adult_max_pax[$adultpax_count];
      $adult_price_details[$adultpax_count]['adult_pax_price']=$adult_pax_price[$adultpax_count];
    }
    $adult_price_details=serialize($adult_price_details);



    $infant_min_pax=$request->get('infant_min_pax');
    $infant_max_pax=$request->get('infant_max_pax');
    $infant_pax_price=$request->get('infant_pax_price');

    $infant_price_details=array();

    for($infantpax_count=0;$infantpax_count<count($infant_min_pax);$infantpax_count++)
    {
      $infant_price_details[$infantpax_count]['infant_min_pax']=$infant_min_pax[$infantpax_count];
      $infant_price_details[$infantpax_count]['infant_max_pax']=$infant_max_pax[$infantpax_count];
      $infant_price_details[$infantpax_count]['infant_pax_price']=$infant_pax_price[$infantpax_count];
    }
    $infant_price_details=serialize($infant_price_details);

    $child_min_pax=$request->get('child_min_pax');
    $child_max_pax=$request->get('child_max_pax');
    $child_pax_price=$request->get('child_pax_price');


    $child_price_details=array();

    for($childpax_count=0;$childpax_count<count($child_min_pax);$childpax_count++)
    {
      $child_price_details[$childpax_count]['child_min_pax']=$child_min_pax[$childpax_count];
      $child_price_details[$childpax_count]['child_max_pax']=$child_max_pax[$childpax_count];
      $child_price_details[$childpax_count]['child_pax_price']=$child_pax_price[$childpax_count];
    }
    $child_price_details=serialize($child_price_details);


    $availability_from=$request->get('availability_from');
  $availability_to=$request->get('availability_to');
  $availability_time_from=$request->get('availability_time_from');
  $availability_time_to=$request->get('availability_time_to');
  $no_of_bookings=$request->get('no_of_bookings');

  $availability_qty_details=array();
  for($avail_count=0;$avail_count<count($availability_from);$avail_count++)
  {
    $availability_qty_details[$avail_count]['availability_from']=$availability_from[$avail_count];
    $availability_qty_details[$avail_count]['availability_to']=$availability_to[$avail_count];
    $availability_qty_details[$avail_count]['availability_time_from']=$availability_time_from[$avail_count];
    $availability_qty_details[$avail_count]['availability_time_to']=$availability_time_to[$avail_count];
    $availability_qty_details[$avail_count]['availability_no_of_bookings']=$no_of_bookings[$avail_count];
  }

    $availability_qty_details=serialize($availability_qty_details);


    $activity_inclusions=$request->get('activity_inclusions');
    $activity_exclusions=$request->get('activity_exclusions');
    $activity_description=$request->get('activity_description');
    $activity_cancel_policy=$request->get('activity_cancellation');
    $activity_terms_conditions=$request->get('activity_terms_conditions');
    $activity_created_by=$user_id;
    if(!empty($request->get('upload_ativity_already_images')))
    {
     $activity_images=$request->get('upload_ativity_already_images');
   }
   else
   {
    $activity_images=array();
  }

//multifile uploading
  if($request->hasFile('upload_ativity_images'))
  {
    foreach($request->file('upload_ativity_images') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_activity = "activity-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/activities_images/';
        $file->move($dir1, $image_activity);
        $activity_images[]=$image_activity;
      }
    }
  }

  $activity_images=serialize($activity_images);
  $update_activity_array=array("activity_type"=>$activity_type,
    "activity_name"=>$activity_name,
    "supplier_id"=>$supplier_id,
    "activity_location"=>$activity_location,
    "activity_country"=>$activity_country,
    "activity_city"=>$activity_city,
    "activity_duration"=>$activity_duration,
    "operation_period_fromdate"=>$operation_period_fromdate,
    "operation_period_todate"=>$operation_period_todate,
    "validity_fromdate"=>$validity_fromdate,
    "validity_todate"=>$validity_todate,
    "validity_fromtime"=>$validity_fromtime,
    "validity_totime"=>$validity_totime,
    "operating_weekdays"=>$operating_weekdays,
    "availability_qty_details"=>$availability_qty_details,
    "age_group_details"=>$age_group_details,
    "adult_price_details"=>$adult_price_details,
    "child_price_details"=>$child_price_details,
    "infant_price_details"=>$infant_price_details,
    "activity_currency"=>$activity_currency,
    "adult_price"=>$adult_price,
    "child_price"=>$child_price,
    "activity_blackout_dates"=>$activity_blackout_dates,
    "nationality_markup_details"=>$nationality_markup_details,
    "activity_transport_pricing"=>$activity_transport_pricing,
    "for_all_ages"=>$for_all_ages,
    "child_adult_age_details"=>$child_adult_age_details,
    "activity_inclusions"=>$activity_inclusions,
    "activity_exclusions"=>$activity_exclusions,
    "activity_description"=>$activity_description,
    "activity_cancel_policy"=>$activity_cancel_policy,
    "activity_terms_conditions"=>$activity_terms_conditions,
    "activity_images"=>$activity_images,
  );
  $update_activity_log_array=array("activity_id"=>$activity_id,
    "activity_name"=>$activity_name,
    "activity_type"=>$activity_type,
    "supplier_id"=>$supplier_id,
    "activity_location"=>$activity_location,
    "activity_country"=>$activity_country,
    "activity_city"=>$activity_city,
    "activity_duration"=>$activity_duration,
    "operation_period_fromdate"=>$operation_period_fromdate,
    "operation_period_todate"=>$operation_period_todate,
    "validity_fromdate"=>$validity_fromdate,
    "validity_todate"=>$validity_todate,
    "validity_fromtime"=>$validity_fromtime,
    "validity_totime"=>$validity_totime,
    "operating_weekdays"=>$operating_weekdays,
    "activity_currency"=>$activity_currency,
    "adult_price"=>$adult_price,
    "child_price"=>$child_price,
    "activity_blackout_dates"=>$activity_blackout_dates,
    "nationality_markup_details"=>$nationality_markup_details,
    "activity_transport_pricing"=>$activity_transport_pricing,
    "for_all_ages"=>$for_all_ages,
    "child_adult_age_details"=>$child_adult_age_details,
    "activity_inclusions"=>$activity_inclusions,
    "activity_exclusions"=>$activity_exclusions,
    "activity_description"=>$activity_description,
    "activity_cancel_policy"=>$activity_cancel_policy,
    "activity_terms_conditions"=>$activity_terms_conditions,
    "activity_images"=>$activity_images,
    "activity_created_by"=>$user_id,
    "activity_role"=>$user_role,
    "activity_operation_performed"=>'UPDATE',
  );
  $update_activity=Activities::where('activity_id',$activity_id)->update($update_activity_array);
  $update_activity=Activities_log::insert($update_activity_log_array);
  if($update_activity)
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function activity_details($activity_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    if(strpos($rights['admin_which'],'view')!==false)
    {
      $get_activity=Activities::where('activity_id',$activity_id)->first();
    }
    else
    {
      $get_activity=Activities::where('activity_id',$activity_id)->where('activity_created_by',$emp_id)->where('activity_role','!=','Supplier')->first();
    }
    if(!empty($get_activity))
    {
      $supplier_id=$get_activity->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_activity->activity_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.activity-details-view')->with(compact('countries','currency','cities','suppliers','get_activity',"countries_data",'rights'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}
public function update_activity_approval(Request $request)
{
  $id=$request->get('activity_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="approve")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_approve_status"=>1]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="reject")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_approve_status"=>2]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }

}
public function update_activity_active_inactive(Request $request)
{
  $id=$request->get('activity_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_status"=>1]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_status"=>0]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}
public function update_activity_bestseller(Request $request)
{
  $id=$request->get('activity_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="bestselleryes")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_best_status"=>1]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="bestsellerno")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_best_status"=>0]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}
public function update_activity_popular(Request $request)
{
  $id=$request->get('activity_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="popularyes")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_popular_status"=>1]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="popularno")
  {
    $update_activity=Activities::where('activity_id',$id)->update(["activity_popular_status"=>0]);
    if($update_activity)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}
public function sort_activity(Request $request)
{
  $sort_activity_array=$request->get('new_data');
  $success_array=array();
  for($count=0;$count<count($sort_activity_array);$count++)
  {
    $activity_id=$sort_activity_array[$count]['activity_id'];
    $new_order=$sort_activity_array[$count]['new_order'];
    $update_activity=Activities::where('activity_id',$activity_id)->update(["activity_show_order"=>$new_order]);
    if($update_activity)
    {
      $success_array[]="success";
    }
    else
    {
      $success_array[]="not_success";
    }
  }
  echo json_encode($success_array);
}
public function create_transport(Request $request)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $vehicle_type=VehicleType::get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    return view('mains.create-transport')->with(compact('countries','currency','suppliers','rights','vehicle_type'));
  }
  else
  {
    return redirect()->route('index');
  }
}
public function insert_transport(Request $request)
{
  $transport_role=$request->get('transport_role');
  if($transport_role=='supplier')
  {
    $user_id=$request->get('supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $transfer_name=$request->get('transfer_name');
  $supplier_id=$request->get('supplier_name');
  $driver_language=$request->get('driver_language');
  $transfer_country=$request->get('transfer_country');
  $transfer_city=$request->get('transfer_city');
  $transfer_arrival_time=$request->get('transfer_arrival_time');
  $transfer_pickup_point=$request->get('transfer_pickup_point');
  $transfer_drop_point=$request->get('transfer_drop_point');
  $transfer_meet_point=$request->get('transfer_meet_point');
  $transfer_description=$request->get('transfer_description');
  $week_monday=$request->get('week_monday');
  $week_tuesday=$request->get('week_tuesday');
  $week_wednesday=$request->get('week_wednesday');
  $week_thursday=$request->get('week_thursday');
  $week_friday=$request->get('week_friday');
  $week_saturday=$request->get('week_saturday');
  $week_sunday=$request->get('week_sunday');
  $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);
  $operating_weekdays=serialize($operating_weekdays);
  $transfer_blackout_dates=$request->get('blackout_days');
  $transfer_nationality=$request->get('transfer_nationality');
  $transfer_markup=$request->get('transfer_markup');
  $transfer_amount=$request->get('transfer_amount');
  $nationality_markup_details=array();
  for($nation_count=0;$nation_count<count($transfer_nationality);$nation_count++)
  {
    $nationality_markup_details[$nation_count]['transfer_nationality']=$transfer_nationality[$nation_count];
    $nationality_markup_details[$nation_count]['transfer_markup']=$transfer_markup[$nation_count];
    $nationality_markup_details[$nation_count]['transfer_amount']=$transfer_amount[$nation_count];
  }
  $nationality_markup_details=serialize($nationality_markup_details);
  $transfer_validity_from=$request->get('transfer_validity_from');
  $transfer_validity_to=$request->get('transfer_validity_to');
  $transfer_vehicle_type=$request->get('transfer_vehicle_type');
  $transfer_vehicle=$request->get('transfer_vehicle');
  $transfer_car_model=$request->get('transfer_car_model');
  $transfer_manufacture_year=$request->get('transfer_manufacture_year');
  $transfer_duration=$request->get('transfer_duration');
  $transfer_max_passengers=$request->get('transfer_max_passengers');
  $transfer_total_vehicles=$request->get('transfer_total_vehicles');
  $transfer_transport_currency=$request->get('transfer_transport_currency');
  $transfer_vehicle_cost=$request->get('transfer_vehicle_cost');
  $transfer_parking_cost=$request->get('transfer_parking_cost');
  $transfer_driver_tip=$request->get('transfer_driver_tip');
  $transfer_recep_disc=$request->get('transfer_recep_disc');
  $transfer_guide_cost=$request->get('transfer_guide_cost');
  $transfer_attraction_cost=$request->get('transfer_attraction_cost');
  $transfer_images_indexes=$request->get('upload_ativity_images_index');
  $transfer_transport_tariff=array();
  for($transport_count=0;$transport_count<count($transfer_vehicle_type);$transport_count++)
  {
    $transfer_transport_tariff[$transport_count]['transfer_validity_from']=$transfer_validity_from[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_validity_to']=$transfer_validity_to[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_vehicle_type']=$transfer_vehicle_type[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_vehicle']=$transfer_vehicle[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_car_model']=$transfer_car_model[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_manufacture_year']=$transfer_manufacture_year[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_duration']=$transfer_duration[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_max_passengers']=$transfer_max_passengers[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_total_vehicles']=$transfer_total_vehicles[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_transport_currency']=$transfer_transport_currency[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_vehicle_cost']=$transfer_vehicle_cost[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_parking_cost']=$transfer_parking_cost[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_driver_tip']=$transfer_driver_tip[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_recep_disc']=$transfer_recep_disc[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_guide_cost']=$transfer_guide_cost[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_attraction_cost']=$transfer_attraction_cost[$transport_count];
    $transfer_images=array();
    $index=$transfer_images_indexes[$transport_count];
//multifile uploading
    if($request->hasFile("upload_ativity_images$index"))
    {
      $file_array=$request->file("upload_ativity_images$index");
      foreach($file_array as $file)
      {
        $extension=strtolower($file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
        {
          $image_name=$file->getClientOriginalName();
          $image_transfer = "transport-".time()."-".$image_name;
          $dir1 = 'assets/uploads/transport_images/';
          $file->move($dir1, $image_transfer);
          $transfer_images[]=$image_transfer;
        }
      }
    }
    $transfer_transport_tariff[$transport_count]['transfer_images']=$transfer_images;
  }
  $transfer_transport_tariff=serialize($transfer_transport_tariff);
  $transfer_inclusions=$request->get('transfer_inclusions');
  $transfer_exclusions=$request->get('transfer_exclusions');
  $transfer_cancel_policy=$request->get('transfer_cancellation');
  $transfer_terms_conditions=$request->get('transfer_terms_conditions');
  $transfer_created_by=$user_id;
// $transfer_images=array();
//multifile uploading
// if($request->hasFile('upload_ativity_images'))
// {
//  foreach($request->file('upload_ativity_images') as $file)
//  {
//   $extension=strtolower($file->getClientOriginalExtension());
//   if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
//   {
//     $image_name=$file->getClientOriginalName();
//     $image_transfer = "trnasport-".$image_name."-".time().'.'.$file->getClientOriginalExtension();
//     $dir1 = 'assets/uploads/transport_images/';
//     $file->move($dir1, $image_transfer);
//     $transfer_images[]=$image_transfer;
//   }
// }
// }
// $transfer_images=serialize($transfer_images);
  $transfer_images="";
  $insert_transfer=new Transport;
  $insert_transfer->transfer_name=$transfer_name;
  $insert_transfer->supplier_id=$supplier_id;
  $insert_transfer->driver_language=$driver_language;
  $insert_transfer->transfer_country=$transfer_country;
  $insert_transfer->transfer_city=$transfer_city;
  $insert_transfer->transfer_pickup_point=$transfer_pickup_point;
  $insert_transfer->transfer_drop_point=$transfer_drop_point;
  $insert_transfer->transfer_meet_point=$transfer_meet_point;
  $insert_transfer->transfer_description=$transfer_description;
  $insert_transfer->transfer_arrival_time=$transfer_arrival_time;
  $insert_transfer->operating_weekdays=$operating_weekdays;
  $insert_transfer->transfer_blackout_dates=$transfer_blackout_dates;
  $insert_transfer->nationality_markup_details=$nationality_markup_details;
  $insert_transfer->transfer_transport_tariff=$transfer_transport_tariff;
  $insert_transfer->transfer_inclusions=$transfer_inclusions;
  $insert_transfer->transfer_exclusions=$transfer_exclusions;
  $insert_transfer->transfer_cancel_policy=$transfer_cancel_policy;
  $insert_transfer->transfer_terms_conditions=$transfer_terms_conditions ;
  $insert_transfer->transfer_images=$transfer_images;
  $insert_transfer->transfer_created_by=$transfer_created_by;
  $insert_transfer->transfer_create_role=$user_role;
  if($user_role=="Supplier")
  {
    $insert_transfer->transport_approve_status=0;
  }
  if($insert_transfer->save())
  {
    $last_insert_id=$insert_transfer->id;
    $insert_transfer=new Transport_log;
    $insert_transfer->transport_id=$last_insert_id;
    $insert_transfer->transfer_name=$transfer_name;
    $insert_transfer->supplier_id=$supplier_id;
    $insert_transfer->driver_language=$driver_language;
    $insert_transfer->transfer_country=$transfer_country;
    $insert_transfer->transfer_city=$transfer_city;
    $insert_transfer->transfer_pickup_point=$transfer_pickup_point;
    $insert_transfer->transfer_drop_point=$transfer_drop_point;
    $insert_transfer->transfer_meet_point=$transfer_meet_point;
    $insert_transfer->transfer_description=$transfer_description;
    $insert_transfer->transfer_arrival_time=$transfer_arrival_time;
    $insert_transfer->operating_weekdays=$operating_weekdays;
    $insert_transfer->transfer_blackout_dates=$transfer_blackout_dates;
    $insert_transfer->nationality_markup_details=$nationality_markup_details;
    $insert_transfer->transfer_transport_tariff=$transfer_transport_tariff;
    $insert_transfer->transfer_inclusions=$transfer_inclusions;
    $insert_transfer->transfer_exclusions=$transfer_exclusions;
    $insert_transfer->transfer_cancel_policy=$transfer_cancel_policy;
    $insert_transfer->transfer_terms_conditions=$transfer_terms_conditions ;
    $insert_transfer->transfer_images=$transfer_images;
    $insert_transfer->transfer_created_by=$transfer_created_by;
    $insert_transfer->transfer_create_role=$user_role;
    $insert_transfer->transfer_operation_performed="INSERT";
    $insert_transfer->save();
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
public function edit_transport($transport_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $vehicle_type=VehicleType::get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_transport=Transport::where('transport_id',$transport_id)->first();
    }
    else
    {
      $get_transport=Transport::where('transport_id',$transport_id)->where('transfer_created_by',$emp_id)->where('transfer_create_role','!=','Supplier')->first();
    }
    if(!empty($get_transport))
    {
      $supplier_id=$get_transport->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_transport->transfer_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.edit-transport')->with(compact('countries','currency','cities','suppliers','get_transport',"countries_data","rights",'vehicle_type'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}
public function update_transport(Request $request)
{
  // echo "<pre>";
  // print_r($request->all());
 $transport_id=urldecode(base64_decode(base64_decode($request->get('transport_id'))));
 $user_id=session()->has('travel_users_id');
 $check_transport=Transport::where('transport_id',$transport_id)->get();
 $transport_role=$request->get('transport_role');
 if($transport_role=='supplier')
 {
  $user_id=$request->get('supplier_name');
  $user_role='Supplier';
}
else
{
  if(session()->get('travel_users_role')=="Admin")
  {
    $user_role='Admin';
  }
  else
  {
   $user_role='Sub-User';
 }
 $user_id=session()->has('travel_users_id');
}
if(count($check_transport)>0)
{
  $transfer_name=$request->get('transfer_name');
  $supplier_id=$request->get('supplier_name');
  $driver_language=$request->get('driver_language');
  $transfer_country=$request->get('transfer_country');
  $transfer_city=$request->get('transfer_city');
  $transfer_arrival_time=$request->get('transfer_arrival_time');
  $transfer_pickup_point=$request->get('transfer_pickup_point');
  $transfer_drop_point=$request->get('transfer_drop_point');
  $transfer_meet_point=$request->get('transfer_meet_point');
  $transfer_description=$request->get('transfer_description');
  $week_monday=$request->get('week_monday');
  $week_tuesday=$request->get('week_tuesday');
  $week_wednesday=$request->get('week_wednesday');
  $week_thursday=$request->get('week_thursday');
  $week_friday=$request->get('week_friday');
  $week_saturday=$request->get('week_saturday');
  $week_sunday=$request->get('week_sunday');
  $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);
  $operating_weekdays=serialize($operating_weekdays);
  $transfer_blackout_dates=$request->get('blackout_days');
  $transfer_nationality=$request->get('transfer_nationality');
  $transfer_markup=$request->get('transfer_markup');
  $transfer_amount=$request->get('transfer_amount');
  $nationality_markup_details=array();
  for($nation_count=0;$nation_count<count($transfer_nationality);$nation_count++)
  {
    $nationality_markup_details[$nation_count]['transfer_nationality']=$transfer_nationality[$nation_count];
    $nationality_markup_details[$nation_count]['transfer_markup']=$transfer_markup[$nation_count];
    $nationality_markup_details[$nation_count]['transfer_amount']=$transfer_amount[$nation_count];
  }
  $nationality_markup_details=serialize($nationality_markup_details);
  $transfer_validity_from=$request->get('transfer_validity_from');
  $transfer_validity_to=$request->get('transfer_validity_to');
  $transfer_vehicle_type=$request->get('transfer_vehicle_type');  
  $transfer_vehicle=$request->get('transfer_vehicle');
  $transfer_car_model=$request->get('transfer_car_model');
  $transfer_manufacture_year=$request->get('transfer_manufacture_year');
  $transfer_duration=$request->get('transfer_duration');
  $transfer_max_passengers=$request->get('transfer_max_passengers');
  $transfer_total_vehicles=$request->get('transfer_total_vehicles'); 
  $transfer_transport_currency=$request->get('transfer_transport_currency');  
  $transfer_vehicle_cost=$request->get('transfer_vehicle_cost');
  $transfer_parking_cost=$request->get('transfer_parking_cost');
  $transfer_driver_tip=$request->get('transfer_driver_tip');  
  $transfer_recep_disc=$request->get('transfer_recep_disc');
  $transfer_guide_cost=$request->get('transfer_guide_cost');
  $transfer_attraction_cost=$request->get('transfer_attraction_cost');  
  $transfer_images_indexes=$request->get('upload_ativity_images_index'); 
  $transfer_transport_tariff=array();
  for($transport_count=0;$transport_count<count($transfer_vehicle_type);$transport_count++)
  {
    $transfer_transport_tariff[$transport_count]['transfer_validity_from']=$transfer_validity_from[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_validity_to']=$transfer_validity_to[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_vehicle_type']=$transfer_vehicle_type[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_vehicle']=$transfer_vehicle[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_car_model']=$transfer_car_model[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_manufacture_year']=$transfer_manufacture_year[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_duration']=$transfer_duration[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_max_passengers']=$transfer_max_passengers[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_total_vehicles']=$transfer_total_vehicles[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_transport_currency']=$transfer_transport_currency[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_vehicle_cost']=$transfer_vehicle_cost[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_parking_cost']=$transfer_parking_cost[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_driver_tip']=$transfer_driver_tip[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_recep_disc']=$transfer_recep_disc[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_guide_cost']=$transfer_guide_cost[$transport_count];
    $transfer_transport_tariff[$transport_count]['transfer_attraction_cost']=$transfer_attraction_cost[$transport_count];
    $index=$transfer_images_indexes[$transport_count];
    $transfer_images=$request->get('upload_ativity_already_images'.$index);
      //multifile uploading
    if($request->hasFile("upload_ativity_images$index"))
    {
      $file_array=$request->file("upload_ativity_images$index");
      foreach($file_array as $file)
      {
        $extension=strtolower($file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
        {
          $image_name=$file->getClientOriginalName();
          $image_transfer = "transport-".time()."-".$image_name;
          $dir1 = 'assets/uploads/transport_images/';
          $file->move($dir1, $image_transfer);
          $transfer_images[]=$image_transfer;
        }
      }
    }
    $transfer_transport_tariff[$transport_count]['transfer_images']=$transfer_images;
  }
  $transfer_transport_tariff=serialize($transfer_transport_tariff);
  $transfer_inclusions=$request->get('transfer_inclusions');
  $transfer_exclusions=$request->get('transfer_exclusions');
  $transfer_cancel_policy=$request->get('transfer_cancellation');
  $transfer_terms_conditions=$request->get('transfer_terms_conditions');
  $transfer_created_by=$user_id;
  
//       //multifile uploading
//   if($request->hasFile('upload_ativity_images'))
//   {
//    foreach($request->file('upload_ativity_images') as $file)
//    {
//     $extension=strtolower($file->getClientOriginalExtension());
//     if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
//     {
//       $image_name=$file->getClientOriginalName();
//       $image_transfer = "trnasport-".$image_name."-".time().'.'.$file->getClientOriginalExtension();
//                 // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
//       $dir1 = 'assets/uploads/transport_images/';
//       $file->move($dir1, $image_transfer);
//       $transfer_images[]=$image_transfer;
//     }
//   }
// }
// $transfer_images=serialize($transfer_images);
  $transfer_images="";
  $update_transport_array=array(
    "transfer_name"=>$transfer_name,
    "supplier_id"=>$supplier_id,
    "driver_language"=>$driver_language,
    "transfer_country"=>$transfer_country,
    "transfer_city"=>$transfer_city,
    "transfer_pickup_point"=>$transfer_pickup_point,
    "transfer_drop_point"=>$transfer_drop_point,
    "transfer_meet_point"=>$transfer_meet_point,
    "transfer_description"=>$transfer_description,
    "transfer_arrival_time"=>$transfer_arrival_time,
    "operating_weekdays"=>$operating_weekdays,
    "transfer_blackout_dates"=>$transfer_blackout_dates,
    "nationality_markup_details"=>$nationality_markup_details,
    "transfer_transport_tariff"=>$transfer_transport_tariff,
    "transfer_inclusions"=>$transfer_inclusions,
    "transfer_exclusions"=>$transfer_exclusions,
    "transfer_cancel_policy"=>$transfer_cancel_policy,
    "transfer_terms_conditions"=>$transfer_terms_conditions ,
    "transfer_images"=>$transfer_images,
    "transfer_create_role"=>$user_role,
  );
  $insert_transport_log_array=array(
    "transport_id"=>$transport_id,
    "transfer_name"=>$transfer_name,
    "supplier_id"=>$supplier_id,
    "driver_language"=>$driver_language,
    "transfer_country"=>$transfer_country,
    "transfer_city"=>$transfer_city,
    "transfer_pickup_point"=>$transfer_pickup_point,
    "transfer_drop_point"=>$transfer_drop_point,
    "transfer_meet_point"=>$transfer_meet_point,
    "transfer_description"=>$transfer_description,
    "transfer_arrival_time"=>$transfer_arrival_time,
    "operating_weekdays"=>$operating_weekdays,
    "transfer_blackout_dates"=>$transfer_blackout_dates,
    "nationality_markup_details"=>$nationality_markup_details,
    "transfer_transport_tariff"=>$transfer_transport_tariff,
    "transfer_inclusions"=>$transfer_inclusions,
    "transfer_exclusions"=>$transfer_exclusions,
    "transfer_cancel_policy"=>$transfer_cancel_policy,
    "transfer_terms_conditions"=>$transfer_terms_conditions ,
    "transfer_images"=>$transfer_images,
    "transfer_created_by"=>$transfer_created_by,
    "transfer_create_role"=>$user_role,
    "transfer_operation_performed"=>"UPDATE",
  );
  $update_transport=Transport::where('transport_id',$transport_id)->update($update_transport_array);
  if($update_transport)
  {
    $insert_log_transport=Transport_log::insert($insert_transport_log_array);
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function transport_details($transport_id)
{
  if(session()->has('travel_users_id'))
  {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::orderBy('code','asc')->get();
   $vehicle_type=VehicleType::get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   $emp_id=session()->get('travel_users_id');
   if(strpos($rights['admin_which'],'view')!==false)
   {
    $get_transport=Transport::where('transport_id',$transport_id)->first();
  }
  else
  {
    $get_transport=Transport::where('transport_id',$transport_id)->where('transfer_created_by',$emp_id)->where('transfer_create_role','!=','Supplier')->first();
  }
  if(!empty($get_transport))
  {
    $supplier_id=$get_transport->supplier_id;
    $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
    $supplier_countries=$get_supplier_countries->supplier_opr_countries;
    $countries_data=explode(',', $supplier_countries);
    return view('mains.transport-details-view')->with(compact('countries','currency','cities','suppliers','get_transport','countries_data','vehicle_type','rights'));
  }
  else
  {
    return redirect()->back();
  }
}
else
{
  return redirect()->route('index');
}
}
public function update_transport_approval(Request $request)
{
 $id=$request->get('transport_id');
 $action_perform=$request->get('action_perform');
 if($action_perform=="approve")
 {
  $update_transport=Transport::where('transport_id',$id)->update(["transport_approve_status"=>1]);
  if($update_transport)
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else if($action_perform=="reject")
{
  $update_transport=Transport::where('transport_id',$id)->update(["transport_approve_status"=>2]);
  if($update_transport)
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_transport_active_inactive(Request $request)
{
 $id=$request->get('transport_id');
 $action_perform=$request->get('action_perform');
 if($action_perform=="active")
 {
  $update_transport=Transport::where('transport_id',$id)->update(["transfer_status"=>1]);
  if($update_transport)
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else if($action_perform=="inactive")
{
  $update_transport=Transport::where('transport_id',$id)->update(["transfer_status"=>0]);
  if($update_transport)
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function create_hotel(Request $request)
{
  if(session()->has('travel_users_id'))
  {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::orderBy('code','asc')->get();
   $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();
   $hotel_meal=HotelMeal::get();
   $fetch_amenities=Amenities::where('amenities_status',1)->get();
   $fetch_sub_amenities=SubAmenities::where('sub_amenities_status',1)->get();
   $suppliers=Suppliers::where('supplier_status',1)->get();

   return view('mains.create-hotel')->with(compact('countries','currency','fetch_hotel_type','suppliers','rights','hotel_meal','fetch_amenities','fetch_sub_amenities'));
 }
 else
 {
  return redirect()->route('index');
}
}
public function insert_hotel(Request $request)
{
// echo "<pre>";
// print_r($request->all());
// die();
 $hotel_role=$request->get('hotel_role');
 if($hotel_role=='supplier')
 {
  $user_id=$request->get('supplier_name');
  $user_role='Supplier';
}
else
{
  $user_id=session()->has('travel_users_id');
  if(session()->get('travel_users_role')=="Admin")
  {
    $user_role='Admin';
  }
  else
  {
   $user_role='Sub-User';
 }
}
$hotel_created_by=$user_id;
$hotel_type=$request->get('hotel_type');
$hotel_name=$request->get('hotel_name');
$supplier_id=$request->get('supplier_name');
$hotel_contact=$request->get('contact_no');
$hotel_rating=$request->get('hotel_rating');
$hotel_address=$request->get('hotel_address');
$hotel_description=$request->get('hotel_description');
$hotel_country=$request->get('hotel_country');
$hotel_city=$request->get('hotel_city');
$hotel_own_currency=$request->get('hotel_own_currency');
// $season_name=$request->get('season_name'); 
// $booking_validity_from=$request->get('booking_validity_from'); 
// $booking_validity_to=$request->get('booking_validity_to'); 
// $stop_sale=$request->get('stop_sale'); 
// $season_details=array();
// for($season=0;$season<count($season_name);$season++)
// {
//   $season_details[$season]["season_name"]=$season_name[$season];
//   $season_details[$season]["booking_validity_from"]=$booking_validity_from[$season];
//   $season_details[$season]["booking_validity_to"]=$booking_validity_to[$season];
//   $season_details[$season]["stop_sale"]=$stop_sale[$season];
// }
// $season_details=serialize($season_details);
// $activity_nationality=$request->get('activity_nationality'); 
// $activity_markup=$request->get('activity_markup'); 
// $activity_amount=$request->get('activity_amount'); 
// $markup_details=array();
// for($markup=0;$markup<count($activity_nationality);$markup++)
// {
//   $markup_details[$markup]["activity_nationality"]=$activity_nationality[$markup];
//   $markup_details[$markup]["activity_markup"]=$activity_markup[$markup];
//   $markup_details[$markup]["activity_amount"]=$activity_amount[$markup];
// }
// $markup_details=serialize($markup_details);
// $blackout_days=$request->get('blackout_days'); 
// $blackout_dates=serialize($blackout_days);
// $surcharge_name=$request->get('surcharge_name'); 
// $surcharge_day=$request->get('surcharge_day'); 
// $surcharge_price=$request->get('surcharge_price'); 
// $surcharge_details=array();
// for($surcharge=0;$surcharge<count($surcharge_name);$surcharge++)
// {
//   $surcharge_details[$surcharge]["surcharge_name"]=$surcharge_name[$surcharge];
//   $surcharge_details[$surcharge]["surcharge_day"]=$surcharge_day[$surcharge];
//   $surcharge_details[$surcharge]["surcharge_price"]=$surcharge_price[$surcharge];
// }
// $surcharge_details=serialize($surcharge_details);



$hotel_promotion_detail=array();
$hotel_promotion_detail['hotel_promotion']=$request->get('hotel_promotion');
$hotel_promotion_detail['hotel_prom_discount']=$request->get('hotel_prom_discount');
$hotel_promotion_detail['hotel_promotion_from']=$request->get('hotel_promotion_from');
$hotel_promotion_detail['hotel_promotion_to']=$request->get('hotel_promotion_to');
$hotel_promotion_detail['hotel_promotion_disc_booking']=$request->get('hotel_promotion_disc_booking');
$hotel_promotion_detail['hotel_promotion_disc_travel']=$request->get('hotel_promotion_disc_travel');
$hotel_promotion_detail=serialize($hotel_promotion_detail);
$hotel_addon_name=$request->get('hotel_addon_name'); 
$hotel_desc=$request->get('hotel_desc'); 
$hotel_adult_cost=$request->get('hotel_adult_cost'); 
$hotel_child_cost=$request->get('hotel_child_cost'); 
$hotel_currency=$request->get('hotel_currency'); 
$addon_details=array();
for($addon=0;$addon<count($hotel_addon_name);$addon++)
{
  $addon_details[$addon]["hotel_addon_name"]=$hotel_addon_name[$addon];
  $addon_details[$addon]["hotel_desc"]=$hotel_desc[$addon];
  $addon_details[$addon]["hotel_adult_cost"]=$hotel_adult_cost[$addon];
  $addon_details[$addon]["hotel_child_cost"]=$hotel_child_cost[$addon];
  $addon_details[$addon]["hotel_currency"]=$hotel_currency[$addon];
}
$addon_details=serialize($addon_details);

$reason_name=$request->get('reason_name'); 
$reason_details=array();
for($reason=0;$reason<count($reason_name);$reason++)
{
  $reason_details[$reason]["reason_name"]=$reason_name[$reason];
}
$reason_details=serialize($reason_details);

$policy_name=$request->get('policy_name'); 
$policy_desc=$request->get('policy_desc'); 
$policy_details=array();
for($policy=0;$policy<count($policy_name);$policy++)
{
  $policy_details[$policy]["policy_name"]=$policy_name[$policy];
  $policy_details[$policy]["policy_desc"]=$policy_desc[$policy];
}
$policy_details=serialize($policy_details);
$booking_validity_from=$request->get('validity_operation_from');
$booking_validity_to=$request->get('validity_operation_to'); 
$hotel_inclusions=$request->get('hotel_inclusions'); 
$hotel_exclusions=$request->get('hotel_exclusions');
$hotel_cancel_policy=$request->get('hotel_cancellation'); 
$hotel_terms_conditions=$request->get('hotel_terms_conditions'); 
$hotel_images=array();
        //multifile uploading
if($request->hasFile('upload_ativity_images'))
{
 foreach($request->file('upload_ativity_images') as $file)
 {
  $extension=strtolower($file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
  {
    $image_name=$file->getClientOriginalName();
    $image_hotel = "hotel-".time()."-".$image_name;
                  // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
    $dir1 = 'assets/uploads/hotel_images/';
    $file->move($dir1, $image_hotel);
    $hotel_images[]=$image_hotel;
  }
}
}
$hotel_images=serialize($hotel_images);
$hotel_amenities=$request->get('amenities');
$hotel_amenities=serialize($hotel_amenities);
$insert_hotel=new Hotels;
$insert_hotel->hotel_name=$hotel_name;
$insert_hotel->hotel_type=$hotel_type;
$insert_hotel->supplier_id=$supplier_id;
$insert_hotel->hotel_contact=$hotel_contact;
$insert_hotel->hotel_rating=$hotel_rating;
$insert_hotel->hotel_address=$hotel_address;
$insert_hotel->hotel_description=$hotel_description;
$insert_hotel->hotel_currency=$hotel_own_currency;
$insert_hotel->hotel_country=$hotel_country;
$insert_hotel->hotel_city=$hotel_city;
$insert_hotel->hotel_promotion_details=$hotel_promotion_detail;
$insert_hotel->hotel_add_ons=$addon_details;
$insert_hotel->hotel_reasons=$reason_details;
$insert_hotel->hotel_other_policies=$policy_details;
$insert_hotel->booking_validity_from=$booking_validity_from;
$insert_hotel->booking_validity_to=$booking_validity_to;
$insert_hotel->hotel_amenities=$hotel_amenities;
$insert_hotel->hotel_inclusions=$hotel_inclusions;
$insert_hotel->hotel_exclusions=$hotel_exclusions;
$insert_hotel->hotel_cancel_policy=$hotel_cancel_policy;
$insert_hotel->hotel_terms_conditions=$hotel_terms_conditions;
$insert_hotel->hotel_images=$hotel_images;
$insert_hotel->hotel_created_by=$hotel_created_by;
$insert_hotel->hotel_create_role=$user_role;
if($user_role=="Supplier")
{
  $insert_hotel->hotel_approve_status=0;
}

if($insert_hotel->save())
{
  $last_insert_id=$insert_hotel->id;
  $insert_hotel_log=new Hotels_log;
  $insert_hotel_log->hotel_id=$last_insert_id;
  $insert_hotel_log->hotel_name=$hotel_name;
  $insert_hotel_log->hotel_type=$hotel_type;
  $insert_hotel_log->supplier_id=$supplier_id;
  $insert_hotel_log->hotel_contact=$hotel_contact;
  $insert_hotel_log->hotel_rating=$hotel_rating;
  $insert_hotel_log->hotel_address=$hotel_address;
  $insert_hotel_log->hotel_description=$hotel_description;
  $insert_hotel_log->hotel_country=$hotel_country;
  $insert_hotel_log->hotel_city=$hotel_city;
  $insert_hotel_log->hotel_promotion_details=$hotel_promotion_detail;
  $insert_hotel_log->hotel_add_ons=$addon_details;
  $insert_hotel->hotel_reasons=$reason_details;
  $insert_hotel_log->hotel_other_policies=$policy_details;
  $insert_hotel_log->booking_validity_from=$booking_validity_from;
  $insert_hotel_log->booking_validity_to=$booking_validity_to;
  $insert_hotel_log->hotel_amenities=$hotel_amenities;
  $insert_hotel_log->hotel_inclusions=$hotel_inclusions;
  $insert_hotel_log->hotel_exclusions=$hotel_exclusions;
  $insert_hotel_log->hotel_cancel_policy=$hotel_cancel_policy;
  $insert_hotel_log->hotel_terms_conditions=$hotel_terms_conditions;
  $insert_hotel_log->hotel_images=$hotel_images;
  $insert_hotel_log->hotel_created_by=$hotel_created_by;
  $insert_hotel_log->hotel_create_role=$user_role;
  $insert_hotel_log->hotel_operation_performed='INSERT';
  $insert_hotel_log->save();



$room_type=$request->get('room_type'); 
$room_class=$request->get('room_class'); 
$room_size=$request->get('room_size'); 
$room_currency=$request->get('room_currency'); 
$room_adult=$request->get('room_adult'); 
$room_cwb=$request->get('room_cwb'); 
$room_cnb=$request->get('room_cnb'); 
$room_qty=$request->get('room_qty');
$room_extra_bed=$request->get('room_extra_bed');
$room_meal=$request->get('room_meal');
$room_checkin=$request->get('room_checkin'); 
$room_checkout=$request->get('room_checkout');  
$room_amenities=$request->get('room_amenities'); 
$season_name=$request->get('season_name'); 
$booking_validity_from=$request->get('booking_validity_from'); 
$booking_validity_to=$request->get('booking_validity_to');
$room_occupancy=$request->get('room_occupancy'); 
$room_price=$request->get('room_price');  
$upload_ativity_images1=$request->get('upload_ativity_images1'); 
for($rooms_count=0;$rooms_count<count($room_type);$rooms_count++)
{
  $insert_hotel_room=new HotelRooms;
  $insert_hotel_room->hotel_id=$last_insert_id;
  $insert_hotel_room->hotel_room_type=$room_type[$rooms_count];
  $insert_hotel_room->hotel_room_class=$room_class[$rooms_count];
  $insert_hotel_room->hotel_room_size=$room_size[$rooms_count];
  $insert_hotel_room->hotel_room_currency=$room_currency[$rooms_count];
  $insert_hotel_room->hotel_room_adults=$room_adult[$rooms_count];
  $insert_hotel_room->hotel_room_cwb=$room_cwb[$rooms_count];
  $insert_hotel_room->hotel_room_cnb=$room_cnb[$rooms_count];
  $insert_hotel_room->hotel_room_no_of_rooms=$room_qty[$rooms_count];
  $insert_hotel_room->hotel_room_extra_bed_price=$room_extra_bed[$rooms_count];
  $insert_hotel_room->hotel_room_meal=$room_meal[$rooms_count];
  $insert_hotel_room->hotel_room_checkin=$room_checkin[$rooms_count];
  $insert_hotel_room->hotel_room_checkout=$room_checkout[$rooms_count];
  $insert_hotel_room->hotel_room_amenities=serialize($room_amenities[$rooms_count]);
  $room_images=array();

  for($i=0;$i<count($upload_ativity_images1[$rooms_count]);$i++)
  {

    if($request->hasFile("upload_ativity_images1.".$rooms_count.".".$i))
    {
      $file_array=$request->file("upload_ativity_images1.".$rooms_count.".".$i);
      foreach($file_array as $file)
      {
        $extension=strtolower($file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
        {

          $image_name=$file->getClientOriginalName();
          $image_transfer = "room-".time().uniqid()."-".$image_name;
          $dir1 = 'assets/uploads/hotel_images/';
          $file->move($dir1, $image_transfer);
          $room_images[$i][]=$image_transfer;
        }
      }
    }
  }
 $insert_hotel_room->hotel_room_images=serialize($room_images);
 $insert_hotel_room->hotel_room_created_by=$hotel_created_by;
 $insert_hotel_room->hotel_room_create_role=$user_role; 

 if($insert_hotel_room->save())
 {

  $last_inserted_room_id=$insert_hotel_room->hotel_room_id;

  for($season_count=0;$season_count<count($season_name[$rooms_count]);$season_count++)
  {
   $insert_hotel_season=new HotelRoomSeasons;
   $insert_hotel_season->hotel_id_fk=$last_insert_id;
   $insert_hotel_season->hotel_room_id_fk=$last_inserted_room_id;
   $insert_hotel_season->hotel_room_season_name=$season_name[$rooms_count][$season_count];
   $insert_hotel_season->hotel_room_season_validity_from=$booking_validity_from[$rooms_count][$season_count];
   $insert_hotel_season->hotel_room_season_validity_to=$booking_validity_to[$rooms_count][$season_count];
   $insert_hotel_season->hotel_room_season_number_occupany=count($room_occupancy[$rooms_count][$season_count]);
   $insert_hotel_season->hotel_room_season_created_by=$hotel_created_by;
   $insert_hotel_season->hotel_room_season_create_role=$user_role;

   if($insert_hotel_season->save())
   {
    $last_inserted_season_id=$insert_hotel_season->hotel_room_season_id;

     for($occupancy_count=0;$occupancy_count<count($room_occupancy[$rooms_count][$season_count]);$occupancy_count++)
  {
   $insert_hotel_season_occupancy=new HotelRoomSeasonOccupancy;
   $insert_hotel_season_occupancy->hotel_id_fk=$last_insert_id;
   $insert_hotel_season_occupancy->hotel_room_id_fk=$last_inserted_room_id;
   $insert_hotel_season_occupancy->hotel_room_season_id_fk=$last_inserted_season_id;
   $insert_hotel_season_occupancy->hotel_room_occupancy_qty=$room_occupancy[$rooms_count][$season_count][$occupancy_count];
   $insert_hotel_season_occupancy->hotel_room_occupancy_price=$room_price[$rooms_count][$season_count][$occupancy_count];
   $insert_hotel_season_occupancy->hotel_room_occupancy_created_by=$hotel_created_by;
   $insert_hotel_season_occupancy->hotel_room_occupancy_created_role=$user_role;
   $insert_hotel_season_occupancy->save();
 
  }


   } 
 
  }
 } 
}



  echo "success";

}
else
{
  echo "fail";
}
}

public function insert_hotel_old(Request $request)
{
// echo "<pre>";
// print_r($request->all());
// die();
 $hotel_role=$request->get('hotel_role');
 if($hotel_role=='supplier')
 {
  $user_id=$request->get('supplier_name');
  $user_role='Supplier';
}
else
{
  $user_id=session()->has('travel_users_id');
  if(session()->get('travel_users_role')=="Admin")
  {
    $user_role='Admin';
  }
  else
  {
   $user_role='Sub-User';
 }
}
$hotel_created_by=$user_id;
$hotel_type=$request->get('hotel_type');
$hotel_name=$request->get('hotel_name');
$supplier_id=$request->get('supplier_name');
$hotel_contact=$request->get('contact_no');
$hotel_rating=$request->get('hotel_rating');
$hotel_address=$request->get('hotel_address');
$hotel_description=$request->get('hotel_description');
$hotel_country=$request->get('hotel_country');
$hotel_city=$request->get('hotel_city');
$season_name=$request->get('season_name'); 
$booking_validity_from=$request->get('booking_validity_from'); 
$booking_validity_to=$request->get('booking_validity_to'); 
$stop_sale=$request->get('stop_sale'); 
$season_details=array();
for($season=0;$season<count($season_name);$season++)
{
  $season_details[$season]["season_name"]=$season_name[$season];
  $season_details[$season]["booking_validity_from"]=$booking_validity_from[$season];
  $season_details[$season]["booking_validity_to"]=$booking_validity_to[$season];
  $season_details[$season]["stop_sale"]=$stop_sale[$season];
}
$season_details=serialize($season_details);
$activity_nationality=$request->get('activity_nationality'); 
$activity_markup=$request->get('activity_markup'); 
$activity_amount=$request->get('activity_amount'); 
$markup_details=array();
for($markup=0;$markup<count($activity_nationality);$markup++)
{
  $markup_details[$markup]["activity_nationality"]=$activity_nationality[$markup];
  $markup_details[$markup]["activity_markup"]=$activity_markup[$markup];
  $markup_details[$markup]["activity_amount"]=$activity_amount[$markup];
}
$markup_details=serialize($markup_details);
$blackout_days=$request->get('blackout_days'); 
$blackout_dates=serialize($blackout_days);
$surcharge_name=$request->get('surcharge_name'); 
$surcharge_day=$request->get('surcharge_day'); 
$surcharge_price=$request->get('surcharge_price'); 
$surcharge_details=array();
for($surcharge=0;$surcharge<count($surcharge_name);$surcharge++)
{
  $surcharge_details[$surcharge]["surcharge_name"]=$surcharge_name[$surcharge];
  $surcharge_details[$surcharge]["surcharge_day"]=$surcharge_day[$surcharge];
  $surcharge_details[$surcharge]["surcharge_price"]=$surcharge_price[$surcharge];
}
$surcharge_details=serialize($surcharge_details);
$room_type=$request->get('room_type'); 
$room_min=$request->get('room_min'); 
$room_max=$request->get('room_max');
$room_class=$request->get('room_class'); 
$room_size=$request->get('room_size'); 
$room_currency=$request->get('room_currency'); 
$room_adult=$request->get('room_adult'); 
$room_cwb=$request->get('room_cwb'); 
$room_cnb=$request->get('room_cnb'); 
$room_weekend=$request->get('room_weekend'); 
$room_qty=$request->get('room_qty');
$room_extra_bed=$request->get('room_extra_bed');
$room_meal=$request->get('room_meal');
$room_night=$request->get('room_night');
$room_checkin=$request->get('room_checkin'); 
$room_checkout=$request->get('room_checkout');  
$room_amenities=$request->get('room_amenities');  
$rate_allocation_details=array();
for($allocation=0;$allocation<count($room_type);$allocation++)
{
  $rate_allocation_details[$allocation]["room_type"]=$room_type[$allocation];
  $rate_allocation_details[$allocation]["room_min"]=$room_min[$allocation];
  $rate_allocation_details[$allocation]["room_max"]=$room_max[$allocation];
  $rate_allocation_details[$allocation]["room_class"]=$room_class[$allocation];
  $rate_allocation_details[$allocation]["room_size"]=$room_size[$allocation];
  $rate_allocation_details[$allocation]["room_currency"]=$room_currency[$allocation];
  $rate_allocation_details[$allocation]["room_adult"]=$room_adult[$allocation];
  $rate_allocation_details[$allocation]["room_cwb"]=$room_cwb[$allocation];
  $rate_allocation_details[$allocation]["room_cnb"]=$room_cnb[$allocation];
  $rate_allocation_details[$allocation]["room_weekend"]=$room_weekend[$allocation];
  $rate_allocation_details[$allocation]["room_qty"]=$room_qty[$allocation];
  $rate_allocation_details[$allocation]["room_extra_bed"]=$room_extra_bed[$allocation];
  $rate_allocation_details[$allocation]["room_meal"]=$room_meal[$allocation];
  $rate_allocation_details[$allocation]["room_night"]=$room_night[$allocation];
  $rate_allocation_details[$allocation]["room_checkin"]=$room_checkin[$allocation];
  $rate_allocation_details[$allocation]["room_checkout"]=$room_checkout[$allocation];
  $rate_allocation_details[$allocation]["room_amenities"]=$room_amenities[$allocation];
  $room_images=array();
  for($i=0;$i<count($room_type[$allocation]);$i++)
  {

    if($request->hasFile("upload_ativity_images1.".$allocation.".".$i))
    {
      $file_array=$request->file("upload_ativity_images1.".$allocation.".".$i);
      foreach($file_array as $file)
      {
        $extension=strtolower($file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
        {

          $image_name=$file->getClientOriginalName();
          $image_transfer = "room-".time().uniqid()."-".$image_name;
          $dir1 = 'assets/uploads/hotel_images/';
          $file->move($dir1, $image_transfer);
          $room_images[$i][]=$image_transfer;
        }
      }
    }
  }
  $rate_allocation_details[$allocation]["room_images"]=$room_images;
}
$rate_allocation_details=serialize($rate_allocation_details);
$hotel_promotion_detail=array();
$hotel_promotion_detail['hotel_promotion']=$request->get('hotel_promotion');
$hotel_promotion_detail['hotel_prom_discount']=$request->get('hotel_prom_discount');
$hotel_promotion_detail['hotel_promotion_from']=$request->get('hotel_promotion_from');
$hotel_promotion_detail['hotel_promotion_to']=$request->get('hotel_promotion_to');
$hotel_promotion_detail['hotel_promotion_disc_booking']=$request->get('hotel_promotion_disc_booking');
$hotel_promotion_detail['hotel_promotion_disc_travel']=$request->get('hotel_promotion_disc_travel');
$hotel_promotion_detail=serialize($hotel_promotion_detail);
$hotel_addon_name=$request->get('hotel_addon_name'); 
$hotel_desc=$request->get('hotel_desc'); 
$hotel_adult_cost=$request->get('hotel_adult_cost'); 
$hotel_child_cost=$request->get('hotel_child_cost'); 
$hotel_currency=$request->get('hotel_currency'); 
$addon_details=array();
for($addon=0;$addon<count($hotel_addon_name);$addon++)
{
  $addon_details[$addon]["hotel_addon_name"]=$hotel_addon_name[$addon];
  $addon_details[$addon]["hotel_desc"]=$hotel_desc[$addon];
  $addon_details[$addon]["hotel_adult_cost"]=$hotel_adult_cost[$addon];
  $addon_details[$addon]["hotel_child_cost"]=$hotel_child_cost[$addon];
  $addon_details[$addon]["hotel_currency"]=$hotel_currency[$addon];
}
$addon_details=serialize($addon_details);

$reason_name=$request->get('reason_name'); 
$reason_details=array();
for($reason=0;$reason<count($reason_name);$reason++)
{
  $reason_details[$reason]["reason_name"]=$reason_name[$reason];
}
$reason_details=serialize($reason_details);

$policy_name=$request->get('policy_name'); 
$policy_desc=$request->get('policy_desc'); 
$policy_details=array();
for($policy=0;$policy<count($policy_name);$policy++)
{
  $policy_details[$policy]["policy_name"]=$policy_name[$policy];
  $policy_details[$policy]["policy_desc"]=$policy_desc[$policy];
}
$policy_details=serialize($policy_details);
$booking_validity_from=$request->get('validity_operation_from');
$booking_validity_to=$request->get('validity_operation_to'); 
$hotel_inclusions=$request->get('hotel_inclusions'); 
$hotel_exclusions=$request->get('hotel_exclusions');
$hotel_cancel_policy=$request->get('hotel_cancellation'); 
$hotel_terms_conditions=$request->get('hotel_terms_conditions'); 
$hotel_images=array();
        //multifile uploading
if($request->hasFile('upload_ativity_images'))
{
 foreach($request->file('upload_ativity_images') as $file)
 {
  $extension=strtolower($file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
  {
    $image_name=$file->getClientOriginalName();
    $image_hotel = "hotel-".time()."-".$image_name;
                  // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
    $dir1 = 'assets/uploads/hotel_images/';
    $file->move($dir1, $image_hotel);
    $hotel_images[]=$image_hotel;
  }
}
}
$hotel_images=serialize($hotel_images);
$hotel_amenities=$request->get('amenities');
$hotel_amenities=serialize($hotel_amenities);
$insert_hotel=new Hotels;
$insert_hotel->hotel_name=$hotel_name;
$insert_hotel->hotel_type=$hotel_type;
$insert_hotel->supplier_id=$supplier_id;
$insert_hotel->hotel_contact=$hotel_contact;
$insert_hotel->hotel_rating=$hotel_rating;
$insert_hotel->hotel_address=$hotel_address;
$insert_hotel->hotel_description=$hotel_description;
$insert_hotel->hotel_country=$hotel_country;
$insert_hotel->hotel_city=$hotel_city;
$insert_hotel->hotel_season_details=$season_details;
$insert_hotel->hotel_markup_details=$markup_details;
$insert_hotel->hotel_blackout_dates=$blackout_dates;
$insert_hotel->hotel_surcharge_details=$surcharge_details;
$insert_hotel->rate_allocation_details=$rate_allocation_details;
$insert_hotel->hotel_promotion_details=$hotel_promotion_detail;
$insert_hotel->hotel_add_ons=$addon_details;
$insert_hotel->hotel_reasons=$reason_details;
$insert_hotel->hotel_other_policies=$policy_details;
$insert_hotel->booking_validity_from=$booking_validity_from;
$insert_hotel->booking_validity_to=$booking_validity_to;
$insert_hotel->hotel_amenities=$hotel_amenities;
$insert_hotel->hotel_inclusions=$hotel_inclusions;
$insert_hotel->hotel_exclusions=$hotel_exclusions;
$insert_hotel->hotel_cancel_policy=$hotel_cancel_policy;
$insert_hotel->hotel_terms_conditions=$hotel_terms_conditions;
$insert_hotel->hotel_images=$hotel_images;
$insert_hotel->hotel_created_by=$hotel_created_by;
$insert_hotel->hotel_create_role=$user_role;
if($user_role=="Supplier")
{
  $insert_hotel->hotel_approve_status=0;
}

if($insert_hotel->save())
{
  $last_insert_id=$insert_hotel->id;
  $insert_hotel_log=new Hotels_log;
  $insert_hotel_log->hotel_id=$last_insert_id;
  $insert_hotel_log->hotel_name=$hotel_name;
  $insert_hotel_log->hotel_type=$hotel_type;
  $insert_hotel_log->supplier_id=$supplier_id;
  $insert_hotel_log->hotel_contact=$hotel_contact;
  $insert_hotel_log->hotel_rating=$hotel_rating;
  $insert_hotel_log->hotel_address=$hotel_address;
  $insert_hotel_log->hotel_description=$hotel_description;
  $insert_hotel_log->hotel_country=$hotel_country;
  $insert_hotel_log->hotel_city=$hotel_city;
  $insert_hotel_log->hotel_season_details=$season_details;
  $insert_hotel_log->hotel_markup_details=$markup_details;
  $insert_hotel_log->hotel_blackout_dates=$blackout_dates;
  $insert_hotel_log->hotel_surcharge_details=$surcharge_details;
  $insert_hotel_log->rate_allocation_details=$rate_allocation_details;
  $insert_hotel_log->hotel_promotion_details=$hotel_promotion_detail;
  $insert_hotel_log->hotel_add_ons=$addon_details;
  $insert_hotel->hotel_reasons=$reason_details;
  $insert_hotel_log->hotel_other_policies=$policy_details;
  $insert_hotel_log->booking_validity_from=$booking_validity_from;
  $insert_hotel_log->booking_validity_to=$booking_validity_to;
  $insert_hotel_log->hotel_amenities=$hotel_amenities;
  $insert_hotel_log->hotel_inclusions=$hotel_inclusions;
  $insert_hotel_log->hotel_exclusions=$hotel_exclusions;
  $insert_hotel_log->hotel_cancel_policy=$hotel_cancel_policy;
  $insert_hotel_log->hotel_terms_conditions=$hotel_terms_conditions;
  $insert_hotel_log->hotel_images=$hotel_images;
  $insert_hotel_log->hotel_created_by=$hotel_created_by;
  $insert_hotel_log->hotel_create_role=$user_role;
  $insert_hotel_log->hotel_operation_performed='INSERT';
  $insert_hotel_log->save();
  echo "success";
}
else
{
  echo "fail";
}
}
public function edit_hotel($hotel_id)
{
  if(session()->has('travel_users_id'))
  {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::orderBy('code','asc')->get();
   $hotel_meal=HotelMeal::get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   $emp_id=session()->get('travel_users_id');
   $fetch_amenities=Amenities::get();
   $fetch_sub_amenities=SubAmenities::get();
   $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();
   if(strpos($rights['admin_which'],'edit_delete')!==false)
   {
    $get_hotels=Hotels::where('hotel_id',$hotel_id)->first();
  }
  else
  {
    $get_hotels=Hotels::where('hotel_id',$hotel_id)->where('hotel_created_by',$emp_id)->where('hotel_create_role','!=','Supplier')->first();
  }
  if(!empty($get_hotels))
  {
   $supplier_id=$get_hotels->supplier_id;
   $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
   $supplier_countries=$get_supplier_countries->supplier_opr_countries;
   $countries_data=explode(',', $supplier_countries);
   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_hotels->hotel_country)->select("cities.*")->orderBy('cities.name','asc')->get();
$hotel_currency="";
   $hotel_currency_fetch=HotelRooms::where('hotel_id',$get_hotels->hotel_id)->first();
   if(!empty($hotel_currency_fetch))
   {
    $hotel_currency=$hotel_currency_fetch->hotel_room_currency;
   }
   return view('mains.edit-hotel')->with(compact('countries','currency','cities','suppliers','get_hotels',"countries_data","rights","hotel_meal","fetch_amenities","fetch_sub_amenities","fetch_hotel_type","hotel_currency"));
 }
 else
 {
  return redirect()->back();
}
}
else
{
  return redirect()->route('index');
}
}

public function edit_old_hotel($hotel_id)
{
  if(session()->has('travel_users_id'))
  {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::orderBy('code','asc')->get();
   $hotel_meal=HotelMeal::get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   $emp_id=session()->get('travel_users_id');
   $fetch_amenities=Amenities::get();
   $fetch_sub_amenities=SubAmenities::get();
   $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();
   if(strpos($rights['admin_which'],'edit_delete')!==false)
   {
    $get_hotels=Hotels::where('hotel_id',$hotel_id)->first();
  }
  else
  {
    $get_hotels=Hotels::where('hotel_id',$hotel_id)->where('hotel_created_by',$emp_id)->where('hotel_create_role','!=','Supplier')->first();
  }
  if(!empty($get_hotels))
  {
   $supplier_id=$get_hotels->supplier_id;
   $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
   $supplier_countries=$get_supplier_countries->supplier_opr_countries;
   $countries_data=explode(',', $supplier_countries);
   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_hotels->hotel_country)->select("cities.*")->orderBy('cities.name','asc')->get();
   return view('mains.edit-hotel-old')->with(compact('countries','currency','cities','suppliers','get_hotels',"countries_data","rights","hotel_meal","fetch_amenities","fetch_sub_amenities","fetch_hotel_type"));
 }
 else
 {
  return redirect()->back();
}
}
else
{
  return redirect()->route('index');
} 
}
public function update_hotel(Request $request)
{
//   echo "<pre>";
// print_r($request->all());
// die();
 $hotel_role=$request->get('hotel_role');
 if($hotel_role=='supplier')
 {
  $user_id=$request->get('supplier_name');
  $user_role='Supplier';
}
else
{
 if(session()->get('travel_users_role')=="Admin")
 {
  $user_role='Admin';
}
else
{
 $user_role='Sub-User';
}
$user_id=session()->has('travel_users_id');
}
$hotel_id=$request->get('hotel_id');
$hotel_created_by=$user_id;
$hotel_type=$request->get('hotel_type');
$hotel_name=$request->get('hotel_name');
$supplier_id=$request->get('supplier_name');
$hotel_contact=$request->get('contact_no');
$hotel_rating=$request->get('hotel_rating');
$hotel_address=$request->get('hotel_address');
$hotel_description=$request->get('hotel_description');
$hotel_country=$request->get('hotel_country');
$hotel_city=$request->get('hotel_city');
$hotel_own_currency=$request->get('hotel_own_currency');
// $season_name=$request->get('season_name'); 
// $booking_validity_from=$request->get('booking_validity_from'); 
// $booking_validity_to=$request->get('booking_validity_to'); 
// $stop_sale=$request->get('stop_sale'); 
// $season_details=array();
// for($season=0;$season<count($season_name);$season++)
// {
//   $season_details[$season]["season_name"]=$season_name[$season];
//   $season_details[$season]["booking_validity_from"]=$booking_validity_from[$season];
//   $season_details[$season]["booking_validity_to"]=$booking_validity_to[$season];
//   $season_details[$season]["stop_sale"]=$stop_sale[$season];
// }
// $season_details=serialize($season_details);
// $activity_nationality=$request->get('activity_nationality'); 
// $activity_markup=$request->get('activity_markup'); 
// $activity_amount=$request->get('activity_amount'); 
// $markup_details=array();
// for($markup=0;$markup<count($activity_nationality);$markup++)
// {
//   $markup_details[$markup]["activity_nationality"]=$activity_nationality[$markup];
//   $markup_details[$markup]["activity_markup"]=$activity_markup[$markup];
//   $markup_details[$markup]["activity_amount"]=$activity_amount[$markup];
// }
// $markup_details=serialize($markup_details);
// $blackout_days=$request->get('blackout_days'); 
// $blackout_dates=serialize($blackout_days);
// $surcharge_name=$request->get('surcharge_name'); 
// $surcharge_day=$request->get('surcharge_day'); 
// $surcharge_price=$request->get('surcharge_price'); 
// $surcharge_details=array();
// for($surcharge=0;$surcharge<count($surcharge_name);$surcharge++)
// {
//   $surcharge_details[$surcharge]["surcharge_name"]=$surcharge_name[$surcharge];
//   $surcharge_details[$surcharge]["surcharge_day"]=$surcharge_day[$surcharge];
//   $surcharge_details[$surcharge]["surcharge_price"]=$surcharge_price[$surcharge];
// }
// $surcharge_details=serialize($surcharge_details);
// $room_type=$request->get('room_type'); 
// $room_min=$request->get('room_min'); 
// $room_max=$request->get('room_max');
// $room_class=$request->get('room_class'); 
// $room_size=$request->get('room_size'); 
// $room_currency=$request->get('room_currency'); 
// $room_adult=$request->get('room_adult'); 
// $room_cwb=$request->get('room_cwb'); 
// $room_cnb=$request->get('room_cnb'); 
// $room_weekend=$request->get('room_weekend'); 
// $room_qty=$request->get('room_qty');
// $room_extra_bed=$request->get('room_extra_bed');
// $room_meal=$request->get('room_meal');
// $room_night=$request->get('room_night');
// $room_checkin=$request->get('room_checkin'); 
// $room_checkout=$request->get('room_checkout');
// $room_amenities=$request->get('room_amenities');    
// $rate_allocation_details=array();
// for($allocation=0;$allocation<count($room_type);$allocation++)
// {
//   $rate_allocation_details[$allocation]["room_type"]=$room_type[$allocation];
//   $rate_allocation_details[$allocation]["room_min"]=$room_min[$allocation];
//   $rate_allocation_details[$allocation]["room_max"]=$room_max[$allocation];
//   $rate_allocation_details[$allocation]["room_class"]=$room_class[$allocation];
//   $rate_allocation_details[$allocation]["room_size"]=$room_size[$allocation];
//   $rate_allocation_details[$allocation]["room_currency"]=$room_currency[$allocation];
//   $rate_allocation_details[$allocation]["room_adult"]=$room_adult[$allocation];
//   $rate_allocation_details[$allocation]["room_cwb"]=$room_cwb[$allocation];
//   $rate_allocation_details[$allocation]["room_cnb"]=$room_cnb[$allocation];
//   $rate_allocation_details[$allocation]["room_weekend"]=$room_weekend[$allocation];
//   $rate_allocation_details[$allocation]["room_qty"]=$room_qty[$allocation];
//   $rate_allocation_details[$allocation]["room_extra_bed"]=$room_extra_bed[$allocation];
//   $rate_allocation_details[$allocation]["room_meal"]=$room_meal[$allocation];
//   $rate_allocation_details[$allocation]["room_night"]=$room_night[$allocation];
//   $rate_allocation_details[$allocation]["room_checkin"]=$room_checkin[$allocation];
//   $rate_allocation_details[$allocation]["room_checkout"]=$room_checkout[$allocation];
//   if(!empty($room_amenities[$allocation]))
//   {
//     $rate_allocation_details[$allocation]["room_amenities"]=$room_amenities[$allocation];
//   }
//   else
//   {
//     $rate_allocation_details[$allocation]["room_amenities"]=array();
//   }
  
//   $room_images=array();
//   for($i=0;$i<count($room_type[$allocation]);$i++)
//   {
//     if(!empty($request->get("upload_ativity_already_images1")[$allocation][$i]))
//     {
//      $already_images=$request->get("upload_ativity_already_images1")[$allocation][$i];

//      for($already=0;$already<count($already_images);$already++)
//      {
//        $room_images[$i][]=$request->get("upload_ativity_already_images1")[$allocation][$i][$already];
//      }
//    }
   
//    if($request->hasFile("upload_ativity_images1.".$allocation.".".$i))
//    {
//     $file_array=$request->file("upload_ativity_images1.".$allocation.".".$i);
//     foreach($file_array as $file)
//     {
//       $extension=strtolower($file->getClientOriginalExtension());
//       if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
//       {

//         $image_name=$file->getClientOriginalName();
//         $image_transfer = "room-".time().uniqid()."-".$image_name;
//         $dir1 = 'assets/uploads/hotel_images/';
//         $file->move($dir1, $image_transfer);
//         $room_images[$i][]=$image_transfer;
//       }
//     }
//   }
// }
// $rate_allocation_details[$allocation]["room_images"]=$room_images;
// }
// $rate_allocation_details=serialize($rate_allocation_details);
$hotel_promotion_detail=array();
$hotel_promotion_detail['hotel_promotion']=$request->get('hotel_promotion');
$hotel_promotion_detail['hotel_prom_discount']=$request->get('hotel_prom_discount');
$hotel_promotion_detail['hotel_promotion_from']=$request->get('hotel_promotion_from');
$hotel_promotion_detail['hotel_promotion_to']=$request->get('hotel_promotion_to');
$hotel_promotion_detail['hotel_promotion_disc_booking']=$request->get('hotel_promotion_disc_booking');
$hotel_promotion_detail['hotel_promotion_disc_travel']=$request->get('hotel_promotion_disc_travel');
$hotel_promotion_detail=serialize($hotel_promotion_detail);
$hotel_addon_name=$request->get('hotel_addon_name'); 
$hotel_desc=$request->get('hotel_desc'); 
$hotel_adult_cost=$request->get('hotel_adult_cost'); 
$hotel_child_cost=$request->get('hotel_child_cost'); 
$hotel_currency=$request->get('hotel_currency'); 
$addon_details=array();
for($addon=0;$addon<count($hotel_addon_name);$addon++)
{
  $addon_details[$addon]["hotel_addon_name"]=$hotel_addon_name[$addon];
  $addon_details[$addon]["hotel_desc"]=$hotel_desc[$addon];
  $addon_details[$addon]["hotel_adult_cost"]=$hotel_adult_cost[$addon];
  $addon_details[$addon]["hotel_child_cost"]=$hotel_child_cost[$addon];
  $addon_details[$addon]["hotel_currency"]=$hotel_currency[$addon];
}
$addon_details=serialize($addon_details);

$reason_name=$request->get('reason_name'); 
$reason_details=array();
for($reason=0;$reason<count($reason_name);$reason++)
{
  $reason_details[$reason]["reason_name"]=$reason_name[$reason];
}
$reason_details=serialize($reason_details);

$policy_name=$request->get('policy_name'); 
$policy_desc=$request->get('policy_desc'); 
$policy_details=array();
for($policy=0;$policy<count($policy_name);$policy++)
{
  $policy_details[$policy]["policy_name"]=$policy_name[$policy];
  $policy_details[$policy]["policy_desc"]=$policy_desc[$policy];
}
$policy_details=serialize($policy_details);
$booking_validity_from=$request->get('validity_operation_from');
$booking_validity_to=$request->get('validity_operation_to'); 
$hotel_inclusions=$request->get('hotel_inclusions'); 
$hotel_exclusions=$request->get('hotel_exclusions');
$hotel_cancel_policy=$request->get('hotel_cancellation'); 
$hotel_terms_conditions=$request->get('hotel_terms_conditions');
if(!empty($request->get('upload_ativity_already_images')))
{
  $hotel_images=$request->get('upload_ativity_already_images');
}
else
{
  $hotel_images=array();
} 

        //multifile uploading
if($request->hasFile('upload_ativity_images'))
{
 foreach($request->file('upload_ativity_images') as $file)
 {
  $extension=strtolower($file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
  {
    $image_name=$file->getClientOriginalName();
    $image_hotel = "hotel-".time()."-".$image_name;
    // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
    $dir1 = 'assets/uploads/hotel_images/';
    $file->move($dir1, $image_hotel);
    $hotel_images[]=$image_hotel;
  }
}
}
$hotel_images=serialize($hotel_images);
$hotel_amenities=$request->get('amenities');
$hotel_amenities=serialize($hotel_amenities);

$update_hotel_data=array(
  "hotel_type"=>$hotel_type,
  "hotel_name"=>$hotel_name,
  "supplier_id"=>$supplier_id,
  "hotel_contact"=>$hotel_contact,
  "hotel_rating"=>$hotel_rating,
  "hotel_address"=>$hotel_address,
  "hotel_description"=>$hotel_description,
  "hotel_currency"=>$hotel_own_currency,
  "hotel_country"=>$hotel_country,
  "hotel_city"=>$hotel_city,
  // "hotel_season_details"=>$season_details,
  // "hotel_markup_details"=>$markup_details,
  // "hotel_blackout_dates"=>$blackout_dates,
  // "hotel_surcharge_details"=>$surcharge_details,
  // "rate_allocation_details"=>$rate_allocation_details,
  "hotel_promotion_details"=>$hotel_promotion_detail,
  "hotel_add_ons"=>$addon_details,
  "hotel_reasons"=>$reason_details,
  "hotel_other_policies"=>$policy_details,
  "booking_validity_from"=>$booking_validity_from,
  "booking_validity_to"=>$booking_validity_to,
  "hotel_amenities"=>$hotel_amenities,
  "hotel_inclusions"=>$hotel_inclusions,
  "hotel_exclusions"=>$hotel_exclusions,
  "hotel_cancel_policy"=>$hotel_cancel_policy,
  "hotel_terms_conditions"=>$hotel_terms_conditions,
  "hotel_images"=>$hotel_images);

$update_hotel_log_data=array(
  "hotel_id"=>$hotel_id,
  "hotel_type"=>$hotel_type,
  "hotel_name"=>$hotel_name,
  "supplier_id"=>$supplier_id,
  "hotel_contact"=>$hotel_contact,
  "hotel_rating"=>$hotel_rating,
  "hotel_address"=>$hotel_address,
  "hotel_description"=>$hotel_description,
  "hotel_country"=>$hotel_country,
  "hotel_city"=>$hotel_city,
  // "hotel_season_details"=>$season_details,
  // "hotel_markup_details"=>$markup_details,
  // "hotel_blackout_dates"=>$blackout_dates,
  // "hotel_surcharge_details"=>$surcharge_details,
  // "rate_allocation_details"=>$rate_allocation_details,
  "hotel_promotion_details"=>$hotel_promotion_detail,
  "hotel_add_ons"=>$addon_details,
  "hotel_reasons"=>$reason_details,
  "hotel_other_policies"=>$policy_details,
  "booking_validity_from"=>$booking_validity_from,
  "booking_validity_to"=>$booking_validity_to,
  "hotel_amenities"=>$hotel_amenities,
  "hotel_inclusions"=>$hotel_inclusions,
  "hotel_exclusions"=>$hotel_exclusions,
  "hotel_cancel_policy"=>$hotel_cancel_policy,
  "hotel_terms_conditions"=>$hotel_terms_conditions,
  "hotel_images"=>$hotel_images,
  "hotel_created_by"=>$hotel_created_by,
  "hotel_create_role"=>$user_role,
  "hotel_operation_performed"=>'UPDATE');
$update_hotel=Hotels::where("hotel_id",$hotel_id)->update($update_hotel_data);
$insert_hotel_log=Hotels_log::insert($update_hotel_log_data);
if($update_hotel)
{
  $last_insert_id=$hotel_id;

 $room_id=$request->get('room_id');
  $room_type=$request->get('room_type'); 
$room_class=$request->get('room_class'); 
$room_size=$request->get('room_size'); 
$room_currency=$request->get('room_currency'); 
$room_adult=$request->get('room_adult'); 
$room_cwb=$request->get('room_cwb'); 
$room_cnb=$request->get('room_cnb'); 
$room_qty=$request->get('room_qty');
$room_extra_bed=$request->get('room_extra_bed');
$room_meal=$request->get('room_meal');
$room_checkin=$request->get('room_checkin'); 
$room_checkout=$request->get('room_checkout');  
$room_amenities=$request->get('room_amenities');
$season_id=$request->get('season_id'); 
$season_name=$request->get('season_name'); 
$booking_validity_from=$request->get('booking_validity_from'); 
$booking_validity_to=$request->get('booking_validity_to');
$occupancy_id=$request->get('occupancy_id'); 
$room_occupancy=$request->get('room_occupancy'); 
$room_price=$request->get('room_price');   
for($rooms_count=0;$rooms_count<count($room_type);$rooms_count++)
{
  if(!empty($room_id[$rooms_count]))
  {
  $insert_hotel_room=HotelRooms::where('hotel_room_id',$room_id[$rooms_count])->first();
  }
  else
  {
  $insert_hotel_room=new HotelRooms;
  }
  $insert_hotel_room->hotel_id=$last_insert_id;
  $insert_hotel_room->hotel_room_type=$room_type[$rooms_count];
  $insert_hotel_room->hotel_room_class=$room_class[$rooms_count];
  $insert_hotel_room->hotel_room_size=$room_size[$rooms_count];
  $insert_hotel_room->hotel_room_currency=$room_currency[$rooms_count];
  $insert_hotel_room->hotel_room_adults=$room_adult[$rooms_count];
  $insert_hotel_room->hotel_room_cwb=$room_cwb[$rooms_count];
  $insert_hotel_room->hotel_room_cnb=$room_cnb[$rooms_count];
  $insert_hotel_room->hotel_room_no_of_rooms=$room_qty[$rooms_count];
  $insert_hotel_room->hotel_room_extra_bed_price=$room_extra_bed[$rooms_count];
  $insert_hotel_room->hotel_room_meal=$room_meal[$rooms_count];
  $insert_hotel_room->hotel_room_checkin=$room_checkin[$rooms_count];
  $insert_hotel_room->hotel_room_checkout=$room_checkout[$rooms_count];
  $insert_hotel_room->hotel_room_amenities=serialize($room_amenities[$rooms_count]);

  if(!empty($request->get('upload_ativity_already_images1')[$rooms_count]))
{
  $room_images=$request->get('upload_ativity_already_images1')[$rooms_count];
}
else
{
 $room_images=array();
}
 

    if($request->hasFile("upload_ativity_images1.".$rooms_count))
    {
      $file_array=$request->file("upload_ativity_images1.".$rooms_count);
      foreach($file_array as $file)
      {
        $extension=strtolower($file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
        {

          $image_name=$file->getClientOriginalName();
          $image_transfer = "room-".time().uniqid()."-".$image_name;
          $dir1 = 'assets/uploads/hotel_images/';
          $file->move($dir1, $image_transfer);
          $room_images[]=$image_transfer;
        }
      }
    }

 $insert_hotel_room->hotel_room_images=serialize($room_images);

 if(!empty($room_id[$rooms_count]))
  {
  }
  else
  {
     $insert_hotel_room->hotel_room_created_by=$hotel_created_by;
 $insert_hotel_room->hotel_room_create_role=$user_role;
  }
 

 if($insert_hotel_room->save())
 {

  $last_inserted_room_id=$insert_hotel_room->hotel_room_id;

  $room_ids_array[]=$last_inserted_room_id;

  for($season_count=0;$season_count<count($season_name[$rooms_count]);$season_count++)
  {
   if(!empty($season_id[$rooms_count][$season_count]))
  {
  $insert_hotel_season=HotelRoomSeasons::where('hotel_room_season_id',$season_id[$rooms_count][$season_count])->first();
  }
  else
  {
  $insert_hotel_season=new HotelRoomSeasons;
  }
  
   $insert_hotel_season->hotel_id_fk=$last_insert_id;
   $insert_hotel_season->hotel_room_id_fk=$last_inserted_room_id;
   $insert_hotel_season->hotel_room_season_name=$season_name[$rooms_count][$season_count];
   $insert_hotel_season->hotel_room_season_validity_from=$booking_validity_from[$rooms_count][$season_count];
   $insert_hotel_season->hotel_room_season_validity_to=$booking_validity_to[$rooms_count][$season_count];
   $insert_hotel_season->hotel_room_season_number_occupany=count($room_occupancy[$rooms_count][$season_count]);

   if(!empty($season_id[$rooms_count][$season_count]))
   {
   }
   else
   {
    $insert_hotel_season->hotel_room_season_created_by=$hotel_created_by;
    $insert_hotel_season->hotel_room_season_create_role=$user_role;
  }
  

   if($insert_hotel_season->save())
   {
    $last_inserted_season_id=$insert_hotel_season->hotel_room_season_id;

     $season_ids_array[]=$last_inserted_season_id;

     for($occupancy_count=0;$occupancy_count<count($room_occupancy[$rooms_count][$season_count]);$occupancy_count++)
  {

    if(!empty($occupancy_id[$rooms_count][$season_count][$occupancy_count]))
  {
  $insert_hotel_season_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_occupancy_id',$occupancy_id[$rooms_count][$season_count][$occupancy_count])->first();
  }
  else
  {
  $insert_hotel_season_occupancy=new HotelRoomSeasonOccupancy;
  }
   $insert_hotel_season_occupancy->hotel_id_fk=$last_insert_id;
   $insert_hotel_season_occupancy->hotel_room_id_fk=$last_inserted_room_id;
   $insert_hotel_season_occupancy->hotel_room_season_id_fk=$last_inserted_season_id;
   $insert_hotel_season_occupancy->hotel_room_occupancy_qty=$room_occupancy[$rooms_count][$season_count][$occupancy_count];
   $insert_hotel_season_occupancy->hotel_room_occupancy_price=$room_price[$rooms_count][$season_count][$occupancy_count];
   if(!empty($occupancy_id[$rooms_count][$season_count][$occupancy_count]))
  {
  }
  else
  {
   $insert_hotel_season_occupancy->hotel_room_occupancy_created_by=$hotel_created_by;
   $insert_hotel_season_occupancy->hotel_room_occupancy_created_role=$user_role;
  }
  
   $insert_hotel_season_occupancy->save();

   $last_inserted_occupancy_id=$insert_hotel_season_occupancy->hotel_room_occupancy_id;

   $occupancy_ids_array[]=$last_inserted_occupancy_id;
 
  }


   } 
 
  }
 } 
}

 $delete_rooms=HotelRooms::whereNotIn('hotel_room_id',$room_ids_array)->where('hotel_id',$hotel_id)->delete();
  $delete_seasons=HotelRoomSeasons::whereNotIn('hotel_room_season_id',$season_ids_array)->where('hotel_id_fk',$hotel_id)->delete();
   $delete_occupancy=HotelRoomSeasonOccupancy::whereNotIn('hotel_room_occupancy_id',$occupancy_ids_array)->where('hotel_id_fk',$hotel_id)->delete();
  echo "success";
}
else
{
  echo "fail";
}
}
public function hotel_details($hotel_id)
{
 if(session()->has('travel_users_id'))
 {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::orderBy('code','asc')->get();
   $hotel_meal=HotelMeal::get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   $emp_id=session()->get('travel_users_id');
   if(strpos($rights['admin_which'],'view')!==false)
   {
     $get_hotels=Hotels::where('hotel_id',$hotel_id)->first();
   }
   else
   {
    $get_hotels=Hotels::where('hotel_id',$hotel_id)->where('hotel_created_by',$emp_id)->where('hotel_create_role','!=','Supplier')->first();
  }
  if(!empty($get_hotels))
  {
    $supplier_id=$get_hotels->supplier_id;
    $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
    $supplier_countries=$get_supplier_countries->supplier_opr_countries;
    $countries_data=explode(',', $supplier_countries);
    return view('mains.hotel-details-view')->with(compact('countries','currency','cities','suppliers','get_hotels','countries_data','rights','hotel_meal'));
  }
  else
  {
    return redirect()->back();
  }
}
else
{
  return redirect()->route('index');
}
}
public function update_hotel_approval(Request $request)
{
  $id=$request->get('hotel_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="approve")
  {
    $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_approve_status"=>1]);
    if($update_hotel)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="reject")
  {
   $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_approve_status"=>2]);
   if($update_hotel)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_hotel_active_inactive(Request $request)
{
  $id=$request->get('hotel_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_status"=>1]);
    if($update_hotel)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
   $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_status"=>0]);
   if($update_hotel)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_hotel_bestseller(Request $request)
{
  $id=$request->get('hotel_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="bestselleryes")
  {
    $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_best_status"=>1]);
    if($update_hotel)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="bestsellerno")
  {
   $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_best_status"=>0]);
   if($update_hotel)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}

public function update_hotel_popular(Request $request)
{
  $id=$request->get('hotel_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="popularyes")
  {
    $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_popular_status"=>1]);
    if($update_hotel)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="popularno")
  {
   $update_hotel=Hotels::where('hotel_id',$id)->update(["hotel_popular_status"=>0]);
   if($update_hotel)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}

public function sort_hotel(Request $request)
{
  $sort_activity_array=$request->get('new_data');
  $success_array=array();
  for($count=0;$count<count($sort_activity_array);$count++)
  {
    $hotel_id=$sort_activity_array[$count]['hotel_id'];
    $new_order=$sort_activity_array[$count]['new_order'];
    $update_activity=Hotels::where('hotel_id',$hotel_id)->update(["hotel_show_order"=>$new_order]);
    if($update_activity)
    {
      $success_array[]="success";
    }
    else
    {
      $success_array[]="not_success";
    }
  }
  echo json_encode($success_array);
}

public function create_guide(Request $request)
{
  if(session()->has('travel_users_id'))
  {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $languages=Languages::get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   return view('mains.admin-create-guide')->with(compact('suppliers','rights','countries','languages'));
 }
 else
 {
  return redirect()->route('index');
}
}
public function insert_guide(Request $request)
{

  $guide_created_by=session()->get('travel_users_id');
  $guide_first_name=$request->get('guide_first_name');
  $guide_last_name=$request->get('guide_last_name');
  $guide_contact=$request->get('contact_number');
  $guide_address=$request->get('address');
  $check_guides=Guides::where('guide_contact',$guide_contact)->get();
  if(count($check_guides)>0)
  {
    echo "exist";
  }
  else
  {
    $guide_supplier_id=$request->get('guide_supplier_name');
    $guide_country=$request->get('guide_country');
    $guide_city=$request->get('guide_city');
    $guide_language=$request->get('guide_language');
    $guide_language=implode(',',$guide_language);
    $guide_price_per_day=$request->get('guide_price_per_day');
     $guide_food_cost=$request->get('guide_food_cost');
      $guide_hotel_cost=$request->get('guide_hotel_cost');
    $guide_description=$request->get('description');
    $week_monday=$request->get('week_monday');
    $week_tuesday=$request->get('week_tuesday');
    $week_wednesday=$request->get('week_wednesday');
    $week_thursday=$request->get('week_thursday');
    $week_friday=$request->get('week_friday');
    $week_saturday=$request->get('week_saturday');
    $week_sunday=$request->get('week_sunday');
    $operating_weekdays=array("monday"=>$week_monday,
      "tuesday"=>$week_tuesday,
      "wednesday"=>$week_wednesday,
      "thursday"=>$week_thursday,
      "friday"=>$week_friday,
      "saturday"=>$week_saturday,
      "sunday"=>$week_sunday);
    $operating_weekdays=serialize($operating_weekdays);
    $guide_blackout_dates=$request->get('blackout_days');
    $guide_nationality=$request->get('guide_nationality');
    $guide_markup=$request->get('guide_markup');
    $guide_amount=$request->get('guide_amount');
    $nationality_markup_details=array();
    for($nation_count=0;$nation_count<count($guide_nationality);$nation_count++)
    {
      $nationality_markup_details[$nation_count]['guide_nationality']=$guide_nationality[$nation_count];
      $nationality_markup_details[$nation_count]['guide_markup']=$guide_markup[$nation_count];
      $nationality_markup_details[$nation_count]['guide_amount']=$guide_amount[$nation_count];
    }
    $nationality_markup_details=serialize($nationality_markup_details);
    $guide_validity_from=$request->get('guide_validity_from');
    $guide_validity_to=$request->get('guide_validity_to');
    $guide_tourname=$request->get('guide_tourname');  
    $guide_cost_four=$request->get('guide_cost_four');
    $guide_cost_seven=$request->get('guide_cost_seven');
    $guide_cost_twenty=$request->get('guide_cost_twenty');
    $guide_duration=$request->get('guide_duration');
    $guide_tariff=array();
    for($transport_count=0;$transport_count<count($guide_tourname);$transport_count++)
    {
      $guide_tariff[$transport_count]['guide_validity_from']=$guide_validity_from[$transport_count];
      $guide_tariff[$transport_count]['guide_validity_to']=$guide_validity_to[$transport_count];
      $guide_tariff[$transport_count]['guide_tourname']=$guide_tourname[$transport_count];
      $guide_tariff[$transport_count]['guide_cost_four']=$guide_cost_four[$transport_count];
      $guide_tariff[$transport_count]['guide_cost_seven']=$guide_cost_seven[$transport_count];
      $guide_tariff[$transport_count]['guide_cost_twenty']=$guide_cost_twenty[$transport_count];
      $guide_tariff[$transport_count]['guide_duration']=$guide_duration[$transport_count];
    }
    $guide_tariff=serialize($guide_tariff);
    if($request->has('tour_name'))
    {
      $tour_name=$request->get('tour_name');
      $tour_vehiclename=$request->get('tour_vehiclename');
      $tour_guide_cost=$request->get('tour_guide_cost');
      $guide_tours_cost=array();
      for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
      {
        $guide_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
        $guide_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
        $guide_tours_cost[$cost_count]["tour_guide_cost"]=$tour_guide_cost[($tour_name[$cost_count]-1)];

      }

    }
    else
    {
      $guide_tours_cost=array();
    }

    $guide_tours_cost=serialize($guide_tours_cost);


     if($request->has('airport_name'))
    {
      $airport_name=$request->get('airport_name');
      $airport_vehicle_name=$request->get('airport_vehicle_name');
      $airport_guide_inside_cost=$request->get('airport_guide_inside_cost');
       $airport_guide_outside_cost=$request->get('airport_guide_outside_cost');
      $guide_airports_cost=array();
      for($cost_count=0;$cost_count<count($airport_name);$cost_count++)
      {
        $guide_airports_cost[$cost_count]["airport_name"]=$airport_name[$cost_count];
        $guide_airports_cost[$cost_count]["airport_vehicle_name"]=$airport_vehicle_name[($airport_name[$cost_count]-1)];
        $guide_airports_cost[$cost_count]["airport_guide_inside_cost"]=$airport_guide_inside_cost[($airport_name[$cost_count]-1)];
         $guide_airports_cost[$cost_count]["airport_guide_outside_cost"]=$airport_guide_outside_cost[($airport_name[$cost_count]-1)];

      }

    }
    else
    {
      $guide_airports_cost=array();
    }

    $guide_airports_cost=serialize($guide_airports_cost);
    $guide_logo_file=$request->get('guide_logo_file');
    if($request->hasFile('guide_logo_file'))
    {
      $guide_logo_file=$request->file('guide_logo_file');
      $extension=strtolower($request->guide_logo_file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
      {
       $image_name=$guide_logo_file->getClientOriginalName();
       $guide_image = "guide-".time()."-".$image_name;
                  // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
       $dir1 = 'assets/uploads/guide_images/';
       $request->file('guide_logo_file')->move($dir1, $guide_image);
     }
     else
     {
      $guide_image = "";
    }
  }
  else
  {
    $guide_image = "";
  }
  $guide_inclusions=$request->get('guide_inclusions');
  $guide_exclusions=$request->get('guide_exclusions');
  $guide_cancel_policy=$request->get('guide_cancellation');
  $guide_terms_conditions=$request->get('guide_terms_conditions');
  $guide=new Guides;
  $guide->guide_first_name=$guide_first_name;
  $guide->guide_last_name=$guide_last_name;
  $guide->guide_contact=$guide_contact;
  $guide->guide_address=$guide_address;
  $guide->guide_supplier_id=$guide_supplier_id;
  $guide->guide_country=$guide_country;
  $guide->guide_city=$guide_city;
  $guide->guide_tours_cost=$guide_tours_cost;
  $guide->guide_airports_cost=$guide_airports_cost;
  $guide->guide_language=$guide_language;
  $guide->guide_price_per_day=$guide_price_per_day;
  $guide->guide_description=$guide_description;
  $guide->operating_weekdays=$operating_weekdays;
  $guide->guide_blackout_dates=$guide_blackout_dates;
  $guide->nationality_markup_details=$nationality_markup_details;
  $guide->guide_tariff=$guide_tariff;
  $guide->guide_food_cost=$guide_food_cost;
  $guide->guide_hotel_cost=$guide_hotel_cost;
  $guide->guide_inclusions=$guide_inclusions;
  $guide->guide_exclusions=$guide_exclusions;
  $guide->guide_cancel_policy=$guide_cancel_policy;
  $guide->guide_terms_conditions=$guide_terms_conditions;
  $guide->guide_image=$guide_image;
  $guide->guide_created_by=$guide_created_by;
  if(session()->get('travel_users_role')=="Admin")
  {
   $guide->guide_role="Admin";
 }
 else
 {
  $guide->guide_role="Sub-User";
}
if($guide->save())
{
  $last_id=$guide->id;
  $guide_log=new Guides_log;
  $guide_log->guide_id=$last_id;
  $guide_log->guide_first_name=$guide_first_name;
  $guide_log->guide_last_name=$guide_last_name;
  $guide_log->guide_contact=$guide_contact;
  $guide_log->guide_address=$guide_address;
  $guide_log->guide_supplier_id=$guide_supplier_id;
  $guide_log->guide_country=$guide_country;
  $guide_log->guide_city=$guide_city;
  $guide_log->guide_tours_cost=$guide_tours_cost;
  $guide_log->guide_airports_cost=$guide_airports_cost;
  $guide_log->guide_language=$guide_language;
  $guide_log->guide_price_per_day=$guide_price_per_day;
  $guide_log->guide_description=$guide_description;
  $guide_log->operating_weekdays=$operating_weekdays;
  $guide_log->guide_blackout_dates=$guide_blackout_dates;
  $guide_log->nationality_markup_details=$nationality_markup_details;
  $guide_log->guide_tariff=$guide_tariff;
  $guide_log->guide_food_cost=$guide_food_cost;
  $guide_log->guide_hotel_cost=$guide_hotel_cost;
  $guide_log->guide_inclusions=$guide_inclusions;
  $guide_log->guide_exclusions=$guide_exclusions;
  $guide_log->guide_cancel_policy=$guide_cancel_policy;
  $guide_log->guide_terms_conditions=$guide_terms_conditions;
  $guide_log->guide_image=$guide_image;
  $guide_log->guide_created_by=$guide_created_by;
  if(session()->get('travel_users_role')=="Admin")
  {
   $guide_log->guide_role="Admin";
 }
 else
 {
  $guide_log->guide_role="Sub-User";
}
$guide_log->guide_operation_performed="INSERT";
$guide_log->save();
echo "success";
}
else
{
  echo "fail";
}
}
}
public function edit_guide($guide_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $languages=Languages::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_guides=Guides::where('guide_id',$guide_id)->first();
    }
    else
    {
      $get_guides=Guides::where('guide_id',$guide_id)->where('guide_created_by',$emp_id)->where('guide_role',"!=","Supplier")->first();
    }

    if(!empty($get_guides))
    {
      $supplier_id=$get_guides->guide_supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $suppliers=Suppliers::where('supplier_status',1)->get();
      $city_id=$get_guides->guide_city;
      // $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->where(function($query) use( $city_id){
      //   $query->where('sightseeing_city_from', $city_id)->orWhereRaw('FIND_IN_SET(?,sightseeing_city_between)', [ $city_id])->orWhere('sightseeing_city_to', $city_id);
      // })->get();
      $get_cities=Cities::where('id',$city_id)->first();

      if($get_cities->name=="Tbilisi")
      {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->get();
     }
     else if($get_cities->name=="Kutaisi" || $get_cities->name=="Batumi")
     {
      $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->where(function($query) use( $city_id){
        $query->where('sightseeing_city_from', $city_id)->orWhere('sightseeing_city_to', $city_id);
      })->get();
     }
     else
     {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->where(function($query) use( $city_id){
        $query->where('sightseeing_city_from', $city_id)->where('sightseeing_city_to', $city_id);
      })->get();

     }

     $fetch_airports=AirportMaster::get();
       

      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_guides->guide_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.admin-edit-guide')->with(compact('languages','countries','cities','get_supplier_countries','get_guides','suppliers',"countries_data",'fetch_vehicle_type','rights','fetch_sightseeing','fetch_airports'));
    }
    else
    {
      return redirect()->back();
    }


  }
  else
  {
    return redirect()->route('index');
  }
}
public function update_guide(Request $request)
{
        // echo "<pre>";
          // print_r($request->all());
          // die();
  $guide_id=$request->get('guide_id');
  $guide_created_by=session()->get('travel_users_id');
  $guide_first_name=$request->get('guide_first_name');
  $guide_last_name=$request->get('guide_last_name');
  $guide_contact=$request->get('contact_number');
  $guide_address=$request->get('address');
  $check_guides=Guides::where('guide_contact',$guide_contact)->where('guide_id','!=',$guide_id)->get();
  if(count($check_guides)>0)
  {

    echo "exist";
  }
  else
  {
   $guide_image_get=Guides::where('guide_id',$guide_id)->first();
   $logo_data=$guide_image_get['guide_image'];
   $guide_supplier_id=$request->get('guide_supplier_name');
   $guide_country=$request->get('guide_country');
   $guide_city=$request->get('guide_city');
   $guide_language=$request->get('guide_language');
   $guide_language=implode(',',$guide_language);
   $guide_price_per_day=$request->get('guide_price_per_day');
    $guide_food_cost=$request->get('guide_food_cost');
      $guide_hotel_cost=$request->get('guide_hotel_cost');
   $guide_description=$request->get('description');
   $week_monday=$request->get('week_monday');
   $week_tuesday=$request->get('week_tuesday');
   $week_wednesday=$request->get('week_wednesday');
   $week_thursday=$request->get('week_thursday');
   $week_friday=$request->get('week_friday');
   $week_saturday=$request->get('week_saturday');
   $week_sunday=$request->get('week_sunday');
   $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);
   $operating_weekdays=serialize($operating_weekdays);
   $guide_blackout_dates=$request->get('blackout_days');
   $guide_nationality=$request->get('guide_nationality');
   $guide_markup=$request->get('guide_markup');
   $guide_amount=$request->get('guide_amount');
   $nationality_markup_details=array();
   for($nation_count=0;$nation_count<count($guide_nationality);$nation_count++)
   {
    $nationality_markup_details[$nation_count]['guide_nationality']=$guide_nationality[$nation_count];
    $nationality_markup_details[$nation_count]['guide_markup']=$guide_markup[$nation_count];
    $nationality_markup_details[$nation_count]['guide_amount']=$guide_amount[$nation_count];
  }
  $nationality_markup_details=serialize($nationality_markup_details);
  $guide_validity_from=$request->get('guide_validity_from');
  $guide_validity_to=$request->get('guide_validity_to');
  $guide_tourname=$request->get('guide_tourname');  
  $guide_cost_four=$request->get('guide_cost_four');
  $guide_cost_seven=$request->get('guide_cost_seven');
  $guide_cost_twenty=$request->get('guide_cost_twenty');
  $guide_duration=$request->get('guide_duration');
  $guide_tariff=array();
  for($transport_count=0;$transport_count<count($guide_tourname);$transport_count++)
  {
    $guide_tariff[$transport_count]['guide_validity_from']=$guide_validity_from[$transport_count];
    $guide_tariff[$transport_count]['guide_validity_to']=$guide_validity_to[$transport_count];
    $guide_tariff[$transport_count]['guide_tourname']=$guide_tourname[$transport_count];
    $guide_tariff[$transport_count]['guide_cost_four']=$guide_cost_four[$transport_count];
    $guide_tariff[$transport_count]['guide_cost_seven']=$guide_cost_seven[$transport_count];
    $guide_tariff[$transport_count]['guide_cost_twenty']=$guide_cost_twenty[$transport_count];
    $guide_tariff[$transport_count]['guide_duration']=$guide_duration[$transport_count];
  }
  $guide_tariff=serialize($guide_tariff);
    //         if($request->has('tour_name'))
    //         {
    //             $tour_name=$request->get('tour_name');
    //         $tour_guide_cost=$request->get('tour_guide_cost');
    // $guide_tours_cost="";
    //         for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
    //         {
    //           $guide_tours_cost.=$tour_name[$cost_count]."---".$tour_guide_cost[$cost_count]."///";
    //         }

    //         }
    //         else
    //         {
    //           $guide_tours_cost="";
    //         }
  if($request->has('tour_name'))
  {
    $tour_name=$request->get('tour_name');
    $tour_vehiclename=$request->get('tour_vehiclename');
    $tour_guide_cost=$request->get('tour_guide_cost');
    $guide_tours_cost=array();
    for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
    {
     $guide_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
     $guide_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
     $guide_tours_cost[$cost_count]["tour_guide_cost"]=$tour_guide_cost[($tour_name[$cost_count]-1)];
   }

 }
 else
 {
  $guide_tours_cost=array();
}

$guide_tours_cost=serialize($guide_tours_cost);

  if($request->has('airport_name'))
    {
       $airport_name=$request->get('airport_name');
      $airport_vehicle_name=$request->get('airport_vehicle_name');
      $airport_guide_inside_cost=$request->get('airport_guide_inside_cost');
       $airport_guide_outside_cost=$request->get('airport_guide_outside_cost');
      $guide_airports_cost=array();
      for($cost_count=0;$cost_count<count($airport_name);$cost_count++)
      {
        $guide_airports_cost[$cost_count]["airport_name"]=$airport_name[$cost_count];
        $guide_airports_cost[$cost_count]["airport_vehicle_name"]=$airport_vehicle_name[($airport_name[$cost_count]-1)];
        $guide_airports_cost[$cost_count]["airport_guide_inside_cost"]=$airport_guide_inside_cost[($airport_name[$cost_count]-1)];
         $guide_airports_cost[$cost_count]["airport_guide_outside_cost"]=$airport_guide_outside_cost[($airport_name[$cost_count]-1)];

      }

    }
    else
    {
      $guide_airports_cost=array();
    }

    $guide_airports_cost=serialize($guide_airports_cost);

$guide_logo_file=$request->get('guide_logo_file');
if($request->hasFile('guide_logo_file'))
{
  $guide_logo_file=$request->file('guide_logo_file');
  $extension=strtolower($request->guide_logo_file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
  {
   $image_name=$guide_logo_file->getClientOriginalName();
   $guide_image = "guide-".time()."-".$image_name;
                    // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
   $dir1 = 'assets/uploads/guide_images/';
   $request->file('guide_logo_file')->move($dir1, $guide_image);
 }
 else
 {
  $guide_image = "";
}
}
else
{
  $guide_image = $logo_data;
}
$guide_inclusions=$request->get('guide_inclusions');
$guide_exclusions=$request->get('guide_exclusions');
$guide_cancel_policy=$request->get('guide_cancellation');
$guide_terms_conditions=$request->get('guide_terms_conditions');

$update_array=array("guide_first_name"=>$guide_first_name,
  "guide_last_name"=>$guide_last_name,
  "guide_contact"=>$guide_contact,
  "guide_address"=>$guide_address,
  "guide_supplier_id"=>$guide_supplier_id,
  "guide_country"=>$guide_country,
  "guide_city"=>$guide_city,
  "guide_tours_cost"=>$guide_tours_cost,
  "guide_airports_cost"=>$guide_airports_cost,
  "guide_language"=>$guide_language,
  "guide_price_per_day"=>$guide_price_per_day,
  "guide_description"=>$guide_description,
  "operating_weekdays"=>$operating_weekdays,
  "guide_blackout_dates"=>$guide_blackout_dates,
  "nationality_markup_details"=>$nationality_markup_details,
  "guide_tariff"=>$guide_tariff,
  "guide_food_cost"=>$guide_food_cost,
  "guide_hotel_cost"=>$guide_hotel_cost,
  "guide_inclusions"=>$guide_inclusions,
  "guide_exclusions"=>$guide_exclusions,
  "guide_cancel_policy"=>$guide_cancel_policy,
  "guide_terms_conditions"=>$guide_terms_conditions,
  "guide_image"=>$guide_image);
$update_guide=Guides::where('guide_id',$guide_id)->update($update_array);
if($update_guide)
{
  $guide_log=new Guides_log;
  $guide_log->guide_id=$guide_id;
  $guide_log->guide_first_name=$guide_first_name;
  $guide_log->guide_last_name=$guide_last_name;
  $guide_log->guide_contact=$guide_contact;
  $guide_log->guide_address=$guide_address;
  $guide_log->guide_supplier_id=$guide_supplier_id;
  $guide_log->guide_country=$guide_country;
  $guide_log->guide_city=$guide_city;
  $guide_log->guide_tours_cost=$guide_tours_cost;
  $guide_log->guide_airports_cost=$guide_airports_cost;
  $guide_log->guide_language=$guide_language;
  $guide_log->guide_price_per_day=$guide_price_per_day;
  $guide_log->guide_description=$guide_description;
  $guide_log->operating_weekdays=$operating_weekdays;
  $guide_log->guide_blackout_dates=$guide_blackout_dates;
  $guide_log->nationality_markup_details=$nationality_markup_details;
  $guide_log->guide_tariff=$guide_tariff;
  $guide_log->guide_food_cost=$guide_food_cost;
  $guide_log->guide_hotel_cost=$guide_hotel_cost;
  $guide_log->guide_inclusions=$guide_inclusions;
  $guide_log->guide_exclusions=$guide_exclusions;
  $guide_log->guide_cancel_policy=$guide_cancel_policy;
  $guide_log->guide_terms_conditions=$guide_terms_conditions;
  $guide_log->guide_image=$guide_image;
  $guide_log->guide_created_by=$guide_created_by;
  if(session()->get('travel_users_role')=="Admin")
  {
   $guide_log->guide_role="Admin";
 }
 else
 {
  $guide_log->guide_role="Sub-User";
}

$guide_log->guide_operation_performed="UPDATE";
$guide_log->save();
echo "success";
}
else
{
  echo "fail";
}
}
}
public function guide_details($guide_id)
{
 if(session()->has('travel_users_id'))
 {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $languages=Languages::get();
   $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
   $emp_id=session()->get('travel_users_id');
   if(strpos($rights['admin_which'],'view')!==false)
   {
    $get_guides=Guides::where('guide_id',$guide_id)->first();
  }
  else
  {
    $get_guides=Guides::where('guide_id',$guide_id)->where('guide_created_by',$emp_id)->where('guide_role',"!=","Supplier")->first();
  }
  if(!empty($get_guides))
  {
   $supplier_id=$get_guides->guide_supplier_id;
   $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();
   $supplier_countries=$get_supplier_countries->supplier_opr_countries;
   $countries_data=explode(',', $supplier_countries);
   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_guides->guide_country)->select("cities.*")->orderBy('cities.name','asc')->get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   return view('mains.admin-guide-details-view')->with(compact('suppliers','countries','cities','get_supplier_countries','get_guides',"countries_data","rights","languages","fetch_vehicle_type"))->with('guide_id',$guide_id);
 }
 else
 {
  return redirect()->back();
}

}
else
{
  return redirect()->route('index');
}
}
public function update_guide_active_inactive(Request $request)
{
  $id=$request->get('guide_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_guide=Guides::where('guide_id',$id)->update(["guide_status"=>1]);
    if($update_guide)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
   $update_guide=Guides::where('guide_id',$id)->update(["guide_status"=>0]);
   if($update_guide)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_guide_approval(Request $request)
{
  $id=$request->get('guide_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="approve")
  {
    $update_guide=Guides::where('guide_id',$id)->update(["guide_approve_status"=>1]);
    if($update_guide)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="reject")
  {
   $update_guide=Guides::where('guide_id',$id)->update(["guide_approve_status"=>2]);
   if($update_guide)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_guide_bestseller(Request $request)
{
  $id=$request->get('guide_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="bestselleryes")
  {
    $update_guide=Guides::where('guide_id',$id)->update(["guide_best_status"=>1]);
    if($update_guide)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="bestsellerno")
  {
   $update_guide=Guides::where('guide_id',$id)->update(["guide_best_status"=>0]);
   if($update_guide)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}

public function update_guide_popular(Request $request)
{
  $id=$request->get('guide_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="popularyes")
  {
    $update_guide=Guides::where('guide_id',$id)->update(["guide_popular_status"=>1]);
    if($update_guide)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="popularno")
  {
   $update_guide=Guides::where('guide_id',$id)->update(["guide_popular_status"=>0]);
   if($update_guide)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}

public function sort_guide(Request $request)
{
  $sort_activity_array=$request->get('new_data');
  $success_array=array();
  for($count=0;$count<count($sort_activity_array);$count++)
  {
    $guide_id=$sort_activity_array[$count]['guide_id'];
    $new_order=$sort_activity_array[$count]['new_order'];
    $update_guide=Guides::where('guide_id',$guide_id)->update(["guide_show_order"=>$new_order]);
    if($update_guide)
    {
      $success_array[]="success";
    }
    else
    {
      $success_array[]="not_success";
    }
  }
  echo json_encode($success_array);
}
public function create_sightseeing(Request $request)
{
  if(session()->has('travel_users_id'))
  {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $fetch_tour_type=TourType::where('tour_type_status',1)->orderBy('tour_type_id','desc')->get();
   $fuel_type=FuelType::get();
        // $currency=Currency::get();
        // $suppliers=Suppliers::where('supplier_status',1)->get();
   return view('mains.create-sightseeing')->with(compact('countries','fuel_type','fetch_tour_type','rights'));
 }
 else
 {
  return redirect()->route('index');
}
}
public function insert_sightseeing(Request $request)
{

 $user_id=session()->get('travel_users_id');
 if(session()->get('travel_users_role')=="Admin")
 {
  $user_role='Admin';
}
else
{
 $user_role='Sub-User';
}
$sightseeing_name=$request->get('sightseeing_name');
$sightseeing_tour_type=$request->get('sightseeing_tour_type');
$sightseeing_desc=$request->get('sightseeing_desc');
$sightseeing_cities_covered=$request->get('sightseeing_cities_covered');
$sightseeing_country=$request->get('sightseeing_country');
$sightseeing_city_from=$request->get('sightseeing_city_from');
$sightseeing_city_between=$request->get('sightseeing_city_between');
if(!empty($sightseeing_city_between))
{
  $sightseeing_city_between=implode(",",$sightseeing_city_between);
}
$sightseeing_city_to=$request->get('sightseeing_city_to');
$sightseeing_distance=$request->get('sightseeing_distance');
$sightseeing_duration=$request->get('sightseeing_duration');
$sightseeing_fuel_type=$request->get('sightseeing_fuel_type');
$sightseeing_total_fuel_cost=$request->get('sightseeing_total_fuel_cost');
$sightseeing_food_cost=$request->get('sightseeing_food_cost');
$sightseeing_hotel_cost=$request->get('sightseeing_hotel_cost');
$sightseeing_tour_expense_entrance=$request->get('sightseeing_tour_expense_entrance');
$sightseeing_adult_cost=$request->get('sightseeing_adult_cost');
$sightseeing_child_cost=$request->get('sightseeing_child_cost');
$sightseeing_discount=$request->get('sightseeing_discount');
$sightseeing_guide_cost=$request->get('sightseeing_guide_cost');
$sightseeing_driver_cost=$request->get('sightseeing_driver_cost');
$sightseeing_additional_cost=$request->get('sightseeing_additional_cost');
$sightseeing_group_adult_cost=$request->get('sightseeing_group_adult_cost');
$sightseeing_group_child_cost=$request->get('sightseeing_group_child_cost');

$sightseeing_attractions=$request->get('tour_attractions');
$sightseeing_images=array();
          //multifile uploading
if($request->hasFile('upload_ativity_images'))
{
 foreach($request->file('upload_ativity_images') as $file)
 {
  $extension=strtolower($file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
  {
    $image_name=$file->getClientOriginalName();
    $image_sightseeing = "sightseeing-".time()."-".$image_name;
                    // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
    $dir1 = 'assets/uploads/sightseeing_images/';
    $file->move($dir1, $image_sightseeing);
    $sightseeing_images[]=$image_sightseeing;
  }
}
}
$sightseeing_images=serialize($sightseeing_images);
$insert_sightseeing=new SightSeeing;
$insert_sightseeing->sightseeing_tour_name=$sightseeing_name;
$insert_sightseeing->sightseeing_tour_type=$sightseeing_tour_type;
$insert_sightseeing->sightseeing_tour_desc=$sightseeing_desc;
$insert_sightseeing->sightseeing_country=$sightseeing_country;
$insert_sightseeing->sightseeing_city_covered=$sightseeing_cities_covered;
$insert_sightseeing->sightseeing_city_from=$sightseeing_city_from;
$insert_sightseeing->sightseeing_city_between=$sightseeing_city_between;
$insert_sightseeing->sightseeing_city_to=$sightseeing_city_to;
$insert_sightseeing->sightseeing_distance_covered=$sightseeing_distance;
$insert_sightseeing->sightseeing_duration=$sightseeing_duration;
$insert_sightseeing->sightseeing_fuel_type=$sightseeing_fuel_type;
$insert_sightseeing->sightseeing_total_fuel_cost=$sightseeing_total_fuel_cost;
$insert_sightseeing->sightseeing_food_cost=$sightseeing_food_cost;
$insert_sightseeing->sightseeing_hotel_cost=$sightseeing_hotel_cost;
$insert_sightseeing->sightseeing_total_expense_cost=$sightseeing_tour_expense_entrance;
$insert_sightseeing->sightseeing_adult_cost=$sightseeing_adult_cost;
$insert_sightseeing->sightseeing_child_cost=$sightseeing_child_cost;
$insert_sightseeing->sightseeing_default_guide_price=$sightseeing_guide_cost;
$insert_sightseeing->sightseeing_default_driver_price=$sightseeing_driver_cost;
$insert_sightseeing->sightseeing_group_adult_cost=$sightseeing_group_adult_cost;
$insert_sightseeing->sightseeing_group_child_cost=$sightseeing_group_child_cost;
$insert_sightseeing->sightseeing_additional_cost=$sightseeing_additional_cost;
$insert_sightseeing->sightseeing_discount=$sightseeing_discount;
$insert_sightseeing->sightseeing_attractions=$sightseeing_attractions;
$insert_sightseeing->sightseeing_images=$sightseeing_images;
$insert_sightseeing->sightseeing_created_by=$user_id;
$insert_sightseeing->sightseeing_create_role=$user_role;
$insert_sightseeing->sightseeing_status=1;
if($insert_sightseeing->save())
{
  $last_id=$insert_sightseeing->id;
  $insert_sightseeing_log=new SightSeeing_log;
  $insert_sightseeing_log->sightseeing_id=$last_id;
  $insert_sightseeing_log->sightseeing_tour_name=$sightseeing_name;
  $insert_sightseeing_log->sightseeing_tour_type=$sightseeing_tour_type;
  $insert_sightseeing_log->sightseeing_tour_desc=$sightseeing_desc;
  $insert_sightseeing_log->sightseeing_country=$sightseeing_country;
  $insert_sightseeing_log->sightseeing_city_covered=$sightseeing_cities_covered;
  $insert_sightseeing_log->sightseeing_city_from=$sightseeing_city_from;
  $insert_sightseeing_log->sightseeing_city_between=$sightseeing_city_between;
  $insert_sightseeing_log->sightseeing_city_to=$sightseeing_city_to;
  $insert_sightseeing_log->sightseeing_distance_covered=$sightseeing_distance;
  $insert_sightseeing_log->sightseeing_duration=$sightseeing_duration;
  $insert_sightseeing_log->sightseeing_fuel_type=$sightseeing_fuel_type;
  $insert_sightseeing_log->sightseeing_total_fuel_cost=$sightseeing_total_fuel_cost;
  $insert_sightseeing_log->sightseeing_food_cost=$sightseeing_food_cost;
  $insert_sightseeing_log->sightseeing_hotel_cost=$sightseeing_hotel_cost;
  $insert_sightseeing_log->sightseeing_total_expense_cost=$sightseeing_tour_expense_entrance;
  $insert_sightseeing_log->sightseeing_adult_cost=$sightseeing_adult_cost;
  $insert_sightseeing_log->sightseeing_child_cost=$sightseeing_child_cost;
  $insert_sightseeing_log->sightseeing_default_guide_price=$sightseeing_guide_cost;
  $insert_sightseeing_log->sightseeing_default_driver_price=$sightseeing_driver_cost;
  $insert_sightseeing_log->sightseeing_group_adult_cost=$sightseeing_group_adult_cost;
  $insert_sightseeing_log->sightseeing_group_child_cost=$sightseeing_group_child_cost;
  $insert_sightseeing_log->sightseeing_additional_cost=$sightseeing_additional_cost;
  $insert_sightseeing_log->sightseeing_discount=$sightseeing_discount;
  $insert_sightseeing_log->sightseeing_attractions=$sightseeing_attractions;
  $insert_sightseeing_log->sightseeing_images=$sightseeing_images;
  $insert_sightseeing_log->sightseeing_created_by=$user_id;
  $insert_sightseeing_log->sightseeing_create_role=$user_role;
  $insert_sightseeing_log->sightseeing_status=1;
  $insert_sightseeing_log->sightseeing_operation="INSERT";
  $insert_sightseeing_log->save();
  echo "success";
}
else
{
  echo "fail";
}
}
public function edit_sightseeing($sightseeing_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    $fuel_type=FuelType::get();
    $fetch_tour_type=TourType::where('tour_type_status',1)->orderBy('tour_type_id','desc')->get();
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_sightseeing=SightSeeing::where('sightseeing_id',$sightseeing_id)->first();
    }
    else
    {
      $get_sightseeing=SightSeeing::where('sightseeing_id',$sightseeing_id)->where('sightseeing_created_by',$emp_id)->where('sightseeing_create_role',"!=","Supplier")->first();
    }
    if(!empty($get_sightseeing))
    {
      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_sightseeing->sightseeing_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.edit-sightseeing')->with(compact('countries','cities','fetch_tour_type','get_sightseeing','rights','fuel_type'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}
public function update_sightseeing(Request $request)
{
 $user_id=session()->get('travel_users_id');
 if(session()->get('travel_users_role')=="Admin")
 {
  $user_role='Admin';
}
else
{
 $user_role='Sub-User';
}
$sightseeing_id=$request->get('sightseeing_id');
$sightseeing_name=$request->get('sightseeing_name');
$sightseeing_tour_type=$request->get('sightseeing_tour_type');
$sightseeing_desc=$request->get('sightseeing_desc');
$sightseeing_cities_covered=$request->get('sightseeing_cities_covered');
$sightseeing_country=$request->get('sightseeing_country');
$sightseeing_city_from=$request->get('sightseeing_city_from');
$sightseeing_city_between=$request->get('sightseeing_city_between');
if(!empty($sightseeing_city_between))
{
  $sightseeing_city_between=implode(",",$sightseeing_city_between);
}
$sightseeing_city_to=$request->get('sightseeing_city_to');
$sightseeing_distance=$request->get('sightseeing_distance');
$sightseeing_duration=$request->get('sightseeing_duration');
$sightseeing_fuel_type=$request->get('sightseeing_fuel_type');
$sightseeing_total_fuel_cost=$request->get('sightseeing_total_fuel_cost');
$sightseeing_food_cost=$request->get('sightseeing_food_cost');
$sightseeing_hotel_cost=$request->get('sightseeing_hotel_cost');
$sightseeing_tour_expense_entrance=$request->get('sightseeing_tour_expense_entrance');
$sightseeing_adult_cost=$request->get('sightseeing_adult_cost');
$sightseeing_child_cost=$request->get('sightseeing_child_cost');
$sightseeing_guide_cost=$request->get('sightseeing_guide_cost');
$sightseeing_driver_cost=$request->get('sightseeing_driver_cost');
$sightseeing_additional_cost=$request->get('sightseeing_additional_cost');
$sightseeing_discount=$request->get('sightseeing_discount');
$sightseeing_group_adult_cost=$request->get('sightseeing_group_adult_cost');
$sightseeing_group_child_cost=$request->get('sightseeing_group_child_cost');
$sightseeing_attractions=$request->get('tour_attractions');

if(!empty($request->get('upload_ativity_already_images')))
{
 $sightseeing_images=$request->get('upload_ativity_already_images');
}
else
{
  $sightseeing_images=array();
}

          //multifile uploading
if($request->hasFile('upload_ativity_images'))
{
 foreach($request->file('upload_ativity_images') as $file)
 {
  $extension=strtolower($file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
  {
    $image_name=$file->getClientOriginalName();
    $image_sightseeing = "sightseeing-".time()."-".$image_name;
                    // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
    $dir1 = 'assets/uploads/sightseeing_images/';
    $file->move($dir1, $image_sightseeing);
    $sightseeing_images[]=$image_sightseeing;
  }
}
}
$sightseeing_images=serialize($sightseeing_images);
$update_sightseeing=array(
  "sightseeing_tour_name"=>$sightseeing_name,
  "sightseeing_tour_type"=>$sightseeing_tour_type,
  "sightseeing_tour_desc"=>$sightseeing_desc,
  "sightseeing_country"=>$sightseeing_country,
  "sightseeing_city_covered"=>$sightseeing_cities_covered,
  "sightseeing_city_from"=>$sightseeing_city_from,
  "sightseeing_city_between"=>$sightseeing_city_between,
  "sightseeing_city_to"=>$sightseeing_city_to,
  "sightseeing_distance_covered"=>$sightseeing_distance,
  "sightseeing_duration"=>$sightseeing_duration,
  "sightseeing_fuel_type"=>$sightseeing_fuel_type,
  "sightseeing_total_fuel_cost"=>$sightseeing_total_fuel_cost,
  "sightseeing_food_cost"=>$sightseeing_food_cost,
  "sightseeing_hotel_cost"=>$sightseeing_hotel_cost,
  "sightseeing_total_expense_cost"=>$sightseeing_tour_expense_entrance,
  "sightseeing_adult_cost"=>$sightseeing_adult_cost,
  "sightseeing_child_cost"=>$sightseeing_child_cost,
  "sightseeing_default_guide_price"=>$sightseeing_guide_cost,
  "sightseeing_default_driver_price"=>$sightseeing_driver_cost,
  "sightseeing_group_adult_cost"=>$sightseeing_group_adult_cost,
  "sightseeing_group_child_cost"=>$sightseeing_group_child_cost,
  "sightseeing_additional_cost"=>$sightseeing_additional_cost,
  "sightseeing_discount"=>$sightseeing_discount,
  "sightseeing_attractions"=>$sightseeing_attractions,
  "sightseeing_images"=>$sightseeing_images,
  "sightseeing_status"=>1);
$update_query=SightSeeing::where('sightseeing_id',$sightseeing_id)->update($update_sightseeing);
if($update_query)
{
  $insert_sightseeing_log=new SightSeeing_log;
  $insert_sightseeing_log->sightseeing_id=$sightseeing_id;
  $insert_sightseeing_log->sightseeing_tour_name=$sightseeing_name;
  $insert_sightseeing_log->sightseeing_tour_type=$sightseeing_tour_type;
  $insert_sightseeing_log->sightseeing_tour_desc=$sightseeing_desc;
  $insert_sightseeing_log->sightseeing_country=$sightseeing_country;
  $insert_sightseeing_log->sightseeing_city_covered=$sightseeing_cities_covered;
  $insert_sightseeing_log->sightseeing_city_from=$sightseeing_city_from;
  $insert_sightseeing_log->sightseeing_city_between=$sightseeing_city_between;
  $insert_sightseeing_log->sightseeing_city_to=$sightseeing_city_to;
  $insert_sightseeing_log->sightseeing_distance_covered=$sightseeing_distance;
  $insert_sightseeing_log->sightseeing_duration=$sightseeing_duration;
  $insert_sightseeing_log->sightseeing_fuel_type=$sightseeing_fuel_type;
  $insert_sightseeing_log->sightseeing_total_fuel_cost=$sightseeing_total_fuel_cost;
  $insert_sightseeing_log->sightseeing_food_cost=$sightseeing_food_cost;
  $insert_sightseeing_log->sightseeing_hotel_cost=$sightseeing_hotel_cost;
  $insert_sightseeing_log->sightseeing_total_expense_cost=$sightseeing_tour_expense_entrance;
  $insert_sightseeing_log->sightseeing_adult_cost=$sightseeing_adult_cost;
  $insert_sightseeing_log->sightseeing_child_cost=$sightseeing_child_cost;
  $insert_sightseeing_log->sightseeing_default_guide_price=$sightseeing_guide_cost;
  $insert_sightseeing_log->sightseeing_default_driver_price=$sightseeing_driver_cost;
  $insert_sightseeing_log->sightseeing_group_adult_cost=$sightseeing_group_adult_cost;
  $insert_sightseeing_log->sightseeing_group_child_cost=$sightseeing_group_child_cost;
  $insert_sightseeing_log->sightseeing_additional_cost=$sightseeing_additional_cost;
  $insert_sightseeing_log->sightseeing_discount=$sightseeing_discount;
  $insert_sightseeing_log->sightseeing_attractions=$sightseeing_attractions;
  $insert_sightseeing_log->sightseeing_images=$sightseeing_images;
  $insert_sightseeing_log->sightseeing_created_by=$user_id;
  $insert_sightseeing_log->sightseeing_create_role=$user_role;
  $insert_sightseeing_log->sightseeing_status=1;
  $insert_sightseeing_log->sightseeing_operation="UPDATE";
  $insert_sightseeing_log->save();
  echo "success";
}
else
{
  echo "fail";
}
}
public function sightseeing_details($sightseeing_id)
{
 if(session()->has('travel_users_id'))
 {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $emp_id=session()->get('travel_users_id');
   $fuel_type=FuelType::get();
   if(strpos($rights['admin_which'],'view')!==false)
   {
    $get_sightseeing=SightSeeing::where('sightseeing_id',$sightseeing_id)->first();
  }
  else
  {
   $get_sightseeing=SightSeeing::where('sightseeing_id',$sightseeing_id)->where('sightseeing_created_by',$emp_id)->where('sightseeing_create_role',"!=","Supplier")->first();
 }
 if(!empty($get_sightseeing))
 {
   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_sightseeing->sightseeing_country)->select("cities.*")->orderBy('cities.name','asc')->get();
   return view('mains.sightseeing-details')->with(compact('countries','cities','get_sightseeing',"rights","fuel_type"))->with('sightseeing_id',$sightseeing_id);
 }
 else
 {
  return redirect()->back();
}

}
else
{
  return redirect()->route('index');
}
}
public function update_sightseeing_active_inactive(Request $request)
{
  $id=$request->get('sightseeing_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_sightseeing=SightSeeing::where('sightseeing_id',$id)->update(["sightseeing_status"=>1]);
    if($update_sightseeing)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
   $update_sightseeing=SightSeeing::where('sightseeing_id',$id)->update(["sightseeing_status"=>0]);
   if($update_sightseeing)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}


public function update_sightseeing_bestseller(Request $request)
{
  $id=$request->get('sightseeing_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="bestselleryes")
  {
    $update_sightseeing=SightSeeing::where('sightseeing_id',$id)->update(["sightseeing_best_status"=>1]);
    if($update_sightseeing)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="bestsellerno")
  {
   $update_sightseeing=SightSeeing::where('sightseeing_id',$id)->update(["sightseeing_best_status"=>0]);
   if($update_sightseeing)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}

public function update_sightseeing_popular(Request $request)
{
  $id=$request->get('sightseeing_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="popularyes")
  {
    $update_sightseeing=SightSeeing::where('sightseeing_id',$id)->update(["sightseeing_popular_status"=>1]);
    if($update_sightseeing)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="popularno")
  {
   $update_sightseeing=SightSeeing::where('sightseeing_id',$id)->update(["sightseeing_popular_status"=>0]);
   if($update_sightseeing)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}

public function sort_sightseeing(Request $request)
{
  $sort_activity_array=$request->get('new_data');
  $success_array=array();
  for($count=0;$count<count($sort_activity_array);$count++)
  {
    $sightseeing_id=$sort_activity_array[$count]['sightseeing_id'];
    $new_order=$sort_activity_array[$count]['new_order'];
    $update_sightseeing=Sightseeing::where('sightseeing_id',$sightseeing_id)->update(["sightseeing_show_order"=>$new_order]);
    if($update_sightseeing)
    {
      $success_array[]="success";
    }
    else
    {
      $success_array[]="not_success";
    }
  }
  echo json_encode($success_array);
}

public function create_transfer(Request $request)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",81)->select("cities.*")->orderBy('cities.name','asc')->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $fetch_airports=AirportMaster::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    return view('mains.create-transfer')->with(compact('countries','suppliers','fetch_cities','rights','fetch_airports','fetch_vehicle_type'));
  }
  else
  {
    return redirect()->route('index');
  }
}
public function fetchAirportTransferData(Request $request)
{
  $country_id=$request->get('country_id');
  $fetch_airports=AirportMaster::get();
  $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
  $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->select("cities.*")->orderBy('cities.name','asc')->get();
  // $html="<div class='col-md-12'>
  // <div class='row'>
  // <div class='col-md-3'>
  // <p>Select Airport</p>
  // </div>
  // <div class='col-md-3'>
  // <p>Select City</p>
  // </div>
  // <div class='col-md-5'>
  // <div class='row'>
  // <div class='col-md-8'>
  // <p>Vehicle Type</p>
  // </div>
  // <div class='col-md-4'>
  // <p>Vehicle Cost</p>
  // </div>
  // </div>

  // </div>
  // </div>";

  // $html.="
  // <div class='transfer_div' id='transfer_div1'>
  // <div class='row mt-15 mb-10'>
  // <div class='col-md-3'>
  // <select class='form-control select2' name='transfer_from[]' id='transfer_from__1' style='width:100%'>
  // <option value='0'>--Select Airport--</option>";
  // foreach($fetch_airports as $airports)
  // {
  //   $html.="<option value='".$airports->airport_master_id."'>".$airports->airport_master_name."</option>";
  // }
  // $html.="</select>
  // </div>";

  // $html.="<div class='col-md-3'>
  // <select class='form-control select2' name='transfer_to[]' id='transfer_to__1' style='width:100%'>
  // <option value='0'>--Select City--</option>";
  // foreach($fetch_cities as $cities)
  // {
  //   $html.="<option value='".$cities->id."'>".$cities->name."</option>";
  // }
  // $html.="</select>
  // </div>
  // <div class='col-md-5'>";
  // foreach($fetch_vehicle_type as $vehcile_type)
  // {
  //   $html.=" <div class='row'>
  //   <div class='col-md-8'><input type='hidden'  class='vehicle_type_name' name='transfer_vehiclename[0][]' value='".$vehcile_type->vehicle_type_id."'>
  //   <input type='text' class='form-control' value='".$vehcile_type->vehicle_type_name."' readonly='readonly'>  </div>  <div class='col-md-4'>
  //   <input type='text' class='form-control vehicle_type_cost' name='transfer_vehiclecost[0][]' value='0' onkeypress='javascript:return validateNumber(event)'> </div>
  //   </div>";
  // }
  // $html.="
  // <br></div>
  // </div>
  // <div class='col-sm-12 col-md-12 add_more_transfer_div'>
  // <img id='add_more_transfer1' class='add_more_transfer plus-icon'   style='margin-left: auto;' src='".asset('assets/images/add_icon.png') ."'>
  // </div>
  // ";
  // $html.="</div>";
  // echo $html;


  $country_id=$request->get('country_id');
  $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->where("cities.city_status",1)->select("cities.*")->orderBy('cities.name','asc')->get();
  $html="
  <style>
  .col-md-1.cstm-col {
   flex: 0 0 9.333333%;
   max-width: 9.333333%;
 }
 </style>
 <div class='col-md-12'>
 <div class='row'>
 <div class='col-md-3'>
 <p>Select Airport</p>
 </div>
 <div class='col-md-9'>
 <div class='row'>
 <div class='col-md-3'>
 <p>Select City</p>
 </div>
 <div class='col-md-3'>
 <p>Vehicle Cost</p>
 </div>
 </div>
 </div>
 </div>
 <div class='transfer_div' id='transfer_div1'>
 <div class='row mt-15 mb-10'>
 <div class='col-md-3'>
 <select class='form-control select2 transfer_from' name='transfer_from[]' id='transfer_from__1' style='width:100%'>
 <option value='0'>--Select Airport--</option>";
 foreach($fetch_airports as $airports)
 {
  $html.="<option value='".$airports->airport_master_id."'>".$airports->airport_master_name."</option>";
}
$html.="</select>
</div>
<div class='col-md-9 destination_city_div'>
</div>
</div>
<div class='col-sm-12 col-md-12 add_more_city_transfer_div'>
<img id='add_more_city_transfer1' class='add_more_city_transfer plus-icon'   style='margin-left: auto;' src='".asset('assets/images/add_icon.png') ."'>
</div>
</div>
</div>";
echo $html;
}

public function fetchCityTransferData(Request $request)
{
  $country_id=$request->get('country_id');
  $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
  $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->where("cities.city_status",1)->select("cities.*")->orderBy('cities.name','asc')->get();
  $fetch_vehicle_type_html="";
  // foreach($fetch_vehicle_type as $vehicle_type)
  // {
  //   $fetch_vehicle_type_html.="<div class='col-md-1 cstm-col'> <p>".$vehicle_type->vehicle_type_name."</p></div>";
  // }
  $html="
  <style>
  .col-md-1.cstm-col {
   flex: 0 0 9.333333%;
   max-width: 9.333333%;
 }
 </style>

 <div class='col-md-12'>
 <div class='row'>
 <div class='col-md-2'>
 <p>From City</p>
 </div>
 <div class='col-md-10'>
 <div class='row'>
 <div class='col-md-3'>
 <p>To City</p>
 </div>
 <div class='col-md-3'>
 <p>Vehicle Cost</p>
 </div>
 ".$fetch_vehicle_type_html."
 </div>
 </div>
 </div>
 ";
 $html.="
 <div class='transfer_div' id='transfer_div1'>
 <div class='row mt-15 mb-10'>
 <div class='col-md-2'>
 <select class='form-control select2 transfer_from' name='transfer_from[]' id='transfer_from__1' style='width:100%'>
 <option value='0'>--Select City--</option>";
 foreach($fetch_cities as $cities)
 {
  $html.="<option value='".$cities->id."'>".$cities->name."</option>";
}
$html.="</select>
</div>
<div class='col-md-10 destination_city_div'>
</div>
</div>
<div class='col-sm-12 col-md-12 add_more_city_transfer_div'>
<img id='add_more_city_transfer1' class='add_more_city_transfer plus-icon'   style='margin-left: auto;' src='".asset('assets/images/add_icon.png') ."'>
</div>
</div>

</div>";
  // $html="<div class='col-md-12'>
  // <div class='row'>
  // <div class='col-md-3'>
  // <p>From City</p>
  // </div>
  // <div class='col-md-3'>
  // <p>To City</p>
  // </div>
  // <div class='col-md-5'>
  // <div class='row'>
  // <div class='col-md-8'>
  // <p>Vehicle Type</p>
  // </div>
  // <div class='col-md-4'>
  // <p>Vehicle Cost</p>
  // </div>
  // </div>


  // </div>
  // </div>";

  // $html.="
  // <div class='transfer_div' id='transfer_div1'>
  // <div class='row mt-15 mb-10'>
  // <div class='col-md-3'>
  // <select class='form-control select2' name='transfer_from[]' id='transfer_from__1' style='width:100%'>
  // <option value='0'>--Select City--</option>";
  //  foreach($fetch_cities as $cities)
  // {
  //   $html.="<option value='".$cities->id."'>".$cities->name."</option>";
  // }
  // $html.="</select>
  // </div>";

  // $html.="<div class='col-md-3'>
  // <select class='form-control select2' name='transfer_to[]' id='transfer_to__1' style='width:100%'>
  // <option value='0'>--Select City--</option>";
  // foreach($fetch_cities as $cities)
  // {
  //   $html.="<option value='".$cities->id."'>".$cities->name."</option>";
  // }
  // $html.="</select>
  // </div>
  // <div class='col-md-5'>";
  // foreach($fetch_vehicle_type as $vehcile_type)
  // {
  //   $html.=" <div class='row'>
  //   <div class='col-md-8'><input type='hidden'  class='vehicle_type_name' name='transfer_vehiclename[0][]' value='".$vehcile_type->vehicle_type_id."'>
  //   <input type='text' class='form-control' value='".$vehcile_type->vehicle_type_name."' readonly='readonly'>  </div>  <div class='col-md-4'>
  //   <input type='text' class='form-control vehicle_type_cost' name='transfer_vehiclecost[0][]' value='0' onkeypress='javascript:return validateNumber(event)'> </div>
  //   </div>";
  // }
  // $html.="
  // <br></div>
  // </div>
  // <div class='col-sm-12 col-md-12 add_more_transfer_div'>
  // <img id='add_more_transfer1' class='add_more_transfer plus-icon'   style='margin-left: auto;' src='".asset('assets/images/add_icon.png') ."'>
  // </div>
  // ";
  // $html.="</div>";
echo $html;

}

public function fetchToCityTransferData(Request $request)
{
 $country_id=$request->get('country_id');
 $city_id=$request->get('city_id');
 $index=$request->get('index');
 $index=$index-1;
 $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
 $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->where("cities.city_status",1)->select("cities.*")->orderBy('cities.name','asc')->get();
 $html="";
 $count=0;
 
 foreach($fetch_cities as $cities)
 {
   if($cities->id!=$city_id)
   {
     $html.="<div class='row'>";
     $html.="<div class='col-md-3'>
     <div class='form-group'>
     <input type='hidden' name='transfer_to[".$index."][]' id='transfer_to__".$index."_".$count."' value='".$cities->id."' style='width:100%'>
     <input type='text' class='form-control' name='transfer_to_name[".$index."][]' id='transfer_to_name__".$index."_".$count."' value='".$cities->name."' readonly>
     </div>
     </div>
     <div class='col-md-3'>
     <div class='form-group'>
     <input type='text' class='form-control vehicle_cost' name='vehicle_cost[".$index."][]' id='vehicle_cost__".$index."_".$count."' value='0' style='width:100%' onkeypress='javascript:return validateNumber(event)'>
     </div>
     </div>"; 
    //  foreach($fetch_vehicle_type as $vehicle_type)
    //  {
    //   $html.="
    //   <div class='col-md-1 cstm-col'> <div class='form-group'>
    //   <input type='hidden' class='form-control vehicle_type_name' name='transfer_vehiclename[".$index."][".$count."][]' value='".$vehicle_type->vehicle_type_id."'>
    //   <input type='text' class='form-control vehicle_type_cost' name='transfer_vehiclecost[".$index."][".$count."][]' value='0' onkeypress='javascript:return validateNumber(event)'></div></div>";
    // }
     $html.="</div>";
     $count++;
   }
   

 }
 echo $html;
}

public function fetchToAirportTransferData(Request $request)
{
 $country_id=$request->get('country_id');
 $city_id=$request->get('city_id');
 $index=$request->get('index');
 $index=$index-1;
 $fetch_airports=AirportMaster::get();
 $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
 $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->where("cities.city_status",1)->select("cities.*")->orderBy('cities.name','asc')->get();
 $html="";
 $count=0;

 foreach($fetch_airports as $airports)
 {
   $html.="<div class='row'>";
   $html.="<div class='col-md-3'>
   <div class='form-group'>
   <input type='hidden' name='transfer_to[".$index."][]' id='transfer_to__".$index."_".$count."' value='".$airports->airport_master_id."' style='width:100%'>
   <input type='text' class='form-control' name='transfer_to_name[".$index."][]' id='transfer_to_name__".$index."_".$count."' value='".$airports->airport_master_name."' readonly>
   </div>
   </div>
   <div class='col-md-3'>
   <div class='form-group'>
   <input type='text' class='form-control vehicle_cost' name='vehicle_cost[".$index."][]' id='vehicle_cost__".$index."_".$count."' value='0' style='width:100%' onkeypress='javascript:return validateNumber(event)'>
   </div>
   </div>"; 
   $html.="</div>";
   $count++;
   
 }
 echo $html;
}

public function insert_transfer(Request $request)
{
  // echo "<pre>";
  // print_r($request->all());
  //   die();
  $transfer_role=$request->get('transfer_role');
  if($transfer_role=='supplier')
  {
    $user_id=$request->get('transfer_supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $transfer_name=$request->get('transfer_name');
  $transfer_description=$request->get('transfer_description');
  $supplier_id=$request->get('transfer_supplier_name');
  $transfer_country=$request->get('transfer_country');
  $transfer_type=$request->get('transfer_type');

  $transfer_from=$request->get('transfer_from');
  $transfer_to=$request->get('transfer_to');
  $transfer_vehiclename=$request->get('transfer_vehiclename');
  $transfer_vehiclecost=$request->get('transfer_vehiclecost');

  $transfer_inclusions=$request->get('transfer_inclusions');
  $transfer_exclusions=$request->get('transfer_exclusions');
  $transfer_cancellation=$request->get('transfer_cancellation');
  $transfer_terms_conditions=$request->get('transfer_terms_conditions');
  $transfer_created_by=$user_id;

  $no_of_transfers=count($transfer_from);
  $vehicle_type=$request->get('vehicle_type');
  $vehicle=$request->get('vehicle');
  $vehicle_info=$request->get('vehicle_info');
  $vehicle_cost=$request->get('vehicle_cost');
  $vehicle_note=$request->get('vehicle_note');
  $vehicle_images=$request->file('vehicle_images');  
  
        //multifile uploading
  $vehicle_images_array=array();
  for($i=0;$i<count($vehicle_images);$i++)
  {
    $vehicle_image_index=$vehicle_images[$i];
    if(!empty($vehicle_image_index))
    {

     foreach($vehicle_image_index as $file)
     {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
      {
        $image_name=$file->getClientOriginalName();
        $image_sightseeing = "vehicle-".time()."-".$image_name;
                        // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/vehicle_images/';
        $file->move($dir1, $image_sightseeing);
        $vehicle_images_array[$i][]=$image_sightseeing;
      }
    }
  }

}


$insert_transfer=new Transfers;
$insert_transfer->transfer_name=$transfer_name;
$insert_transfer->transfer_description=$transfer_description;
$insert_transfer->supplier_id=$supplier_id;
$insert_transfer->transfer_country=$transfer_country;
$insert_transfer->transfer_type=$transfer_type;
$insert_transfer->no_of_transfer_available=$no_of_transfers;
$insert_transfer->transfer_vehicle_type=$vehicle_type[0];
$insert_transfer->transfer_vehicle=$vehicle[0];
$insert_transfer->transfer_vehicle_info=$vehicle_info[0];
$insert_transfer->transfer_vehicle_note=$vehicle_note[0];
$insert_transfer->transfer_vehicle_images=serialize($vehicle_images_array);
$insert_transfer->transfer_inclusions=$transfer_inclusions;
$insert_transfer->transfer_exclusions=$transfer_exclusions;
$insert_transfer->transfer_cancellation=$transfer_cancellation;
$insert_transfer->transfer_terms_and_conditions=$transfer_terms_conditions;
$insert_transfer->transfer_created_by=$transfer_created_by;
$insert_transfer->transfer_role=$user_role;
if($user_role=="Supplier")
{
  $insert_transfer->transfer_approve_status=0;
}
if($insert_transfer->save())
{

  $last_id=$insert_transfer->id;
  
  for($transfer_from_count=0;$transfer_from_count < count($transfer_from);$transfer_from_count++)
  {
    for($transfer_count=0;$transfer_count < count($transfer_to[$transfer_from_count]);$transfer_count++)
    {
      $tranfer_vehicle_tariff=array();
      $insert_transfer_details=new TransferDetails;
      $insert_transfer_details->transfer_id=$last_id;
      $insert_transfer_details->transfer_details_type=$transfer_type;
      $insert_transfer_details->from_city_airport=$transfer_from[$transfer_from_count];
      $insert_transfer_details->to_city_airport=$transfer_to[$transfer_from_count][$transfer_count];
      $insert_transfer_details->transfer_vehicle_cost=$vehicle_cost[$transfer_from_count][$transfer_count];
      $insert_transfer_details->transfer_details_created_by=$transfer_created_by;
      $insert_transfer_details->transfer_details_role=$user_role;
      $insert_transfer_details->save();
    }
  }
  
          //  $from_city=$request->get('from_city');
          // $to_city=$request->get('to_city');
  

          //first code (transfer city)

          //       for($transfer_count=0;$transfer_count < count($transfer_to[0]);$transfer_count++)
          // {
          //   $tranfer_vehicle_tariff=array();
          // $insert_transfer_details=new TransferDetails;
          // $insert_transfer_details->transfer_id=$last_id;
          // $insert_transfer_details->transfer_details_type=$transfer_type;
          // $insert_transfer_details->from_city_airport=$transfer_from[0];
          // $insert_transfer_details->to_city_airport=$transfer_to[0][$transfer_count];
          // $tranfer_vehicle_tariff[]=$transfer_vehiclename[0][$transfer_count];
          // $tranfer_vehicle_tariff[]=$transfer_vehiclecost[0][$transfer_count];
          // $insert_transfer_details->transfer_vehicle_tariff=serialize($tranfer_vehicle_tariff);
          // $insert_transfer_details->transfer_details_created_by=$transfer_created_by;
          // $insert_transfer_details->transfer_details_role=$user_role;
          //  $insert_transfer_details->save();
          // }

          //second code (transfer city)
  
          // $tranfer_vehicle_tariff=array();
          // $insert_transfer_details=new TransferDetails;
          // $insert_transfer_details->transfer_id=$last_id;
          // $insert_transfer_details->transfer_details_type=$transfer_type;
          // $insert_transfer_details->from_city_airport=$from_city;
          // $insert_transfer_details->to_city_airport=$to_city;
          // $insert_transfer_details->transfer_vehicle_type=implode(",",$vehicle_type);
          // $insert_transfer_details->transfer_vehicle=implode(",",$vehicle);
          // $insert_transfer_details->transfer_vehicle_info=serialize($vehicle_info);
          // $insert_transfer_details->transfer_vehicle_cost=serialize($vehicle_cost);
          // $insert_transfer_details->transfer_vehicle_images=serialize($vehicle_images_array);
          // $insert_transfer_details->transfer_details_created_by=$transfer_created_by;
          // $insert_transfer_details->transfer_details_role=$user_role;
          //  $insert_transfer_details->save();

  echo "success";
}
else
{
  echo "fail";
}
}

public function edit_transfer($transfer_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    $fetch_vehicles=Vehicles::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_transfer=Transfers::where('transfer_id',$transfer_id)->first();
    }
    else
    {
      $get_transfer=Transfers::where('transfer_id',$transfer_id)->where('transfer_created_by',$emp_id)->where('transfer_role','!=','Supplier')->first();
    }

    if(!empty($get_transfer))
    {
      $get_transfer_details=TransferDetails::where('transfer_id',$transfer_id)->get();
      $fetch_airports=AirportMaster::get();
      $supplier_id=$get_transfer->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_transfer->transfer_country)->where('city_status',1)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.edit-transfer')->with(compact('countries','suppliers','fetch_cities','suppliers','get_transfer','countries_data','get_transfer_details','rights','fetch_airports','fetch_vehicle_type','fetch_vehicles'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}
public function update_transfer(Request $request)
{
  // echo "<pre>";
  // print_r($request->all());
  //   die();
  $transfer_id=$request->get('transfer_id');
  $transfer_role=$request->get('transfer_role');
  if($transfer_role=='supplier')
  {
    $user_id=$request->get('transfer_supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $transfer_name=$request->get('transfer_name');
  $transfer_description=$request->get('transfer_description');
  $supplier_id=$request->get('transfer_supplier_name');
  $transfer_country=$request->get('transfer_country');
  $transfer_type=$request->get('transfer_type');

  $transfer_from=$request->get('transfer_from');
  $transfer_to=$request->get('transfer_to');
  $transfer_vehiclename=$request->get('transfer_vehiclename');
  $transfer_vehiclecost=$request->get('transfer_vehiclecost');

  $transfer_inclusions=$request->get('transfer_inclusions');
  $transfer_exclusions=$request->get('transfer_exclusions');
  $transfer_cancellation=$request->get('transfer_cancellation');
  $transfer_terms_conditions=$request->get('transfer_terms_conditions');
  $transfer_created_by=$user_id;

  $no_of_transfers=count($transfer_from);
  $change_occured=$request->get('change_occured');

  $vehicle_type=$request->get('vehicle_type');
  $vehicle=$request->get('vehicle');
  $vehicle_info=$request->get('vehicle_info');
  $vehicle_cost=$request->get('vehicle_cost');
  $vehicle_note=$request->get('vehicle_note');
  $vehicle_images=$request->file('vehicle_images');  
  $vehicle_already_images=$request->get('vehicle_already_images');



  $vehicle_images_array=array();



                    //multifile uploading
  for($i=0;$i<count($vehicle_images);$i++)
  {
    $vehicle_image_index=$vehicle_images[$i];
    for($j=0;$j<count($vehicle_already_images);$j++)
    {
      $vehicle_images_array[$i][]=$vehicle_already_images[$j];
    }

    if(!empty($vehicle_image_index))
    {

     foreach($vehicle_image_index as $file)
     {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_sightseeing = "vehicle-".time()."-".$image_name;
                                  // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/vehicle_images/';
        $file->move($dir1, $image_sightseeing);
        $vehicle_images_array[$i][]=$image_sightseeing;
      }
    }
  }

}

if(count($vehicle_images)<=0)
{
  for($j=0;$j<count($vehicle_already_images);$j++)
  {
    $vehicle_images_array[0][]=$vehicle_already_images[$j];
  }
}

$transfer_array=array("transfer_name"=>$transfer_name,
  "transfer_description"=>$transfer_description,
  "supplier_id"=>$supplier_id,
  "transfer_country"=>$transfer_country,
  "transfer_type"=>$transfer_type,
  "no_of_transfer_available"=>$no_of_transfers,
  "transfer_vehicle_type"=>$vehicle_type[0],
  "transfer_vehicle"=>$vehicle[0],
  "transfer_vehicle_info"=>$vehicle_info[0],
  "transfer_vehicle_note"=>$vehicle_note[0],
  "transfer_vehicle_images"=>serialize($vehicle_images_array),
  "transfer_inclusions"=>$transfer_inclusions,
  "transfer_exclusions"=>$transfer_exclusions,
  "transfer_cancellation"=>$transfer_cancellation,
  "transfer_terms_and_conditions"=>$transfer_terms_conditions,
);

$update_transfer=Transfers::where('transfer_id',$transfer_id)->update($transfer_array);
if($update_transfer)
{
  if($change_occured>0)
  {
   $last_id=$transfer_id;
   $delete_transfers=TransferDetails::where('transfer_id',$transfer_id)->delete();

   
   for($transfer_from_count=0;$transfer_from_count < count($transfer_from);$transfer_from_count++)
   {
    for($transfer_count=0;$transfer_count < count($transfer_to[$transfer_from_count]);$transfer_count++)
    {
      $tranfer_vehicle_tariff=array();
      $insert_transfer_details=new TransferDetails;
      $insert_transfer_details->transfer_id=$last_id;
      $insert_transfer_details->transfer_details_type=$transfer_type;
      $insert_transfer_details->from_city_airport=$transfer_from[$transfer_from_count];
      $insert_transfer_details->to_city_airport=$transfer_to[$transfer_from_count][$transfer_count];
      $insert_transfer_details->transfer_vehicle_cost=$vehicle_cost[$transfer_from_count][$transfer_count];
      $insert_transfer_details->transfer_details_created_by=$transfer_created_by;
      $insert_transfer_details->transfer_details_role=$user_role;
      $insert_transfer_details->save();
    }
  }

}

echo "success";
}
else
{
  echo "fail";
}

}
public function transfer_details($transfer_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $suppliers=Suppliers::where('supplier_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    $fetch_vehicles=Vehicles::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_transfer=Transfers::where('transfer_id',$transfer_id)->first();
    }
    else
    {
      $get_transfer=Transfers::where('transfer_id',$transfer_id)->where('transfer_created_by',$emp_id)->where('transfer_role','!=','Supplier')->first();
    }

    if(!empty($get_transfer))
    {
      $get_transfer_details=TransferDetails::where('transfer_id',$transfer_id)->get();
      $fetch_airports=AirportMaster::get();
      $supplier_id=$get_transfer->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_transfer->transfer_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.transfer-details-view')->with(compact('countries','suppliers','fetch_cities','suppliers','get_transfer','countries_data','get_transfer_details','rights','fetch_airports','fetch_vehicles','fetch_vehicle_type'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
  
}
public function update_transfer_approval(Request $request)
{
  $id=$request->get('transfer_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="approve")
  {
    $update_transfer=Transfers::where('transfer_id',$id)->update(["transfer_approve_status"=>1]);
    if($update_transfer)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="reject")
  {
   $update_transfer=Transfers::where('transfer_id',$id)->update(["transfer_approve_status"=>2]);
   if($update_transfer)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_transfer_active_inactive(Request $request)
{
  $id=$request->get('transfer_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_transfer=Transfers::where('transfer_id',$id)->update(["transfer_status"=>1]);
    if($update_transfer)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
   $update_transfer=Transfers::where('transfer_id',$id)->update(["transfer_status"=>0]);
   if($update_transfer)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}


public function create_driver(Request $request)
{
  if(session()->has('travel_users_id'))
  {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $languages=Languages::get();
   $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   return view('mains.create-driver')->with(compact('suppliers','rights','countries','languages','fetch_vehicle_type'));
 }
 else
 {
  return redirect()->route('index');
}
}

public function insert_driver(Request $request)
{
  // echo "<pre>";
  // print_r($request->all());
  // echo "</pre>";
  // die();
  $driver_created_by=session()->get('travel_users_id');
  $driver_first_name=$request->get('driver_first_name');
  $driver_last_name=$request->get('driver_last_name');
  $driver_contact=$request->get('contact_number');
  $driver_address=$request->get('address');
  $check_drivers=Drivers::where('driver_contact',$driver_contact)->get();
  if(count($check_drivers)>0)
  {
    echo "exist";
  }
  else
  {
    $driver_supplier_id=$request->get('driver_supplier_name');
    $driver_country=$request->get('driver_country');
    $driver_city=$request->get('driver_city');
    $driver_language=$request->get('driver_language');
    $driver_language=implode(',',$driver_language);
    $driver_price_per_day=$request->get('driver_price_per_day');
    $driver_food_cost=$request->get('driver_food_cost');
      $driver_hotel_cost=$request->get('driver_hotel_cost');
    $driver_description=$request->get('description');
    $week_monday=$request->get('week_monday');
    $week_tuesday=$request->get('week_tuesday');
    $week_wednesday=$request->get('week_wednesday');
    $week_thursday=$request->get('week_thursday');
    $week_friday=$request->get('week_friday');
    $week_saturday=$request->get('week_saturday');
    $week_sunday=$request->get('week_sunday');
    $operating_weekdays=array("monday"=>$week_monday,
      "tuesday"=>$week_tuesday,
      "wednesday"=>$week_wednesday,
      "thursday"=>$week_thursday,
      "friday"=>$week_friday,
      "saturday"=>$week_saturday,
      "sunday"=>$week_sunday);
    $operating_weekdays=serialize($operating_weekdays);
    $driver_blackout_dates=$request->get('blackout_days');
    $driver_nationality=$request->get('driver_nationality');
    $driver_markup=$request->get('driver_markup');
    $driver_amount=$request->get('driver_amount');



    $vehicle_type_array=$request->get('vehicle_type');
    $vehicle_type=implode(",",$vehicle_type_array);

    $vehicle_array=$request->get('vehicle');
    $vehicle=implode(",",$vehicle_array);

    $vehicle_info_array=$request->get('vehicle_info');
    $vehicle_info=implode("---",$vehicle_info_array);


    $vehicle_images=$request->file('vehicle_images');  
    
        //multifile uploading
    $vehicle_images_array=array();

    if(count($vehicle_images)>0)
    {
     $vehicle_images_array_indexes=array_keys($vehicle_images); 
   }
   


   for($i=0;$i<count($vehicle_images);$i++)
   {
    $actual_index=$vehicle_images_array_indexes[$i];
    if(!empty($vehicle_images[$actual_index]))
    {
      $vehicle_image_index=$vehicle_images[$actual_index];
      if(!empty($vehicle_image_index))
      {

       foreach($vehicle_image_index as $file)
       {
        $extension=strtolower($file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
        {
          $image_name=$file->getClientOriginalName();
          $image_sightseeing = "driver-vehicle-".time()."-".$image_name;
                        // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
          $dir1 = 'assets/uploads/driver_vehicle_images/';
          $file->move($dir1, $image_sightseeing);
          $vehicle_images_array[$actual_index][]=$image_sightseeing;
        }
      }
    }
  }

}

$vehicle_images_array=serialize($vehicle_images_array);


$nationality_markup_details=array();
for($nation_count=0;$nation_count<count($driver_nationality);$nation_count++)
{
  $nationality_markup_details[$nation_count]['driver_nationality']=$driver_nationality[$nation_count];
  $nationality_markup_details[$nation_count]['driver_markup']=$driver_markup[$nation_count];
  $nationality_markup_details[$nation_count]['driver_amount']=$driver_amount[$nation_count];
}
$nationality_markup_details=serialize($nationality_markup_details);
$driver_validity_from=$request->get('driver_validity_from');
$driver_validity_to=$request->get('driver_validity_to');
$driver_tourname=$request->get('driver_tourname');  
$driver_cost_four=$request->get('driver_cost_four');
$driver_cost_seven=$request->get('driver_cost_seven');
$driver_cost_twenty=$request->get('driver_cost_twenty');
$driver_duration=$request->get('driver_duration');
$driver_tariff=array();
for($transport_count=0;$transport_count<count($driver_tourname);$transport_count++)
{
  $driver_tariff[$transport_count]['driver_validity_from']=$driver_validity_from[$transport_count];
  $driver_tariff[$transport_count]['driver_validity_to']=$driver_validity_to[$transport_count];
  $driver_tariff[$transport_count]['driver_tourname']=$driver_tourname[$transport_count];
  $driver_tariff[$transport_count]['driver_cost_four']=$driver_cost_four[$transport_count];
  $driver_tariff[$transport_count]['driver_cost_seven']=$driver_cost_seven[$transport_count];
  $driver_tariff[$transport_count]['driver_cost_twenty']=$driver_cost_twenty[$transport_count];
  $driver_tariff[$transport_count]['driver_duration']=$driver_duration[$transport_count];
}
$driver_tariff=serialize($driver_tariff);
if($request->has('tour_name'))
{
  $tour_name=$request->get('tour_name');
  $tour_vehiclename=$request->get('tour_vehiclename');
  $tour_driver_cost=$request->get('tour_guide_cost');
  $driver_tours_cost=array();
  for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
  {
    if(!empty($tour_name[$cost_count]))
    {
      $driver_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
      $driver_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
      $driver_tours_cost[$cost_count]["tour_driver_cost"]=$tour_driver_cost[($tour_name[$cost_count]-1)];
    }
    
  }

}
else
{
  $driver_tours_cost=array();
}

$driver_tours_cost=serialize($driver_tours_cost);
$driver_logo_file=$request->get('driver_logo_file');
if($request->hasFile('driver_logo_file'))
{
  $driver_logo_file=$request->file('driver_logo_file');
  $extension=strtolower($request->driver_logo_file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
  {
   $image_name=$request->file('driver_logo_file')->getClientOriginalName();
   $driver_image = "driver-".time()."-".$image_name;
                  // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
   $dir1 = 'assets/uploads/driver_images/';
   $request->file('driver_logo_file')->move($dir1, $driver_image);
 }
 else
 {
  $driver_image = "";
}
}
else
{
  $driver_image = "";
}
$driver_inclusions=$request->get('driver_inclusions');
$driver_exclusions=$request->get('driver_exclusions');
$driver_cancel_policy=$request->get('driver_cancellation');
$driver_terms_conditions=$request->get('driver_terms_conditions');



$driver=new Drivers;
$driver->driver_first_name=$driver_first_name;
$driver->driver_last_name=$driver_last_name;
$driver->driver_contact=$driver_contact;
$driver->driver_address=$driver_address;
$driver->driver_supplier_id=$driver_supplier_id;
$driver->driver_country=$driver_country;
$driver->driver_city=$driver_city;
$driver->driver_tours_cost=$driver_tours_cost;
$driver->driver_language=$driver_language;
$driver->driver_price_per_day=$driver_price_per_day;
$driver->driver_description=$driver_description;
$driver->operating_weekdays=$operating_weekdays;
$driver->driver_blackout_dates=$driver_blackout_dates;
$driver->nationality_markup_details=$nationality_markup_details;
$driver->driver_tariff=$driver_tariff;
$driver->driver_food_cost=$driver_food_cost;
$driver->driver_hotel_cost=$driver_hotel_cost;
$driver->driver_inclusions=$driver_inclusions;
$driver->driver_exclusions=$driver_exclusions;
$driver->driver_cancel_policy=$driver_cancel_policy;
$driver->driver_terms_conditions=$driver_terms_conditions;
$driver->driver_image=$driver_image;
$driver->driver_vehicle_type=$vehicle_type;
$driver->driver_vehicle=$vehicle;
$driver->driver_vehicle_info=$vehicle_info;
$driver->driver_vehicle_images= $vehicle_images_array;
$driver->driver_created_by=$driver_created_by;
if(session()->get('travel_users_role')=="Admin")
{
 $driver->driver_role="Admin";
}
else
{
  $driver->driver_role="Sub-User";
}
if($driver->save())
{
  $last_id=$driver->id;
  $driver_log=new Drivers_log;
  $driver_log->driver_id=$last_id;
  $driver_log->driver_first_name=$driver_first_name;
  $driver_log->driver_last_name=$driver_last_name;
  $driver_log->driver_contact=$driver_contact;
  $driver_log->driver_address=$driver_address;
  $driver_log->driver_supplier_id=$driver_supplier_id;
  $driver_log->driver_country=$driver_country;
  $driver_log->driver_city=$driver_city;
  $driver_log->driver_tours_cost=$driver_tours_cost;
  $driver_log->driver_language=$driver_language;
  $driver_log->driver_price_per_day=$driver_price_per_day;
  $driver_log->driver_description=$driver_description;
  $driver_log->operating_weekdays=$operating_weekdays;
  $driver_log->driver_blackout_dates=$driver_blackout_dates;
  $driver_log->nationality_markup_details=$nationality_markup_details;
  $driver_log->driver_tariff=$driver_tariff;
  $driver_log->driver_food_cost=$driver_food_cost;
$driver_log->driver_hotel_cost=$driver_hotel_cost;
  $driver_log->driver_inclusions=$driver_inclusions;
  $driver_log->driver_exclusions=$driver_exclusions;
  $driver_log->driver_cancel_policy=$driver_cancel_policy;
  $driver_log->driver_terms_conditions=$driver_terms_conditions;
  $driver_log->driver_image=$driver_image;
  $driver_log->driver_vehicle_type=$vehicle_type;
  $driver_log->driver_vehicle=$vehicle;
  $driver_log->driver_vehicle_info=$vehicle_info;
  $driver_log->driver_vehicle_images= $vehicle_images_array;
  $driver_log->driver_created_by=$driver_created_by;
  if(session()->get('travel_users_role')=="Admin")
  {
   $driver_log->driver_role="Admin";
 }
 else
 {
  $driver_log->driver_role="Sub-User";
}
$driver_log->driver_operation_performed="INSERT";
$driver_log->save();
echo "success";
}
else
{
  echo "fail";
}
}
}
public function edit_driver($driver_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('service-management');
    $countries=Countries::where('country_status',1)->get();
    $languages=Languages::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    $emp_id=session()->get('travel_users_id');
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_drivers=Drivers::where('driver_id',$driver_id)->first();
    }
    else
    {
      $get_drivers=Drivers::where('driver_id',$driver_id)->where('driver_created_by',$emp_id)->where('driver_role',"!=","Supplier")->first();
    }

    if(!empty($get_drivers))
    {
      $supplier_id=$get_drivers->driver_supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $suppliers=Suppliers::where('supplier_status',1)->get();
      $city_id=$get_drivers->driver_city;
      // $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->where(function($query) use( $city_id){
      //   $query->where('sightseeing_city_from', $city_id)->orWhereRaw('FIND_IN_SET(?,sightseeing_city_between)', [ $city_id])->orWhere('sightseeing_city_to', $city_id);
      // })->get();

       // $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->get();
       $get_cities=Cities::where('id',$city_id)->first();
      if($get_cities->name=="Tbilisi")
      {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->get();
     }
     else if($get_cities->name=="Kutaisi" || $get_cities->name=="Batumi")
     {
      $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->where(function($query) use( $city_id){
        $query->where('sightseeing_city_from', $city_id)->orWhere('sightseeing_city_to', $city_id);
      })->get();
     }
     else
     {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->where(function($query) use( $city_id){
        $query->where('sightseeing_city_from', $city_id)->where('sightseeing_city_to', $city_id);
      })->get();

     }


       


      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_drivers->driver_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('mains.edit-driver')->with(compact('languages','countries','cities','get_supplier_countries','get_drivers','suppliers',"countries_data",'fetch_vehicle_type','rights','fetch_sightseeing'));
    }
    else
    {
      return redirect()->back();
    }


  }
  else
  {
    return redirect()->route('index');
  }
}
public function update_driver(Request $request)
{
        // echo "<pre>";
        //   print_r($request->all());
        //   die();
  $driver_id=$request->get('driver_id');
  $driver_created_by=session()->get('travel_users_id');
  $driver_first_name=$request->get('driver_first_name');
  $driver_last_name=$request->get('driver_last_name');
  $driver_contact=$request->get('contact_number');
  $driver_address=$request->get('address');
  $check_drivers=Drivers::where('driver_contact',$driver_contact)->where('driver_id','!=',$driver_id)->get();
  if(count($check_drivers)>0)
  {

    echo "exist";
  }
  else
  {
   $driver_image_get=Drivers::where('driver_id',$driver_id)->first();
   $logo_data=$driver_image_get['driver_image'];
   $driver_supplier_id=$request->get('driver_supplier_name');
   $driver_country=$request->get('driver_country');
   $driver_city=$request->get('driver_city');
   $driver_language=$request->get('driver_language');
   $driver_language=implode(',',$driver_language);
   $driver_price_per_day=$request->get('driver_price_per_day');
     $driver_food_cost=$request->get('driver_food_cost');
      $driver_hotel_cost=$request->get('driver_hotel_cost');
   $driver_description=$request->get('description');
   $week_monday=$request->get('week_monday');
   $week_tuesday=$request->get('week_tuesday');
   $week_wednesday=$request->get('week_wednesday');
   $week_thursday=$request->get('week_thursday');
   $week_friday=$request->get('week_friday');
   $week_saturday=$request->get('week_saturday');
   $week_sunday=$request->get('week_sunday');
   $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);
   $operating_weekdays=serialize($operating_weekdays);
   $driver_blackout_dates=$request->get('blackout_days');
   $driver_nationality=$request->get('driver_nationality');
   $driver_markup=$request->get('driver_markup');
   $driver_amount=$request->get('driver_amount');

   $vehicle_type_array=$request->get('vehicle_type');
   $vehicle_type=implode(",",$vehicle_type_array);

   $vehicle_array=$request->get('vehicle');
   $vehicle=implode(",",$vehicle_array);

   $vehicle_info_array=$request->get('vehicle_info');
   $vehicle_info=implode("---",$vehicle_info_array);
   $vehicle_already_images=$request->get('vehicle_already_images');

   $vehicle_images=$request->file('vehicle_images');  

        //multifile uploading
   $vehicle_images_array=array();

   if(count($vehicle_already_images)>0)
   {
     $vehicle_already_images_array_indexes=array_keys($vehicle_already_images); 
   }

   

   for($i=0;$i<count($vehicle_already_images);$i++)
   {
     $actual_index=$vehicle_already_images_array_indexes[$i];
     if(!empty($vehicle_already_images[$actual_index]))
     {
      for($j=0;$j<count($vehicle_already_images[$actual_index]);$j++)
      {
        $vehicle_images_array[$actual_index][]=$vehicle_already_images[$actual_index][$j];
      }
    }
    
  }

  if(count($vehicle_images)>0)
  {
   $vehicle_images_array_indexes=array_keys($vehicle_images); 
 }

 for($i=0;$i<count($vehicle_images);$i++)
 {
  $actual_index=$vehicle_images_array_indexes[$i];
  if(!empty($vehicle_images[$actual_index]))
  {
   $vehicle_image_index=$vehicle_images[$actual_index];

   if(!empty($vehicle_image_index))
   {

     foreach($vehicle_image_index as $file)
     {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
      {
        $image_name=$file->getClientOriginalName();
        $image_sightseeing = "driver-vehicle-".time()."-".$image_name;
        $dir1 = 'assets/uploads/driver_vehicle_images/';
        $file->move($dir1, $image_sightseeing);
        $vehicle_images_array[$actual_index][]=$image_sightseeing;
      }
    }
  }
}

}

$vehicle_images_array=serialize($vehicle_images_array);


$nationality_markup_details=array();
for($nation_count=0;$nation_count<count($driver_nationality);$nation_count++)
{
  $nationality_markup_details[$nation_count]['driver_nationality']=$driver_nationality[$nation_count];
  $nationality_markup_details[$nation_count]['driver_markup']=$driver_markup[$nation_count];
  $nationality_markup_details[$nation_count]['driver_amount']=$driver_amount[$nation_count];
}
$nationality_markup_details=serialize($nationality_markup_details);
$driver_validity_from=$request->get('driver_validity_from');
$driver_validity_to=$request->get('driver_validity_to');
$driver_tourname=$request->get('driver_tourname');  
$driver_cost_four=$request->get('driver_cost_four');
$driver_cost_seven=$request->get('driver_cost_seven');
$driver_cost_twenty=$request->get('driver_cost_twenty');
$driver_duration=$request->get('driver_duration');
$driver_tariff=array();
for($transport_count=0;$transport_count<count($driver_tourname);$transport_count++)
{
  $driver_tariff[$transport_count]['driver_validity_from']=$driver_validity_from[$transport_count];
  $driver_tariff[$transport_count]['driver_validity_to']=$driver_validity_to[$transport_count];
  $driver_tariff[$transport_count]['driver_tourname']=$driver_tourname[$transport_count];
  $driver_tariff[$transport_count]['driver_cost_four']=$driver_cost_four[$transport_count];
  $driver_tariff[$transport_count]['driver_cost_seven']=$driver_cost_seven[$transport_count];
  $driver_tariff[$transport_count]['driver_cost_twenty']=$driver_cost_twenty[$transport_count];
  $driver_tariff[$transport_count]['driver_duration']=$driver_duration[$transport_count];
}
$driver_tariff=serialize($driver_tariff);
    //         if($request->has('tour_name'))
    //         {
    //             $tour_name=$request->get('tour_name');
    //         $tour_driver_cost=$request->get('tour_driver_cost');
    // $driver_tours_cost="";
    //         for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
    //         {
    //           $driver_tours_cost.=$tour_name[$cost_count]."---".$tour_driver_cost[$cost_count]."///";
    //         }

    //         }
    //         else
    //         {
    //           $driver_tours_cost="";
    //         }
if($request->has('tour_name'))
{
  $tour_name=$request->get('tour_name');
  $tour_vehiclename=$request->get('tour_vehiclename');
  $tour_driver_cost=$request->get('tour_guide_cost');
  $driver_tours_cost=array();
  for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
  {
    if(!empty($tour_name[$cost_count]))
    {
      $driver_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
      $driver_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
      $driver_tours_cost[$cost_count]["tour_driver_cost"]=$tour_driver_cost[($tour_name[$cost_count]-1)];
    }
  }

}
else
{
  $driver_tours_cost=array();
}

$driver_tours_cost=serialize($driver_tours_cost);

$driver_logo_file=$request->get('driver_logo_file');
if($request->hasFile('driver_logo_file'))
{
  $driver_logo_file=$request->file('driver_logo_file');
  $extension=strtolower($request->driver_logo_file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
  {
   $image_name=$driver_logo_file->getClientOriginalName();
   $driver_image = "driver-".time()."-".$image_name;
                    // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
   $dir1 = 'assets/uploads/driver_images/';
   $request->file('driver_logo_file')->move($dir1, $driver_image);
 }
 else
 {
  $driver_image = "";
}
}
else
{
  $driver_image = $logo_data;
}
$driver_inclusions=$request->get('driver_inclusions');
$driver_exclusions=$request->get('driver_exclusions');
$driver_cancel_policy=$request->get('driver_cancellation');
$driver_terms_conditions=$request->get('driver_terms_conditions');

$update_array=array("driver_first_name"=>$driver_first_name,
  "driver_last_name"=>$driver_last_name,
  "driver_contact"=>$driver_contact,
  "driver_address"=>$driver_address,
  "driver_supplier_id"=>$driver_supplier_id,
  "driver_country"=>$driver_country,
  "driver_city"=>$driver_city,
  "driver_tours_cost"=>$driver_tours_cost,
  "driver_language"=>$driver_language,
  "driver_price_per_day"=>$driver_price_per_day,
  "driver_description"=>$driver_description,
  "operating_weekdays"=>$operating_weekdays,
  "driver_blackout_dates"=>$driver_blackout_dates,
  "nationality_markup_details"=>$nationality_markup_details,
  "driver_tariff"=>$driver_tariff,
  "driver_food_cost"=>$driver_food_cost,
  "driver_hotel_cost"=>$driver_hotel_cost,
  "driver_inclusions"=>$driver_inclusions,
  "driver_exclusions"=>$driver_exclusions,
  "driver_cancel_policy"=>$driver_cancel_policy,
  "driver_terms_conditions"=>$driver_terms_conditions,
  "driver_image"=>$driver_image,
  "driver_vehicle_type"=>$vehicle_type,
  "driver_vehicle"=>$vehicle,
  "driver_vehicle_info"=>$vehicle_info,
  "driver_vehicle_images"=>$vehicle_images_array);
$update_driver=Drivers::where('driver_id',$driver_id)->update($update_array);
if($update_driver)
{
  $driver_log=new Drivers_log;
  $driver_log->driver_id=$driver_id;
  $driver_log->driver_first_name=$driver_first_name;
  $driver_log->driver_last_name=$driver_last_name;
  $driver_log->driver_contact=$driver_contact;
  $driver_log->driver_address=$driver_address;
  $driver_log->driver_supplier_id=$driver_supplier_id;
  $driver_log->driver_country=$driver_country;
  $driver_log->driver_city=$driver_city;
  $driver_log->driver_tours_cost=$driver_tours_cost;
  $driver_log->driver_language=$driver_language;
  $driver_log->driver_price_per_day=$driver_price_per_day;
  $driver_log->driver_description=$driver_description;
  $driver_log->operating_weekdays=$operating_weekdays;
  $driver_log->driver_blackout_dates=$driver_blackout_dates;
  $driver_log->nationality_markup_details=$nationality_markup_details;
  $driver_log->driver_tariff=$driver_tariff;
  $driver_log->driver_food_cost=$driver_food_cost;
  $driver_log->driver_hotel_cost=$driver_hotel_cost;
  $driver_log->driver_inclusions=$driver_inclusions;
  $driver_log->driver_exclusions=$driver_exclusions;
  $driver_log->driver_cancel_policy=$driver_cancel_policy;
  $driver_log->driver_terms_conditions=$driver_terms_conditions;
  $driver_log->driver_image=$driver_image;
  $driver_log->driver_vehicle_type=$vehicle_type;
  $driver_log->driver_vehicle=$vehicle;
  $driver_log->driver_vehicle_info=$vehicle_info;
  $driver_log->driver_vehicle_images= $vehicle_images_array;
  $driver_log->driver_created_by=$driver_created_by;
  if(session()->get('travel_users_role')=="Admin")
  {
   $driver_log->driver_role="Admin";
 }
 else
 {
  $driver_log->driver_role="Sub-User";
}

$driver_log->driver_operation_performed="UPDATE";
$driver_log->save();
echo "success";
}
else
{
  echo "fail";
}
}
}
public function driver_details($driver_id)
{
 if(session()->has('travel_users_id'))
 {
   $rights=$this->rights('service-management');
   $countries=Countries::where('country_status',1)->get();
   $languages=Languages::get();
   $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
   $emp_id=session()->get('travel_users_id');
   if(strpos($rights['admin_which'],'view')!==false)
   {
    $get_drivers=Drivers::where('driver_id',$driver_id)->first();
  }
  else
  {
    $get_drivers=Drivers::where('driver_id',$driver_id)->where('driver_created_by',$emp_id)->where('driver_role',"!=","Supplier")->first();
  }
  if(!empty($get_drivers))
  {
   $supplier_id=$get_drivers->driver_supplier_id;
   $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();
   $supplier_countries=$get_supplier_countries->supplier_opr_countries;
   $countries_data=explode(',', $supplier_countries);
   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_drivers->driver_country)->select("cities.*")->orderBy('cities.name','asc')->get();
   $suppliers=Suppliers::where('supplier_status',1)->get();
   return view('mains.driver-details-view')->with(compact('suppliers','countries','cities','get_supplier_countries','get_drivers',"countries_data","rights","languages","fetch_vehicle_type"))->with('driver_id',$driver_id);
 }
 else
 {
  return redirect()->back();
}

}
else
{
  return redirect()->route('index');
}
}
public function update_driver_active_inactive(Request $request)
{
  $id=$request->get('driver_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_driver=Drivers::where('driver_id',$id)->update(["driver_status"=>1]);
    if($update_driver)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
   $update_driver=Drivers::where('driver_id',$id)->update(["driver_status"=>0]);
   if($update_driver)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_driver_approval(Request $request)
{
  $id=$request->get('driver_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="approve")
  {
    $update_driver=Drivers::where('driver_id',$id)->update(["driver_approve_status"=>1]);
    if($update_driver)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="reject")
  {
   $update_driver=Drivers::where('driver_id',$id)->update(["driver_approve_status"=>2]);
   if($update_driver)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function update_driver_bestseller(Request $request)
{
  $id=$request->get('driver_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="bestselleryes")
  {
    $update_driver=Drivers::where('driver_id',$id)->update(["driver_best_status"=>1]);
    if($update_driver)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="bestsellerno")
  {
   $update_driver=Drivers::where('driver_id',$id)->update(["driver_best_status"=>0]);
   if($update_driver)
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
else
{
  echo "fail";
}
}
public function sort_driver(Request $request)
{
  $sort_activity_array=$request->get('new_data');
  $success_array=array();
  for($count=0;$count<count($sort_activity_array);$count++)
  {
    $driver_id=$sort_activity_array[$count]['driver_id'];
    $new_order=$sort_activity_array[$count]['new_order'];
    $update_driver=Drivers::where('driver_id',$driver_id)->update(["driver_show_order"=>$new_order]);
    if($update_driver)
    {
      $success_array[]="success";
    }
    else
    {
      $success_array[]="not_success";
    }
  }
  echo json_encode($success_array);
}


public function agent_bookings(Request $request)
{
  if(session()->has('travel_users_id'))
  {
     $rights=$this->rights('agent-bookings');
    $fetch_bookings=Bookings::where('booking_date',date('Y-m-d'))->where('booking_sightseeing_id',null)->where('itinerary_status',null)->get();
    return view('mains.agent-bookings')->with(compact('fetch_bookings','rights'));
  }
  else
  {
    return redirect()->route('index');
  }
}



public function agent_bookings_details(Request $request)
{
  if(session()->has('travel_users_id'))
  {
     $rights=$this->rights('agent-bookings');
    $booking_id=$request->get('booking_id');
    $booking_details=Bookings::where('booking_sep_id',$booking_id)->first();
    $fetch_booking_installments=BookingInstallment::where('booking_id',$booking_id)->get();
     $fetch_booking_expenses=Expense::where('expense_booking_id',$booking_id)->get();
      $fetch_booking_incomes=Income::where('incomes_booking_id',$booking_id)->get();
    return view('mains.agent-booking-details')->with(compact('booking_details','fetch_booking_installments','fetch_booking_expenses','fetch_booking_incomes','rights'));
  }
  else
  {
    return redirect()->route('index');
  }


}
public function agent_bookings_cancel(Request $request)
{
  $booking_id=$request->get('booking_id');
  $booking_role=$request->get('booking_role');
  $action_id=$request->get('action_id');


    $check_booking=Bookings::where('booking_id',$booking_id)->first();
    if(!empty($check_booking))
    {
      if($check_booking['booking_type']=="itinerary")
      {
        if($check_booking['booking_complete_status']==1)
        {
          echo "process";
        }
        else
        {
           $booking_sep_id=$check_booking['booking_sep_id'];
        $update_booking=Bookings::where('booking_sep_id',$booking_sep_id)->update(["booking_status"=>2]);

        if($update_booking)
        {
          $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

               $fetch_admin=Users::where('users_pid',0)->first();
                $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
              if($booking_role=="agent")
              {
               $agent_id=$action_id;

               $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              //Agent Cancellation Acknowledgement
               $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is cancelled by admin</p><p>Any payment for this booking will be refunded shortly.</p>';
               $data = array(
                'name' => $fetch_agent->agent_name,
                'email' =>$fetch_agent->company_email
              );
               Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
                $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION BY ADMIN');
              });

             }
             else
             {

               $users_id=$action_id;
               $fetch_user=Users::where('users_id',$users_id)->first();

              //User Cancellation Acknowledgement
               $htmldata='<p>Dear '.$fetch_user->users_fname.' '.$fetch_user->users_lname.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is cancelled by admin</p><p>Any payment for this booking will be refunded shortly.</p>';
               $data = array(
                'name' => $fetch_user->users_fname.' '.$fetch_user->users_lname,
                'email' =>$fetch_user->users_email
              );
               Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
                $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION BY ADMIN');
              });

             }


               $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been successfully cancelled.</p>';
            $data_admin= array(
              'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
              'email' =>$fetch_admin->users_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' CANCELLATION ON PORTAL');
            });
             

             $fetch_subsequent_booking=Bookings::where('booking_sep_id',$booking_id)->get();

             foreach($fetch_subsequent_booking as $subsequent_booking)
             {
              $booking_type=$subsequent_booking->booking_type;
              $booking_supplier_id=$subsequent_booking->booking_supplier_id;

              if($booking_type!="sightseeing")
              {
               if($booking_supplier_id!=null && $booking_supplier_id!="")
               {

                $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

                      //Supplier Cancellation Acknowledgement
                $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by admin . Please login into your supplier portal to get more information :'.route('supplier').'</p>';
                $data_supplier= array(
                  'name' => $fetch_supplier->supplier_name,
                  'email' =>$fetch_supplier->company_email
                );
                Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier,$booking_type) {
                  $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                  $m->to($data_supplier['email'], $data_supplier['name'])->subject(strtoupper($booking_type).' BOOKING CANCELLATION');
                });


              }

            }

          }

          echo "success";
        }
        else
          echo "fail";
        }
       

      }
      else if($check_booking['booking_type']=="sightseeing")
      {
        if($check_booking['booking_complete_status']==1)
        {
          echo "process";
        }
        else
        {
        $update_booking=Bookings::where('booking_id',$booking_id)->orWhere('booking_sightseeing_id',$booking_id)->update(["booking_status"=>2]);

        if($update_booking)
        {
           $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $fetch_admin=Users::where('users_pid',0)->first();
              $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
               if($booking_role=="agent")
              {
               $agent_id=$action_id;

               $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              //Agent Cancellation Acknowledgement
               $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is cancelled by admin</p><p>Any payment for this booking will be refunded shortly.</p>';
               $data = array(
                'name' => $fetch_agent->agent_name,
                'email' =>$fetch_agent->company_email
              );
               Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
                $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION BY ADMIN');
              });

             }
             else
             {

               $users_id=$action_id;
               $fetch_user=Users::where('users_id',$users_id)->first();

              //User Cancellation Acknowledgement
               $htmldata='<p>Dear '.$fetch_user->users_fname.' '.$fetch_user->users_lname.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is cancelled by admin</p><p>Any payment for this booking will be refunded shortly.</p>';
               $data = array(
                'name' => $fetch_user->users_fname.' '.$fetch_user->users_lname,
                'email' =>$fetch_user->users_email
              );
               Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
                $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION BY ADMIN');
              });

             }



            $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled.</p>';
            $data_admin= array(
              'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
              'email' =>$fetch_admin->users_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' CANCELLATION ON PORTAL');
            });

             $fetch_subsequent_booking=Bookings::where('booking_sightseeing_id',$booking_id)->get();

             foreach($fetch_subsequent_booking as $subsequent_booking)
             {
              $booking_type=$subsequent_booking->booking_type;
               $booking_supplier_id=$subsequent_booking->booking_supplier_id;

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
                  //Supplier Cancellation Acknowledgement
            $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by admin . Please login into your supplier portal to get more information :'.route('supplier').'</p>';
            $data_supplier= array(
              'name' => $fetch_supplier->supplier_name,
              'email' =>$fetch_supplier->company_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_supplier['email'], $data_supplier['name'])->subject('BOOKING CANCELLATION');
            });

             }
          



          echo "reload_success";
        } 
        else
          echo "fail";
      }

      }
      else
      {
        if($check_booking['booking_complete_status']==1)
        {
          echo "process";
        }
        else
        {
          $update_booking=Bookings::where('booking_id',$booking_id)->update(["booking_status"=>2]);

          if($update_booking)
          {
             $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $agent_id=session()->get('travel_agent_id');
              $fetch_admin=Users::where('users_pid',0)->first();

              $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

              if($booking_role=="agent")
              {
               $agent_id=$action_id;

               $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              //Agent Cancellation Acknowledgement
               $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is cancelled by admin</p><p>Any payment for this booking will be refunded shortly.</p>';
               $data = array(
                'name' => $fetch_agent->agent_name,
                'email' =>$fetch_agent->company_email
              );
               Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
                $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION BY ADMIN');
              });

             }
             else
             {

               $users_id=$action_id;
               $fetch_user=Users::where('users_id',$users_id)->first();

              //User Cancellation Acknowledgement
               $htmldata='<p>Dear '.$fetch_user->users_fname.' '.$fetch_user->users_lname.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is cancelled by admin</p><p>Any payment for this booking will be refunded shortly.</p>';
               $data = array(
                'name' => $fetch_user->users_fname.' '.$fetch_user->users_lname,
                'email' =>$fetch_user->users_email
              );
               Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
                $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION BY ADMIN');
              });

             }

            //Supplier Cancellation Acknowledgement
            $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by admin . Please login into your supplier portal to get more information. '.route('supplier').'</p>';
            $data_supplier= array(
              'name' => $fetch_supplier->supplier_name,
              'email' =>$fetch_supplier->company_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_supplier['email'], $data_supplier['name'])->subject('BOOKING CANCELLATION');
            });


            $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled </p>';
            $data_admin= array(
              'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
              'email' =>$fetch_admin->users_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' CANCELLATION ON PORTAL');
            });
            echo "success";
          }
          else
            echo "fail";

      }
      }
    }
}

public function booking_attachment_html(Request $request)
{
  if(session()->has('travel_users_id'))
  {
    $booking_id=$request->get('booking_id');
    $fetch_booking=Bookings::where('booking_sep_id',$booking_id)->first();
    if(!empty($fetch_booking))
    {
      
      if($fetch_booking->booking_attachments!="")
      {

       $attachments=unserialize($fetch_booking->booking_attachments);
       if(count($attachments)>0)
       {
        $html='<div id="previewImg_new" class="row" style="margin-bottom:40px">';
        $count=1;
        foreach($attachments as $attachment)
        {
         $html.=' <div class="col-md-2 already_attachments" id="already_attachments'.($count+1).'">
                             <span class="pull-right remove_already_attachments" title="Delete Attachment" id="remove_already_attachments'.($count+1).'" style="cursor:pointermargin-right: -20px;">  X </span>
                              <input type="hidden" name="upload_already_attachments[]" value="'.$attachment.'">
                             <a href="'.asset('assets/uploads/booking_attachments').'/'.$attachment.'" target="_blank" class="text-primary">Attachment '.$count.' </a> </div>';
          $count++;
        }
        $html.='</div>';

        $html.='<div class="input-group control-group increment" id="increment" >
          <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_attachment" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_attachment" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>';

        echo $html;



      }
      else
      {
        $html='<div id="previewImg_new" class="row"></div>
        <div class="input-group control-group increment" id="increment">
          <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_attachment" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_attachment" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>';

        echo $html;
      }


   }
   else
   {
    $html='<div id="previewImg_new" class="row"></div>
        <div class="input-group control-group increment" id="increment">
          <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_attachment" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_attachment" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>';

        echo $html;
   }

    }
    else
    {
      echo "invalid";
    }

  }
  else
  {
    echo "user_session";
  }

}

public function booking_attachment_upload(Request $request)
{
  if(session()->has('travel_users_id'))
  {
  $booking_id=$request->get('attachment_id');
  if(!empty($request->get('upload_already_attachments')))
    {
     $booking_attachments=$request->get('upload_already_attachments');
   }
   else
   {
    $booking_attachments=array();
  }

//multifile uploading
  if($request->hasFile('upload_attachments'))
  {
    foreach($request->file('upload_attachments') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_booking = time()."-".$image_name;
        $dir1 = 'assets/uploads/booking_attachments/';
        $file->move($dir1, $image_booking);
        $booking_attachments[]=$image_booking;
      }
    }
  }

  $booking_attachments=serialize($booking_attachments);

  $update_booking=Bookings::where('booking_sep_id',$booking_id)->update(["booking_attachments"=>$booking_attachments]);
  if($update_booking)
  {
    Session::flash('success-message',"This booking attachments are updated successfully");
  }
  else
  {
     Session::flash('error-message',"This booking attachments cannot be updated right now");
  }
  return redirect()->back();

}
else
{
   return redirect()->route('index');
}

}


public function agent_sub_bookings($booking_id,Bookings $booking)
{
   if(session()->has('travel_users_id'))
  {
     $rights=$this->rights('agent-bookings');
  $html='<table id="booking_table" class="table table-condensed table-striped">
                                            <thead>
                                                <tr>
                                                    <td>Sr no.</td>
                                                    <td>Booking ID</td>
                                                    <td>Booking Type</td>
                                                    <td>Type Name</td>
                                                    <td>Agent/Operator</td>
                                                    <td>Booked From</td>
                                                    <td>Supplier</td>
                                                    <td>Supplier Number</td>
                                                    <td>Customer Name</td>
                                                    <td>Customer Email</td>
                                                    <td>Customer Mobile</td>
                                                    <td>Booking Date</td>
                                                    <td>Supplier Amount</td>
                                                    <td>Agent Amount</td>
                                                    <td>Total Customer Amount</td>
                                                    <td>Supplier\'s Approval</td>
                                                    <td>Supplier\'s Remarks</td>
                                                    <td>Admin Remarks</td>
                                                    <td>Full Payment</td>
                                                    <td>Is Complete</td>
                                                     <td>Invoices</td>
                                                    <td>Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>';

  $fetch_bookings=$booking->where('booking_sep_id',$booking_id)->get();
$count=1;
  foreach($fetch_bookings as $bookings)
  {
    $html.='<tr>
    <td>'.$count.'</td>
    <td>'.$bookings->booking_sep_id.'</td>
    <td>'.ucwords($bookings->booking_type).'</td>
    <td>';

    $booking_type_id=$bookings->booking_type_id;
    if($bookings->booking_type=="activity")
    {
      $fetch_activity=$this->searchActivity($booking_type_id);
      $html.=$fetch_activity['activity_name'];
    }
    else if($bookings->booking_type=="hotel")
    {
      $fetch_hotel=$this->searchHotel($booking_type_id);
      $html.=$fetch_hotel['hotel_name'];
    }
    else if($bookings->booking_type=="guide")
    {
      $fetch_guide=$this->searchGuide($booking_type_id);
      $html.=$fetch_guide['guide_first_name']." ".$fetch_guide['guide_last_name'];
    }
    else if($bookings->booking_type=="driver")
    {
      $fetch_driver=$this->searchDriver($booking_type_id);
      $html.=$fetch_driver['driver_first_name']." ".$fetch_driver['driver_last_name'];
    }
    else if($bookings->booking_type=="sightseeing")
    {
      $fetch_sightseeing=$this->searchSightseeingTourName($booking_type_id);
      $html.=$fetch_sightseeing['sightseeing_tour_name'];
    }
    else if($bookings->booking_type=="itinerary")
    {
      $fetch_itinerary=$this->searchItinerary($booking_type_id);
      $html.=$fetch_itinerary['itinerary_tour_name'];
    }
     else if($bookings->booking_type=="restaurant")
    {
      $fetch_restaurant=$this->searchRestaurant($booking_type_id);
      $html.=$fetch_restaurant['restaurant_name'];
    }
    else if($bookings->booking_type=="transfer")
    {

      $html.=$bookings->booking_subject_name;
    }
$html.='</td>
    <td>';
    if($bookings->booking_role=="AGENT")
    {
        
    $fetch_agent=$this->searchAgent($bookings->booking_agent_id);
    $html.=$fetch_agent['agent_name'];
    }
    else
    {
      $fetch_user=$this->searchUser($bookings->booking_agent_id);
    $html.=$fetch_user['users_fname']." ".$fetch_user['users_lname'];
    }
  
    
    

    $html.='</td>
    <td>'.$bookings->booking_role.' PORTAL</td>';
    if($bookings->booking_type=="sightseeing")
    {
      $html.='<td>NA</td><td>NA</td>';
   }
   else
   {
     $fetch_supplier=$this->searchSupplier($bookings->booking_supplier_id);
     $html.='<td>'.$fetch_supplier['supplier_name'].'</td>';
     $html.='<td>'.$fetch_supplier['company_contact'].'</td>';
   }
   
    $html.='<td>'.$bookings->customer_name.'</td>
    <td>'.$bookings->customer_email.'</td>
    <td>'.$bookings->customer_contact.'</td>
    <td>'.$bookings->booking_date.'</td>
    <td>'.$bookings->booking_currency.' '.$bookings->booking_supplier_amount.'</td>
    <td>'.$bookings->booking_currency.' '.$bookings->booking_agent_amount.'</td>
    <td>'.$bookings->booking_currency.' '.$bookings->booking_amount.'</td>
    <td id="status_'.$bookings->booking_id.'">';
    if($bookings->booking_supplier_status==0)
    {
     $html.='<button class="btn btn-sm btn-warning">Pending</button>';
   }
   elseif($bookings->booking_supplier_status==1)
   {
    $html.='<button class="btn btn-sm btn-success">Confirmed</button>';
  }

  elseif($bookings->booking_supplier_status==2)
  {
    $html.='<button class="btn btn-sm btn-danger">Rejected</button>';
  }

  $html.='</td>
  <td>'.$bookings->booking_supplier_message.'</td>
  <td id="remarks_booking_'.$bookings->booking_id.'">'.$bookings->booking_admin_message.'</td>';

  if($bookings->itinerary_status==0)
  {
    if($bookings->booking_final_amount_status==0)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button></td>';
    }
    elseif($bookings->booking_final_amount_status==1)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button></td>';
    }
  }
  else
  {
    $html.='<td></td>';
  }

   if($bookings->itinerary_status==0)
  {
    if($bookings->booking_complete_status==0)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button></td>';
    }
    elseif($bookings->booking_complete_status==1)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button></td>';
    }
  }
  else
  {
    $html.='<td></td>';
  }

   if($bookings->itinerary_status==0)
  {
  $html.='<td><a href="'.route('admin-agent-invoice',['booking_id'=>$bookings->booking_sep_id]).'" class="btn btn-rounded btn-default btn-sm">Agent Invoice</a> &nbsp;
<a href="'.route('admin-customer-invoice',['booking_id'=>$bookings->booking_sep_id]).'" class="btn btn-rounded btn-default btn-sm">Customer Invoice</a>
  </td>';
}
else
{
$html.='<td></td>';
}

  $html.='<td>
  <span>';
  if($bookings->booking_status==2)
  {
   $html.='<button type="button" class="btn btn-sm btn-rounded btn-info" disabled="disabled">Cancelled</button>';
 } 
 elseif($bookings->booking_admin_status==1)
 {
   $html.=' <button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';

 }
 elseif($bookings->booking_admin_status==0)
 {
  $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_booking_'.$bookings->booking_id.'">Approve</button>
  <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_booking_'.$bookings->booking_id.'">Reject</button>';

}
elseif($bookings->booking_admin_status==2)
{
 $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';

}

$html.=' </span>';
if($bookings->itinerary_status==0 || $bookings->itinerary_status==null)
{
$html.='<a class="btn btn-rounded btn-success btn-sm" href="'.route('booking-details').'?booking_id='.$bookings->booking_sep_id.'" target="_blank">View Details</a>';
}

$html.='</td>
</tr>';

$count++;
  }

  $html.="</tbody>
        </table>";

        return view('mains.agent-sub-bookings')->with('htmldata',$html);
         }
  else
  {
    return redirect()->route('index');
  }


}
public function agent_bookings_filters(Request $request,Bookings $booking)
{
  $from_date=$request->get('from_date');
  $to_date=$request->get('to_date');
  $booking_type=$request->get('booking_type');

  if($from_date!="")
  {
    $from_date1=explode("/", $from_date);
    $from_date=$from_date1[2]."-".$from_date1[1]."-".$from_date1[0];
  }
  if($to_date!="")
  {
    $to_date1=explode("/", $to_date);
    $to_date=$to_date1[2]."-".$to_date1[1]."-".$to_date1[0];
  }
  $fetch_bookings=$booking->newQuery();
  $html='<table id="booking_table" class="table table-condensed table-striped">
                                            <thead>
                                                <tr>
                                                    <td>Sr no.</td>
                                                    <td>Booking ID</td>
                                                    <td>Booking Type</td>
                                                    <td>Type Name</td>
                                                    <td>Agent/Operator</td>
                                                   
                                                    <td>Supplier</td>
                                                    <td>Supplier Number</td>
                                                    <td>Customer Name</td>
                                                    <td>Customer Email</td>
                                                    <td>Customer Mobile</td>
                                                    <td>Booking Date</td>
                                                    <td>Supplier Amount</td>
                                                    <td>Agent Amount</td>
                                                    <td>Total Customer Amount</td>
                                                    <td>Supplier\'s Approval</td>
                                                    <td>Supplier\'s Remarks</td>
                                                    <td>Admin Remarks</td>
                                                     <td>Booked From</td>
                                                      <td>Sub Bookings</td>
                                                    <td>Full Payment</td>
                                                    <td>Is Complete</td>
                                                     <td>Invoices</td>
                                                      <td>Installments</td>
                                                    <td>Action</td>
                                                </tr>
                                            </thead>
                                            <tbody>';

  if($from_date!="" && $to_date=="")
  {
   $fetch_bookings=$fetch_bookings->where('booking_date',$from_date);
  }
  else if($from_date=="" && $to_date!="")
  {
   $fetch_bookings=$fetch_bookings->where('booking_date',$to_date);
  }
  else if($from_date!="" && $to_date!="")
  {
     $fetch_bookings=$fetch_bookings->whereBetween('booking_date',array($from_date,$to_date));
  }
   
  if($booking_type!="0")
  {
    $fetch_bookings=$fetch_bookings->where('booking_type',$booking_type);
  }

  $fetch_bookings=$fetch_bookings->where('itinerary_status',null)->where('booking_sightseeing_id',null)->get();
$count=1;
  foreach($fetch_bookings as $bookings)
  {
    $html.='<tr>
    <td>'.$count.'</td>
    <td>'.$bookings->booking_sep_id.'</td>
    <td>'.ucwords($bookings->booking_type).'</td>
    <td>';

    $booking_type_id=$bookings->booking_type_id;
    if($bookings->booking_type=="activity")
    {
      $fetch_activity=$this->searchActivity($booking_type_id);
      $html.=$fetch_activity['activity_name'];
    }
    else if($bookings->booking_type=="hotel")
    {
      $fetch_hotel=$this->searchHotel($booking_type_id);
      $html.=$fetch_hotel['hotel_name'];
    }
    else if($bookings->booking_type=="guide")
    {
      $fetch_guide=$this->searchGuide($booking_type_id);
      $html.=$fetch_guide['guide_first_name']." ".$fetch_guide['guide_last_name'];
    }
    else if($bookings->booking_type=="driver")
    {
      $fetch_driver=$this->searchDriver($booking_type_id);
      $html.=$fetch_driver['driver_first_name']." ".$fetch_driver['driver_last_name'];
    }
    else if($bookings->booking_type=="sightseeing")
    {
      $fetch_sightseeing=$this->searchSightseeingTourName($booking_type_id);
      $html.=$fetch_sightseeing['sightseeing_tour_name'];
    }
    else if($bookings->booking_type=="itinerary")
    {
      $fetch_itinerary=$this->searchItinerary($booking_type_id);
      $html.=$fetch_itinerary['itinerary_tour_name'];
    }
     else if($bookings->booking_type=="restaurant")
    {
      $fetch_restaurant=$this->searchRestaurant($booking_type_id);
      $html.=$fetch_restaurant['restaurant_name'];
    }
    else if($bookings->booking_type=="transfer")
    {

      $html.=$bookings->booking_subject_name;
    }
$html.='</td>
    <td>';
    if($bookings->booking_role=="AGENT")
    {
        
    $fetch_agent=$this->searchAgent($bookings->booking_agent_id);
    $html.=$fetch_agent['agent_name'];
    }
    else
    {
      $fetch_user=$this->searchUser($bookings->booking_agent_id);
    $html.=$fetch_user['users_fname']." ".$fetch_user['users_lname'];
    }
  
    
    

    $html.='</td>';
    if($bookings->booking_type=="sightseeing")
    {
      $html.='<td>NA</td><td>NA</td>';
   }
   else
   {
     $fetch_supplier=$this->searchSupplier($bookings->booking_supplier_id);
     $html.='<td>'.$fetch_supplier['supplier_name'].'</td>';
     $html.='<td>'.$fetch_supplier['company_contact'].'</td>';
   }
   
    $html.='<td>'.$bookings->customer_name.'</td>
    <td>'.$bookings->customer_email.'</td>
    <td>'.$bookings->customer_contact.'</td>
    <td>'.$bookings->booking_date.'</td>
    <td>'.$bookings->booking_currency.' '.$bookings->booking_supplier_amount.'</td>
    <td>'.$bookings->booking_currency.' '.$bookings->booking_agent_amount.'</td>
    <td>'.$bookings->booking_currency.' '.$bookings->booking_amount.'</td>
    <td id="status_'.$bookings->booking_id.'">';
    if($bookings->booking_supplier_status==0)
    {
     $html.='<button class="btn btn-sm btn-warning">Pending</button>';
   }
   elseif($bookings->booking_supplier_status==1)
   {
    $html.='<button class="btn btn-sm btn-success">Confirmed</button>';
  }

  elseif($bookings->booking_supplier_status==2)
  {
    $html.='<button class="btn btn-sm btn-danger">Rejected</button>';
  }

  $html.='</td>
  <td>'.$bookings->booking_supplier_message.'</td>
  <td id="remarks_booking_'.$bookings->booking_id.'">'.$bookings->booking_admin_message.'</td>
  <td>'.$bookings->booking_role.' PORTAL</td>';

  if($bookings->booking_type=="itinerary" || $bookings->booking_type=="transfer" || ($bookings->booking_type=="sightseeing" && $bookings->booking_supplier_id!=null))
  {
    $html.='<td><a class="btn btn-info btn-sm" href="'.route('agent-sub-bookings',['booking_id'=>$bookings->booking_sep_id]).'" target="_blank">View Sub Bookings</a></td>';
 }
 else
 {
  $html.='<td> No Sub Bookings</a>';
}

        

  if($bookings->itinerary_status==0)
  {
    if($bookings->booking_final_amount_status==0)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button></td>';
    }
    elseif($bookings->booking_final_amount_status==1)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button></td>';
    }
  }
  else
  {
    $html.='<td></td>';
  }

   if($bookings->itinerary_status==0)
  {
    if($bookings->booking_complete_status==0)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button></td>';
    }
    elseif($bookings->booking_complete_status==1)
    {
      $html.='<td><button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button></td>';
    }
  }
  else
  {
    $html.='<td></td>';
  }

   if($bookings->itinerary_status==0)
  {
  $html.='<td><a href="'.route('admin-agent-invoice',['booking_id'=>$bookings->booking_sep_id]).'" class="btn btn-rounded btn-default btn-sm">Agent Invoice</a> &nbsp;
<a href="'.route('admin-customer-invoice',['booking_id'=>$bookings->booking_sep_id]).'" class="btn btn-rounded btn-default btn-sm">Customer Invoice</a>
  </td>';
}
else
{
$html.='<td></td>';
}

if(($bookings->itinerary_status==0 || $bookings->itinerary_status==null))
{
$html.='<td><button id="installment_'.$bookings->booking_sep_id.'" class="btn btn-rounded btn-primary btn-sm create_installment" ';

if($bookings->booking_admin_status!=1)
  $html.='disabled';

   $html.='>Installments</button></td>';
}
else if($bookings->booking_final_amount_status==1)
{
  $html.='<td>Completed</td>';
}
else
{
  $html.='<td>Not Available</td>';
}

  $html.='<td>
  <span>';
  if($bookings->booking_status==2)
  {
   $html.='<button type="button" class="btn btn-sm btn-rounded btn-info" disabled="disabled">Cancelled</button>';
 } 
 elseif($bookings->booking_admin_status==1)
 {
   $html.=' <button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>';

 }
 elseif($bookings->booking_admin_status==0)
 {
  $html.='<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_booking_'.$bookings->booking_id.'">Approve</button>
  <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_booking_'.$bookings->booking_id.'">Reject</button>';

}
elseif($bookings->booking_admin_status==2)
{
 $html.='<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>';

}

$html.=' </span>';
if($bookings->itinerary_status==0 || $bookings->itinerary_status==null)
{
$html.='<a class="btn btn-rounded btn-success btn-sm" href="'.route('booking-details').'?booking_id='.$bookings->booking_sep_id.'" target="_blank">View Details</a>';
}

$html.='</td>
</tr>';

$count++;
  }

  $html.="</tbody>
        </table>";

        echo $html;


}
public function approve_agent_booking(Request $request)
{
  $action_perform=$request->get('action_perform');
  $booking_id=$request->get('booking_id');
  $remarks=$request->get('remarks');

  if($action_perform=="approve")
  {
    $check_booking=Bookings::where('booking_id',$booking_id)->first();
    if(!empty($check_booking))
    {
      if($check_booking['booking_type']=="itinerary")
      {
         if($check_booking['booking_status']==2)
        {
          echo "cancel";
        }
        else
        {
          $booking_sep_id=$check_booking['booking_sep_id'];
          $update_booking=Bookings::where('booking_sep_id',$booking_sep_id)->update(["booking_admin_status"=>1,"booking_admin_message"=>$remarks]);

          if($update_booking)
          {
            $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $fetch_admin=Users::where('users_pid',0)->first();

              $agent_id=$check_booking['booking_agent_id'];

              $fetch_agent=Agents::where('agent_id',$agent_id)->first();
              //Agent Request Acceot
                  $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Congratulations ! Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is successfully confirmed by Traveldoor Admin with remarks <b>'.$remarks.'</b></p>';
                  $data = array(
                    'name' => $fetch_agent->agent_name,
                    'email' =>$fetch_agent->company_email
                  );
                  Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data,$booking_type) {
                    $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                    $m->to($data['email'], $data['name'])->subject(strtoupper($booking_type).' BOOKING CONFIRMED SUCCESSFULLY ');
                  });

                  $fetch_subsequent_booking=Bookings::where('booking_sep_id',$booking_id)->get();

             foreach($fetch_subsequent_booking as $subsequent_booking)
             {
              $booking_type=$subsequent_booking->booking_type;
              $booking_supplier_id=$subsequent_booking->booking_supplier_id;

              if($booking_type!="sightseeing")
              {
               if($booking_supplier_id!=null && $booking_supplier_id!="")
               {

                $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

                      //Supplier Cancellation Acknowledgement
                       $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>Congratulations ! As per your response , '.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been confirmed by admin with remarks <b>'.$remarks.'</b>. Please login into your supplier portal to get more information. '.route('supplier').'</p>';
                    $data_supplier= array(
                      'name' => $fetch_supplier->supplier_name,
                      'email' =>$fetch_supplier->company_email
                    );
                    Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier,$booking_type) {
                      $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                      $m->to($data_supplier['email'], $data_supplier['name'])->subject(strtoupper($booking_type).' CONFIRMED BY ADMIN');
                    });


              }

            }

          }

                

            echo "reload_success";
          }
          else
          {
            echo "fail";
          }

        }

      }
      else
      { 
         if($check_booking['booking_status']==2)
        {
          echo "cancel";
        }
        else
        {
          $update_booking=Bookings::where('booking_id',$booking_id)->update(["booking_admin_status"=>1,"booking_admin_message"=>$remarks]);

          if($update_booking)
          {
              $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $fetch_admin=Users::where('users_pid',0)->first();

              $agent_id=$check_booking['booking_agent_id'];

              $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

              //Agent Cancellation Acknowledgement
              if($check_booking['booking_sightseeing_id']==null || $check_booking['booking_sightseeing_id']=="")
              {
                if($check_booking['itinerary_status']==null)
                {
                  $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Congratulations ! Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is successfully confirmed by Traveldoor Admin with remarks <b>'.$remarks.'</b></p>';
                  $data = array(
                    'name' => $fetch_agent->agent_name,
                    'email' =>$fetch_agent->company_email
                  );
                  Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data,$booking_type) {
                    $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                    $m->to($data['email'], $data['name'])->subject(strtoupper($booking_type).' BOOKING CONFIRMED SUCCESSFULLY ');
                  });
                }
                   
              }
          

            if($booking_type!="sightseeing")
            {
                  $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>Congratulations ! As per your response , '.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been confirmed by admin with remarks <b>'.$remarks.'</b>. Please login into your supplier portal to get more information. '.route('supplier').'</p>';
                    $data_supplier= array(
                      'name' => $fetch_supplier->supplier_name,
                      'email' =>$fetch_supplier->company_email
                    );
                    Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier,$booking_type) {
                      $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                      $m->to($data_supplier['email'], $data_supplier['name'])->subject(strtoupper($booking_type).' CONFIRMED BY ADMIN');
                    });
            }
           

            echo "success";
          }
          else
          {
            echo "fail";
          }
        }         
      }
    }
  
  }
  else
  {

    $check_booking=Bookings::where('booking_id',$booking_id)->first();
    if(!empty($check_booking))
    {
      if($check_booking['booking_type']=="itinerary")
      {
         if($check_booking['booking_status']==2)
        {
          echo "cancel";
        }
        else
        {
        $booking_sep_id=$check_booking['booking_sep_id'];
        $update_booking=Bookings::where('booking_sep_id',$booking_sep_id)->update(["booking_admin_status"=>2,"booking_admin_message"=>$remarks]);

        if($update_booking)
        {
           $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $fetch_admin=Users::where('users_pid',0)->first();

              $agent_id=$check_booking['booking_agent_id'];

              $fetch_agent=Agents::where('agent_id',$agent_id)->first();
              //Agent Request Acceot
                  $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Sorry ! Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is successfully rejected by Traveldoor Admin with remarks <b>'.$remarks.'</b></p>';
                  $data = array(
                    'name' => $fetch_agent->agent_name,
                    'email' =>$fetch_agent->company_email
                  );
                  Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data,$booking_type) {
                    $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                    $m->to($data['email'], $data['name'])->subject(strtoupper($booking_type).' BOOKING REJECTED ');
                  });

                  $fetch_subsequent_booking=Bookings::where('booking_sep_id',$booking_id)->get();

             foreach($fetch_subsequent_booking as $subsequent_booking)
             {
              $booking_type=$subsequent_booking->booking_type;
              $booking_supplier_id=$subsequent_booking->booking_supplier_id;

              if($booking_type!="sightseeing")
              {
               if($booking_supplier_id!=null && $booking_supplier_id!="")
               {

                $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

                      //Supplier Cancellation Acknowledgement
                       $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>Sorry ! As per your response , '.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been rejected by admin with remarks <b>'.$remarks.'</b>. Please login into your supplier portal to get more information. '.route('supplier').'</p>';
                    $data_supplier= array(
                      'name' => $fetch_supplier->supplier_name,
                      'email' =>$fetch_supplier->company_email
                    );
                    Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier,$booking_type) {
                      $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                      $m->to($data_supplier['email'], $data_supplier['name'])->subject(strtoupper($booking_type).' REJECTED BY ADMIN');
                    });


              }

            }

          }

          echo "reload_success";
        }
        else
          echo "fail";
      }

      }
      else
      {
         if($check_booking['booking_status']==2)
        {
          echo "cancel";
        }
        else
        {
          $update_booking=Bookings::where('booking_id',$booking_id)->update(["booking_admin_status"=>2,"booking_admin_message"=>$remarks]);

        if($update_booking)
        {
           $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $fetch_admin=Users::where('users_pid',0)->first();

              $agent_id=$check_booking['booking_agent_id'];

              $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

              //Agent Cancellation Acknowledgement
              if($check_booking['booking_sightseeing_id']==null || $check_booking['booking_sightseeing_id']=="")
              {
                if($check_booking['itinerary_status']==null)
                {
                  $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Sorry ! Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is successfully rejected by Traveldoor Admin with remarks <b>'.$remarks.'</b></p>';
                  $data = array(
                    'name' => $fetch_agent->agent_name,
                    'email' =>$fetch_agent->company_email
                  );
                  Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data,$booking_type) {
                    $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                    $m->to($data['email'], $data['name'])->subject(strtoupper($booking_type).' BOOKING REJECTED');
                  });
                }
                   
              }
          

            if($booking_type!="sightseeing")
            {
                  $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>Sorry ! As per your response , '.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been rejected by admin with remarks <b>'.$remarks.'</b>. Please login into your supplier portal to get more information. '.route('supplier').'</p>';
                    $data_supplier= array(
                      'name' => $fetch_supplier->supplier_name,
                      'email' =>$fetch_supplier->company_email
                    );
                    Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier,$booking_type) {
                      $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                      $m->to($data_supplier['email'], $data_supplier['name'])->subject(strtoupper($booking_type).' REJECTED BY ADMIN');
                    });
            }
          echo "success";
        }
        else
        {
          echo "fail";
        }
      }
      }
    }


  
  }
}
public function customer_bookings(Request $request)
{
  if(session()->has('travel_users_id'))
  {
    $agent_id=session()->get('travel_users_id');
    $fetch_bookings=BookingCustomer::get();
    return view('mains.customer-bookings')->with(compact('fetch_bookings'));
  }
  else
  {
    return redirect()->route('index');
  }
}
public function pdf(Request $request)
{
  $data="hello";
  $pdf = PDF::loadView('pdf.invoice',["data"=>$data]);
  return $pdf->stream();
  // return view('pdf.invoice',["data"=>$data]); 
}
public function agent_invoice(Request $request)
{
   if(session()->has('travel_users_id'))
  {
  $countries=Countries::get();
  $booking_id=$request->get('booking_id');
  $fetch_data=Bookings::where("booking_sep_id",$booking_id)->get();
  $fetch_data=$fetch_data->toArray();
   $agent_info=Agents::where('agent_id',$fetch_data[0]['booking_agent_id'])->first();
  $pdf = PDF::loadView('pdf.invoice',["data"=>$fetch_data,"countries"=>$countries,"customer_type"=>"agent","agent_info"=>$agent_info]);
 return $pdf->download('Agent-Invoice-'.$booking_id.".pdf");
  // return view('pdf.invoice',["data"=>$data]); 
   }
  else
  {
    return redirect()->route('index');
  }
}

public function customer_invoice(Request $request)
{

   if(session()->has('travel_users_id'))
  {
  $countries=Countries::get();
  $booking_id=$request->get('booking_id');
  $fetch_data=Bookings::where("booking_sep_id",$booking_id)->get();
  $fetch_data=$fetch_data->toArray();
  $agent_info=Agents::where('agent_id',$fetch_data[0]['booking_agent_id'])->first();
  $pdf = PDF::loadView('pdf.invoice',["data"=>$fetch_data,"countries"=>$countries,"customer_type"=>"customer","agent_info"=>$agent_info]);
   return $pdf->download('Customer-Invoice-'.$booking_id.".pdf");
  // return view('pdf.invoice',["data"=>$data]); 
  }
  else
  {
    return redirect()->route('index');
  }
}

public function exchange_rates(Request $request)
{
  $rights=$this->rights('exchange-rates');
     $room_currency_array=array("USD","EUR");
    $currency_price_array=array();

    // if(session()->has('currencies'))
    // {
    //   $currency_price_array=session()->get('currencies');
    // }
    // else
    // {
    if(strpos($rights['admin_which'],'view')!==false || $rights['view']==1)
      {
     foreach($room_currency_array as $currency)
      {
           $new_currency=$currency;

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$new_currency."_".$this->base_currency;

        //  $conversion_price=$convertedPriceArray->$currency_attribute;
          
        //  $currency_price_array[$currency]=round($conversion_price,2);
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);

           // session()->get('currencies',$currency_price_array[$currency]);
         
      }
    }
    else
    {
      $currency_price_array=array();
    }
 
    // }
      $currency=Currency::orderBy('code','asc')->get();

      return view('mains.exchange-rates',["currency"=>$currency])->with(compact('rights',$rights))->with(compact("currency_price_array",$currency_price_array));


}

public function get_exchange_rates(Request $request)
{
    $from_currency=$request->get('from_currency');
           $new_currency=$request->get('to_currency');
           $amount=$request->get('amount');

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$from_currency."_".$new_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$from_currency."_".$new_currency;

        //  $conversion_price=round($convertedPriceArray->$currency_attribute,2);
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$from_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=$row->nodeValue;
                    }
                  }
                 
                }
                 $conversion_price=round($conversion_price,2);
        

         $calculate_amount=round($amount*$conversion_price,2);

         echo $new_currency." ".$calculate_amount;


}

public function get_booking_and_installment_detail(Request $request)
{
  $booking_id=$request->get('booking_id');
  $request_type=$request->get('request_type');
  $data=array();
  $total_amount=0;
  $get_booking=Bookings::where('booking_sep_id',$booking_id)->first();
  if(!empty($get_booking))
  {
    $total_amount=$get_booking->booking_agent_amount;
  }
  else
  {
    $total_amount=0;
  }
  $data["total_amount"]=$total_amount;
 $table='<div class="table-responsive">
                        <table  id="installments_table" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>S. No.</th>
                              <th>Installment Date</th>
                              <th>Amount to be paid</th>
                               <th>Paid On</th>
                              <th>Status</th>';
                              if($request_type=="agent")
                               {
                                 $table.='<th>Admin Approval</th>
                                 <th>Action</th>';
                               }
                               else
                               {
                                   $table.='<th>Payment Mode</th><th>Payment Info(Transaction ID /Reference ID)</th><th>Payment Attachments</th><th>Admin Approval</th><th>Admin Remarks</th>
                                   ';
                               }
                               $table.='</tr>
                          </thead>
                          <tbody>';
                          
  $get_booking_installment=BookingInstallment::where('booking_id',$booking_id)->get();
  if(count($get_booking_installment)>0)
  {
    $pending_amount=0;
     $received_amount=0;
     $remaining_amount=$total_amount;
     $total_amount_calculate=0;

    
                          $counter=1;
     foreach($get_booking_installment as $installments)
     {

      $total_amount_calculate+=$installments->booking_install_amount;
      if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
      {
         $received_amount+=$installments->booking_install_amount;
        
      }
      else if($installments->booking_confirm_status==0)
      {
        $pending_amount+=$installments->booking_install_amount;
      }

      if($installments->booking_confirm_status!=2 && $total_amount_calculate<=$total_amount)
      {
       $remaining_amount=$remaining_amount-$installments->booking_install_amount;
      }

      $table.="<tr>
        <td>".$counter."</td>
        <td>".date('d/m/Y h:ia',strtotime($installments->created_at))."</td>
         <td id='approve_payment_".$installments->booking_installment_id."_amount'>GEL ".$installments->booking_install_amount."</td>
          <td>".date('d/m/Y h:ia',strtotime($installments->booking_paid_date." ".$installments->booking_paid_time))."</td>";

          if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
          {
            $table.="<td>Paid</td>";
          }
          else if($installments->booking_paid_status==1 && $installments->booking_confirm_status==2)
          {
             $table.="<td>Rejected</td>";
          }
          else
            {
             $table.="<td>Pending</td>";
          }
 if($request_type=="agent" && $installments->booking_paid_status==1)
        {

          if($installments->booking_confirm_status==0)
          {
             $table.='<td><button class="btn btn-sm btn-warning">Pending</button></td>';
          }
          else if($installments->booking_confirm_status==1)
          {
             $table.='<td><button class="btn btn-sm btn-primary" disabled>Approved</button></td>';
          }
          else
          {
             $table.='<td><button class="btn btn-sm btn-danger" disabled>Rejected</button></td>';
          }
          if($installments->booking_confirm_status==0)
          {
              $table.='<td><button class="btn btn-sm btn-primary" disabled>Pending</button></td>';
          }
          else if($installments->booking_confirm_status==1)
          {
              $table.='<td><button class="btn btn-sm btn-primary" disabled>Paid</button></td>';
          }
          else
          {
            $table.='<td></td>';
          }

         
       }
       else if($request_type=="agent" && $installments->booking_paid_status==0)
       {
        $table.='<td></td><td><a class="btn btn-sm btn-primary" href="'.route('agent-get-payments').'?installmentid='.$installments->booking_installment_id.'&bookingid='.$installments->booking_id.'" >Pay</a></td>';
       }
       else
       {
        if($installments->booking_paid_status==1)
          {
         if($installments->booking_confirm_status==0)
          {
             $table.='<td>'.$installments->booking_payment_mode.'</td><td>'.$installments->booking_paid_info.'</td>';
              if($installments->booking_payment_attachments!="")
                                                                    {
              $attachments=unserialize($installments->booking_payment_attachments);
          if(count($attachments)>0)
          {
            $table.='<td>';
            $count=1;
            foreach($attachments as $attachment)
            {
              $table.='<a href="'.asset('assets/uploads/agent_payments').'/'.$attachment.'" target="_blank" class="text-primary">Attachment '.$count.'</a><br>';
               $count++;
            }
           
            $table.='</td>';
          }
          else
          {
              $table.='<td>NA</td>';
          }
          }
          else
          {
              $table.='<td>NA</td>';
          }

             $table.='<td style="white-space:nowrap;"><button class="btn btn-sm btn-primary approve_payment" id="approve_payment_'.$installments->booking_installment_id.'">Approve</button> &nbsp; <button class="btn btn-sm btn-danger reject_payment" id="reject_payment_'.$installments->booking_installment_id.'">Reject</button></td><td id="remarks_booking_'.$installments->booking_installment_id.'">'.$installments->booking_confirm_remarks.'</td>';
          }
          else if($installments->booking_confirm_status==1)
          {
             $table.='<td>'.$installments->booking_payment_mode.'</td><td>'.$installments->booking_paid_info.'</td>';
              if($installments->booking_payment_attachments!="")
                                                                    {
          $attachments=unserialize($installments->booking_payment_attachments);
          if(count($attachments)>0)
          {
            $table.='<td>';
            $count=1;
            foreach($attachments as $attachment)
            {
              $table.='<a href="'.asset('assets/uploads/agent_payments').'/'.$attachment.'" target="_blank" class="text-primary">Attachment '.$count.'</a><br>';
               $count++;
            }
           
            $table.='</td>';
          }
          else
          {
              $table.='<td>NA</td>';
          }
          }
          else
          {
              $table.='<td>NA</td>';
          }

             $table.='<td><button class="btn btn-sm btn-primary" disabled>Approved</button></td><td id="remarks_booking_'.$installments->booking_installment_id.'">'.$installments->booking_confirm_remarks.'</td>';
          }
          else
          {
             $table.='<td>'.$installments->booking_payment_mode.'</td><td>'.$installments->booking_paid_info.'</td>';
              if($installments->booking_payment_attachments!="")
                                                                    {
              $attachments=unserialize($installments->booking_payment_attachments);
          if(count($attachments)>0)
          {
            $table.='<td>';
            $count=1;
            foreach($attachments as $attachment)
            {
              $table.='<a href="'.asset('assets/uploads/agent_payments').'/'.$attachment.'" target="_blank" class="text-primary">Attachment '.$count.'</a><br>';
               $count++;
            }
           
            $table.='</td>';
          }
          else
          {
              $table.='<td>Not Available</td>';
          }
          }
          else
          {
              $table.='<td>NA</td>';
          }

             $table.='<td><button class="btn btn-sm btn-danger" disabled>Rejected</button></td><td id="remarks_booking_'.$installments->booking_installment_id.'">'.$installments->booking_confirm_remarks.'</td>';
          }

        }
        else
        {
          $table.='<td></td><td></td><td></td><td></td><td></td>';
        }

       }
       
        $table.="</tr>";

        $counter++;
     }

     if(count($get_booking_installment)<=0)
     {
      if($request_type=="agent")
      {
      $table.='<tr><td colspan=7 class="text-center">No Payments Generated Yet</td></tr>';
      }
      else
      {
         $table.='<tr><td colspan=8 class="text-center">No Payments Generated Yet</td></tr>';
      }
     
     }

  

     $data["remaining_amount"]=$remaining_amount;
      $data["received_amount"]=$received_amount;
      $data["pending_amount"]=$pending_amount;
     
  }
  else
  {
     if($request_type=="agent")
      {
      $table.='<tr><td colspan=7 class="text-center">No Payments Generated Yet</td></tr>';
      }
      else
      {
         $table.='<tr><td colspan=8 class="text-center">No Payments Generated Yet</td></tr>';
      }
     
     $data["remaining_amount"]=$total_amount;
      $data["received_amount"]=0;
      $data["pending_amount"]=0;
  }
   $table.='</tbody>
     </table>';
   $data["previous_installments_html"]=$table;

  echo json_encode($data);

  
}

public function insert_installment(Request $request)
{
  $booking_id=$request->get('booking_id');
  $amount=$request->get('pay_amount');

  $agent_id="";
  $supplier_id="";
  $booking_type="";
  $get_booking=Bookings::where('booking_sep_id',$booking_id)->first();
  if(!empty($get_booking))
  {
    $total_amount=$get_booking->booking_agent_amount;
    $booking_type=$get_booking->booking_type;
    $agent_id=$get_booking->booking_agent_id;
    $supplier_id=$get_booking->booking_supplier_id;
  }
  else
  {
    $total_amount=0;
  }


  $remaining_amount=0;
   $get_booking_installment=BookingInstallment::where('booking_id',$booking_id)->get();
  if(count($get_booking_installment)>0)
  {
     $remaining_amount=$total_amount;

     foreach($get_booking_installment as $installments)
     {
      // if($installments->booking_paid_status==1)
      // {
      //    $remaining_amount+=($total_amount-$installments->booking_install_amount);
      // }
      // else
      // {
      //   $remaining_amount+=($total_amount-$installments->booking_install_amount);
      // }

        if($installments->booking_confirm_status!=2)
        {
           $remaining_amount=$remaining_amount-$installments->booking_install_amount;
        }
     }
     
  }
  else
  {
     $remaining_amount=$total_amount;
  }

  //dont want restrictions on total amount to be paid

  // if($remaining_amount>=$amount)
  // {

$remaining_amount=$remaining_amount-$amount;
// if($remaining_amount>=0)
// {
  $insert_installment=new BookingInstallment;
  $insert_installment->booking_id=$booking_id;
  $insert_installment->booking_type=$booking_type;
  $insert_installment->booking_agent_id=$agent_id;
  $insert_installment->booking_supplier_id=$supplier_id;
  $insert_installment->booking_install_amount=$amount;
  $insert_installment->booking_install_before_amount=$amount;
  $insert_installment->booking_remaining_amount=$remaining_amount;
  $insert_installment->booking_paid_status=0;

  if($insert_installment->save())
  {
    $fetch_admin=Users::where('users_pid',0)->first();

              $fetch_agent=Agents::where('agent_id',$agent_id)->first();
              //Agent Request Acceot
                  $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>New payment has been generated for '.ucfirst($booking_type).' booking with booking id :'.$booking_id.'</p><p>You have to pay the amount GEL '.$amount.' for now.</p> <p>We will notify you if there is any remaning amount to be paid for this booking.</p> <p>Please visit this link for more details : <a href="'.route('agent-bookings').'">Click here</a></p>';
                  $data = array(
                    'name' => $fetch_agent->agent_name,
                    'email' =>$fetch_agent->company_email
                  );
                  Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data,$booking_type,$booking_id) {
                    $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                    $m->to($data['email'], $data['name'])->subject(strtoupper($booking_type).'BOOKING NEW PAYMENT with BOOKING ID '.$booking_id);
                  });

    echo "success";
  }
  else
  {
    echo "fail";
  }
// }
// else
// {
//   echo "no_more";
// }

// }
// else
// {
//   echo "more_amount";
// }
  
}



public function approve_agent_payment(Request $request)
{
   $action_perform=$request->get('action_perform');
  $installment_id=$request->get('installment_id');
  $amount=$request->get('amount');
  $remarks=$request->get('remarks');
  // echo "success";
  // die();
  if($action_perform=="approve")
  {
    $check_booking=BookingInstallment::where('booking_installment_id',$installment_id)->first();
    if(!empty($check_booking))
    {
  
          $update_booking=BookingInstallment::where('booking_installment_id',$installment_id)->update(["booking_confirm_status"=>1,"booking_confirm_remarks"=>$remarks,"booking_install_amount"=>$amount]);

          if($update_booking)
          {
            //start check whether full booking amount is paid 
                $get_booking=Bookings::where('booking_sep_id',$check_booking->booking_id)->where('itinerary_status',null)->first();
                if(!empty($get_booking))
                {
                  $total_amount=$get_booking->booking_agent_amount;
                }
                else
                {
                  $total_amount=0;
                }

                  $remaining_amount=$total_amount;

              $get_booking_installment=BookingInstallment::where('booking_id',$check_booking->booking_id)->get();
            if(count($get_booking_installment)>0)
            {
               foreach($get_booking_installment as $installments)
               {
                if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                {
                   $remaining_amount=$remaining_amount-$installments->booking_install_amount;
                  
                }
               }
             }
             if($remaining_amount==0)
             {
              //if full amount is paid i.e. remaning amount is 0 then update final_amount_status of that booking
              $update_booking=Bookings::where('booking_sep_id',$check_booking->booking_id)->update(["booking_final_amount_status"=>1]);
             }
             //end check whether full booking amount is paid 



             $payment_amount=$check_booking->booking_install_amount;
             $payment_mode=$check_booking->booking_payment_mode;
              $fetch_agent=Agents::where('agent_id',$check_booking->booking_agent_id)->first();
          $fetch_admin=Users::where('users_pid',0)->first();
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Congratulations!</p><p>Traveldoor admin has approved your payment of  <b>GEL '.$payment_amount.'</b> paid through '.$payment_mode.' against payment for booking id :'.$check_booking->booking_id.' by giving remarks " <b>'.$check_booking->booking_confirm_remarks.' </b>"</p>
        ';
        $data = array(
          'name' => $fetch_agent->agent_name,
          'email' =>$fetch_agent->company_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data['email'], $data['name'])->subject('PAYMENT APPROVED BY TRAVELDOOR ADMIN');
        });


           echo "success";     
          }
          else
          {
            echo "fail";
          }

        }

      }
      else
      {

        $check_booking=BookingInstallment::where('booking_installment_id',$installment_id)->first();
    if(!empty($check_booking))
    {
  
          $update_booking=BookingInstallment::where('booking_installment_id',$installment_id)->update(["booking_confirm_status"=>2,"booking_confirm_remarks"=>$remarks]);

          if($update_booking)
          {
            //start check whether full booking amount is paid 
                $get_booking=Bookings::where('booking_sep_id',$check_booking->booking_id)->where('itinerary_status',null)->first();
                if(!empty($get_booking))
                {
                  $total_amount=$get_booking->booking_agent_amount;
                }
                else
                {
                  $total_amount=0;
                }

                  $remaining_amount=$total_amount;

              $get_booking_installment=BookingInstallment::where('booking_id',$check_booking->booking_id)->get();
            if(count($get_booking_installment)>0)
            {
               foreach($get_booking_installment as $installments)
               {
                if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                {
                   $remaining_amount=$remaining_amount-$installments->booking_install_amount;
                  
                }
               }
             }
             if($remaining_amount==0)
             {
              //if full amount is paid i.e. remaning amount is 0 then update final_amount_status of that booking
              $update_booking=Bookings::where('booking_sep_id',$check_booking->booking_id)->update(["booking_final_amount_status"=>1]);
             }
             //end check whether full booking amount is paid 


              $payment_amount=$check_booking->booking_install_amount;
             $payment_mode=$check_booking->booking_payment_mode;
              $fetch_agent=Agents::where('agent_id',$check_booking->booking_agent_id)->first();
          $fetch_admin=Users::where('users_pid',0)->first();
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p> <p>We are sorry to inform you : </p> <p>Traveldoor admin has rejected your payment of  <b>GEL '.$payment_amount.'</b> paid through '.$payment_mode.' against payment for booking id :'.$check_booking->booking_id.' by giving remarks " <b>'.$check_booking->booking_confirm_remarks.' </b> "</p>
        ';
        $data = array(
          'name' => $fetch_agent->agent_name,
          'email' =>$fetch_agent->company_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data['email'], $data['name'])->subject('PAYMENT REJECTED BY TRAVELDOOR ADMIN');
        });


           echo "success";     
          }
          else
          {
            echo "fail";
          }

        }

      }
}






public function expense_category(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('expense-category');
        $fetch_expense_category=IncomeExpenseCategory::where('expense_category_type','expense')->get();
        return view('mains.expense-category')->with(compact('fetch_expense_category','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function expense_category_insert(Request $request)

{

    $expense_category_name=$request->get('expense_category_name');

    $check_expense_category=IncomeExpenseCategory::where('expense_category_name',$expense_category_name)->where('expense_category_type','expense')->first();

    if(!empty($check_expense_category))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $expense_category_insert=new IncomeExpenseCategory;

        $expense_category_insert->expense_category_name=$expense_category_name;

        $expense_category_insert->expense_category_type="expense";

        $expense_category_insert->expense_category_created_by=$emp_id;

        if($expense_category_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function expense_category_update(Request $request)

{

    $expense_category_id=$request->get('expense_category_id');

   $expense_category_name=$request->get('expense_category_name');

    $check_expense_category=IncomeExpenseCategory::where('expense_category_name',$expense_category_name)->where('expense_category_type','expense')->where('expense_category_id','!=',$expense_category_id)->first();

    if(!empty($check_expense_category))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_expense_category_array=array("expense_category_name"=>$expense_category_name);
        $update_expense_category=IncomeExpenseCategory::where('expense_category_id',$expense_category_id)->update($update_expense_category_array);

        if($update_expense_category)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function income_category(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('income-category');
        $fetch_income_category=IncomeExpenseCategory::where('expense_category_type','income')->get();
        return view('mains.income-category')->with(compact('fetch_income_category','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function income_category_insert(Request $request)

{

    $income_category_name=$request->get('income_category_name');

    $check_income_category=IncomeExpenseCategory::where('expense_category_name',$income_category_name)->where('expense_category_type','income')->first();

    if(!empty($check_income_category))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $income_category_insert=new IncomeExpenseCategory;

        $income_category_insert->expense_category_name=$income_category_name;

        $income_category_insert->expense_category_type="income";

        $income_category_insert->expense_category_created_by=$emp_id;

        if($income_category_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function income_category_update(Request $request)

{

    $income_category_id=$request->get('income_category_id');

   $income_category_name=$request->get('income_category_name');

    $check_income_category=IncomeExpenseCategory::where('expense_category_name',$income_category_name)->where('expense_category_type','income')->where('expense_category_id','!=',$income_category_id)->first();

    if(!empty($check_income_category))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_income_category_array=array("expense_category_name"=>$income_category_name,);
        $update_income_category=IncomeExpenseCategory::where('expense_category_id',$income_category_id)->update($update_income_category_array);

        if($update_income_category)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}



public function view_booking_expenses(Request $request)
{
  if(session()->has('travel_users_id'))

       {

       $emp_id=session()->get('travel_users_id');
    $rights=$this->rights('booking-expenses');

  $fetch_expenses=Expense::where('expenses_type','booking')->get();
  return view('mains.expense-management')->with(compact('fetch_expenses','rights'))->with('expense_type','booking');

    }

    else

    {

        return redirect()->route('index');

    }
  
}
public function add_booking_expenses(Request $request)
{

   if(session()->has('travel_users_id'))

       {

         $rights=$this->rights('booking-expenses');
       $fetch_expense_category=IncomeExpenseCategory::where('expense_category_type','expense')->where('expense_category_status',1)->get();
  $fetch_bookings=Bookings::where('booking_sightseeing_id',null)->where('itinerary_status',null)->where('booking_admin_status',1)->get();
  return view('mains.create-expense')->with(compact('rights','fetch_bookings','fetch_expense_category'))->with('expense_type','booking');

    }

    else

    {

        return redirect()->route('index');

    }
 
}
public function edit_booking_expenses(Request $request,$expense_id)
{

   if(session()->has('travel_users_id'))

       {

         $rights=$this->rights('booking-expenses');
         if(is_numeric($expense_id))
         {
           $fetch_expenses=Expense::where('expenses_id',$expense_id)->first();
           if(!empty($fetch_expenses))
           {
             $fetch_expense_category=IncomeExpenseCategory::where('expense_category_type','expense')->where('expense_category_status',1)->get();
           $fetch_bookings=Bookings::where('booking_sightseeing_id',null)->where('itinerary_status',null)->where('booking_admin_status',1)->get();
           return view('mains.edit-expense')->with(compact('rights','fetch_bookings','fetch_expense_category','fetch_expenses'))->with('expense_type','booking');
           }
           else
            return redirect()->back();      
         }
         else
          return redirect()->back();
    }

    else
    {
        return redirect()->route('index');
    }
 
}

public function view_office_expenses(Request $request)
{


   if(session()->has('travel_users_id'))

       {
 $emp_id=session()->get('travel_users_id');
    $rights=$this->rights('office-expenses');
  $fetch_expenses=Expense::where('expenses_type','office')->get();
  return view('mains.expense-management')->with(compact('fetch_expenses','rights'))->with('expense_type','office');

    }

    else

    {

        return redirect()->route('index');

    }
  
}
public function add_office_expenses(Request $request)
{


   if(session()->has('travel_users_id'))

       {
  $rights=$this->rights('office-expenses');
    $fetch_expense_category=IncomeExpenseCategory::where('expense_category_type','expense')->where('expense_category_status',1)->get();
  return view('mains.create-expense')->with(compact('rights','fetch_expense_category'))->with('expense_type','office');

    }

    else

    {

        return redirect()->route('index');

    }
  
}
public function edit_office_expenses(Request $request,$expense_id)
{

   if(session()->has('travel_users_id'))

       {

         $rights=$this->rights('office-expenses');
         if(is_numeric($expense_id))
         {
           $fetch_expenses=Expense::where('expenses_id',$expense_id)->first();
           if(!empty($fetch_expenses))
           {
             $fetch_expense_category=IncomeExpenseCategory::where('expense_category_type','expense')->where('expense_category_status',1)->get();
           return view('mains.edit-expense')->with(compact('rights','fetch_expense_category','fetch_expenses'))->with('expense_type','office');
           }
           else
            return redirect()->back();      
         }
         else
          return redirect()->back();
    }

    else
    {
        return redirect()->route('index');
    }
 
}

public function insert_expenses(Request $request)
{
   if(session()->has('travel_users_id'))

       {
  $expense_category=$request->get('expense_category');
   $expense_booking_id=$request->get('expense_booking_id');
   $expense_type=$request->get('expense_type');
   $expense_occured=$request->get('expense_occured');
   $expense_amount=$request->get('expense_amount');
   $expense_remarks=$request->get('expense_remarks');
  

        $emp_id=session()->get('travel_users_id');

        $expense_insert=new Expense;
        $expense_insert->expenses_type=$expense_type;
        $expense_insert->expense_category_id=$expense_category;
        $expense_insert->expense_booking_id=$expense_booking_id;
        $expense_insert->expense_occured_on=$expense_occured;
        $expense_insert->expense_amount=$expense_amount;
        $expense_insert->expense_remarks=$expense_remarks;
        $expense_insert->expense_created_by=$emp_id;

        if($expense_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }
      }
      else
      {
        echo "fail";
      }

}

public function update_expenses(Request $request)
{

   if(session()->has('travel_users_id'))

       {
  $expense_id=$request->get('expense_id');
  $expense_category=$request->get('expense_category');
   $expense_booking_id=$request->get('expense_booking_id');
   $expense_type=$request->get('expense_type');
   $expense_occured=$request->get('expense_occured');
   $expense_amount=$request->get('expense_amount');
   $expense_remarks=$request->get('expense_remarks');
  
   $check_expense=Expense::where('expenses_id',$expense_id)->first();
   if(!empty($check_expense))
   {
    $update_data=array("expenses_type"=>$expense_type,
        "expense_category_id"=>$expense_category,
        "expense_booking_id"=>$expense_booking_id,
        "expense_occured_on"=>$expense_occured,
        "expense_amount"=>$expense_amount,
        "expense_remarks"=>$expense_remarks);
    $update_query=Expense::where('expenses_id',$expense_id)->update($update_data);
    if($update_query)
      echo "success";
    else
      echo "fail";
   }
   else
   {
    echo "fail"; 
   }
 }
 else
 {
  echo "fail";
 }

}


public function expense_details($expense_id)
{
    if(session()->has('travel_users_id'))
       {
    $fetch_expense_detail=Expense::where('expenses_id',$expense_id)->first();
    if(!empty($fetch_expense_detail))
    {
      $users=Users::where("users_status",1)->where('users_role','!=','Partner')->get();
      $rights=$this->rights($fetch_expense_detail->expense_type.'-expenses');
        return view('mains.expense-details')->with(compact('rights','fetch_expense_detail','users'));
    }
    else
    {
      return redirect()->back();
    }

    }
    else
    {
        return redirect()->route('index');
    }
  

}

public function view_booking_incomes(Request $request)
{
  if(session()->has('travel_users_id'))
    {
         $emp_id=session()->get('travel_users_id');
         $rights=$this->rights('booking-incomes');

         $fetch_incomes=Income::where('incomes_type','booking')->get();
         return view('mains.income-management')->with(compact('fetch_incomes','rights'))->with('incomes_type','booking');
    }
    else
    {
        return redirect()->route('index');
    }
  
}
public function add_booking_incomes(Request $request)
{

   if(session()->has('travel_users_id'))
       {

         $rights=$this->rights('booking-incomes');
         $fetch_income_category=IncomeExpenseCategory::where('expense_category_type','income')->where('expense_category_status',1)->get();
         $fetch_bookings=Bookings::where('booking_sightseeing_id',null)->where('itinerary_status',null)->where('booking_admin_status',1)->get();
         return view('mains.create-income')->with(compact('rights','fetch_bookings','fetch_income_category'))->with('incomes_type','booking');

    }
    else
    {
        return redirect()->route('index');
    }

}
public function edit_booking_incomes(Request $request,$income_id)
{

   if(session()->has('travel_users_id'))

       {

         $rights=$this->rights('booking-incomes');
         if(is_numeric($income_id))
         {
           $fetch_incomes=Income::where('incomes_id',$income_id)->first();
           if(!empty($fetch_incomes))
           {
             $fetch_income_category=IncomeExpenseCategory::where('expense_category_type','income')->where('expense_category_status',1)->get();
         $fetch_bookings=Bookings::where('booking_sightseeing_id',null)->where('itinerary_status',null)->where('booking_admin_status',1)->get();
         return view('mains.edit-income')->with(compact('rights','fetch_bookings','fetch_income_category','fetch_incomes'))->with('incomes_type','booking');
           }
           else
            return redirect()->back();      
         }
         else
          return redirect()->back();
    }

    else
    {
        return redirect()->route('index');
    }
 
}




public function view_office_incomes(Request $request)
{


   if(session()->has('travel_users_id'))

       {
 $emp_id=session()->get('travel_users_id');
    $rights=$this->rights('office-incomes');
  $fetch_incomes=Income::where('incomes_type','office')->get();
  return view('mains.income-management')->with(compact('fetch_incomes','rights'))->with('incomes_type','office');

    }

    else

    {

        return redirect()->route('index');

    }
  
}
public function add_office_incomes(Request $request)
{


   if(session()->has('travel_users_id'))

       {
  $rights=$this->rights('office-incomes');
    $fetch_income_category=IncomeExpenseCategory::where('expense_category_type','income')->where('expense_category_status',1)->get();
  return view('mains.create-income')->with(compact('rights','fetch_income_category'))->with('incomes_type','office');

    }

    else

    {

        return redirect()->route('index');

    }
  
}
public function edit_office_incomes(Request $request,$income_id)
{

   if(session()->has('travel_users_id'))

       {

         $rights=$this->rights('office-incomes');
         if(is_numeric($income_id))
         {
           $fetch_incomes=Income::where('incomes_id',$income_id)->first();
           if(!empty($fetch_incomes))
           {
             $fetch_income_category=IncomeExpenseCategory::where('expense_category_type','income')->where('expense_category_status',1)->get();
  return view('mains.edit-income')->with(compact('rights','fetch_income_category','fetch_incomes'))->with('incomes_type','office');
           }
           else
            return redirect()->back();      
         }
         else
          return redirect()->back();
    }

    else
    {
        return redirect()->route('index');
    }
 
}


public function insert_incomes(Request $request)
{
  if(session()->has('travel_users_id'))

       {
   $income_category=$request->get('income_category');
   $income_booking_id=$request->get('income_booking_id');
   $income_type=$request->get('income_type');
   $income_occured=$request->get('income_occured');
   $income_amount=$request->get('income_amount');
   $income_remarks=$request->get('income_remarks');
  

   $emp_id=session()->get('travel_users_id');
   $income_insert=new Income;
   $income_insert->incomes_type=$income_type;
   $income_insert->incomes_category_id=$income_category;
   $income_insert->incomes_booking_id=$income_booking_id;
   $income_insert->incomes_occured_on=$income_occured;
   $income_insert->incomes_amount=$income_amount;
   $income_insert->incomes_remarks=$income_remarks;
   $income_insert->incomes_created_by=$emp_id;

   if($income_insert->save())
   {
    echo "success";
  }
  else
  {
    echo "fail";
  }
  }
  else
  {
    echo "fail";
  }
}

public function update_incomes(Request $request)
{
  if(session()->has('travel_users_id'))

       {
    $income_id=$request->get('income_id');
   $income_category=$request->get('income_category');
   $income_booking_id=$request->get('income_booking_id');
   $income_type=$request->get('income_type');
   $income_occured=$request->get('income_occured');
   $income_amount=$request->get('income_amount');
   $income_remarks=$request->get('income_remarks');
  

  $check_income=Income::where('incomes_id',$income_id)->first();
   if(!empty($check_income))
   {
    $update_data=array("incomes_type"=>$income_type,
        "incomes_category_id"=>$income_category,
        "incomes_booking_id"=>$income_booking_id,
        "incomes_occured_on"=>$income_occured,
        "incomes_amount"=>$income_amount,
        "incomes_remarks"=>$income_remarks);
    $update_query=Income::where('incomes_id',$income_id)->update($update_data);
    if($update_query)
      echo "success";
    else
      echo "fail";
   }
   else
   {
    echo "fail"; 
   }
 }
 else
 {
  echo "fail";
 }

}



public function income_details($income_id)
{
    if(session()->has('travel_users_id'))
       {
    $fetch_income_detail=Income::where('incomes_id',$income_id)->first();
    if(!empty($fetch_income_detail))
    {
      $users=Users::where("users_status",1)->where('users_role','!=','Partner')->get();
      $rights=$this->rights($fetch_income_detail->incomes_type.'-incomes');
      return view('mains.income-details')->with(compact('rights','fetch_income_detail','users'));
    }
    else
    {
      return redirect()->back();
    }

    }
    else
    {
        return redirect()->route('index');
    }
}




public function booking_complete(Request $request)
{
  $booking_id=$request->get('booking_id');
 
  $check_booking=Bookings::where('booking_sep_id',$booking_id)->where('itinerary_status',null)->first();
  if(!empty($check_booking))
  {
    if($check_booking->booking_final_amount_status==0)
    {
      echo "payment_error";
    }
    else if($check_booking->booking_final_amount_status==1 && $check_booking->booking_complete_status==1)
    {
      echo "already";
    }
     else if($check_booking->booking_final_amount_status==1 && $check_booking->booking_complete_status==0)
     {
      $update_booking=Bookings::where('booking_sep_id',$booking_id)->update(["booking_complete_status"=>1,"booking_complete_timestamp"=>date("Y-m-d H:i:s")]);
      
      if($update_booking)
      {
        echo "success";
      }
      else
      {
        echo "fail";
      }
     }
     else
     {
      echo "fail";
     }
  }
  else
  {
    echo "no_book";
  }
}


}