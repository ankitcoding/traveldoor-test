<?php
  namespace App\Http\Controllers;
  use App\Activities;
  use App\ActivityType;
  use App\AirportMaster;
  use App\Transfers;
  use App\TransferDetails;
  use App\Vehicles;
  use App\TourType;
  use App\HotelType;
  use App\Hotels;
  use App\HotelRooms;
  use App\HotelRoomSeasons;
  use App\HotelRoomSeasonOccupancy;
  use App\HotelMeal;
  use App\Currency;
  use App\Countries;
  use App\Cities;
  use App\Users;
  use App\Suppliers;
  use App\Guides;
  use App\Drivers;
  use App\GuideExpense;
  use App\Languages;
  use App\Bookings;
  use App\BookingCustomer;
  use App\SightSeeing;
  use App\SavedItinerary;
  use App\VehicleType;
  use App\Amenities;
  use App\SubAmenities;
  use App\CustomerMarkup;
  use App\Restaurants;
use App\RestaurantFood;
use App\RestaurantMenuCategory;
use App\RestaurantType;
  use Session;
  use Mail;
  use App\Mail\SendMailable;

  use Illuminate\Http\Request;

  class ApiController extends Controller
  {

     private $currencyApiKey;
    private $base_currency;
    public function __construct()
    {
      date_default_timezone_set('Asia/Dubai');
      $this->currencyApiKey="f4f2d4f26341429dcf7e";
       $this->base_currency="GEL";
    }


    public function getCountries(Request $request)
    {
      $fetch_countries=Countries::where("country_status",1)->get();

      if(count($fetch_countries)>0)
      {
        $data=array();
        $data=$fetch_countries;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

    public function getActivityType(Request $request)
    {
        $fetch_activity_type=ActivityType::where('activity_type_status',1)->get();

      if(count($fetch_activity_type)>0)
      {
        $data=array();
        $data=$fetch_activity_type;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

    public function getTourType(Request $request)
    {
        $fetch_tour_type=TourType::where('tour_type_status',1)->get();

      if(count($fetch_tour_type)>0)
      {
        $data=array();
        $data=$fetch_tour_type;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }
    public function getHotelType(Request $request)
    {
        $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();

      if(count($fetch_hotel_type)>0)
      {
        $data=array();
        $data=$fetch_hotel_type;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }


    
    public function getLanguages(Request $request)
    {
      $fetch_langauges=Languages::where("language_status",1)->get();

      if(count($fetch_langauges)>0)
      {
        $data=array();
        $data=$fetch_langauges;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

    public function getRestaurantType(Request $request)
    {
        $fetch_restaurant_type=RestaurantType::where('restaurant_type_status',1)->get();

      if(count($fetch_restaurant_type)>0)
      {
        $data=array();
        $data=$fetch_restaurant_type;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

    public function getCities(Request $request)
    {
      $country_id=$request->get('country_id');

      $service_type=$request->get('service_type');

      $get_countries=Countries::where('country_id',$country_id)->first();

      if($get_countries)

      {
        $get_cities=array();
        if($service_type=="restaurant")
        {
          $get_cities=Restaurants::distinct()->where('restaurant_status',1)->select('restaurant_city')->get();
        }
        else if($service_type=="activities")
        {
          $get_cities=Activities::distinct()->where('activity_status',1)->where('activity_approve_status',1)->select('activity_city')->get();
        }
         else if($service_type=="hotel")
        {
          $get_cities=Hotels::distinct()->where('hotel_status',1)->where('hotel_approve_status',1)->select('hotel_city')->get();
        }
        else if($service_type=="guide")
        {
          $get_cities=Guides::distinct()->where('guide_status',1)->select('guide_city')->get();
        }
         else if($service_type=="sightseeing")
        {
          $get_cities_from=Sightseeing::distinct()->where('sightseeing_status',1)->select('sightseeing_city_from')->get();
          $get_cities_to=Sightseeing::distinct()->where('sightseeing_status',1)->select('sightseeing_city_to')->get();

              $get_cities_from=$get_cities_from->toArray();
            $get_cities_to=$get_cities_to->toArray();
          $get_cities=array_merge($get_cities_from,$get_cities_to);
        }
        else if($service_type=="transfer")
        {
          $get_cities_airport_from=TransferDetails::distinct()->where('transfer_details_type','from-airport')->select('to_city_airport')->get();
          $get_cities_airport_to=TransferDetails::distinct()->where('transfer_details_type','to-airport')->select('from_city_airport')->get();
           $get_cities_city_from=TransferDetails::distinct()->where('transfer_details_type','city')->select('from_city_airport')->get();
            $get_cities_city_to=TransferDetails::distinct()->where('transfer_details_type','city')->select('to_city_airport')->get();

            $get_cities_airport_from=$get_cities_airport_from->toArray();
            $get_cities_airport_to=$get_cities_airport_to->toArray();
            $get_cities_city_from=$get_cities_city_from->toArray();
            $get_cities_city_to=$get_cities_city_to->toArray();
            $get_cities=array_merge($get_cities_airport_from,$get_cities_airport_to,$get_cities_city_from,$get_cities_city_to);
    
        }
         else if($service_type=="package")
        {
           $get_cities_itinerary=SavedItinerary::where('itinerary_bc_status',1)->get();

           foreach($get_cities_itinerary as $city_itinerary)
           {
            $all_cities=explode(",",$city_itinerary->itinerary_package_cities);
            $get_cities=array_merge($get_cities, $all_cities);
           }
        }

        // $citydata="<option value='0' hidden>SELECT CITY</option>";

        $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country_id)->whereIn("cities.id",$get_cities)->select("cities.*")->orderBy('cities.name','asc')->get();

        // foreach($fetch_cities as $cities)

        // {

        //  $citydata.="<option value='".$cities->id."'>".$cities->name."</option>";

        // }

        // echo $citydata;

        if(count($fetch_cities)>0)
        {
          $data=array();
          $data=$fetch_cities;
          return json_encode($data);
        }
        else
        {
          $data=array();
          return json_encode($data);
        }

      }

      else

      {
 $data=array();
          return json_encode($data);
      }
    }

    public function fetchActivites(Request $request)
    {
      $fetch_activites=Activities::where('activity_status',1)->orderBy('activity_id','desc')->get();
      if(count($fetch_activites)>0)
      {
        $data=array();
        $fetch_activites['markup']= 10;
        $data=$fetch_activites;
        
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

    public function filteredfetchActivites(Request $request)
    {

      $country_id=$request->get('country_id');
      $city_id=$request->get('city_id');
      $date_from=$request->get('date_from');
        $activity_type=$request->get('activity_type');
    // $date_to=$request->get('date_to');
      $limit=9;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }
    
       $activity_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Activity")->first();
      if($fetch_markup)
      {
        $activity_markup=$fetch_markup['customer_markup_cost'];

      }


      $fetch_activites=Activities::where('activity_country',$country_id)->where('activity_city',$city_id);

      if($date_from!="")
      {
        $fetch_activites=$fetch_activites->where('validity_fromdate',"<=",$date_from)->where('validity_todate',">=",$date_from);
      }

      if($activity_type!="0")
      {
        $fetch_activites=$fetch_activites->where('activity_type',$activity_type);
      }

      $fetch_activites=$fetch_activites->where('activity_approve_status',1)->where('activity_status',1)->orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','desc')->offset($offset*$limit)->take($limit)->get();

      if(count($fetch_activites)>0)
      {
        $data=array();
          $fetch_activites['markup']= $activity_markup;
        $data=$fetch_activites;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

     public function fetchActivitiesPricing(Request $request)
    {
      $pax_type=$request->get('pax_type');
      $pax_qty=$request->get('pax_qty');
       $activity_id=$request->get('activity_id');
       $activity_markup=$request->get('markup');

      $get_activities=Activities::where('activity_id',$activity_id)->where('activity_status',1)->first();

      if(!empty($get_activities))
      {

        $data=array();
        
       
        if($pax_type=="adult")
        {
           $adult_price=0;

        $adult_price_details=unserialize($get_activities->adult_price_details);
        if(!empty($adult_price_details))
        {
          $min_price=0;

         for($i=0;$i< count($adult_price_details);$i++)
         {

          if($adult_price_details[$i]['adult_min_pax']<=$pax_qty && $adult_price_details[$i]['adult_max_pax']>=$pax_qty)
          {
            $adult_price=$adult_price_details[$i]['adult_pax_price'];
            break;
          }

          if($adult_price_details[0]['adult_min_pax']>$pax_qty)
          {
            $min_price++;
          }
        }

        if($adult_price==0 && $min_price>0)
        {
         $adult_price=$adult_price_details[0]['adult_pax_price'];

       }
       else if($adult_price==0 && $min_price==0)
       {
        $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];
       }

        }
        else
        {
          $adult_price=0;
        }
       
      
        $markup_cost=round(($adult_price*$activity_markup)/100);
        $total_cost=round(($adult_price+$markup_cost));

           $data["cost_t"]=$total_cost;
        }
        else if($pax_type=="child")
        {
           $child_price=0;

            $child_price_details=unserialize($get_activities->child_price_details);
            if(!empty($child_price_details))
            {
               $min_price=0;
              for($i=0;$i< count($child_price_details);$i++)
              {

                if($child_price_details[$i]['child_min_pax']<=$pax_qty && $child_price_details[$i]['child_max_pax']>=$pax_qty)
                {
                  $child_price=$child_price_details[$i]['child_pax_price'];
                  break;
                }

                if($child_price_details[0]['child_min_pax']>$pax_qty)
                {
                  $min_price++;
                }

              }

              // if($child_price==0)
              // {
              //      $child_price=$child_price_details[($i-1)]['child_pax_price'];

              // }


                if($child_price==0 && $min_price>0)
                {
                 $child_price=$child_price_details[0]['child_pax_price'];

               }
               else if($child_price==0 && $min_price==0)
               {
                if($i==0)
                {
          $child_price=$child_price_details[0]['child_pax_price'];
                }
                else
                {
                   $child_price=$child_price_details[($i-1)]['child_pax_price'];
                }
              
               }

           }
           else
           {
            $child_price=0;
          }


          $markup_cost=round(($child_price*$activity_markup)/100);
          $total_cost=round(($child_price+$markup_cost));
           $data["cost_t"]=$total_cost;

        }
        else if($pax_type=="infant")
        {

          $infant_price=0;

            $infant_price_details=unserialize($get_activities->infant_price_details);
            if(!empty($infant_price_details))
            {
               $min_price=0;
               for($i=0;$i< count($infant_price_details);$i++)
              {

                if($infant_price_details[$i]['infant_min_pax']<=$pax_qty && $infant_price_details[$i]['infant_max_pax']>=$pax_qty)
                {
                   $infant_price=$infant_price_details[$i]['infant_pax_price'];
                  break;
                }

                 if($infant_price_details[0]['infant_min_pax']>$pax_qty)
                {
                  $min_price++;
                }

              }

                if($infant_price==0 && $min_price>0)
                {
                $infant_price=$infant_price_details[0]['infant_pax_price'];

               }
               else  if($infant_price==0 && $min_price==0)
               {
               $infant_price=$infant_price_details[($i-1)]['infant_pax_price'];
               }

            
           }
           else
           {
            $infant_price=0;
           }


          $markup_cost=round(($infant_price*$activity_markup)/100);
          $total_cost=round(($infant_price+$markup_cost));
           $data["cost_t"]=$total_cost;

        }
        
        // echo json_encode($data);
      return json_encode($data);

      }

    }

    public function activityDetailView(Request $request)
    {

      $activity_id=$request->get('activity_id');

      $get_activities=Activities::where('activity_id',$activity_id)->where('activity_status',1)->first();
      if($get_activities)
      {
        
        $get_dates_from=strtotime($get_activities->validity_fromdate);
        $get_dates_to=strtotime($get_activities->validity_todate);

        $blackout_days=explode(',',$get_activities->activity_blackout_dates);

        $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;
        $dates="";
        for($count=0;$count<=$date_diff;$count++)
        {
          $calculated_date=date("Y-m-d",strtotime("+$count day",strtotime($get_activities->validity_fromdate)));

          if(!in_array($calculated_date, $blackout_days))
          {
            $dates.=$calculated_date;
            if($count<$date_diff)
            {
              $dates.=", ";
            } 
          }

        }
        $countries=Countries::get();
        foreach($countries as $country)
        {
          if($country->country_id==$get_activities->activity_country)
          {
            $country_name=$country->country_name;
          }


        }


        $fetch_city=ServiceManagement::searchCities($get_activities->activity_city,$get_activities->activity_country);
        $city_name=$fetch_city['name'];

         $fetch_markup=CustomerMarkup::where('customer_markup',"Activity")->first();
      if($fetch_markup)
      {
        $activity_markup=$fetch_markup['customer_markup_cost'];

      }

        $activity_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "activity"=>$get_activities,
          "country"=>$country_name,
          "city"=>$city_name,
          "enabled_dates"=>$dates,
          "markup"=>$activity_markup);
        return json_encode($activity_data);
      }
      else
      {
        $activity_data=array("errorCode"=>"1","errorMessage"=>"No data Found");
        return json_encode($activity_data);
      }

    }

    public function activityBooked(Request $request)
    {

      $fetch_admin=Users::where('users_pid',0)->first();
      $login_customer_id=$request->get('login_customer_id');
      $login_customer_name=$request->get('login_customer_name');
      $login_customer_email=$request->get('login_customer_email');
        $booking_whole_data=serialize($request->all());
      $customer_name=$request->get('customer_name');
      $customer_email=$request->get('customer_email');  
      $customer_phone=$request->get('customer_phone');  
      $customer_country=$request->get('customer_country');  
      $customer_address=$request->get('customer_address');  
      $customer_remarks=$request->get('customer_remarks'); 
      $activity_id=$request->get('activity_id');
      $booking_supplier_id=$request->get('booking_supplier_id');
      $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
      $booking_amount=$request->get('booking_amount');
      $booking_date=$request->get('booking_date');
      $booking_selected_time=$request->get('booking_time');
      $adult_price=$request->get('adult_price');
      $booking_adult_count=$request->get('booking_adult_count');
      $booking_child_count=$request->get('booking_child_count');
      $booking_infant_count=$request->get('booking_infant_count');
      $customer_adult_price=$request->get('customer_adult_price');
      $customer_child_price=$request->get('customer_child_price');
      $customer_infant_price=$request->get('customer_infant_price');
      $booking_markup_per=$request->get('booking_markup_per');
      $get_activities=Activities::where('activity_id',$activity_id)->where('activity_status',1)->first();
      if($get_activities)
      {

         $adult_price=0;

       $adult_price_details=unserialize($get_activities->adult_price_details);
       if(!empty($adult_price_details))
       {
         $min_price=0;

         for($i=0;$i< count($adult_price_details);$i++)
         {

          if($adult_price_details[$i]['adult_min_pax']<=$booking_adult_count && $adult_price_details[$i]['adult_max_pax']>=$booking_adult_count)
          {
            $adult_price=$adult_price_details[$i]['adult_pax_price'];
            break;
          }

          if($adult_price_details[0]['adult_min_pax']>$booking_adult_count)
          {
            $min_price++;
          }
        }

        if($adult_price==0 && $min_price>0)
        {
         $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];

       }
       else if($adult_price==0 && $min_price==0)
       {
        if($i==0)
        {
          $adult_price=$adult_price_details[0]['adult_pax_price'];
        }
        else
        {
          $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];
        }
                
       }

     }
     else
     {
      $adult_price=0;
      }



           $child_price=0;

            $child_price_details=unserialize($get_activities->child_price_details);
            if(!empty($child_price_details))
            {
               $min_price=0;
              for($i=0;$i< count($child_price_details);$i++)
              {

                if($child_price_details[$i]['child_min_pax']<=$booking_child_count && $child_price_details[$i]['child_max_pax']>=$booking_child_count)
                {
                  $child_price=$child_price_details[$i]['child_pax_price'];
                  break;
                }

                 if($child_price_details[0]['child_min_pax']>$booking_child_count)
                  {
                    $min_price++;
                  }
              }

              if($child_price==0 && $min_price>0)
                {
                 $child_price=$child_price_details[0]['child_pax_price'];

               }
               else if($child_price==0 && $min_price==0)
               {
                if($i==0)
                {
                  $child_price=$child_price_details[0]['child_pax_price'];
                }
                else
                {
                  $child_price=$child_price_details[($i-1)]['child_pax_price'];
                }
                 
               }

           }
           else
           {
            $child_price=0;
          }



            $infant_price=0;

            $infant_price_details=unserialize($get_activities->infant_price_details);
            if(!empty($infant_price_details))
            {
                   $min_price=0;
                   for($i=0;$i< count($infant_price_details);$i++)
                   {

                    if($infant_price_details[$i]['infant_min_pax']<=$booking_infant_count && $infant_price_details[$i]['infant_max_pax']>=$booking_infant_count)
                    {
                     $infant_price=$infant_price_details[$i]['infant_pax_price'];
                     break;
                   }

                     if($infant_price_details[0]['infant_min_pax']>$booking_infant_count)
                     {
                      $min_price++;
                      }
                }

              if($infant_price==0 && $min_price>0)
              {
                 $infant_price=$infant_price_details[0]['infant_pax_price'];

               }
               else if($infant_price==0 && $min_price==0)
               {
                  if($i==0)
                {
                   $infant_price=$infant_price_details[0]['infant_pax_price'];
                }
                else
                {
                   $infant_price=$infant_price_details[($i-1)]['infant_pax_price'];
                }

                
               }

          
           }
           else
           {
            $infant_price=0;
           }



        $supplier_adult_price=$adult_price;
        $supplier_child_price=$child_price;
        $supplier_infant_price=$infant_price;
         $booking_supplier_amount=0;
        if(!empty($booking_adult_count))
        {
          $booking_supplier_amount+=($booking_adult_count*$supplier_adult_price);
        }
         if(!empty($booking_child_count))
        {
          $booking_supplier_amount+=($booking_child_count*$supplier_child_price);
        }
          if(!empty($booking_infant_count))
        {
          $booking_supplier_amount+=($booking_infant_count*$supplier_infant_price);
        }
        $booking_currency=$get_activities->activity_currency;
        $date=date("Y-m-d");
        $time=date("H:i:s");
         $get_latest_id=BookingCustomer::latest()->value('booking_sep_id');
             $get_latest_id=$get_latest_id+1;
        $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="activity";
        $insert_booking->booking_type_id=$activity_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
         $insert_booking->booking_adult_count=$booking_adult_count;
        $insert_booking->booking_child_count=$booking_child_count;
        $insert_booking->booking_infant_count =$booking_infant_count;
        $insert_booking->supplier_adult_price=$supplier_adult_price;
        $insert_booking->supplier_child_price=$supplier_child_price;
        $insert_booking->supplier_infant_price=$supplier_infant_price;
         $insert_booking->customer_adult_price=$customer_adult_price;
        $insert_booking->customer_child_price=$customer_child_price;
        $insert_booking->customer_infant_price=$customer_infant_price;
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_supplier_amount=$booking_supplier_amount;
        $insert_booking->booking_amount=$booking_amount;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$booking_date;
         $insert_booking->booking_selected_time=$booking_selected_time;
         $insert_booking->booking_whole_data=$booking_whole_data;
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;
        if($insert_booking->save())
        {
          $booking_id=$get_latest_id;
          $htmldata='<p>Dear '.$customer_name.',</p><p>You have successfully booked activity on traveldoor[dot]ge . Your booking id is '.$booking_id.'.</p>
          ';
          $data = array(
            'name' => $customer_name,
            'email' =>$customer_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
          });

          $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new activity booking on traveldoor[dot]ge with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
          $data_supplier= array(
            'name' => $fetch_supplier->supplier_name,
            'email' =>$fetch_supplier->company_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW ACTIVITY BOOKING');
          });

          $htmldata='<p>Dear Admin,</p><p>New activity has been booked with booking id '.$booking_id.' by customer '.$customer_name.' for the supplier '.$fetch_supplier->supplier_name.' </p>';
          $data_admin= array(
            'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
            'email' =>$fetch_admin->users_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_admin['email'], $data_admin['name'])->subject('NEW ACTIVITY BOOKING ON PORTAL');
          });



          $result_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "result"=>"success");
          return json_encode($result_data);
        }
        else
        {
            $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
        }

      }

    }

    public function fetchHotels(Request $request)
    {
      $date_from=date('Y-m-d');
      $fetch_hotels=Hotels::where('hotel_status',1)->where('booking_validity_to',">",$date_from)->orderBy('hotel_id','desc')->get();

      if(count($fetch_hotels)>0)
      {
        $data=array();
        $fetch_hotels['markup']= 5;
        $data=$fetch_hotels;
        
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

   public function filteredfetchHotels(Request $request)
    {

      $country_id=$request->get('country_id');
      $city_id=$request->get('city_id');
      $date_from=$request->get('date_from');
      $date_to=$request->get('date_to');
      $room_quantity=$request->get('room_qty');
      $hotel_price=$request->get('hotel_price');
      $hotel_star=$request->get('hotel_star');
      $hotel_type=$request->get('hotel_type');
    // $date_to=$request->get('date_to');
      $limit=15;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }
      $hotel_price_min=$hotel_price_max=0;
      $hotel_price_low=$hotel_price_high=array();
      if(!empty($hotel_price))
      {
        foreach($hotel_price as $price_value)
        {
          $prices=explode("-",$price_value);
          $hotel_price_low[]=$prices[0];
          $hotel_price_high[]=$prices[1];
        }
        $hotel_price_min=min($hotel_price_low);
        $hotel_price_max=max($hotel_price_high);
      }

       $hotel_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Hotel")->first();
      if($fetch_markup)
      {
        $hotel_markup=$fetch_markup['customer_markup_cost'];

      }



      $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id);

      if($date_from!="")
      {
         $fetch_hotels=$fetch_hotels->where('booking_validity_from',"<=",$date_from)->where('booking_validity_to',">=",$date_from);
      }

      if(!empty($hotel_star))
      {
        $fetch_hotels=$fetch_hotels->whereIn('hotel_rating',$hotel_star);
      }
      if(!empty($hotel_type))
      {
        $fetch_hotels=$fetch_hotels->whereIn('hotel_type',$hotel_type);
      }
      //  if(($hotel_price_min==0 || $hotel_price_min>0) && $hotel_price_max>0)
      // {
      //   $fetch_hotels=$fetch_hotels->join('hotel_room_season_occupancy_price','hotel_room_season_occupancy_price.hotel_id_fk',"=","hotels.hotel_id")->where('hotel_room_occupancy_price',">=",$hotel_price_min)->where('hotel_room_occupancy_price',"<=",$hotel_price_max);
      // }

      $fetch_hotels=$fetch_hotels->where('hotel_approve_status',1)->where('hotel_status',1)->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','desc')->offset($offset*$limit)->take($limit)->get();

      $hotel_cost=array();
      foreach($fetch_hotels as $hotel_key=>$hotels)
      {

        $hotel_rooms=HotelRooms::where('hotel_id',$hotels->hotel_id)->get();

        $hotel_currency=$hotels->hotel_currency;

        $room_min_price=0;
        foreach($hotel_rooms as $rooms_value)
        {
          if($hotel_currency==null)
          {
            $hotel_currency=$rooms_value->hotel_room_currency;
          }
          $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$rooms_value->hotel_room_id)->where('hotel_room_season_validity_from','<=',$date_from)->where('hotel_room_season_validity_to','>=',$date_from)->get();

          foreach($get_hotel_seasons as $hotel_seasons)
          {
            $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();

            foreach($get_occupancy as $occupancy)
            {
              if($hotel_price_min==0 && $hotel_price_max==0)
              {
                  if($room_min_price==0)
              {
                $room_min_price=$occupancy->hotel_room_occupancy_price;
              }
              else if($occupancy->hotel_room_occupancy_price<$room_min_price)
              {
                $room_min_price=$occupancy->hotel_room_occupancy_price;
              }
              }
              else if(($hotel_price_min==0 || $hotel_price_min>0) && $hotel_price_max>0)
              {
                 if($room_min_price==0)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost && $hotel_price_max>=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;

                  }
                  else if($occupancy->hotel_room_occupancy_price<$room_min_price)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost && $hotel_price_max>=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;

                  }
              }
              else if(($hotel_price_min==0 || $hotel_price_min>0) && $hotel_price_max==0)
              {
                 if($room_min_price==0)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;

                  }
                  else if($occupancy->hotel_room_occupancy_price<$room_min_price)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;
                  
                  }
              }
            
            }
          }
        }

          $new_currency="";
          if($room_min_price>0)
          {

            $price=$room_min_price;
            
            if($hotel_currency!="GEL")
            {
              $conversion_price=0;
              $new_currency= $hotel_currency;
              $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
              $scriptDocument = new \DOMDocument();
                  libxml_use_internal_errors(TRUE); //disable libxml errors
                  if(!empty($fetch_html)){
                     //load
                    $scriptDocument->loadHTML($fetch_html);

                  //init DOMXPath
                    $scriptDOMXPath = new \DOMXPath($scriptDocument);
                    //get all the h2's with an id
                    $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                    //check
                    if($scriptRow->length > 0){
                      foreach($scriptRow as $row){
                        $conversion_price=round($row->nodeValue,2);
                      }
                    }

                  }
                 

              }
              else
              {
                 $hotel_cost[$hotel_key]=round($price);
              }

      }
  }

  

      if(count($fetch_hotels)>0)
      {
        $data=array();
          $fetch_hotels['markup']= $hotel_markup;
        $data=array("hotels"=>$fetch_hotels,
              "hotel_cost"=>$hotel_cost,
              "hotel_price_min"=>$hotel_price_min,
              "hotel_price_max"=>$hotel_price_max,);
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }


    public function hotelDetailView(Request $request)
    {
      $mainid=$request->get('hotel_id');
      
      $get_hotels=Hotels::where('hotel_status',1)->where('hotel_id',$mainid)->first();
      if($get_hotels)
      {
        
      $checkin_date=$request->get('checkin_date');

      $checkout_date=$request->get('checkout_date');

      $room_quantity=$request->get('room_quantity');

      $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $checkin_date.' 0:00:00');

      $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $checkout_date.' 0:00:00');

      $NoOfNights = $check_in_day->diffInDays($check_out_day);
      $amenities_array=array();
      $main_amenities=0;
                      $get_amenities=unserialize($get_hotels->hotel_amenities);
                   
                      if($get_amenities==null || $get_amenities=="")
                      {

                      }
                      else
                      {

                      

                          
               
                         foreach($get_amenities as $amenities)
                         {
                          
                        
                              $get_amenities_name=LoginController::fetchAmenitiesName($amenities[0]);

                              $amenities_array[$main_amenities]["amenities_name"]=$get_amenities_name['amenities_name'];


                         if(!empty($amenities[1]))
                          {
                         $sub_amenities_count=0;


   
                          foreach($amenities[1] as $sub_amenities)
                          {
                            
                            
                             $get_sub_amenities_name=LoginController::fetchSubAmenitiesName($sub_amenities,$amenities[0]);

                              $amenities_array[$main_amenities]["sub_amenities"][$sub_amenities_count]=$get_sub_amenities_name['sub_amenities_name'];

                             $sub_amenities_count++;
                            
                             

                          }
                          $main_amenities++;
                          }
                         
                
                          
                          }

                          }
                        
                          $hotel_rooms=HotelRooms::where('hotel_id',$mainid)->get();
                          $hotel_room_seasons=HotelRoomSeasons::where('hotel_id_fk',$mainid)->where('hotel_room_season_validity_from','<=',$checkin_date)->where('hotel_room_season_validity_to','>=',$checkin_date)->get();
                          $hotel_room_season_occupancy=HotelRoomSeasonOccupancy::where('hotel_id_fk',$mainid)->get();
                          $hotel_meal=HotelMeal::get();

       $room_min_price=0;
         $currency=$get_hotels['hotel_currency'];
       foreach($hotel_rooms as $rooms_value)
       {

        if($currency==null)
        {
          $currency=$rooms_value->hotel_room_currency;
        }
      }

        $currency_price_array=array();
          if($currency=="GEL")
          {

          }
          else
          {
             $new_currency=$currency;
          $conversion_price=0;
            $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                  $scriptDocument = new \DOMDocument();
                  libxml_use_internal_errors(TRUE); //disable libxml errors
                  if(!empty($fetch_html)){
                     //load
                     $scriptDocument->loadHTML($fetch_html);
                   
                  //init DOMXPath
                    $scriptDOMXPath = new \DOMXPath($scriptDocument);
                    //get all the h2's with an id
                    $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                    //check
                    if($scriptRow->length > 0){
                      foreach($scriptRow as $row){
                             $conversion_price=round($row->nodeValue,2);
                      }
                    }
                   
                  }
                   $currency_price_array[$currency]=round($conversion_price,2);
          }
           

        $currency_price_array["GEL"]=1;
       
       $hotel_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Hotel")->first();
      if($fetch_markup)
      {
        $hotel_markup=$fetch_markup['customer_markup_cost'];

      }

        $hotel_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "hotel"=>$get_hotels,
          "hotel_amenities"=>$amenities_array,
          "checkin_date"=>$checkin_date,
          "checkout_date"=>$checkout_date,
          "room_quantity"=>$room_quantity,
          "NoOfNights"=>$NoOfNights,
          "hotel_rooms"=>$hotel_rooms,
          "hotel_room_seasons"=>$hotel_room_seasons,
          "hotel_room_season_occupancy"=>$hotel_room_season_occupancy,
          "hotel_meal"=>$hotel_meal,
          "currency_price_array"=>$currency_price_array,
          "markup"=>$hotel_markup);
        return json_encode($hotel_data);
      }
      else
      {
        $hotel_data=array("errorCode"=>"1","errorMessage"=>"No data Found");
        return json_encode($hotel_data);
      }

    }

    public function hotelBooked(Request $request)
    {

      $fetch_admin=Users::where('users_pid',0)->first();
      $login_customer_id=$request->get('login_customer_id');
      $login_customer_name=$request->get('login_customer_name');
      $login_customer_email=$request->get('login_customer_email');
      $customer_name=$request->get('customer_name');
      $customer_email=$request->get('customer_email');  
      $customer_phone=$request->get('customer_phone');  
      $customer_country=$request->get('customer_country');  
      $customer_address=$request->get('customer_address');  
      $customer_remarks=$request->get('customer_remarks'); 
      $hotel_id=$request->get('hotel_id');
      $booking_supplier_id=$request->get('booking_supplier_id');
      $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
      $booking_amount=$request->get('booking_amount');
      $checkin_date=$request->get('checkin_date');
      $checkout_date=$request->get('checkout_date');
      $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $request->get('checkin_date').' 0:00:00');
      $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $request->get('checkout_date').' 0:00:00');
      $NoOfdays = $check_in_day->diffInDays($check_out_day);
      $booking_room_count=$request->get('booking_room_quantity');
      $booking_room_count_text=$request->get('booking_room_quantity_text');
      $booking_room_name=$request->get('room_name');
      $booking_adult_count=$request->get('booking_adult_count');
          $booking_child_count=$request->get('booking_child_count');
      $supplier_room_price=$request->get('supplier_price');
      $customer_room_price=$request->get('customer_room_price');
      $booking_markup_per=$request->get('booking_markup_per');
      $hotel_room_detail=$request->get('hotel_room_detail');
      $get_hotels=Hotels::where('hotel_id',$hotel_id)->where('hotel_status',1)->first();

      if($get_hotels)
      {
      
        $booking_currency=$request->get('room_currency');
        $date=date("Y-m-d");
        $time=date("H:i:s");
        $get_latest_id=BookingCustomer::latest()->value('booking_sep_id');
             $get_latest_id=$get_latest_id+1;
        $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="hotel";
        $insert_booking->booking_type_id=$hotel_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
        $insert_booking->booking_adult_count=$booking_adult_count;
            $insert_booking->booking_child_count=$booking_child_count;
        $insert_booking->booking_rooms_count=$booking_room_count;
        $insert_booking->booking_rooms_count_details=$booking_room_count_text;
        $insert_booking->booking_subject_name=$booking_room_name;
        $insert_booking->booking_subject_days=$NoOfdays;
        $insert_booking->supplier_adult_price=$supplier_room_price;
        $insert_booking->customer_adult_price=$customer_room_price;
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_amount=$booking_amount;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$checkin_date;
        $insert_booking->booking_selected_to_date=$checkout_date;
         $insert_booking->booking_other_info=serialize($hotel_room_detail);
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;

        if($insert_booking->save())
        {
          $booking_id=$get_latest_id;
          $htmldata='<p>Dear '.$customer_name.',</p><p>You have successfully booked hotel on traveldoor[dot]ge . Your booking id is '.$booking_id.'.</p>
          ';
          $data = array(
            'name' => $customer_name,
            'email' =>$customer_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
          });

          $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new hotel booking on traveldoor[dot]ge with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
          $data_supplier= array(
            'name' => $fetch_supplier->supplier_name,
            'email' =>$fetch_supplier->company_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW HOTEL BOOKING');
          });

          $htmldata='<p>Dear Admin,</p><p>New hotel has been booked with booking id '.$booking_id.' by customer '.$customer_name.' for the supplier '.$fetch_supplier->supplier_name.' </p>';
          $data_admin= array(
            'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
            'email' =>$fetch_admin->users_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_admin['email'], $data_admin['name'])->subject('NEW HOTEL BOOKING ON PORTAL');
          });



          $result_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "result"=>"success");
          return json_encode($result_data);
        }
        else
        {
            $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
        }

      }

    }

    public function fetchGuides(Request $request)
    {

      $date_from=date('Y-m-d');
      $fetch_guides=Guides::where('guide_status',1)->orderBy('guide_id','desc')->get();

      $languages=Languages::get();

       $guide_expense=GuideExpense::get();

       $hotel_cost=0;

       $food_cost=0;

      foreach($guide_expense as $expense)

      {

          if($expense->guide_expense=="HOTEL COST")

          {

               $hotel_cost=$expense->guide_expense_cost;

          }

          else if($expense->guide_expense=="FOOD COST")

          {

              $food_cost=$expense->guide_expense_cost;

          }

          

      }

      if(count($fetch_guides)>0)
      {
        $data=array();
        $fetch_guides['markup']= 12;
        $fetch_guides['hotel_cost']= $hotel_cost;
        $fetch_guides['food_cost']= $food_cost;
        $data=$fetch_guides;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }

    }

    public function filteredfetchGuides(Request $request)
    {

      $country_id=$request->get('country_id');
      $city_id=$request->get('city_id');
      $date_from=$request->get('date_from');
      $date_to=$request->get('date_to');

      $guides_array=array();


      $languages=languages::get();

      $guide_expense=GuideExpense::get();

      $hotel_cost=0;

      $food_cost=0;

      $guide_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Guide")->first();
      if($fetch_markup)
      {
        $guide_markup=$fetch_markup['customer_markup_cost'];

      }

      foreach($guide_expense as $expense)
      {
        if($expense->guide_expense=="HOTEL COST")
        {
          $hotel_cost=$expense->guide_expense_cost;
        }
        else if($expense->guide_expense=="FOOD COST")
        {
          $food_cost=$expense->guide_expense_cost;
        }
      }
      $fetch_guides=Guides::where('guide_country',$country_id)->where('guide_city',$city_id)->where('guide_price_per_day',">",0)->where('guide_status',1)->orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','desc')->get();

      if(count($fetch_guides)>0)
      {
        foreach($fetch_guides as $guides)

        {



          $operating_days=unserialize($guides->operating_weekdays);

          $operating_days_array=array();

          foreach($operating_days as $key=>$days)

          {

            if($days=="Yes")

            {

              $operating_days_array[]= ucwords($key);

            }



          }

          $check_blackout_during_dates=0;



          $get_dates_from=strtotime($date_from);

          $get_dates_to=strtotime($date_to);



          $blackout_days=explode(',',$guides->guide_blackout_dates);



          $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;

          $dates="";

          for($count=0;$count<=$date_diff;$count++)

          {

            $calculated_date=date("Y-m-d",strtotime("+$count day",strtotime($date_from)));



            $calculated_day=date("l",strtotime("+$count day",strtotime($date_from)));

            if(in_array($calculated_date, $blackout_days) || !in_array($calculated_day, $operating_days_array))

            {

              $check_blackout_during_dates++;



            }





          }





          if($check_blackout_during_dates<=0)

          {
            $guides_array[]=$guides;

          }



        }

        $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date_from.' 0:00:00');

        $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date_to.' 0:00:00');

        $NoOfDays = $check_in_day->diffInDays($check_out_day);

        $NoOfDays=($NoOfDays+1);

        $data=array();
        $fetch_guides_data=array();
        $fetch_guides=$guides_array;
        $fetch_guides_data['markup']= $guide_markup;
        $fetch_guides_data['food_cost']= $food_cost;
        $fetch_guides_data['hotel_cost']= $hotel_cost;
        $fetch_guides_data['no_of_days']= $NoOfDays;
        $data['error']="0";
        $data['error_code']="";
        $data['guides']=$fetch_guides;
        $data['guides_data']=$fetch_guides_data;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }

    }
    
    public function guideDetailView(Request $request)
    {


      $guide_id=$request->get('guide_id');

      $get_guides=Guides::where('guide_id',$guide_id)->where('guide_status',1)->first();
      if($get_guides)
      {
      
        $countries=Countries::get();
        foreach($countries as $country)
        {
          if($country->country_id==$get_guides->guide_country)
          {
            $country_name=$country->country_name;
          }


        }
        
           $guide_expense=GuideExpense::get();
       $hotel_cost=0;

       $food_cost=0;

      foreach($guide_expense as $expense)

      {

          if($expense->guide_expense=="HOTEL COST")

          {

               $hotel_cost=$expense->guide_expense_cost;

          }

          else if($expense->guide_expense=="FOOD COST")

          {

              $food_cost=$expense->guide_expense_cost;

          }

          

      }



        $fetch_city=ServiceManagement::searchCities($get_guides->guide_city,$get_guides->guide_country);
        $city_name=$fetch_city['name'];
        
        $languages=languages::get();
         $languages_data=explode(",",$get_guides->guide_language);

     foreach($languages as $language)

     {

      if(in_array($language->language_id,$languages_data))

      {



      $languages_spoken[]=$language->language_name;

     }

   }

     $guide_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Guide")->first();
      if($fetch_markup)
      {
        $guide_markup=$fetch_markup['customer_markup_cost'];

      }

        $guide_data=array("errorCode"=>"0",
                       "errorMessage"=>"",
                       "guide"=>$get_guides,
                       "country"=>$country_name,
                       "city"=>$city_name,
                          "hotel_cost"=>$hotel_cost,
                       "food_cost"=>$food_cost,
                      "languages_spoken"=>$languages_spoken,
                       "markup"=>$guide_markup);
        return json_encode($guide_data);
      }
      else
      {
        $guide_data=array("errorCode"=>"1","errorMessage"=>"No data Found");
        return json_encode($guide_data);
      }

    }

    public function guideBooked(Request $request)
    {

      $fetch_admin=Users::where('users_pid',0)->first();
      $login_customer_id=$request->get('login_customer_id');
      $login_customer_name=$request->get('login_customer_name');
      $login_customer_email=$request->get('login_customer_email');
      $customer_name=$request->get('customer_name');
      $customer_email=$request->get('customer_email');  
      $customer_phone=$request->get('customer_phone');  
      $customer_country=$request->get('customer_country');  
      $customer_address=$request->get('customer_address');  
      $customer_remarks=$request->get('customer_remarks'); 
      $guide_id=$request->get('guide_id');
      $booking_supplier_id=$request->get('booking_supplier_id');
      $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
      $booking_amount=$request->get('booking_amount');
      $from_date=$request->get('from_date');
      $to_date=$request->get('to_date');
      $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $request->get('from_date').' 0:00:00');
      $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $request->get('to_date').' 0:00:00');
      $NoOfdays = $check_in_day->diffInDays($check_out_day);
      $NoOfdays = ($NoOfdays+1);
       $booking_guide_quantity=$request->get('booking_guide_quantity');
      $supplier_guide_price=$request->get('supplier_guide_price');
      $customer_guide_price=$request->get('customer_guide_price');
      $booking_markup_per=$request->get('booking_markup_per');

      $guide_expense=GuideExpense::get();
      $hotel_cost=0;
      $food_cost=0;
      foreach($guide_expense as $expense)
      {
        if($expense->guide_expense=="HOTEL COST")
        {
          $hotel_cost=$expense->guide_expense_cost;
        }
        else if($expense->guide_expense=="FOOD COST")
        {
          $food_cost=$expense->guide_expense_cost;
        }
      }

        $other_expenses=array("hotel_cost"=>$hotel_cost,"food_cost"=>$food_cost);
      $get_guides=Guides::where('guide_id',$guide_id)->where('guide_status',1)->first();

      if($get_guides)
      {
      
        $booking_currency=$request->get('guide_currency');
        $date=date("Y-m-d");
        $time=date("H:i:s");
         $get_latest_id=BookingCustomer::latest()->value('booking_sep_id');
             $get_latest_id=$get_latest_id+1;
        $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="guide";
        $insert_booking->booking_type_id=$guide_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
        $insert_booking->booking_adult_count=$booking_guide_quantity;
        $insert_booking->booking_subject_days=$NoOfdays;
        $insert_booking->supplier_adult_price=$supplier_guide_price;
        $insert_booking->customer_adult_price=$customer_guide_price;
        $insert_booking->other_expenses=serialize($other_expenses);
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_amount=$booking_amount;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$from_date;
        $insert_booking->booking_selected_to_date=$to_date;
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;

        if($insert_booking->save())
        {
          $booking_id=$get_latest_id;
          $htmldata='<p>Dear '.$customer_name.',</p><p>You have successfully booked guide on traveldoor[dot]ge . Your booking id is '.$booking_id.'.</p>
          ';
          $data = array(
            'name' => $customer_name,
            'email' =>$customer_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
          });

          $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new guide booking on traveldoor[dot]ge with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
          $data_supplier= array(
            'name' => $fetch_supplier->supplier_name,
            'email' =>$fetch_supplier->company_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW GUIDE BOOKING');
          });

          $htmldata='<p>Dear Admin,</p><p>New guide has been booked with booking id '.$booking_id.' by customer <b>'.$customer_name.'</b> for the supplier <b>'.$fetch_supplier->supplier_name.'</b> </p>';
          $data_admin= array(
            'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
            'email' =>$fetch_admin->users_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_admin['email'], $data_admin['name'])->subject('NEW GUIDE BOOKING ON PORTAL');
          });



          $result_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "result"=>"success");
          return json_encode($result_data);
        }
        else
        {
            $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
        }

      }

    }

    public function fetchSightseeing(Request $request)
    {

    }

    public function filteredfetchSightseeing(Request $request)
    {

      $country_id=$request->get('country_id');
      $city_id=$request->get('city_id');
      $date_from=$request->get('date_from');
        $tour_type=$request->get('tour_type');
        $limit=9;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }

       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id){
           $query->where('sightseeing_city_from',$city_id)->orWhere('sightseeing_city_to',$city_id); });


       if(isset($tour_type) && $tour_type!="0")
      {
        $fetch_sightseeing=$fetch_sightseeing->where('sightseeing_tour_type',$tour_type);
      }


      $fetch_sightseeing=$fetch_sightseeing->where('sightseeing_status',1)->orderBy('sightseeing_popular_status','desc')->orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->orderBy('sightseeing_id','desc')->offset($offset*$limit)->take($limit)->get();
      $sightseeing_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Sightseeing")->first();
      if($fetch_markup)
      {
        $sightseeing_markup=$fetch_markup['customer_markup_cost'];

      }

      if(count($fetch_sightseeing)>0)
      {
        $data=array();
        $fetch_sightseeing['markup']= $sightseeing_markup;
        $data=$fetch_sightseeing;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

    public function sightseeingDetailView(Request $request)
    {

      $sightseeing_id=$request->get('sightseeing_id');
      $get_vehicles=VehicleType::get();

      $get_sightseeing=SightSeeing::where('sightseeing_status',1)->where('sightseeing_id',$sightseeing_id)->first();

      if($get_sightseeing)
      {
        $countries=Countries::get();
        foreach($countries as $country)
        {
          if($country->country_id==$get_sightseeing->sightseeing_country)
          {
            $country_name=$country->country_name;
          }


        }
        $between_city="";
        $get_from_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_from,$get_sightseeing->sightseeing_country);
        $from_city=$get_from_city['name'];

        if($get_sightseeing->sightseeing_city_between!=null && $get_sightseeing->sightseeing_city_between!="")
        {
          $all_between_cities=explode(",",$get_sightseeing->sightseeing_city_between);
          for($cities=0;$cities< count($all_between_cities);$cities++)
          {
            $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$get_sightseeing->sightseeing_country);
            $between_city.=$fetch_city['name']."-";
          }
        }

         $get_to_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_to,$get_sightseeing->sightseeing_country);
        $to_city=$get_to_city['name'];

          $fetch_markup=CustomerMarkup::where('customer_markup',"Sightseeing")->first();
      if($fetch_markup)
      {
        $sightseeing_markup=$fetch_markup['customer_markup_cost'];

      }

        $sightseeing_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "sightseeing"=>$get_sightseeing,
          "country"=>$country_name,
          "from_city"=>$from_city,
          "between_city"=>$between_city,
          "to_city"=>$to_city,
          "vehicle_types"=>$get_vehicles,
          "markup"=> $sightseeing_markup);
        return json_encode($sightseeing_data);
      }
      else
      {
        $sightseeing_data=array("errorCode"=>"1","errorMessage"=>"No data Found");
        return json_encode($sightseeing_data);
      }

    }

    public function fetchGuidesSightseeing(Request $request)
    {
      $languages=languages::get();
      $country_id=$request->get('sightseeing_country_id');
      $city_id=$request->get('sightseeing_first_city_id');
       $current_city_id=$request->get('sightseeing_city_id');
      $date_from=$request->get('sightseeing_checkin_date');
      $date_to=$request->get('sightseeing_checkin_date');
       $day_sequence=$request->get('day_sequence');

      $sightseeing_id=$request->get('sightseeing_id');
      $vehicle_selected=$request->get('vehicle_type_id');

     $get_sightseeing_detail=Sightseeing::where('sightseeing_id',$sightseeing_id)->first();
    $to_city="";
    if(!empty($get_sightseeing_detail))
    {
      $to_city=$get_sightseeing_detail->sightseeing_city_to;
    }

       if($day_sequence==1   || $day_sequence=="1")
      {
         $fetch_guides=Guides::where('guide_country',$country_id)->where(function ($query) use ($city_id,$current_city_id){
      $query->where('guide_city',$city_id)->orWhere('guide_city',$current_city_id); })->where('guide_status',1)->orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','desc')->get();
      }
      else
      {
         $fetch_guides=Guides::where('guide_country',$country_id)->where('guide_status',1)->orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','desc')->get();
      }


      $check_guide_count=0;
      $guides_array=array();
      foreach($fetch_guides as $guides)
      {
        $guide_cost_vehicle_selected=0;
        $get_tour_cost=unserialize($guides->guide_tours_cost);

        foreach($get_tour_cost as $tour_cost)
        {
          if($tour_cost['tour_name']==$sightseeing_id)
          {

            $cost_count=0;
            foreach($tour_cost['tour_vehicle_name'] as $vehicle_names)
            {
              if($vehicle_names==$vehicle_selected)
              {
                $guide_cost_vehicle_selected=$tour_cost['tour_guide_cost'][$cost_count];

                if($guides->guide_city!=$to_city && $to_city!="")
               {
                $guide_cost_vehicle_selected+=$guides->guide_hotel_cost ? $guides->guide_hotel_cost : 0;
               
                $guide_cost_vehicle_selected+=$guides->guide_food_cost ? $guides->guide_food_cost : 0;
              }

                $operating_days=unserialize($guides->operating_weekdays);

                $operating_days_array=array();

                foreach($operating_days as $key=>$days)

                {

                  if($days=="Yes")

                  {

                    $operating_days_array[]= ucwords($key);

                  }



                }

                $check_blackout_during_dates=0;



                $get_dates_from=strtotime($date_from);

                $get_dates_to=strtotime($date_to);



                $blackout_days=explode(',',$guides->guide_blackout_dates);



                $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;

                $dates="";

                // for($count=0;$count<=$date_diff;$count++)

                // {

                //  $calculated_date=date("Y-m-d",strtotime("+$count day",strtotime($date_from)));



                //  $calculated_day=date("l",strtotime("+$count day",strtotime($date_from)));

                //  if(in_array($calculated_date, $blackout_days) || !in_array($calculated_day, $operating_days_array))

                //  {

                //    $check_blackout_during_dates++;



                //  }





                // }



                

                if($check_blackout_during_dates<=0)

                {
                  if(($guide_cost_vehicle_selected!="" && $guide_cost_vehicle_selected>0))
                        {
                  $guides_array[$check_guide_count]["guide_id"]=$guides->guide_id;
                  $guides_array[$check_guide_count]["guide_first_name"]=$guides->guide_first_name;
                  $guides_array[$check_guide_count]["guide_last_name"]=$guides->guide_last_name;
                  $guides_array[$check_guide_count]["guide_tours_cost"]=$guide_cost_vehicle_selected;
                  $guides_array[$check_guide_count]["guide_description"]=$guides->guide_description;
                  $guides_array[$check_guide_count]["guide_image"]=$guides->guide_image;
                  $guides_array[$check_guide_count]["guide_language"]=$guides->guide_language;
                  $check_guide_count++;

                }

                }





              }
              $cost_count++;
            }
            
          }
        }
      }
  if(count($fetch_guides)>0)
      {
        $data=array();
        $fetch_guides=$guides_array;
        $data['error']="0";
        $data['error_code']="";
        $data['guides']=$fetch_guides;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }


    }


    public function fetchDriversSightseeing(Request $request)
    {
     $languages=languages::get();
      $country_id=$request->get('sightseeing_country_id');
      $city_id=$request->get('sightseeing_first_city_id');
       $current_city_id=$request->get('sightseeing_city_id');
      $date_from=$request->get('sightseeing_checkin_date');
      $date_to=$request->get('sightseeing_checkin_date');
       $day_sequence=$request->get('day_sequence');

      $sightseeing_id=$request->get('sightseeing_id');
      $vehicle_selected=$request->get('vehicle_type_id');


$get_sightseeing_detail=Sightseeing::where('sightseeing_id',$sightseeing_id)->first();
    $to_city="";
    if(!empty($get_sightseeing_detail))
    {
      $to_city=$get_sightseeing_detail->sightseeing_city_to;
    }

       if($day_sequence==1   || $day_sequence=="1")
      {

          $fetch_drivers=Drivers::where('driver_country',$country_id)->where(function ($query) use ($city_id,$current_city_id){
      $query->where('driver_city',$city_id)->orWhere('driver_city',$current_city_id); })->whereRaw('FIND_IN_SET('.$vehicle_selected.',driver_vehicle_type)')->where('driver_status',1)->orderBy(\DB::raw('-`driver_show_order`'), 'desc')->orderBy('driver_id','desc')->get();
      }
      else
      {
         $fetch_drivers=Drivers::where('driver_country',$country_id)->whereRaw('FIND_IN_SET('.$vehicle_selected.',driver_vehicle_type)')->where('driver_status',1)->orderBy(\DB::raw('-`driver_show_order`'), 'desc')->orderBy('driver_id','desc')->get();
      }

    $drivers_array=array();
      $check_driver_count=0;
      foreach($fetch_drivers as $drivers)
      {
        $get_vehicle_type=$drivers->driver_vehicle_type;
        $get_vehicle_names=$drivers->driver_vehicle;
      
        $driver_vehicle_images=unserialize($drivers->driver_vehicle_images);
    $vehicle_images_show="<div class='vehicle_images row'>";
    $image_count=1;

    $image_vehicle="";

    $vehicle_name_id=array();
    if($get_vehicle_type!=null && $get_vehicle_type!="")
    {
      $vehicle_type=explode(",",$get_vehicle_type);

      $vehicle_names=explode(",",$get_vehicle_names);

          //get vehicle type that matches with selected vehicle 
          // print_r($vehicle_type);
       
          //  print_r($vehicle_names);
         
          $matched_vehicle_type=array_keys($vehicle_type, $vehicle_selected); 
  // print_r($matched_vehicle_type);

      //fetch vehicle names for vehicle type selected
      foreach($vehicle_names as $vehicle_name_key=>$vehicle_name_values)
      {
       if(in_array($vehicle_name_key, $matched_vehicle_type))
       {
        // echo $vehicle_name_key;

        $vehicle_name_id[]=$vehicle_names[$vehicle_name_key];
        // echo $vehicle_names[$vehicle_name_key];
      }

    }
     //end of code for fetch vehicle names for vehicle type selected

    if(!empty($driver_vehicle_images))
    {
             // print_r($driver_vehicle_images);
      foreach($driver_vehicle_images as $vehicle_main_index=>$vehicle_images)
      {
   // echo $vehicle_main_index;
   //  print_r($matched_vehicle_type);
              // check if images index matches with indexes present in matched_vehicle_type array 

              // if(array_key_exists($vehicle_main_index, $matched_vehicle_type))
              // {
        if(in_array($vehicle_main_index, $matched_vehicle_type))
        {

          foreach($vehicle_images as $images)
          {
            $class='';
            $style='';
            if($image_count>=1)
            {
             $class="class='more-vehicle-image'";
             $style="style='display:none'";
           }
           if($image_vehicle=="")
           {
            $image_vehicle=asset('assets/uploads/driver_vehicle_images')."/".$images;
          }

          $vehicle_images_show.="<div class='col-md-4' ".$style."><img src='".asset('assets/uploads/driver_vehicle_images')."/".$images."' alt='Vehicle Image' width=50 height=50 ".$class."></div>";
          $image_count++;
        }
      }

    }
  }


  }

  $vehicle_images_show.="</div>";
        $driver_cost_vehicle_selected=0;
        $get_tour_cost=unserialize($drivers->driver_tours_cost);

        foreach($get_tour_cost as $tour_cost)
        {
          if($tour_cost['tour_name']==$sightseeing_id)
          {
            $cost_count=0;
            foreach($tour_cost['tour_vehicle_name'] as $vehicle_names)
            {
            
              if($vehicle_names==$vehicle_selected)
              {
                //    echo $vehicle_names."<br>";
                //    echo $tour_cost['tour_driver_cost'][$cost_count]."<br>";
                //    echo $cost_count."<br>"."<br>";
                $driver_cost_vehicle_selected=$tour_cost['tour_driver_cost'][$cost_count];

                if($drivers->driver_city!=$to_city && $to_city!="")
               {
                $driver_cost_vehicle_selected+=$drivers->driver_hotel_cost ? $drivers->driver_hotel_cost : 0;
                $driver_cost_vehicle_selected+=$drivers->driver_food_cost  ? $drivers->driver_food_cost : 0;

              }
              
                $operating_days=unserialize($drivers->operating_weekdays);

                $operating_days_array=array();

                foreach($operating_days as $key=>$days)

                {

                  if($days=="Yes")

                  {

                    $operating_days_array[]= ucwords($key);

                  }



                }

                $check_blackout_during_dates=0;



                $get_dates_from=strtotime($date_from);

                $get_dates_to=strtotime($date_to);



                $blackout_days=explode(',',$drivers->driver_blackout_dates);



                $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;

                $dates="";

                // for($count=0;$count<=$date_diff;$count++)

                // {

                //  $calculated_date=date("Y-m-d",strtotime("+$count day",strtotime($date_from)));



                //  $calculated_day=date("l",strtotime("+$count day",strtotime($date_from)));

                //  if(in_array($calculated_date, $blackout_days) || !in_array($calculated_day, $operating_days_array))

                //  {

                //    $check_blackout_during_dates++;



                //  }





                // }



              

                // if($check_blackout_during_dates<=0)

                // {
                  if(($driver_cost_vehicle_selected!="" && $driver_cost_vehicle_selected>0))
                          {
                  $drivers_array[$check_driver_count]["driver_id"]=$drivers->driver_id;
                  $drivers_array[$check_driver_count]["driver_first_name"]=$drivers->driver_first_name;
                  $drivers_array[$check_driver_count]["driver_last_name"]=$drivers->driver_last_name;
                  $drivers_array[$check_driver_count]["driver_tours_cost"]=$driver_cost_vehicle_selected;
                  $drivers_array[$check_driver_count]["driver_description"]=$drivers->driver_description;
                   $drivers_array[$check_driver_count]["vehicle_images_show"]=$vehicle_images_show;
                if($image_vehicle!="")
                        {
                          $drivers_array[$check_driver_count]["vehicle_image"]=$image_vehicle;
                          $drivers_array[$check_driver_count]["driver_image"]=$drivers->driver_image;
                        }
                        else if($drivers->driver_image!="" && $drivers->driver_image!=null)
                        {
                         $drivers_array[$check_driver_count]["driver_image"]=$drivers->driver_image;
                        }
                        else
                        {
                           $drivers_array[$check_driver_count]["vehicle_image"]=null;
                          $drivers_array[$check_driver_count]["driver_image"]=null;
                        }

                  
                  $drivers_array[$check_driver_count]["driver_language"]=$drivers->driver_language;
                  
                  $check_driver_count++;
                // }


                }





              }
    $cost_count++;
            }
          
          }
        }
      }
  if(count($fetch_drivers)>0)
      {
        $data=array();
        $fetch_drivers=$drivers_array;
        $data['error']="0";
        $data['error_code']="";
        $data['drivers']=$fetch_drivers;
        // echo "<pre>";
        // print_r($data);
        // die();
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }


    }

    public function sightseeingBooked(Request $request)
    {
      $fetch_admin=Users::where('users_pid',0)->first();
      $login_customer_id=$request->get('login_customer_id');
      $login_customer_name=$request->get('login_customer_name');
      $login_customer_email=$request->get('login_customer_email');
      $customer_name=$request->get('customer_name');
      $customer_email=$request->get('customer_email');  
      $customer_phone=$request->get('customer_phone');  
      $customer_country=$request->get('customer_country');  
      $customer_address=$request->get('customer_address');  
      $customer_remarks=$request->get('customer_remarks'); 
      $sightseeing_id=$request->get('sightseeing_id');
      $booking_supplier_id=$request->get('booking_supplier_id');
      $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
      $booking_amount=$request->get('booking_amount');
      $booking_date=$request->get('booking_date');
      $booking_adult_count=$request->get('booking_adult_count');
      $booking_child_count=$request->get('booking_child_count');
      $customer_adult_price=$request->get('customer_adult_price');
      $customer_child_price=$request->get('customer_child_price');
      $booking_markup_per=$request->get('booking_markup_per');
      $vehicle_type_id=$request->get('vehicle_type_id');
        $vehicle_type_name=$request->get('vehicle_type_name');
      $guide_id=$request->get('guide_id');
      $guide_name=$request->get('guide_name');
      $guide_cost=$request->get('guide_cost');
      $driver_id=$request->get('driver_id');
      $driver_name=$request->get('driver_name');
      $driver_cost=$request->get('driver_cost');
      $sightseeing_food_cost_w_markup=$request->get('sightseeing_food_cost');
      $sightseeing_hotel_cost_w_markup=$request->get('sightseeing_hotel_cost');
      $get_sightseeing=Sightseeing::where('sightseeing_id',$sightseeing_id)->where('sightseeing_status',1)->first();
      if($get_sightseeing)
      {
        $supplier_adult_price=$get_sightseeing->sightseeing_adult_cost;
        $supplier_child_price=$get_sightseeing->sightseeing_child_cost;
        $sightseeing_food_cost=$get_sightseeing->sightseeing_food_cost;
        $sightseeing_hotel_cost=$get_sightseeing->sightseeing_hotel_cost;

        $other_expenses=array("food_cost"=>$sightseeing_food_cost,
                            "food_cost_with_markup"=>$sightseeing_food_cost_w_markup,
                            "hotel_cost"=>$sightseeing_hotel_cost,
                            "hotel_cost_with_markup"=> $sightseeing_hotel_cost_w_markup);

          $booking_vehicle_guide_name=array("vehicle_type_id"=>$vehicle_type_id,
                            "vehicle_type_name"=>$vehicle_type_name,
                            "guide_id"=>$guide_id,
                            "guide_name"=> $guide_name,
                          "guide_cost"=> $guide_cost,
                          "driver_id"=>$driver_id,
                            "driver_name"=> $driver_name,
                          "driver_cost"=> $driver_cost);

          $booking_currency=$request->get('sightseeing_currency');


        $date=date("Y-m-d");
        $time=date("H:i:s");
        $get_latest_id=BookingCustomer::latest()->value('booking_sep_id');
        $get_latest_id=$get_latest_id+1;

        $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="sightseeing";
        $insert_booking->booking_type_id=$sightseeing_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
        $insert_booking->booking_adult_count=$booking_adult_count;
        $insert_booking->booking_child_count=$booking_child_count;
        $insert_booking->booking_subject_name=serialize($booking_vehicle_guide_name);
        $insert_booking->supplier_adult_price=$supplier_adult_price;
        $insert_booking->supplier_child_price=$supplier_child_price;
        $insert_booking->customer_adult_price=$customer_adult_price;
        $insert_booking->customer_child_price=$customer_child_price;
        $insert_booking->other_expenses=serialize($other_expenses);
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_amount=$booking_amount;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$booking_date;
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;

        if($insert_booking->save())
        {
          $booking_id=$get_latest_id;
          $sightseeing_inserted_id=$insert_booking->id;
          $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="guide";
        $insert_booking->booking_type_id=$guide_id;
         $insert_booking->booking_sightseeing_id=$sightseeing_inserted_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
        $insert_booking->booking_adult_count=1;
          $insert_booking->booking_subject_days=1;
        $insert_booking->supplier_adult_price=$guide_cost;
        $insert_booking->customer_adult_price=$guide_cost;
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_amount=$guide_cost;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$booking_date;
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;
        $insert_booking->save();

        $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="driver";
        $insert_booking->booking_type_id=$driver_id;
         $insert_booking->booking_sightseeing_id=$sightseeing_inserted_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
        $insert_booking->booking_adult_count=1;
          $insert_booking->booking_subject_days=1;
        $insert_booking->supplier_adult_price=$driver_cost;
        $insert_booking->customer_adult_price=$driver_cost;
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_amount=$driver_cost;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$booking_date;
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;
        $insert_booking->save();


          $htmldata='<p>Dear '.$customer_name.',</p><p>You have successfully booked sightseeing on traveldoor[dot]ge . Your booking id is '.$booking_id.'.</p>
          ';
          $data = array(
            'name' => $customer_name,
            'email' =>$customer_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
          });

          $htmldata='<p>Dear Admin,</p><p>New sightseeing has been booked with booking id '.$booking_id.' by customer <b>'.$customer_name.'</b></p>';
          $data_admin= array(
            'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
            'email' =>$fetch_admin->users_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_admin['email'], $data_admin['name'])->subject('NEW SIGHTSEEING BOOKING ON PORTAL');
          });




          $result_data=array("errorCode"=>"0",
            "errorMessage"=>"",
            "result"=>"success");
          return json_encode($result_data);
        }
        else
        {
            $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
        }

      }

    }

    public function fetchItinerary(Request $request)
    {

    }

    public function filteredfetchItinerary(Request $request)
    {

      $country_id=$request->get('country_id');
      $city_id=$request->get('city_id');
      $date_from=$request->get('date_from');
      $limit=10;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }

        // $fetch_itinerary=SavedItinerary::whereRaw("FIND_IN_SET(".$country_id.",itinerary_package_countries)")->whereRaw("FIND_IN_SET(".$city_id.",itinerary_package_cities)")->where('itinerary_bc_status',1)->orderBy(\DB::raw('-`itinerary_show_order`'), 'desc')->orderBy('itinerary_id','desc')->offset($offset*$limit)->take($limit)->get();


          $fetch_itinerary=SavedItinerary::whereRaw("FIND_IN_SET(".$country_id.",itinerary_package_countries)")->whereRaw("FIND_IN_SET(".$city_id.",itinerary_package_cities)")->where('itinerary_validity_operation_from','<=',$date_from)->where('itinerary_validity_operation_to','>=',$date_from)->where('itinerary_bc_status',1)->orderBy(\DB::raw('-`itinerary_show_order`'), 'desc')->orderBy('itinerary_id','desc')->offset($offset*$limit)->take($limit)->get();

      if(count($fetch_itinerary)>0)
      {
        $fetch_itinerary_array=array();
        $count=0;
        foreach($fetch_itinerary as $itinerary)
        {
             $itinerary_image=unserialize($itinerary->itinerary_image);
           $fetch_image=unserialize($itinerary->itinerary_package_services);
          $itnerary_sightseeing_image_id=$fetch_image[0]['sightseeing']['sightseeing_id'];
          $itnerary_activity_image_id=$fetch_image[0]['activity']['activity_id'][0];
          $itnerary_hotel_image_id=$fetch_image[0]['hotel']['hotel_id'];
          $hotel_array=array_column($fetch_image, 'hotel');
          $activity_array=array_column($fetch_image, 'activity');
          $sightseeing_array=array_column($fetch_image, 'sightseeing');
          $transfer_array=array_column($fetch_image, 'transfer');

          $itinerary_image_link="";
          if($itinerary_image!=null && !empty($itinerary_image))
      {

      if(!empty($itinerary_image))
      {
        $itinerary_image_link=asset("assets/uploads/itinerary_images")."/".$itinerary_image[0];
      }
      else
      {
        $itinerary_image_link=asset("assets/images/no-photo.png");
      }

      }
      else if(!empty($itnerary_sightseeing_image_id))
      {
        $fetch_sightseeing=SightSeeing::where('sightseeing_id',$itnerary_sightseeing_image_id)->first();
        $fetch_sightseeing_image=unserialize($fetch_sightseeing['sightseeing_images']);

      if(!empty($fetch_sightseeing_image))
      {
        $itinerary_image_link=asset("assets/uploads/sightseeing_images")."/".$fetch_sightseeing_image[0];
      }
      else
      {
        $itinerary_image_link=asset("assets/images/no-photo.png");
      }
       
      }
      else if(!empty($itnerary_activity_image_id))
      {
        $fetch_activity=Activities::where('activity_id',$itnerary_activity_image_id)->first();
        $fetch_activity_image=unserialize($fetch_activity['activity_images']);

        if(!empty($fetch_activity_image))
      {
          $itinerary_image_link=asset("assets/uploads/activity_images")."/".$fetch_activity_image[0];
      }
      else
      {
        $itinerary_image_link=asset("assets/images/no-photo.png");
      }

      
      }
      else
      {
        $fetch_hotel=Hotels::where('hotel_id',$itnerary_hotel_image_id)->first();
        $fetch_hotel_image=unserialize($fetch_hotel['hotel_images']);
         if(!empty($fetch_hotel_image))
      {
          $itinerary_image_link=asset("assets/uploads/hotel_images")."/".$fetch_hotel_image[0];
      }
      else
      {
        $itinerary_image_link=asset("assets/images/no-photo.png");
      }
   
      }

          $fetch_itinerary_array[$count]= $itinerary;
          $fetch_itinerary_array[$count]['itinerary_image']= $itinerary_image_link;
          $count++;
        }
        
        $data=array();


         $itinerary_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Itinerary")->first();
      if($fetch_markup)
      {
        $itinerary_markup=$fetch_markup['customer_markup_cost'];

      }

        $fetch_itinerary_array['markup']= $itinerary_markup;
        $data=$fetch_itinerary_array;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }

    public function itineraryDetailView(Request $request)
    {
      $itinerary_id=$request->get('itinerary_id');
      $itinerary_date_from=$request->get('itinerary_date_from');
        $countries=Countries::get();
        $get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->where('itinerary_status',1)->first();
        $itinerary_array=array();
        $itinerary_images=array();
         $activity_pax_count_array=array();
      if($get_itinerary)
      {
        $itinerary_array['itinerary_id']=$get_itinerary->itinerary_id;
         $itinerary_array['itinerary_name']=$get_itinerary->itinerary_tour_name;
          $itinerary_array['itinerary_tour_description']=$get_itinerary->itinerary_tour_description;
           $itinerary_array['itinerary_tour_days']=$get_itinerary->itinerary_tour_days;


           $itinerary_package_countries=explode(",",$get_itinerary->itinerary_package_countries);
           $itinerary_package_cities=explode(",",$get_itinerary->itinerary_package_cities);
           $itinerary_package_title=unserialize($get_itinerary->itinerary_package_title);
           $itinerary_package_description=unserialize($get_itinerary->itinerary_package_description);
           $itinerary_package_services=unserialize($get_itinerary->itinerary_package_services);

           for($days_count=0;$days_count<=$get_itinerary->itinerary_tour_days;$days_count++)
           {
            $itinerary_array['itinerary_details_day_wise'][$days_count]['day_number']=($days_count+1);
            $itinerary_array['itinerary_details_day_wise'][$days_count]['day_title']=$itinerary_package_title[$days_count];
            $itinerary_array['itinerary_details_day_wise'][$days_count]['day_desc']=$itinerary_package_description[$days_count];
            $itinerary_array['itinerary_details_day_wise'][$days_count]['country_id']=$itinerary_package_countries[$days_count];

            $day_country="";
            foreach($countries as $country)
            {
              if($country->country_id==$itinerary_package_countries[$days_count])
              {
                $day_country=$country->country_name;
              }


            } 

            $itinerary_array['itinerary_details_day_wise'][$days_count]['country_name']=$day_country;

            $itinerary_array['itinerary_details_day_wise'][$days_count]['city_id']=$itinerary_package_cities[$days_count];

            $fetch_city_name=ServiceManagement::searchCities($itinerary_package_cities[$days_count],$itinerary_package_countries[$days_count]);

            $itinerary_array['itinerary_details_day_wise'][$days_count]['city_name']=$fetch_city_name['name'];

            $days_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from)));

            $itinerary_array['itinerary_details_day_wise'][$days_count]['date']=$days_date;
             if($days_count<$get_itinerary->itinerary_tour_days)
             {
            if(!empty($itinerary_package_services[$days_count]['hotel']['hotel_id']) && $itinerary_package_services[$days_count]['hotel']['hotel_no_of_days']!="0")
            {
              $fetch_hotel=ServiceManagement::searchHotel($itinerary_package_services[$days_count]['hotel']['hotel_id']);
              $fetch_hotelrooms=HotelRooms::where('hotel_id',$itinerary_package_services[$days_count]['hotel']['hotel_id'])->get();
              $fetch_hotelseasons=HotelRoomSeasons::where('hotel_id_fk',$itinerary_package_services[$days_count]['hotel']['hotel_id'])->get();
              $fetch_hotelseasonoccupancy=HotelRoomSeasonOccupancy::where('hotel_id_fk',$itinerary_package_services[$days_count]['hotel']['hotel_id'])->get();
              $itinerary_array['itinerary_details_day_wise'][$days_count]['hotel']['hotel_details']=$fetch_hotel;
               $itinerary_array['itinerary_details_day_wise'][$days_count]['hotel']['hotel_room_details']=$fetch_hotelrooms;
                $itinerary_array['itinerary_details_day_wise'][$days_count]['hotel']['hotel_room_season_details']=$fetch_hotelseasons;
                 $itinerary_array['itinerary_details_day_wise'][$days_count]['hotel']['hotel_room_season_occupancy_details']=$fetch_hotelseasonoccupancy;

              $itinerary_array['itinerary_details_day_wise'][$days_count]['hotel']['hotel_itinerary_details']=$itinerary_package_services[$days_count]['hotel'];

              $checkin_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); 
              $checkout_date=date('Y-m-d',strtotime("+".($itinerary_package_services[$days_count]['hotel']['hotel_no_of_days'])." days",strtotime($checkin_date)));

              $itinerary_array['itinerary_details_day_wise'][$days_count]['hotel']['hotel_checkin']=$checkin_date;
              $itinerary_array['itinerary_details_day_wise'][$days_count]['hotel']['hotel_checkout']=$checkout_date;
              $hotel_images=unserialize($fetch_hotel['hotel_images']);
              if(!empty($hotel_images));
              {
                if($hotel_images[0]!="")
                {
                   $itinerary_images['hotel'][]=$hotel_images[0];
                }
              
              }

            }
          }
           if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))
            {
            $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);

            $itinerary_array['itinerary_details_day_wise'][$days_count]['sightseeing']['sightseeing_details']=$fetch_sightseeing;
            $itinerary_array['itinerary_details_day_wise'][$days_count]['sightseeing']['sightseeing_itinerary_details']=$itinerary_package_services[$days_count]['sightseeing'];

            $get_from_city=ServiceManagement::searchCities($fetch_sightseeing['sightseeing_city_from'],$itinerary_package_countries[$days_count]);
            $itinerary_array['itinerary_details_day_wise'][$days_count]['sightseeing']['from_city']=$get_from_city['name']."-";


            $between_city="";
            if($fetch_sightseeing['sightseeing_city_between']!=null && $fetch_sightseeing['sightseeing_city_between']!="")
            {
              $all_between_cities=explode(",",$fetch_sightseeing['sightseeing_city_between']);
              for($cities=0;$cities< count($all_between_cities);$cities++)
              {
                $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$itinerary_package_countries[$days_count]);
                $between_city.=$fetch_city['name']."-";
              }
            }
            $itinerary_array['itinerary_details_day_wise'][$days_count]['sightseeing']['between_city']=$between_city;


            $get_from_city=ServiceManagement::searchCities($fetch_sightseeing['sightseeing_city_to'],$itinerary_package_countries[$days_count]);
            $itinerary_array['itinerary_details_day_wise'][$days_count]['sightseeing']['to_city']=$get_from_city['name'];

            $sightseeing_images=unserialize($fetch_sightseeing['sightseeing_images']);
              if(!empty($sightseeing_images));
              {
                if($sightseeing_images[0]!="")
                {
                   $itinerary_images['sightseeing'][]=$sightseeing_images[0];
                }
              }
            }

      
                                                $activity_id_status=0;
                                                if(isset($itinerary_package_services[$days_count]['activity']['activity_id']))
                                                {
                                                    $check_activity_id_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);

                                                if($check_activity_id_count==1)
                                                {
                                                    if($itinerary_package_services[$days_count]['activity']['activity_id'][0]!="")
                                                    {
                                                        $activity_id_status++;
                                                    }
    
                                                }
                                                else
                                                {
                                                    for($check=0;$check< $check_activity_id_count; $check++)
                                                    {
                                                        if($itinerary_package_services[$days_count]['activity']['activity_id'][$check]!="")
                                                        {
                                                             $activity_id_status++;
                                                        }
                                                    }
                                                } 
                                                }

                                                
                                                if($activity_id_status>0)
                                                {
                                                  $activity_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);

                                                  for($activity_counter=0;$activity_counter < $activity_count;$activity_counter++)
                                                  {
                                                      if($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]=="")
                                                        {
                                                          continue;
                                                        }



                                                   $fetch_activity=ServiceManagement::searchActivity($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]);

                                                    $adult_age="";
                                                        $child_age="";
                                                        $age_group_details=unserialize($fetch_activity['age_group_details']);
                                                        if(!empty($age_group_details))
                                                        {
                                                            $adult_age=$age_group_details['adults'];
                                                            $child_age=$age_group_details['child'];

                                                        }
                                                        if(!empty($adult_age) && $adult_age['allowed']=="yes") 
                                                        {
                                                         $activity_pax_count_array[$days_count][$activity_counter]['adult_price_details']=unserialize($fetch_activity['adult_price_details']);
                                                     }
                                                     else{
                                                     $activity_pax_count_array[$days_count][$activity_counter]['adult_price_details']=array();

                                                 }

                                                 if(!empty($child_age) && $child_age['allowed']=="yes") 
                                                 { 
                                                   $activity_pax_count_array[$days_count][$activity_counter]['child_price_details']=unserialize($fetch_activity['child_price_details']);
                                               }
                                               else
                                               {
                                                $activity_pax_count_array[$days_count][$activity_counter]['child_price_details']=array(); 
                                            }

                                             $activity_not_available=0;
                                               $check_activity_avail=Activities::where('activity_id',$itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter])->where('validity_fromdate','<=',$days_date)->where('validity_todate','>=',$days_date)->get();
                                                if(count($check_activity_avail)<=0){
                                                        $activity_not_available=1;
                                                     }

                                                   $itinerary_array['itinerary_details_day_wise'][$days_count]['activity'][$activity_counter]['activity_details']=$fetch_activity;
                                                   $itinerary_array['itinerary_details_day_wise'][$days_count]['activity'][$activity_counter]['activity_itinerary_details']=$itinerary_package_services[$days_count]['activity'];

                                                   $activity_images=unserialize($fetch_activity['activity_images']);

                                                     $itinerary_array['itinerary_details_day_wise'][$days_count]['activity'][$activity_counter]['activity_not_available']=$activity_not_available;
                                                   if(!empty($activity_images));
                                                   {
                                                    if($activity_images[0]!="")
                                                    {

                                                     $itinerary_images['activity'][]=$activity_images[0];
                                                   }
                                                 }
                                               }
                                             }


            if($itinerary_package_services[$days_count]['transfer']['transfer_id']!="")
            {
               $fetch_transfer=ServiceManagement::searchTransfers($itinerary_package_services[$days_count]['transfer']['transfer_id']);

              $itinerary_array['itinerary_details_day_wise'][$days_count]['transfer']['transfer_details']=$fetch_transfer;

              $itinerary_array['itinerary_details_day_wise'][$days_count]['transfer']['transfer_itinerary_details']=$itinerary_package_services[$days_count]['transfer'];

                 $transfer_name="";
                 if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="from-airport") 
                 {
                  $fetch_from_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_from_airport']);

                  $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                  $transfer_name="From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];

                }
                else if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="to-airport")
                {

                  $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                  $fetch_to_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_to_airport']);

                  $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];

                }
                else
                {

                  $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                  $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                  $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];

                }
                $itinerary_array['itinerary_details_day_wise'][$days_count]['transfer']['transfer_name']=$transfer_name;
              $transfer_images=unserialize($fetch_transfer['transfer_vehicle_images']);
              if(!empty($transfer_images));
              {
                if($transfer_images[0][0]!="")
                {
                   $itinerary_images['transfer'][]=$transfer_images[0][0];
                }
              
              }

            }


         }

            $itinerary_image['itinerary'][]=unserialize($get_itinerary->itinerary_image);
            $itinerary_array['itinerary_details_day_wise'][$days_count]['day_number']=($days_count+1);
            $itinerary_array['itinerary_details_day_wise'][$days_count]['day_title']="";
            $itinerary_array['itinerary_details_day_wise'][$days_count]['day_desc']="";
            $days_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from)));
            $itinerary_array['itinerary_details_day_wise'][$days_count]['date']=$days_date;

             $itinerary_array['itinerary_exclusions']=$get_itinerary->itinerary_exclusions;
              $itinerary_array['itinerary_terms_and_conditions']=$get_itinerary->itinerary_terms_and_conditions;
               $itinerary_array['itinerary_cancellation']=$get_itinerary->itinerary_cancellation;
                $itinerary_array['total_cost']=$get_itinerary->itinerary_total_cost;
            $itinerary_array['itinerary_images']= $itinerary_images;

            $itinerary_markup=0;

            $fetch_markup=CustomerMarkup::where('customer_markup',"Itinerary")->first();
            if($fetch_markup)
            {
              $itinerary_markup=$fetch_markup['customer_markup_cost'];

            }

            $get_vehicles=VehicleType::get();

        $itinerary_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "itinerary"=>$itinerary_array,
          "markup"=>$itinerary_markup,
          "get_vehicles"=>$get_vehicles,
          "activity_pax_count_array"=>$activity_pax_count_array);
        return json_encode($itinerary_data);
      }
      else
      {
        $itinerary_data=array("errorCode"=>"1","errorMessage"=>"No data Found");
        return json_encode($itinerary_data);
      }
    }

    public function itinerary_get_hotels(Request $request)
    {

      $country_id=$request->get('country_id');
      $city_id=$request->get('city_id');
      $hotel_checkin=$request->get('hotel_checkin');
      $hotel_current_id=$request->get('hotel_current_id');

      $limit=16;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }
      $hotel_price_min=$hotel_price_max=0;
      $hotel_price_low=$hotel_price_high=array();
      if(!empty($hotel_price))
      {
        foreach($hotel_price as $price_value)
        {
          $prices=explode("-",$price_value);
          $hotel_price_low[]=$prices[0];
          $hotel_price_high[]=$prices[1];
        }
        $hotel_price_min=min($hotel_price_low);
        $hotel_price_max=max($hotel_price_high);
      }


    $hotel_markup=0;


      $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id);

      if($hotel_checkin!="")
      {
         $fetch_hotels=$fetch_hotels->where('booking_validity_from',"<=",$hotel_checkin)->where('booking_validity_to',">=",$hotel_checkin);
      }

      $fetch_hotels=$fetch_hotels->where('hotel_approve_status',1)->where('hotel_status',1)->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','desc')->offset($offset*$limit)->take($limit)->get();

      $hotel_cost=array();
      foreach($fetch_hotels as $hotel_key=>$hotels)
      {

        $hotel_rooms=HotelRooms::where('hotel_id',$hotels->hotel_id)->get();

        $hotel_currency=$hotels->hotel_currency;

        $room_min_price=0;
        foreach($hotel_rooms as $rooms_value)
        {
          if($hotel_currency==NULL || $hotel_currency=="")
          {
            $hotel_currency=$rooms_value->hotel_room_currency;
          }
          $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$rooms_value->hotel_room_id)->where('hotel_room_season_validity_from','<=',$hotel_checkin)->where('hotel_room_season_validity_to','>=',$hotel_checkin)->get();

          foreach($get_hotel_seasons as $hotel_seasons)
          {
            $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();

            foreach($get_occupancy as $occupancy)
            {
              if($hotel_price_min==0 && $hotel_price_max==0)
              {
                  if($room_min_price==0)
              {
                $room_min_price=$occupancy->hotel_room_occupancy_price;
              }
              else if($occupancy->hotel_room_occupancy_price<$room_min_price)
              {
                $room_min_price=$occupancy->hotel_room_occupancy_price;
              }
              }
              else if(($hotel_price_min==0 || $hotel_price_min>0) && $hotel_price_max>0)
              {
                 if($room_min_price==0)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost && $hotel_price_max>=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;

                  }
                  else if($occupancy->hotel_room_occupancy_price<$room_min_price)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost && $hotel_price_max>=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;

                  }
              }
              else if(($hotel_price_min==0 || $hotel_price_min>0) && $hotel_price_max==0)
              {
                 if($room_min_price==0)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;

                  }
                  else if($occupancy->hotel_room_occupancy_price<$room_min_price)
                  {
                    $markup_cost=round($occupancy->hotel_room_occupancy_price*$hotel_markup)/100;
                     $total_markup_cost=round($occupancy->hotel_room_occupancy_price+$markup_cost);

                    if($hotel_price_min<=$total_markup_cost)
                    $room_min_price=$occupancy->hotel_room_occupancy_price;
                  
                  }
              }
            
            }
          }
        }

          $new_currency="";
          if($room_min_price>0)
          {

            $price=$room_min_price;
            
            if($hotel_currency!="GEL")
            {
              $conversion_price=0;
              $new_currency= $hotel_currency;
              $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
              $scriptDocument = new \DOMDocument();
                  libxml_use_internal_errors(TRUE); //disable libxml errors
                  if(!empty($fetch_html)){
                     //load
                    $scriptDocument->loadHTML($fetch_html);

                  //init DOMXPath
                    $scriptDOMXPath = new \DOMXPath($scriptDocument);
                    //get all the h2's with an id
                    $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                    //check
                    if($scriptRow->length > 0){
                      foreach($scriptRow as $row){
                        $conversion_price=round($row->nodeValue,2);
                      }
                    }

                  }
                 $hotel_cost[$hotel_key]=round($price*$conversion_price);

              }
              else
              {
                 $hotel_cost[$hotel_key]=round($price);
              }

      }
  }

  

      if(count($fetch_hotels)>0)
      {
        $data=array();
          $fetch_hotels['markup']= $hotel_markup;
        $data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "hotels"=>$fetch_hotels,
              "hotel_cost"=>$hotel_cost,
              "hotel_price_min"=>$hotel_price_min,
              "hotel_price_max"=>$hotel_price_max,);
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }

    }
    public function itinerary_get_hotel_details(Request $request)
    {
    
     $hotel_id=$request->get('hotel_id');
     $checkin_date=$request->get('hotel_checkin');
      $checkout_date=$request->get('hotel_checkout');
      $room_occupancy_ids=$request->get('room_occupancy_ids');
       $room_quantity_array=$request->get('room_quantity_array');

    $html="";
    $fetch_hotel=Hotels::where('hotel_id',$hotel_id)->first();
    if($fetch_hotel)
    {
      $hotel_images=unserialize($fetch_hotel->hotel_images);
         $get_hotel_booking_count=Bookings::where('booking_type','hotel')->where(function ($q) use($checkin_date,$checkout_date){
    $q->where(function ($q1) use($checkin_date){ 

      $q1->where('booking_selected_date',"<=",$checkin_date)
      ->where('booking_selected_to_date',">=",$checkin_date);
    })->orWhere(function ($q2) use($checkout_date){ 

      $q2->where('booking_selected_date',"<=",$checkout_date)
      ->where('booking_selected_to_date',">=",$checkout_date);
    });

        })->where('booking_type_id',$fetch_hotel['hotel_id'])->where('booking_admin_status','!=',2)->get();
      $past_bookings_array=$get_hotel_booking_count->toArray();

        $get_hotel_booking_count=BookingCustomer::where('booking_type','hotel')->where(function ($q) use($checkin_date,$checkout_date){
    $q->where(function ($q1) use($checkin_date){ 

      $q1->where('booking_selected_date',"<=",$checkin_date)
      ->where('booking_selected_to_date',">=",$checkin_date);
    })->orWhere(function ($q2) use($checkout_date){ 

      $q2->where('booking_selected_date',"<=",$checkout_date)
      ->where('booking_selected_to_date',">=",$checkout_date);
    });

        })->where('booking_type_id',$fetch_hotel['hotel_id'])->get();
      $past_bookingscustomers_array=$get_hotel_booking_count->toArray();
      $past_bookings_array=array_merge($past_bookings_array, $past_bookingscustomers_array);
  
                        $booking_room_with_qty=array(); 
                        $count=0;
                        foreach($past_bookings_array as $past_bookings)
                        {
                          $room_count_details=explode(",",$past_bookings['booking_rooms_count_details']);

                          for($index=0;$index< count($room_count_details);$index++)
                          {
                            $get_room_array=explode("---",$room_count_details[$index]);

                            $booking_room_with_qty[$index]['room_id']=$get_room_array[0];
                             $booking_room_with_qty[$index]['room_occupancy_id']=$get_room_array[1];
                            $booking_room_with_qty[$index]['room_qty']=$get_room_array[3];

                          }

                           
                        }
       

                  
      $html.=' <style>
      .hotel-content {
        padding: 15px;
      }
      .checked {
        color: orange;
      }
      .carousel-inner img {
        width: 100%;
        height: 100%;
      }
      h2.title-name {
        color: #F44336;
        font-size: 24px;
        display: inline-block;
      }
      .rating-div {
        display: inline-block;
        margin-left: 10px;
      }
      p.sub.sub-title {
        color: #ffa500 !IMPORTANT;
        font-size: 15px;
        background: #ffffc0;
        display: inline-block;
        padding: 10px;
        border-radius: 5px;
      }
      .sub {
        color: gray !important;
        font-size: 15px !important;
      }
      .carousel-inner {
        border-radius: 6px;
      }
      div#demo {
        margin-top: 25px;
      }
      p.check-in {
        color: black;
        font-weight: 600;
        margin-bottom: 4px;
      }
      h2.title {
        font-size: 20px;
      }
      p.paragraph {
        color: #4E4E4E;
        font-weight: 400;
      }
      button.read-more {
        border: 0px;
        background: transparent;
        color: #008cff;
      }
      .blackfont {
        color: #000000;
        padding: 0px 6px;
        font-weight: 600;
      }
      ul.select-rooms {
        list-style-type: none;
        display: inline-flex;
        padding: 0px;
      }
      ul.select-rooms li {
        padding-left: 15px;
      }
      ul.select-rooms i {
        font-size: 12px;
        font-weight: normal;
        padding-right: 8px;
      }
      span.text-rt {
        float: right;
      }
      span.text-middle {
        padding-left: 9rem;
      }
      a.btn-include {
        border-radius: 34px;
        background-color: #ffffff;
        box-shadow: 0 1px 7px 0 rgba(0, 0, 0, 0.2);
        display: inline-block;
        flex-shrink: 0;
        color: #008cff;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: 700;
        padding: 9px 20px;
        cursor: pointer;
        outline: 0;
        border: 0;
        text-align: center;
        min-width: 90px;
      }
      .p-div{
        display: flex;
        justify-content: space-between;
      }
      .about {
        background: #cbfffa;
        color: #066b61;
        padding: 15px;
        margin-bottom: 15px;
      }
      .amenties {
        background: #d6ffd9;
        padding: 15px;
      }
      .blackfont {
        color: #33883a;
        padding: 0px 6px;
        font-weight: 600;
        font-size: 20px;
      }
      .amenties {
        background: #d6ffd9;
        padding: 15px;
        margin-bottom: 20px;
      }

      .room_selected_qty
     {
      width: 90%;
      padding: 8px;
     }

      * {box-sizing:border-box}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Hide the images by default */
.myhotelSlides {
  display: none;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -22px;
  padding: 16px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
}

/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

.active-slide, .dot:hover {
  background-color: #717171;
}

/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}

@-webkit-keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

@keyframes fade {
  from {opacity: .4}
  to {opacity: 1}
}

      </style>
      <body>
      <div class="hotel-content">
      <h2 class="title-name">'.$fetch_hotel->hotel_name.'</h2>
      <div class="rating-div">';
      for($i=1;$i<=5;$i++)
      {
        if($i<=$fetch_hotel->hotel_rating)
        {
          $html.="<span class='fa fa-star checked'></span>";
        }
        else
        {
          $html.="<span class='fa fa-star'></span>";
        }

      }

      $html.='</div>
      <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_address.'">';
       if(!empty($hotel_images))
      {
        $html.=' <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$fetch_hotel->hotel_id.'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotel_images[0].'">';
      }
      else
      {
        $html.='<input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$fetch_hotel->hotel_id.'" value="'.asset("assets/images/no-photo.png").'">';
      }
      $html.='<input type="hidden" class="search_hotel_id" name="search_hotel_id__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_id.'">
      <input type="hidden" class="search_hotel_name" name="search_hotel_name__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_name.'">
      <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_rating.'">
       <input type="hidden" class="search_hotel_detail_link" name="search_hotel_detail_link__'.$fetch_hotel->hotel_id.'" value="'.route('hotel-detail',['hotelid'=>base64_encode($fetch_hotel->hotel_id),'itinerary'=>1]).'">
      <br>
      <p class="sub sub-title">'.$fetch_hotel->hotel_address.'</p>';
      $html.='<!-- Slideshow container -->
      <div class="slideshow-container">

      <!-- Full-width images with number and caption text -->';
      if(!empty($hotel_images))
      {
        for($hotel_count=0;$hotel_count< count($hotel_images);$hotel_count++)
        {
          $html.='<div class="myhotelSlides fade">
          <div class="numbertext">'.($hotel_count+1).'/'.count($hotel_images).'</div>
          <img src='.asset('assets/uploads/hotel_images').'/'.rawurlencode($hotel_images[$hotel_count]).' style="width:100%">
          </div>';
        }
      }

      $html.='<!-- Next and previous buttons -->
      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>
      </div>
      <br>

      <!-- The dots/circles -->
      <div style="text-align:center">';
      if(!empty($hotel_images))
      {
        for($hotel_count=0;$hotel_count< count($hotel_images);$hotel_count++)
        {
          $html.='<span class="dot" onclick="currentSlide('.(count($hotel_images)+$hotel_count+1).')"></span>';
        }
      }

     $html.='</div><hr class="h-hr">
      <div class="about py-4">
      <h2 class="title">About</h2>
      <p class="paragraph">'.$fetch_hotel->hotel_description.'</p>
      </div>';
      $get_amenities=unserialize($fetch_hotel->hotel_amenities);
      if($get_amenities==null || $get_amenities=="")
      {
        $html.="";
      }
      else
      {
        $html.='
        <div class="amenties">
        <p class="blackfont">Amenities</p>
        <div class="container">
        <div class="row">';
        foreach($get_amenities as $amenities)
        {
          if(!empty($amenities[1]))
          {
            $html.='<div class="col-md-4">
            <h3 class="amenty-title">';
            $get_amenities_name=LoginController::fetchAmenitiesName($amenities[0]);
            $html.=$get_amenities_name['amenities_name'];
            $html.='</h3>';
            foreach($amenities[1] as $sub_amenities)
            {
              $html.='<p class="amenty-item"><i class="fa fa-check"></i>';
              $get_sub_amenities_name=LoginController::fetchSubAmenitiesName($sub_amenities,$amenities[0]);
              $html.=$get_sub_amenities_name['sub_amenities_name'];
              $html.='</p>';
            }
            $html.='</div>';
          }
        }
        $html.='</div>
        </div>
        </div>';
      }
      $html.='<p class="blackfont">Select Rooms</p><hr>';


    
     $hotel_rooms=HotelRooms::where('hotel_id',$fetch_hotel->hotel_id)->get();

     $hotel_currency=$fetch_hotel->hotel_currency;


     $room_min_price=0;
     foreach($hotel_rooms as $rooms_value)
     {

      if($hotel_currency==NULL || $hotel_currency=="")
      {
        $hotel_currency=$rooms_value->hotel_room_currency;
      }
    }
      
      $currency_price_array=array();
    
        if($hotel_currency!="GEL")
        {
           $new_currency=$hotel_currency;


          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$hotel_currency]=round($conversion_price,2);
        }
         

      $currency_price_array["GEL"]=1;


    $hotel_rooms=HotelRooms::where('hotel_id',$fetch_hotel->hotel_id)->get();
    $hotel_meal=HotelMeal::get();
     $currency=$hotel_currency;
    $html.='<div class="row" style="margin-top:20px" >
        <div class="col-md-12" >
            <table class="table table-bordered" id="room_type_table">
                <thead>
                    <tr>
                        <th>Room Type</th>
                        <th>Sleeps/ Occupancy</th>
                        <th>Meal</th>
                        <th>Today\'s Price</th>
                        <th>Select Rooms</th>
                    </tr>
                </thead>
                <tbody>';
                 
                    foreach($hotel_rooms as $room_key=>$room_values)
                  {
                    if($currency==null)
                    {
                        $currency=$room_values->hotel_room_currency;
                    }
                    $room_price_currency_rate=$currency_price_array[$currency];
                     $html_rooms="";
                        $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$room_values->hotel_room_id)->where('hotel_room_season_validity_from','<=',$checkin_date)->where('hotel_room_season_validity_to','>=',$checkin_date)->get();
                    $sleeps_count=0;
                    $occupany_counter=0;

                        foreach($get_hotel_seasons as $hotel_seasons)
                        {

                            $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();
                             $sleeps_count+=count($get_occupancy);
                            foreach($get_occupancy as $occupancy)
                            {
                                if($occupany_counter!=0)
                                {
                                 $html_rooms.="<tr>"; 
                                }
                                 $rooms_occupancy_qty="";
                                    $total_cost=round($occupancy->hotel_room_occupancy_price*$room_price_currency_rate);
                                    if(in_array($occupancy->hotel_room_occupancy_id,$room_occupancy_ids))
                                    {
                                      $room_occupancy_ids_index=array_search($occupancy->hotel_room_occupancy_id,$room_occupancy_ids);
                                      if(array_key_exists($room_occupancy_ids_index, $room_quantity_array))
                                      {
                                          $rooms_occupancy_qty=$room_quantity_array[$room_occupancy_ids_index];
                                      }
                                    
                                    }
                                 $html_rooms.='<td><input type="hidden" name="room_occupancy_id__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="'.$occupancy->hotel_room_occupancy_id.'"><input type="hidden" name="room_occupancy_qty__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="'.$occupancy->hotel_room_occupancy_qty.'">';
                                 for($sleeps=1;$sleeps<=$occupancy->hotel_room_occupancy_qty;$sleeps++)
                                 {
                                    $html_rooms.='<span class="fa fa-user"></span>';
                                 }

                                
                                 $html_rooms.='</td>';
                                  foreach($hotel_meal as $meal)
                                  {
                                    if($meal->hotel_meals_id==$room_values->hotel_room_meal)
                                    {
                                      $html_rooms.="<td>$meal->hotel_meals_name</td>";  
                                    }
                                  }
                                  
                                    $past_booking_qty=0;
                                    foreach($booking_room_with_qty as $room_with_qty_key=>$room_with_qty_value)
                                    {
                                      if($room_with_qty_value['room_id']==$room_values->hotel_room_id)
                                      {
                                        $past_booking_qty+=$room_with_qty_value['room_qty'];
                                      }

                                    }

                                  $html_rooms.='<td> <input type="hidden" name="room_price_convert_rate__'.$room_key.'__'.$fetch_hotel->hotel_id.'" value="'.$room_price_currency_rate.'"> <input type="hidden" name="room_price__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="'.$total_cost.'"><input type="hidden" name="room_price_currency__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="GEL"> GEL '.($total_cost).'</td><td>';

                                   if($rooms_occupancy_qty!="")
                                 {
                                  $html_rooms.='<span style="font-weight:600;">('.$rooms_occupancy_qty.' Rooms)</span>';

                                 }

                               $html_rooms.='<select name="room_select__'.$room_key.'__'.$occupany_counter.'" class="form-control room_select room_select__'.$room_key.' room_selected_qty" id="room_select__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'">
                                        <option value="0">0</option>';
                                        for($i=1;$i<=($room_values->hotel_room_no_of_rooms-$past_booking_qty);$i++)
                                        {
                                          $html_rooms.="<option value='".$i."'";

                                          // if($rooms_occupancy_qty==$i)
                                          // {
                                          //   $html_rooms.=" selected='selected'";
                                          // }

                                          $html_rooms.=">".$i." ( GEL ".($total_cost*$i)." )</option>";  
                                        }
                                    
                                  $html_rooms.="</select>";

                                  $html_rooms.="</td></tr>";
                                   $occupany_counter++;

                            }
                        }

                    $html.='<tr>
                        <td rowspan="'.$sleeps_count.'">
                            <input type="hidden" name="room_id__'.$room_key.'__'.$fetch_hotel->hotel_id.'" value="'.$room_values->hotel_room_id.'">
                            <input type="hidden" name="room_name__'.$room_key.'__'.$fetch_hotel->hotel_id.'" value="'.ucwords($room_values->hotel_room_class).' '.ucwords($room_values->hotel_room_type).' Room">';
                           $html.=ucwords($room_values->hotel_room_class).' '.ucwords($room_values->hotel_room_type).' Room ('.$room_values->hotel_room_adults.' Adults + ';
                            if($room_values->hotel_room_cwb=="")
                            {
                              $html.='0';
                            }
                            else 
                              {
                                $html.=''.$room_values->hotel_room_cwb;
                              }

                             $html.=' Child)
                        </td>
                        '.$html_rooms;
                  
                   }


                $html.='</tbody>
            </table>
        </div>
    </div>';
      $html.='</div>
      <script>
      var slideIndex = '.(count($hotel_images)+1).';
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
  slideIndex += n
  if(slideIndex<'.(count($hotel_images)+1).')
  {
    slideIndex='.(count($hotel_images)+1).'
  }
showSlides(slideIndex)

}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("myhotelSlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = '.(count($hotel_images)+1).'}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active-slide", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active-slide";
}
      </script>';
      echo $html;
      // $get_hotel_data=array("errorCode"=>"0",
      //     "errorMessage"=>"",
      //     "hotel_details"=>utf8_encode($html));
      //   return json_encode($get_hotel_data);
    }
    else
    {
      $html="";
      // $get_hotel_data=array("errorCode"=>"0",
      //     "errorMessage"=>"",
      //     "hotel_details"=>$html);
      //   return json_encode($get_hotel_data);
    }
      

    }
    public function itinerary_get_activities(Request $request)
    {

      $country_id=$request->get('country_id');

      $city_id=$request->get('city_id');
    $cities_array=explode(",",$city_id);
     $cities_array=array_unique($cities_array);
    $cities_array=array_filter($cities_array);

     $activity_date=$request->get('activity_date');
     $exist_activity_ids=$request->get('exist_activity_ids');


      $fetch_activites=Activities::where('activity_country',$country_id)->whereIn('activity_city',$cities_array)->where('validity_fromdate','<=',$activity_date)->where('validity_todate','>=',$activity_date);

     if(!empty($exist_activity_ids))
     {
      $fetch_activites=$fetch_activites->whereNotIn('activity_id',$exist_activity_ids);
     }

      $fetch_activites=$fetch_activites->where('activity_status',1)->orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','desc')->get();
    $fetch_activity_array=array();
    $count=0;
      foreach($fetch_activites as $activities)
      {
         $operating_days=unserialize($activities->operating_weekdays);
      $operating_days_array=array();
      foreach($operating_days as $key=>$days)
      {
        if($days=="Yes")
        {
          $operating_days_array[]= ucwords($key);
        }
      }

      $blackout_days=explode(',',$activities->activity_blackout_dates);
       $calculated_day=date("l",strtotime($activity_date));

      $check_blackout_operating=0;
      if(in_array($activity_date, $blackout_days) || !in_array($calculated_day, $operating_days_array))
      {
        $check_blackout_operating=1;
      }

       $adult_price=0;
      $adult_price_details=unserialize($activities->adult_price_details);

      if(!empty($adult_price_details))
      {
         $adult_price=$adult_price_details[0]['adult_pax_price'];
      }
      else
      {
        $adult_price=0;
      }
     

      $total_cost=round($adult_price);


        $get_activity_booking_count=Bookings::where('booking_type','activity')->where('booking_selected_date',$activity_date)->where('booking_type_id',$activities->activity_id)->where('booking_admin_status','!=',2)->get();

        $past_bookings=0;
        foreach($get_activity_booking_count as $book_count)
        {
          $past_bookings+=is_numeric($book_count->booking_adult_count) ? $book_count->booking_adult_count : 0;
          $past_bookings+=is_numeric($book_count->booking_child_count) ? $book_count->booking_child_count : 0;
          $past_bookings+=is_numeric($book_count->booking_infant_count) ? $book_count->booking_infant_count : 0;
        }


         $get_activity_booking_customer_count=BookingCustomer::where('booking_type','activity')->where('booking_selected_date',$activity_date)->where('booking_type_id',$activities->activity_id)->get();

        foreach($get_activity_booking_customer_count as $book_count)
        {
          $past_bookings+=is_numeric($book_count->booking_adult_count) ? $book_count->booking_adult_count : 0;
          $past_bookings+=is_numeric($book_count->booking_child_count) ? $book_count->booking_child_count : 0;
          $past_bookings+=is_numeric($book_count->booking_infant_count) ? $book_count->booking_infant_count : 0;
        }


        $no_of_bookings=0;
        $availability_qty_details=unserialize($activities->availability_qty_details);

        for($avail=0;$avail< count($availability_qty_details);$avail++)
        {
          if($activity_date>=$availability_qty_details[$avail]['availability_from'] && $activity_date<=$availability_qty_details[$avail]['availability_to'])
          {
            if(!empty($availability_qty_details[$avail]["availability_time_from"]))
            {
              $no_of_bookings=$availability_qty_details[$avail]["availability_no_of_bookings"][0];    
            }
            else
            {
              $no_of_bookings=0;

            }
            // $no_of_bookings=$availability_qty_details[$avail]['no_of_bookings'];
          }
        }

        $no_of_bookings=$no_of_bookings-$past_bookings;

         if($no_of_bookings>0 && $check_blackout_operating==0)
        {
          $adult_age="";
          $child_age="";
          $age_group_details=unserialize($activities->age_group_details);
          $adult_price_details=unserialize($activities->adult_price_details);
          $child_price_details=unserialize($activities->child_price_details);

          if(!empty($age_group_details))
          {
            $adult_age=$age_group_details['adults'];
            $child_age=$age_group_details['child'];

          }

          if(!empty($adult_age) && $adult_age['allowed']=="yes") 
          {
            if(!empty($adult_price_details))
            {
              $min_adult=$adult_price_details[0]['adult_min_pax'];
              $max_adult=$adult_price_details[0]['adult_max_pax'];
              foreach($adult_price_details as $adult_price_i)
              {
                if($adult_price_i['adult_min_pax']<$min_adult)
                {
                  $min_adult=$adult_price_i['adult_min_pax'];
                }
                if($adult_price_i['adult_max_pax']>$max_adult)
                {
                  $max_adult=$adult_price_i['adult_max_pax'];
                }

              }

            }
            else
            {
             $min_adult=0;
             $max_adult=0; 
           }


         }
         else
         {
           $min_adult=0;
           $max_adult=0; 
         }


         if(!empty($child_age) && $child_age['allowed']=="yes") 
         { 
         if(!empty($child_price_details))
         {
          $min_child=$child_price_details[0]['child_min_pax'];
          $max_child=$child_price_details[0]['child_max_pax'];
          foreach($child_price_details as $child_price_i)
          {  
            if($child_price_i['child_min_pax']<$min_child)
            {
              $min_child=$child_price_i['child_min_pax'];
            }
            if($child_price_i['child_max_pax']>$max_child)
            {
              $max_child=$child_price_i['child_max_pax'];
            }

          }
        }
        else
        {
         $min_child=0;
         $max_child=0;

       }
     }
     else
     {

$min_child=0;
         $max_child=0;
     }


        $fetch_activity_array[$count]['activity_id']=$activities->activity_id;
        $fetch_activity_array[$count]['activity_currency']=$activities->activity_currency;
        $fetch_activity_array[$count]['total_cost']=$total_cost;
        $fetch_activity_array[$count]['activity_name']=$activities->activity_name;
        $fetch_activity_array[$count]['activity_location']=$activities->activity_location;
        $fetch_activity_array[$count]['activity_images']=$activities->activity_images;
        $fetch_activity_array[$count]['adult_min_max']=$min_adult.'-'.$max_adult;
        $fetch_activity_array[$count]['child_min_max']=$min_child.'-'.$max_child;

        $count++;

      }

      }
      $get_activity_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "activity"=>$fetch_activity_array);
        return json_encode($get_activity_data);
    }

    public function itinerary_get_sightseeing(Request $request)
    {

      $country_id=$request->get('country_id');

      $city_id=$request->get('city_id');
        $sightseeing_current_id=$request->get('sightseeing_current_id');
          $prev_city_id=$request->get('prev_city_id');
 $limit=16;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }
       $fetch_sightseeing_array=array();
  $count=0;


      if($offset==0){

         if($prev_city_id!="")
    {
      $fetch_sightseeing_current=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id,$prev_city_id){
          $query->where(function ($query1) use ($city_id,$prev_city_id){
            $query1->where('sightseeing_city_from',$city_id)->orWhere('sightseeing_city_from',$prev_city_id);

          })->where('sightseeing_city_to',$city_id); });

    }
    else
    {
       $fetch_sightseeing_current=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id){
      $query->where('sightseeing_city_from',$city_id)->where('sightseeing_city_to',$city_id); });
    }
  $fetch_sightseeing_current=$fetch_sightseeing_current->where('sightseeing_id',$sightseeing_current_id)->where('sightseeing_status',1)->orderBy('sightseeing_id','desc')->offset($offset*$limit)->take($limit)->get();
  foreach($fetch_sightseeing_current as $sightseeing)
      {
         $address="";
         $fetch_sightseeing_array[$count]['sightseeing_id']=$sightseeing->sightseeing_id;
          $fetch_sightseeing_array[$count]['sightseeing_tour_name']=$sightseeing->sightseeing_tour_name;
    $fetch_sightseeing_array[$count]['sightseeing_adult_cost']=$sightseeing->sightseeing_adult_cost;
    $fetch_sightseeing_array[$count]['sightseeing_map_location']=$address;
    $fetch_sightseeing_array[$count]['sightseeing_default_guide_price']=$sightseeing->sightseeing_default_guide_price;
    $fetch_sightseeing_array[$count]['sightseeing_default_driver_price']=$sightseeing->sightseeing_default_driver_price;
    $fetch_sightseeing_array[$count]['sightseeing_food_cost']=$sightseeing->sightseeing_food_cost;
    $fetch_sightseeing_array[$count]['sightseeing_hotel_cost']=$sightseeing->sightseeing_hotel_cost;
    $fetch_sightseeing_array[$count]['sightseeing_images']=$sightseeing->sightseeing_images;
     $fetch_sightseeing_array[$count]['sightseeing_attractions']=htmlentities($sightseeing->sightseeing_attractions);
    $fetch_sightseeing_array[$count]['sightseeing_distance_covered']=$sightseeing->sightseeing_distance_covered;

     $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_from,$sightseeing->sightseeing_country);
         $address.=$get_from_city['name']."-";
          if($sightseeing->sightseeing_city_between!=NULL && $sightseeing->sightseeing_city_between!="")
         {
          $all_between_cities=explode(",",$sightseeing->sightseeing_city_between);
          for($cities=0;$cities< count($all_between_cities);$cities++)
          {
            $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$sightseeing->sightseeing_country);
            $address.=$fetch_city['name']."-";
          }
         }


         $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_to,$sightseeing->sightseeing_country);
         $address.=$get_from_city['name'];

    $fetch_sightseeing_array[$count]['sightseeing_map_location']=$address;



    $count++;
      }

      }

      if($offset==0 && $sightseeing_current_id!="")
      {
        $limit--;
      }

 if($prev_city_id!="")
    {
      $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id,$prev_city_id){
          $query->where(function ($query1) use ($city_id,$prev_city_id){
            $query1->where('sightseeing_city_from',$city_id)->orWhere('sightseeing_city_from',$prev_city_id);

          })->where('sightseeing_city_to',$city_id); });

    }
    else
    {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id){
      $query->where('sightseeing_city_from',$city_id)->where('sightseeing_city_to',$city_id); });
    }

      
  $fetch_sightseeing=$fetch_sightseeing->where('sightseeing_id','!=',$sightseeing_current_id)->where('sightseeing_default_guide_price',">",0)->where('sightseeing_default_driver_price',">",0)->where('sightseeing_status',1)->orderBy('sightseeing_popular_status','desc')->orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->orderBy('sightseeing_id','desc')->offset($offset*$limit)->take($limit)->get();

   
  foreach($fetch_sightseeing as $sightseeing)
      {
         $address="";
         $fetch_sightseeing_array[$count]['sightseeing_id']=$sightseeing->sightseeing_id;
          $fetch_sightseeing_array[$count]['sightseeing_tour_name']=$sightseeing->sightseeing_tour_name;
    $fetch_sightseeing_array[$count]['sightseeing_adult_cost']=$sightseeing->sightseeing_adult_cost;
    $fetch_sightseeing_array[$count]['sightseeing_map_location']=$address;
    $fetch_sightseeing_array[$count]['sightseeing_default_guide_price']=$sightseeing->sightseeing_default_guide_price;
    $fetch_sightseeing_array[$count]['sightseeing_default_driver_price']=$sightseeing->sightseeing_default_driver_price;
    $fetch_sightseeing_array[$count]['sightseeing_food_cost']=$sightseeing->sightseeing_food_cost;
    $fetch_sightseeing_array[$count]['sightseeing_hotel_cost']=$sightseeing->sightseeing_hotel_cost;
    $fetch_sightseeing_array[$count]['sightseeing_images']=$sightseeing->sightseeing_images;
      $fetch_sightseeing_array[$count]['sightseeing_attractions']=htmlentities($sightseeing->sightseeing_attractions);
    $fetch_sightseeing_array[$count]['sightseeing_distance_covered']=$sightseeing->sightseeing_distance_covered;

     $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_from,$sightseeing->sightseeing_country);
         $address.=$get_from_city['name']."-";
          if($sightseeing->sightseeing_city_between!=NULL && $sightseeing->sightseeing_city_between!="")
         {
          $all_between_cities=explode(",",$sightseeing->sightseeing_city_between);
          for($cities=0;$cities< count($all_between_cities);$cities++)
          {
            $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$sightseeing->sightseeing_country);
            $address.=$fetch_city['name']."-";
          }
         }


         $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_to,$sightseeing->sightseeing_country);
         $address.=$get_from_city['name'];

    $fetch_sightseeing_array[$count]['sightseeing_map_location']=$address;



    $count++;
      }

      $currency="GEL";


      $get_sightseeing_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "sightseeing"=>$fetch_sightseeing_array,
          "currency"=>$currency);
        return json_encode($get_sightseeing_data);
    }

    public function itinerary_get_sightseeing_details(Request $request)
    {
      $mainid=$request->sightseeing_id;
      $get_vehicles=VehicleType::get();
      $get_sightseeing=SightSeeing::where('sightseeing_status',1)->where('sightseeing_id',$mainid)->first();
      if($get_sightseeing)
      {
        $get_from_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_from,$get_sightseeing->sightseeing_country);
        $via_route=$get_from_city['name']."-";

        if($get_sightseeing->sightseeing_city_between!=null && $get_sightseeing->sightseeing_city_between!="")
        {
          $all_between_cities=explode(",",$get_sightseeing->sightseeing_city_between);
          for($cities=0;$cities< count($all_between_cities);$cities++)
          {
            $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$get_sightseeing->sightseeing_country);
            $via_route.=$fetch_city['name']."-";
          }
        }
        $get_from_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_to,$get_sightseeing->sightseeing_country);
        $via_route.=$get_from_city['name'];

        $sightseeing_images_array=array();
        if($get_sightseeing->sightseeing_images!=null && $get_sightseeing->sightseeing_images!="")
        {
          $sightseeing_images=unserialize($get_sightseeing->sightseeing_images);
          if(!empty($sightseeing_images[0]))
          {
            foreach($sightseeing_images as $sightseeing_image)
            {
              $sightseeing_images_array[]=asset("assets/uploads/sightseeing_images")."/".$sightseeing_image;
            }
          }
          else
          {
            $sightseeing_images_array[]=asset("assets/images/no-photo.png");
          }
        }
        else
        {
          $sightseeing_images_array[]=asset("assets/images/no-photo.png");
        }
        $price=$get_sightseeing->sightseeing_adult_cost;
        $price+=round($get_sightseeing->sightseeing_food_cost);
        $price+=round($get_sightseeing->sightseeing_hotel_cost);
        $total_adult_cost=round($price);
        $group_tour_status=0;
        if($get_sightseeing->sightseeing_group_adult_cost!=null && $get_sightseeing->sightseeing_group_adult_cost!="")
        {
          $group_tour_status=1;

        }
        $sightseeing_additional_cost=$get_sightseeing->sightseeing_additional_cost;
        if($get_sightseeing->sightseeing_additional_cost=="" || $get_sightseeing->sightseeing_additional_cost==null)
        {
          $sightseeing_additional_cost=0;
        }
        $sightseeing_cities="";
        if($get_sightseeing->sightseeing_city_between!="")
        {
          $sightseeing_cities=$get_sightseeing->sightseeing_city_between.",".$get_sightseeing->sightseeing_city_to;
        }
        else if($get_sightseeing->sightseeing_city_to!="")
        {
          $sightseeing_cities=$get_sightseeing->sightseeing_city_to;
        }
        else
        {
          $sightseeing_cities=$get_sightseeing->sightseeing_city_from;
        }
        $group_adult_cost=round($get_sightseeing->sightseeing_group_adult_cost);
        $data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "sightseeing_id"=>$get_sightseeing->sightseeing_id,
          "sightseeing_name"=>$get_sightseeing->sightseeing_tour_name,
          "sightseeing_route"=>$via_route,
          "sightseeing_cities"=>$sightseeing_cities,
          "sightseeing_distance_covered"=>$get_sightseeing->sightseeing_distance_covered." KMS",
          "sightseeing_duration"=>$get_sightseeing->sightseeing_duration." HOURS",
          "sightseeing_images"=>$sightseeing_images_array,
          "sightseeing_tour_desc"=>$get_sightseeing->sightseeing_tour_desc,
          "sightseeing_attractions"=>$get_sightseeing->sightseeing_attractions,
          "sightseeing_group_tour_status"=>$group_tour_status,
          "group_adult_cost"=>$group_adult_cost,
          "total_adult_cost"=>$total_adult_cost,
          "sightseeing_additional_cost"=>$sightseeing_additional_cost,
          "sightseeing_default_guide_price"=>$get_sightseeing->sightseeing_default_guide_price,
          "sightseeing_default_driver_price"=>$get_sightseeing->sightseeing_default_driver_price,
          "sightseeing_details_link"=>route('sightseeing-details-view',['sightseeing_id'=>base64_encode($get_sightseeing->sightseeing_id),'itinerary'=>1]));


        return json_encode($data);

      }
      else
      {
        $data=array();
        return json_encode($data);

      }
    }

    public function fetchItineraryGuidesDetails(Request $request)
    {
       $guide_id=$request->get('guide_id');
    $countries=Countries::get();
    $languages=languages::get();
    $get_guides=Guides::where('guide_id',$guide_id)->first();

    $languages_data=explode(",",$get_guides->guide_language);
    foreach($languages as $language)
    {
      if(in_array($language->language_id,$languages_data))
      {
        $languages_spoken[]=$language->language_name;
      }
    }
    $html='
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700&subset=latin-ext">
    <style>
.icon-2 {
  display: inline-block;
  width: 1em;
  height: 1em;
  stroke-width: 0;
  stroke: currentColor;
  fill: currentColor;
}

.wrapper-2 {
  width: 100%;
  width: 100%;
  height: auto;
  min-height: 100vh;
  padding: 0;
  padding-top: 70px;
  display: flex;
  margin-bottom: 50px;
  
}
@media screen and (max-width: 768px) {
  .wrapper-2 {
    height: auto;
    min-height: 100vh;
    padding-top: 100px;
  }
}

.profile-card {
  width: 100%;
  min-height: 460px;
  margin: auto;
  box-shadow: 0px 8px 60px -10px rgba(13, 28, 39, 0.6);
  background: #fff;
  border-radius: 12px;
  
  position: relative;
}
.profile-card.active .profile-card__cnt {
  filter: blur(6px);
}
.profile-card.active .profile-card-message,
.profile-card.active .profile-card__overlay {
  opacity: 1;
  pointer-events: auto;
  transition-delay: .1s;
}
.profile-card.active .profile-card-form {
  transform: none;
  transition-delay: .1s;
}
.profile-card__img {
  width: 150px;
  height: 150px;
  margin-left: auto;
  margin-right: auto;
  transform: translateY(-50%);
  border-radius: 50%;
  overflow: hidden;
  position: relative;
  z-index: 4;
  box-shadow: 0px 5px 50px 0px #6c44fc, 0px 0px 0px 7px rgba(107, 74, 255, 0.5);
}
@media screen and (max-width: 576px) {
  .profile-card__img {
    width: 120px;
    height: 120px;
  }
}
.profile-card__img img {
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 50%;
}
.profile-card__cnt {
  margin-top: -50px;
  text-align: center;
  padding: 0 20px;
  padding-bottom: 40px;
  transition: all .3s;
}
span.profile-card-loc__txt {
    position: absolute;
    top: 20px;
    right: 37px;
    font-size: 16px;
    color: red;
    background: #ebebeb;
    padding: 8px 15px;
    border-radius: 5px;
}
button.profile-card__button.button--orange {
    position: absolute;
    top: 65px;
    right: 21px;
}
.profile-card__name {
  font-weight: 700;
  font-size: 24px;
  color: #6944ff;
  margin-bottom: 15px;
}
.profile-card__txt {
  font-size: 18px;
  font-weight: 500;
  color: #324e63;
  margin-bottom: 15px;
}
.profile-card__txt strong {
  font-weight: 700;
}
.profile-card-loc {
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  font-weight: 600;
}
.profile-card-loc__icon {
  display: inline-flex;
  font-size: 27px;
  margin-right: 10px;
}
.profile-card-inf {
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  align-items: flex-start;
  margin-top: 0px;
}
.profile-card-inf__item {
  padding: 10px 35px;
  min-width: 150px;
}
p#supplier_id {
    margin: 0;
    text-align: center;
}
@media screen and (max-width: 768px) {
  .profile-card-inf__item {
    padding: 10px 20px;
    min-width: 120px;
  }
}
.profile-card-inf__title {
  font-weight: 700;
  font-size: 17px;
  color: #324e63;
}
.profile-card-inf__txt {
  font-weight: 500;
  margin-top: 7px;
}
.profile-card-social {
  margin-top: 0px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
}
.profile-card-social__item {
    display: inline-flex;
    width: 85px;
    height: 34px;
    margin: 15px;
    border-radius: 50px;
    align-items: center;
    justify-content: center;
    color: #fff;
    background: #405de6;
    box-shadow: 0px 7px 30px rgba(43, 98, 169, 0.5);
    position: relative;
    font-size: 21px;
    flex-shrink: 0;
    transition: all .3s;
}
.profile-card-social .icon-font {
    display: inline-flex;
    font-size: 14px;
}
@media screen and (max-width: 768px) {
  .profile-card-social__item {
    width: 50px;
    height: 50px;
    margin: 10px;
  }
}
.theme-rosegold a:hover, .theme-rosegold a:active, .theme-rosegold a:focus {
    color: #ffffff;
}
.profile-card-social__item.facebook {
  background: linear-gradient(45deg, #3b5998, #0078d7);
  box-shadow: 0px 4px 30px rgba(43, 98, 169, 0.5);
}
.profile-card-social__item.twitter {
  background: linear-gradient(45deg, #1da1f2, #0e71c8);
  box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.7);
}
.profile-card-social__item.instagram {
  background: linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d);
  box-shadow: 0px 4px 30px rgba(120, 64, 190, 0.6);
}
.profile-card-social__item.behance {
  background: linear-gradient(45deg, #1769ff, #213fca);
  box-shadow: 0px 4px 30px rgba(27, 86, 231, 0.7);
}
.profile-card-social__item.github {
  background: linear-gradient(45deg, #333333, #626b73);
  box-shadow: 0px 4px 30px rgba(63, 65, 67, 0.6);
}
.profile-card-social__item.codepen {
  background: linear-gradient(45deg, #324e63, #414447);
  box-shadow: 0px 4px 30px rgba(55, 75, 90, 0.6);
}
.profile-card-social__item.link {
  background: linear-gradient(45deg, #d5135a, #f05924);
  box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.6);
}
.profile-card-social .icon-font {
  display: inline-flex;
}
.profile-card-ctr {
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
}
@media screen and (max-width: 576px) {
  .profile-card-ctr {
    flex-wrap: wrap;
  }
}
.profile-card__button {
    background: none;
    border: none;
    font-family: "Quicksand", sans-serif;
    font-weight: 700;
    font-size: 18px;
    margin: 15px 35px;
    padding: 7px 31px;
    min-width: auto;
    border-radius: 50px;
    min-height: 50px;
    color: #fff;
    cursor: pointer;
    backface-visibility: hidden;
    transition: all .3s;
}
@media screen and (max-width: 768px) {
  .profile-card__button {
    min-width: 170px;
    margin: 15px 25px;
  }
}
@media screen and (max-width: 576px) {
  .profile-card__button {
    min-width: inherit;
    margin: 0;
    margin-bottom: 16px;
    width: 100%;
    max-width: 300px;
  }
  .profile-card__button:last-child {
    margin-bottom: 0;
  }
}
.profile-card__button:focus {
  outline: none !important;
}
@media screen and (min-width: 768px) {
  .profile-card__button:hover {
    transform: translateY(-5px);
  }
}
.profile-card__button:first-child {
  margin-left: 0;
}
.profile-card__button:last-child {
  margin-right: 0;
}
.profile-card__button.button--blue {
  background: linear-gradient(45deg, #1da1f2, #0e71c8);
  box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.4);
}
.profile-card__button.button--blue:hover {
  box-shadow: 0px 7px 30px rgba(19, 127, 212, 0.75);
}
.profile-card__button.button--orange {
  background: linear-gradient(45deg, #d5135a, #f05924);
  box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.35);
}
.profile-card__button.button--orange:hover {
  box-shadow: 0px 7px 30px rgba(223, 45, 70, 0.75);
}
.profile-card__button.button--gray {
  box-shadow: none;
  background: #dcdcdc;
  color: #142029;
}
.profile-card-message {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  padding-top: 130px;
  padding-bottom: 100px;
  opacity: 0;
  pointer-events: none;
  transition: all .3s;
}
.profile-card-form {
  box-shadow: 0 4px 30px rgba(15, 22, 56, 0.35);
  max-width: 80%;
  margin-left: auto;
  margin-right: auto;
  height: 100%;
  background: #fff;
  border-radius: 10px;
  padding: 35px;
  transform: scale(0.8);
  position: relative;
  z-index: 3;
  transition: all .3s;
}
@media screen and (max-width: 768px) {
  .profile-card-form {
    max-width: 90%;
    height: auto;
  }
}
@media screen and (max-width: 576px) {
  .profile-card-form {
    padding: 20px;
  }
}
.profile-card-form__bottom {
  justify-content: space-between;
  display: flex;
}
@media screen and (max-width: 576px) {
  .profile-card-form__bottom {
    flex-wrap: wrap;
  }
}
.profile-card textarea {
  width: 100%;
  resize: none;
  height: 210px;
  margin-bottom: 20px;
  border: 2px solid #dcdcdc;
  border-radius: 10px;
  padding: 15px 20px;
  color: #324e63;
  font-weight: 500;
  font-family: "Quicksand", sans-serif;
  outline: none;
  transition: all .3s;
}
.profile-card textarea:focus {
  outline: none;
  border-color: #8a979e;
}
.profile-card__overlay {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  pointer-events: none;
  opacity: 0;
  background: rgba(22, 33, 72, 0.35);
  border-radius: 12px;
  transition: all .3s;
}
.main-footer {
    background-color: rgb(254, 254, 255);
    padding: 15px 30px;
    border-top: 1px solid #cccccc;
    z-index: 999;
    position: relative;
    width: 100%;
    left: 10px !important;
    text-align: center;
    margin: 0;
}
ul.nav.nav-tabs.c-nav {
    border: none;
    margin-top: 10px;
    margin-bottom: 10px;
}

img.ul-img {
    width: 100%;
    height: 100%;
}
li.c-item {
    display: flex;
}
a.img-div {
     background: #ffcec3 !important;
    display: block;
    padding: 6px 8px 9px;
    width: 35px;
    height: 35px;
    margin-bottom: 20px;
    border-radius: 50%;
}
.c-tab-contents {
    background: #ffddd5;
    margin-top: 0px;
    padding: 20px;
    border-radius: 5px;
}
h3.g-title {
    color: #ed512a;
    text-align: left;
    font-size: 19px;
    border-bottom: 1px solid #ffbcad;
    padding-bottom: 7px;
}
.g-deatil {
    margin-top: 10px;
    font-size: 16px;
    text-transform: capitalize;
}
/* Style the tab */
.tab {
  overflow: hidden;
  background-color: white;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 11px 16px;
    transition: 0.3s;
    color: #5c6975;
    font-size: 12px;
    border: 1px solid grey;
    border-radius: 50px !IMPORTANT;
    margin-right: 10px
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #133a67;
  color:white;
}

/* Style the tab content */
.tabcontent {
  display: none;
     background: #ffddd5;
    margin-top: 10px;
    padding: 20px;
    border-radius: 5px;
}
    </style>
    <div class="row">
                  <div class="col-md-12">
                    <div class="wrapper-2">
                        <div class="profile-card js-profile-card">
                          <div class="profile-card__img">';
                            if($get_guides->guide_image!="" && $get_guides->guide_image!=null)
                            {
                             $html.='<img src="'.asset("assets/uploads/guide_images").'/'.$get_guides->guide_image.'" style="width:100%;">'; 
                            }
                            else
                            {
                              $html.='<img src="'.asset("assets/images/no-photo.png").'" style="width:100%">';
                            }

                          $html.='</div>
                          <div class="profile-card__cnt js-profile-cnt">
                            <div class="profile-card__name">'.ucwords($get_guides->guide_first_name).'</div>
                            <div class="profile-card__txt">Guide from <strong id="guide_country">';

                            if($get_guides->guide_country!="" && $get_guides->guide_country!=null)
                            {
                             foreach($countries as $country)
                             {
                               if($country->country_id==$get_guides->guide_country)
                               {
                                $html.=$country->country_name;
                              }     
                            }
                          }
                          else
                          {
                            $html.='No Data Available';
                          }
                          $html.='</strong>,
                          <span id="guide_city">';
                          if($get_guides->guide_city!="" && $get_guides->guide_city!=null)
                          {
                           $fetch_city=ServiceManagement::searchCities($get_guides->guide_city,$get_guides->guide_country);
                            $html.=$fetch_city['name'].'</span>';
                          }
                          else
                          {
                              $html.='No Data Available';
                          }

                          $html.='</div>
                         <p class="des" id="supplier_id"> '.$get_guides->guide_description.'
                         </p>
                            <div class="profile-card-loc">
                              <span class="profile-card-loc__icon">
                             
                              </span>
                            </div>
                      
                            <div class="profile-card-social" id="guide_language">';
                             foreach($languages_spoken as $language){
                               $html.='<a href="#" class="profile-card-social__item facebook" target="_blank">
                                <span class="icon-font">'.$language.'</span>
                              </a>';
                              }
                             
                            $html.='</div>
                           <div class="tab">
  <button class="modal_tablinks active" id="tab__guide_cancel_policy">Cancellation</button>
  <button class="modal_tablinks" id="tab__guide_terms_conditions">Terms & Condition</button>
</div>
<div class="col-md-12">
<div id="guide_cancel_policy" class="guide_cancel_policy tabcontent" style="display:block">
  <h3 class="g-title">Cancellation</h3>
  <p>'.$get_guides->guide_cancel_policy.'</p>
</div>
<div id="guide_terms_conditions" class="guide_terms_conditions tabcontent">
  <h3 class="g-title">Terms & Condition</h3>
  <p>'.$get_guides->guide_terms_conditions.'</p>
</div>
</div>
                          <div class="profile-card-message js-message">
                            <form class="profile-card-form">
                              <div class="profile-card-form__container">
                                <textarea placeholder="Say something..."></textarea>
                              </div>
                      
                              <div class="profile-card-form__bottom">
                                <button class="profile-card__button button--blue js-message-close">
                                  Send
                                </button>
                      
                                <button class="profile-card__button button--gray js-message-close">
                                  Cancel
                                </button>
                              </div>
                            </form>
                      
                            <div class="profile-card__overlay js-message-close"></div>
                          </div>
                      
                        </div>
                      
                      </div>
                   </form>
                      
                 </div> 
               </div>

               <script>
                jQuery(document).on("click",".modal_tablinks",function()
                {
                    jQuery.each(jQuery(".modal_tablinks"),function()
                    {
                      jQuery(this).removeClass("active");
                    })
                    jQuery.each(jQuery(".tabcontent"),function()
                    {
                      jQuery(this).css("display","none");
                    })

                     jQuery(this).addClass("active");

                     var id=jQuery(this).attr("id").split("__")[1];

                     jQuery("."+id).css("display","block");

                  })
               </script>';

               echo $html;


    }
     public function fetchItineraryDriversDetails(Request $request)
    {
      $driver_id=$request->get('driver_id');
    $countries=Countries::get();
    $languages=languages::get();
    $get_drivers=Drivers::where('driver_id',$driver_id)->first();

    $languages_data=explode(",",$get_drivers->driver_language);
    foreach($languages as $language)
    {
      if(in_array($language->language_id,$languages_data))
      {
        $languages_spoken[]=$language->language_name;
      }
    }
    $html='
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700&subset=latin-ext">
    <style>
.icon-2 {
  display: inline-block;
  width: 1em;
  height: 1em;
  stroke-width: 0;
  stroke: currentColor;
  fill: currentColor;
}

.wrapper-2 {
  width: 100%;
  width: 100%;
  height: auto;
  min-height: 100vh;
  padding: 0;
  padding-top: 70px;
  display: flex;
  margin-bottom: 50px;
  
}
@media screen and (max-width: 768px) {
  .wrapper-2 {
    height: auto;
    min-height: 100vh;
    padding-top: 100px;
  }
}

.profile-card {
  width: 100%;
  min-height: 460px;
  margin: auto;
  box-shadow: 0px 8px 60px -10px rgba(13, 28, 39, 0.6);
  background: #fff;
  border-radius: 12px;
  
  position: relative;
}
.profile-card.active .profile-card__cnt {
  filter: blur(6px);
}
.profile-card.active .profile-card-message,
.profile-card.active .profile-card__overlay {
  opacity: 1;
  pointer-events: auto;
  transition-delay: .1s;
}
.profile-card.active .profile-card-form {
  transform: none;
  transition-delay: .1s;
}
.profile-card__img {
  width: 150px;
  height: 150px;
  margin-left: auto;
  margin-right: auto;
  transform: translateY(-50%);
  border-radius: 50%;
  overflow: hidden;
  position: relative;
  z-index: 4;
  box-shadow: 0px 5px 50px 0px #6c44fc, 0px 0px 0px 7px rgba(107, 74, 255, 0.5);
}
@media screen and (max-width: 576px) {
  .profile-card__img {
    width: 120px;
    height: 120px;
  }
}
.profile-card__img img {
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 50%;
}
.profile-card__cnt {
  margin-top: -50px;
  text-align: center;
  padding: 0 20px;
  padding-bottom: 40px;
  transition: all .3s;
}
span.profile-card-loc__txt {
    position: absolute;
    top: 20px;
    right: 37px;
    font-size: 16px;
    color: red;
    background: #ebebeb;
    padding: 8px 15px;
    border-radius: 5px;
}
button.profile-card__button.button--orange {
    position: absolute;
    top: 65px;
    right: 21px;
}
.profile-card__name {
  font-weight: 700;
  font-size: 24px;
  color: #6944ff;
  margin-bottom: 15px;
}
.profile-card__txt {
  font-size: 18px;
  font-weight: 500;
  color: #324e63;
  margin-bottom: 15px;
}
.profile-card__txt strong {
  font-weight: 700;
}
.profile-card-loc {
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  font-weight: 600;
}
.profile-card-loc__icon {
  display: inline-flex;
  font-size: 27px;
  margin-right: 10px;
}
.profile-card-inf {
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  align-items: flex-start;
  margin-top: 0px;
}
.profile-card-inf__item {
  padding: 10px 35px;
  min-width: 150px;
}
p#supplier_id {
    margin: 0;
    text-align: center;
}
@media screen and (max-width: 768px) {
  .profile-card-inf__item {
    padding: 10px 20px;
    min-width: 120px;
  }
}
.profile-card-inf__title {
  font-weight: 700;
  font-size: 17px;
  color: #324e63;
}
.profile-card-inf__txt {
  font-weight: 500;
  margin-top: 7px;
}
.profile-card-social {
  margin-top: 0px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
}
.profile-card-social__item {
    display: inline-flex;
    width: 85px;
    height: 34px;
    margin: 15px;
    border-radius: 50px;
    align-items: center;
    justify-content: center;
    color: #fff;
    background: #405de6;
    box-shadow: 0px 7px 30px rgba(43, 98, 169, 0.5);
    position: relative;
    font-size: 21px;
    flex-shrink: 0;
    transition: all .3s;
}
.profile-card-social .icon-font {
    display: inline-flex;
    font-size: 14px;
}
@media screen and (max-width: 768px) {
  .profile-card-social__item {
    width: 50px;
    height: 50px;
    margin: 10px;
  }
}
.theme-rosegold a:hover, .theme-rosegold a:active, .theme-rosegold a:focus {
    color: #ffffff;
}
.profile-card-social__item.facebook {
  background: linear-gradient(45deg, #3b5998, #0078d7);
  box-shadow: 0px 4px 30px rgba(43, 98, 169, 0.5);
  color:white;
}
.profile-card-social__item.twitter {
  background: linear-gradient(45deg, #1da1f2, #0e71c8);
  box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.7);
}
.profile-card-social__item.instagram {
  background: linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d);
  box-shadow: 0px 4px 30px rgba(120, 64, 190, 0.6);
}
.profile-card-social__item.behance {
  background: linear-gradient(45deg, #1769ff, #213fca);
  box-shadow: 0px 4px 30px rgba(27, 86, 231, 0.7);
}
.profile-card-social__item.github {
  background: linear-gradient(45deg, #333333, #626b73);
  box-shadow: 0px 4px 30px rgba(63, 65, 67, 0.6);
}
.profile-card-social__item.codepen {
  background: linear-gradient(45deg, #324e63, #414447);
  box-shadow: 0px 4px 30px rgba(55, 75, 90, 0.6);
}
.profile-card-social__item.link {
  background: linear-gradient(45deg, #d5135a, #f05924);
  box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.6);
}
.profile-card-social .icon-font {
  display: inline-flex;
}
.profile-card-ctr {
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
}
@media screen and (max-width: 576px) {
  .profile-card-ctr {
    flex-wrap: wrap;
  }
}
.profile-card__button {
    background: none;
    border: none;
    font-family: "Quicksand", sans-serif;
    font-weight: 700;
    font-size: 18px;
    margin: 15px 35px;
    padding: 7px 31px;
    min-width: auto;
    border-radius: 50px;
    min-height: 50px;
    color: #fff;
    cursor: pointer;
    backface-visibility: hidden;
    transition: all .3s;
}
@media screen and (max-width: 768px) {
  .profile-card__button {
    min-width: 170px;
    margin: 15px 25px;
  }
}
@media screen and (max-width: 576px) {
  .profile-card__button {
    min-width: inherit;
    margin: 0;
    margin-bottom: 16px;
    width: 100%;
    max-width: 300px;
  }
  .profile-card__button:last-child {
    margin-bottom: 0;
  }
}
.profile-card__button:focus {
  outline: none !important;
}
@media screen and (min-width: 768px) {
  .profile-card__button:hover {
    transform: translateY(-5px);
  }
}
.profile-card__button:first-child {
  margin-left: 0;
}
.profile-card__button:last-child {
  margin-right: 0;
}
.profile-card__button.button--blue {
  background: linear-gradient(45deg, #1da1f2, #0e71c8);
  box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.4);
}
.profile-card__button.button--blue:hover {
  box-shadow: 0px 7px 30px rgba(19, 127, 212, 0.75);
}
.profile-card__button.button--orange {
  background: linear-gradient(45deg, #d5135a, #f05924);
  box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.35);
}
.profile-card__button.button--orange:hover {
  box-shadow: 0px 7px 30px rgba(223, 45, 70, 0.75);
}
.profile-card__button.button--gray {
  box-shadow: none;
  background: #dcdcdc;
  color: #142029;
}
.profile-card-message {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  padding-top: 130px;
  padding-bottom: 100px;
  opacity: 0;
  pointer-events: none;
  transition: all .3s;
}
.profile-card-form {
  box-shadow: 0 4px 30px rgba(15, 22, 56, 0.35);
  max-width: 80%;
  margin-left: auto;
  margin-right: auto;
  height: 100%;
  background: #fff;
  border-radius: 10px;
  padding: 35px;
  transform: scale(0.8);
  position: relative;
  z-index: 3;
  transition: all .3s;
}
@media screen and (max-width: 768px) {
  .profile-card-form {
    max-width: 90%;
    height: auto;
  }
}
@media screen and (max-width: 576px) {
  .profile-card-form {
    padding: 20px;
  }
}
.profile-card-form__bottom {
  justify-content: space-between;
  display: flex;
}
@media screen and (max-width: 576px) {
  .profile-card-form__bottom {
    flex-wrap: wrap;
  }
}
.profile-card textarea {
  width: 100%;
  resize: none;
  height: 210px;
  margin-bottom: 20px;
  border: 2px solid #dcdcdc;
  border-radius: 10px;
  padding: 15px 20px;
  color: #324e63;
  font-weight: 500;
  font-family: "Quicksand", sans-serif;
  outline: none;
  transition: all .3s;
}
.profile-card textarea:focus {
  outline: none;
  border-color: #8a979e;
}
.profile-card__overlay {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  pointer-events: none;
  opacity: 0;
  background: rgba(22, 33, 72, 0.35);
  border-radius: 12px;
  transition: all .3s;
}
.main-footer {
    background-color: rgb(254, 254, 255);
    padding: 15px 30px;
    border-top: 1px solid #cccccc;
    z-index: 999;
    position: relative;
    width: 100%;
    left: 10px !important;
    text-align: center;
    margin: 0;
}
ul.nav.nav-tabs.c-nav {
    border: none;
    margin-top: 10px;
    margin-bottom: 10px;
}

img.ul-img {
    width: 100%;
    height: 100%;
}
li.c-item {
    display: flex;
}
a.img-div {
     background: #ffcec3 !important;
    display: block;
    padding: 6px 8px 9px;
    width: 35px;
    height: 35px;
    margin-bottom: 20px;
    border-radius: 50%;
}
.c-tab-contents {
    background: #ffddd5;
    margin-top: 0px;
    padding: 20px;
    border-radius: 5px;
}
h3.g-title {
    color: #ed512a;
    text-align: left;
    font-size: 19px;
    border-bottom: 1px solid #ffbcad;
    padding-bottom: 7px;
}
.g-deatil {
    margin-top: 10px;
    font-size: 16px;
    text-transform: capitalize;
}

/* Style the tab */
.tab {
  overflow: hidden;
  background-color: white;
}

/* Style the buttons inside the tab */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 11px 16px;
    transition: 0.3s;
    color: #5c6975;
    font-size: 12px;
    border: 1px solid grey;
    border-radius: 50px !IMPORTANT;
    margin-right: 10px
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #133a67;
  color:white;
}

/* Style the tab content */
.tabcontent {
  display: none;
     background: #ffddd5;
    margin-top: 10px;
    padding: 20px;
    border-radius: 5px;
}
    </style>
    <div class="row">
                  <div class="col-md-12">
                    <div class="wrapper-2">
                        <div class="profile-card js-profile-card">
                          <div class="profile-card__img">';
                            if($get_drivers->driver_image!="" && $get_drivers->driver_image!=null)
                            {
                             $html.='<img src="'.asset("assets/uploads/driver_images").'/'.$get_drivers->driver_image.'" style="width:100%;">'; 
                            }
                            else
                            {
                              $html.='<img src="'.asset("assets/images/no-photo.png").'" style="width:100%">';
                            }

                          $html.='</div>
                          <div class="profile-card__cnt js-profile-cnt">
                            <div class="profile-card__name">'.ucwords($get_drivers->driver_first_name).'</div>
                            <div class="profile-card__txt">Driver from <strong id="driver_country">';

                            if($get_drivers->driver_country!="" && $get_drivers->driver_country!=null)
                            {
                             foreach($countries as $country)
                             {
                               if($country->country_id==$get_drivers->driver_country)
                               {
                                $html.=$country->country_name;
                              }     
                            }
                          }
                          else
                          {
                            $html.='No Data Available';
                          }
                          $html.='</strong>,
                          <span id="driver_city">';
                          if($get_drivers->driver_city!="" && $get_drivers->driver_city!=null)
                          {
                           $fetch_city=ServiceManagement::searchCities($get_drivers->driver_city,$get_drivers->driver_country);
                            $html.=$fetch_city['name'].'</span>';
                          }
                          else
                          {
                              $html.='No Data Available';
                          }

                          $html.='</div>
                         <p class="des" id="supplier_id"> '.$get_drivers->driver_description.'
                         </p>
                            <div class="profile-card-loc">
                              <span class="profile-card-loc__icon">
                             
                              </span>
                            </div>
                      
                            <div class="profile-card-social" id="guide_language">';
                             foreach($languages_spoken as $language){
                               $html.='<a href="#" class="profile-card-social__item facebook" target="_blank">
                                <span class="icon-font">'.$language.'</span>
                              </a>';
                              }
                             
                            $html.='</div>
                          <div class="tab">
  <button class="modal_tablinks active" id="tab__driver_cancel_policy">Cancellation</button>
  <button class="modal_tablinks" id="tab__driver_terms_conditions">Terms & Condition</button>
</div>
<div class="col-md-12">
<div id="driver_cancel_policy" class="driver_cancel_policy tabcontent" style="display:block">
  <h3 class="g-title">Cancellation</h3>
  <p>'.$get_drivers->driver_cancel_policy.'</p>
</div>
<div id="driver_terms_conditions" class="driver_terms_conditions tabcontent">
  <h3 class="g-title">Terms & Condition</h3>
  <p>'.$get_drivers->driver_terms_conditions.'</p>
</div>
</div>
                          <div class="profile-card-message js-message">
                            <form class="profile-card-form">
                              <div class="profile-card-form__container">
                                <textarea placeholder="Say something..."></textarea>
                              </div>
                      
                              <div class="profile-card-form__bottom">
                                <button class="profile-card__button button--blue js-message-close">
                                  Send
                                </button>
                      
                                <button class="profile-card__button button--gray js-message-close">
                                  Cancel
                                </button>
                              </div>
                            </form>
                      
                            <div class="profile-card__overlay js-message-close"></div>
                          </div>
                      
                        </div>
                      
                      </div>
                   </form>
                      
                 </div> 
               </div>

               <script>
                jQuery(document).on("click",".modal_tablinks",function()
                {
                    jQuery.each(jQuery(".modal_tablinks"),function()
                    {
                      jQuery(this).removeClass("active");
                    })
                    jQuery.each(jQuery(".tabcontent"),function()
                    {
                      jQuery(this).css("display","none");
                    })

                     jQuery(this).addClass("active");

                     var id=jQuery(this).attr("id").split("__")[1];

                     jQuery("."+id).css("display","block");

                  })
               </script>';

               echo $html;

    }

     public function itinerary_get_transfers(Request $request)
    {
    
      $country_id=$request->get('country_id');
       $city_id=$request->get('city_id');
        $transfer_type=$request->get('transfer_type');
         $currency="GEL";
        if($request->get('transfer_type')=="from-airport")
  {

    $from_airport=$request->get('from_airport');
    $to_city=$request->get('to_city');
    
    $airports=AirportMaster::where('airport_master_id',$from_airport)->first();
    $airport_name=$airports['airport_master_name'];
    $cities=Cities::where('id',$to_city)->first();
    $city_name=$cities['name'];
   
    $vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
     $vehicles=Vehicles::get();
  $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_airport)->where('transfer_details.to_city_airport',$to_city)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_details_id','desc')->get();

  $search_text='<p>Showing results for Airport Transfer from <b>"'. $airport_name.'"</b> to <b>"'.$city_name.'"</b></p>';
      $get_transfers_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "transfers"=>$fetch_transfers,
          "search_text"=>$search_text,
          "vehicle_type"=>$vehicle_type,
          "vehicles"=>$vehicles,
         "currency"=>$currency);
    }
    else if($request->get('transfer_type')=='to-airport'){
  $from_city=$request->get('from_city');
    $to_airport=$request->get('to_airport');
    
    $airports=AirportMaster::where('airport_master_id',$to_airport)->first();
    $airport_name=$airports['airport_master_name'];
    $cities=Cities::where('id',$from_city)->first();
    $city_name=$cities['name'];
   
    $vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
     $vehicles=Vehicles::get();
 $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_city)->where('transfer_details.to_city_airport',$to_airport)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_details_id','desc')->get();

  $search_text='<p>Showing results for Airport Transfer from <b>"'. $city_name.'"</b> to <b>"'.$airport_name.'"</b></p>';
      $get_transfers_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "transfers"=>$fetch_transfers,
          "search_text"=>$search_text,
          "vehicle_type"=>$vehicle_type,
          "vehicles"=>$vehicles,
         "currency"=>$currency);

       
    }
    else
    {
          $from_city=$request->get('from_city');
    $to_city=$request->get('to_city');
    
    $cities=Cities::where('id',$from_city)->first();
    $from_city_name=$cities['name'];
    $cities=Cities::where('id',$to_city)->first();
    $to_city_name=$cities['name'];
   
    $vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
     $vehicles=Vehicles::get();
   $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_city)->where('transfer_details.to_city_airport',$to_city)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_id','desc')->get();

    $search_text='<p>Showing results for City Transfer from <b>"'. $from_city_name.'"</b> to <b>"'.$to_city_name.'"</b></p>';
      $get_transfers_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "transfers"=>$fetch_transfers,
          "search_text"=>$search_text,
          "vehicle_type"=>$vehicle_type,
          "vehicles"=>$vehicles,
         "currency"=>$currency);
    }

 return json_encode($get_transfers_data);
    }
    public function itineraryBooked(Request $request)
    {
      
      $fetch_admin=Users::where('users_pid',0)->first();
      $login_customer_id=$request->get('login_customer_id');
      $login_customer_name=$request->get('login_customer_name');
      $login_customer_email=$request->get('login_customer_email');
      $customer_name=$request->get('customer_name');
      $customer_email=$request->get('customer_email');  
      $customer_phone=$request->get('customer_phone');  
      $customer_country=$request->get('customer_country');  
      $customer_address=$request->get('customer_address');  
      $customer_remarks=$request->get('customer_remarks'); 
      $itinerary_id=$request->get('booking_whole_data')['itinerary_id'];
      $booking_whole_data=serialize($request->all());
      $booking_details=$request->get('booking_whole_data')['booking_details_array'];
      
      $booking_amount=$request->get('booking_whole_data')['booking_amount'];
      $booking_amount_without_markup=$request->get('booking_whole_data')['booking_amount_without_markup'];
      $booking_from_date=$request->get('booking_whole_data')['booking_from_date'];
      $booking_to_date=$request->get('booking_whole_data')['booking_to_date'];
      $booking_adult_count=$request->get('booking_whole_data')['booking_adult_count'];
      $booking_rooms_count=$request->get('booking_whole_data')['booking_rooms_count'];
      $booking_markup_per=$request->get('booking_whole_data')['booking_markup_per'];
      $get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->where('itinerary_status',1)->first();
      if($get_itinerary)
      {

        $get_itinerary_id=BookingCustomer::latest()->value('booking_sep_id');

        $get_itinerary_id=$get_itinerary_id+1;

        $booking_currency=$request->get('booking_whole_data')['itinerary_currency'];

        $date=date("Y-m-d");

        $time=date("H:i:s");

        $insert_booking=new BookingCustomer;

        $insert_booking->booking_type="itinerary";

        $insert_booking->booking_sep_id=$get_itinerary_id;

        $insert_booking->booking_type_id=$itinerary_id;

        $insert_booking->customer_login_id=$login_customer_id;

        $insert_booking->customer_login_name=$login_customer_name;

        $insert_booking->customer_login_email=$login_customer_email;

        $insert_booking->customer_name=$customer_name;

        $insert_booking->customer_contact=$customer_phone;

        $insert_booking->customer_email=$customer_email;

        $insert_booking->customer_country=$customer_country;

        $insert_booking->customer_address=$customer_address;

        $insert_booking->booking_remarks=$customer_remarks;

        $insert_booking->booking_adult_count=$booking_adult_count;

        $insert_booking->booking_rooms_count=$booking_rooms_count;

        $insert_booking->booking_currency=$booking_currency;

        $insert_booking->booking_amount=$booking_amount;

        $insert_booking->booking_amount_without_markup=$booking_amount_without_markup;

        $insert_booking->booking_markup_per=$booking_markup_per;

        $insert_booking->booking_selected_date=$booking_from_date;

        $insert_booking->booking_selected_to_date=$booking_to_date;

        $insert_booking->booking_whole_data=$booking_whole_data;

        $insert_booking->booking_date=$date;

        $insert_booking->booking_time=$time;

        if($insert_booking->save())

        {

          for($booking_count=0;$booking_count < count($booking_details);$booking_count++)
          {

            
            $date=date("Y-m-d");

            $time=date("H:i:s");

            if(!empty($booking_details[$booking_count]['hotel']))
            {

              $get_hotels=Hotels::where('hotel_id',$booking_details[$booking_count]['hotel']['hotel_id'])->first();

              $insert_booking=new BookingCustomer;
              $insert_booking->booking_sep_id=$get_itinerary_id;
              $insert_booking->booking_type="hotel";
              $insert_booking->booking_type_id=$booking_details[$booking_count]['hotel']['hotel_id'];
              $insert_booking->customer_login_id=$login_customer_id;
              $insert_booking->customer_login_name=$login_customer_name;
              $insert_booking->customer_login_email=$login_customer_email;
              $insert_booking->booking_supplier_id=$get_hotels['supplier_id'];
              $insert_booking->customer_name=$customer_name;
              $insert_booking->customer_contact=$customer_phone;
              $insert_booking->customer_email=$customer_email;
              $insert_booking->customer_country=$customer_country;
              $insert_booking->customer_address=$customer_address;
              $insert_booking->booking_adult_count=$booking_details[$booking_count]['hotel']['room_quantity'];
              $insert_booking->booking_subject_name=$booking_details[$booking_count]['hotel']['room_name'];
              $insert_booking->booking_subject_days=$booking_details[$booking_count]['hotel']['no_of_days'];
              $insert_booking->booking_currency=$booking_currency;
              $insert_booking->booking_amount=round($booking_details[$booking_count]['hotel']['hotel_cost']*$booking_details[$booking_count]['hotel']['room_quantity']);
              $insert_booking->supplier_adult_price=round($booking_details[$booking_count]['hotel']['hotel_cost']);
              $insert_booking->customer_adult_price=round($booking_details[$booking_count]['hotel']['hotel_cost']);
              $insert_booking->booking_remarks=$customer_remarks;
              $insert_booking->booking_selected_date=$booking_details[$booking_count]['hotel']['hotel_checkin'];
              $insert_booking->booking_selected_to_date=$booking_details[$booking_count]['hotel']['hotel_checkout'];
              $insert_booking->booking_date=$date;
              $insert_booking->booking_time=$time;
              $insert_booking->save();

            }


            if(!empty($booking_details[$booking_count]['activity']))
            {

              for($activity_count=0;$activity_count <count($booking_details[$booking_count]['activity']);$activity_count++)
              {
                $get_activities=Activities::where('activity_id',$booking_details[$booking_count]['activity'][$activity_count]['activity_id'])->first();


                $insert_booking=new BookingCustomer;
                $insert_booking->booking_sep_id=$get_itinerary_id;
                $insert_booking->booking_type="activity";
                $insert_booking->booking_type_id=$booking_details[$booking_count]['activity'][$activity_count]['activity_id'];
                $insert_booking->customer_login_id=$login_customer_id;
                $insert_booking->customer_login_name=$login_customer_name;
                $insert_booking->customer_login_email=$login_customer_email;
                $insert_booking->booking_supplier_id=$get_activities['supplier_id'];
                $insert_booking->customer_name=$customer_name;
                $insert_booking->customer_contact=$customer_phone;
                $insert_booking->customer_email=$customer_email;
                $insert_booking->customer_country=$customer_country;
                $insert_booking->customer_address=$customer_address;
                $insert_booking->booking_adult_count=$booking_adult_count;
                $insert_booking->supplier_adult_price=$booking_details[$booking_count]['activity'][$activity_count]['activity_cost'];
                $insert_booking->customer_adult_price=$booking_details[$booking_count]['activity'][$activity_count]['activity_cost'];
                $insert_booking->booking_currency=$get_activities['activity_currency'];
                  $insert_booking->booking_amount=round($booking_details[$booking_count]['activity'][$activity_count]['activity_cost']*$booking_adult_count);
                $insert_booking->booking_remarks=$customer_remarks;
                $insert_booking->booking_selected_date=$booking_details[$booking_count]['dates'];
                $insert_booking->booking_date=$date;
                $insert_booking->booking_time=$time;
                $insert_booking->save();

              }



            }

            if(!empty($booking_details[$booking_count]['sightseeing']))
            {
              $get_sightseeing=SightSeeing::where('sightseeing_id',$booking_details[$booking_count]['sightseeing']['sightseeing_id'])->first();

              $supplier_adult_price=$get_sightseeing->sightseeing_adult_cost;

              $supplier_child_price=$get_sightseeing->sightseeing_child_cost;

              $sightseeing_food_cost=$get_sightseeing->sightseeing_food_cost;

              $sightseeing_hotel_cost=$get_sightseeing->sightseeing_hotel_cost;


              $other_expenses=array("food_cost"=>$sightseeing_food_cost,
                "hotel_cost"=>$sightseeing_hotel_cost);

              $insert_booking=new BookingCustomer;
              $insert_booking->booking_sep_id=$get_itinerary_id;
              $insert_booking->booking_type="sightseeing";
              $insert_booking->booking_type_id=$booking_details[$booking_count]['sightseeing']['sightseeing_id'];
              $insert_booking->customer_login_id=$login_customer_id;
              $insert_booking->customer_login_name=$login_customer_name;
              $insert_booking->customer_login_email=$login_customer_email;
              $insert_booking->customer_name=$customer_name;
              $insert_booking->customer_contact=$customer_phone;
              $insert_booking->customer_email=$customer_email;
              $insert_booking->customer_country=$customer_country;
              $insert_booking->customer_address=$customer_address;
              $insert_booking->booking_adult_count=$booking_adult_count;
              $insert_booking->supplier_adult_price=$supplier_adult_price;
              $insert_booking->customer_adult_price=$supplier_adult_price;
              $insert_booking->other_expenses=serialize($other_expenses);
              $insert_booking->booking_currency=$booking_currency;
              $insert_booking->booking_amount=round($booking_details[$booking_count]['sightseeing']['sightseeing_cost']*$booking_adult_count);
              $insert_booking->booking_remarks=$customer_remarks;
              $insert_booking->booking_selected_date=$booking_details[$booking_count]['dates'];
              $insert_booking->booking_date=$date;
              $insert_booking->booking_time=$time;
              $insert_booking->save();





            }


          }


          $result_data=array("errorCode"=>"0",
            "errorMessage"=>"",
            "result"=>"success");
          return json_encode($result_data);
        }
        else
        {
          $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
        }
      }
      else
      {
            $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
      }
    }



    public function filteredfetchTransfer(Request $request)
    {
      if($request->get('transfer_type')=="airport")
      {
      $transfer_type=$request->get('transfer_type');
      $country_id=$request->get('country_id');
      $pickup_type=$request->get('pickup_type');
      $transfer_airport=$request->get('transfer_airport');
      $transfer_airport_city=$request->get('transfer_airport_city');
      $from_airport_city="";
      $to_airport_city="";
      if($pickup_type=="from_airport")
      {
        $from_airport_city= $transfer_airport;
        $to_airport_city=$transfer_airport_city;
        $transfer_type="from-airport";
      }
      else if($pickup_type=="to_airport")
      {
        $from_airport_city= $transfer_airport_city;
        $to_airport_city=$transfer_airport;
        $transfer_type="to-airport";
      }
       $airport_date_from=$request->get('airport_date_from');
      $airport_select_time=$request->get('airport_select_time');
      $pickup_location=$request->get('transfer_airport_city_location');
      $dropoff_location=$request->get('transfer_airport_city_location');
      $airport_adults=$request->has('airport_adults') ? $request->get('airport_adults') : 0;
      $airport_child=$request->has('airport_child') ? $request->get('airport_child') : 0;
       $total_pax=$airport_adults+$airport_child;


       $airports=AirportMaster::where('airport_master_id',$transfer_airport)->first();
      $airport_name=$airports['airport_master_name'];
      $cities=Cities::where('id',$transfer_airport_city)->first();
      $city_name=$cities['name'];
      $vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
      $vehicles=Vehicles::get();


      $limit=9;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }
         $vehicle_type_array=array();
         foreach($vehicle_type as $vehicle_type_value)
         {
          $vehicle_min= $vehicle_type_value['vehicle_type_min'];
          $vehicle_max= $vehicle_type_value['vehicle_type_max'];
          if($total_pax>=$vehicle_min && $total_pax<=$vehicle_max)
          {
            $vehicle_type_array[]=$vehicle_type_value['vehicle_type_id'];
          }

        }

    



      $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->whereIn('transfers.transfer_vehicle_type',$vehicle_type_array)->where('transfer_details.from_city_airport',$from_airport_city)->where('transfer_details.to_city_airport',$to_airport_city)->where('transfers.transfer_type',$transfer_type)->where('transfer_details.transfer_vehicle_cost','>',0)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_details_id','desc')->offset($offset*$limit)->take($limit)->get();
       $transfer_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Transfer")->first();
      if($fetch_markup)
      {
        $transfer_markup=$fetch_markup['customer_markup_cost'];

      }
      if(count($fetch_transfers)>0)
      {
        $data=array();
          $fetch_transfers['markup']=  $transfer_markup;
          $fetch_transfers['vehicle_type']= $vehicle_type;
          $fetch_transfers['vehicles']= $vehicles;
           $fetch_transfers['transfer_type']= $transfer_type;
             $fetch_transfers['airport_name']= $airport_name;
             $fetch_transfers['city_name']= $city_name;
        $data=array("transfers"=>$fetch_transfers);
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }
    else
    {
       $transfer_type=$request->get('transfer_type');
  $country_id=$request->get('country_id');
  $pickup_location=$request->get('pickup_location');
  $from_city=$request->get('from_city');
  $to_city=$request->get('to_city');
  $dropoff_location=$request->get('dropoff_location');
  $outstation_date_from=$request->get('outstation_date_from');
  $outstation_select_time=$request->get('outstation_select_time');
  $outstation_adults=$request->has('outstation_adults') ? $request->get('outstation_adults') : 0;
  $outstation_child=$request->has('outstation_child') ? $request->get('outstation_child') : 0;
  $outstation_child_age=$request->get('outstation_child_age');
    $total_pax=$outstation_adults+$outstation_child;



       $cities=Cities::where('id',$from_city)->first();
  $from_city_name=$cities['name'];
  $cities=Cities::where('id',$to_city)->first();
  $to_city_name=$cities['name'];
      $vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
      $vehicles=Vehicles::get();


      $limit=9;
      if($request->has('offset'))
      {
          $offset=$request->get('offset');
      }
      else
      {
          $offset=0;
      }

      $vehicle_type_array=array();
         foreach($vehicle_type as $vehicle_type_value)
         {
          $vehicle_min= $vehicle_type_value['vehicle_type_min'];
          $vehicle_max= $vehicle_type_value['vehicle_type_max'];
          if($total_pax>=$vehicle_min && $total_pax<=$vehicle_max)
          {
            $vehicle_type_array[]=$vehicle_type_value['vehicle_type_id'];
          }

        }

    


      $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->whereIn('transfers.transfer_vehicle_type',$vehicle_type_array)->where('transfer_details.from_city_airport',$from_city)->where('transfer_details.to_city_airport',$to_city)->where('transfers.transfer_type',$transfer_type)->where('transfer_details.transfer_vehicle_cost','>',0)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_id','desc')->offset($offset*$limit)->take($limit)->get();

       $transfer_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Transfer")->first();
      if($fetch_markup)
      {
        $transfer_markup=$fetch_markup['customer_markup_cost'];

      }
      if(count($fetch_transfers)>0)
      {
          $fetch_transfers['markup']= $transfer_markup;
          $fetch_transfers['vehicle_type']= $vehicle_type;
          $fetch_transfers['vehicles']= $vehicles;
           $fetch_transfers['transfer_type']= $transfer_type;
             $fetch_transfers['from_city_name']= $from_city_name;
             $fetch_transfers['to_city_name']= $to_city_name;
        $data=array("transfers"=>$fetch_transfers);
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }

    }
    }

    public function getAirports(Request $request)
    {
       $fetch_airport=AirportMaster::where('airport_master_status',1)->get();

      if(count($fetch_airport)>0)
      {
        $data=array();
        $data=$fetch_airport;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }

    }


      public function transferDetailView(Request $request)
    {
      // print_r($request->all());
      // die();

      $transfer_id=$request->get('transfer_id');
      $transfer_type=$request->get('transfer_type');
      $country_id=$request->get('transfer_country');
      $pickup_type=$request->get('pickup_type');
      $get_vehicle_types=VehicleType::where('vehicle_type_status',1)->get();

      $get_vehicles=Vehicles::where('vehicle_status',1)->get();

      $from_city_airport=$request->get("from_city_airport");
      $to_city_airport=$request->get("to_city_airport");

      $get_transfer=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_city_airport)->where('transfer_details.to_city_airport',$to_city_airport)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->where('transfer_details.transfer_details_id',$transfer_id)->orderBy('transfer_details.transfer_id','desc')->first();
      if($get_transfer)
      {

        $countries=Countries::get();
        foreach($countries as $country)
        {
          if($country->country_id==$get_transfer->transfer_country)
          {
            $country_name=$country->country_name;
          }


        }

        $from_airport=$to_airport=$from_city=$to_city="";

        if($transfer_type=="city")
        {
      $fetch_city=ServiceManagement::searchCities($from_city_airport,$get_transfer->transfer_country);
        $from_city=$fetch_city['name'];

         $fetch_city=ServiceManagement::searchCities($to_city_airport,$get_transfer->transfer_country);
        $to_city=$fetch_city['name'];
        }
        else
        {
          if($pickup_type=="from_airport")
          {
              $airports=LoginController::searchAirports($from_city_airport);
              $from_airport=$airports['airport_master_name'];
             $fetch_city=ServiceManagement::searchCities($to_city_airport,$get_transfer->transfer_country);
            $to_city=$fetch_city['name'];


          }
          else if($pickup_type=="to_airport")
          {
        $fetch_city=ServiceManagement::searchCities($from_city_airport,$get_transfer->transfer_country);
        $from_city=$fetch_city['name'];
         $airports=LoginController::searchAirports($to_city_airport);
              $to_airport=$airports['airport_master_name'];

          }
        }
         $fetch_markup=CustomerMarkup::where('customer_markup',"Transfer")->first();
      if($fetch_markup)
      {
        $transfer_markup=$fetch_markup['customer_markup_cost'];

      }

        $transfer_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "transfer"=>$get_transfer,
          "country"=>$country_name,
          "from_airport"=>$from_airport,
          "to_airport"=>$to_airport,
          "from_city"=>$from_city,
          "to_city"=>$to_city,
          "get_vehicle_types"=>$get_vehicle_types,
           "get_vehicles"=>$get_vehicles,
          "markup"=> $transfer_markup);
        return json_encode($transfer_data);
      }
      else
      {
        $activity_data=array("errorCode"=>"1","errorMessage"=>"No data Found");
        return json_encode($activity_data);
      }

    }


    public function transferBooked(Request $request)
    {

      $fetch_admin=Users::where('users_pid',0)->first();
      $login_customer_id=$request->get('login_customer_id');
      $login_customer_name=$request->get('login_customer_name');
      $login_customer_email=$request->get('login_customer_email');
      $customer_name=$request->get('customer_name');
      $customer_email=$request->get('customer_email');  
      $customer_phone=$request->get('customer_phone');  
      $customer_country=$request->get('customer_country');  
      $customer_address=$request->get('customer_address');  
      $customer_remarks=$request->get('customer_remarks'); 
      $transfer_id=$request->get('transfer_id');
        $transfer_details_id=$request->get('transfer_details_id');
         $transfer_type=$request->get('transfer_type');
      $booking_supplier_id=$request->get('booking_supplier_id');
      $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
      $booking_amount=$request->get('booking_amount');
      $booking_date=$request->get('booking_date');
      $booking_time=$request->get('booking_time');
      $booking_adult_count=$request->get('booking_adult_count');
      $booking_child_count=$request->get('booking_child_count');
      $booking_vehicle_count=$request->get('booking_vehicle_count');
      $supplier_vehicle_price=$request->get('supplier_vehicle_price');
       $customer_vehicle_price=$request->get('customer_vehicle_price');
      $transfer_currency=$request->get('transfer_currency');
      $booking_markup_per=$request->get('booking_markup_per');
        $vehicle_type_id=$request->get('vehicle_type_id');
  $vehicle_type_name=$request->get('vehicle_type_name');
  $guide_id=$request->get('guide_id');
   $fetch_guide_id=Guides::where('guide_id',$guide_id)->first();
  $booking_guide_supplier_id=$fetch_guide_id['guide_supplier_id'];
  $guide_name=$request->get('guide_name');
  $guide_supplier_cost=$request->get('guide_supplier_cost');
  $guide_cost=$request->get('guide_cost');
  $with_guide=$request->get('with_guide');
     $fetch_transfer=Transfers::where('transfer_id',$transfer_id)->where('transfer_status',1)->first();

      if($fetch_transfer)
      {
      
        $booking_other_info=array();
  $transfer_name="";
  if($transfer_type=="city")
  {
    $booking_other_info=array("transfer_details_id"=>$transfer_details_id,
      "transfer_type"=>$transfer_type,
      "from_city"=>$request->get('from_city'),
      "to_city"=>$request->get('to_city'),
      "transfer_pickup_location"=>$request->get('pickup_location'),
      "transfer_dropoff_location"=>$request->get('dropoff_location'));

    $fetch_from_city=ServiceManagement::searchCities(session()->get('transfer_from_city'),$fetch_transfer['transfer_country']);
    $fetch_to_city=ServiceManagement::searchCities(session()->get('transfer_to_city'),$fetch_transfer['transfer_country']);
    $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];
  }
  else if($transfer_type=="from-airport")
  {
    $booking_other_info=array("transfer_details_id"=>$transfer_details_id,
      "transfer_type"=>$transfer_type,
      "from_airport"=>$request->get('from_airport'),
      "to_city"=>$request->get('to_city'),
      "transfer_dropoff_location"=>$request->get('dropoff_location'));

    $fetch_from_airport=ServiceManagement::searchAirports(session()->get('transfer_from_city_airport'));
    $fetch_to_city=ServiceManagement::searchCities(session()->get('transfer_to_city_airport'),$fetch_transfer['transfer_country']);
    $transfer_name="From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];

  }
  else
  {
    $booking_other_info=array("transfer_details_id"=>$transfer_details_id,
      "transfer_type"=>$transfer_type,
      "from_city"=>$request->get('from_city'),
      "to_airport"=>$request->get('to_airport'),
      "transfer_pickup_location"=>$request->get('pickup_location'));

    $fetch_from_city=ServiceManagement::searchCities(session()->get('transfer_from_city_airport'),$fetch_transfer['transfer_country']);
    $fetch_to_airport=ServiceManagement::searchAirports(session()->get('transfer_to_city_airport'));
    $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];
  }


   if($transfer_type!="city")
  {
    $transfer_supplier_amount=0;

     if($with_guide==1)
     {
       $booking_other_info["vehicle_type_id"]=$vehicle_type_id;
       $booking_other_info["vehicle_type_name"]=$vehicle_type_name;

      $booking_other_info["guide_id"]=$guide_id;
       $booking_other_info["guide_name"]=$guide_name;

      $booking_other_info["guide_supplier_cost"]=$guide_supplier_cost;
       $booking_other_info["guide_cost"]=$guide_cost;


      $transfer_supplier_amount+=$supplier_vehicle_price;
      $transfer_supplier_amount+=$guide_supplier_cost;
     }
     else

     {
      $transfer_supplier_amount=$supplier_vehicle_price;
     }


  }
  else

     {
      $transfer_supplier_amount=$supplier_vehicle_price;
     }
      

        $booking_other_info=serialize($booking_other_info);
           $booking_whole_data=serialize($request->all());
  $booking_currency=$request->get('transfer_currency');
  $date=date("Y-m-d");
  $time=date("H:i:s");
   $get_latest_id=BookingCustomer::latest()->value('booking_sep_id');
             $get_latest_id=$get_latest_id+1;
        $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="transfer";
  $insert_booking->booking_type_id=$transfer_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
         $insert_booking->booking_adult_count=$booking_adult_count;
        $insert_booking->booking_child_count=$booking_child_count;
         $insert_booking->booking_vehicle_count=$booking_vehicle_count;
      $insert_booking->supplier_vehicle_price=$supplier_vehicle_price;
    $insert_booking->customer_vehicle_price=$customer_vehicle_price;
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_supplier_amount=$transfer_supplier_amount;
        $insert_booking->booking_amount=$booking_amount;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$booking_date;
         $insert_booking->booking_selected_time=$booking_time;
         $insert_booking->booking_other_info=$booking_other_info;
         $insert_booking->booking_whole_data=$booking_whole_data;
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;

        if($insert_booking->save())
        {
          if($with_guide==1)
     {

          $sightseeing_inserted_id=$insert_booking->id;
          $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="guide";
        $insert_booking->booking_type_id=$guide_id;
         $insert_booking->booking_sightseeing_id=$sightseeing_inserted_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
        $insert_booking->booking_adult_count=1;
          $insert_booking->booking_subject_days=1;
        $insert_booking->supplier_adult_price=$guide_supplier_cost;
        $insert_booking->customer_adult_price=$guide_cost;
        $insert_booking->booking_currency=$booking_currency;
         $insert_booking->booking_supplier_amount=$guide_supplier_cost;
        $insert_booking->booking_amount=$guide_cost;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$booking_date;
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;
        $insert_booking->save();

     }
          $booking_id=$get_latest_id;
          $htmldata='<p>Dear '.$customer_name.',</p><p>You have successfully booked transfer on traveldoor[dot]ge . Your booking id is '.$booking_id.'.</p>
          ';
          $data = array(
            'name' => $customer_name,
            'email' =>$customer_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
          });

          $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new transfer booking on traveldoor[dot]ge with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
          $data_supplier= array(
            'name' => $fetch_supplier->supplier_name,
            'email' =>$fetch_supplier->company_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW TRANSFER BOOKING');
          });

          $htmldata='<p>Dear Admin,</p><p>New transfer has been booked with booking id '.$booking_id.' by customer '.$customer_name.' for the supplier '.$fetch_supplier->supplier_name.' </p>';
          $data_admin= array(
            'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
            'email' =>$fetch_admin->users_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_admin['email'], $data_admin['name'])->subject('NEW TRANSFER BOOKING ON PORTAL');
          });

          $result_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "result"=>"success");
          return json_encode($result_data);
        }
        else
        {
            $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
        }

      }

    }


     public function fetchGuidesTransfer(Request $request)
    {
      $languages=languages::get();
      $country_id=$request->get('country_id');
      $date_from=$request->get('transfer_date');
      $date_to=$request->get('transfer_date');
      $markup=$request->get('markup');


      $airport_id=$request->get('airport_id');
      $city_id=$request->get('city_id');
      $vehicle_selected=$request->get('vehicle_type_id');
      $get_airport_detail=AirportMaster::where('airport_master_id',$airport_id)->first();
     if(!empty($get_airport_detail))
     {
      $airport_city=$get_airport_detail->airport_master_city;
     }
     else
     {
      $airport_city="";
     }

       $fetch_guides=Guides::where('guide_country',$country_id)->where('guide_status',1)->orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','desc')->get();

      $check_guide_count=0;
      $guides_array=array();
      foreach($fetch_guides as $guides)
      {
        $guide_cost_vehicle_selected=0;
        $get_tour_cost=unserialize($guides->guide_airports_cost);
        if($get_tour_cost=="" || $get_tour_cost==null)
      {
        continue;
        }
        foreach($get_tour_cost as $tour_cost)
        {
          if($tour_cost['airport_name']==$airport_id)
        {

            $cost_count=0;
           foreach($tour_cost['airport_vehicle_name'] as $vehicle_names)
          {
              if($vehicle_names==$vehicle_selected)
              {

                if(!empty($tour_cost['airport_guide_inside_cost']))
              {
                  if($city_id==$airport_city)
              {
                 $guide_cost_vehicle_selected=$tour_cost['airport_guide_inside_cost'][$cost_count];
              }
              else
              {
                  $guide_cost_vehicle_selected=$tour_cost['airport_guide_outside_cost'][$cost_count];
              }
             
              }
              else
              {
                continue;
                
              }

               $guide_markup_cost=round(($guide_cost_vehicle_selected*$markup)/100);
            $total_customer_cost=round(($guide_cost_vehicle_selected+$guide_markup_cost));

                $operating_days=unserialize($guides->operating_weekdays);

                $operating_days_array=array();

                foreach($operating_days as $key=>$days)

                {

                  if($days=="Yes")

                  {

                    $operating_days_array[]= ucwords($key);

                  }



                }

                $check_blackout_during_dates=0;



                $get_dates_from=strtotime($date_from);

                $get_dates_to=strtotime($date_to);



                $blackout_days=explode(',',$guides->guide_blackout_dates);



                $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;

                $dates="";

                // for($count=0;$count<=$date_diff;$count++)

                // {

                //  $calculated_date=date("Y-m-d",strtotime("+$count day",strtotime($date_from)));



                //  $calculated_day=date("l",strtotime("+$count day",strtotime($date_from)));

                //  if(in_array($calculated_date, $blackout_days) || !in_array($calculated_day, $operating_days_array))

                //  {

                //    $check_blackout_during_dates++;



                //  }





                // }



                

                if($check_blackout_during_dates<=0)

                {
                  if(($guide_cost_vehicle_selected!="" && $guide_cost_vehicle_selected>0))
                        {
                  $guides_array[$check_guide_count]["guide_id"]=$guides->guide_id;
                  $guides_array[$check_guide_count]["guide_first_name"]=$guides->guide_first_name;
                  $guides_array[$check_guide_count]["guide_last_name"]=$guides->guide_last_name;
                  $guides_array[$check_guide_count]["guide_supplier_cost"]=$guide_cost_vehicle_selected;
                  $guides_array[$check_guide_count]["guide_customer_cost"]=$total_customer_cost;
                  $guides_array[$check_guide_count]["guide_description"]=$guides->guide_description;
                  $guides_array[$check_guide_count]["guide_image"]=$guides->guide_image;
                  $guides_array[$check_guide_count]["guide_language"]=$guides->guide_language;
                  $check_guide_count++;

                }

                }





              }
              $cost_count++;
            }
            
          }
        }
      }
  if(count($fetch_guides)>0)
      {
        $data=array();
        $fetch_guides=$guides_array;
        $data['error']="0";
        $data['error_code']="";
        $data['guides']=$fetch_guides;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }


    }


    public function filteredfetchRestaurants(Request $request)
    {

      $country_id=$request->get('country_id');
      $city_id=$request->get('city_id');
      $select_date=$request->get('select_date');
       $select_time=$request->get('select_time');
        $restaurant_type=$request->get('restaurant_type');
    // $date_to=$request->get('date_to');
      $limit=9;
    if($request->has('offset'))
    {
        $offset=$request->get('offset');
    }
    else
    {
        $offset=0;
    }
$selected_time=date("H:i:s", strtotime($select_time));

$restaurant_select_date=$select_date;

    $fetch_restaurant=Restaurants::where('restaurant_country',$country_id)->where('restaurant_city',$city_id)->where('validity_fromdate','<=',$select_date)->where('validity_todate','>=',$select_date);
    if($restaurant_type!="0")
    {
      $fetch_restaurant=$fetch_restaurant->where('restaurant_type',$restaurant_type);
    }
    $fetch_restaurant=$fetch_restaurant->where('restaurant_status',1)->orderBy('restaurant_popular_status','desc')->orderBy(\DB::raw('-`restaurant_show_order`'), 'desc')->orderBy('restaurant_id','desc')->offset($offset*$limit)->take($limit)->get();


       $restaurant_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Restaurant")->first();
      if($fetch_markup)
      {
        $restaurant_markup=$fetch_markup['customer_markup_cost'];

      }
      if(count($fetch_restaurant)>0)
      {
        $data=array();
          $fetch_restaurant['markup']= $restaurant_markup;
        $data=$fetch_restaurant;
        return json_encode($data);
      }
      else
      {
        $data=array();
        return json_encode($data);
      }
    }


    public function restaurantDetailView(Request $request)
    {
       $restaurant_id=$request->get('restaurant_id');

      $get_restaurant=Restaurants::where('restaurant_id', $restaurant_id)->first();
      if($get_restaurant)
      {
         $get_menu_categories=RestaurantMenuCategory::where('restaurant_menu_category_status',1)->get();
          $get_all_foods=RestaurantFood::where('food_status',1)->where('food_approval_status',1)->where('restaurant_id_fk',$get_restaurant->restaurant_id)->get();
        $countries=Countries::get();
        foreach($countries as $country)
        {
          if($country->country_id==$get_restaurant->restaurant_country)
          {
            $country_name=$country->country_name;
          }


        }


        $fetch_city=ServiceManagement::searchCities($get_restaurant->restaurant_city,$get_restaurant->restaurant_country);
        $city_name=$fetch_city['name'];

         $restaurant_markup=0;

      $fetch_markup=CustomerMarkup::where('customer_markup',"Restaurant")->first();
      if($fetch_markup)
      {
        $restaurant_markup=$fetch_markup['customer_markup_cost'];

      }

        $restaurant_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "restaurant"=>$get_restaurant,
          "get_menu_categories"=>$get_menu_categories,
          "get_all_foods"=>$get_all_foods,
          "country"=>$country_name,
          "city"=>$city_name,
          "markup"=>$restaurant_markup);
        return json_encode($restaurant_data);
      }
      else
      {
        $activity_data=array("errorCode"=>"1","errorMessage"=>"No data Found");
        return json_encode($activity_data);
      }
    }



    public function restaurantBooked(Request $request)
    {
         $fetch_admin=Users::where('users_pid',0)->first();
      $login_customer_id=$request->get('login_customer_id');
      $login_customer_name=$request->get('login_customer_name');
      $login_customer_email=$request->get('login_customer_email');
      $customer_name=$request->get('customer_name');
      $customer_email=$request->get('customer_email');  
      $customer_phone=$request->get('customer_phone');  
      $customer_country=$request->get('customer_country');  
      $customer_address=$request->get('customer_address');  
      $customer_remarks=$request->get('customer_remarks'); 
      $restaurant_id=$request->get('restaurant_id');
      $booking_supplier_id=$request->get('booking_supplier_id');
      $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
      $booking_amount=$request->get('booking_amount');
      $selected_date=$request->get('selected_date');
      $selected_time=$request->get('selected_time');
      $no_of_persons=$request->get('no_of_persons');
      $booking_supplier_amount=$request->get('booking_supplier_amount');
      $food_details=$request->get('food_details');
      $booking_markup_per=$request->get('booking_markup_per');
      $selected_food=$request->get('selected_food');
      
    $get_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->where('restaurant_status',1)->first();
    if($get_restaurant)
    {
      
        $booking_currency=$request->get('restaurant_currency');
        $date=date("Y-m-d");
        $time=date("H:i:s");
        $get_latest_id=BookingCustomer::latest()->value('booking_sep_id');
             $get_latest_id=$get_latest_id+1;
        $insert_booking=new BookingCustomer;
        $insert_booking->booking_sep_id=$get_latest_id;
        $insert_booking->booking_type="restaurant";
        $insert_booking->booking_type_id=$restaurant_id;
        $insert_booking->customer_login_id=$login_customer_id;
        $insert_booking->customer_login_name=$login_customer_name;
        $insert_booking->customer_login_email=$login_customer_email;
        $insert_booking->booking_supplier_id=$booking_supplier_id;
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address;
        $insert_booking->booking_adult_count=$no_of_persons;
        $insert_booking->booking_food_details=$selected_food;
        $insert_booking->booking_subject_name=$get_restaurant->restaurant_name;
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_supplier_amount=$booking_supplier_amount;
        $insert_booking->booking_amount=$booking_amount;
        $insert_booking->booking_markup_per=$booking_markup_per;
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_selected_date=$selected_date;
        $insert_booking->booking_selected_time=$selected_time;
       $insert_booking->booking_other_info=serialize($food_details);
        $insert_booking->booking_whole_data=serialize($request->all());
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;

        if($insert_booking->save())
        {
          $booking_id=$get_latest_id;
          $htmldata='<p>Dear '.$customer_name.',</p><p>You have successfully booked restaurant on traveldoor[dot]ge . Your booking id is '.$booking_id.'.</p>
          ';
          $data = array(
            'name' => $customer_name,
            'email' =>$customer_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
          });

          $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new restaurant booking on traveldoor[dot]ge with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
          $data_supplier= array(
            'name' => $fetch_supplier->supplier_name,
            'email' =>$fetch_supplier->company_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW RESTAURANT BOOKING');
          });

          $htmldata='<p>Dear Admin,</p><p>New restaurant has been booked with booking id '.$booking_id.' by customer '.$customer_name.' for the supplier '.$fetch_supplier->supplier_name.' </p>';
          $data_admin= array(
            'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
            'email' =>$fetch_admin->users_email
          );
          Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
            $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
            $m->to($data_admin['email'], $data_admin['name'])->subject('NEW RESTAURANT BOOKING ON PORTAL');
          });



          $result_data=array("errorCode"=>"0",
          "errorMessage"=>"",
          "result"=>"success");
          return json_encode($result_data);
        }
        else
        {
            $result_data=array("errorCode"=>"1","errorMessage"=>"Booking Failed",
          "result"=>"fail");
          return json_encode($result_data);
        }

      }

    }
  }
