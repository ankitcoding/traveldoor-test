<?php

namespace App\Http\Controllers;

use App\Activities;
use App\AirportMaster;
use App\Countries;
use App\Cities;
use App\Hotels;
use App\HotelRooms;
use App\HotelRoomSeasons;
use App\HotelRoomSeasonOccupancy;
use App\SavedItinerary;
use App\SavedItineraryLog;
use App\SightSeeing;
use App\States;
use App\Suppliers;
use App\Transfers;
use App\TransferDetails;
use App\Users;
use App\UserRights;
use App\Vehicles;
use App\VehicleType;
use App\Bookings;

use Illuminate\Http\Request;

class ItinerariesController extends Controller
{ private $currencyApiKey;
  private $base_currency;
  public function __construct()
  {
    date_default_timezone_set('Asia/Dubai');
    $this->currencyApiKey="f4f2d4f26341429dcf7e";
     $this->base_currency="GEL";
  }
	 private function rights($menu)
    {
        $emp_id=session()->get('travel_users_id');
        $right_array=array();
        $employees=Users::where('users_id',$emp_id)->where('users_pid',0)->where('users_status',1)->first();
        if(!empty($employees))
        {
            $right_array['add']=1;
            $right_array['view']=1;
            $right_array['edit_delete']=1;
            $right_array['report']=1;
            $right_array['admin']=1;
            $right_array['admin_which']="add,view,edit_delete,report";
        }
        else
        {

            $employees=Users::where('users_id',$emp_id)->where('users_status',1)->first();
            if(!empty($employees))
            {
                $user_rights=UserRights::where('emp_id',$emp_id)->where('menu',$menu)->first();
                if(!empty($user_rights))
                {
                    $right_array['add']=$user_rights->add_status;
                    $right_array['view']=$user_rights->view_status;
                    $right_array['edit_delete']=$user_rights->edit_del_status;
                    $right_array['report']=$user_rights->report_status;
                    $right_array['admin']=$user_rights->admin_status;
                    if($user_rights->admin_which_status!="")
                        $right_array['admin_which']=$user_rights->admin_which_status;
                    else
                        $right_array['admin_which']="No";
                }
                else
                {
                    $right_array['add']=0;
                    $right_array['view']=0;
                    $right_array['edit_delete']=0;
                    $right_array['report']=0;
                    $right_array['admin']=0;
                    $right_array['admin_which']="No";
                }
            }
            else
            {
                $right_array['add']=0;
                $right_array['view']=0;
                $right_array['edit_delete']=0;
                $right_array['report']=0;
                $right_array['admin']=0;
                $right_array['admin_which']="No";
            }

        }

        return $right_array;

    }
	public function saved_itinerary(Request $request)
	{
		if(session()->has('travel_users_id'))
		{
			$rights=$this->rights('saved-itinerary');
			$countries=Countries::get();
			$cities=Cities::get();
			$emp_id=session()->get('travel_users_id');
			 if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
            {
               $get_itinerary=SavedItinerary::orderBy(\DB::raw('-`itinerary_show_order`'), 'desc')->orderBy('itinerary_id','asc')->get();
            }
            else
            {

            	$get_itinerary=SavedItinerary::where('itinerary_created_by',$emp_id)->orderBy(\DB::raw('-`itinerary_show_order`'), 'desc')->orderBy('itinerary_id','asc')->get();
            }
   			
			return view('mains.saved-itinerary-new')->with(compact('countries','cities','get_itinerary','rights'));
		}
		else
		{
			return redirect()->route('index');
		}

	}
	public function create_itinerary(Request $request)
	{
		if(session()->has('travel_users_id'))
		{
			 $rights=$this->rights('saved-itinerary');
			$countries=Countries::where('country_status',1)->get();
			$cities=Cities::get();
			$fetch_airports=AirportMaster::get();
			$suppliers=Suppliers::where('supplier_status',1)->get();
			 $get_vehicles=VehicleType::get();
			return view('mains.create-itinerary-new')->with(compact('countries','cities','fetch_airports','suppliers','get_vehicles','rights'));
		}
		else
		{
			return redirect()->route('index');
		}

	}

	public function insert_itinerary(Request $request)
	{
		// echo "<pre>";
		// print_r($request->all());

		if(session()->get('travel_users_role')=="Admin")
		{
			$user_role='Admin';
		}
		else
		{
			$user_role='Sub-User';
		}
		$user_id=session()->has('travel_users_id');
		$itinerary_tour_name=$request->get('tour_name');
		$itinerary_tour_description=$request->get('tour_description');
		$itinerary_occupancy=$request->get('occupancy');
		$itinerary_adult=$request->get('adults');
		$itinerary_child_with_bed=$request->get('child_with_bed');
		$itinerary_child_with_no_bed=$request->get('child_with_no_bed');
		$itinerary_country=$request->get('country');
		$itinerary_tour_fromdate=$request->get('tour_fromdate');
		$itinerary_tour_todate=$request->get('tour_todate');

		$itinerary_dates=$request->get('days_date');
		$itinerary_package_dates=array();
		for($dates_count=0;$dates_count<count($itinerary_dates);$dates_count++)
		{
			$itinerary_package_dates[$dates_count]=$itinerary_dates[$dates_count];
		}
		$itinerary_package_dates=serialize($itinerary_package_dates);

		$itinerary_countries=$request->get('days_country');
		$itinerary_package_countries=array();
		for($country_count=0;$country_count<count($itinerary_countries);$country_count++)
		{
			$itinerary_package_countries[$country_count]=$itinerary_countries[$country_count];
		}
		$itinerary_package_countries=serialize($itinerary_package_countries);

		$itinerary_cities=$request->get('days_city');
		$itinerary_package_cities=array();
		for($city_count=0;$city_count<count($itinerary_cities);$city_count++)
		{
			$itinerary_package_cities[$city_count]=$itinerary_cities[$city_count];
		}
		$itinerary_package_cities=serialize($itinerary_package_cities);

		$itinerary_suppliers=$request->get('supplier_name');
		$itinerary_package_suppliers=array();
		for($supplier_count=0;$supplier_count<count($itinerary_suppliers);$supplier_count++)
		{
			$itinerary_package_suppliers[$supplier_count]=$itinerary_suppliers[$supplier_count];
		}
		$itinerary_package_suppliers=serialize($itinerary_package_suppliers);

		$itinerary_service_type=$request->get('service_type');
		$itinerary_package_service_type=array();
		for($type_count=0;$type_count<count($itinerary_service_type);$type_count++)
		{
			$itinerary_package_service_type[$type_count]=$itinerary_service_type[$type_count];
		}
		$itinerary_package_service_type=serialize($itinerary_package_service_type);

		$itinerary_services=$request->get('days_services');
		$itinerary_package_services=array();
		for($service_count=0;$service_count<count($itinerary_services);$service_count++)
		{
			$itinerary_package_services[$service_count]=$itinerary_services[$service_count];
		}
		$itinerary_package_services=serialize($itinerary_package_services);


		$insert_itinerary=new SavedItinerary;
		$insert_itinerary->itinerary_tour_name=$itinerary_tour_name;
		$insert_itinerary->itinerary_tour_description=$itinerary_tour_description;
		$insert_itinerary->itinerary_occupancy=$itinerary_occupancy;
		$insert_itinerary->itinerary_adult=$itinerary_adult;
		$insert_itinerary->itinerary_child_with_bed=$itinerary_child_with_bed;
		$insert_itinerary->itinerary_child_with_no_bed=$itinerary_child_with_no_bed;
		$insert_itinerary->itinerary_country=$itinerary_country;
		$insert_itinerary->itinerary_tour_fromdate=$itinerary_tour_fromdate;
		$insert_itinerary->itinerary_tour_todate=$itinerary_tour_todate;
		$insert_itinerary->itinerary_package_dates=$itinerary_package_dates;
		$insert_itinerary->itinerary_package_countries=$itinerary_package_countries;
		$insert_itinerary->itinerary_package_cities=$itinerary_package_cities;
		$insert_itinerary->itinerary_package_suppliers=$itinerary_package_suppliers;
		$insert_itinerary->itinerary_package_service_type=$itinerary_package_service_type;
		$insert_itinerary->itinerary_package_services=$itinerary_package_services;
		$insert_itinerary->itinerary_created_by=$user_id;
		$insert_itinerary->itinerary_created_role=$user_role;

		if($insert_itinerary->save())
		{
		$last_itinerary_id=$insert_itinerary->id;
		$insert_itinerary_log=new SavedItineraryLog;
		$insert_itinerary_log->itinerary_id=$last_itinerary_id;
		$insert_itinerary_log->itinerary_tour_name=$itinerary_tour_name;
		$insert_itinerary_log->itinerary_tour_description=$itinerary_tour_description;
		$insert_itinerary_log->itinerary_occupancy=$itinerary_occupancy;
		$insert_itinerary_log->itinerary_adult=$itinerary_adult;
		$insert_itinerary_log->itinerary_child_with_bed=$itinerary_child_with_bed;
		$insert_itinerary_log->itinerary_child_with_no_bed=$itinerary_child_with_no_bed;
		$insert_itinerary_log->itinerary_country=$itinerary_country;
		$insert_itinerary_log->itinerary_tour_fromdate=$itinerary_tour_fromdate;
		$insert_itinerary_log->itinerary_tour_todate=$itinerary_tour_todate;
		$insert_itinerary_log->itinerary_package_dates=$itinerary_package_dates;
		$insert_itinerary_log->itinerary_package_countries=$itinerary_package_countries;
		$insert_itinerary_log->itinerary_package_cities=$itinerary_package_cities;
		$insert_itinerary_log->itinerary_package_suppliers=$itinerary_package_suppliers;
		$insert_itinerary_log->itinerary_package_service_type=$itinerary_package_service_type;
		$insert_itinerary_log->itinerary_package_services=$itinerary_package_services;
		$insert_itinerary_log->itinerary_created_by=$user_id;
		$insert_itinerary_log->itinerary_created_role=$user_role;
		$insert_itinerary_log->itinerary_operation_performed="INSERT";
		$insert_itinerary_log->save();
			echo "success";
		}
		else
		{
			echo "fail";
		}
	}


	public function insert_itinerary_new(Request $request)
	{

		if(session()->get('travel_users_role')=="Admin")
		{
			$user_role='Admin';
		}
		else
		{
			$user_role='Sub-User';
		}
		$user_id=session()->has('travel_users_id');
		$itinerary_tour_name=$request->get('tour_name');
		$itinerary_tour_description=$request->get('tour_description');
		$itinerary_tour_days=$request->get('no_of_days');
		$itinerary_total_cost=$request->get('total_cost');
		$itinerary_validity_operation_from=$request->get('validity_operation_from');
		$itinerary_validity_operation_to=$request->get('validity_operation_to');
		$itinerary_exclusions=$request->get('tour_exclusions');
		$itinerary_terms_and_conditions=$request->get('tour_terms_and_conditions');
		$itinerary_cancellation=$request->get('tour_cancellation');
		$days_number=$request->get('days_number');
		$days_title=$request->get('days_title');
		$days_description=json_decode($request->get('days_description'));
		$hotel_id=$request->get('hotel_id');
		$hotel_name=$request->get('hotel_name');
		$room_id=$request->get('room_id');
		$room_name=$request->get('room_name');
		$room_occupancy_id=$request->get('room_occupancy_id');
		$hotel_cost=$request->get('hotel_cost');
		$hotel_no_of_days=$request->get('hotel_no_of_days');
		$activity_id=$request->get('activity_id');
		$activity_name=$request->get('activity_name');
		$activity_cost=$request->get('activity_cost');

		$sightseeing_id=$request->get('sightseeing_id');
		$sightseeing_name=$request->get('sightseeing_name');
		$sightseeing_tour_type=$request->get('sightseeing_tour_type');
		$sightseeing_vehicle_type=$request->get('sightseeing_vehicle_type');
		$sightseeing_guide_id=$request->get('sightseeing_guide_id');
		$sightseeing_guide_name=$request->get('sightseeing_guide_name');
		$sightseeing_guide_cost=$request->get('sightseeing_guide_cost');
		$sightseeing_driver_id=$request->get('sightseeing_driver_id');
		$sightseeing_driver_name=$request->get('sightseeing_driver_name');
		$sightseeing_driver_cost=$request->get('sightseeing_driver_cost');
		$sightseeing_adult_cost=$request->get('sightseeing_adult_cost');
		$sightseeing_additional_cost=$request->get('sightseeing_additional_cost');
		$sightseeing_cost=$request->get('sightseeing_cost');

		$transfer_id=$request->get('transfer_id');
		$transfer_name=$request->get('transfer_name');
		$transfer_type=$request->get('transfer_type');
		$transfer_from_city=$request->get('transfer_from_city');
		$transfer_to_city=$request->get('transfer_to_city');
		$transfer_from_airport=$request->get('transfer_from_airport');
		$transfer_to_airport=$request->get('transfer_to_airport');
		$transfer_pickup=$request->get('transfer_pickup');
		$transfer_dropoff=$request->get('transfer_dropoff');
		$transfer_vehicle_type=$request->get('transfer_vehicle_type');
		$transfer_guide_id=$request->get('transfer_guide_id');
		$transfer_guide_name=$request->get('transfer_guide_name');
		$transfer_guide_cost=$request->get('transfer_guide_cost');
		$transfer_cost=$request->get('transfer_cost');
		$transfer_total_cost=$request->get('transfer_total_cost');


		$itinerary_countries=$request->get('days_country');
		$itinerary_package_countries_array=array();
		for($country_count=0;$country_count<count($itinerary_countries);$country_count++)
		{
			$itinerary_package_countries_array[$country_count]=$itinerary_countries[$country_count];
		}
		$itinerary_package_countries=implode(",",$itinerary_package_countries_array);

		$itinerary_cities=$request->get('days_city');
		$itinerary_package_cities_array=array();
		for($city_count=0;$city_count<count($itinerary_cities);$city_count++)
		{
			$itinerary_package_cities_array[$city_count]=$itinerary_cities[$city_count];
		}
		$itinerary_package_cities=implode(",",$itinerary_package_cities_array);

		$days_title_array=array();
		for($title_count=0;$title_count<count($days_title);$title_count++)
		{
			$days_title_array[$title_count]=$days_title[$title_count];
		}
		$itinerary_package_title=serialize($days_title_array);

		$days_description_array=array();
		for($desc_count=0;$desc_count<count($days_description);$desc_count++)
		{
			$days_description_array[$desc_count]=$days_description[$desc_count];
		}
		$itinerary_package_description=serialize($days_description_array);

		$services_array=array();
		for($services_count=0;$services_count<=count($days_number)-1;$services_count++)
		{
			if($services_count<count($days_number)-1)
			{
			$services_array[$services_count]["hotel"]["hotel_id"]=$hotel_id[$services_count];
			$services_array[$services_count]["hotel"]["hotel_name"]=$hotel_name[$services_count];
			$services_array[$services_count]["hotel"]["room_id"]=$room_id[$services_count];
			$services_array[$services_count]["hotel"]["room_name"]=$room_name[$services_count];
			$services_array[$services_count]["hotel"]["room_occupancy_id"]=$room_occupancy_id[$services_count];
			$services_array[$services_count]["hotel"]["hotel_cost"]=$hotel_cost[$services_count];
			$services_array[$services_count]["hotel"]["hotel_no_of_days"]=$hotel_no_of_days[$services_count];
			}
			


			$activity_id_array=explode("///",$activity_id[$services_count]);
			$activity_name_array=explode("///",$activity_name[$services_count]);
			$activity_cost_array=explode("///",$activity_cost[$services_count]);


			$services_array[$services_count]["activity"]["activity_id"]=$activity_id_array;
			$services_array[$services_count]["activity"]["activity_name"]=$activity_name_array;
			$services_array[$services_count]["activity"]["activity_cost"]=$activity_cost_array;

			$services_array[$services_count]["sightseeing"]["sightseeing_id"]=$sightseeing_id[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_name"]=$sightseeing_name[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_tour_type"]=$sightseeing_tour_type[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_vehicle_type"]=$sightseeing_vehicle_type[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_guide_id"]=$sightseeing_guide_id[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_guide_name"]=$sightseeing_guide_name[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_guide_cost"]=$sightseeing_guide_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_driver_id"]=$sightseeing_driver_id[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_driver_name"]=$sightseeing_driver_name[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_driver_cost"]=$sightseeing_driver_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_adult_cost"]=$sightseeing_adult_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_additional_cost"]=$sightseeing_additional_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_cost"]=$sightseeing_cost[$services_count];
			
			$services_array[$services_count]["transfer"]["transfer_id"]=$transfer_id[$services_count];
			$services_array[$services_count]["transfer"]["transfer_name"]=$transfer_name[$services_count];
			$services_array[$services_count]["transfer"]["transfer_type"]=$transfer_type[$services_count];
			$services_array[$services_count]["transfer"]["transfer_from_city"]=$transfer_from_city[$services_count];
			$services_array[$services_count]["transfer"]["transfer_to_city"]=$transfer_to_city[$services_count];
			$services_array[$services_count]["transfer"]["transfer_from_airport"]=$transfer_from_airport[$services_count];
			$services_array[$services_count]["transfer"]["transfer_to_airport"]=$transfer_to_airport[$services_count];
			$services_array[$services_count]["transfer"]["transfer_pickup"]=$transfer_pickup[$services_count];
			$services_array[$services_count]["transfer"]["transfer_dropoff"]=$transfer_dropoff[$services_count];
			$services_array[$services_count]["transfer"]["transfer_vehicle_type"]=$transfer_vehicle_type[$services_count];
			$services_array[$services_count]["transfer"]["transfer_guide_id"]=$transfer_guide_id[$services_count];
			$services_array[$services_count]["transfer"]["transfer_guide_name"]=$transfer_guide_name[$services_count];
			$services_array[$services_count]["transfer"]["transfer_guide_cost"]=$transfer_guide_cost[$services_count];
			$services_array[$services_count]["transfer"]["transfer_cost"]=$transfer_cost[$services_count];
			$services_array[$services_count]["transfer"]["transfer_total_cost"]=$transfer_total_cost[$services_count];
		}	

		$itinerary_package_services=serialize($services_array);

		$itinerary_images=array();
        //multifile uploading
		if($request->hasFile('upload_ativity_images'))
		{
			foreach($request->file('upload_ativity_images') as $file)
			{
				$extension=strtolower($file->getClientOriginalExtension());
				if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
				{
					$image_name=$file->getClientOriginalName();
					$image_itinerary = "itinerary-".time()."-".$image_name;
					$dir1 = 'assets/uploads/itinerary_images/';
					$file->move($dir1, $image_itinerary);
					$itinerary_images[]=$image_itinerary;
				}
			}
		}
		$itinerary_images=serialize($itinerary_images);



		$insert_itinerary=new SavedItinerary;
		$insert_itinerary->itinerary_tour_name=$itinerary_tour_name;
		$insert_itinerary->itinerary_tour_description=$itinerary_tour_description;
		$insert_itinerary->itinerary_tour_days=$itinerary_tour_days;
		$insert_itinerary->itinerary_package_countries=$itinerary_package_countries;
		$insert_itinerary->itinerary_package_title=$itinerary_package_title;
		$insert_itinerary->itinerary_package_description=$itinerary_package_description;
		$insert_itinerary->itinerary_package_cities=$itinerary_package_cities;
		$insert_itinerary->itinerary_package_services=$itinerary_package_services;
		$insert_itinerary->itinerary_total_cost=$itinerary_total_cost;
		$insert_itinerary->itinerary_validity_operation_from=$itinerary_validity_operation_from;
		$insert_itinerary->itinerary_validity_operation_to=$itinerary_validity_operation_to;
		$insert_itinerary->itinerary_exclusions=$itinerary_exclusions;
		$insert_itinerary->itinerary_terms_and_conditions=$itinerary_terms_and_conditions;
		$insert_itinerary->itinerary_cancellation=$itinerary_cancellation;
		$insert_itinerary->itinerary_image=$itinerary_images;
		$insert_itinerary->itinerary_created_by=$user_id;
		$insert_itinerary->itinerary_created_role=$user_role;

		if($insert_itinerary->save())
		{

			echo "success";
		}
		else
		{
			echo "fail";
		}
	}

	public function edit_itinerary($itinerary_id)
	{

		if(session()->has('travel_users_id'))
		{
			$rights=$this->rights('saved-itinerary');
			$itinerary_id=$itinerary_id;
			$countries=Countries::get();
			$cities=Cities::get();
			$suppliers=Suppliers::where('supplier_status',1)->get();
			
			$emp_id=session()->get('travel_users_id');
			if(strpos($rights['admin_which'],'edit_delete')!==false)
			{
				$get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->first();
			}
			else
			{
				$get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->where('itinerary_created_by',$emp_id)->first();
			}
			
			if($get_itinerary)
			{
				return view('mains.edit-itinerary')->with(compact('countries','cities','suppliers','get_itinerary','rights'));
			}
			else
			{
				return redirect()->back();
			}

		}
		else
		{
			return redirect()->route('index');
		}

	}

	public function edit_itinerary_new($itinerary_id)
	{

		if(session()->has('travel_users_id'))
		{
			$rights=$this->rights('saved-itinerary');
			$itinerary_id=$itinerary_id;
			$countries=Countries::get();
			$cities=Cities::get();
			$emp_id=session()->get('travel_users_id');
			 $get_vehicles=VehicleType::get();
			 	$fetch_airports=AirportMaster::get();
			if(strpos($rights['admin_which'],'edit_delete')!==false)
			{
				$get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->first();
			}
			else
			{
				$get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->where('itinerary_created_by',$emp_id)->first();
			}
			
			if($get_itinerary)
			{
				return view('mains.edit-itinerary-new')->with(compact('countries','cities','get_itinerary','rights','get_vehicles','fetch_airports'));
			}
			else
			{
				return redirect()->back();
			}

		}
		else
		{
			return redirect()->route('index');
		}

	}

	public function update_itinerary(Request $request)
	{
		if(session()->get('travel_users_role')=="Admin")
		{
			$user_role='Admin';
		}
		else
		{
			$user_role='Sub-User';
		}
		$itinerary_id=$request->get('itinerary_id');
		$user_id=session()->has('travel_users_id');
		$itinerary_tour_name=$request->get('tour_name');
		$itinerary_tour_description=$request->get('tour_description');
		$itinerary_occupancy=$request->get('occupancy');
		$itinerary_adult=$request->get('adults');
		$itinerary_child_with_bed=$request->get('child_with_bed');
		$itinerary_child_with_no_bed=$request->get('child_with_no_bed');
		$itinerary_country=$request->get('country');
		$itinerary_tour_fromdate=$request->get('tour_fromdate');
		$itinerary_tour_todate=$request->get('tour_todate');

		$itinerary_dates=$request->get('days_date');
		$itinerary_package_dates=array();
		for($dates_count=0;$dates_count<count($itinerary_dates);$dates_count++)
		{
			$itinerary_package_dates[$dates_count]=$itinerary_dates[$dates_count];
		}
		$itinerary_package_dates=serialize($itinerary_package_dates);

		$itinerary_countries=$request->get('days_country');
		$itinerary_package_countries=array();
		for($country_count=0;$country_count<count($itinerary_countries);$country_count++)
		{
			$itinerary_package_countries[$country_count]=$itinerary_countries[$country_count];
		}
		$itinerary_package_countries=serialize($itinerary_package_countries);

		$itinerary_cities=$request->get('days_city');
		$itinerary_package_cities=array();
		for($city_count=0;$city_count<count($itinerary_cities);$city_count++)
		{
			$itinerary_package_cities[$city_count]=$itinerary_cities[$city_count];
		}
		$itinerary_package_cities=serialize($itinerary_package_cities);

		$itinerary_suppliers=$request->get('supplier_name');
		$itinerary_package_suppliers=array();
		for($supplier_count=0;$supplier_count<count($itinerary_suppliers);$supplier_count++)
		{
			$itinerary_package_suppliers[$supplier_count]=$itinerary_suppliers[$supplier_count];
		}
		$itinerary_package_suppliers=serialize($itinerary_package_suppliers);

		$itinerary_service_type=$request->get('service_type');
		$itinerary_package_service_type=array();
		for($type_count=0;$type_count<count($itinerary_service_type);$type_count++)
		{
			$itinerary_package_service_type[$type_count]=$itinerary_service_type[$type_count];
		}
		$itinerary_package_service_type=serialize($itinerary_package_service_type);

		$itinerary_services=$request->get('days_services');
		$itinerary_package_services=array();
		for($service_count=0;$service_count<count($itinerary_services);$service_count++)
		{
			$itinerary_package_services[$service_count]=$itinerary_services[$service_count];
		}
		$itinerary_package_services=serialize($itinerary_package_services);

		$update_itinerary_array=array(
		"itinerary_tour_name"=>$itinerary_tour_name,
		"itinerary_tour_description"=>$itinerary_tour_description,
		"itinerary_occupancy"=>$itinerary_occupancy,
		"itinerary_adult"=>$itinerary_adult,
		"itinerary_child_with_bed"=>$itinerary_child_with_bed,
		"itinerary_child_with_no_bed"=>$itinerary_child_with_no_bed,
		"itinerary_country"=>$itinerary_country,
		"itinerary_tour_fromdate"=>$itinerary_tour_fromdate,
		"itinerary_tour_todate"=>$itinerary_tour_todate,
		"itinerary_package_dates"=>$itinerary_package_dates,
		"itinerary_package_countries"=>$itinerary_package_countries,
		"itinerary_package_cities"=>$itinerary_package_cities,
		"itinerary_package_suppliers"=>$itinerary_package_suppliers,
		"itinerary_package_service_type"=>$itinerary_package_service_type,
		"itinerary_package_services"=>$itinerary_package_services,
		"itinerary_created_by"=>$user_id,
		"itinerary_created_role"=>$user_role);

		$update_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->update($update_itinerary_array);

		if($update_itinerary)
		{
		$insert_itinerary_log=new SavedItineraryLog;
		$insert_itinerary_log->itinerary_id=$itinerary_id;
		$insert_itinerary_log->itinerary_tour_name=$itinerary_tour_name;
		$insert_itinerary_log->itinerary_tour_description=$itinerary_tour_description;
		$insert_itinerary_log->itinerary_occupancy=$itinerary_occupancy;
		$insert_itinerary_log->itinerary_adult=$itinerary_adult;
		$insert_itinerary_log->itinerary_child_with_bed=$itinerary_child_with_bed;
		$insert_itinerary_log->itinerary_child_with_no_bed=$itinerary_child_with_no_bed;
		$insert_itinerary_log->itinerary_country=$itinerary_country;
		$insert_itinerary_log->itinerary_tour_fromdate=$itinerary_tour_fromdate;
		$insert_itinerary_log->itinerary_tour_todate=$itinerary_tour_todate;
		$insert_itinerary_log->itinerary_package_dates=$itinerary_package_dates;
		$insert_itinerary_log->itinerary_package_countries=$itinerary_package_countries;
		$insert_itinerary_log->itinerary_package_cities=$itinerary_package_cities;
		$insert_itinerary_log->itinerary_package_suppliers=$itinerary_package_suppliers;
		$insert_itinerary_log->itinerary_package_service_type=$itinerary_package_service_type;
		$insert_itinerary_log->itinerary_package_services=$itinerary_package_services;
		$insert_itinerary_log->itinerary_created_by=$user_id;
		$insert_itinerary_log->itinerary_created_role=$user_role;
		$insert_itinerary_log->itinerary_operation_performed="UPDATE";
		$insert_itinerary_log->save();
			echo "success";
		}
		else
		{
			echo "fail";
		}
	}

	public function update_itinerary_new(Request $request)
	{
		if(session()->get('travel_users_role')=="Admin")
		{
			$user_role='Admin';
		}
		else
		{
			$user_role='Sub-User';
		}
		$itinerary_id=$request->get('itinerary_id');
		$user_id=session()->has('travel_users_id');
		$itinerary_tour_name=$request->get('tour_name');
		$itinerary_tour_description=$request->get('tour_description');
		$itinerary_tour_days=$request->get('no_of_days');
		$itinerary_total_cost=$request->get('total_cost');
		$itinerary_validity_operation_from=$request->get('validity_operation_from');
		$itinerary_validity_operation_to=$request->get('validity_operation_to');
		$itinerary_exclusions=$request->get('tour_exclusions');
		$itinerary_terms_and_conditions=$request->get('tour_terms_and_conditions');
		$itinerary_cancellation=$request->get('tour_cancellation');
		$days_number=$request->get('days_number');
		$days_title=$request->get('days_title');
		$days_description=json_decode($request->get('days_description'));
		$hotel_id=$request->get('hotel_id');
		$hotel_name=$request->get('hotel_name');
		$room_id=$request->get('room_id');
		$room_name=$request->get('room_name');
		$room_occupancy_id=$request->get('room_occupancy_id');
		$hotel_cost=$request->get('hotel_cost');
		$hotel_no_of_days=$request->get('hotel_no_of_days');
		$activity_id=$request->get('activity_id');
		$activity_name=$request->get('activity_name');
		$activity_cost=$request->get('activity_cost');

		$sightseeing_id=$request->get('sightseeing_id');
		$sightseeing_name=$request->get('sightseeing_name');
		$sightseeing_tour_type=$request->get('sightseeing_tour_type');
		$sightseeing_vehicle_type=$request->get('sightseeing_vehicle_type');
		$sightseeing_guide_id=$request->get('sightseeing_guide_id');
		$sightseeing_guide_name=$request->get('sightseeing_guide_name');
		$sightseeing_guide_cost=$request->get('sightseeing_guide_cost');
		$sightseeing_driver_id=$request->get('sightseeing_driver_id');
		$sightseeing_driver_name=$request->get('sightseeing_driver_name');
		$sightseeing_driver_cost=$request->get('sightseeing_driver_cost');
		$sightseeing_adult_cost=$request->get('sightseeing_adult_cost');
		$sightseeing_additional_cost=$request->get('sightseeing_additional_cost');
		$sightseeing_cost=$request->get('sightseeing_cost');

		$transfer_id=$request->get('transfer_id');
		$transfer_name=$request->get('transfer_name');
		$transfer_type=$request->get('transfer_type');
		$transfer_from_city=$request->get('transfer_from_city');
		$transfer_to_city=$request->get('transfer_to_city');
		$transfer_from_airport=$request->get('transfer_from_airport');
		$transfer_to_airport=$request->get('transfer_to_airport');
		$transfer_pickup=$request->get('transfer_pickup');
		$transfer_dropoff=$request->get('transfer_dropoff');
		$transfer_vehicle_type=$request->get('transfer_vehicle_type');
		$transfer_guide_id=$request->get('transfer_guide_id');
		$transfer_guide_name=$request->get('transfer_guide_name');
		$transfer_guide_cost=$request->get('transfer_guide_cost');
		$transfer_cost=$request->get('transfer_cost');
		$transfer_total_cost=$request->get('transfer_total_cost');



		$itinerary_countries=$request->get('days_country');
		$itinerary_package_countries_array=array();
		for($country_count=0;$country_count<count($itinerary_countries);$country_count++)
		{
			$itinerary_package_countries_array[$country_count]=$itinerary_countries[$country_count];
		}
		$itinerary_package_countries=implode(",",$itinerary_package_countries_array);

		$itinerary_cities=$request->get('days_city');
		$itinerary_package_cities_array=array();
		for($city_count=0;$city_count<count($itinerary_cities);$city_count++)
		{
			$itinerary_package_cities_array[$city_count]=$itinerary_cities[$city_count];
		}
		$itinerary_package_cities=implode(",",$itinerary_package_cities_array);

		$days_title_array=array();
		for($title_count=0;$title_count<count($days_title);$title_count++)
		{
			$days_title_array[$title_count]=$days_title[$title_count];
		}
		$itinerary_package_title=serialize($days_title_array);

		$days_description_array=array();
		for($desc_count=0;$desc_count<count($days_description);$desc_count++)
		{
			$days_description_array[$desc_count]=$days_description[$desc_count];
		}
		$itinerary_package_description=serialize($days_description_array);

		$services_array=array();
		for($services_count=0;$services_count<=count($days_number)-1;$services_count++)
		{
			if($services_count<count($days_number)-1)
			{
			$services_array[$services_count]["hotel"]["hotel_id"]=$hotel_id[$services_count];
			$services_array[$services_count]["hotel"]["hotel_name"]=$hotel_name[$services_count];
			$services_array[$services_count]["hotel"]["room_id"]=$room_id[$services_count];
			$services_array[$services_count]["hotel"]["room_name"]=$room_name[$services_count];
			$services_array[$services_count]["hotel"]["room_occupancy_id"]=$room_occupancy_id[$services_count];
			$services_array[$services_count]["hotel"]["hotel_cost"]=$hotel_cost[$services_count];
			$services_array[$services_count]["hotel"]["hotel_no_of_days"]=$hotel_no_of_days[$services_count];
			}
			


			$activity_id_array=explode("///",$activity_id[$services_count]);
			$activity_name_array=explode("///",$activity_name[$services_count]);
			$activity_cost_array=explode("///",$activity_cost[$services_count]);


			$services_array[$services_count]["activity"]["activity_id"]=$activity_id_array;
			$services_array[$services_count]["activity"]["activity_name"]=$activity_name_array;
			$services_array[$services_count]["activity"]["activity_cost"]=$activity_cost_array;

			$services_array[$services_count]["sightseeing"]["sightseeing_id"]=$sightseeing_id[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_name"]=$sightseeing_name[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_tour_type"]=$sightseeing_tour_type[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_vehicle_type"]=$sightseeing_vehicle_type[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_guide_id"]=$sightseeing_guide_id[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_guide_name"]=$sightseeing_guide_name[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_guide_cost"]=$sightseeing_guide_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_driver_id"]=$sightseeing_driver_id[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_driver_name"]=$sightseeing_driver_name[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_driver_cost"]=$sightseeing_driver_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_adult_cost"]=$sightseeing_adult_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_additional_cost"]=$sightseeing_additional_cost[$services_count];
			$services_array[$services_count]["sightseeing"]["sightseeing_cost"]=$sightseeing_cost[$services_count];
			
			$services_array[$services_count]["transfer"]["transfer_id"]=$transfer_id[$services_count];
			$services_array[$services_count]["transfer"]["transfer_name"]=$transfer_name[$services_count];
			$services_array[$services_count]["transfer"]["transfer_type"]=$transfer_type[$services_count];
			$services_array[$services_count]["transfer"]["transfer_from_city"]=$transfer_from_city[$services_count];
			$services_array[$services_count]["transfer"]["transfer_to_city"]=$transfer_to_city[$services_count];
			$services_array[$services_count]["transfer"]["transfer_from_airport"]=$transfer_from_airport[$services_count];
			$services_array[$services_count]["transfer"]["transfer_to_airport"]=$transfer_to_airport[$services_count];
			$services_array[$services_count]["transfer"]["transfer_pickup"]=$transfer_pickup[$services_count];
			$services_array[$services_count]["transfer"]["transfer_dropoff"]=$transfer_dropoff[$services_count];
			$services_array[$services_count]["transfer"]["transfer_vehicle_type"]=$transfer_vehicle_type[$services_count];
			$services_array[$services_count]["transfer"]["transfer_guide_id"]=$transfer_guide_id[$services_count];
			$services_array[$services_count]["transfer"]["transfer_guide_name"]=$transfer_guide_name[$services_count];
			$services_array[$services_count]["transfer"]["transfer_guide_cost"]=$transfer_guide_cost[$services_count];
			$services_array[$services_count]["transfer"]["transfer_cost"]=$transfer_cost[$services_count];
			$services_array[$services_count]["transfer"]["transfer_total_cost"]=$transfer_total_cost[$services_count];
		}
		$itinerary_package_services=serialize($services_array);

		$upload_already_ativity_images=$request->get('upload_already_ativity_images');
		$itinerary_images=array();
		if($request->hasFile('upload_ativity_images'))
		{
			
        //multifile uploading
		if($request->hasFile('upload_ativity_images'))
		{
			foreach($request->file('upload_ativity_images') as $file)
			{
				$extension=strtolower($file->getClientOriginalExtension());
				if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
				{
					$image_name=$file->getClientOriginalName();
					$image_itinerary = "itinerary-".time()."-".$image_name;
					$dir1 = 'assets/uploads/itinerary_images/';
					$file->move($dir1, $image_itinerary);
					$itinerary_images[]=$image_itinerary;
				}
			}
		}
		
		}
		else
		{
			$itinerary_images[]=$upload_already_ativity_images;
		}
		$itinerary_images=serialize($itinerary_images);
		


		$update_itinerary_array=array(
		"itinerary_tour_name"=>$itinerary_tour_name,
		"itinerary_tour_description"=>$itinerary_tour_description,
		"itinerary_tour_days"=>$itinerary_tour_days,
		"itinerary_package_countries"=>$itinerary_package_countries,
		"itinerary_package_cities"=>$itinerary_package_cities,
		"itinerary_package_title"=>$itinerary_package_title,
		"itinerary_package_description"=>$itinerary_package_description,
		"itinerary_package_services"=>$itinerary_package_services,
		"itinerary_validity_operation_from"=>$itinerary_validity_operation_from,
		"itinerary_validity_operation_to"=>$itinerary_validity_operation_to,
		"itinerary_total_cost"=>$itinerary_total_cost,
		"itinerary_exclusions"=>$itinerary_exclusions,
		"itinerary_terms_and_conditions"=>$itinerary_terms_and_conditions,
		"itinerary_cancellation"=>$itinerary_cancellation,
		"itinerary_created_by"=>$user_id,
		"itinerary_created_role"=>$user_role,
		"itinerary_image"=>$itinerary_images);

		$update_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->update($update_itinerary_array);

		if($update_itinerary)
		{

			echo "success";
		}
		else
		{
			echo "fail";
		}
	}


// 	public function itinerary_get_hotels(Request $request)
// 	{
		

// 		$country_id=$request->get('country_id');

//  		 $city_id=$request->get('city_id');





// 		$fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id)->where('hotel_status',1)->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','desc')->get();

// 		$html='

// 		<style>

// 		button.book-btn {

// 			background: red;

// 			border: none;

// 			border-radius: 50px;

// 			margin-top: 120px;

// 			padding: 10px 20px;

// 			text-align: center;

// 			color: white;

// 			width: 160px;

// 		}
// 		h4.card-title
// 		{
// 			font-size:14px !important
// 		}

// .cstm-img{
// 	    max-height: 132px;
//     height: 132px;
//     object-fit: cover;
// }
// img.tick {
//     position: absolute;
//     width: auto;
//    height: 40px;
//     top: 0;
//     left: 0;
// }
// 		</style>
// 		<div class="row">';
// 		$room_currency_array=array("USD","EUR");
//         $room_currency_array=array_unique($room_currency_array);
      
//       $currency_price_array=array();
//       foreach($room_currency_array as $currency)
//       {
//         if($currency=="GEL")
//         {

//         }
//         else
//         {
//           $new_currency=$currency;
//         //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
//         //   $cURLConnection = curl_init();

//         //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
//         //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

//         //   $convertedPrice = curl_exec($cURLConnection);
//         //   curl_close($cURLConnection);

//         //   $convertedPriceArray=json_decode($convertedPrice);
//         //   $currency_attribute=$new_currency."_".$this->base_currency;

//         //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
          
//         //  $currency_price_array[$currency]=round($conversion_price,2);
        
//             $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
//                 $scriptDocument = new \DOMDocument();
//                 libxml_use_internal_errors(TRUE); //disable libxml errors
//                 if(!empty($fetch_html)){
//                    //load
//                    $scriptDocument->loadHTML($fetch_html);
                 
//                 //init DOMXPath
//                 	$scriptDOMXPath = new \DOMXPath($scriptDocument);
//                 	//get all the h2's with an id
//                 	$scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
//                 	//check
//                 	if($scriptRow->length > 0){
//                 		foreach($scriptRow as $row){
//                            $conversion_price=round($row->nodeValue,2);
//                 		}
//                 	}
                 
//                 }
//                  $currency_price_array[$currency]=round($conversion_price,2);
        
//         }
         
//       }
//       $currency_price_array["GEL"]=1;



// 		foreach($fetch_hotels as $hotels)

// 		{

// 			// $rate_allocation_details=unserialize($hotels->rate_allocation_details);

// 			// $price=min($rate_allocation_details[0]['room_max']);

//         $hotel_season_details=unserialize($hotels->hotel_season_details);

//        $rate_allocation_details=unserialize($hotels->rate_allocation_details);
//     $room_price_array=array();
//       for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
//       {

//         // if($hotel_season_details[$rate_allocation_count]['booking_validity_from']<=date('Y-m-d') && $hotel_season_details[$rate_allocation_count]['booking_validity_to']>=date('Y-m-d'))
//         // {
//       	if($hotel_season_details[$rate_allocation_count]['booking_validity_from']>=date('Y-m-d') || $hotel_season_details[$rate_allocation_count]['booking_validity_to']>=date('Y-m-d') )
//         {

//         }
//         else
//         {
//           continue;
//         }
//         for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
//         {
//           $room_price_array[]=$rate_allocation_details[$rate_allocation_count]['room_max'][$room_count];

//         }
//       }
//       if(count($room_price_array)>0)
//       {

//       		$price=min($room_price_array);

// 			$room_currency=$rate_allocation_details[0]['room_currency'][0];



			

// 			 $room_price_currency_rate=$currency_price_array[$rate_allocation_details[0]['room_currency'][0]];

// 			 $total_cost=round(($price*$room_price_currency_rate));

// 			$newid=base64_encode($hotels->hotel_id);
// 			$hotelimage=unserialize($hotels->hotel_images);

// 			$html.='<div class="col-md-6 select_hotel" id="hotel__'.$hotels['hotel_id'].'">
// 			<div class="card">
// 			<div style="position:relative">
// 			<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
// 			$hotel_image="";
// 			if(!empty($hotelimage))
// 			{
// 				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'" alt="Hotel Image">';
// 				$hotel_image=asset("assets/uploads/hotel_images"). '/'.$hotelimage[0];
// 			}
// 			else
// 			{
// 				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Hotel Image">';
// 				$hotel_image=asset("assets/images/no-photo.png");
// 			} 
//   $html.='</div>
//   <div class="card-body">
//     <h4 class="card-title">'.$hotels['hotel_name'].'</h4>
//     <input type="hidden" class="search_hotel_cost" name="search_hotel_cost__'.$hotels['hotel_id'].'" value="'.$total_cost.'">
//     <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_address'].'">
//     <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.$hotel_image.'">
//       <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_rating'].'">
//     <div class="row" style="margin-top:20px">
//                         <div class="col-md-12">
//                         <table class="table" id="room_type_table">
//                             <thead>
//                                 <tr>
//                                     <th>Select</th>
//                                     <th>Room Type</th>
//                                     <th>Price per Night</th>
//                                 </tr>
//                             </thead>
//                             <tbody>';

//                         $hotel_season_details=unserialize($hotels->hotel_season_details);
//                         $hotel_blackout_dates=unserialize($hotels->hotel_blackout_dates);
//                         $rate_allocation_details=unserialize($hotels->rate_allocation_details);

                      
//                                 $room_counter=0;

//                                 for($rate_allocation_count=0;$rate_allocation_count< count($hotels->hotel_season_details);$rate_allocation_count++)
//                                 {
                                        
//                                     for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
//                                     {
                                        

//                                     	 $total_cost=round($rate_allocation_details[$rate_allocation_count]['room_max'][$room_count],2);
//                                         $room_price_currency_rate=$currency_price_array[$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count]];

// 											 $total_cost=round(($total_cost*$room_price_currency_rate));
                                        
                                       
//                                           $html.='<tr>
//                                     <td>
//                                          <input type="radio"
//                                                 class="with-gap radio-col-primary" name="room_type_'.$hotels['hotel_id'].'" id="room_'.$room_counter.'_'.$hotels['hotel_id'].'" value="'.$room_counter.'" class="room_type_change" >
//                                             <label for="room_'.$room_counter.'_'.$hotels['hotel_id'].'"></label>
                                   
//                                     </td>
//                                     <td>
//                                         <a href="#" class="details"><i class="fa fa-caret-right"></i>'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room ('.$rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count].' Adults + ';

//                                             if($rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count]=="")
//                                             {
//                                             	$html.=' 0 ';
//                                             }
//                                             else
//                                             {
//                                             	$html.=$rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count];
//                                             }
                                          

//                                         $html.='Child)<input type="hidden" name="room_name_'.$room_counter.'_'.$hotels['hotel_id'].'" value="'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room';
//                                             $html.='"></a>
//                                        <!--  <p>1 single bed <i class="fa fa-bed"></i></p> -->
//                                     </td>
//                                     <td>
//                                         <input type="hidden" name="room_price_'.$room_counter.'_'.$hotels['hotel_id'].'" value="'.$total_cost.'">';
                                       
//                                         $html.='<input type="hidden" name="room_price_currency_'.$room_counter.'_'.$hotels['hotel_id'].'" value="GEL"> 
//                                         <span class="show-price">GEL '.$total_cost.'</span>
//                                     </td>
//                                 </tr>';
//                                         $room_counter++;

//                                     }



//                                 }

                  
                              
//                             $html.='</tbody>
//                         </table>
//                         </div>
//                     </div>
// 	    <a href="'.route('hotel-details',["hotel_id"=>$hotels['hotel_id']]).'" class=" btn btn-sm btn-primary" target="_blank">View Details</a>
// 	  </div>
// 	</div>
// </div>';
// 		}
// 		}
// 		echo $html;

// 	}

	public function itinerary_get_hotels(Request $request)
  {
  $limit=12;
    if($request->has('offset'))
    {
        $offset=$request->get('offset');
    }
    else
    {
        $offset=0;
    }
    $country_id=$request->get('country_id');
    $city_id=$request->get('city_id');
      $hotel_current_id=$request->get('hotel_current_id');
    $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id)->where('hotel_approve_status',1)->where('booking_validity_to',">=",date('Y-m-d'))->where('hotel_status',1)->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->offset($offset*$limit)->take($limit)->orderBy('hotel_id','desc')->get();


    if($offset==0)
    {


    $html_header='<style>
    .card-body .card-title {
      margin-bottom: 0;
      border-bottom: none;
      text-overflow: ellipsis !important;
      white-space: nowrap;
      color: black;
      overflow: hidden;
      font-size: 14px;
    }
    .card {
      border-radius: 0px;
      box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
      margin-bottom: 30px !important;
      min-height: 230px;
    }
    .card-img-top {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    .more-btn{
      border: none !important;
      background: no-repeat;
      float: right;
      color: #2196F3;
      font-size: 12px;
      font-weight: 700;
    }
    .card-body {
      padding: 10px;
    }
    .fa-star-checked
    {
    	color:#FFD700;
    }
    </style>
    <div class="row" id="hotel_sort_div">';
}
else
{
	$html_header="";
}
     $html="";
      $html_current="";

      $room_currency_array=array("USD");
      $room_currency_array=array_unique($room_currency_array);
      
      $currency_price_array=array();
      foreach($room_currency_array as $currency)
      {
           $new_currency=$currency;
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);
         
      }

      $currency_price_array["GEL"]=1;
    foreach($fetch_hotels as $hotels)
    {

     //    $hotel_season_details=unserialize($hotels->hotel_season_details);

     //   $rate_allocation_details=unserialize($hotels->rate_allocation_details);


     //  $room_price_array=array();
     // $room_currency_array=array();
     //  for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
     //  {

     //    if($hotel_season_details[$rate_allocation_count]['booking_validity_from']<=$hotel_checkin && $hotel_season_details[$rate_allocation_count]['booking_validity_to']>=$hotel_checkin)
     //    {

     //    }
     //    else
     //    {
     //      continue;
     //    }
     //    for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
     //    {
     //      $room_price_array[]=$rate_allocation_details[$rate_allocation_count]['room_max'][$room_count];
     //       $room_currency_array[]=$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count];

     //    }
     //  }


          $room_price_array=array();
     $room_currency_array=array();


     $hotel_rooms=HotelRooms::where('hotel_id',$hotels->hotel_id)->get();

     $hotel_currency=$hotels->hotel_currency;

     $room_min_name="";
     $room_min_id="";
     $room_min_occupancy="";
     $room_min_price=0;
     foreach($hotel_rooms as $rooms_value)
     {

      if($hotel_currency==null)
      {
        $hotel_currency=$rooms_value->hotel_room_currency;
      }
      $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$rooms_value->hotel_room_id)->get();

      foreach($get_hotel_seasons as $hotel_seasons)
      {
      	if($hotel_seasons->hotel_room_season_validity_to>=date('Y-m-d'))
  {
        $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();

        foreach($get_occupancy as $occupancy)
        {
          if($room_min_price==0)
          {
             $room_min_price=$occupancy->hotel_room_occupancy_price;
          }
          else if($occupancy->hotel_room_occupancy_price<$room_min_price)
          {
            $room_min_price=$occupancy->hotel_room_occupancy_price;
            $room_min_name=ucwords($rooms_value->hotel_room_class)." ".ucwords($rooms_value->hotel_room_type)." Room";
            $room_min_id=$rooms_value->hotel_room_id;
            $room_min_occupancy=$occupancy->hotel_room_occupancy_id;
          }
        }
    }
      }
     }

      $new_currency="";
       if($room_min_price>0)
      {

        $price=$room_min_price;
         if(isset($currency_price_array[$hotel_currency]))
        {
        $price=round($room_min_price*$currency_price_array[$hotel_currency],2);
        }
        else
        {
          $new_currency= $hotel_currency;
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
         $price=round($price*$conversion_price,2);
          
        }

        



        $total_cost=round($price);

      $newid=base64_encode($hotels->hotel_id);
      $hotelimage=unserialize($hotels->hotel_images);
      $style="";
 
         $html.='<div class="col-md-3 select_hotel" id="hotel__'.$hotels->hotel_id.'">
      <div class="card">
      <div style="position:relative">
      <!--<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">-->';
      if(!empty($hotelimage))
      {
        $html.='  <img class="card-img-top cstm-img" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'" alt="Hotel Image" style="width:222px;height:110px">';
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Hotel Image" style="width:222px;height:110px">';
      }

     $html.=' 
      </div>
      <div class="card-body">

      <span class="card-rating">';
      for($i=1;$i<=5;$i++)
      {
        if($i<=$hotels->hotel_rating)
        {
          $html.="<i class='fa fa-star  fa-star-checked checked'></i>";
        }
        else
        {
          $html.="<i class='fa fa-star'></i>";
        }

      }
      $html.='</span>
      <span class="card-rating-text" style="display:none">'.$hotels->hotel_rating.'</span>
      <h5 class="card-title">'.$hotels->hotel_name.' <br> <small>'.$hotels->hotel_address.'</small></h5>
      <span><small>Price/Room</small></span><br>
      <span class="card-price">GEL '.$total_cost.'</span>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <button class="more-btn">Details</button>
       <input type="hidden" class="search_hotel_name" name="search_hotel_name__'.$hotels->hotel_id.'" value="'.$hotels->hotel_name.'">
      <input type="hidden" class="search_hotel_cost" name="search_hotel_cost__'.$hotels->hotel_id.'" value="'.$total_cost.'">
        <input type="hidden" class="search_room_name" name="search_room_name__'.$hotels->hotel_id.'" value="'.$room_min_name.'">
         <input type="hidden" class="search_room_id" name="search_room_id__'.$hotels->hotel_id.'" value="'.$room_min_id.'">
          <input type="hidden" class="search_room_occupancy_id" name="search_room_occupancy_id__'.$hotels->hotel_id.'" value="'.$room_min_occupancy.'">
      <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$hotels->hotel_id.'" value="'.$hotels->hotel_address.'">';
       if(!empty($hotelimage))
      {
        $html.=' <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels->hotel_id.'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'">';
      }
      else
      {
        $html.='<input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels->hotel_id.'" value="'.asset("assets/images/no-photo.png").'">';
      }

       $html.='
      <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$hotels->hotel_id.'" value="'.$hotels->hotel_rating.'">
      </div>
      </div>
      </div>';
      }
     
    }
    echo $html_header." ".$html_current." ".$html;
}

	public function itinerary_get_activities(Request $request)
	{

		$country_id=$request->get('country_id');

		$city_id=$request->get('city_id');
		$cities=$request->get('cities');

		$cities_array=explode(",",$cities);

		$cities_array=array_unique($cities_array);
		$cities_array=array_filter($cities_array);

		$cities_array[]=$city_id;




		$fetch_activites=Activities::where('activity_country',$country_id)->whereIn('activity_city',$cities_array);
		$fetch_activites=$fetch_activites->where('activity_status',1)->orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','desc')->get();
		$html='<style>

		h4.card-title
		{
			font-size:14px !important
		}

		.cstm-img{
			max-height: 132px;
			height: 132px;
			object-fit: cover;
		}
			img.tick {
	    position: absolute;
	    width: auto;
	   height: 40px;
	    top: 0;
	    left: 0;
	}
		</style>
		<div class="row">';



		foreach($fetch_activites as $activities)

		{
			 $adult_price=0;
      $adult_price_details=unserialize($activities->adult_price_details);

      if(!empty($adult_price_details))
      {
         $adult_price=$adult_price_details[0]['adult_pax_price'];
      }
      else
      {
        $adult_price=0;
      }
     

      $total_cost=round($adult_price);
      $activity_date=date("Y-m-d");

        $no_of_bookings=0;
        // $availability_qty_details=unserialize($activities->availability_qty_details);

        // for($avail=0;$avail< count($availability_qty_details);$avail++)
        // {
        //   if($activity_date>=$availability_qty_details[$avail]['availability_from'] && $activity_date<=$availability_qty_details[$avail]['availability_to'])
        //   {
        //     $no_of_bookings=$availability_qty_details[$avail]['no_of_bookings'];
        //   }
        // }
         $availability_qty_details=unserialize($activities->availability_qty_details);

        for($avail=0;$avail< count($availability_qty_details);$avail++)
        {
          if($activity_date>=$availability_qty_details[$avail]['availability_from'] && $activity_date<=$availability_qty_details[$avail]['availability_to'])
          {
            if(!empty($availability_qty_details[$avail]["availability_time_from"]))
            {
              $no_of_bookings=$availability_qty_details[$avail]["availability_no_of_bookings"][0];    
            }
            else
            {
              $no_of_bookings=0;

            }
            // $no_of_bookings=$availability_qty_details[$avail]['no_of_bookings'];
          }
        }


        if($no_of_bookings>0)
        {

			$get_activity_images=unserialize($activities->activity_images);
			$html.='<div class="col-md-4 select_activity" id="activity__'.$activities->activity_id.'"><div class="card">
			<div style="position:relative">
			<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
			$img_url="";
			if(!empty($get_activity_images))
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/activities_images"). '/'.$get_activity_images[0].'" alt="Activity Image">';
				$img_url=asset("assets/uploads/activities_images"). '/'.$get_activity_images[0];
			}
			else
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Activity Image">';
				$img_url=asset("assets/images/no-photo.png");
			} 

			$html.='</div><div class="card-body">
			<h4 class="card-title">'.$activities->activity_name.'</h4>
			<p class="card-text">'.$activities->activity_currency.' '.$total_cost.'</p>
			 <input type="hidden" class="search_activity_cost" name="search_activity_cost__'.$activities->activity_id.'" value="'.$total_cost.'">
			  <input type="hidden" class="search_activity_address" name="search_activity_address__'.$activities->activity_id.'" value="'.$activities->activity_location.'">
			   <input type="hidden" class="search_activity_image" name="search_activity_image__'.$activities->activity_id.'" value="'.$img_url.'">

			<a href="'.route('activity-details',["activity_id"=>$activities->activity_id]).'" class=" btn btn-sm btn-primary" target="_blank">View Details</a>
			</div>
			</div>
			</div>';

		}
	}


		echo $html;


	}

	public function itinerary_get_sightseeing(Request $request)
	{
 $limit=9;
    if($request->has('offset'))
    {
        $offset=$request->get('offset');
    }
    else
    {
        $offset=0;
    }
		$country_id=$request->get('country_id');

		$city_id=$request->get('city_id');
		$prev_city_id=$request->get('prev_city_id');
		if($prev_city_id!="")
		{
			$fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id,$prev_city_id){
			    $query->where(function ($query1) use ($city_id,$prev_city_id){
			    	$query1->where('sightseeing_city_from',$city_id)->orWhere('sightseeing_city_from',$prev_city_id);

			    })->where('sightseeing_city_to',$city_id); });
		}
		else
		{
			 $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id){
    $query->where('sightseeing_city_from',$city_id)->where('sightseeing_city_to',$city_id); });
		}

$fetch_sightseeing=$fetch_sightseeing->where('sightseeing_status',1)->orderBy('sightseeing_popular_status','desc')->orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->orderBy('sightseeing_id','desc')->offset($offset*$limit)->take($limit)->get();

if($offset==0)
{

		$html='<style>

		h4.card-title
		{
			font-size:14px !important
		}

		.cstm-img{
			max-height: 132px;
			height: 132px;
			object-fit: cover;
		}
			img.tick {
	    position: absolute;
	    width: auto;
	   height: 40px;
	    top: 0;
	    left: 0;
	}
		</style>
		<div class="row" id="sightseeing_sort_div">';
	}
	else
	{
		$html='';
	}
		$currency="GEL";


		foreach($fetch_sightseeing as $sightseeing)

{

 $price=$sightseeing->sightseeing_adult_cost;

 $price+=round($sightseeing->sightseeing_food_cost);
 $price+=round($sightseeing->sightseeing_hotel_cost);

  if($sightseeing->sightseeing_default_guide_price!="" && $sightseeing->sightseeing_default_guide_price!=null)
      {
        $price+=round($sightseeing->sightseeing_default_guide_price);
      }

      if($sightseeing->sightseeing_default_driver_price!="" && $sightseeing->sightseeing_default_driver_price!=null)
      {
        $price+=round($sightseeing->sightseeing_default_driver_price);
      }

$total_cost=round($price);

  $newid=base64_encode($sightseeing->sightseeing_id);
  		$address="";
			 $sightseeingimage=unserialize($sightseeing->sightseeing_images);

			 $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_from,$sightseeing->sightseeing_country);
			 $address.=$get_from_city['name']."-";

			 if($sightseeing->sightseeing_city_between!=null && $sightseeing->sightseeing_city_between!="")
			 {
			 	$all_between_cities=explode(",",$sightseeing->sightseeing_city_between);
			 	for($cities=0;$cities< count($all_between_cities);$cities++)
			 	{
			 		$fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$sightseeing->sightseeing_country);
			 		$address.=$fetch_city['name']."-";
			 	}
			 }


			 $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_to,$sightseeing->sightseeing_country);
			 $address.=$get_from_city['name'];

                                               
		
			$html.='<div class="col-md-4 select_sightseeing" id="sightseeing__'.$sightseeing->sightseeing_id.'"><div class="card">
			<div style="position:relative">
			<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
			$image_url="";
			if(!empty($sightseeingimage))
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/sightseeing_images"). '/'.$sightseeingimage[0].'" alt="Sightseeing Image">';
				$image_url=asset("assets/uploads/sightseeing_images"). '/'.$sightseeingimage[0];
			}
			else
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Activity Image">';
				$image_url=asset("assets/images/no-photo.png");
			} 

			$html.='</div><div class="card-body">
			<h4 class="card-title">'.$sightseeing->sightseeing_tour_name.'</h4>
			<p class="card-text">'.$currency.' '.$total_cost.'</p>
			 <input type="hidden" class="search_sightseeing_cost" name="search_sightseeing_cost__'.$sightseeing->sightseeing_id.'" value="'.$total_cost.'">
			 <input type="hidden" class="search_sightseeing_address" name="search_sightseeing_address__'.$sightseeing->sightseeing_id.'" value="'.$address.'">
			 <input type="hidden" class="search_sightseeing_distance" name="search_sightseeing_distance__'.$sightseeing->sightseeing_id.'" value="'.$sightseeing->sightseeing_distance_covered.'">
			 <input type="hidden" class="search_sightseeing_image" name="search_sightseeing_image__'.$sightseeing->sightseeing_id.'" value="'.$image_url.'">
			<button id="sightseeing_details_'.$sightseeing->sightseeing_id.'" class=" btn btn-sm btn-primary sightseeing_details" target="_blank">View Details</button>
			</div>
			</div>
			</div>';

		}


		echo $html;

	}

	public function itinerary_get_sightseeing_details(Request $request)
	{
	$mainid=$request->sightseeing_id;
      $get_vehicles=VehicleType::get();
      $get_sightseeing=SightSeeing::where('sightseeing_status',1)->where('sightseeing_id',$mainid)->first();
      if($get_sightseeing)
      {
      	$get_from_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_from,$get_sightseeing->sightseeing_country);
      	$via_route=$get_from_city['name']."-";

      	if($get_sightseeing->sightseeing_city_between!=null && $get_sightseeing->sightseeing_city_between!="")
      	{
      		$all_between_cities=explode(",",$get_sightseeing->sightseeing_city_between);
      		for($cities=0;$cities< count($all_between_cities);$cities++)
      		{
      			$fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$get_sightseeing->sightseeing_country);
      				$via_route.=$fetch_city['name']."-";
      		}
      	}
      	$get_from_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_to,$get_sightseeing->sightseeing_country);
      	$via_route.=$get_from_city['name'];

      	$sightseeing_images_array=array();
      	if($get_sightseeing->sightseeing_images!=null && $get_sightseeing->sightseeing_images!="")
      	{
      		$sightseeing_images=unserialize($get_sightseeing->sightseeing_images);
      		if(!empty($sightseeing_images[0]))
      		{
      			foreach($sightseeing_images as $sightseeing_image)
      			{
      				$sightseeing_images_array[]=asset("assets/uploads/sightseeing_images")."/".$sightseeing_image;
      			}
      		}
      		else
      		{
      			$sightseeing_images_array[]=asset("assets/images/no-photo.png");
      		}
      	}
      	else
      	{
      		$sightseeing_images_array[]=asset("assets/images/no-photo.png");
      	}
      	 $price=$get_sightseeing->sightseeing_adult_cost;
		 $price+=round($get_sightseeing->sightseeing_food_cost);
		 $price+=round($get_sightseeing->sightseeing_hotel_cost);
		$total_adult_cost=round($price);
		$group_tour_status=0;
		if($get_sightseeing->sightseeing_group_adult_cost!=null && $get_sightseeing->sightseeing_group_adult_cost!="")
		{
			$group_tour_status=1;
			
		}
		$sightseeing_additional_cost=$get_sightseeing->sightseeing_additional_cost;
		if($get_sightseeing->sightseeing_additional_cost=="" || $get_sightseeing->sightseeing_additional_cost==null)
		{
			$sightseeing_additional_cost=0;
		}
		$sightseeing_cities="";
		if($get_sightseeing->sightseeing_city_between!="")
		{
			$sightseeing_cities=$get_sightseeing->sightseeing_city_between.",".$get_sightseeing->sightseeing_city_to;
		}
		else if($get_sightseeing->sightseeing_city_to!="")
		{
			$sightseeing_cities=$get_sightseeing->sightseeing_city_to;
		}
		else
		{
			$sightseeing_cities=$get_sightseeing->sightseeing_city_from;
		}
      	$group_adult_cost=round($get_sightseeing->sightseeing_group_adult_cost);
     	$data=array("sightseeing_id"=>$get_sightseeing->sightseeing_id,
     				"sightseeing_name"=>$get_sightseeing->sightseeing_tour_name,
     				"sightseeing_route"=>$via_route,
     				"sightseeing_cities"=>$sightseeing_cities,
     				"sightseeing_distance_covered"=>$get_sightseeing->sightseeing_distance_covered." KMS",
     				"sightseeing_duration"=>$get_sightseeing->sightseeing_duration." HOURS",
     				"sightseeing_images"=>$sightseeing_images_array,
     				"sightseeing_tour_desc"=>$get_sightseeing->sightseeing_tour_desc,
     				"sightseeing_attractions"=>$get_sightseeing->sightseeing_attractions,
     				"sightseeing_group_tour_status"=>$group_tour_status,
     				"group_adult_cost"=>$group_adult_cost,
     				"total_adult_cost"=>$total_adult_cost,
     				"sightseeing_additional_cost"=>$sightseeing_additional_cost,
     				"sightseeing_default_guide_price"=>$get_sightseeing->sightseeing_default_guide_price,
     				"sightseeing_default_driver_price"=>$get_sightseeing->sightseeing_default_driver_price,
     				"sightseeing_details_link"=>route('sightseeing-details-view',['sightseeing_id'=>base64_encode($get_sightseeing->sightseeing_id),'itinerary'=>1]));

     	echo json_encode($data);

	}
	else
	{
		$data=array();
     	echo json_encode($data);

	}

}

	public function itinerary_get_transfer(Request $request)
	{
if($request->get('transfer_type')=="from-airport")
  {
  
    $transfer_type=$request->get('transfer_type');
    $country_id=$request->get('country_id');

    $from_airport=$request->get('from_airport');
    $to_city=$request->get('to_city');
    
    $airports=AirportMaster::where('airport_master_id',$from_airport)->first();
    $airport_name=$airports['airport_master_name'];
    $cities=Cities::where('id',$to_city)->first();
    $city_name=$cities['name'];
   
    $vehicle_type=VehicleType::get();
    $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_airport)->where('transfer_details.to_city_airport',$to_city)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_details_id','desc')->get();
    $html=' <p>Showing results for Airport Transfer from <b>"'. $airport_name.'"</b> to <b>"'.$city_name.'"</b></p>
    <style>
    button.book-btn {
      background: red;
      border: none;
      border-radius: 50px;
      margin-top: 120px;
      padding: 10px 20px;
      text-align: center;
      color: white;
      width: 160px;
    }
    .hotel-name small
    {
      color:grey;
    }
    h4.card-title
		{
			font-size:14px !important
		}

		.cstm-img{
			max-height: 132px;
			height: 132px;
			object-fit: cover;
		}
			img.tick {
	    position: absolute;
	    width: auto;
	   height: 40px;
	    top: 0;
	    left: 0;
	}
    </style>

    <div class="row">';


    foreach($fetch_transfers as $transfers)
    {
   
      $vehicle_type_name="";
      $vehicle_name="";
      $vehicle_min=0;
      $vehicle_max=0;
      $vehicle_type_data=VehicleType::where('vehicle_type_id',$transfers->transfer_vehicle_type)->first();
      $vehicles=Vehicles::where('vehicle_id',$transfers->transfer_vehicle)->first();

      $vehicle_type_name=$vehicle_type_data['vehicle_type_name'];
      $vehicle_min=$vehicle_type_data['vehicle_type_min'];
      $vehicle_max=$vehicle_type_data['vehicle_type_max'];


      $vehicle_name=$vehicles['vehicle_name'];
     	$vehicle_cost=$transfers->transfer_vehicle_cost;
  
       $html.='<div class="col-md-4 select_transfer" id="transfer__'.$transfers->transfer_id.'"><div class="card">
			<div style="position:relative">
			<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
			 $transferimage=unserialize($transfers->transfer_vehicle_images);
			if(!empty($transferimage[0]))
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0].'" alt="Transfer Image">';
				 $actual_image=asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0];
			}
			else
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Transfer Image">';
				 $actual_image=asset("assets/images/no-photo.png");
			} 

			$html.='</div><div class="card-body">
			<h4 class="card-title">'.$vehicle_name.' ('.$vehicle_type_name.')  <br> <small>'.$transfers->transfer_vehicle_info.'</small></h4>
			<p class="card-text">GEL '. $vehicle_cost.'</p>
			 <input type="hidden" class="search_transfer_cost" name="search_transfer_cost__'.$transfers->transfer_id.'" value="'.$vehicle_cost.'">
			 <input type="hidden" class="search_transfer_from_airport" name="search_transfer_from_airport__'.$transfers->transfer_id.'" value="'.$transfers->from_city_airport.'">
			 <input type="hidden" class="search_transfer_to_city" name="search_transfer_to_city__'.$transfers->transfer_id.'" value="'.$transfers->to_city_airport.'">
			 <input type="hidden" class="search_vehicle_image" name="search_vehicle_image__'.$transfers->transfer_id.'" value="'.$actual_image.'">
			  <input type="hidden" class="search_vehicle_type" name="search_vehicle_type__'.$transfers->transfer_id.'" value="'.$transfers->transfer_vehicle_type.'">
			</div>
			</div>
			</div>';
   
    
    }
     if(preg_match('/img/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }
      
    
  }
  else if($request->get('transfer_type')=="to-airport")
{

    $transfer_type=$request->get('transfer_type');
    $country_id=$request->get('country_id');

    $from_city=$request->get('from_city');
    $to_airport=$request->get('to_airport');
    
    $airports=AirportMaster::where('airport_master_id',$to_airport)->first();
    $airport_name=$airports['airport_master_name'];
    $cities=Cities::where('id',$from_city)->first();
    $city_name=$cities['name'];
   
    $vehicle_type=VehicleType::get();
    $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_city)->where('transfer_details.to_city_airport',$to_airport)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_details_id','desc')->get();

    $html=' <p>Showing results for Airport Transfer from <b>"'. $city_name.'"</b> to <b>"'.$airport_name.'"</b></p>
    <style>
    button.book-btn {
      background: red;
      border: none;
      border-radius: 50px;
      margin-top: 120px;
      padding: 10px 20px;
      text-align: center;
      color: white;
      width: 160px;
    }
    .hotel-name small
    {
      color:grey;
    }
    h4.card-title
		{
			font-size:14px !important
		}

		.cstm-img{
			max-height: 132px;
			height: 132px;
			object-fit: cover;
		}
			img.tick {
	    position: absolute;
	    width: auto;
	   height: 40px;
	    top: 0;
	    left: 0;
	}
    </style>

    <div class="row">';
    foreach($fetch_transfers as $transfers)
    {
   
      $vehicle_type_name="";
      $vehicle_name="";
      $vehicle_min=0;
      $vehicle_max=0;
      $vehicle_type_data=VehicleType::where('vehicle_type_id',$transfers->transfer_vehicle_type)->first();
      $vehicles=Vehicles::where('vehicle_id',$transfers->transfer_vehicle)->first();

      $vehicle_type_name=$vehicle_type_data['vehicle_type_name'];
      $vehicle_min=$vehicle_type_data['vehicle_type_min'];
      $vehicle_max=$vehicle_type_data['vehicle_type_max'];


      $vehicle_name=$vehicles['vehicle_name'];


     
       $vehicle_cost=$transfers->transfer_vehicle_cost;
  
        $html.='<div class="col-md-4 select_transfer" id="transfer__'.$transfers->transfer_id.'"><div class="card">
			<div style="position:relative">
			<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
			 $transferimage=unserialize($transfers->transfer_vehicle_images);
			 $actual_image="";
			if(!empty($transferimage[0]))
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0].'" alt="Transfer Image">';
				 $actual_image=asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0];
			}
			else
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Transfer Image">';
				 $actual_image=asset("assets/images/no-photo.png");
			} 

			$html.='</div><div class="card-body">
			<h4 class="card-title">'.$vehicle_name.' ('.$vehicle_type_name.')  <br> <small>'.$transfers->transfer_vehicle_info.'</small></h4>
			<p class="card-text">GEL '. $vehicle_cost.'</p>
			 <input type="hidden" class="search_transfer_cost" name="search_transfer_cost__'.$transfers->transfer_id.'" value="'.$vehicle_cost.'">
			 <input type="hidden" class="search_transfer_from_city" name="search_transfer_from_city__'.$transfers->transfer_id.'" value="'.$transfers->from_city_airport.'">
			 <input type="hidden" class="search_transfer_to_airport" name="search_transfer_to_airport__'.$transfers->transfer_id.'" value="'.$transfers->to_city_airport.'">
			 <input type="hidden" class="search_vehicle_image" name="search_vehicle_image__'.$transfers->transfer_id.'" value="'.$actual_image.'">
			 <input type="hidden" class="search_vehicle_type" name="search_vehicle_type__'.$transfers->transfer_id.'" value="'.$transfers->transfer_vehicle_type.'">
			</div>
			</div>
			</div>';
   
    
    }
     if(preg_match('/img/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }

}
  else
  {
    // $markup=$this->agent_markup();
    // $transfer_markup=0;
    // if($markup!="")
    // {
    //   $get_all_services=explode("///",$markup);
    //   for($markup=0;$markup<count($get_all_services);$markup++)
    //   {
    //     $get_individual_service=explode("---",$get_all_services[$markup]);
    //     if($get_individual_service[0]=="transfer")
    //     {
    //       if($get_individual_service[1]!="")
    //       {
    //         $transfer_markup=$get_individual_service[1];
    //       }
    //       break;
    //     }
    //   }
    // }
    $transfer_type=$request->get('transfer_type');
    $country_id=$request->get('country_id');
    $from_city=$request->get('from_city');
    $to_city=$request->get('to_city');
    
    $cities=Cities::where('id',$from_city)->first();
    $from_city_name=$cities['name'];
    $cities=Cities::where('id',$to_city)->first();
    $to_city_name=$cities['name'];
    

    $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_city)->where('transfer_details.to_city_airport',$to_city)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_id','desc')->get();

    $html='<p>Showing results for City Transfer from <b>"'. $from_city_name.'"</b> to <b>"'.$to_city_name.'"</b></p>
     <style>
    button.book-btn {
      background: red;
      border: none;
      border-radius: 50px;
      margin-top: 120px;
      padding: 10px 20px;
      text-align: center;
      color: white;
      width: 160px;
    }
    .hotel-name small
    {
      color:grey;
    }
    h4.card-title
		{
			font-size:14px !important
		}

		.cstm-img{
			max-height: 132px;
			height: 132px;
			object-fit: cover;
		}
			img.tick {
	    position: absolute;
	    width: auto;
	   height: 40px;
	    top: 0;
	    left: 0;
	}
    </style>
    <div class="row">';
    foreach($fetch_transfers as $transfers)
    {
      $vehicle_type_name="";
      $vehicle_name="";
      $vehicle_min=0;
      $vehicle_max=0;
      $vehicle_type_data=VehicleType::where('vehicle_type_id',$transfers->transfer_vehicle_type)->first();
      $vehicles=Vehicles::where('vehicle_id',$transfers->transfer_vehicle)->first();

      $vehicle_type_name=$vehicle_type_data['vehicle_type_name'];
      $vehicle_min=$vehicle_type_data['vehicle_type_min'];
      $vehicle_max=$vehicle_type_data['vehicle_type_max'];


      $vehicle_name=$vehicles['vehicle_name'];

       $vehicle_cost=$transfers->transfer_vehicle_cost;



        $html.='<div class="col-md-4 select_transfer" id="transfer__'.$transfers->transfer_id.'"><div class="card">
			<div style="position:relative">
			<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
			 $transferimage=unserialize($transfers->transfer_vehicle_images);
			if(!empty($transferimage[0]))
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0].'" alt="Transfer Image">';
				 $actual_image=asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0];
			}
			else
			{
				$html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Transfer Image">';
				 $actual_image=asset("assets/images/no-photo.png");
			} 

			$html.='</div><div class="card-body">
			<h4 class="card-title">'.$vehicle_name.' ('.$vehicle_type_name.')</h4>
			<p class="card-text">GEL '. $vehicle_cost.'</p>
			 <input type="hidden" class="search_transfer_cost" name="search_transfer_cost__'.$transfers->transfer_id.'" value="'.$vehicle_cost.'">
			 <input type="hidden" class="search_transfer_from_city" name="search_transfer_from_city__'.$transfers->transfer_id.'" value="'.$transfers->from_city_airport.'">
			 <input type="hidden" class="search_transfer_to_city" name="search_transfer_to_city__'.$transfers->transfer_id.'" value="'.$transfers->to_city_airport.'">
			 <input type="hidden" class="search_vehicle_image" name="search_vehicle_image__'.$transfers->transfer_id.'" value="'.$actual_image.'">
			 <input type="hidden" class="search_vehicle_type" name="search_vehicle_type__'.$transfers->transfer_id.'" value="'.$transfers->transfer_vehicle_type.'">
			</div>
			</div>
			</div>';
  
    }
     if(preg_match('/img/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }
  }


	}

	public function update_itinerary_active_inactive(Request $request)
	{
		$id=$request->get('itinerary_id');

  $action_perform=$request->get('action_perform');



  if($action_perform=="b2bactive")

  {

    $update_itinerary=SavedItinerary::where('itinerary_id',$id)->update(["itinerary_status"=>1]);

    if($update_itinerary)

    {

      echo "success";

    }

    else

    {

      echo "fail";

    }

  }

  else if($action_perform=="b2binactive")

  {

   $update_itinerary=SavedItinerary::where('itinerary_id',$id)->update(["itinerary_status"=>0]);

   if($update_itinerary)

   {

    echo "success";

  }

  else

  {

    echo "fail";

  }

}
 else if($action_perform=="b2cinactive")

  {

   $update_itinerary=SavedItinerary::where('itinerary_id',$id)->update(["itinerary_bc_status"=>0]);

   if($update_itinerary)

   {

    echo "success";

  }

  else

  {

    echo "fail";

  }

}
else if($action_perform=="b2cactive")

  {

   $update_itinerary=SavedItinerary::where('itinerary_id',$id)->update(["itinerary_bc_status"=>1]);

   if($update_itinerary)

   {

    echo "success";

  }

  else

  {

    echo "fail";

  }

}

else

{

  echo "fail";

}

	}
	public function sort_itinerary(Request $request)
  {
    $sort_activity_array=$request->get('new_data');
    $success_array=array();
    for($count=0;$count<count($sort_activity_array);$count++)
    {
      $itinerary_id=$sort_activity_array[$count]['itinerary_id'];
      $new_order=$sort_activity_array[$count]['new_order'];
      $update_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->update(["itinerary_show_order"=>$new_order]);
      if($update_itinerary)
      {
        $success_array[]="success";
      }
      else
      {
        $success_array[]="not_success";
      }
    }
    echo json_encode($success_array);
  }
  public function update_itinerary_bestseller(Request $request)
  {
    $id=$request->get('itinerary_id');
    $action_perform=$request->get('action_perform');
    if($action_perform=="bestselleryes")
    {
      $update_itinerary=SavedItinerary::where('itinerary_id',$id)->update(["itinerary_best_status"=>1]);
      if($update_itinerary)
      {
        echo "success";
      }
      else
      {
        echo "fail";
      }
    }
    else if($action_perform=="bestsellerno")
    {
      $update_itinerary=SavedItinerary::where('itinerary_id',$id)->update(["itinerary_best_status"=>0]);
      if($update_itinerary)
      {
        echo "success";
      }
      else
      {
        echo "fail";
      }
    }
    else
    {
      echo "fail";
    }
  }
}
