<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\States;
use App\Cities;
use App\Currency;
use App\Suppliers;
use App\UserRights;
use App\Restaurants;
use App\Restaurants_log;
use App\RestaurantMenuCategory;
use App\RestaurantFood;
use App\RestaurantFood_log;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use PDF;
use Session;
use Cookie;
use Mail;
class FoodController extends Controller
{
	private function rights($menu)
    {
        $emp_id=session()->get('travel_users_id');
        $right_array=array();
        $employees=Users::where('users_id',$emp_id)->where('users_pid',0)->where('users_status',1)->first();
        if(!empty($employees))
        {
            $right_array['add']=1;
            $right_array['view']=1;
            $right_array['edit_delete']=1;
            $right_array['report']=1;
            $right_array['admin']=1;
            $right_array['admin_which']="add,view,edit_delete,report";
        }
        else
        {

            $employees=Users::where('users_id',$emp_id)->where('users_status',1)->first();
            if(!empty($employees))
            {
                $user_rights=UserRights::where('emp_id',$emp_id)->where('menu',$menu)->first();
                if(!empty($user_rights))
                {
                    $right_array['add']=$user_rights->add_status;
                    $right_array['view']=$user_rights->view_status;
                    $right_array['edit_delete']=$user_rights->edit_del_status;
                    $right_array['report']=$user_rights->report_status;
                    $right_array['admin']=$user_rights->admin_status;
                    if($user_rights->admin_which_status!="")
                        $right_array['admin_which']=$user_rights->admin_which_status;
                    else
                        $right_array['admin_which']="No";
                }
                else
                {
                    $right_array['add']=0;
                    $right_array['view']=0;
                    $right_array['edit_delete']=0;
                    $right_array['report']=0;
                    $right_array['admin']=0;
                    $right_array['admin_which']="No";
                }
            }
            else
            {
                $right_array['add']=0;
                $right_array['view']=0;
                $right_array['edit_delete']=0;
                $right_array['report']=0;
                $right_array['admin']=0;
                $right_array['admin_which']="No";
            }

        }

        return $right_array;

    }

public function food_management(Request $request)
{
    if(session()->has('travel_users_id'))
  {
    $emp_id=session()->get('travel_users_id');
    $rights=$this->rights('food-management');
    if(strpos($rights['admin_which'],'add')!==false || strpos($rights['admin_which'],'view')!==false)
    {
      $get_food=RestaurantFood::orderBy('restaurant_food_id','asc')->get();
    }
    else
    {
      $get_food=RestaurantFood::where('food_created_by',$emp_id)->where('food_role','!=','Supplier')->orderBy('restaurant_food_id','asc')->get();
    }
    return view('mains.restaurant-food-management')->with(compact('get_food','rights'));
  }
  else
  {
    return redirect()->route('index');
  }
}
public function create_food(Request $request)
{
  if(session()->has('travel_users_id'))
      {
    $rights=$this->rights('food-management');
    $fetch_restaurants=Restaurants::get();
     $menu_categories=RestaurantMenuCategory::get();
    return view('mains.create-restaurant-food')->with(compact('fetch_restaurants','menu_categories','rights'));
  }
  else
  {
    return redirect()->route('index');
  }
}

public function insert_food(Request $request)
{
    // echo "<pre>";
    // print_r($request->all());
    // die();
  $food_role=$request->get('food_role');
  if($food_role=='supplier')
  {
    $user_id=$request->get('supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $restaurant=$request->get('restaurant');
  $menu_category=$request->get('menu_category');
  $food_name=$request->get('food_name');
  $food_price=$request->get('food_price');
  $food_discounted_price=$request->get('food_discounted_price');
  $food_unit=$request->get('food_unit');
  $food_package_count=$request->get('food_package_count');
  $food_available_for_delivery=$request->get('food_available_for_delivery');
   $food_featured=$request->get('food_featured');
  $food_ingredients=$request->get('food_ingredients');
  $validity_fromdate=$request->get('validity_operation_from');
  $validity_todate=$request->get('validity_operation_to');
  $food_description=$request->get('food_description');
  $food_created_by=$user_id;
  $food_images=array();
//multifile uploading
  if($request->hasFile('upload_ativity_images'))
  {
    foreach($request->file('upload_ativity_images') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_restaurant = "food-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/food_images/';
        $file->move($dir1, $image_restaurant);
        $food_images[]=$image_restaurant;
      }
    }
  }
  $food_images=serialize($food_images);

  $insert_restaurant=new RestaurantFood;
  $insert_restaurant->restaurant_id_fk=$restaurant; 
  $insert_restaurant->menu_category_id_fk=$menu_category;
  $insert_restaurant->food_name=$food_name;
  $insert_restaurant->food_price=$food_price;
  $insert_restaurant->food_discounted_price=$food_discounted_price;
  $insert_restaurant->food_description=$food_description;
  $insert_restaurant->food_ingredients=$food_ingredients;
  $insert_restaurant->food_unit=$food_unit;
  $insert_restaurant->food_package_count=$food_package_count;
  $insert_restaurant->validity_fromdate=$validity_fromdate;
  $insert_restaurant->validity_todate=$validity_todate;
  $insert_restaurant->food_featured=$food_featured;
  $insert_restaurant->food_available_for_delivery=$food_available_for_delivery;
  $insert_restaurant->food_images=$food_images;
  $insert_restaurant->food_created_by=$food_created_by;
  $insert_restaurant->food_role=$user_role;
  if($user_role=="Supplier")
  {
    $insert_restaurant->food_approval_status=0;
  }
  if($insert_restaurant->save())
  {

    $last_id=$insert_restaurant->id;
    $insert_restaurant_log=new RestaurantFood_log;
    $insert_restaurant_log->restaurant_food_id=$last_id;
    $insert_restaurant_log->restaurant_id_fk=$restaurant; 
    $insert_restaurant_log->menu_category_id_fk=$menu_category;
    $insert_restaurant_log->food_name=$food_name;
    $insert_restaurant_log->food_price=$food_price;
    $insert_restaurant_log->food_discounted_price=$food_discounted_price;
    $insert_restaurant_log->food_description=$food_description;
    $insert_restaurant_log->food_ingredients=$food_ingredients;
    $insert_restaurant_log->food_unit=$food_unit;
    $insert_restaurant_log->food_package_count=$food_package_count;
    $insert_restaurant_log->validity_fromdate=$validity_fromdate;
    $insert_restaurant_log->validity_todate=$validity_todate;
    $insert_restaurant_log->food_featured=$food_featured;
    $insert_restaurant_log->food_available_for_delivery=$food_available_for_delivery;
    $insert_restaurant_log->food_images=$food_images;
    $insert_restaurant_log->food_created_by=$food_created_by;
    $insert_restaurant_log->food_role=$user_role;
    $insert_restaurant_log->food_operation_performed="INSERT";
    $insert_restaurant_log->save();
    echo "success";
  }
  else
  {
    echo "fail";
  }
}


public function edit_food($food_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('food-management');
    $emp_id=session()->get('travel_users_id');
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_food=RestaurantFood::where('restaurant_food_id',$food_id)->first();
    }
    else
    {
      $get_food=RestaurantFood::where('restaurant_food_id',$food_id)->where('food_created_by',$emp_id)->where('food_role','!=','Supplier')->first();
    }

    if(!empty($get_food))
    {
       $fetch_restaurants=Restaurants::get();
     $menu_categories=RestaurantMenuCategory::get();

      return view('mains.edit-restaurant-food')->with(compact('get_food','fetch_restaurants','menu_categories','rights'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}

public function update_food (Request $request)
{
  // echo "<pre>";
  //   print_r($request->all());
  //   die();
   $food_id=urldecode(base64_decode(base64_decode($request->get('food_id'))));
  $food_role=$request->get('food_role');
  if($food_role=='supplier')
  {
    $user_id=$request->get('supplier_name');
    $user_role='Supplier';
  }
  else
  {
    if(session()->get('travel_users_role')=="Admin")
    {
      $user_role='Admin';
    }
    else
    {
      $user_role='Sub-User';
    }
    $user_id=session()->has('travel_users_id');
  }
  $restaurant=$request->get('restaurant');
  $menu_category=$request->get('menu_category');
  $food_name=$request->get('food_name');
  $food_price=$request->get('food_price');
  $food_discounted_price=$request->get('food_discounted_price');
  $food_unit=$request->get('food_unit');
  $food_package_count=$request->get('food_package_count');
  $food_available_for_delivery=$request->get('food_available_for_delivery');
   $food_featured=$request->get('food_featured');
  $food_ingredients=$request->get('food_ingredients');
  $validity_fromdate=$request->get('validity_operation_from');
  $validity_todate=$request->get('validity_operation_to');
  $food_description=$request->get('food_description');
  $food_created_by=$user_id;
  if(!empty($request->get('upload_ativity_already_images')))
  {
   $food_images=$request->get('upload_ativity_already_images');
  }
  else
  {
  $food_images=array();
  }
//multifile uploading
  if($request->hasFile('upload_ativity_images'))
  {
    foreach($request->file('upload_ativity_images') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_restaurant = "food-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/food_images/';
        $file->move($dir1, $image_restaurant);
        $food_images[]=$image_restaurant;
      }
    }
  }
  $food_images=serialize($food_images);

  $update_food_array=array("restaurant_id_fk"=>$restaurant, 
  "menu_category_id_fk"=>$menu_category,
  "food_name"=>$food_name,
  "food_price"=>$food_price,
  "food_discounted_price"=>$food_discounted_price,
  "food_description"=>$food_description,
  "food_ingredients"=>$food_ingredients,
  "food_unit"=>$food_unit,
  "food_package_count"=>$food_package_count,
  "validity_fromdate"=>$validity_fromdate,
  "validity_todate"=>$validity_todate,
  "food_featured"=>$food_featured,
  "food_available_for_delivery"=>$food_available_for_delivery,
  "food_images"=>$food_images);
  
  $update=RestaurantFood::where('restaurant_food_id',$food_id)->update($update_food_array);
  if($update)
  {
    $insert_restaurant_log=new RestaurantFood_log;
    $insert_restaurant_log->restaurant_food_id=$food_id;
    $insert_restaurant_log->restaurant_id_fk=$restaurant; 
    $insert_restaurant_log->menu_category_id_fk=$menu_category;
    $insert_restaurant_log->food_name=$food_name;
    $insert_restaurant_log->food_price=$food_price;
    $insert_restaurant_log->food_discounted_price=$food_discounted_price;
    $insert_restaurant_log->food_description=$food_description;
    $insert_restaurant_log->food_ingredients=$food_ingredients;
    $insert_restaurant_log->food_unit=$food_unit;
    $insert_restaurant_log->food_package_count=$food_package_count;
    $insert_restaurant_log->validity_fromdate=$validity_fromdate;
    $insert_restaurant_log->validity_todate=$validity_todate;
    $insert_restaurant_log->food_featured=$food_featured;
    $insert_restaurant_log->food_available_for_delivery=$food_available_for_delivery;
    $insert_restaurant_log->food_images=$food_images;
    $insert_restaurant_log->food_created_by=$food_created_by;
    $insert_restaurant_log->food_role=$user_role;
    $insert_restaurant_log->food_operation_performed="UPDATE";
    $insert_restaurant_log->save();
    echo "success";
  }
  else
  {
    echo "fail";
  }
}


public function food_details($food_id)
{
  if(session()->has('travel_users_id'))
  {
    $rights=$this->rights('food-management');
    $emp_id=session()->get('travel_users_id');
    if(strpos($rights['admin_which'],'edit_delete')!==false)
    {
      $get_food=RestaurantFood::where('restaurant_food_id',$food_id)->first();
    }
    else
    {
      $get_food=RestaurantFood::where('restaurant_food_id',$food_id)->where('food_created_by',$emp_id)->where('food_role','!=','Supplier')->first();
    }

    if(!empty($get_food))
    {
       $fetch_restaurants=Restaurants::get();
     $menu_categories=RestaurantMenuCategory::get();

      return view('mains.restaurant-food-detail-view')->with(compact('get_food','fetch_restaurants','menu_categories','rights'));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
    return redirect()->route('index');
  }
}


public function update_food_approval(Request $request)
{
  $id=$request->get('restaurant_food_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="approve")
  {
    $update_restaurant=RestaurantFood::where('restaurant_food_id',$id)->update(["food_approval_status"=>1]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="reject")
  {
    $update_restaurant=RestaurantFood::where('restaurant_food_id',$id)->update(["food_approval_status"=>2]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}

  public function update_food_active_inactive(Request $request)
{
  $id=$request->get('restaurant_food_id');
  $action_perform=$request->get('action_perform');
  if($action_perform=="active")
  {
    $update_restaurant=RestaurantFood::where('restaurant_food_id',$id)->update(["food_status"=>1]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else if($action_perform=="inactive")
  {
    $update_restaurant=RestaurantFood::where('restaurant_food_id',$id)->update(["food_status"=>0]);
    if($update_restaurant)
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  else
  {
    echo "fail";
  }
}
}