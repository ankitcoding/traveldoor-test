<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\Cities;
use App\Currency;
use App\UserRights;
use App\Agents;
use App\Agents_log;
use App\Suppliers;
use App\Activities;
use App\Transport;
use App\Hotels;
use App\HotelRooms;
use App\HotelRoomSeasons;
use App\HotelRoomSeasonOccupancy;
use App\SavedItinerary;
use App\Bookings;
use App\Guides;
use App\Drivers;
use App\GuideExpense;
use App\Languages;
use App\SightSeeing;
use App\Vehicles;
use App\VehicleType;
use App\HotelType;
use App\TourType;
use App\ActivityType;
use App\AirportMaster;
use App\Amenities;
use App\SubAmenities;
use App\Transfers;
use App\TransferDetails;
use App\AgentSavedItinerary;
use App\HotelMeal;
use App\AgentWallet;
use App\BookingInstallment;
use App\Restaurants;
use App\RestaurantFood;
use App\RestaurantMenuCategory;
use App\RestaurantType;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use App\OnlinePayment;
use PDF;
use Session;
use Cookie;
use Mail;
class AgentController extends Controller
{
  private $currencyApiKey;
  private $base_currency;
  public function __construct()
  {
    date_default_timezone_set('Asia/Dubai');
    $this->currencyApiKey="f4f2d4f26341429dcf7e";
     $this->base_currency="GEL";
  }
  private function agent_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_service_markup;
    }
    return $markup;
  }

  private function agent_own_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_own_service_markup;
    }
    return $markup;
  }
  public function agent_index()
  {

    if(session()->has('travel_agent_id'))
    {
      return redirect()->route('agent-home');
    }
    else
    {
       return view('agent.index');
    }

  }
  public function agent_login_check(Request $request)
  {
    $username=$request->get('username');
    $password=$request->get('password');
    $check_users=Agents::where('company_email',$username)->where('agent_password',md5($password))->first();
    if($check_users)
    {
      if($check_users->agent_status=='1')
      {
        session()->put('travel_agent_id',$check_users->agent_id);
        session()->put('travel_email_agent',$check_users->company_email);
        session()->put('travel_agent_fullame',$check_users->agent_name);
        session()->put('travel_agent_companyname',$check_users->company_name);
        session()->put('travel_agent_country',$check_users->agent_opr_countries);
        session()->put('travel_agent_type',$check_users->agent_service_type);
        session()->put('travel_agent_logo',$check_users->agent_logo);
        session()->put('travel_users_role',"Agent");
        echo "success";
      }
      else
      {
        echo "inactive";
      }
    }
    else
    {
      echo "fail";
    }
  }
//logout
  public function agent_logout(Request $request)
  {
    Session::forget('travel_agent_id');
    Session::forget('travel_email_agent');
    Session::forget('travel_agent_fullame');
    Session::forget('travel_agent_companyname');
    Session::forget('travel_agent_country');
    Session::forget('travel_users_role');
    if(!Session::has('travel_agent_id'))
    {
      return redirect()->intended('agent');
    }
  }
  public function register_agent(Request $request)
  {
    if(session()->has('travel_agent_id'))
    {
      return view('agent.home');
    }
    else
    {
      $countries=Countries::get();
      $countries_operation=Countries::get();
      $currency=Currency::get();
      return view('agent.register-agent')->with(compact('countries','countries_operation','currency'));
    }
  }
  public function insert_agent(Request $request)
  {
    $agent_name=$request->get('agent_name');
    $company_name=$request->get('company_name');
    $email_id=$request->get('email_id');
    $contact_number=$request->get('contact_number');
    $check_supplier=Agents::where('company_email',$email_id)->orWhere('company_contact',$contact_number)->get();
    if(count($check_supplier)>0)
    {
      echo "exist";
    }
    else
    {
      $fax_number=$request->get('fax_number');
      $agent_reference_id=$request->get('agent_reference_id');
      $address=$request->get('address');
      $agent_country=$request->get('agent_country');
      $agent_city=$request->get('agent_city');
      $corporate_reg_no=$request->get('corporate_reg_no');
      $corporate_description=$request->get('corporate_description');
      $skype_id=$request->get('skype_id');
      $operating_hrs_from=$request->get('operating_hrs_from');
      $operating_hrs_to=$request->get('operating_hrs_to');
      $week_monday=$request->get('week_monday');
      $week_tuesday=$request->get('week_tuesday');
      $week_wednesday=$request->get('week_wednesday');
      $week_thursday=$request->get('week_thursday');
      $week_friday=$request->get('week_friday');
      $week_saturday=$request->get('week_saturday');
      $week_sunday=$request->get('week_sunday');
      $agent_opr_currency=$request->get('agent_opr_currency');
      $agent_opr_countries=$request->get('agent_opr_countries');
      $account_number=$request->get('account_number');
      $bank_name=$request->get('bank_name');
      $bank_ifsc=$request->get('bank_ifsc');
      $bank_iban=$request->get('bank_iban');
      $bank_currency=$request->get('bank_currency');
      $service_type=$request->get('service_type');
      $contact_person_name=$request->get('contact_person_name');
      $contact_person_number=$request->get('contact_person_number');
      $contact_person_email=$request->get('contact_person_email');
      $agent_certificate_file=$request->get('agent_certificate_file');
      $agent_logo_file=$request->get('agent_logo_file');
      if($request->hasFile('agent_certificate_file'))
      {
        $agent_certificate_file=$request->file('agent_certificate_file');
        $extension=strtolower($request->agent_certificate_file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
        {
          $certificate_agent = "certificate-".time().'.'.$request->file('agent_certificate_file')->getClientOriginalExtension();
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
          $dir = 'assets/uploads/agent_certificates/';
          $request->file('agent_certificate_file')->move($dir, $certificate_agent);
        }
        else
        {
          $certificate_agent = "";
        }
      }
      else
      {
        $certificate_agent = "";
      }
      if($request->hasFile('agent_logo_file'))
      {
        $agent_logo_file=$request->file('agent_logo_file');
        $extension=strtolower($request->agent_logo_file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
        {
          $logo_agents = "logo-".time().'.'.$request->file('agent_logo_file')->getClientOriginalExtension();
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
          $dir1 = 'assets/uploads/agent_logos/';
          $request->file('agent_logo_file')->move($dir1, $logo_agents);
        }
        else
        {
          $logo_agents = "";
        }
      }
      else
      {
        $logo_agents = "";
      }
      $operating_weekdays=array("monday"=>$week_monday,
        "tuesday"=>$week_tuesday,
        "wednesday"=>$week_wednesday,
        "thursday"=>$week_thursday,
        "friday"=>$week_friday,
        "saturday"=>$week_saturday,
        "sunday"=>$week_sunday);
      $operating_weekdays=serialize($operating_weekdays);
      $agent_opr_currency=implode(",",$agent_opr_currency);
      $agent_opr_countries=implode(",",$agent_opr_countries);
      $service_type=implode(",",$service_type);
      $agent_bank_details=array();
      for($bank_count=0;$bank_count<count($account_number);$bank_count++)
      {
        $agent_bank_details[$bank_count]['account_number']=$account_number[$bank_count];
        $agent_bank_details[$bank_count]['bank_name']=$bank_name[$bank_count];
        $agent_bank_details[$bank_count]['bank_ifsc']=$bank_ifsc[$bank_count];
        $agent_bank_details[$bank_count]['bank_iban']=$bank_iban[$bank_count];
        $agent_bank_details[$bank_count]['bank_currency']=$bank_currency[$bank_count];
      }
      $contact_persons=array();
      for($contact_count=0;$contact_count<count($contact_person_name);$contact_count++)
      {
        $contact_persons[$contact_count]['contact_person_name']=$contact_person_name[$contact_count];
        $contact_persons[$contact_count]['contact_person_number']=$contact_person_number[$contact_count];
        $contact_persons[$contact_count]['contact_person_email']=$contact_person_email[$contact_count];
      }
      $agent_password_hint=$request->get('acc_password');
      $agent_password=md5($agent_password_hint);
      $agent_bank_details=serialize($agent_bank_details);
      $contact_persons=serialize($contact_persons);
      $agent=new Agents;
      $agent->agent_name=$agent_name;
      $agent->company_name=$company_name;
      $agent->company_email=$email_id;
      $agent->agent_password=$agent_password;
      $agent->agent_password_hint=$agent_password_hint;
      $agent->company_contact=$contact_number;
      $agent->company_fax=$fax_number;
      $agent->agent_ref_id=$agent_reference_id;
      $agent->address=$address;
      $agent->agent_country=$agent_country;
      $agent->agent_city=$agent_city;
      $agent->corporate_reg_no=$corporate_reg_no;
      $agent->corporate_desc=$corporate_description;
      $agent->skype_id=$skype_id;
      $agent->operating_hrs_from=$operating_hrs_from;
      $agent->operating_hrs_to=$operating_hrs_to;
      $agent->operating_weekdays=$operating_weekdays;
      $agent->certificate_corp=$certificate_agent;
      $agent->agent_logo=$logo_agents;
      $agent->agent_opr_currency=$agent_opr_currency;
      $agent->agent_opr_countries=$agent_opr_countries;
      $agent->agent_bank_details=$agent_bank_details;
      $agent->agent_service_type=$service_type;
      $agent->contact_persons=$contact_persons;
      $agent->agent_status=0;
      if($agent->save())
      {
        echo "success";
      }
      else
      {
        echo "fail";
      }
    }
  }
  public function agent_home(Request $request)
  {
    if(session()->has('travel_agent_id'))
    {
      // $countries=Countries::get();
      $agent_id=session()->get('travel_agent_id');
      // $get_itinerary=SavedItinerary::where('itinerary_status',1)->paginate(9);
      // $get_activities=Activities::where('activity_status',1)->paginate(9);
      // $get_transport=Transport::where('transfer_status',1)->paginate(9);
      // $get_hotels=Hotels::where('hotel_status',1)->get();
      // $hotel_meal=HotelMeal::get();
      // return view('agent.home')->with(compact('countries','agent_id','get_itinerary','get_activities','get_transport','get_hotels','hotel_meal'));
       return view('agent.home')->with(compact('agent_id'));
    }
    else
    {
      return redirect()->route('agent');
    }
  }
  public function agent_profile(Request $request)
  {
    if(session()->has('travel_agent_id'))
    {
      $agent_id=session()->get('travel_agent_id');
      $currency=Currency::get();
      $countries=Countries::get();
      $get_agent=Agents::where('agent_id',$agent_id)->first();
      if($get_agent)
      {
        return view('agent.agent-profile')->with(compact('get_agent','countries','currency'));
      }
      else
      {
        return redirect()->back();
      }
    }
    else
    {
      return redirect()->route('agent');
    }
  }

  public function agent_profile_edit(Request $request)
{
 if(session()->has('travel_agent_id'))
 {
  $agent_id=session()->get('travel_agent_id');
  $currency=Currency::get();
  $countries=Countries::where('country_status',1)->get();
$countries_operation=Countries::get();
  $get_agent=Agents::where('agent_id',$agent_id)->first();
  $agent_id=base64_encode(base64_encode($agent_id));
  if($get_agent)
  {
    $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_agent->agent_country)->select("cities.*")->orderBy('cities.name','asc')->get();
    return view('agent.agent-profile-edit')->with(compact('get_agent','countries','countries_operation','currency','cities'))->with('agent_id',$agent_id);
  }
  else
  {
    return redirect()->back();
  }
}
else
{
  return redirect()->route('index');
}
}

public function agent_profile_update(Request $request)
{
  $created_by=session()->get('travel_users_id');
  $agent_id=urldecode(base64_decode(base64_decode($request->get('agent_id'))));

      $check_agent=Agents::where('agent_id',$agent_id)->first();
      if(!$check_agent)
      {
        echo "fail";
      }
      else
      {
        $certificate_data=$check_agent->certificate_corp;
        $logo_data=$check_agent->agent_logo;

        $agent_name=$request->get('agent_name');
        $company_name=$request->get('company_name');
        $email_id=$request->get('email_id');
        $contact_number=$request->get('contact_number');
        $fax_number=$request->get('fax_number');
        $check_agent_exist=Agents::where('agent_id','!=',$agent_id)->where(function($query) use($email_id,$contact_number){

          $query->where('company_email',$email_id)->orWhere('company_contact',$contact_number);

        })->get();
        if(count($check_agent_exist)>0)
        {

          echo "exist";
        }
        else
        {
          $agent_reference_id=$request->get('agent_reference_id');
          $address=$request->get('address');
          $agent_country=$request->get('agent_country');
          $agent_city=$request->get('agent_city');
          $corporate_reg_no=$request->get('corporate_reg_no');
          $corporate_description=$request->get('corporate_description');
          $skype_id=$request->get('skype_id');
          $operating_hrs_from=$request->get('operating_hrs_from');
          $operating_hrs_to=$request->get('operating_hrs_to');
          $week_monday=$request->get('week_monday');  
          $week_tuesday=$request->get('week_tuesday');
          $week_wednesday=$request->get('week_wednesday');
          $week_thursday=$request->get('week_thursday');
          $week_friday=$request->get('week_friday');
          $week_saturday=$request->get('week_saturday');
          $week_sunday=$request->get('week_sunday');
          $agent_opr_currency=$request->get('agent_opr_currency');
          $agent_opr_countries=$request->get('agent_opr_countries');
          $account_number=$request->get('account_number');
          $bank_name=$request->get('bank_name');
          $bank_ifsc=$request->get('bank_ifsc');
          $bank_iban=$request->get('bank_iban');
          $bank_currency=$request->get('bank_currency');
          $service_type=$request->get('service_type');
          $contact_person_name=$request->get('contact_person_name');
          $contact_person_number=$request->get('contact_person_number');  
          $contact_person_email=$request->get('contact_person_email');
          $agent_certificate_file=$request->get('agent_certificate_file');
          $agent_logo_file=$request->get('agent_logo_file');

          if($request->hasFile('agent_certificate_file'))
          {
            $agent_certificate_file=$request->file('agent_certificate_file');
            $extension=strtolower($request->agent_certificate_file->getClientOriginalExtension());
            if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
            {
              $certificate_agent = "certificate-".time().'.'.$request->file('agent_certificate_file')->getClientOriginalExtension();

                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
              $dir = 'assets/uploads/agent_certificates/';

              $request->file('agent_certificate_file')->move($dir, $certificate_agent);
            }
            else
            {
              $certificate_agent = "";
            }
          }
          else
          {
            $certificate_agent=$certificate_data;
          }


          if($request->hasFile('agent_logo_file'))
          {
            $agent_logo_file=$request->file('agent_logo_file');
            $extension=strtolower($request->agent_logo_file->getClientOriginalExtension());
            if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
            {
              $logo_agent = "logo-".time().'.'.$request->file('agent_logo_file')->getClientOriginalExtension();

                    // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
              $dir1 = 'assets/uploads/agent_logos/';

              $request->file('agent_logo_file')->move($dir1, $logo_agent);
            }
            else
            {
              $logo_agent = "";
            }
          }
          else
          {
            $logo_agent=$logo_data;
          }


          $operating_weekdays=array("monday"=>$week_monday,
            "tuesday"=>$week_tuesday,
            "wednesday"=>$week_wednesday,
            "thursday"=>$week_thursday,
            "friday"=>$week_friday,
            "saturday"=>$week_saturday,
            "sunday"=>$week_sunday);

          $operating_weekdays=serialize($operating_weekdays);
          $agent_opr_currency=implode(",",$agent_opr_currency);
          $agent_opr_countries=implode(",",$agent_opr_countries);

          $service_type=implode(",",$service_type);

                 //agent markup new code
          //      if($request->has('service_name'))
          //      {
          //       $service_name=$request->get('service_name');

          //       $service_cost=$request->get('service_cost');

          //       $agent_service_markup="";
          //       for($markup_count=0;$markup_count<count($service_name);$markup_count++)

          //       {

          //         $agent_service_markup.=$service_name[$markup_count]."---".$service_cost[$markup_count]."///";
          //     }


          // }
          // else
          // {
          //     $agent_service_markup="";
          // }
      //end of agent markup new code


          $agent_bank_details=array();
          for($bank_count=0;$bank_count<count($account_number);$bank_count++)
          {
            $agent_bank_details[$bank_count]['account_number']=$account_number[$bank_count];
            $agent_bank_details[$bank_count]['bank_name']=$bank_name[$bank_count];
            $agent_bank_details[$bank_count]['bank_ifsc']=$bank_ifsc[$bank_count];
            $agent_bank_details[$bank_count]['bank_iban']=$bank_iban[$bank_count];
            $agent_bank_details[$bank_count]['bank_currency']=$bank_currency[$bank_count];

          }
          $contact_persons=array();
          for($contact_count=0;$contact_count<count($contact_person_name);$contact_count++)
          {
            $contact_persons[$contact_count]['contact_person_name']=$contact_person_name[$contact_count];
            $contact_persons[$contact_count]['contact_person_number']=$contact_person_number[$contact_count];
            $contact_persons[$contact_count]['contact_person_email']=$contact_person_email[$contact_count];

          }

          $agent_bank_details=serialize($agent_bank_details);
          $contact_persons=serialize($contact_persons);

          $update_data=array("agent_name"=>$agent_name,
            "company_name"=>$company_name,
            "company_email"=>$email_id,
            "company_contact"=>$contact_number,
            "company_fax"=>$fax_number,
            // "agent_ref_id"=>$agent_reference_id,
            "address"=>$address,
            "agent_country"=>$agent_country,
            "agent_city"=>$agent_city,
            "corporate_reg_no"=>$corporate_reg_no,
            "corporate_desc"=>$corporate_description,
            "skype_id"=>$skype_id,  
            "operating_hrs_from"=>$operating_hrs_from,
            "operating_hrs_to"=>$operating_hrs_to,  
            "operating_weekdays"=>$operating_hrs_to,
            "operating_weekdays"=>$operating_weekdays,
            "certificate_corp"=>$certificate_agent,  
            "agent_logo"=>$logo_agent,
            "agent_opr_currency"=>$agent_opr_currency,
            "agent_opr_countries"=>$agent_opr_countries,  
            "agent_bank_details"=>$agent_bank_details,
            "agent_service_type"=>$service_type,
                    // "agent_service_markup"=>$agent_service_markup,
            "contact_persons"=>$contact_persons
          );


          $update_query=Agents::where('agent_id',$agent_id)->update($update_data);
          if($update_query)
          {
            $agent_log=new Agents_log;
            $agent_log->agent_id=$agent_id;
            $agent_log->agent_name=$agent_name;
            $agent_log->company_name=$company_name;
            $agent_log->company_email=$email_id;
            $agent_log->company_contact=$contact_number;
            $agent_log->company_fax=$fax_number;
            $agent_log->agent_ref_id=$agent_reference_id;
            $agent_log->address=$address;
            $agent_log->agent_country=$agent_country;
            $agent_log->agent_city=$agent_city;
            $agent_log->corporate_reg_no=$corporate_reg_no;
            $agent_log->corporate_desc=$corporate_description;
            $agent_log->skype_id=$skype_id; 
            $agent_log->operating_hrs_from=$operating_hrs_from;
            $agent_log->operating_hrs_to=$operating_hrs_to; 
            $agent_log->operating_weekdays=$operating_hrs_to;
            $agent_log->operating_weekdays=$operating_weekdays;
            $agent_log->certificate_corp=$certificate_agent;  
            $agent_log->agent_logo=$logo_agent;
            $agent_log->agent_opr_currency=$agent_opr_currency;
            $agent_log->agent_opr_countries=$agent_opr_countries; 
            $agent_log->agent_bank_details=$agent_bank_details;
            $agent_log->agent_service_type=$service_type;
                // $agent_log->agent_service_markup=$agent_service_markup;
            $agent_log->contact_persons=$contact_persons; 
            $agent_log->agent_created_by=$created_by;
            $agent_log->agent_created_role="Agent";
            $agent_log->agent_operation_performed="UPDATE";

            $agent_log->save();

        session()->put('travel_agent_id',$agent_id);
        session()->put('travel_email_agent',$email_id);
        session()->put('travel_agent_fullame',$agent_name);
        session()->put('travel_agent_companyname',$company_name);
        session()->put('travel_agent_country',$agent_opr_countries);
        session()->put('travel_agent_type',$service_type);
        session()->put('travel_agent_logo',$logo_agent);


            echo "success";
          }
          else
          {
            echo "fail";
          }
        }
      }

}

public function enter_agent_markup()
{
  if(session()->has('travel_agent_id'))
  {
   $agent_id=session()->get('travel_agent_id');
   $fetch_agent=Agents::where('agent_id',$agent_id)->first();
   return view('agent.agent-markup')->with(compact('agent_id','fetch_agent'));
 }
 else
 {
  return redirect()->route('agent');
}

}

public function agent_markup_update(Request $request)
{
  $agent_id=session()->get('travel_agent_id');
  $agent_service_markup="";
      //agent markup new code
  if($request->has('service_name'))
  {
    $service_name=$request->get('service_name');

    $service_cost=$request->get('service_cost');

    $agent_service_markup="";
    for($markup_count=0;$markup_count<count($service_name);$markup_count++)
    {

      $agent_service_markup.=$service_name[$markup_count]."---".$service_cost[$markup_count]."///";
    }


  }
  else
  {
    $agent_service_markup="";
  }
      //end of agent markup new code

  $update_data=array("agent_own_service_markup"=>$agent_service_markup,
);


  $update_query=Agents::where('agent_id',$agent_id)->update($update_data);
  if($update_query)
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }

}

 
  
  public function itinerary_search(Request $request)
  {
    if(session()->has('travel_agent_id'))
    {
      $countries=Countries::where('country_status',1)->get();
      $agent_id=session()->get('travel_agent_id');
      $get_itinerary=SavedItinerary::where('itinerary_status',1)->get();
      return view('agent.itinerary-search')->with(compact('countries','agent_id','get_itinerary'));
    }
    else
    {
      return redirect()->route('agent');
    }
  }
  public function transportation_search()
  {
    if(session()->has('travel_agent_id'))
    {
      $countries=Countries::get();
      $agent_id=session()->get('travel_agent_id');
      return view('agent.transportation-search')->with(compact('countries','agent_id'));
    }
    else
    {
      return redirect()->route('agent');
    }
  }

  public static function fetchGuidesCost($sightseeing_id,$guide_id,$vehicle_selected,$sightseeing_date)
  {

  
    $get_sightseeing_detail=Sightseeing::where('sightseeing_id',$sightseeing_id)->first();
    $to_city="";
    if(!empty($get_sightseeing_detail))
    {
      $to_city=$get_sightseeing_detail->sightseeing_city_to;
    }
    $date_from=$sightseeing_date;
    $date_to=$sightseeing_date;
    $guides=Guides::where('guide_id',$guide_id)->where('guide_status',1)->orderBy('guide_id','desc')->first();


      $get_tour_cost=unserialize($guides->guide_tours_cost);

      $price=0;
      foreach($get_tour_cost as $tour_cost)
      {
        if($tour_cost['tour_name']==$sightseeing_id)
        {
          $cost_count=0;
          foreach($tour_cost['tour_vehicle_name'] as $vehicle_names)
          {
            if($vehicle_names==$vehicle_selected)
            {

              $price=$tour_cost['tour_guide_cost'][$cost_count];
              $actual_price=$tour_cost['tour_guide_cost'][$cost_count];
          
               if($guides->guide_city!=$to_city && $to_city!="")
               {
                $price+=$guides->guide_hotel_cost ? $guides->guide_hotel_cost : 0;
               
                $price+=$guides->guide_food_cost ? $guides->guide_food_cost : 0;
            

              }
              $guide_cost_vehicle_selected=$price;
               $operating_days=unserialize($guides->operating_weekdays);
              $operating_days_array=array();
              foreach($operating_days as $key=>$days)
              {
                if($days=="Yes")
                {
                  $operating_days_array[]= ucwords($key);
                }
              }
              $check_blackout_during_dates=0;
              $get_dates_from=strtotime($date_from);
              $get_dates_to=strtotime($date_to);
              $blackout_days=explode(',',$guides->guide_blackout_dates);
              $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;
              $dates="";
              if($check_blackout_during_dates<=0)
              {


               $get_guide_booking_count=Bookings::where('booking_type','guide')->where(function ($q) use($date_from,$date_to){
                $q->where(function ($q1) use($date_from){ 

                  $q1->where('booking_selected_date',"<=",$date_from)
                  ->where('booking_selected_to_date',">=",$date_from);
                })->orWhere(function ($q2) use($date_to){ 

                  $q2->where('booking_selected_date',"<=",$date_to)
                  ->where('booking_selected_to_date',">=",$date_to);
                });

              })->where('booking_type_id',$guides->guide_id)->where('booking_admin_status','!=',2)->get();
               $past_bookings_array=$get_guide_booking_count->toArray();

                if(count($past_bookings_array)<=0 && ($guide_cost_vehicle_selected!="" && $guide_cost_vehicle_selected>0) && ($actual_price>0 && $actual_price!=""))
                {
                    return $price;

           }
        }
      }
    }
  }
}

  
       return 0;     
  

  }
  

  public static function fetchDriversCost($sightseeing_id,$driver_id,$vehicle_selected,$sightseeing_date)
  {
     $get_sightseeing_detail=Sightseeing::where('sightseeing_id',$sightseeing_id)->first();
    $to_city="";
    if(!empty($get_sightseeing_detail))
    {
      $to_city=$get_sightseeing_detail->sightseeing_city_to;
    }
    $date_from=$sightseeing_date;
    $date_to=$sightseeing_date;
    $drivers=Drivers::where('driver_id',$driver_id)->where('driver_status',1)->orderBy('driver_id','desc')->first();


      $get_tour_cost=unserialize($drivers->driver_tours_cost);

      $price=0;
      foreach($get_tour_cost as $tour_cost)
      {
        if($tour_cost['tour_name']==$sightseeing_id)
        {
          $cost_count=0;
          foreach($tour_cost['tour_vehicle_name'] as $vehicle_names)
          {
            if($vehicle_names==$vehicle_selected)
            {

              $price=$tour_cost['tour_driver_cost'][$cost_count];
               $actual_price=$tour_cost['tour_driver_cost'][$cost_count];
          
               if($drivers->driver_city!=$to_city && $to_city!="")
               {
                $price+=$drivers->driver_hotel_cost ? $drivers->driver_hotel_cost : 0;
               
                $price+=$drivers->driver_food_cost ? $drivers->driver_food_cost : 0;
            

              }
              $driver_cost_vehicle_selected=$price;
               $operating_days=unserialize($drivers->operating_weekdays);
              $operating_days_array=array();
              foreach($operating_days as $key=>$days)
              {
                if($days=="Yes")
                {
                  $operating_days_array[]= ucwords($key);
                }
              }
              $check_blackout_during_dates=0;
              $get_dates_from=strtotime($date_from);
              $get_dates_to=strtotime($date_to);
              $blackout_days=explode(',',$drivers->driver_blackout_dates);
              $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;
              $dates="";
              if($check_blackout_during_dates<=0)
              {


               $get_driver_booking_count=Bookings::where('booking_type','driver')->where(function ($q) use($date_from,$date_to){
                $q->where(function ($q1) use($date_from){ 

                  $q1->where('booking_selected_date',"<=",$date_from)
                  ->where('booking_selected_to_date',">=",$date_from);
                })->orWhere(function ($q2) use($date_to){ 

                  $q2->where('booking_selected_date',"<=",$date_to)
                  ->where('booking_selected_to_date',">=",$date_to);
                });

              })->where('booking_type_id',$drivers->driver_id)->where('booking_admin_status','!=',2)->get();
               $past_bookings_array=$get_driver_booking_count->toArray();

                if(count($past_bookings_array)<=0 && ($driver_cost_vehicle_selected!="" && $driver_cost_vehicle_selected>0) && ($actual_price>0 && $actual_price!=""))
                {
                    return $price;

          }
        }
      }
    }
  }
}
  
       return 0;   
  }
 
 
 
  public function fetchItinerary(Request $request)
  {
    $markup=$this->agent_markup();
    $itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
    $own_markup=$this->agent_own_markup();
    $own_itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $own_itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
    $country_id=$request->get('country_id');
    $city_id=$request->get('city_id');
    $date_from=$request->get('date_from');
    $no_of_nights=$request->get('no_of_nights');
    session()->put('itinerary_country_id',$country_id);
    session()->put('itinerary_city_id',$city_id);
    session()->put('itinerary_date_from',$date_from);
    session()->put('no_of_nights',$no_of_nights);
    $fetch_itinerary=SavedItinerary::whereRaw("FIND_IN_SET(".$country_id.",itinerary_package_countries)")->whereRaw("FIND_IN_SET(".$city_id.",itinerary_package_cities)")->where('itinerary_validity_operation_from','<=',$date_from)->where('itinerary_validity_operation_to','>=',$date_from);

    if($no_of_nights!=0)
    {
      $fetch_itinerary=$fetch_itinerary->where('itinerary_tour_days',$no_of_nights);
    }
    $fetch_itinerary=$fetch_itinerary->orderBy(\DB::raw('-`itinerary_show_order`'), 'desc')->where('itinerary_status',1)->orderBy('itinerary_id','desc')->get();

    $html='
    <style>
    button.book-btn {
      background: red;
      border: none;
      border-radius: 50px;
      margin-top: 120px;
      padding: 10px 20px;
      text-align: center;
      color: white;
      width: 160px;
      cursor:pointer;
    }
    </style>
    <div class="row">';
    foreach($fetch_itinerary as $itinerary)
    {
      $price=$itinerary->itinerary_total_cost;
      $markup_cost=round(($price*$itinerary_markup)/100);
      $total_agent_cost=round(($price+$markup_cost));

      $own_itinerary_markup_cost=round(($total_agent_cost*$own_itinerary_markup)/100);
        $total_cost=round(($total_agent_cost+$own_itinerary_markup_cost));

      $newid=$itinerary->itinerary_id;
      $html.='<div class="col-md-12" style="margin:0 0 25px">
      <div class="hotel-list-div">
      <div class="hotel-div">
      <div class="hotel-img-div">
      <div class="flexslider2">
      <ul class="slides">';
      $services_provided=array();
      $itinerary_image=unserialize($itinerary->itinerary_image);

      $fetch_image=unserialize($itinerary->itinerary_package_services);
      $itnerary_sightseeing_image_id=$fetch_image[0]['sightseeing']['sightseeing_id'];
      $itnerary_activity_image_id=$fetch_image[0]['activity']['activity_id'][0];
      $itnerary_hotel_image_id=$fetch_image[0]['hotel']['hotel_id'];
      $hotel_array=array_column($fetch_image, 'hotel');
      $activity_array=array_column($fetch_image, 'activity');
      $sightseeing_array=array_column($fetch_image, 'sightseeing');
      $transfer_array=array_column($fetch_image, 'transfer');
      if(array_keys($hotel_array,"hotel_id") !== false)
        $services_provided[]="hotels";
      if(array_keys($activity_array,"activity_id") !== false)
        $services_provided[]="activity";
      if(array_filter($sightseeing_array))
        $services_provided[]="sightseeing";
      if(array_filter($transfer_array))
      {
        $services_provided[]="transfer";
      }

      if($itinerary_image!=null && !empty($itinerary_image))
      {

      if(!empty($itinerary_image))
      {
        $html.='<img class="img-slide" src="'.asset("assets/uploads/itinerary_images")."/".$itinerary_image[0].'" alt="slide" />';
      }
      else
      {
        $html.='<img class="img-slide" src="'.asset("assets/images/no-photo.png").'" alt="slide"  />';
      }

      }
      else if(!empty($itnerary_sightseeing_image_id))
      {
        $fetch_sightseeing=SightSeeing::where('sightseeing_id',$itnerary_sightseeing_image_id)->first();
        $fetch_sightseeing_image=unserialize($fetch_sightseeing['sightseeing_images']);

      if(!empty($fetch_sightseeing_image))
      {
        $html.='<img class="img-slide" src="'.asset("assets/uploads/sightseeing_images")."/".$fetch_sightseeing_image[0].'" alt="slide" />';
      }
      else
      {
        $html.='<img class="img-slide" src="'.asset("assets/images/no-photo.png").'" alt="slide"  />';
      }
       
      }
      else if(!empty($itnerary_activity_image_id))
      {
        $fetch_activity=Activities::where('activity_id',$itnerary_activity_image_id)->first();
        $fetch_activity_image=unserialize($fetch_activity['activity_images']);

        if(!empty($fetch_activity_image))
      {
          $html.='<img class="img-slide" src="'.asset("assets/uploads/activity_images")."/".$fetch_activity_image[0].'" alt="slide" />';
      }
      else
      {
        $html.='<img class="img-slide" src="'.asset("assets/images/no-photo.png").'" alt="slide"  />';
      }

      
      }
      else
      {
        $fetch_hotel=Hotels::where('hotel_id',$itnerary_hotel_image_id)->first();
        $fetch_hotel_image=unserialize($fetch_hotel['hotel_images']);
         if(!empty($fetch_hotel_image))
      {
          $html.='<img class="img-slide" src="'.asset("assets/uploads/hotel_images")."/".$fetch_hotel_image[0].'" alt="slide" />';
      }
      else
      {
        $html.='<img class="img-slide" src="'.asset("assets/images/no-photo.png").'"  alt="slide"  />';
      }
   
      }
      $html.='</ul>
      </div>
      </div>
      <div class="hotel-details">
      <div class="heading-div">
      <p class="hotel-name">'.$itinerary->itinerary_tour_name.'</p>
      </div>
      <div class="heading-div">
      <span>'.($itinerary->itinerary_tour_days+1).' Days</span>
      </div>
      <p class="info" style="display:none">
      </p>
      <p class="time-info">'.substr($itinerary->itinerary_tour_description,0,120).'</p>
      <style>
      .tourmaster-tour-info.tourmaster-tour-info-duration-text p {
        width: 70%;
      }
      .tourmaster-tour-grid.tourmaster-tour-frame .tourmaster-tour-price-wrap {
        top: 0 !IMPORTANT;
        right: 25px;
      }
      </style>
      <div class="inclusions">
      <span style="color: #009688;
      font-size: 16px;">Included at this price</span>
      <br>';
      if(in_array("hotels",$services_provided))
        $html.='<span class="inclusion-item"><i class="fa fa-bed"></i> Hotels</span>';
      if(in_array("activity",$services_provided))
        $html.='<span class="inclusion-item"><i class="fa fa-star-o"></i> Activities</span>';
      if(in_array("sightseeing",$services_provided))
        $html.='<span class="inclusion-item"><i class="fa fa-tripadvisor"></i> SightSeeing</span>';
      if(in_array("transfer",$services_provided))
        $html.='<span class="inclusion-item"><i class="fa fa-car"></i> Transfers</span>';
      $html.='</div>
      </div>
      </div>
      <div class="hotel-info-div">
      <p class="offer">GEL '.$total_cost.'</p>
      <p class="days" style="display:none">Price Per Person</p>
      <a href="'.route('itinerary-details-view',['itinerary_id'=>$newid]).'" target="_blank"><button class="book-btn">More Details</button></a>
      </div>
      </div>
      </div>';
    }
    if(preg_match('/slides/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }
  }
  public function itinerary_get_hotels(Request $request)
  {

     $limit=12;
    if($request->has('offset'))
    {
        $offset=$request->get('offset');
    }
    else
    {
        $offset=0;
    }

    $country_id=$request->get('country_id');
    $city_id=$request->get('city_id');
     $hotel_checkin=$request->get('hotel_checkin');
      $hotel_current_id=$request->get('hotel_current_id');
   $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id)->where('hotel_id','!=',$hotel_current_id)->where('hotel_approve_status',1)->where('hotel_status',1)->where('booking_validity_from',"<=",$hotel_checkin)->where('booking_validity_to',">=",$hotel_checkin)->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->offset($offset*$limit)->take($limit)->orderBy('hotel_id','desc')->get();


  $room_currency_array=array("USD");
      $room_currency_array=array_unique($room_currency_array);
      
      $currency_price_array=array();
      foreach($room_currency_array as $currency)
      {
           $new_currency=$currency;
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);
         
      }

      $currency_price_array["GEL"]=1;



$html="";
      $html_current="";
      

    if($offset==0)
    {
       
          $html_header='<style>
    .card-body .card-title {
      margin-bottom: 0;
      border-bottom: none;
      text-overflow: ellipsis !important;
      white-space: nowrap;
      color: black;
      overflow: hidden;
      font-size: 14px;
    }
    .card {
      border-radius: 0px;
      box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
      margin-bottom: 30px !important;
      min-height: 230px;
    }
    .card-img-top {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    div.card.sold {
    
    cursor: not-allowed;
    pointer-events: none;
    -webkit-appearance: none;
}
.sold-para {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    text-align: center;
    color: #ff2626;
    font-size: 30px;
    padding: 65px 0;
    z-index: 9999;
    opacity: 1;
    pointer-events: none;
    background: #ffffff94;
}
    .more-btn{
      border: none !important;
      background: no-repeat;
      float: right;
      color: #2196F3;
      font-size: 12px;
      font-weight: 700;
    }
    .card-body {
      padding: 10px;
    }
    </style>
    <div class="row" id="hotel_sort_div">';


     $fetch_current_hotel=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id)->where('hotel_id',$hotel_current_id)->where('hotel_approve_status',1)->where('hotel_status',1)->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','desc')->get();

       foreach($fetch_current_hotel as $hotels)
    {

          $room_price_array=array();


     $hotel_rooms=HotelRooms::where('hotel_id',$hotels->hotel_id)->get();

     $hotel_currency=$hotels->hotel_currency;


     $room_min_price=0;
     foreach($hotel_rooms as $rooms_value)
     {

      if($hotel_currency==null)
      {
        $hotel_currency=$rooms_value->hotel_room_currency;
      }
      $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$rooms_value->hotel_room_id)->where('hotel_room_season_validity_from','<=',$hotel_checkin)->where('hotel_room_season_validity_to','>=',$hotel_checkin)->get();

      foreach($get_hotel_seasons as $hotel_seasons)
      {
        $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();

        foreach($get_occupancy as $occupancy)
        {
          if($room_min_price==0)
          {
             $room_min_price=$occupancy->hotel_room_occupancy_price;
          }
          else if($occupancy->hotel_room_occupancy_price<$room_min_price)
          {
            $room_min_price=$occupancy->hotel_room_occupancy_price;
          }
        }
      }
     }

      $new_currency="";
       if($room_min_price>0)
      {

        $price=$room_min_price;
       if(isset($currency_price_array[$hotel_currency]))
        {
        $price=round($room_min_price*$currency_price_array[$hotel_currency],2);
        }
        else
        {
          $new_currency= $hotel_currency;
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
         $price=round($price*$conversion_price,2);
          
        }




        $total_cost=round($price);

      $newid=base64_encode($hotels->hotel_id);
      $hotelimage=unserialize($hotels->hotel_images);
      $style="";
      if($hotels['hotel_id']==$hotel_current_id)
      {
         $style="style='border:8px solid #9c27b0;'";
         $html_current.='<div class="col-md-3 select_hotel" id="hotel__'.$hotels['hotel_id'].'">
      <div class="card" '.$style.'>
      <div style="position:relative">
      <!--<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">-->';
      if(!empty($hotelimage))
      {
        $html_current.='  <img class="card-img-top cstm-img" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'" alt="Hotel Image" style="width:222px;height:110px">';
      }
      else
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Hotel Image" style="width:222px;height:110px">';
      }

     $html_current.=' 
      </div>
      <div class="card-body">

      <span class="card-rating">';
      for($i=1;$i<=5;$i++)
      {
        if($i<=$hotels['hotel_rating'])
        {
          $html_current.="<i class='fa fa-star checked'></i>";
        }
        else
        {
          $html_current.="<i class='fa fa-star'></i>";
        }

      }
      $html_current.='</span>
      <span class="card-rating-text" style="display:none">'.$hotels['hotel_rating'].'</span>
      <h5 class="card-title">'.$hotels['hotel_name'].' <br> <small>'.$hotels['hotel_address'].'</small></h5>
      <span><small>Price/Room</small></span><br>
      <span class="card-price">GEL '.$total_cost.'</span>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <button class="more-btn">Details</button>
      <input type="hidden" class="search_hotel_cost" name="search_hotel_cost__'.$hotels['hotel_id'].'" value="'.$total_cost.'">
      <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_address'].'">';
       if(!empty($hotelimage))
      {
        $html_current.=' <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'">';
      }
      else
      {
        $html_current.='<input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.asset("assets/images/no-photo.png").'">';
      }

       $html_current.='
      <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_rating'].'">
      </div>
      </div>
      </div>';

      }
      
    }
  }

  }
  else
  {

    $html_header="";
  }
    
    foreach($fetch_hotels as $hotels)
    {

     //    $hotel_season_details=unserialize($hotels->hotel_season_details);

     //   $rate_allocation_details=unserialize($hotels->rate_allocation_details);


     //  $room_price_array=array();
     // $room_currency_array=array();
     //  for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
     //  {

     //    if($hotel_season_details[$rate_allocation_count]['booking_validity_from']<=$hotel_checkin && $hotel_season_details[$rate_allocation_count]['booking_validity_to']>=$hotel_checkin)
     //    {

     //    }
     //    else
     //    {
     //      continue;
     //    }
     //    for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
     //    {
     //      $room_price_array[]=$rate_allocation_details[$rate_allocation_count]['room_max'][$room_count];
     //       $room_currency_array[]=$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count];

     //    }
     //  }


          $room_price_array=array();

     $hotel_rooms=HotelRooms::where('hotel_id',$hotels->hotel_id)->get();

     $hotel_currency=$hotels->hotel_currency;


     $room_min_price=0;
     foreach($hotel_rooms as $rooms_value)
     {

      if($hotel_currency==null)
      {
        $hotel_currency=$rooms_value->hotel_room_currency;
      }
      $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$rooms_value->hotel_room_id)->where('hotel_room_season_validity_from','<=',$hotel_checkin)->where('hotel_room_season_validity_to','>=',$hotel_checkin)->get();

      foreach($get_hotel_seasons as $hotel_seasons)
      {
        $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();

        foreach($get_occupancy as $occupancy)
        {
          if($room_min_price==0)
          {
             $room_min_price=$occupancy->hotel_room_occupancy_price;
          }
          else if($occupancy->hotel_room_occupancy_price<$room_min_price)
          {
            $room_min_price=$occupancy->hotel_room_occupancy_price;
          }
        }
      }
     }

      $new_currency="";
       if($room_min_price>0)
      {

        $price=$room_min_price;

         if(isset($currency_price_array[$hotel_currency]))
        {
        $price=round($room_min_price*$currency_price_array[$hotel_currency],2);
        }
        else
        {
          $new_currency= $hotel_currency;
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
         $price=round($price*$conversion_price,2);
          
        }




        $total_cost=round($price);

      $newid=base64_encode($hotels->hotel_id);
      $hotelimage=unserialize($hotels->hotel_images);
      $style="";
      if($hotels['hotel_id']==$hotel_current_id)
      {
         $style="style='border:8px solid #9c27b0;'";
         $html_current.='<div class="col-md-3 select_hotel" id="hotel__'.$hotels['hotel_id'].'">
      <div class="card" '.$style.'>
      <div style="position:relative">
      <!--<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">-->';
      if(!empty($hotelimage))
      {
        $html_current.='  <img class="card-img-top cstm-img" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'" alt="Hotel Image" style="width:222px;height:110px">';
      }
      else
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Hotel Image" style="width:222px;height:110px">';
      }

     $html_current.=' 
      </div>
      <div class="card-body">

      <span class="card-rating">';
      for($i=1;$i<=5;$i++)
      {
        if($i<=$hotels['hotel_rating'])
        {
          $html_current.="<i class='fa fa-star checked'></i>";
        }
        else
        {
          $html_current.="<i class='fa fa-star'></i>";
        }

      }
      $html_current.='</span>
      <span class="card-rating-text" style="display:none">'.$hotels['hotel_rating'].'</span>
      <h5 class="card-title">'.$hotels['hotel_name'].' <br> <small>'.$hotels['hotel_address'].'</small></h5>
      <span><small>Price/Room</small></span><br>
      <span class="card-price">GEL '.$total_cost.'</span>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <button class="more-btn">Details</button>
      <input type="hidden" class="search_hotel_cost" name="search_hotel_cost__'.$hotels['hotel_id'].'" value="'.$total_cost.'">
      <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_address'].'">';
       if(!empty($hotelimage))
      {
        $html_current.=' <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'">';
      }
      else
      {
        $html_current.='<input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.asset("assets/images/no-photo.png").'">';
      }

       $html_current.='
      <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_rating'].'">
      </div>
      </div>
      </div>';

      }
      else
      {

        if($hotels['booking_validity_from']<=$hotel_checkin && $hotels['booking_validity_to']>=$hotel_checkin)
{
  $html.='<div class="col-md-3 select_hotel" id="hotel__'.$hotels['hotel_id'].'">';
   $html.='<div class="card">';
}
else
{
    $html.='<div class="col-md-3" id="hotel__'.$hotels['hotel_id'].'">';
    $html.='<div class="card sold">
      <div class="sold-para">SOLD OUT</div>';

}

       
     $html.='
      <div style="position:relative">
      <!--<img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">-->';
      if(!empty($hotelimage))
      {
        $html.='  <img class="card-img-top cstm-img" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'" alt="Hotel Image" style="width:222px;height:110px">';
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Hotel Image" style="width:222px;height:110px">';
      }

     $html.=' 
      </div>
      <div class="card-body">

      <span class="card-rating">';
      for($i=1;$i<=5;$i++)
      {
        if($i<=$hotels['hotel_rating'])
        {
          $html.="<i class='fa fa-star checked'></i>";
        }
        else
        {
          $html.="<i class='fa fa-star'></i>";
        }

      }
      $html.='</span>
      <span class="card-rating-text" style="display:none">'.$hotels['hotel_rating'].'</span>
      <h5 class="card-title">'.$hotels['hotel_name'].' <br> <small>'.$hotels['hotel_address'].'</small></h5>
      <span><small>Price/Room</small></span><br>
      <span class="card-price">GEL '.$total_cost.'</span>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <button class="more-btn">Details</button>
      <input type="hidden" class="search_hotel_cost" name="search_hotel_cost__'.$hotels['hotel_id'].'" value="'.$total_cost.'">
      <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_address'].'">';
       if(!empty($hotelimage))
      {
        $html.=' <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'">';
      }
      else
      {
        $html.='<input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.asset("assets/images/no-photo.png").'">';
      }

       $html.='
      <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_rating'].'">
      </div>
      </div></div>';
      }
     
    }
    }
    echo $html_header." ".$html_current." ".$html;
        //     $html='
        //     <style>
        //     button.book-btn {
        //       background: red;
        //       border: none;
        //       border-radius: 50px;
        //       margin-top: 120px;
        //       padding: 10px 20px;
        //       text-align: center;
        //       color: white;
        //       width: 160px;
        //     }
        //     h4.card-title
        //     {
        //       font-size:14px !important
        //     }
        // .cstm-img{
        //       max-height: 132px;
        //     height: 132px;
        //     object-fit: cover;
        // }
        // img.tick {
        //     position: absolute;
        //     width: auto;
        //    height: 40px;
        //     top: 0;
        //     left: 0;
        // }
        //     </style>
        //     <div class="row">';
          //     foreach($fetch_hotels as $hotels)
          //     {
          //       $rate_allocation_details=unserialize($hotels->rate_allocation_details);
          //       $price=min($rate_allocation_details[0]['room_max']);
          //       $room_currency=$rate_allocation_details[0]['room_currency'][0];
          //       $total_cost=round($price,2);
          //       $newid=base64_encode($hotels->hotel_id);
          //       $hotelimage=unserialize($hotels->hotel_images);
          //       $html.='<div class="col-md-6 select_hotel" id="hotel__'.$hotels['hotel_id'].'">
            //       <div class="card">
              //       <div style="position:relative">
                //       <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">
                //   <img class="card-img-top cstm-img" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'" alt="Hotel Image">
              //   </div>
              //   <div class="card-body">
                //     <h4 class="card-title">'.$hotels['hotel_name'].'</h4>
                //     <input type="hidden" class="search_hotel_cost" name="search_hotel_cost__'.$hotels['hotel_id'].'" value="'.$total_cost.'">
                //     <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_address'].'">
                //     <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$hotels['hotel_id'].'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[0].'">
                //       <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$hotels['hotel_id'].'" value="'.$hotels['hotel_rating'].'">
                //     <div class="row" style="margin-top:20px">
                  //                         <div class="col-md-12">
                    //                         <table class="table" id="room_type_table">
                      //                             <thead>
                        //                                 <tr>
                          //                                     <th>Select</th>
                          //                                     <th>Room Type</th>
                          //                                     <th>Price per Night</th>
                        //                                 </tr>
                      //                             </thead>
                      //                             <tbody>';
                        //                         $hotel_season_details=unserialize($hotels->hotel_season_details);
                        //                         $hotel_blackout_dates=unserialize($hotels->hotel_blackout_dates);
                        //                         $rate_allocation_details=unserialize($hotels->rate_allocation_details);

                        //                                 $room_counter=0;
                        //                                 for($rate_allocation_count=0;$rate_allocation_count< count($hotels->hotel_season_details);$rate_allocation_count++)
                        //                                 {

                        //                                     for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
                        //                                     {


                        //                                           $html.='<tr>
                          //                                     <td>
                            //                                          <input type="radio"
                            //                                                 class="with-gap radio-col-primary" name="room_type_'.$hotels['hotel_id'].'" id="room_'.$room_counter.'_'.$hotels['hotel_id'].'" value="'.$room_counter.'" class="room_type_change" >
                            //                                             <label for="room_'.$room_counter.'_'.$hotels['hotel_id'].'"></label>

                          //                                     </td>
                          //                                     <td>
                            //                                         <a href="#" class="details"><i class="fa fa-caret-right"></i>'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room ('.$rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count].' Adults + ';
                              //                                             if($rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count]=="")
                              //                                             {
                              //                                               $html.=' 0 ';
                              //                                             }
                              //                                             else
                              //                                             {
                              //                                               $html.=$rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count];
                              //                                             }

                              //                                         $html.='Child)<input type="hidden" name="room_name_'.$room_counter.'_'.$hotels['hotel_id'].'" value="'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room';
                            //                                             $html.='"></a>
                            //                                        <!--  <p>1 single bed <i class="fa fa-bed"></i></p> -->
                          //                                     </td>
                          //                                     <td>
                            //                                         <input type="hidden" name="room_price_'.$room_counter.'_'.$hotels['hotel_id'].'" value="'.$rate_allocation_details[$rate_allocation_count]['room_max'][$room_count].'">';
                            //                                         $total_cost=round($rate_allocation_details[$rate_allocation_count]['room_max'][$room_count],2);

                            //                                         $html.='
                            //                                          <input type="hidden" name="room_price_currency_'.$room_counter.'_'.$hotels['hotel_id'].'" value="'.$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count].'">
                            //                                         <span class="show-price">'.$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count].' '.$total_cost.'</span>
                          //                                     </td>
                        //                                 </tr>';
                        //                                         $room_counter++;
                        //                                     }
                        //                                 }


                      //                             $html.='</tbody>
                    //                         </table>
                  //                         </div>
                //                     </div>
                //       <a href="'.route('hotel-detail',["hotelid"=>base64_encode($hotels['hotel_id'])]).'" class=" btn btn-sm btn-primary" target="_blank">View Details</a>
              //     </div>
            //   </div>
          // </div>';
          //     }
          //     echo $html;
  }
  // public function itinerary_get_hotel_details(Request $request)
  // {
  //   $hotel_id=$request->get('hotel_id');
  //   $selected_no_of_rooms=$request->get('no_of_rooms');
  //    $checkin_date=$request->get('hotel_checkin');
  //     $checkout_date=$request->get('hotel_checkout');
  //   $html="";
  //   $fetch_hotel=Hotels::where('hotel_id',$hotel_id)->first();
  //   if($fetch_hotel)
  //   {
  //     $hotel_images=unserialize($fetch_hotel->hotel_images);
  //        $get_hotel_booking_count=Bookings::where('booking_type','hotel')->where(function ($q) use($checkin_date,$checkout_date){
  //   $q->where(function ($q1) use($checkin_date){ 

  //     $q1->where('booking_selected_date',"<=",$checkin_date)
  //     ->where('booking_selected_to_date',">=",$checkin_date);
  //   })->orWhere(function ($q2) use($checkout_date){ 

  //     $q2->where('booking_selected_date',"<=",$checkout_date)
  //     ->where('booking_selected_to_date',">=",$checkout_date);
  //   });

  //       })->where('booking_type_id',$fetch_hotel['hotel_id'])->where('booking_admin_status','!=',2)->get();
  //     $past_bookings_array=$get_hotel_booking_count->toArray();

  
  //                       $booking_room_with_qty=array();  
  //                       $count=0;
  //                       foreach($past_bookings_array as $past_bookings)
  //                       {
  //                           $other_info=unserialize($past_bookings['booking_other_info']);

  //                          for($i=0;$i< count($other_info);$i++)
  //                          {
  //                              $booking_room_with_qty[$count]['room_name']=$other_info[$i]['room_name'];
  //                               $booking_room_with_qty[$count]['room_quantity']=$other_info[$i]['room_qty'];
  //                               $count++;
                                
  //                          }

                           
  //                       }
  //                        if(count($booking_room_with_qty)>0)
  //                           {
  //                       $booking_room_with_qty = array_reduce($booking_room_with_qty, function ($a, $b) {
  //                               isset($a[$b['room_name']]) ? $a[$b['room_name']]['room_quantity'] += $b['room_quantity'] : $a[$b['room_name']] = $b;  
  //                               return $a;
  //                           });
                           
  //                                $booking_room_with_qty = array_values($booking_room_with_qty);
  //                           }
                       

                  
  //     $html.=' <style>
  //     .content {
  //       padding: 15px;
  //     }
  //     .checked {
  //       color: orange;
  //     }
  //     .carousel-inner img {
  //       width: 100%;
  //       height: 100%;
  //     }
  //     h2.title-name {
  //       color: #F44336;
  //       font-size: 24px;
  //       display: inline-block;
  //     }
  //     .rating-div {
  //       display: inline-block;
  //       margin-left: 10px;
  //     }
  //     p.sub.sub-title {
  //       color: #ffa500 !IMPORTANT;
  //       font-size: 15px;
  //       background: #ffffc0;
  //       display: inline-block;
  //       padding: 10px;
  //       border-radius: 5px;
  //     }
  //     .sub {
  //       color: gray !important;
  //       font-size: 15px !important;
  //     }
  //     .carousel-inner {
  //       border-radius: 6px;
  //     }
  //     div#demo {
  //       margin-top: 25px;
  //     }
  //     p.check-in {
  //       color: black;
  //       font-weight: 600;
  //       margin-bottom: 4px;
  //     }
  //     h2.title {
  //       font-size: 20px;
  //     }
  //     p.paragraph {
  //       color: #4E4E4E;
  //       font-weight: 400;
  //     }
  //     button.read-more {
  //       border: 0px;
  //       background: transparent;
  //       color: #008cff;
  //     }
  //     .blackfont {
  //       color: #000000;
  //       padding: 0px 6px;
  //       font-weight: 600;
  //     }
  //     ul.select-rooms {
  //       list-style-type: none;
  //       display: inline-flex;
  //       padding: 0px;
  //     }
  //     ul.select-rooms li {
  //       padding-left: 15px;
  //     }
  //     ul.select-rooms i {
  //       font-size: 12px;
  //       font-weight: normal;
  //       padding-right: 8px;
  //     }
  //     span.text-rt {
  //       float: right;
  //     }
  //     span.text-middle {
  //       padding-left: 9rem;
  //     }
  //     a.btn-include {
  //       border-radius: 34px;
  //       background-color: #ffffff;
  //       box-shadow: 0 1px 7px 0 rgba(0, 0, 0, 0.2);
  //       display: inline-block;
  //       flex-shrink: 0;
  //       color: #008cff;
  //       text-transform: uppercase;
  //       font-size: 12px;
  //       font-weight: 700;
  //       padding: 9px 20px;
  //       cursor: pointer;
  //       outline: 0;
  //       border: 0;
  //       text-align: center;
  //       min-width: 90px;
  //     }
  //     .p-div{
  //       display: flex;
  //       justify-content: space-between;
  //     }
  //     .about {
  //       background: #cbfffa;
  //       color: #066b61;
  //       padding: 15px;
  //       margin-bottom: 15px;
  //     }
  //     .amenties {
  //       background: #d6ffd9;
  //       padding: 15px;
  //     }
  //     .blackfont {
  //       color: #33883a;
  //       padding: 0px 6px;
  //       font-weight: 600;
  //       font-size: 20px;
  //     }
  //     .amenties {
  //       background: #d6ffd9;
  //       padding: 15px;
  //       margin-bottom: 20px;
  //     }
  //     </style>
  //     <body>
  //     <div class="content">
  //     <h2 class="title-name">'.$fetch_hotel->hotel_name.'</h2>
  //     <div class="rating-div">';
  //     for($i=1;$i<=5;$i++)
  //     {
  //       if($i<=$fetch_hotel->hotel_rating)
  //       {
  //         $html.="<span class='fa fa-star checked'></span>";
  //       }
  //       else
  //       {
  //         $html.="<span class='fa fa-star'></span>";
  //       }

  //     }

  //     $html.='</div>
  //     <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_address.'">';
  //      if(!empty($hotel_images))
  //     {
  //       $html.=' <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$fetch_hotel->hotel_id.'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotel_images[0].'">';
  //     }
  //     else
  //     {
  //       $html.='<input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$fetch_hotel->hotel_id.'" value="'.asset("assets/images/no-photo.png").'">';
  //     }
  //     $html.='<input type="hidden" class="search_hotel_id" name="search_hotel_id__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_id.'">
  //     <input type="hidden" class="search_hotel_name" name="search_hotel_name__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_name.'">
  //     <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_rating.'">
  //      <input type="hidden" class="search_hotel_detail_link" name="search_hotel_detail_link__'.$fetch_hotel->hotel_id.'" value="'.route('hotel-detail',['hotelid'=>base64_encode($fetch_hotel->hotel_id),'itinerary'=>1]).'">
  //     <br>
  //     <p class="sub sub-title">'.$fetch_hotel->hotel_address.'</p>
  //     <div id="demo" class="carousel slide" data-ride="carousel">
  //     <!-- Indicators -->
  //     <ul class="carousel-indicators">
  //     ';
  //     if(!empty($hotel_images))
  //     {
  //       for($hotel_count=0;$hotel_count< count($hotel_images);$hotel_count++)
  //       {
  //         $html.='<li data-target="#demo" data-slide-to="'.$hotel_count.'"';
  //         if($hotel_count==0)
  //         {
  //           $html.='class="active"';
  //         }
  //         $html.='></li>';
  //       }
  //     }
  //     else
  //     {
  //       $html.='<li data-target="#demo" data-slide-to="0" class="active"></li>';
  //     }
  //     $html.='</ul>
  //     <div class="carousel-inner">';
  //     if(!empty($hotel_images))
  //     {
  //       for($hotel_count=0;$hotel_count< count($hotel_images);$hotel_count++)
  //       {
  //         $html.='<div class="carousel-item ';
  //         if($hotel_count==0)
  //         {
  //           $html.='active';
  //         }
  //         $html.='">
  //         <img src="'.asset('assets/uploads/hotel_images').'/'.$hotel_images[$hotel_count].'" alt="'.$hotel_images[$hotel_count].'" width="1100"
  //         height="500">
  //         </div>';
  //       }
  //     }
  //     else
  //     {
  //       $html.='<div class="carousel-item active">
  //       <img src="'.asset('assets/images/no-photo.png').'" width="1100" height="500">
  //       </div>';
  //     }
  //     $html.='</div>
  //     <!-- Left and right controls -->
  //     <a class="carousel-control-prev" href="#demo" data-slide="prev">
  //     <span class="carousel-control-prev-icon"></span>
  //     </a>
  //     <a class="carousel-control-next" href="#demo" data-slide="next">
  //     <span class="carousel-control-next-icon"></span>
  //     </a>
  //     </div>
  //     <hr class="h-hr">
  //     <div class="about py-4">
  //     <h2 class="title">About</h2>
  //     <p class="paragraph">'.$fetch_hotel->hotel_description.'</p>
  //     </div>';
  //     $get_amenities=unserialize($fetch_hotel->hotel_amenities);
  //     if($get_amenities==null || $get_amenities=="")
  //     {
  //       $html.="";
  //     }
  //     else
  //     {
  //       $html.='
  //       <div class="amenties">
  //       <p class="blackfont">Amenities</p>
  //       <div class="container">
  //       <div class="row">';
  //       foreach($get_amenities as $amenities)
  //       {
  //         if(!empty($amenities[1]))
  //         {
  //           $html.='<div class="col-md-4">
  //           <h3 class="amenty-title">';
  //           $get_amenities_name=LoginController::fetchAmenitiesName($amenities[0]);
  //           $html.=$get_amenities_name['amenities_name'];
  //           $html.='</h3>';
  //           foreach($amenities[1] as $sub_amenities)
  //           {
  //             $html.='<p class="amenty-item"><i class="fa fa-check"></i>';
  //             $get_sub_amenities_name=LoginController::fetchSubAmenitiesName($sub_amenities,$amenities[0]);
  //             $html.=$get_sub_amenities_name['sub_amenities_name'];
  //             $html.='</p>';
  //           }
  //           $html.='</div>';
  //         }
  //       }
  //       $html.='</div>
  //       </div>
  //       </div>';
  //     }
  //     $html.='<p class="blackfont">Select Rooms</p><hr>';


  //     $hotel_season_details=unserialize($fetch_hotel->hotel_season_details);

  //      $rate_allocation_details=unserialize($fetch_hotel->rate_allocation_details);
  //    $room_currency_array=array();
  //     for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
  //     {

  //       for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
  //       {
  //         $room_currency_array[]=$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count];

  //       }
  //     }


  //     $room_currency_array=array_unique($room_currency_array);
      
  //     $currency_price_array=array();
  //     foreach($room_currency_array as $currency)
  //     {
  //       if($currency=="GEL")
  //       {

  //       }
  //       else
  //       {
  //          $new_currency=$currency;

  //       //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
  //       //   $cURLConnection = curl_init();

  //       //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
  //       //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

  //       //   $convertedPrice = curl_exec($cURLConnection);
  //       //   curl_close($cURLConnection);

  //       //   $convertedPriceArray=json_decode($convertedPrice);
  //       //   $currency_attribute=$new_currency."_".$this->base_currency;

  //       //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
          
  //       //  $currency_price_array[$currency]=round($conversion_price,2);
  //         $conversion_price=0;
  //         $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
  //               $scriptDocument = new \DOMDocument();
  //               libxml_use_internal_errors(TRUE); //disable libxml errors
  //               if(!empty($fetch_html)){
  //                  //load
  //                  $scriptDocument->loadHTML($fetch_html);
                 
  //               //init DOMXPath
  //                $scriptDOMXPath = new \DOMXPath($scriptDocument);
  //                //get all the h2's with an id
  //                $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
  //                //check
  //                if($scriptRow->length > 0){
  //                  foreach($scriptRow as $row){
  //                          $conversion_price=round($row->nodeValue,2);
  //                  }
  //                }
                 
  //               }
  //                $currency_price_array[$currency]=round($conversion_price,2);
  //       }
         
  //     }

  //     $currency_price_array["GEL"]=1;


  //     $hotel_season_details=unserialize($fetch_hotel->hotel_season_details);
  //     $hotel_blackout_dates=unserialize($fetch_hotel->hotel_blackout_dates);
  //     $rate_allocation_details=unserialize($fetch_hotel->rate_allocation_details);
  //     $room_counter=0;
  //     for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
  //     {
  //       for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
  //       {
  //          $room_price_currency_rate=$currency_price_array[$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count]];
  //         $html.='<div class="row">
  //         <div class="col-md-12">
  //         <p class="p-div"><span class="text-lt">';
  //         $html.='<a href="#" class="details">'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room ('.$rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count].' Adults + ';
  //         if($rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count]=="")
  //         {
  //           $html.='0';
  //         }
  //         else
  //         {
  //           $html.=$rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count];
  //         }
  //         $html.=' Child)<input type="hidden" class="room_detail_name" name="room_name_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room';
  //         $room_name=ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room';
  //         $html.='"></a>
  //         </span>';
  //         $room_past_qty=0;
  //         foreach($booking_room_with_qty as $room_with_qty)
  //         {
  //           if($room_with_qty['room_name']==$room_name)
  //           {
  //             $room_past_qty=$room_with_qty['room_quantity'];
  //           }
  //         }


  //        $html.='<span class="text-middle"><i class="fa fa-check"></i> <input type="hidden" class="room_detail_price" name="room_price_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.round($rate_allocation_details[$rate_allocation_count]['room_max'][$room_count]*$room_price_currency_rate).'">';
  //         $total_cost=round($rate_allocation_details[$rate_allocation_count]['room_max'][$room_count]*$room_price_currency_rate);
  //         $html.='
  //         <input type="hidden" class="room_detail_currency" name="room_price_currency_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count].'">
  //         <span class="show-price">GEL '.$total_cost.'</span></span>
  //         <span class="text-rt">
  //         <!--<a href="#" class="btn-include room_type_change" name="room_type_'.$fetch_hotel->hotel_id.'" id="room_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.$room_counter.'">Include</a>-->';
  //          $room_qty=0; 
  //          if(!empty($rate_allocation_details[$rate_allocation_count]['room_qty'][$room_count]))
  //          {   
  //            $room_qty=$rate_allocation_details[$rate_allocation_count]['room_qty'][$room_count];
  //          }
  //          else
  //          {
  //            $room_qty=$selected_no_of_rooms;
  //          }
         
  //        if(($room_qty-$room_past_qty)>0)
  //        {                    
  //          $html.='<select name="room_selected_qty_'.$fetch_hotel->hotel_id.'" class="room_selected_qty form-control" id="roomselectedqty_'.$room_counter.'_'.$fetch_hotel->hotel_id.'">
  //          <option value="0">0</option>
  //          ';
  //          for($i=1;$i<=$room_qty-$room_past_qty;$i++)
  //          {
  //           $html.='<option value="'.$i.'">'.$i.'</option>';
  //         }
  //         $html.='</select>';
  //       }
  //       else
  //       {
  //         $html.='<button type="button" class="book-btn-sold" disabled="disabled">SOLD OUT</button>';
  //       }

  //         $html.='</span>
  //         </p>
  //         </div>
  //         </div>
  //         <hr>';
  //         $room_counter++;
  //       }
  //     }
  //     $html.='</div>';
  //   }
  //   else
  //   {
  //     $html="";
  //   }
  //   echo $html;
  // }

  public function itinerary_get_hotel_details(Request $request)
  {
    $hotel_id=$request->get('hotel_id');
    $selected_no_of_rooms=$request->get('no_of_rooms');
     $checkin_date=$request->get('hotel_checkin');
      $checkout_date=$request->get('hotel_checkout');
      $room_occupancy_ids=$request->get('room_occupancy_ids');
       $room_quantity_array=$request->get('room_quantity_array');

    $html="";
    $fetch_hotel=Hotels::where('hotel_id',$hotel_id)->first();
    if($fetch_hotel)
    {
      $hotel_images=unserialize($fetch_hotel->hotel_images);
         $get_hotel_booking_count=Bookings::where('booking_type','hotel')->where(function ($q) use($checkin_date,$checkout_date){
    $q->where(function ($q1) use($checkin_date){ 

      $q1->where('booking_selected_date',"<=",$checkin_date)
      ->where('booking_selected_to_date',">=",$checkin_date);
    })->orWhere(function ($q2) use($checkout_date){ 

      $q2->where('booking_selected_date',"<=",$checkout_date)
      ->where('booking_selected_to_date',">=",$checkout_date);
    });

        })->where('booking_type_id',$fetch_hotel['hotel_id'])->where('booking_admin_status','!=',2)->get();
      $past_bookings_array=$get_hotel_booking_count->toArray();

  
                        $booking_room_with_qty=array(); 
                        $count=0;
                        foreach($past_bookings_array as $past_bookings)
                        {
                           //  $other_info=unserialize($past_bookings['booking_other_info']);

                           // for($i=0;$i< count($other_info);$i++)
                           // {
                           //     $booking_room_with_qty[$count]['room_name']=$other_info[$i]['room_name'];
                           //      $booking_room_with_qty[$count]['room_quantity']=$other_info[$i]['room_qty'];
                           //      $count++;
                                
                           // }

                          $room_count_details=explode(",",$past_bookings['booking_rooms_count_details']);

                          for($index=0;$index< count($room_count_details);$index++)
                          {
                            $get_room_array=explode("---",$room_count_details[$index]);

                            $booking_room_with_qty[$index]['room_id']=$get_room_array[0];
                             $booking_room_with_qty[$index]['room_occupancy_id']=$get_room_array[1];
                            $booking_room_with_qty[$index]['room_qty']=$get_room_array[3];

                          }

                           
                        }
                        //  if(count($booking_room_with_qty)>0)
                        //     {
                        // $booking_room_with_qty = array_reduce($booking_room_with_qty, function ($a, $b) {
                        //         isset($a[$b['room_name']]) ? $a[$b['room_name']]['room_quantity'] += $b['room_quantity'] : $a[$b['room_name']] = $b;  
                        //         return $a;
                        //     });
                           
                        //          $booking_room_with_qty = array_values($booking_room_with_qty);
                        //     }
                       

                  
      $html.=' <style>
      .content {
        padding: 15px;
      }
      .checked {
        color: orange;
      }
      .carousel-inner img {
        width: 100%;
        height: 100%;
      }
      h2.title-name {
        color: #F44336;
        font-size: 24px;
        display: inline-block;
      }
      .rating-div {
        display: inline-block;
        margin-left: 10px;
      }
      p.sub.sub-title {
        color: #ffa500 !IMPORTANT;
        font-size: 15px;
        background: #ffffc0;
        display: inline-block;
        padding: 10px;
        border-radius: 5px;
      }
      .sub {
        color: gray !important;
        font-size: 15px !important;
      }
      .carousel-inner {
        border-radius: 6px;
      }
      div#demo {
        margin-top: 25px;
      }
      p.check-in {
        color: black;
        font-weight: 600;
        margin-bottom: 4px;
      }
      h2.title {
        font-size: 20px;
      }
      p.paragraph {
        color: #4E4E4E;
        font-weight: 400;
      }
      button.read-more {
        border: 0px;
        background: transparent;
        color: #008cff;
      }
      .blackfont {
        color: #000000;
        padding: 0px 6px;
        font-weight: 600;
      }
      ul.select-rooms {
        list-style-type: none;
        display: inline-flex;
        padding: 0px;
      }
      ul.select-rooms li {
        padding-left: 15px;
      }
      ul.select-rooms i {
        font-size: 12px;
        font-weight: normal;
        padding-right: 8px;
      }
      span.text-rt {
        float: right;
      }
      span.text-middle {
        padding-left: 9rem;
      }
      a.btn-include {
        border-radius: 34px;
        background-color: #ffffff;
        box-shadow: 0 1px 7px 0 rgba(0, 0, 0, 0.2);
        display: inline-block;
        flex-shrink: 0;
        color: #008cff;
        text-transform: uppercase;
        font-size: 12px;
        font-weight: 700;
        padding: 9px 20px;
        cursor: pointer;
        outline: 0;
        border: 0;
        text-align: center;
        min-width: 90px;
      }
      .p-div{
        display: flex;
        justify-content: space-between;
      }
      .about {
        background: #cbfffa;
        color: #066b61;
        padding: 15px;
        margin-bottom: 15px;
      }
      .amenties {
        background: #d6ffd9;
        padding: 15px;
      }
      .blackfont {
        color: #33883a;
        padding: 0px 6px;
        font-weight: 600;
        font-size: 20px;
      }
      .amenties {
        background: #d6ffd9;
        padding: 15px;
        margin-bottom: 20px;
      }
      </style>
      <body>
      <div class="content">
      <h2 class="title-name">'.$fetch_hotel->hotel_name.'</h2>
      <div class="rating-div">';
      for($i=1;$i<=5;$i++)
      {
        if($i<=$fetch_hotel->hotel_rating)
        {
          $html.="<span class='fa fa-star checked'></span>";
        }
        else
        {
          $html.="<span class='fa fa-star'></span>";
        }

      }

      $html.='</div>
      <input type="hidden" class="search_hotel_address" name="search_hotel_address__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_address.'">';
       if(!empty($hotel_images))
      {
        $html.=' <input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$fetch_hotel->hotel_id.'" value="'.asset("assets/uploads/hotel_images"). '/'.$hotel_images[0].'">';
      }
      else
      {
        $html.='<input type="hidden" class="search_hotel_image" name="search_hotel_image__'.$fetch_hotel->hotel_id.'" value="'.asset("assets/images/no-photo.png").'">';
      }
      $html.='<input type="hidden" class="search_hotel_id" name="search_hotel_id__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_id.'">
      <input type="hidden" class="search_hotel_name" name="search_hotel_name__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_name.'">
      <input type="hidden" class="search_hotel_rating" name="search_hotel_rating__'.$fetch_hotel->hotel_id.'" value="'.$fetch_hotel->hotel_rating.'">
       <input type="hidden" class="search_hotel_detail_link" name="search_hotel_detail_link__'.$fetch_hotel->hotel_id.'" value="'.route('hotel-detail',['hotelid'=>base64_encode($fetch_hotel->hotel_id),'itinerary'=>1]).'">
      <br>
      <p class="sub sub-title">'.$fetch_hotel->hotel_address.'</p>
      <div id="demo" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ul class="carousel-indicators">
      ';
      if(!empty($hotel_images))
      {
        for($hotel_count=0;$hotel_count< count($hotel_images);$hotel_count++)
        {
          $html.='<li data-target="#demo" data-slide-to="'.$hotel_count.'"';
          if($hotel_count==0)
          {
            $html.='class="active"';
          }
          $html.='></li>';
        }
      }
      else
      {
        $html.='<li data-target="#demo" data-slide-to="0" class="active"></li>';
      }
      $html.='</ul>
      <div class="carousel-inner">';
      if(!empty($hotel_images))
      {
        for($hotel_count=0;$hotel_count< count($hotel_images);$hotel_count++)
        {
          $html.='<div class="carousel-item ';
          if($hotel_count==0)
          {
            $html.='active';
          }
          $html.='">
          <img src="'.asset('assets/uploads/hotel_images').'/'.$hotel_images[$hotel_count].'" alt="'.$hotel_images[$hotel_count].'" width="1100"
          height="500">
          </div>';
        }
      }
      else
      {
        $html.='<div class="carousel-item active">
        <img src="'.asset('assets/images/no-photo.png').'" width="1100" height="500">
        </div>';
      }
      $html.='</div>
      <!-- Left and right controls -->
      <a class="carousel-control-prev" href="#demo" data-slide="prev">
      <span class="carousel-control-prev-icon"></span>
      </a>
      <a class="carousel-control-next" href="#demo" data-slide="next">
      <span class="carousel-control-next-icon"></span>
      </a>
      </div>
      <hr class="h-hr">
      <div class="about py-4">
      <h2 class="title">About</h2>
      <p class="paragraph">'.$fetch_hotel->hotel_description.'</p>
      </div>';
      $get_amenities=unserialize($fetch_hotel->hotel_amenities);
      if($get_amenities==null || $get_amenities=="")
      {
        $html.="";
      }
      else
      {
        $html.='
        <div class="amenties">
        <p class="blackfont">Amenities</p>
        <div class="container">
        <div class="row">';
        foreach($get_amenities as $amenities)
        {
          if(!empty($amenities[1]))
          {
            $html.='<div class="col-md-4">
            <h3 class="amenty-title">';
            $get_amenities_name=LoginController::fetchAmenitiesName($amenities[0]);
            $html.=$get_amenities_name['amenities_name'];
            $html.='</h3>';
            foreach($amenities[1] as $sub_amenities)
            {
              $html.='<p class="amenty-item"><i class="fa fa-check"></i>';
              $get_sub_amenities_name=LoginController::fetchSubAmenitiesName($sub_amenities,$amenities[0]);
              $html.=$get_sub_amenities_name['sub_amenities_name'];
              $html.='</p>';
            }
            $html.='</div>';
          }
        }
        $html.='</div>
        </div>
        </div>';
      }
      $html.='<p class="blackfont">Select Rooms</p><hr>';


    
     $hotel_rooms=HotelRooms::where('hotel_id',$fetch_hotel->hotel_id)->get();

     $hotel_currency=$fetch_hotel->hotel_currency;


     $room_min_price=0;
     foreach($hotel_rooms as $rooms_value)
     {

      if($hotel_currency==null)
      {
        $hotel_currency=$rooms_value->hotel_room_currency;
      }
    }
      
      $currency_price_array=array();
    
        if($hotel_currency!="GEL")
        {
           $new_currency=$hotel_currency;


          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$hotel_currency]=round($conversion_price,2);
        }
         

      $currency_price_array["GEL"]=1;


      // $hotel_season_details=unserialize($fetch_hotel->hotel_season_details);
      // $hotel_blackout_dates=unserialize($fetch_hotel->hotel_blackout_dates);
      // $rate_allocation_details=unserialize($fetch_hotel->rate_allocation_details);
      // $room_counter=0;
      // for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
      // {
      //   for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
      //   {
      //      $room_price_currency_rate=$currency_price_array[$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count]];
      //     $html.='<div class="row">
      //     <div class="col-md-12">
      //     <p class="p-div"><span class="text-lt">';
      //     $html.='<a href="#" class="details">'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room ('.$rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count].' Adults + ';
      //     if($rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count]=="")
      //     {
      //       $html.='0';
      //     }
      //     else
      //     {
      //       $html.=$rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count];
      //     }
      //     $html.=' Child)<input type="hidden" class="room_detail_name" name="room_name_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room';
      //     $room_name=ucwords($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]).' '.ucwords($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]).' Room';
      //     $html.='"></a>
      //     </span>';
      //     $room_past_qty=0;
      //     foreach($booking_room_with_qty as $room_with_qty)
      //     {
      //       if($room_with_qty['room_name']==$room_name)
      //       {
      //         $room_past_qty=$room_with_qty['room_quantity'];
      //       }
      //     }


      //    $html.='<span class="text-middle"><i class="fa fa-check"></i> <input type="hidden" class="room_detail_price" name="room_price_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.round($rate_allocation_details[$rate_allocation_count]['room_max'][$room_count]*$room_price_currency_rate).'">';
      //     $total_cost=round($rate_allocation_details[$rate_allocation_count]['room_max'][$room_count]*$room_price_currency_rate);
      //     $html.='
      //     <input type="hidden" class="room_detail_currency" name="room_price_currency_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count].'">
      //     <span class="show-price">GEL '.$total_cost.'</span></span>
      //     <span class="text-rt">
      //     <!--<a href="#" class="btn-include room_type_change" name="room_type_'.$fetch_hotel->hotel_id.'" id="room_'.$room_counter.'_'.$fetch_hotel->hotel_id.'" value="'.$room_counter.'">Include</a>-->';
      //      $room_qty=0; 
      //      if(!empty($rate_allocation_details[$rate_allocation_count]['room_qty'][$room_count]))
      //      {   
      //        $room_qty=$rate_allocation_details[$rate_allocation_count]['room_qty'][$room_count];
      //      }
      //      else
      //      {
      //        $room_qty=$selected_no_of_rooms;
      //      }
         
      //    if(($room_qty-$room_past_qty)>0)
      //    {                    
      //      $html.='<select name="room_selected_qty_'.$fetch_hotel->hotel_id.'" class="room_selected_qty form-control" id="roomselectedqty_'.$room_counter.'_'.$fetch_hotel->hotel_id.'">
      //      <option value="0">0</option>
      //      ';
      //      for($i=1;$i<=$room_qty-$room_past_qty;$i++)
      //      {
      //       $html.='<option value="'.$i.'">'.$i.'</option>';
      //     }
      //     $html.='</select>';
      //   }
      //   else
      //   {
      //     $html.='<button type="button" class="book-btn-sold" disabled="disabled">SOLD OUT</button>';
      //   }

      //     $html.='</span>
      //     </p>
      //     </div>
      //     </div>
      //     <hr>';
          // $room_counter++;
      //   }
      // }


    $hotel_rooms=HotelRooms::where('hotel_id',$fetch_hotel->hotel_id)->get();
    $hotel_meal=HotelMeal::get();
     $currency=$hotel_currency;
    $html.='<div class="row" style="margin-top:20px" >
        <div class="col-md-12" >
            <table class="table table-bordered" id="room_type_table">
                <thead>
                    <tr>
                        <th>Room Type</th>
                        <th>Sleeps/ Occupancy</th>
                        <th>Meal</th>
                        <th>Today\'s Price</th>
                        <th>Select Rooms</th>
                    </tr>
                </thead>
                <tbody>';
                 
                    foreach($hotel_rooms as $room_key=>$room_values)
                  {
                    if($currency==null)
                    {
                        $currency=$room_values->hotel_room_currency;
                    }
                    $room_price_currency_rate=$currency_price_array[$currency];
                     $html_rooms="";
                        $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$room_values->hotel_room_id)->where('hotel_room_season_validity_from','<=',$checkin_date)->where('hotel_room_season_validity_to','>=',$checkin_date)->get();
                    $sleeps_count=0;
                    $occupany_counter=0;

                        foreach($get_hotel_seasons as $hotel_seasons)
                        {

                            $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();
                             $sleeps_count+=count($get_occupancy);
                            foreach($get_occupancy as $occupancy)
                            {
                                if($occupany_counter!=0)
                                {
                                 $html_rooms.="<tr>"; 
                                }
                                 $html_rooms.='<td><input type="hidden" name="room_occupancy_id__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="'.$occupancy->hotel_room_occupancy_id.'"><input type="hidden" name="room_occupancy_qty__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="'.$occupancy->hotel_room_occupancy_qty.'">';
                                 for($sleeps=1;$sleeps<=$occupancy->hotel_room_occupancy_qty;$sleeps++)
                                 {
                                    $html_rooms.='<span class="fa fa-user"></span>';
                                 }


                                 $html_rooms.='</td>';
                                  foreach($hotel_meal as $meal)
                                  {
                                    if($meal->hotel_meals_id==$room_values->hotel_room_meal)
                                    {
                                      $html_rooms.="<td>$meal->hotel_meals_name</td>";  
                                    }
                                  }
                                   $rooms_occupancy_qty="";
                                    $total_cost=round($occupancy->hotel_room_occupancy_price*$room_price_currency_rate);
                                    if(in_array($occupancy->hotel_room_occupancy_id,$room_occupancy_ids))
                                    {
                                      $room_occupancy_ids_index=array_search($occupancy->hotel_room_occupancy_id,$room_occupancy_ids);
                                      if(array_key_exists($room_occupancy_ids_index, $room_quantity_array))
                                      {
                                          $rooms_occupancy_qty=$room_quantity_array[$room_occupancy_ids_index];
                                      }
                                    
                                    }
                                    $past_booking_qty=0;
                                    foreach($booking_room_with_qty as $room_with_qty_key=>$room_with_qty_value)
                                    {
                                      if($room_with_qty_value['room_id']==$room_values->hotel_room_id)
                                      {
                                        $past_booking_qty+=$room_with_qty_value['room_qty'];
                                      }

                                    }

                                  $html_rooms.='<td> <input type="hidden" name="room_price_convert_rate__'.$room_key.'__'.$fetch_hotel->hotel_id.'" value="'.$room_price_currency_rate.'"> <input type="hidden" name="room_price__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="'.$total_cost.'"><input type="hidden" name="room_price_currency__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'" value="GEL"> GEL '.($total_cost).'</td><td>
                                <select name="room_select__'.$room_key.'__'.$occupany_counter.'" class="form-control room_select room_select__'.$room_key.' room_selected_qty" id="room_select__'.$room_key.'__'.$occupany_counter.'__'.$fetch_hotel->hotel_id.'">
                                        <option value="0">0</option>';
                                        for($i=1;$i<=($room_values->hotel_room_no_of_rooms-$past_booking_qty);$i++)
                                        {
                                          $html_rooms.="<option value='".$i."'";

                                          if($rooms_occupancy_qty==$i)
                                          {
                                            $html_rooms.=" selected";
                                          }

                                          $html_rooms.=">".$i." ( GEL ".($total_cost*$i)." )</option>";  
                                        }
                                    
                                  $html_rooms.="</select>";

                                  $html_rooms.="</td></tr>";
                                   $occupany_counter++;

                            }
                        }

                    $html.='<tr>
                        <td rowspan="'.$sleeps_count.'">
                            <input type="hidden" name="room_id__'.$room_key.'__'.$fetch_hotel->hotel_id.'" value="'.$room_values->hotel_room_id.'">
                            <input type="hidden" name="room_name__'.$room_key.'__'.$fetch_hotel->hotel_id.'" value="'.ucwords($room_values->hotel_room_class).' '.ucwords($room_values->hotel_room_type).' Room">';
                           $html.=ucwords($room_values->hotel_room_class).' '.ucwords($room_values->hotel_room_type).' Room ('.$room_values->hotel_room_adults.' Adults + ';
                            if($room_values->hotel_room_cwb=="")
                            {
                              $html.='0';
                            }
                            else 
                              {
                                $html.=''.$room_values->hotel_room_cwb;
                              }

                             $html.=' Child)
                        </td>
                        '.$html_rooms;
                  
                   }


                $html.='</tbody>
            </table>
        </div>
    </div>';
      $html.='</div>';
    }
    else
    {
      $html="";
    }
    echo $html;
  }




  public function itinerary_get_activities(Request $request)
  {
    $activity_current_id=$request->get('activity_current_id');
    $country_id=$request->get('country_id');
    $city_id=$request->get('city_id');
    $cities_array=explode(",",$city_id);

    $cities_array=array_unique($cities_array);
    $cities_array=array_filter($cities_array);
    $activity_date=$request->get('activity_date');
    $fetch_activites=Activities::where('activity_country',$country_id)->whereIn('activity_city',$cities_array)->where('validity_fromdate','<=',$activity_date)->where('validity_todate','>=',$activity_date)->where('activity_status',1)->orderBy(\DB::raw('-`activity_show_order`'), 'desc')->orderBy('activity_id','desc')->get();
    $html_header='<style>
    h4.card-title
    {
      font-size:14px !important
    }
    .cstm-img{
      max-height: 132px;
      height: 132px;
      object-fit: cover;
    }
    img.tick {
      position: absolute;
      width: auto;
      height: 40px;
      top: 0;
      left: 0;
    }
    .card-body .card-title {
      margin-bottom: 0;
      border-bottom: none;
      text-overflow: ellipsis !important;
      white-space: nowrap;
      color: black;
      overflow: hidden;
      font-size: 14px;
    }
    .card {
      border-radius: 0px;
      box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
      margin-bottom: 30px !important;
      min-height: 230px;
    }
    .card-img-top {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    .more-btn{
      border: none !important;
      background: no-repeat;
      float: right;
      color: #2196F3;
      font-size: 12px;
      font-weight: 700;
    }
    .card-body {
      padding: 10px;
    }
    </style>
    <div class="row" id="activities_sort_div">';

    $html_current="";
    $html="";
    foreach($fetch_activites as $activities)
    {

       $operating_days=unserialize($activities->operating_weekdays);
      $operating_days_array=array();
      foreach($operating_days as $key=>$days)
      {
        if($days=="Yes")
        {
          $operating_days_array[]= ucwords($key);
        }
      }

      $blackout_days=explode(',',$activities->activity_blackout_dates);
       $calculated_day=date("l",strtotime($activity_date));

      $check_blackout_operating=0;
      if(in_array($activity_date, $blackout_days) || !in_array($calculated_day, $operating_days_array))
      {
        $check_blackout_operating=1;
      }



      $adult_price=0;
      $adult_price_details=unserialize($activities->adult_price_details);

      if(!empty($adult_price_details))
      {
         $adult_price=$adult_price_details[0]['adult_pax_price'];
      }
      else
      {
        $adult_price=0;
      }
     

      $total_cost=round($adult_price);



          
        $get_activity_booking_count=Bookings::where('booking_type','activity')->where('booking_selected_date',$activity_date)->where('booking_type_id',$activities->activity_id)->where('booking_admin_status','!=',2)->get();

        $past_bookings=0;
        foreach($get_activity_booking_count as $book_count)
        {
          $past_bookings+=is_numeric($book_count->booking_adult_count) ? $book_count->booking_adult_count : 0;
          $past_bookings+=is_numeric($book_count->booking_child_count) ? $book_count->booking_child_count : 0;
          $past_bookings+=is_numeric($book_count->booking_infant_count) ? $book_count->booking_infant_count : 0;
        }

        $no_of_bookings=0;
        $availability_qty_details=unserialize($activities->availability_qty_details);

        for($avail=0;$avail< count($availability_qty_details);$avail++)
        {
          if($activity_date>=$availability_qty_details[$avail]['availability_from'] && $activity_date<=$availability_qty_details[$avail]['availability_to'])
          {
            if(!empty($availability_qty_details[$avail]["availability_time_from"]))
            {
              $no_of_bookings=$availability_qty_details[$avail]["availability_no_of_bookings"][0];    
            }
            else
            {
              $no_of_bookings=0;

            }
            // $no_of_bookings=$availability_qty_details[$avail]['no_of_bookings'];
          }
        }

        $no_of_bookings=$no_of_bookings-$past_bookings;


        if($no_of_bookings>0 && $check_blackout_operating==0)
        {
          $adult_age="";
          $child_age="";
          $age_group_details=unserialize($activities->age_group_details);
          $adult_price_details=unserialize($activities->adult_price_details);
          $child_price_details=unserialize($activities->child_price_details);

          if(!empty($age_group_details))
          {
            $adult_age=$age_group_details['adults'];
            $child_age=$age_group_details['child'];

          }

          if(!empty($adult_age) && $adult_age['allowed']=="yes") 
          {
            if(!empty($adult_price_details))
            {
              $min_adult=$adult_price_details[0]['adult_min_pax'];
              $max_adult=$adult_price_details[0]['adult_max_pax'];
              foreach($adult_price_details as $adult_price_i)
              {
                if($adult_price_i['adult_min_pax']<$min_adult)
                {
                  $min_adult=$adult_price_i['adult_min_pax'];
                }
                if($adult_price_i['adult_max_pax']>$max_adult)
                {
                  $max_adult=$adult_price_i['adult_max_pax'];
                }

              }

            }
            else
            {
             $min_adult=0;
             $max_adult=0; 
           }


         }
         else
         {
           $min_adult=0;
           $max_adult=0; 
         }


         if(!empty($child_age) && $child_age['allowed']=="yes") 
         { 
         if(!empty($child_price_details))
         {
          $min_child=$child_price_details[0]['child_min_pax'];
          $max_child=$child_price_details[0]['child_max_pax'];
          foreach($child_price_details as $child_price_i)
          {  
            if($child_price_i['child_min_pax']<$min_child)
            {
              $min_child=$child_price_i['child_min_pax'];
            }
            if($child_price_i['child_max_pax']>$max_child)
            {
              $max_child=$child_price_i['child_max_pax'];
            }

          }
        }
        else
        {
         $min_child=0;
         $max_child=0;

       }
     }
     else
     {

$min_child=0;
         $max_child=0;
     }

      $get_activity_images=unserialize($activities->activity_images);
 $style="";
      if($activities->activity_id==$activity_current_id)
      {
        $style="style='border:8px solid #9c27b0;'";
         $html_current.='<div class="col-md-3 select_activity" id="activity__'.$activities->activity_id.'"><div class="card" '.$style.'>
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
      $img_url="";
      if(!empty($get_activity_images))
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/activities_images"). '/'.$get_activity_images[0].'" alt="Activity Image">';
        $img_url=asset("assets/uploads/activities_images"). '/'.$get_activity_images[0];
      }
      else
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Activity Image">';
        $img_url=asset("assets/images/no-photo.png");
      }
      $html_current.='</div><div class="card-body">
      <h4 class="card-title">'.$activities->activity_name.'</h4>
      <p class="card-text">'.$activities->activity_currency.' '.$total_cost.'</p>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <a href="'.route('activity-details-view',["activity_id"=>$activities->activity_id,'itinerary'=>1]).'" class="more-btn" target="_blank">View Details</a>
      <input type="hidden" class="search_activity_cost" name="search_activity_cost__'.$activities->activity_id.'" value="'.$total_cost.'">
      <input type="hidden" class="search_activity_address" name="search_activity_address__'.$activities->activity_id.'" value="'.$activities->activity_location.'">
      <input type="hidden" class="search_activity_image" name="search_activity_image__'.$activities->activity_id.'" value="'.$img_url.'">
      <input type="hidden" class="search_activity_adult_min_max" name="search_activity_adult_min_max__'.$activities->activity_id.'" value="'.$min_adult.'-'.$max_adult.'">
      <input type="hidden" class="search_activity_child_min_max" name="search_activity_child_min_max__'.$activities->activity_id.'" value="'.$min_child.'-'.$max_child.'">
      <input type="hidden" class="search_activity_detail_link" name="search_activity_detail_link__'.$activities->activity_id.'" value="'.route('activity-details-view',['activity_id'=>$activities->activity_id,'itinerary'=>1]).'">
      </div>
      </div>
      </div>';



      }
      else
      {
         $html.='<div class="col-md-3 select_activity" id="activity__'.$activities->activity_id.'"><div class="card">
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
      $img_url="";
      if(!empty($get_activity_images))
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/activities_images"). '/'.$get_activity_images[0].'" alt="Activity Image">';
        $img_url=asset("assets/uploads/activities_images"). '/'.$get_activity_images[0];
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Activity Image">';
        $img_url=asset("assets/images/no-photo.png");
      }
      $html.='</div><div class="card-body">
      <h4 class="card-title">'.$activities->activity_name.'</h4>
      <p class="card-text">'.$activities->activity_currency.' '.$total_cost.'</p>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <a href="'.route('activity-details-view',["activity_id"=>$activities->activity_id,'itinerary'=>1]).'" class="more-btn" target="_blank">View Details</a>
      <input type="hidden" class="search_activity_cost" name="search_activity_cost__'.$activities->activity_id.'" value="'.$total_cost.'">
      <input type="hidden" class="search_activity_address" name="search_activity_address__'.$activities->activity_id.'" value="'.$activities->activity_location.'">
      <input type="hidden" class="search_activity_image" name="search_activity_image__'.$activities->activity_id.'" value="'.$img_url.'">
      <input type="hidden" class="search_activity_adult_min_max" name="search_activity_adult_min_max__'.$activities->activity_id.'" value="'.$min_adult.'-'.$max_adult.'">
      <input type="hidden" class="search_activity_child_min_max" name="search_activity_child_min_max__'.$activities->activity_id.'" value="'.$min_child.'-'.$max_child.'">
      <input type="hidden" class="search_activity_detail_link" name="search_activity_detail_link__'.$activities->activity_id.'" value="'.route('activity-details-view',['activity_id'=>$activities->activity_id,'itinerary'=>1]).'">
      </div>
      </div>
      </div>';

      }
     
        }
    }
    echo $html_header."".$html_current."".$html;
  }
  public function itinerary_get_sightseeing(Request $request)
  {
     $limit=12;
    if($request->has('offset'))
    {
        $offset=$request->get('offset');
    }
    else
    {
        $offset=0;
    }
    $country_id=$request->get('country_id');
     $sightseeing_current_id=$request->get('sightseeing_current_id');
      $city_id=$request->get('city_id');
    $prev_city_id=$request->get('prev_city_id');
    if($prev_city_id!="")
    {
      $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id,$prev_city_id){
          $query->where(function ($query1) use ($city_id,$prev_city_id){
            $query1->where('sightseeing_city_from',$city_id)->orWhere('sightseeing_city_from',$prev_city_id);

          })->where('sightseeing_city_to',$city_id); });
    }
    else
    {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id){
    $query->where('sightseeing_city_from',$city_id)->where('sightseeing_city_to',$city_id); });
    }

     $fetch_sightseeing=$fetch_sightseeing->where('sightseeing_id','!=',$sightseeing_current_id)->orderBy('sightseeing_popular_status','desc')->orderBy(\DB::raw('-`sightseeing_show_order`'), 'desc')->where('sightseeing_status',1)->orderBy('sightseeing_id','desc')->offset($offset*$limit)->take($limit)->get();

    $currency="GEL";
    $html="";
      $html_current="";


    if($offset==0)
    {
    $html_header='<style>
    h4.card-title
    {
      font-size:14px !important
    }
    .cstm-img{
      max-height: 132px;
      height: 132px;
      object-fit: cover;
    }
    img.tick {
      position: absolute;
      width: auto;
      height: 40px;
      top: 0;
      left: 0;
    }
    .card-body .card-title {
      margin-bottom: 0;
      border-bottom: none;
      text-overflow: ellipsis !important;
      white-space: nowrap;
      color: black;
      overflow: hidden;
      font-size: 14px;
    }
    .card {
      border-radius: 0px;
      box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
      margin-bottom: 30px !important;
      min-height: 230px;
    }
    .card-img-top {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    .more-btn{
      border: none !important;
      background: no-repeat;
      float: right;
      color: #2196F3;
      font-size: 12px;
      font-weight: 700;
    }
    .card-body {
      padding: 10px;
    }
    </style>
    <div class="row" id="sightseeing_sort_div">';

 if($prev_city_id!="")
    {
      $fetch_current_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id,$prev_city_id){
          $query->where(function ($query1) use ($city_id,$prev_city_id){
            $query1->where('sightseeing_city_from',$city_id)->orWhere('sightseeing_city_from',$prev_city_id);

          })->where('sightseeing_city_to',$city_id); });
    }
    else
    {
       $fetch_current_sightseeing=SightSeeing::where('sightseeing_country',$country_id)->where(function ($query) use ($city_id){
    $query->where('sightseeing_city_from',$city_id)->where('sightseeing_city_to',$city_id); });
    }

     $fetch_current_sightseeing=$fetch_current_sightseeing->where('sightseeing_id',$sightseeing_current_id)->where('sightseeing_status',1)->orderBy('sightseeing_id','desc')->offset($offset*$limit)->take($limit)->get();

     foreach($fetch_current_sightseeing as $sightseeing)
    {
      $price=$sightseeing->sightseeing_adult_cost;
      $price+=round($sightseeing->sightseeing_food_cost);
      $price+=round($sightseeing->sightseeing_hotel_cost);
       if($sightseeing->sightseeing_default_guide_price!="" && $sightseeing->sightseeing_default_guide_price!=null)
      {
        $price+=round($sightseeing->sightseeing_default_guide_price);
      }

      if($sightseeing->sightseeing_default_driver_price!="" && $sightseeing->sightseeing_default_driver_price!=null)
      {
        $price+=round($sightseeing->sightseeing_default_driver_price);
      }

      $total_cost=round($price);
      $newid=base64_encode($sightseeing->sightseeing_id);
      $address="";
      $sightseeingimage=unserialize($sightseeing->sightseeing_images);
      $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_from,$sightseeing->sightseeing_country);
      $address.=$get_from_city['name']."-";
      if($sightseeing->sightseeing_city_between!=null && $sightseeing->sightseeing_city_between!="")
      {
        $all_between_cities=explode(",",$sightseeing->sightseeing_city_between);
        for($cities=0;$cities< count($all_between_cities);$cities++)
        {
          $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$sightseeing->sightseeing_country);
          $address.=$fetch_city['name']."-";
        }
      }
      $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_to,$sightseeing->sightseeing_country);
      $address.=$get_from_city['name'];
      $style="";
    
      if($sightseeing->sightseeing_id==$sightseeing_current_id)
      {
        $style="style='border:8px solid #9c27b0;'";

        $html_current.='<div class="col-md-3 select_sightseeing" id="sightseeing__'.$sightseeing->sightseeing_id.'">
      <div class="card" '.$style.'>
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
      if(!empty($sightseeingimage))
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/sightseeing_images"). '/'.$sightseeingimage[0].'" alt="Sightseeing Image">';
      }
      else
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Activity Image">';
      }
      $attractions=htmlentities($sightseeing->sightseeing_attractions);
      $html_current.='</div><div class="card-body">
      <h4 class="card-title">'.$sightseeing->sightseeing_tour_name.'</h4>
      <p class="card-text">'.$currency.' '.$total_cost.'</p>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <button id="sightseeing_details_'.$sightseeing->sightseeing_id.'" class="more-btn sightseeing_details">More Details</button>
      <input type="hidden" class="search_sightseeing_cost" name="search_sightseeing_cost__'.$sightseeing->sightseeing_id.'" value="'.$total_cost.'">
      <input type="hidden" class="search_sightseeing_address" name="search_sightseeing_address__'.$sightseeing->sightseeing_id.'" value="'.$address.'">
      <input type="hidden" class="search_sightseeing_distance" name="search_sightseeing_distance__'.$sightseeing->sightseeing_id.'" value="'.$sightseeing->sightseeing_distance_covered.'">';
      if(!empty($sightseeingimage))
      {
        $html_current.='<input type="hidden" class="search_sightseeing_image" name="search_sightseeing_image__'.$sightseeing->sightseeing_id.'" value="'.asset("assets/uploads/sightseeing_images"). '/'.$sightseeingimage[0].'">';
      }
      else
      {
        $html_current.='<input type="hidden" class="search_sightseeing_image" name="search_sightseeing_image__'.$sightseeing->sightseeing_id.'" value="'.asset("assets/images/no-photo.png").'">';
      }
      
       $html_current.='<input type="hidden" class="search_sightseeing_attractions" name="search_sightseeing_attractions__'.$sightseeing->sightseeing_id.'" value="'.$attractions.'">

      </div>
      </div>
      </div>';

      }  
    }
  }
  else
  {
    $html_header="";
  }
   
    foreach($fetch_sightseeing as $sightseeing)
    {
      $price=$sightseeing->sightseeing_adult_cost;
      $price+=round($sightseeing->sightseeing_food_cost);
      $price+=round($sightseeing->sightseeing_hotel_cost);
       if($sightseeing->sightseeing_default_guide_price!="" && $sightseeing->sightseeing_default_guide_price!=null)
      {
        $price+=round($sightseeing->sightseeing_default_guide_price);
      }

      if($sightseeing->sightseeing_default_driver_price!="" && $sightseeing->sightseeing_default_driver_price!=null)
      {
        $price+=round($sightseeing->sightseeing_default_driver_price);
      }

      $total_cost=round($price);
      $newid=base64_encode($sightseeing->sightseeing_id);
      $address="";
      $sightseeingimage=unserialize($sightseeing->sightseeing_images);
      $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_from,$sightseeing->sightseeing_country);
      $address.=$get_from_city['name']."-";
      if($sightseeing->sightseeing_city_between!=null && $sightseeing->sightseeing_city_between!="")
      {
        $all_between_cities=explode(",",$sightseeing->sightseeing_city_between);
        for($cities=0;$cities< count($all_between_cities);$cities++)
        {
          $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$sightseeing->sightseeing_country);
          $address.=$fetch_city['name']."-";
        }
      }
      $get_from_city=ServiceManagement::searchCities($sightseeing->sightseeing_city_to,$sightseeing->sightseeing_country);
      $address.=$get_from_city['name'];
      $style="";
    
        $html.='<div class="col-md-3 select_sightseeing" id="sightseeing__'.$sightseeing->sightseeing_id.'">
      <div class="card" '.$style.'>
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
      if(!empty($sightseeingimage))
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/sightseeing_images"). '/'.$sightseeingimage[0].'" alt="Sightseeing Image">';
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Activity Image">';
      }
      $attractions=htmlentities($sightseeing->sightseeing_attractions);
      $html.='</div><div class="card-body">
      <h4 class="card-title">'.$sightseeing->sightseeing_tour_name.'</h4>
      <p class="card-text">'.$currency.' '.$total_cost.'</p>
      <span class="card-price-text" style="display:none">'.$total_cost.'</span>
      <button id="sightseeing_details_'.$sightseeing->sightseeing_id.'" class="more-btn sightseeing_details">More Details</button>
      <input type="hidden" class="search_sightseeing_cost" name="search_sightseeing_cost__'.$sightseeing->sightseeing_id.'" value="'.$total_cost.'">
      <input type="hidden" class="search_sightseeing_address" name="search_sightseeing_address__'.$sightseeing->sightseeing_id.'" value="'.$address.'">
      <input type="hidden" class="search_sightseeing_distance" name="search_sightseeing_distance__'.$sightseeing->sightseeing_id.'" value="'.$sightseeing->sightseeing_distance_covered.'">';
      if(!empty($sightseeingimage))
      {
        $html.='<input type="hidden" class="search_sightseeing_image" name="search_sightseeing_image__'.$sightseeing->sightseeing_id.'" value="'.asset("assets/uploads/sightseeing_images"). '/'.$sightseeingimage[0].'">';
      }
      else
      {
        $html.='<input type="hidden" class="search_sightseeing_image" name="search_sightseeing_image__'.$sightseeing->sightseeing_id.'" value="'.asset("assets/images/no-photo.png").'">';
      }
      
       $html.='<input type="hidden" class="search_sightseeing_attractions" name="search_sightseeing_attractions__'.$sightseeing->sightseeing_id.'" value="'.$attractions.'">

      </div>
      </div>
      </div>';


      
    }
    echo $html_header." ".$html_current." ".$html;
  }

  public function itinerary_get_transfer(Request $request)
  {
    $css=' <style>
    button.book-btn {
      background: red;
      border: none;
      border-radius: 50px;
      margin-top: 120px;
      padding: 10px 20px;
      text-align: center;
      color: white;
      width: 160px;
    }
    .hotel-name small
    {
      color:grey;
    }
    h4.card-title
    {
      font-size:14px !important
    }

    .cstm-img{
      max-height: 132px;
      height: 132px;
      object-fit: cover;
    }
      img.tick {
      position: absolute;
      width: auto;
     height: 40px;
      top: 0;
      left: 0;
  }
  .card-rating-text
  {
    font-size: 12px;
    float: right;
    font-weight: 600;
    color: red;
  }
  .select_transfer
  {
    cursor:pointer;
  }
    </style>';
    if($request->get('transfer_type')=="from-airport")
  {
  
    $transfer_type=$request->get('transfer_type');
    $country_id=$request->get('country_id');

    $from_airport=$request->get('from_airport');
    $to_city=$request->get('to_city');
    
    $airports=AirportMaster::where('airport_master_id',$from_airport)->first();
    $airport_name=$airports['airport_master_name'];
    $cities=Cities::where('id',$to_city)->first();
    $city_name=$cities['name'];
   
    $vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_airport)->where('transfer_details.to_city_airport',$to_city)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_details_id','desc')->get();
    $html=' <p>Showing results for Airport Transfer from <b>"'. $airport_name.'"</b> to <b>"'.$city_name.'"</b></p>'.$css.'
    <div class="row" id="transfer_sort_div">';
    foreach($fetch_transfers as $transfers)
    {
   
      $vehicle_type_name="";
      $vehicle_name="";
      $vehicle_min=0;
      $vehicle_max=0;
      $vehicle_type_data=VehicleType::where('vehicle_type_id',$transfers->transfer_vehicle_type)->first();
      $vehicles=Vehicles::where('vehicle_id',$transfers->transfer_vehicle)->first();

      $vehicle_type_name=$vehicle_type_data['vehicle_type_name'];
      $vehicle_min=$vehicle_type_data['vehicle_type_min'];
      $vehicle_max=$vehicle_type_data['vehicle_type_max'];


      $vehicle_name=$vehicles['vehicle_name'];
      $vehicle_cost=$transfers->transfer_vehicle_cost;
  
       $html.='<div class="col-md-4 select_transfer" id="transfer__'.$transfers->transfer_id.'"><div class="card">
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
       $transferimage=unserialize($transfers->transfer_vehicle_images);
      if(!empty($transferimage[0]))
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0].'" alt="Transfer Image">';
         $actual_image=asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0];
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Transfer Image">';
         $actual_image=asset("assets/images/no-photo.png");
      } 

      $html.='</div><div class="card-body">
      <h4 class="card-title">'.$vehicle_name.' ('.$vehicle_type_name.')<br> <small>'.$transfers->transfer_vehicle_info.'</small></h4>
      <span class="card-rating-text">Pax Capacity :'.$vehicle_min.' - '.$vehicle_max.'</span>
      <p class="card-text">GEL '. $vehicle_cost.'</p>
      <span class="card-price-text" style="display:none">'.$vehicle_cost.'</span>
       <input type="hidden" class="search_transfer_cost" name="search_transfer_cost__'.$transfers->transfer_id.'" value="'.$vehicle_cost.'">
       <input type="hidden" class="search_transfer_from_airport" name="search_transfer_from_airport__'.$transfers->transfer_id.'" value="'.$transfers->from_city_airport.'">
       <input type="hidden" class="search_transfer_to_city" name="search_transfer_to_city__'.$transfers->transfer_id.'" value="'.$transfers->to_city_airport.'">
       <input type="hidden" class="search_vehicle_image" name="search_vehicle_image__'.$transfers->transfer_id.'" value="'.$actual_image.'">
        <input type="hidden" class="search_vehicle_type" name="search_vehicle_type__'.$transfers->transfer_id.'" value="'.$transfers->transfer_vehicle_type.'">
      </div>
      </div>
      </div>';
   
    
    }
     if(preg_match('/img/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }
      
    
  }
  else if($request->get('transfer_type')=="to-airport")
{

    $transfer_type=$request->get('transfer_type');
    $country_id=$request->get('country_id');

    $from_city=$request->get('from_city');
    $to_airport=$request->get('to_airport');
    
    $airports=AirportMaster::where('airport_master_id',$to_airport)->first();
    $airport_name=$airports['airport_master_name'];
    $cities=Cities::where('id',$from_city)->first();
    $city_name=$cities['name'];
   
    $vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_city)->where('transfer_details.to_city_airport',$to_airport)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_details_id','desc')->get();

    $html=' <p>Showing results for Airport Transfer from <b>"'. $city_name.'"</b> to <b>"'.$airport_name.'"</b></p>'.$css.'
    <div class="row" id="transfer_sort_div">';
    foreach($fetch_transfers as $transfers)
    {
   
      $vehicle_type_name="";
      $vehicle_name="";
      $vehicle_min=0;
      $vehicle_max=0;
      $vehicle_type_data=VehicleType::where('vehicle_type_id',$transfers->transfer_vehicle_type)->first();
      $vehicles=Vehicles::where('vehicle_id',$transfers->transfer_vehicle)->first();

      $vehicle_type_name=$vehicle_type_data['vehicle_type_name'];
      $vehicle_min=$vehicle_type_data['vehicle_type_min'];
      $vehicle_max=$vehicle_type_data['vehicle_type_max'];


      $vehicle_name=$vehicles['vehicle_name'];


     
       $vehicle_cost=$transfers->transfer_vehicle_cost;
  
        $html.='<div class="col-md-4 select_transfer" id="transfer__'.$transfers->transfer_id.'"><div class="card">
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
       $transferimage=unserialize($transfers->transfer_vehicle_images);
       $actual_image="";
      if(!empty($transferimage[0]))
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0].'" alt="Transfer Image">';
         $actual_image=asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0];
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Transfer Image">';
         $actual_image=asset("assets/images/no-photo.png");
      } 

      $html.='</div><div class="card-body">
      <h4 class="card-title">'.$vehicle_name.' ('.$vehicle_type_name.')<br> <small>'.$transfers->transfer_vehicle_info.'</small></h4>
      <span class="card-rating-text">Pax Capacity :'.$vehicle_min.' - '.$vehicle_max.'</span>
      <p class="card-text">GEL '. $vehicle_cost.'</p>
       <span class="card-price-text" style="display:none">'.$vehicle_cost.'</span>
       <input type="hidden" class="search_transfer_cost" name="search_transfer_cost__'.$transfers->transfer_id.'" value="'.$vehicle_cost.'">
       <input type="hidden" class="search_transfer_from_city" name="search_transfer_from_city__'.$transfers->transfer_id.'" value="'.$transfers->from_city_airport.'">
       <input type="hidden" class="search_transfer_to_airport" name="search_transfer_to_airport__'.$transfers->transfer_id.'" value="'.$transfers->to_city_airport.'">
       <input type="hidden" class="search_vehicle_image" name="search_vehicle_image__'.$transfers->transfer_id.'" value="'.$actual_image.'">
        <input type="hidden" class="search_vehicle_type" name="search_vehicle_type__'.$transfers->transfer_id.'" value="'.$transfers->transfer_vehicle_type.'">
      </div>
      </div>
      </div>';
   
    
    }
     if(preg_match('/img/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }

}
  else
  {
    // $markup=$this->agent_markup();
    // $transfer_markup=0;
    // if($markup!="")
    // {
    //   $get_all_services=explode("///",$markup);
    //   for($markup=0;$markup<count($get_all_services);$markup++)
    //   {
    //     $get_individual_service=explode("---",$get_all_services[$markup]);
    //     if($get_individual_service[0]=="transfer")
    //     {
    //       if($get_individual_service[1]!="")
    //       {
    //         $transfer_markup=$get_individual_service[1];
    //       }
    //       break;
    //     }
    //   }
    // }
    $transfer_type=$request->get('transfer_type');
    $country_id=$request->get('country_id');
    $from_city=$request->get('from_city');
    $to_city=$request->get('to_city');
    
    $cities=Cities::where('id',$from_city)->first();
    $from_city_name=$cities['name'];
    $cities=Cities::where('id',$to_city)->first();
    $to_city_name=$cities['name'];
    

    $fetch_transfers=Transfers::join('transfer_details','transfer_details.transfer_id','=','transfers.transfer_id')->where('transfers.transfer_country',$country_id)->where('transfer_details.from_city_airport',$from_city)->where('transfer_details.to_city_airport',$to_city)->where('transfers.transfer_type',$transfer_type)->where('transfers.transfer_approve_status',1)->where('transfers.transfer_status',1)->orderBy('transfer_details.transfer_id','desc')->get();

    $html='<p>Showing results for City Transfer from <b>"'. $from_city_name.'"</b> to <b>"'.$to_city_name.'"</b></p>'.$css.'
    <div class="row" id="transfer_sort_div">';
    foreach($fetch_transfers as $transfers)
    {
      $vehicle_type_name="";
      $vehicle_name="";
      $vehicle_min=0;
      $vehicle_max=0;
      $vehicle_type_data=VehicleType::where('vehicle_type_id',$transfers->transfer_vehicle_type)->first();
      $vehicles=Vehicles::where('vehicle_id',$transfers->transfer_vehicle)->first();

      $vehicle_type_name=$vehicle_type_data['vehicle_type_name'];
      $vehicle_min=$vehicle_type_data['vehicle_type_min'];
      $vehicle_max=$vehicle_type_data['vehicle_type_max'];


      $vehicle_name=$vehicles['vehicle_name'];

       $vehicle_cost=$transfers->transfer_vehicle_cost;



        $html.='<div class="col-md-4 select_transfer" id="transfer__'.$transfers->transfer_id.'"><div class="card">
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
       $transferimage=unserialize($transfers->transfer_vehicle_images);
      if(!empty($transferimage[0]))
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0].'" alt="Transfer Image">';
         $actual_image=asset("assets/uploads/vehicle_images"). '/'.$transferimage[0][0];
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Transfer Image">';
         $actual_image=asset("assets/images/no-photo.png");
      } 

      $html.='</div><div class="card-body">
      
      <h4 class="card-title">'.$vehicle_name.' ('.$vehicle_type_name.')<br> <small>'.$transfers->transfer_vehicle_info.'</small></h4>
        <span class="card-rating-text">Pax Capacity : '.$vehicle_min.' - '.$vehicle_max.'</span>
      <p class="card-text">GEL '. $vehicle_cost.'</p>
       <span class="card-price-text" style="display:none">'.$vehicle_cost.'</span>
       <input type="hidden" class="search_transfer_cost" name="search_transfer_cost__'.$transfers->transfer_id.'" value="'.$vehicle_cost.'">
       <input type="hidden" class="search_transfer_from_city" name="search_transfer_from_city__'.$transfers->transfer_id.'" value="'.$transfers->from_city_airport.'">
       <input type="hidden" class="search_transfer_to_city" name="search_transfer_to_city__'.$transfers->transfer_id.'" value="'.$transfers->to_city_airport.'">
       <input type="hidden" class="search_vehicle_image" name="search_vehicle_image__'.$transfers->transfer_id.'" value="'.$actual_image.'">
      </div>
      </div>
      </div>';
  
    }
     if(preg_match('/img/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }
  }
  }
   public function itinerary_get_restaurant(Request $request)
  {
     $limit=12;
    if($request->has('offset'))
    {
        $offset=$request->get('offset');
    }
    else
    {
        $offset=0;
    }
    $country_id=$request->get('country_id');
     $restaurant_current_id=$request->get('restaurant_current_id');
     $restaurant_date=$request->get('restaurant_date');
      $city_id=$request->get('city_id');
    $cities_array=explode(",",$city_id);

    $cities_array=array_unique($cities_array);
    $cities_array=array_filter($cities_array);
   
       $fetch_restaurant=Restaurants::where('restaurant_country',$country_id)->whereIn('restaurant_city',$cities_array)->where('restaurant_id','!=',$restaurant_current_id)->where('validity_fromdate','<=',$restaurant_date)->where('validity_todate','>=',$restaurant_date)->where('restaurant_status',1)->orderBy('restaurant_id','desc')->offset($offset*$limit)->take($limit)->get();

    $currency="GEL";
    $html="";
      $html_current="";


    if($offset==0)
    {
    $html_header='<style>
    h4.card-title
    {
      font-size:14px !important
    }
    .cstm-img{
      max-height: 132px;
      height: 132px;
      object-fit: cover;
    }
    img.tick {
      position: absolute;
      width: auto;
      height: 40px;
      top: 0;
      left: 0;
    }
    .card-body .card-title {
      margin-bottom: 0;
      border-bottom: none;
      text-overflow: ellipsis !important;
      white-space: nowrap;
      color: black;
      overflow: hidden;
      font-size: 14px;
    }
     .card-body .card-text {
       text-overflow: ellipsis !important;
      white-space: nowrap;
       overflow: hidden;
     }
    .card {
      border-radius: 0px;
      box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
      margin-bottom: 30px !important;
      min-height: 230px;
    }
    .card-img-top {
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    .more-btn{
      border: none !important;
      background: no-repeat;
      float: right;
      color: #2196F3;
      font-size: 12px;
      font-weight: 700;
    }
    .card-body {
      padding: 10px;
    }
    .card-type
    {
      font-size:10px;
    }
    </style>
    <div class="row" id="sightseeing_sort_div">';
      $fetch_restaurant_current=Restaurants::where('restaurant_country',$country_id)->whereIn('restaurant_city',$cities_array)->where('restaurant_id',$restaurant_current_id)->where('validity_fromdate','<=',$restaurant_date)->where('validity_todate','>=',$restaurant_date)->where('restaurant_status',1)->orderBy('restaurant_id','desc')->offset($offset*$limit)->take($limit)->get();
        $style="style='border:8px solid #9c27b0;'";
    foreach($fetch_restaurant_current as $restaurant)
    {
    
      $newid=base64_encode($restaurant->restaurant_id);
      $address=$restaurant->restaurant_address;
      $restaurantimage=unserialize($restaurant->restaurant_images);
$image_url="";
        $html_current.='<div class="col-md-3 select_restaurant" id="restaurant__'.$restaurant->restaurant_id.'">
      <div class="card" '.$style.'>
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
      if(!empty($restaurantimage))
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/restaurant_images"). '/'.$restaurantimage[0].'" alt="Restaurant Image">';
        $image_url=asset("assets/uploads/restaurant_images"). '/'.$restaurantimage[0];
      }
      else
      {
        $html_current.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Restaurant Image">';
         $image_url=asset("assets/images/no-photo.png");
      }
      $html_current.='</div><div class="card-body">
      <span class="card-type">'.$restaurant->getRestaurantType->restaurant_type_name.'</span>
      <h4 class="card-title">'.$restaurant->restaurant_name.'</h4>
      <p class="card-text">'.$address.'</p>
      <button id="restaurant_details_'.$restaurant->restaurant_id.'" class="more-btn restaurant_details">More Details</button>
      <input type="hidden" class="search_restaurant_address" name="search_restaurant_address__'.$restaurant->restaurant_id.'" value="'.$address.'">
        <input type="hidden" class="search_restaurant_image" name="search_restaurant_image__'.$restaurant->restaurant_id.'" value="'.$image_url.'">';
      if(!empty($restaurantimage))
      {
        $html_current.='<input type="hidden" class="search_restaurant_image" name="search_restaurant_image__'.$restaurant->restaurant_id.'" value="'.asset("assets/uploads/restaurant_images"). '/'.$restaurantimage[0].'">';
      }
      else
      {
        $html_current.='<input type="hidden" class="search_restaurant_image" name="search_restaurant_image__'.$restaurant->restaurant_id.'" value="'.asset("assets/images/no-photo.png").'">';
      }
      
       $html_current.='
      </div>
      </div>
      </div>';


    }

  }
  else
  {
    $html_header="";
  }
   
    foreach($fetch_restaurant as $restaurant)
    {
    
      $newid=base64_encode($restaurant->restaurant_id);
      $address=$restaurant->restaurant_address;
      $restaurantimage=unserialize($restaurant->restaurant_images);
$image_url="";
        $html.='<div class="col-md-3 select_restaurant" id="restaurant__'.$restaurant->restaurant_id.'">
      <div class="card" >
      <div style="position:relative">
      <img src="https://crm.traveldoor.ge/assets/uploads/hotel_images/hotel-success-tick.png-1571899984.png" class="tick" style="display:none">';
      if(!empty($restaurantimage))
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/uploads/restaurant_images"). '/'.$restaurantimage[0].'" alt="Restaurant Image">';
        $image_url=asset("assets/uploads/restaurant_images"). '/'.$restaurantimage[0];
      }
      else
      {
        $html.=' <img class="card-img-top cstm-img" src="'.asset("assets/images/no-photo.png").'" alt="Restaurant Image">';
         $image_url=asset("assets/images/no-photo.png");
      }
      $html.='</div><div class="card-body">
      <span class="card-type">'.$restaurant->getRestaurantType->restaurant_type_name.'</span>
      <h4 class="card-title">'.$restaurant->restaurant_name.'</h4>
      <p class="card-text">'.$address.'</p>
      <button id="restaurant_details_'.$restaurant->restaurant_id.'" class="more-btn restaurant_details">More Details</button>
      <input type="hidden" class="search_restaurant_address" name="search_restaurant_address__'.$restaurant->restaurant_id.'" value="'.$address.'">
        <input type="hidden" class="search_restaurant_image" name="search_restaurant_image__'.$restaurant->restaurant_id.'" value="'.$image_url.'">';
      if(!empty($restaurantimage))
      {
        $html.='<input type="hidden" class="search_restaurant_image" name="search_restaurant_image__'.$restaurant->restaurant_id.'" value="'.asset("assets/uploads/restaurant_images"). '/'.$restaurantimage[0].'">';
      }
      else
      {
        $html.='<input type="hidden" class="search_restaurant_image" name="search_restaurant_image__'.$restaurant->restaurant_id.'" value="'.asset("assets/images/no-photo.png").'">';
      }
      
       $html.='
      </div>
      </div>
      </div>';


    }
    echo $html_header." ".$html_current." ".$html;
  }

 

  public function itinerary_details_view($itinerary_id,Request $request)
  {
    $markup=$this->agent_markup();
    $itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
    $own_markup=$this->agent_own_markup();
    $own_itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $own_itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
    $countries=Countries::get();
    $cities=Cities::get();
    $itinerary_date_from=session()->get('itinerary_date_from');
    $agent_id=session()->get('travel_agent_id');
    $get_vehicles=VehicleType::where('vehicle_type_status',1)->get();
    $get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->where('itinerary_status',1)->first();
    if($get_itinerary)
    {
      $room_currency_array=array("USD");
      $room_currency_array=array_unique($room_currency_array);
      
      $currency_price_array=array();
      foreach($room_currency_array as $currency)
      {
        if($currency=="GEL")
        {

        }
        else
        {
           $new_currency=$currency;

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$new_currency."_".$this->base_currency;

        //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
          
        //  $currency_price_array[$currency]=round($conversion_price,2);
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);
        }
         
      }

      $currency_price_array["GEL"]=1;
     

      if($request->has('saved'))
      {
        $saved_id=urldecode(base64_decode(base64_decode($request->get('saved'))));

         $fetch_saved_itinerary=AgentSavedItinerary::where('agent_itinerary_id',$saved_id)->where('agent_itinerary_created_by',$agent_id)->first();

         return view('agent.itinerary-details')->with(compact('countries','cities','agent_id','get_itinerary','fetch_saved_itinerary','get_vehicles','currency_price_array'))->with('markup',$itinerary_markup)->with('own_markup',$own_itinerary_markup)->with('itinerary_date_from',$itinerary_date_from)->with('saved',1);
      }
      else
      {
         return view('agent.itinerary-details')->with(compact('countries','cities','agent_id','get_itinerary','get_vehicles','currency_price_array'))->with('markup',$itinerary_markup)->with('own_markup',$own_itinerary_markup)->with('itinerary_date_from',$itinerary_date_from);
      }



     
    }
    else
    {
      return redirect()->back();
    }
  }
 

 


public function itinerary_booking_whole_cost(Request $request)
{
  // echo "<pre>";
  // print_r($request->all());
  // die();
                            
  $agent_id=session()->get('travel_agent_id');
  $itinerary_id=$request->get('itinerary_id');
  $booking_amount=$request->get('total_cost');
  $booking_markup_per=$request->get('markup_per');
  $booking_own_markup_per=$request->get('own_markup_per');
  $booking_amount_without_markup=$request->get('total_cost_w_markup');
   $booking_amount_without_agent_markup=$request->get('total_cost_w_agent_markup');
  $booking_from_date=$request->get('from_date');
  $booking_to_date=$request->get('to_date');
  $booking_dates_array=$request->get('days_dates');
  $booking_details_array=array();

  $hotel_id=$request->get('hotel_id');
  $hotel_name=$request->get('hotel_name');
  $room_name=$request->get('room_name');
  $hotel_cost=$request->get('hotel_cost');
  $hotel_no_of_days=$request->get('hotel_no_of_days');
  $rooms_qty=$request->get('room_qty');
  $hotel_checkin=$request->get('hotel_checkin');
  $hotel_checkout=$request->get('hotel_checkout');
  $hotel_room_name=$request->get('hotel_room_name');
  $hotel_room_qty=$request->get('hotel_room_qty');
  $hotel_room_cost=$request->get('hotel_room_cost');
  $hotel_room_id=$request->get('hotel_room_id');
  $hotel_occupancy_id=$request->get('hotel_occupancy_id');
  $hotel_occupancy_qty=$request->get('hotel_occupancy_qty');

  $sightseeing_id=$request->get('sightseeing_id');
  $sightseeing_name=$request->get('sightseeing_name');
  $sightseeing_tour_type=$request->get('sightseeing_tour_type');
  $sightseeing_vehicle_type=$request->get('sightseeing_vehicle_type');
  $sightseeing_guide_id=$request->get('sightseeing_guide_id');
  $sightseeing_guide_name=$request->get('sightseeing_guide_name'); 
  $sightseeing_guide_cost=$request->get('sightseeing_guide_cost');
  $sightseeing_driver_id=$request->get('sightseeing_driver_id');  
  $sightseeing_driver_name=$request->get('sightseeing_driver_name'); 
  $sightseeing_driver_cost=$request->get('sightseeing_driver_cost');
  $sightseeing_adult_cost=$request->get('sightseeing_adult_cost');
    $sightseeing_additional_cost=$request->get('sightseeing_additional_cost');
  $sightseeing_cost=$request->get('sightseeing_cost');

  $activity_id=$request->get('activity_id');
  $activity_name=$request->get('activity_name');
  $activity_cost=$request->get('activity_cost');

  $transfer_id=$request->get('transfer_id');
  $transfer_name=$request->get('transfer_name');
  $transfer_car_name=$request->get('transfer_car_name');
  $transfer_type=$request->get('transfer_type');
  $transfer_from_city=$request->get('transfer_from_city');
  $transfer_to_city=$request->get('transfer_to_city');
  $transfer_from_airport=$request->get('transfer_from_airport');
  $transfer_to_airport=$request->get('transfer_to_airport');
  $transfer_pickup=$request->get('transfer_pickup');
  $transfer_dropoff=$request->get('transfer_dropoff');
  $transfer_vehicle_type=$request->get('transfer_vehicle_type');
    $transfer_guide_id=$request->get('transfer_guide_id');
    $transfer_guide_name=$request->get('transfer_guide_name');
    $transfer_guide_cost=$request->get('transfer_guide_cost');
  $transfer_cost=$request->get('transfer_cost');
  $transfer_total_cost=$request->get('transfer_total_cost');

$restaurant_id=$request->get('restaurant_id');
$restaurant_name=$request->get('restaurant_name');
$restaurant_food_for=$request->get('restaurant_food_for');
$restaurant_cost=$request->get('restaurant_cost');
$restaurant_food_name=$request->get('restaurant_food_name');
$restaurant_food_qty=$request->get('restaurant_food_qty');
$restaurant_food_price=$request->get('restaurant_food_price');
$restaurant_food_id=$request->get('restaurant_food_id');
$restaurant_food_category_id=$request->get('restaurant_food_category_id');
$restaurant_food_unit=$request->get('restaurant_food_unit');

  $rooms_count=$request->get('rooms_count');
  $select_adults=$request->get('select_adults');
  $select_child=$request->get('select_child');
  $child_age=$request->get('child_age');

  $no_of_adults=array_sum($select_adults);
  $no_of_child=array_sum($select_child);
  if($request->has('child_age'))
  {
     $child_age_array=serialize($child_age);
  }
  else
  {
    $child_age=array();
    $child_age_array=serialize($child_age);

  }

  $activities_select_adults=$request->get('activities_select_adults');
  $activities_select_child=$request->get('activities_select_child');
  $activities_select_child_age=$request->get('activities_select_child_age');

  // $activities_select_child_age=array_map('array_values', $activities_select_child_age);

  $no_of_pax=$no_of_adults+$no_of_child;
  $calculate_total_cost=0;
$total_rooms_count=0;

  for($dates_count=0;$dates_count<=count($booking_dates_array)-1;$dates_count++)
  {
    $booking_details_array[$dates_count]['dates']=$booking_dates_array[$dates_count];
    if(!empty($hotel_id[$dates_count]))
    {

      $booking_details_array[$dates_count]['hotel']['hotel_id']=$hotel_id[$dates_count];
      $booking_details_array[$dates_count]['hotel']['hotel_name']=$hotel_name[$dates_count];
      $booking_details_array[$dates_count]['hotel']['room_name']=$room_name[$dates_count];
      $booking_details_array[$dates_count]['hotel']['hotel_checkin']=$hotel_checkin[$dates_count];
      $booking_details_array[$dates_count]['hotel']['hotel_checkout']=$hotel_checkout[$dates_count];
      $booking_details_array[$dates_count]['hotel']['no_of_days']=$hotel_no_of_days[$dates_count];
      $booking_details_array[$dates_count]['hotel']['room_quantity']=$rooms_qty[$dates_count];
       $total_rooms_count+=$rooms_qty[$dates_count];
      if(empty($hotel_room_qty[$dates_count]))
      {
      // $booking_details_array[$dates_count]['hotel']['hotel_cost']=$hotel_cost[$dates_count]*count($rooms_count);
      // $calculate_total_cost+=$hotel_cost[$dates_count]*count($rooms_count);

      $hotel_room_name_array=$hotel_room_name[$dates_count];
        $hotel_room_cost_array=$hotel_room_cost[$dates_count];
        $hotel_total_cost=0;
          for($i=0;$i<count($hotel_room_name_array);$i++)
          {

            $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_name']=$hotel_room_name_array[$i];
             $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_qty']=$rooms_qty[$dates_count];
             $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_cost']=$hotel_room_cost_array[$i];


             $hotel_total_cost+=$hotel_room_cost_array[$i]*$rooms_qty[$dates_count];

          }
      $booking_details_array[$dates_count]['hotel']['hotel_cost']=$hotel_total_cost;
      $calculate_total_cost+=$hotel_total_cost;


      }
      else
      {
       $hotel_room_name_array=$hotel_room_name[$dates_count];
        $hotel_room_qty_array=$hotel_room_qty[$dates_count];
        $hotel_room_cost_array=$hotel_room_cost[$dates_count];
         $hotel_room_id_array=$hotel_room_id[$dates_count];
          $hotel_occupancy_id_array=$hotel_occupancy_id[$dates_count];
          $hotel_occupancy_qty_array=$hotel_occupancy_qty[$dates_count];
        $hotel_total_cost=0;
          for($i=0;$i<count($hotel_room_name_array);$i++)
          {

           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_name']=$hotel_room_name_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_qty']=$hotel_room_qty_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_cost']=$hotel_room_cost_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_id']=$hotel_room_id_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_id']=$hotel_occupancy_id_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_qty']=$hotel_occupancy_qty_array[$i];

             $hotel_total_cost+=$hotel_room_cost_array[$i]*$hotel_room_qty_array[$i];

          }
      $booking_details_array[$dates_count]['hotel']['hotel_cost']=$hotel_total_cost;
      $calculate_total_cost+=$hotel_total_cost;




      }
     

    }
    if(!empty($sightseeing_id[$dates_count]))
    {
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_id"]=$sightseeing_id[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_name"]=$sightseeing_name[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_tour_type"]=$sightseeing_tour_type[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_vehicle_type"]=$sightseeing_vehicle_type[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_guide_id"]=$sightseeing_guide_id[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_guide_name"]=$sightseeing_guide_name[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_guide_cost"]=$sightseeing_guide_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_driver_id"]=$sightseeing_driver_id[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_driver_name"]=$sightseeing_driver_name[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_driver_cost"]=$sightseeing_driver_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_adult_cost"]=$sightseeing_adult_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_additional_cost"]=$sightseeing_additional_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_cost"]=$sightseeing_cost[$dates_count];

      if($sightseeing_tour_type[$dates_count]=="private")
      {
        $calculate_total_cost+=$sightseeing_guide_cost[$dates_count];
        $calculate_total_cost+=$sightseeing_driver_cost[$dates_count];
        $calculate_total_cost+=$sightseeing_additional_cost[$dates_count];
         $get_sightseing=Sightseeing::where('sightseeing_id',$sightseeing_id[$dates_count])->first();
        $child_cost=$get_sightseing->sightseeing_child_cost;
        $calculate_total_cost+=$sightseeing_adult_cost[$dates_count]*$no_of_adults;
        if($no_of_child>0)
        {
           $calculate_total_cost+=$child_cost*$no_of_child;
        }
      
      }
      else
      {
         $calculate_total_cost+=$sightseeing_additional_cost[$dates_count];
          $get_sightseing=Sightseeing::where('sightseeing_id',$sightseeing_id[$dates_count])->first();
          $child_cost=$get_sightseing->sightseeing_group_child_cost;
          $calculate_total_cost+=$sightseeing_adult_cost[$dates_count]*$no_of_adults;
          if($no_of_child>0)
          {
             $calculate_total_cost+=$child_cost*$no_of_child;
          }

      }
    }
    if(!empty($transfer_id[$dates_count]))
    {
      $booking_details_array[$dates_count]['transfer']['transfer_id']=$transfer_id[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_name']=$transfer_name[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_car_name']=$transfer_car_name[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_type']=$transfer_type[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_from_city']=$transfer_from_city[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_to_city']=$transfer_to_city[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_from_airport']=$transfer_from_airport[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_to_airport']=$transfer_to_airport[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_pickup']=$transfer_pickup[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_dropoff']=$transfer_dropoff[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_vehicle_type"]=$transfer_vehicle_type[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_guide_id"]=$transfer_guide_id[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_guide_name"]=$transfer_guide_name[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_guide_cost"]=$transfer_guide_cost[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_cost"]=$transfer_cost[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_total_cost"]=$transfer_total_cost[$dates_count];
      // $calculate_total_cost+=$transfer_cost[$dates_count]*$no_of_pax;
      $calculate_total_cost+=$transfer_total_cost[$dates_count];

    }

    if(!empty($activity_id[$dates_count]))
    {
      if(!empty($activities_select_child_age[$dates_count]))
      {
         $activities_select_child_age_array=array_values($activities_select_child_age[$dates_count]);
      }
    
    for($activity_count=0;$activity_count<count($activity_id[$dates_count]);$activity_count++)
    {
      if($activity_id[$dates_count][$activity_count]=="")
      { 
        continue;

      }
      if(!empty($activities_select_adults[$dates_count]))
      {
         $booking_activity_adult_count=$activities_select_adults[$dates_count][$activity_count];
      }
      else
      {
         $booking_activity_adult_count=0;
      }

      if(!empty($activities_select_child[$dates_count]))
      {
          $booking_activity_child_count=$activities_select_child[$dates_count][$activity_count];
      }
      else
      {
         $booking_activity_child_count=0;
      }
     
     if($booking_activity_child_count>0)
     {
      if(!empty($activities_select_child_age_array[$activity_count]))
      {
       $booking_activities_select_child_age=$activities_select_child_age_array[$activity_count];
      }
      else
      {
        $booking_activities_select_child_age=array();
      }
     }
     else
     {
      $booking_activities_select_child_age=array();
     }
     
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_id']=$activity_id[$dates_count][$activity_count];
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_name']=$activity_name[$dates_count][$activity_count];
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_cost']=$activity_cost[$dates_count][$activity_count];
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_no_of_adult']=$booking_activity_adult_count;
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_no_of_child']=$booking_activity_child_count;
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_child_age']=$booking_activities_select_child_age;

        $get_activities=Activities::where('activity_id',$activity_id[$dates_count][$activity_count])->first();
      // $child_cost=$get_activity->child_price;
      //  $calculate_total_cost+=$activity_cost[$dates_count][$activity_count]*$no_of_adults;

      // if($no_of_child>0)
      // {
      //    $calculate_total_cost+=$child_cost*$no_of_child;
      // }


          $adult_price=0;

     $adult_price_details=unserialize($get_activities->adult_price_details);
     if(!empty($adult_price_details))
     {
       $min_price=0;

       for($i=0;$i< count($adult_price_details);$i++)
       {

        if($adult_price_details[$i]['adult_min_pax']<=$booking_activity_adult_count && $adult_price_details[$i]['adult_max_pax']>=$booking_activity_adult_count)
        {
          $adult_price=$adult_price_details[$i]['adult_pax_price'];
          break;
        }

        if($adult_price_details[0]['adult_min_pax']>$booking_activity_adult_count)
        {
          $min_price++;
        }
      }

      if($adult_price==0 && $min_price>0)
      {
       $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];

     }
     else if($adult_price==0 && $min_price==0)
     {
      if($i==0)
      {
       $adult_price=$adult_price_details[0]['adult_pax_price'];
      }
      else
      {
         $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];
      }
     }

   }
   else
   {
    $adult_price=0;
    }



         $child_price=0;

          $child_price_details=unserialize($get_activities->child_price_details);
          if(!empty($child_price_details))
          {
             $min_price=0;
            for($i=0;$i< count($child_price_details);$i++)
            {

              if($child_price_details[$i]['child_min_pax']<=$booking_activity_child_count && $child_price_details[$i]['child_max_pax']>=$booking_activity_child_count)
              {
                $child_price=$child_price_details[$i]['child_pax_price'];
                break;
              }

               if($child_price_details[0]['child_min_pax']>$booking_activity_child_count)
                {
                  $min_price++;
                }
            }

            if($child_price==0 && $min_price>0)
              {
               $child_price=$child_price_details[0]['child_pax_price'];

             }
             else if($child_price==0 && $min_price==0)
             {
               if($i==0)
               { 
                 $child_price=$child_price_details[0]['child_pax_price'];
               }
               else
               {
                $child_price=$child_price_details[($i-1)]['child_pax_price'];
                }
             }

         }
         else
         {
          $child_price=0;
        }






      $supplier_adult_price=$adult_price;
      $supplier_child_price=$child_price;
      $booking_supplier_amount=0;
      if(!empty($booking_activity_adult_count))
      {
        $calculate_total_cost+=($booking_activity_adult_count*$supplier_adult_price);
      }
       if(!empty($booking_activity_child_count))
      {
        $calculate_total_cost+=($booking_activity_child_count*$supplier_child_price);
      }
      }

   }


   if(isset($restaurant_id[$dates_count]))
   {
    $restaurant_count=0;
    foreach($restaurant_id[$dates_count] as $restaurant_id_key =>$restaurant_id_value)
    {
      if($restaurant_id_value!="")
      {
         foreach($restaurant_food_id[$dates_count][$restaurant_id_key] as $restaurant_child_food_id_key =>  $restaurant_child_food_id_value)
      {
       $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_id']=$restaurant_food_id[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

       $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_name']=$restaurant_food_name[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

        $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_category_id']=$restaurant_food_category_id[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

         $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_unit']=$restaurant_food_unit[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

         $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_qty']=$restaurant_food_qty[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

          $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_price']=$restaurant_food_price[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];
          // echo $restaurant_food_qty[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]."----".$restaurant_food_price[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]."<br>";

          $calculate_total_cost+=($restaurant_food_qty[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]*$restaurant_food_price[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]);
      }

       $restaurant_count++;
      }
     

    }

   }


  }

  $booking_amount_without_markup=$calculate_total_cost;
  $calculate_markup_cost=round(($calculate_total_cost*$booking_markup_per)/100);
  $calculate_total_with_agent_markup_cost=round($calculate_total_cost+$calculate_markup_cost);
  $calculate_own_markup_cost=round(($calculate_total_with_agent_markup_cost*$booking_own_markup_per)/100);
  $calculate_total_with_markup_cost=round($calculate_total_with_agent_markup_cost+$calculate_own_markup_cost);

  $booking_amount=$calculate_total_with_markup_cost;

  return $booking_amount_without_markup."--".$calculate_total_with_agent_markup_cost."--".$booking_amount;

}
public function itinerary_booking(Request $request)
{

  // echo "<pre>";
  // print_r($request->all());
  // die();
                            
  $agent_id=session()->get('travel_agent_id');
  $itinerary_id=$request->get('itinerary_id');
  $booking_amount=$request->get('total_cost');
  $booking_markup_per=$request->get('markup_per');
  $booking_own_markup_per=$request->get('own_markup_per');
  $booking_amount_without_markup=$request->get('total_cost_w_markup');
   $booking_amount_without_agent_markup=$request->get('total_cost_w_agent_markup');
  $booking_from_date=$request->get('from_date');
  $booking_to_date=$request->get('to_date');
  $booking_dates_array=$request->get('days_dates');
  $booking_details_array=array();

  $hotel_id=$request->get('hotel_id');
  $hotel_name=$request->get('hotel_name');
  $room_name=$request->get('room_name');
  $hotel_cost=$request->get('hotel_cost');
  $hotel_no_of_days=$request->get('hotel_no_of_days');
  $rooms_qty=$request->get('room_qty');
  $hotel_checkin=$request->get('hotel_checkin');
  $hotel_checkout=$request->get('hotel_checkout');
  $hotel_room_name=$request->get('hotel_room_name');
  $hotel_room_qty=$request->get('hotel_room_qty');
  $hotel_room_cost=$request->get('hotel_room_cost');
  $hotel_room_id=$request->get('hotel_room_id');
  $hotel_occupancy_id=$request->get('hotel_occupancy_id');
  $hotel_occupancy_qty=$request->get('hotel_occupancy_qty');

  $sightseeing_id=$request->get('sightseeing_id');
  $sightseeing_name=$request->get('sightseeing_name');
  $sightseeing_tour_type=$request->get('sightseeing_tour_type');
  $sightseeing_vehicle_type=$request->get('sightseeing_vehicle_type');
  $sightseeing_guide_id=$request->get('sightseeing_guide_id');
  $sightseeing_guide_name=$request->get('sightseeing_guide_name'); 
  $sightseeing_guide_cost=$request->get('sightseeing_guide_cost');
  $sightseeing_driver_id=$request->get('sightseeing_driver_id');  
  $sightseeing_driver_name=$request->get('sightseeing_driver_name'); 
  $sightseeing_driver_cost=$request->get('sightseeing_driver_cost');
  $sightseeing_adult_cost=$request->get('sightseeing_adult_cost');
   $sightseeing_additional_cost=$request->get('sightseeing_additional_cost');
  $sightseeing_cost=$request->get('sightseeing_cost');

  $activity_id=$request->get('activity_id');
  $activity_name=$request->get('activity_name');
  $activity_cost=$request->get('activity_cost');

  $transfer_id=$request->get('transfer_id');
  $transfer_name=$request->get('transfer_name');
  $transfer_car_name=$request->get('transfer_car_name');
  $transfer_type=$request->get('transfer_type');
  $transfer_from_city=$request->get('transfer_from_city');
  $transfer_to_city=$request->get('transfer_to_city');
  $transfer_from_airport=$request->get('transfer_from_airport');
  $transfer_to_airport=$request->get('transfer_to_airport');
  $transfer_pickup=$request->get('transfer_pickup');
  $transfer_dropoff=$request->get('transfer_dropoff');
  $transfer_vehicle_type=$request->get('transfer_vehicle_type');
    $transfer_guide_id=$request->get('transfer_guide_id');
    $transfer_guide_name=$request->get('transfer_guide_name');
    $transfer_guide_cost=$request->get('transfer_guide_cost');
  $transfer_cost=$request->get('transfer_cost');
  $transfer_total_cost=$request->get('transfer_total_cost');

  $restaurant_id=$request->get('restaurant_id');
$restaurant_name=$request->get('restaurant_name');
$restaurant_food_for=$request->get('restaurant_food_for');
$restaurant_cost=$request->get('restaurant_cost');
$restaurant_food_name=$request->get('restaurant_food_name');
$restaurant_food_qty=$request->get('restaurant_food_qty');
$restaurant_food_price=$request->get('restaurant_food_price');
$restaurant_food_id=$request->get('restaurant_food_id');
$restaurant_food_category_id=$request->get('restaurant_food_category_id');
$restaurant_food_unit=$request->get('restaurant_food_unit');


  $rooms_count=$request->get('rooms_count');
  $select_adults=$request->get('select_adults');
  $select_child=$request->get('select_child');
  $child_age=$request->get('child_age');

  $no_of_adults=array_sum($select_adults);
  $no_of_child=array_sum($select_child);
  if($request->has('child_age'))
  {
     $child_age_array=serialize($child_age);
  }
  else
  {
    $child_age=array();
    $child_age_array=serialize($child_age);

  }

  $activities_select_adults=$request->get('activities_select_adults');
  $activities_select_child=$request->get('activities_select_child');
  $activities_select_child_age=$request->get('activities_select_child_age');

  // $activities_select_child_age=array_map('array_values', $activities_select_child_age);

  $no_of_pax=$no_of_adults+$no_of_child;
  $calculate_total_cost=0;
$total_rooms_count=0;
  for($dates_count=0;$dates_count<=count($booking_dates_array)-1;$dates_count++)
  {
    $booking_details_array[$dates_count]['dates']=$booking_dates_array[$dates_count];
    if(!empty($hotel_id[$dates_count]))
    {

      $booking_details_array[$dates_count]['hotel']['hotel_id']=$hotel_id[$dates_count];
      $booking_details_array[$dates_count]['hotel']['hotel_name']=$hotel_name[$dates_count];
      $booking_details_array[$dates_count]['hotel']['room_name']=$room_name[$dates_count];
      $booking_details_array[$dates_count]['hotel']['hotel_checkin']=$hotel_checkin[$dates_count];
      $booking_details_array[$dates_count]['hotel']['hotel_checkout']=$hotel_checkout[$dates_count];
      $booking_details_array[$dates_count]['hotel']['no_of_days']=$hotel_no_of_days[$dates_count];
      $booking_details_array[$dates_count]['hotel']['room_quantity']=$rooms_qty[$dates_count];
       $total_rooms_count+=$rooms_qty[$dates_count];
        if(empty($hotel_room_qty[$dates_count]))
      {
      // $booking_details_array[$dates_count]['hotel']['hotel_cost']=$hotel_cost[$dates_count]*count($rooms_count);
      // $calculate_total_cost+=$hotel_cost[$dates_count]*count($rooms_count);

      $hotel_room_name_array=$hotel_room_name[$dates_count];
        $hotel_room_cost_array=$hotel_room_cost[$dates_count];
        $hotel_total_cost=0;
          for($i=0;$i<count($hotel_room_name_array);$i++)
          {

             $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_name']=$hotel_room_name_array[$i];
             $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_qty']=$rooms_qty[$dates_count];
             $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_cost']=$hotel_room_cost_array[$i];


             $hotel_total_cost+=$hotel_room_cost_array[$i]*$rooms_qty[$dates_count];

          }
      $booking_details_array[$dates_count]['hotel']['hotel_cost']=$hotel_total_cost;
      $calculate_total_cost+=$hotel_total_cost;


      }
      else
      {
        $hotel_room_name_array=$hotel_room_name[$dates_count];
        $hotel_room_qty_array=$hotel_room_qty[$dates_count];
        $hotel_room_cost_array=$hotel_room_cost[$dates_count];
         $hotel_room_id_array=$hotel_room_id[$dates_count];
          $hotel_occupancy_id_array=$hotel_occupancy_id[$dates_count];
          $hotel_occupancy_qty_array=$hotel_occupancy_qty[$dates_count];
        $hotel_total_cost=0;
          for($i=0;$i<count($hotel_room_name_array);$i++)
          {

           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_name']=$hotel_room_name_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_qty']=$hotel_room_qty_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_cost']=$hotel_room_cost_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_id']=$hotel_room_id_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_id']=$hotel_occupancy_id_array[$i];
           $booking_details_array[$dates_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_qty']=$hotel_occupancy_qty_array[$i];

             $hotel_total_cost+=$hotel_room_cost_array[$i]*$hotel_room_qty_array[$i];

          }
      $booking_details_array[$dates_count]['hotel']['hotel_cost']=$hotel_total_cost;
      $calculate_total_cost+=$hotel_total_cost;



      }
     

    }
    if(!empty($sightseeing_id[$dates_count]))
    {
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_id"]=$sightseeing_id[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_name"]=$sightseeing_name[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_tour_type"]=$sightseeing_tour_type[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_vehicle_type"]=$sightseeing_vehicle_type[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_guide_id"]=$sightseeing_guide_id[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_guide_name"]=$sightseeing_guide_name[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_guide_cost"]=$sightseeing_guide_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_driver_id"]=$sightseeing_driver_id[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_driver_name"]=$sightseeing_driver_name[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_driver_cost"]=$sightseeing_driver_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_adult_cost"]=$sightseeing_adult_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_additional_cost"]=$sightseeing_additional_cost[$dates_count];
      $booking_details_array[$dates_count]["sightseeing"]["sightseeing_cost"]=$sightseeing_cost[$dates_count];

      if($sightseeing_tour_type[$dates_count]=="private")
      {
        $calculate_total_cost+=$sightseeing_guide_cost[$dates_count];
        $calculate_total_cost+=$sightseeing_driver_cost[$dates_count];
        $calculate_total_cost+=$sightseeing_additional_cost[$dates_count];
         $get_sightseing=Sightseeing::where('sightseeing_id',$sightseeing_id[$dates_count])->first();
        $child_cost=$get_sightseing->sightseeing_child_cost;
        $calculate_total_cost+=$sightseeing_adult_cost[$dates_count]*$no_of_adults;
        if($no_of_child>0)
        {
           $calculate_total_cost+=$child_cost*$no_of_child;
        }
      
      }
      else
      {
          $calculate_total_cost+=$sightseeing_additional_cost[$dates_count];
          $get_sightseing=Sightseeing::where('sightseeing_id',$sightseeing_id[$dates_count])->first();
          $child_cost=$get_sightseing->sightseeing_group_child_cost;
          $calculate_total_cost+=$sightseeing_adult_cost[$dates_count]*$no_of_adults;
          if($no_of_child>0)
          {
             $calculate_total_cost+=$child_cost*$no_of_child;
          }

      }
    }
    if(!empty($transfer_id[$dates_count]))
    {
      $booking_details_array[$dates_count]['transfer']['transfer_id']=$transfer_id[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_name']=$transfer_name[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_car_name']=$transfer_car_name[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_type']=$transfer_type[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_from_city']=$transfer_from_city[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_to_city']=$transfer_to_city[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_from_airport']=$transfer_from_airport[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_to_airport']=$transfer_to_airport[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_pickup']=$transfer_pickup[$dates_count];
      $booking_details_array[$dates_count]['transfer']['transfer_dropoff']=$transfer_dropoff[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_vehicle_type"]=$transfer_vehicle_type[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_guide_id"]=$transfer_guide_id[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_guide_name"]=$transfer_guide_name[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_guide_cost"]=$transfer_guide_cost[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_cost"]=$transfer_cost[$dates_count];
      $booking_details_array[$dates_count]["transfer"]["transfer_total_cost"]=$transfer_total_cost[$dates_count];
      // $calculate_total_cost+=$transfer_cost[$dates_count]*$no_of_pax;
      $calculate_total_cost+=$transfer_total_cost[$dates_count];

    }

    if(!empty($activity_id[$dates_count]))
    {
      if(!empty($activities_select_child_age[$dates_count]))
      {
         $activities_select_child_age_array=array_values($activities_select_child_age[$dates_count]);
      }
    
    for($activity_count=0;$activity_count<count($activity_id[$dates_count]);$activity_count++)
    {
      if($activity_id[$dates_count][$activity_count]=="")
      { 
        continue;

      }
      if(!empty($activities_select_adults[$dates_count]))
      {
         $booking_activity_adult_count=$activities_select_adults[$dates_count][$activity_count];
      }
      else
      {
         $booking_activity_adult_count=0;
      }

      if(!empty($activities_select_child[$dates_count]))
      {
          $booking_activity_child_count=$activities_select_child[$dates_count][$activity_count];
      }
      else
      {
         $booking_activity_child_count=0;
      }
     
     if($booking_activity_child_count>0)
     {
         if(!empty($activities_select_child_age_array[$activity_count]))
      {
       $booking_activities_select_child_age=$activities_select_child_age_array[$activity_count];
      }
      else
      {
        $booking_activities_select_child_age=array();
      }
     }
     else
     {
      $booking_activities_select_child_age=array();
     }
     
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_id']=$activity_id[$dates_count][$activity_count];
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_name']=$activity_name[$dates_count][$activity_count];
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_cost']=$activity_cost[$dates_count][$activity_count];
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_no_of_adult']=$booking_activity_adult_count;
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_no_of_child']=$booking_activity_child_count;
      $booking_details_array[$dates_count]['activity'][$activity_count]['activity_child_age']=$booking_activities_select_child_age;

        $get_activities=Activities::where('activity_id',$activity_id[$dates_count][$activity_count])->first();
      // $child_cost=$get_activity->child_price;
      //  $calculate_total_cost+=$activity_cost[$dates_count][$activity_count]*$no_of_adults;

      // if($no_of_child>0)
      // {
      //    $calculate_total_cost+=$child_cost*$no_of_child;
      // }


          $adult_price=0;

     $adult_price_details=unserialize($get_activities->adult_price_details);
     if(!empty($adult_price_details))
     {
       $min_price=0;

       for($i=0;$i< count($adult_price_details);$i++)
       {

        if($adult_price_details[$i]['adult_min_pax']<=$booking_activity_adult_count && $adult_price_details[$i]['adult_max_pax']>=$booking_activity_adult_count)
        {
          $adult_price=$adult_price_details[$i]['adult_pax_price'];
          break;
        }

        if($adult_price_details[0]['adult_min_pax']>$booking_activity_adult_count)
        {
          $min_price++;
        }
      }

      if($adult_price==0 && $min_price>0)
      {
       $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];

     }
     else if($adult_price==0 && $min_price==0)
     {
      if($i==0)
      {
       $adult_price=$adult_price_details[0]['adult_pax_price'];
      }
      else
      {
         $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];
      }
     }

   }
   else
   {
    $adult_price=0;
    }



         $child_price=0;

          $child_price_details=unserialize($get_activities->child_price_details);
          if(!empty($child_price_details))
          {
             $min_price=0;
            for($i=0;$i< count($child_price_details);$i++)
            {

              if($child_price_details[$i]['child_min_pax']<=$booking_activity_child_count && $child_price_details[$i]['child_max_pax']>=$booking_activity_child_count)
              {
                $child_price=$child_price_details[$i]['child_pax_price'];
                break;
              }

               if($child_price_details[0]['child_min_pax']>$booking_activity_child_count)
                {
                  $min_price++;
                }
            }

            if($child_price==0 && $min_price>0)
              {
               $child_price=$child_price_details[0]['child_pax_price'];

             }
             else if($child_price==0 && $min_price==0)
             {
               if($i==0)
               { 
                 $child_price=$child_price_details[0]['child_pax_price'];
               }
               else
               {
                $child_price=$child_price_details[($i-1)]['child_pax_price'];
                }
             }

         }
         else
         {
          $child_price=0;
        }






      $supplier_adult_price=$adult_price;
      $supplier_child_price=$child_price;
      $booking_supplier_amount=0;
      if(!empty($booking_activity_adult_count))
      {
        $calculate_total_cost+=($booking_activity_adult_count*$supplier_adult_price);
      }
       if(!empty($booking_activity_child_count))
      {
        $calculate_total_cost+=($booking_activity_child_count*$supplier_child_price);
      }
      }

   }

   if(isset($restaurant_id[$dates_count]))
   {
    $restaurant_count=0;
    foreach($restaurant_id[$dates_count] as $restaurant_id_key =>$restaurant_id_value)
    {
      if($restaurant_id_value!="")
      {
        
      $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['restaurant_id']=$restaurant_id[$dates_count][$restaurant_id_key];
      $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['restaurant_name']=$restaurant_name[$dates_count][$restaurant_id_key];
      $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['restaurant_cost']=$restaurant_cost[$dates_count][$restaurant_id_key];
       $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['restaurant_food_for']=$restaurant_food_for[$dates_count][$restaurant_id_key];
         foreach($restaurant_food_id[$dates_count][$restaurant_id_key] as $restaurant_child_food_id_key =>  $restaurant_child_food_id_value)
      {

       $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_id']=$restaurant_food_id[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

       $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_name']=$restaurant_food_name[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

        $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_category_id']=$restaurant_food_category_id[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

         $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_unit']=$restaurant_food_unit[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

         $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_qty']=$restaurant_food_qty[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];

          $booking_details_array[$dates_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_price']=$restaurant_food_price[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key];
          // echo $restaurant_food_qty[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]."----".$restaurant_food_price[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]."<br>";

          $calculate_total_cost+=($restaurant_food_qty[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]*$restaurant_food_price[$dates_count][$restaurant_id_key][$restaurant_child_food_id_key]);
      }

       $restaurant_count++;
      }
     

    }

   }


  }
  // if(!empty($transfer_id[$dates_count]))
  // {
  //   $booking_details_array[$dates_count]['transfer']['transfer_id']=$transfer_id[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_name']=$transfer_name[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_car_name']=$transfer_car_name[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_type']=$transfer_type[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_from_city']=$transfer_from_city[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_to_city']=$transfer_to_city[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_from_airport']=$transfer_from_airport[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_to_airport']=$transfer_to_airport[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_pickup']=$transfer_pickup[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_dropoff']=$transfer_dropoff[$dates_count];
  //   $booking_details_array[$dates_count]['transfer']['transfer_cost']=$transfer_cost[$dates_count];
  //   $calculate_total_cost+=$transfer_cost[$dates_count]*$no_of_pax;
  // }
  $booking_amount_without_markup=$calculate_total_cost;
  $calculate_markup_cost=round(($calculate_total_cost*$booking_markup_per)/100);
  $calculate_total_with_agent_markup_cost=round($calculate_total_cost+$calculate_markup_cost);
  $calculate_own_markup_cost=round(($calculate_total_with_agent_markup_cost*$booking_own_markup_per)/100);
  $calculate_total_with_markup_cost=round($calculate_total_with_agent_markup_cost+$calculate_own_markup_cost);

  $booking_amount=$calculate_total_with_markup_cost;

  $booking_currency="GEL";
  $booking_array=array("agent_id"=>$agent_id,
    "itinerary_id"=>$itinerary_id,
    "booking_amount"=>$booking_amount,
    "booking_amount_without_markup"=>$booking_amount_without_markup,
     "booking_amount_without_agent_markup"=>$calculate_total_with_agent_markup_cost,
    "booking_from_date"=>$booking_from_date,
    "booking_to_date"=>$booking_to_date,
    "booking_rooms_count"=>$total_rooms_count,
    "booking_adult_count"=>$no_of_adults,
    "booking_child_count"=>$no_of_child,
    "booking_child_age"=>$child_age_array,
    "booking_markup_per"=>$booking_markup_per,
    "booking_own_markup_per"=>$booking_own_markup_per,
    "itinerary_currency"=>$booking_currency,
    "booking_details_array"=>$booking_details_array
  );
  $get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->where('itinerary_status',1)->first();
  $countries=Countries::get();
  return view('agent.booking-page')->with(compact('booking_array','get_itinerary','countries'))->with('booking_type','package');
}
public function confirm_itinerary_booking(Request $request)
{
    // echo "<pre>";
    //                             print_r($request->all());
    //                             die();
  $agent_id=session()->get('travel_agent_id');
  $fetch_admin=Users::where('users_pid',0)->first();
  $fetch_agent=Agents::where('agent_id',$agent_id)->first();
  $customer_name=$request->get('customer_name');
  $customer_email=$request->get('customer_email');  
  $customer_phone=$request->get('customer_phone');  
  $customer_country=$request->get('customer_country');  
  $customer_address=$request->get('customer_address'); 
  $customer_remarks=$request->get('customer_remarks'); 
  $booking_whole_data=$request->get('booking_details');
  $booking_details=unserialize($request->get('booking_details'));
  $itinerary_id=$booking_details['itinerary_id'];
  $booking_amount=$booking_details['booking_amount'];

  $booking_amount_without_markup=$booking_details['booking_amount_without_markup'];
  $booking_amount_without_agent_markup=$booking_details['booking_amount_without_agent_markup'];
  $booking_from_date=$booking_details['booking_from_date'];
  $booking_to_date=$booking_details['booking_to_date'];
  $booking_adult_count=$booking_details['booking_adult_count'];
  $booking_child_count=$booking_details['booking_child_count'];
  $booking_child_age=$booking_details['booking_child_age'];
  $booking_rooms_count=$booking_details['booking_rooms_count'];
  $booking_markup_per=$booking_details['booking_markup_per'];
  $booking_own_markup_per=$booking_details['booking_own_markup_per'];
     $booking_attachments=array();
//multifile uploading
  if($request->hasFile('customer_file'))
  {
    foreach($request->file('customer_file') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_activity = "booking-attachment-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/booking_attachments/';
        $file->move($dir1, $image_activity);
        $booking_attachments[]=$image_activity;
      }
    }
  }
  $booking_attachments=serialize($booking_attachments);

  $get_itinerary=SavedItinerary::where('itinerary_id',$itinerary_id)->where('itinerary_status',1)->first();
  if($get_itinerary)
  {

    $room_currency_array=array("USD","EUR");
    $currency_price_array=array();
      foreach($room_currency_array as $currency)
      {
        if($currency=="GEL")
        {

        }
        else
        {
           $new_currency=$currency;

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$new_currency."_".$this->base_currency;

        //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
          
        //  $currency_price_array[$currency]=round($conversion_price,2);
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);
        }
         
      }

      $currency_price_array["GEL"]=1;


    $get_itinerary_id=Bookings::latest()->value('booking_sep_id');
    $get_itinerary_id=$get_itinerary_id+1;
    $booking_currency=$booking_details['itinerary_currency']; 
    $date=date("Y-m-d");
    $time=date("H:i:s");
    $insert_booking=new Bookings;
    $insert_booking->booking_type="itinerary";
    $insert_booking->booking_sep_id=$get_itinerary_id;
    $insert_booking->booking_type_id=$itinerary_id;
    $insert_booking->booking_agent_id=$agent_id;
    $insert_booking->booking_role="AGENT";
    $insert_booking->customer_name=$customer_name;
    $insert_booking->customer_contact=$customer_phone;
    $insert_booking->customer_email=$customer_email;
    $insert_booking->customer_country=$customer_country;
    $insert_booking->customer_address=$customer_address; 
    $insert_booking->booking_remarks=$customer_remarks;
    $insert_booking->booking_adult_count=$booking_adult_count;
    $insert_booking->booking_child_count=$booking_child_count;
    $insert_booking->booking_rooms_count=$booking_rooms_count;
    $insert_booking->booking_child_age=$booking_child_age;
    $insert_booking->booking_currency=$booking_currency;
    $insert_booking->booking_currency_conversions=serialize($currency_price_array);
    $insert_booking->booking_supplier_amount=$booking_amount_without_markup;
    $insert_booking->booking_agent_amount=$booking_amount_without_agent_markup;
    $insert_booking->booking_amount=$booking_amount;
    $insert_booking->booking_amount_without_markup=$booking_amount_without_markup;
    $insert_booking->booking_markup_per=$booking_markup_per;
    $insert_booking->booking_own_markup_per=$booking_own_markup_per;
    $insert_booking->booking_selected_date=$booking_from_date;
    $insert_booking->booking_selected_to_date=$booking_to_date;
    $insert_booking->booking_whole_data=$booking_whole_data;
    $insert_booking->booking_attachments=$booking_attachments;
    $insert_booking->booking_date=$date;
    $insert_booking->booking_time=$time;
    if($insert_booking->save())
    {
      $booking_array_count=$booking_details['booking_details_array'];
      for($booking_count=0;$booking_count <= count($booking_array_count)-1;$booking_count++)
      {
        $date=date("Y-m-d");
        $time=date("H:i:s");
        if(!empty($booking_array_count[$booking_count]['hotel']))
        {
          $booking_room_quantity_details=array();
          foreach($booking_array_count[$booking_count]['hotel']['hotel_room_detail'] as $hotel_room_detail)
          {
            $room_id=$room_occupancy_id=$room_occupancy_qty=$room_qty="";
            if(!empty($hotel_room_detail['room_id']))
            {
              $room_id=$hotel_room_detail['room_id'];
            }

            if(!empty($hotel_room_detail['room_occupancy_id']))
            {
              $room_occupancy_id=$hotel_room_detail['room_occupancy_id'];
            }

            if(!empty($hotel_room_detail['room_occupancy_qty']))
            {
              $room_occupancy_qty=$hotel_room_detail['room_occupancy_qty'];
            }
             if(!empty($hotel_room_detail['room_qty']))
            {
              $room_qty=$hotel_room_detail['room_qty'];
            }

             $booking_room_quantity_details[]=$room_id."---".$room_occupancy_id."---".$room_occupancy_qty."---".$room_qty;
            
          }
         

          $get_hotels=Hotels::where('hotel_id',$booking_array_count[$booking_count]['hotel']['hotel_id'])->first();
          $insert_booking=new Bookings;
          $insert_booking->booking_sep_id=$get_itinerary_id;
          $insert_booking->booking_type="hotel";
          $insert_booking->booking_type_id=$booking_array_count[$booking_count]['hotel']['hotel_id'];
          $insert_booking->booking_agent_id=$agent_id;
          $insert_booking->booking_role="AGENT";
          $insert_booking->booking_supplier_id=$get_hotels['supplier_id'];
          $insert_booking->customer_name=$customer_name;
          $insert_booking->customer_contact=$customer_phone;
          $insert_booking->customer_email=$customer_email;
          $insert_booking->customer_country=$customer_country;
          $insert_booking->customer_address=$customer_address; 
          $insert_booking->booking_remarks=$customer_remarks;
          $insert_booking->booking_rooms_count=$booking_array_count[$booking_count]['hotel']['room_quantity'];
          $insert_booking->booking_rooms_count_details=implode(",",$booking_room_quantity_details);
          $insert_booking->booking_adult_count=$booking_adult_count;
          $insert_booking->booking_child_count=$booking_child_count;
           $insert_booking->booking_child_age=$booking_child_age;
          $insert_booking->booking_subject_name=$booking_array_count[$booking_count]['hotel']['room_name'];
          $insert_booking->booking_subject_days=$booking_array_count[$booking_count]['hotel']['no_of_days'];
          $insert_booking->booking_currency=$booking_currency;
            $insert_booking->booking_currency_conversions=serialize($currency_price_array);
          // $insert_booking->booking_amount=round($booking_array_count[$booking_count]['hotel']['hotel_cost']*$booking_array_count[$booking_count]['hotel']['room_quantity']);
          $insert_booking->booking_supplier_amount=round($booking_array_count[$booking_count]['hotel']['hotel_cost']);
          $insert_booking->booking_agent_amount=round($booking_array_count[$booking_count]['hotel']['hotel_cost']);
          $insert_booking->booking_amount=round($booking_array_count[$booking_count]['hotel']['hotel_cost']);
          $insert_booking->supplier_adult_price=round($booking_array_count[$booking_count]['hotel']['hotel_cost']);
          $insert_booking->agent_adult_price=round($booking_array_count[$booking_count]['hotel']['hotel_cost']);
           $insert_booking->customer_adult_price=round($booking_array_count[$booking_count]['hotel']['hotel_cost']);
          $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['hotel']['hotel_checkin'];
          $insert_booking->booking_selected_to_date=$booking_array_count[$booking_count]['hotel']['hotel_checkout'];
          if(!empty($booking_array_count[$booking_count]['hotel']['hotel_room_detail']))
          {
             $insert_booking->booking_other_info=serialize($booking_array_count[$booking_count]['hotel']['hotel_room_detail']);
          }
         
          $insert_booking->booking_date=$date;
          $insert_booking->booking_time=$time;
          $insert_booking->itinerary_status=1;
          $insert_booking->save();
        }
        if(!empty($booking_array_count[$booking_count]['activity']))
        {
          for($activity_count=0;$activity_count <count($booking_array_count[$booking_count]['activity']);$activity_count++)
          {
            if(!isset($booking_array_count[$booking_count]['activity'][$activity_count]))
            {
              continue;
            }


           $get_activities=Activities::where('activity_id',$booking_array_count[$booking_count]['activity'][$activity_count]['activity_id'])->first();


           $activity_no_of_adult=$booking_array_count[$booking_count]['activity'][$activity_count]['activity_no_of_adult'];
           $activity_no_of_child=$booking_array_count[$booking_count]['activity'][$activity_count]['activity_no_of_child'];
           $activity_child_age=$booking_array_count[$booking_count]['activity'][$activity_count]['activity_child_age'];
          $total_adult_cost=0;
          $total_child_cost=0;
           // $child_cost=$get_activities->child_price;
           // $total_adult_cost=round($booking_array_count[$booking_count]['activity'][$activity_count]['activity_cost']*$booking_adult_count);
           // $total_child_cost=round($child_cost*$booking_child_count);
           // $total_cost=$total_adult_cost+$total_child_cost;
                 $adult_price=0;

     $adult_price_details=unserialize($get_activities->adult_price_details);
     if(!empty($adult_price_details))
     {
       $min_price=0;

       for($i=0;$i< count($adult_price_details);$i++)
       {

        if($adult_price_details[$i]['adult_min_pax']<=$activity_no_of_adult && $adult_price_details[$i]['adult_max_pax']>=$activity_no_of_adult)
        {
          $adult_price=$adult_price_details[$i]['adult_pax_price'];
          break;
        }

        if($adult_price_details[0]['adult_min_pax']>$activity_no_of_adult)
        {
          $min_price++;
        }
      }

      if($adult_price==0 && $min_price>0)
      {
       $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];

     }
     else if($adult_price==0 && $min_price==0)
     {
      if($i==0){
        $adult_price=$adult_price_details[0]['adult_pax_price'];
      }
      else
      {
         $adult_price=$adult_price_details[($i-1)]['adult_pax_price'];
      }


      
     }

   }
   else
   {
    $adult_price=0;
    }



         $child_price=0;

          $child_price_details=unserialize($get_activities->child_price_details);
          if(!empty($child_price_details))
          {
             $min_price=0;
            for($i=0;$i< count($child_price_details);$i++)
            {

              if($child_price_details[$i]['child_min_pax']<=$activity_no_of_child && $child_price_details[$i]['child_max_pax']>=$activity_no_of_child)
              {
                $child_price=$child_price_details[$i]['child_pax_price'];
                break;
              }

               if($child_price_details[0]['child_min_pax']>$activity_no_of_child)
                {
                  $min_price++;
                }
            }

            if($child_price==0 && $min_price>0)
              {
               $child_price=$child_price_details[0]['child_pax_price'];

             }
             else if($child_price==0 && $min_price==0)
             {
               if($i==0){
                $child_price=$child_price_details[0]['child_pax_price'];
              }
              else
              {
                $child_price=$child_price_details[($i-1)]['child_pax_price'];
              }

              
             }

         }
         else
         {
          $child_price=0;
        }






      $supplier_adult_price=$adult_price;
      $supplier_child_price=$child_price;
      $booking_supplier_amount=0;
      if(!empty($activity_no_of_adult))
      {
        $total_adult_cost=($activity_no_of_adult*$supplier_adult_price);
      }
       if(!empty($activity_no_of_child))
      {
        $total_child_cost=($activity_no_of_child*$supplier_child_price);
      }
      $total_cost=$total_adult_cost+$total_child_cost;


           $insert_booking=new Bookings;
           $insert_booking->booking_sep_id=$get_itinerary_id;
           $insert_booking->booking_type="activity";
           $insert_booking->booking_type_id=$booking_array_count[$booking_count]['activity'][$activity_count]['activity_id'];
           $insert_booking->booking_agent_id=$agent_id;
           $insert_booking->booking_role="AGENT";
           $insert_booking->booking_supplier_id=$get_activities['supplier_id'];
           $insert_booking->customer_name=$customer_name;
           $insert_booking->customer_contact=$customer_phone;
           $insert_booking->customer_email=$customer_email;
           $insert_booking->customer_country=$customer_country;
           $insert_booking->customer_address=$customer_address; 
           $insert_booking->booking_remarks=$customer_remarks;
           $insert_booking->booking_adult_count=$activity_no_of_adult;
           $insert_booking->booking_child_count=$activity_no_of_child;
            $insert_booking->booking_child_age=serialize($activity_child_age);
           $insert_booking->booking_currency=$get_activities['activity_currency'];
           $insert_booking->supplier_adult_price=$supplier_adult_price;
            $insert_booking->supplier_child_price=$supplier_child_price;
            $insert_booking->agent_adult_price=$supplier_adult_price;
           $insert_booking->agent_child_price=$supplier_child_price;
            $insert_booking->customer_adult_price=$supplier_adult_price;
           $insert_booking->customer_child_price=$supplier_child_price;
           $insert_booking->booking_supplier_amount=$total_cost;
          $insert_booking->booking_agent_amount=$total_cost;
           $insert_booking->booking_amount=$total_cost;
           $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['dates'];
           $insert_booking->booking_date=$date;
           $insert_booking->booking_time=$time;
           $insert_booking->itinerary_status=1;
           $insert_booking->save();
         }

       
       }
       if(!empty($booking_array_count[$booking_count]['sightseeing']))
       {


        $get_sightseeing=SightSeeing::where('sightseeing_id',$booking_array_count[$booking_count]['sightseeing']['sightseeing_id'])->first();


       


    if($booking_array_count[$booking_count]['sightseeing']['sightseeing_tour_type']=="private")
    {
      $supplier_adult_price=$get_sightseeing->sightseeing_adult_cost;
      $supplier_child_price=$get_sightseeing->sightseeing_child_cost;
      $sightseeing_food_cost=$get_sightseeing->sightseeing_food_cost;
      $sightseeing_hotel_cost=$get_sightseeing->sightseeing_hotel_cost;
      $other_expenses=array("food_cost"=>$sightseeing_food_cost,
        "hotel_cost"=>$sightseeing_hotel_cost);
      $booking_vehicle_guide_name=array("tour_type"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_tour_type'],
        "vehicle_type_id"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_vehicle_type'],
        "guide_id"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_id'],
        "guide_name"=> $booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_name'],
        "guide_cost"=> $booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'],
        "driver_id"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_id'],
        "driver_name"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_name'],
        "driver_cost"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'],
       "additional_cost"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_additional_cost']);
         $total_cost=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'];
         $total_cost+=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'];
           $total_cost+=$booking_array_count[$booking_count]['sightseeing']['sightseeing_additional_cost'];

         $total_adult_cost=round($booking_array_count[$booking_count]['sightseeing']['sightseeing_adult_cost']*$booking_adult_count);

           $total_child_cost=round($supplier_child_price*$booking_child_count);


           $total_cost+=($total_adult_cost+$total_child_cost);


              $fetch_guide_id=Guides::where('guide_id',$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_id'])->first();
            $booking_guide_supplier_id=$fetch_guide_id['guide_supplier_id'];


            $fetch_driver_id=Drivers::where('driver_id',$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_id'])->first();
            $booking_driver_supplier_id=$fetch_driver_id['driver_supplier_id'];



    }
    else
    {
     $supplier_adult_price=$get_sightseeing->sightseeing_group_adult_cost;
     $supplier_child_price=$get_sightseeing->sightseeing_group_child_cost;
     $other_expenses=array();
     $booking_vehicle_guide_name=array("tour_type"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_tour_type'],
       "additional_cost"=>$booking_array_count[$booking_count]['sightseeing']['sightseeing_additional_cost']);

      $total_cost=$booking_array_count[$booking_count]['sightseeing']['sightseeing_additional_cost'];
         $total_adult_cost=round($booking_array_count[$booking_count]['sightseeing']['sightseeing_adult_cost']*$booking_adult_count);
           $total_child_cost=round($supplier_child_price*$booking_child_count);

           
           $total_cost+=($total_adult_cost+$total_child_cost);
           $booking_driver_supplier_id="";
           $booking_driver_supplier_id="";
   }




        $insert_booking=new Bookings;
        $insert_booking->booking_sep_id=$get_itinerary_id;
        $insert_booking->booking_type="sightseeing";
        $insert_booking->booking_type_id=$booking_array_count[$booking_count]['sightseeing']['sightseeing_id'];
        $insert_booking->booking_supplier_id=$booking_guide_supplier_id.",".$booking_driver_supplier_id;
        $insert_booking->booking_agent_id=$agent_id;
        $insert_booking->booking_role="AGENT";
        $insert_booking->customer_name=$customer_name;
        $insert_booking->customer_contact=$customer_phone;
        $insert_booking->customer_email=$customer_email;
        $insert_booking->customer_country=$customer_country;
        $insert_booking->customer_address=$customer_address; 
        $insert_booking->booking_remarks=$customer_remarks;
        $insert_booking->booking_adult_count=$booking_adult_count;
        $insert_booking->booking_child_count=$booking_child_count;
         $insert_booking->booking_child_age=$booking_child_age;
          $insert_booking->booking_subject_name=serialize($booking_vehicle_guide_name);
        $insert_booking->booking_currency=$booking_currency;
        $insert_booking->supplier_adult_price=$supplier_adult_price;
        $insert_booking->supplier_child_price=$supplier_child_price;
        $insert_booking->agent_adult_price=$supplier_adult_price;
        $insert_booking->agent_child_price=$supplier_child_price;
        $insert_booking->customer_adult_price=$supplier_adult_price;
        $insert_booking->customer_child_price=$supplier_child_price;
        $insert_booking->other_expenses=serialize($other_expenses);
        $insert_booking->booking_supplier_amount=$total_cost;
        $insert_booking->booking_agent_amount=$total_cost;
        $insert_booking->booking_amount=$total_cost;
        $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['dates'];
         $insert_booking->booking_supplier_status="1";
 $insert_booking->booking_supplier_message="Confirmed";
        $insert_booking->booking_date=$date;
        $insert_booking->booking_time=$time;
        $insert_booking->itinerary_status=1;
        $insert_booking->save();

        if($booking_array_count[$booking_count]['sightseeing']['sightseeing_tour_type']=="private")
    {
      $sightseeing_inserted_id=$insert_booking->id;

        $insert_booking=new Bookings;
    $insert_booking->booking_sep_id=$get_itinerary_id;
    $insert_booking->booking_type="guide";
    $insert_booking->booking_type_id=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_id'];
    $insert_booking->booking_sightseeing_id=$sightseeing_inserted_id;
    $insert_booking->booking_agent_id=$agent_id;
    $insert_booking->booking_role="AGENT";
    $insert_booking->booking_supplier_id=$booking_guide_supplier_id;
    $insert_booking->customer_name=$customer_name;
    $insert_booking->customer_contact=$customer_phone;
    $insert_booking->customer_email=$customer_email;
    $insert_booking->customer_country=$customer_country;
    $insert_booking->customer_address=$customer_address;
    $insert_booking->booking_adult_count=1;
    $insert_booking->booking_subject_days=1;
    $insert_booking->supplier_adult_price=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'];
    $insert_booking->agent_adult_price=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'];
    $insert_booking->customer_adult_price=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'];
    $insert_booking->booking_currency=$booking_currency;
    $insert_booking->booking_supplier_amount=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'];
    $insert_booking->booking_agent_amount=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'];
    $insert_booking->booking_amount=$booking_array_count[$booking_count]['sightseeing']['sightseeing_guide_cost'];
    $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['dates'];
    $insert_booking->booking_selected_to_date=$booking_array_count[$booking_count]['dates'];
    $insert_booking->booking_date=$date;
    $insert_booking->booking_time=$time;
     $insert_booking->itinerary_status=1;
    $insert_booking->save();

    $insert_booking=new Bookings;
    $insert_booking->booking_sep_id=$get_itinerary_id;
    $insert_booking->booking_type="driver";
    $insert_booking->booking_type_id=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_id'];
    $insert_booking->booking_sightseeing_id=$sightseeing_inserted_id;
    $insert_booking->booking_agent_id=$agent_id;
    $insert_booking->booking_role="AGENT";
    $insert_booking->booking_supplier_id=$booking_driver_supplier_id;
    $insert_booking->customer_name=$customer_name;
    $insert_booking->customer_contact=$customer_phone;
    $insert_booking->customer_email=$customer_email;
    $insert_booking->customer_country=$customer_country;
    $insert_booking->customer_address=$customer_address;
    $insert_booking->booking_adult_count=1;
    $insert_booking->booking_subject_days=1;
    $insert_booking->supplier_adult_price=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'];
    $insert_booking->agent_adult_price=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'];
    $insert_booking->customer_adult_price=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'];
    $insert_booking->booking_currency=$booking_currency;
    $insert_booking->booking_supplier_amount=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'];
    $insert_booking->booking_agent_amount=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'];
    $insert_booking->booking_amount=$booking_array_count[$booking_count]['sightseeing']['sightseeing_driver_cost'];
    $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['dates'];
    $insert_booking->booking_selected_to_date=$booking_array_count[$booking_count]['dates'];
    $insert_booking->booking_date=$date;
    $insert_booking->booking_time=$time;
     $insert_booking->itinerary_status=1;
    $insert_booking->save();



    }

      }
      if(!empty($booking_array_count[$booking_count]['transfer']))
      {
        $get_transfer=Transfers::where('transfer_id',$booking_array_count[$booking_count]['transfer']['transfer_id'])->first();
        if($booking_array_count[$booking_count]['transfer']['transfer_type']=="city")
        {
          $booking_other_info=array(
            "transfer_type"=>$booking_array_count[$booking_count]['transfer']['transfer_type'],
            "transfer_car_name"=>$booking_array_count[$booking_count]['transfer']['transfer_car_name'],
            "from_city"=>$booking_array_count[$booking_count]['transfer']['transfer_from_city'],
            "to_city"=>$booking_array_count[$booking_count]['transfer']['transfer_to_city'],
            "transfer_pickup_location"=>$booking_array_count[$booking_count]['transfer']['transfer_pickup'],
            "transfer_dropoff_location"=>$booking_array_count[$booking_count]['transfer']['transfer_dropoff']);
        }
        else if($booking_array_count[$booking_count]['transfer']['transfer_type']=="from-airport")
        {
          $booking_other_info=array(
            "transfer_type"=>$booking_array_count[$booking_count]['transfer']['transfer_type'],
            "transfer_car_name"=>$booking_array_count[$booking_count]['transfer']['transfer_car_name'],
            "from_airport"=>$booking_array_count[$booking_count]['transfer']['transfer_from_airport'],
            "to_city"=>$booking_array_count[$booking_count]['transfer']['transfer_to_city'],
            "transfer_dropoff_location"=>$booking_array_count[$booking_count]['transfer']['transfer_dropoff']);
        }
        else
        {
         $booking_other_info=array(
          "transfer_type"=>$booking_array_count[$booking_count]['transfer']['transfer_type'],
          "transfer_car_name"=>$booking_array_count[$booking_count]['transfer']['transfer_car_name'],
          "from_city"=>$booking_array_count[$booking_count]['transfer']['transfer_from_city'],
          "to_airport"=>$booking_array_count[$booking_count]['transfer']['transfer_to_airport'],
          "transfer_pickup_location"=>$booking_array_count[$booking_count]['transfer']['transfer_pickup']);
       }

        if($booking_array_count[$booking_count]['transfer']['transfer_type']!="city")
    {

     if($booking_array_count[$booking_count]['transfer']['transfer_guide_id']!="")
     {
       $booking_other_info["vehicle_type_id"]=$booking_array_count[$booking_count]['transfer']['transfer_vehicle_type'];

      $booking_other_info["guide_id"]=$booking_array_count[$booking_count]['transfer']['transfer_guide_id'];
       $booking_other_info["guide_name"]=$booking_array_count[$booking_count]['transfer']['transfer_guide_name'];
       $booking_other_info["guide_cost"]=$booking_array_count[$booking_count]['transfer']['transfer_guide_cost'];

     }
  }

       $booking_other_info=serialize($booking_other_info);
       $insert_booking=new Bookings;
       $insert_booking->booking_sep_id=$get_itinerary_id;
       $insert_booking->booking_type="transfer";
       $insert_booking->booking_type_id=$booking_array_count[$booking_count]['transfer']['transfer_id'];
       $insert_booking->booking_agent_id=$agent_id;
       $insert_booking->booking_role="AGENT";
       $insert_booking->booking_supplier_id=$get_transfer['supplier_id'];
       $insert_booking->customer_name=$customer_name;
       $insert_booking->customer_contact=$customer_phone;
       $insert_booking->customer_email=$customer_email;
       $insert_booking->customer_country=$customer_country;
       $insert_booking->customer_address=$customer_address;
       $insert_booking->booking_remarks=$customer_remarks;
       $insert_booking->booking_adult_count=$booking_adult_count;
       $insert_booking->booking_child_count=$booking_child_count;
        $insert_booking->booking_child_age=$booking_child_age;
        // $insert_booking->booking_vehicle_count=($booking_adult_count+$booking_child_count);
       $insert_booking->booking_vehicle_count=1;
       $insert_booking->booking_subject_name=$booking_array_count[$booking_count]['transfer']['transfer_name'];
       $insert_booking->supplier_vehicle_price=$booking_array_count[$booking_count]['transfer']['transfer_cost'];
       $insert_booking->agent_vehicle_price=$booking_array_count[$booking_count]['transfer']['transfer_cost'];
        $insert_booking->customer_vehicle_price=$booking_array_count[$booking_count]['transfer']['transfer_cost'];
       $insert_booking->booking_currency=$booking_currency;
        $insert_booking->booking_supplier_amount=round($booking_array_count[$booking_count]['transfer']['transfer_total_cost']);
         $insert_booking->booking_agent_amount=round($booking_array_count[$booking_count]['transfer']['transfer_total_cost']);
       $insert_booking->booking_amount=round($booking_array_count[$booking_count]['transfer']['transfer_total_cost']);
       $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['dates'];
       $insert_booking->booking_other_info=$booking_other_info;
       $insert_booking->booking_date=$date;
       $insert_booking->booking_time=$time;
       $insert_booking->itinerary_status=1;
        if($insert_booking->save())
      {
        if($booking_array_count[$booking_count]['transfer']['transfer_guide_id']!="")
        {
         $transfer_inserted_id=$insert_booking->id;
         
          $fetch_guide_id=Guides::where('guide_id',$booking_array_count[$booking_count]['transfer']['transfer_guide_id'])->first();
            $booking_guide_supplier_id=$fetch_guide_id['guide_supplier_id'];

        $insert_booking=new Bookings;
    $insert_booking->booking_sep_id=$get_itinerary_id;
    $insert_booking->booking_type="guide";
    $insert_booking->booking_type_id=$booking_array_count[$booking_count]['transfer']['transfer_guide_id'];
    $insert_booking->booking_sightseeing_id=$transfer_inserted_id;
    $insert_booking->booking_agent_id=$agent_id;
    $insert_booking->booking_role="AGENT";
    $insert_booking->booking_supplier_id=$booking_guide_supplier_id;
    $insert_booking->customer_name=$customer_name;
    $insert_booking->customer_contact=$customer_phone;
    $insert_booking->customer_email=$customer_email;
    $insert_booking->customer_country=$customer_country;
    $insert_booking->customer_address=$customer_address;
    $insert_booking->booking_adult_count=1;
    $insert_booking->booking_subject_days=1;
    $insert_booking->supplier_adult_price=$booking_array_count[$booking_count]['transfer']['transfer_guide_cost'];
    $insert_booking->agent_adult_price=$booking_array_count[$booking_count]['transfer']['transfer_guide_cost'];
    $insert_booking->customer_adult_price=$booking_array_count[$booking_count]['transfer']['transfer_guide_cost'];
    $insert_booking->booking_currency=$booking_currency;
    $insert_booking->booking_supplier_amount=$booking_array_count[$booking_count]['transfer']['transfer_guide_cost'];
    $insert_booking->booking_agent_amount=$booking_array_count[$booking_count]['transfer']['transfer_guide_cost'];
    $insert_booking->booking_amount=$booking_array_count[$booking_count]['transfer']['transfer_guide_cost'];
    $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['dates'];
    $insert_booking->booking_selected_to_date=$booking_array_count[$booking_count]['dates'];
    $insert_booking->booking_date=$date;
    $insert_booking->booking_time=$time;
     $insert_booking->itinerary_status=1;
    $insert_booking->save();
  }


      }

     }

     if(!empty($booking_array_count[$booking_count]['restaurant']))
     {
      $restaurants=$booking_array_count[$booking_count]['restaurant'];
      foreach($restaurants as $restaurant_key =>$restaurant_value)
      {
        $get_restaurant=Restaurants::where('restaurant_id',$restaurant_value['restaurant_id'])->first();


        $food_details=array();
        $selected_food=array();
         $counter=0;
         foreach($restaurant_value['food_details'] as $restaurant_food_key =>$restaurant_food_value)
        {
           $selected_food[]=$restaurant_food_value['food_qty']." X ".$restaurant_food_value['food_name']." (".$restaurant_food_value['food_unit'].")";
        $food_details[$counter]['food_id']=$restaurant_food_value['food_id'];
        $food_details[$counter]['food_category_id']=$restaurant_food_value['food_category_id'];
        $food_details[$counter]['food_name']=$restaurant_food_value['food_name'];
        $food_details[$counter]['food_price']=$restaurant_food_value['food_price'];
        $food_details[$counter]['food_unit']=$restaurant_food_value['food_unit'];
        $food_details[$counter]['food_qty']=$restaurant_food_value['food_qty'];
        $counter++;

        }

    $selected_food_text=implode("---", $selected_food);

      $insert_booking=new Bookings;
       $insert_booking->booking_sep_id=$get_itinerary_id;
       $insert_booking->booking_type="restaurant";
       $insert_booking->booking_type_id=$restaurant_value['restaurant_id'];
       $insert_booking->booking_agent_id=$agent_id;
       $insert_booking->booking_role="AGENT";
       $insert_booking->booking_supplier_id=$get_restaurant->supplier_id;
       $insert_booking->customer_name=$customer_name;
       $insert_booking->customer_contact=$customer_phone;
       $insert_booking->customer_email=$customer_email;
       $insert_booking->customer_country=$customer_country;
       $insert_booking->customer_address=$customer_address;
       $insert_booking->booking_remarks=$customer_remarks;
       $insert_booking->booking_adult_count=$booking_adult_count;
       $insert_booking->booking_child_count=$booking_child_count;
        $insert_booking->booking_child_age=$booking_child_age;
         $insert_booking->booking_food_details=$selected_food_text;
       $insert_booking->booking_subject_name=$restaurant_value['restaurant_name'];
       $insert_booking->booking_supplier_amount=$restaurant_value['restaurant_cost'];
       $insert_booking->booking_agent_amount=$restaurant_value['restaurant_cost'];
        $insert_booking->booking_amount=$restaurant_value['restaurant_cost'];
       $insert_booking->booking_currency=$booking_currency;
       $insert_booking->booking_selected_date=$booking_array_count[$booking_count]['dates'];
        $insert_booking->booking_other_info=serialize($food_details);
       $insert_booking->booking_date=$date;
       $insert_booking->booking_time=$time;
       $insert_booking->itinerary_status=1;
        $insert_booking->save();

      }

    }


   }


                                    // $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>You have successfully booked activity for your customer on traveldoor[dot]ge portal . Your booking id is '.$booking_id.'.</p>
                                    // ';
                                    // $data = array(
                                    //   'name' => $fetch_agent->agent_name,
                                    //   'email' =>$fetch_agent->company_email
                                    // );
                                    // Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
                                    //   $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                                    //   $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
                                    // });
                                    // $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new activity booking on traveldoor[dot]ge portal with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
                                    // $data_supplier= array(
                                    //   'name' => $fetch_supplier->supplier_name,
                                    //   'email' =>$fetch_supplier->company_email
                                    // );
                                    // Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
                                    //   $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                                    //   $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW ACTIVITY BOOKING');
                                    // });
                                    // $htmldata='<p>Dear Admin,</p><p>New activity has been booked with booking id '.$booking_id.' by agent '.$fetch_agent->agent_name.' for the supplier '.$fetch_supplier->supplier_name.' </p>';
                                    // $data_admin= array(
                                    //   'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
                                    //   'email' =>$fetch_admin->users_email
                                    // );
                                    // Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
                                    //   $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                                    //   $m->to($data_admin['email'], $data_admin['name'])->subject('NEW ACTIVITY BOOKING ON PORTAL');
                                    // });
 $message="You have successfully booked package for your customer ".$customer_name;
 session()->put('booking_message',$message);
 return redirect()->route('booking-success');
}
else
{
  $message="You selected package cannot be booked right now";
  session()->put('booking_message_fail',$message);
  return redirect()->route('booking-fail');
}
}
else
{
  return redirect()->back();
}
}

public function itinerary_save(Request $request)
{

// echo "<pre>";
// print_r($request->all());
// echo "</pre>";
// die();
    $agent_id=session()->get('travel_agent_id');
    $itinerary_id=$request->get('itinerary_id');
    $itinerary_tour_name=$request->get('itinerary_tour_name');
    $itinerary_tour_description=$request->get('itinerary_tour_description');
    $itinerary_tour_days=$request->get('no_of_days');
    $itinerary_total_cost=$request->get('total_cost_w_markup');
    $days_number=($itinerary_tour_days+1);
    $from_date=$request->get('from_date');
    $to_date=$request->get('to_date');
    $hotel_id=$request->get('hotel_id');
    $hotel_name=$request->get('hotel_name');
    $room_name=$request->get('room_name');
    $hotel_cost=$request->get('hotel_cost');
    $hotel_no_of_days=$request->get('hotel_no_of_days');
    $room_qty=$request->get('room_qty');
    $hotel_checkin=$request->get('hotel_checkin');
    $hotel_checkout=$request->get('hotel_checkout');
    $hotel_room_name=$request->get('hotel_room_name');
    $hotel_room_qty=$request->get('hotel_room_qty');
    $hotel_room_cost=$request->get('hotel_room_cost');
    $hotel_room_id=$request->get('hotel_room_id');
    $hotel_occupancy_id=$request->get('hotel_occupancy_id');
    $hotel_occupancy_qty=$request->get('hotel_occupancy_qty');

    $activity_id=$request->get('activity_id');
    $activity_name=$request->get('activity_name');
    $activity_cost=$request->get('activity_cost');

    $sightseeing_id=$request->get('sightseeing_id');
    $sightseeing_name=$request->get('sightseeing_name');
    $sightseeing_tour_type=$request->get('sightseeing_tour_type');
    $sightseeing_vehicle_type=$request->get('sightseeing_vehicle_type');
    $sightseeing_guide_id=$request->get('sightseeing_guide_id');
    $sightseeing_guide_name=$request->get('sightseeing_guide_name');
    $sightseeing_guide_cost=$request->get('sightseeing_guide_cost');
    $sightseeing_driver_id=$request->get('sightseeing_driver_id');
    $sightseeing_driver_name=$request->get('sightseeing_driver_name');
    $sightseeing_driver_cost=$request->get('sightseeing_driver_cost');
    $sightseeing_adult_cost=$request->get('sightseeing_adult_cost');
      $sightseeing_additional_cost=$request->get('sightseeing_additional_cost');
    $sightseeing_cost=$request->get('sightseeing_cost');

    $transfer_id=$request->get('transfer_id');
    $transfer_name=$request->get('transfer_name');
    $transfer_type=$request->get('transfer_type');
    $transfer_from_city=$request->get('transfer_from_city');
    $transfer_to_city=$request->get('transfer_to_city');
    $transfer_from_airport=$request->get('transfer_from_airport');
    $transfer_to_airport=$request->get('transfer_to_airport');
    $transfer_pickup=$request->get('transfer_pickup');
    $transfer_dropoff=$request->get('transfer_dropoff');
     $transfer_vehicle_type=$request->get('transfer_vehicle_type');
    $transfer_guide_id=$request->get('transfer_guide_id');
    $transfer_guide_name=$request->get('transfer_guide_name');
    $transfer_guide_cost=$request->get('transfer_guide_cost');
  $transfer_cost=$request->get('transfer_cost');
  $transfer_total_cost=$request->get('transfer_total_cost');


  $restaurant_id=$request->get('restaurant_id');
$restaurant_name=$request->get('restaurant_name');
$restaurant_food_for=$request->get('restaurant_food_for');
$restaurant_cost=$request->get('restaurant_cost');
$restaurant_food_name=$request->get('restaurant_food_name');
$restaurant_food_qty=$request->get('restaurant_food_qty');
$restaurant_food_price=$request->get('restaurant_food_price');
$restaurant_food_id=$request->get('restaurant_food_id');
$restaurant_food_category_id=$request->get('restaurant_food_category_id');
$restaurant_food_unit=$request->get('restaurant_food_unit');

    $customer_name=$request->get('customer_name');
    $customer_contact=$request->get('customer_contact');
    $customer_email=$request->get('customer_email');


     $rooms_count=$request->get('rooms_count');
  $select_adults=$request->get('select_adults');
  $select_child=$request->get('select_child');
  $child_age=$request->get('child_age');
  $no_of_adults=array_sum($select_adults);
  $no_of_child=array_sum($select_child);

    $activities_select_adults=$request->get('activities_select_adults');
  $activities_select_child=$request->get('activities_select_child');
  $activities_select_child_age=$request->get('activities_select_child_age');


    $itinerary_countries=$request->get('days_country');
    $itinerary_package_countries_array=array();
    for($country_count=0;$country_count<count($itinerary_countries);$country_count++)
    {
      $itinerary_package_countries_array[$country_count]=$itinerary_countries[$country_count];
    }
    $itinerary_package_countries=implode(",",$itinerary_package_countries_array);

    $itinerary_cities=$request->get('days_city');
    $itinerary_package_cities_array=array();
    for($city_count=0;$city_count<count($itinerary_cities);$city_count++)
    {
      $itinerary_package_cities_array[$city_count]=$itinerary_cities[$city_count];
    }
    $itinerary_package_cities=implode(",",$itinerary_package_cities_array);
$total_rooms_count=0;
    $services_array=array();
    for($services_count=0;$services_count<=$days_number-1;$services_count++)
    {
      if($services_count<($days_number-1))
      {
        if(!empty($hotel_id[$services_count]))
        {
          $services_array[$services_count]["hotel"]["hotel_id"]=$hotel_id[$services_count];
          $services_array[$services_count]["hotel"]["hotel_name"]=$hotel_name[$services_count];
          $services_array[$services_count]["hotel"]["room_name"]=$room_name[$services_count];
          $services_array[$services_count]["hotel"]["room_qty"]=$room_qty[$services_count];
          $services_array[$services_count]["hotel"]["hotel_cost"]=$hotel_cost[$services_count];
          $services_array[$services_count]["hotel"]["hotel_no_of_days"]=$hotel_no_of_days[$services_count];
          $services_array[$services_count]["hotel"]["hotel_checkin"]=$hotel_checkin[$services_count];
          $services_array[$services_count]["hotel"]["hotel_checkout"]=$hotel_checkout[$services_count];
           $total_rooms_count+=$room_qty[$services_count];
          if(empty($hotel_room_qty[$services_count]))
      {
      // $booking_details_array[$services_count]['hotel']['hotel_cost']=$hotel_cost[$services_count]*count($rooms_count);
      // $calculate_total_cost+=$hotel_cost[$dates_count]*count($rooms_count);

            $hotel_room_name_array=$hotel_room_name[$services_count];
            $hotel_room_cost_array=$hotel_room_cost[$services_count];
            $hotel_total_cost=0;
            for($i=0;$i<count($hotel_room_name_array);$i++)
            {

             $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_name']=$hotel_room_name_array[$i];
             $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_qty']=$rooms_qty[$services_count];
             $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_cost']=$hotel_room_cost_array[$i];

             $hotel_total_cost+=$hotel_room_cost_array[$i]*$room_qty[$services_count];

           }
           $services_array[$services_count]['hotel']['hotel_cost']=$hotel_total_cost;
  
  
         }
         else
         {
           $hotel_room_name_array=$hotel_room_name[$services_count];
        $hotel_room_qty_array=$hotel_room_qty[$services_count];
        $hotel_room_cost_array=$hotel_room_cost[$services_count];
         $hotel_room_id_array=$hotel_room_id[$services_count];
          $hotel_occupancy_id_array=$hotel_occupancy_id[$services_count];
          $hotel_occupancy_qty_array=$hotel_occupancy_qty[$services_count];
          $hotel_total_cost=0;
          for($i=0;$i<count($hotel_room_name_array);$i++)
          {

           
           $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_name']=$hotel_room_name_array[$i];
           $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_qty']=$hotel_room_qty_array[$i];
           $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_cost']=$hotel_room_cost_array[$i];
           $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_id']=$hotel_room_id_array[$i];
           $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_id']=$hotel_occupancy_id_array[$i];
           $services_array[$services_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_qty']=$hotel_occupancy_qty_array[$i];

             $hotel_total_cost+=$hotel_room_cost_array[$i]*$hotel_room_qty_array[$i];

         }
        $services_array[$services_count]['hotel']['hotel_cost']=$hotel_total_cost;

       }
     }
   }




      if(!empty($activity_id[$services_count]))
      {
      $services_array[$services_count]["activity"]["activity_id"]=$activity_id[$services_count];
      $services_array[$services_count]["activity"]["activity_name"]=$activity_name[$services_count];
      $services_array[$services_count]["activity"]["activity_cost"]=$activity_cost[$services_count];
      }
     
       if(!empty($sightseeing_id[$services_count]))
      {
      $services_array[$services_count]["sightseeing"]["sightseeing_id"]=$sightseeing_id[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_name"]=$sightseeing_name[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_tour_type"]=$sightseeing_tour_type[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_vehicle_type"]=$sightseeing_vehicle_type[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_guide_id"]=$sightseeing_guide_id[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_guide_name"]=$sightseeing_guide_name[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_guide_cost"]=$sightseeing_guide_cost[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_driver_id"]=$sightseeing_driver_id[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_driver_name"]=$sightseeing_driver_name[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_driver_cost"]=$sightseeing_driver_cost[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_adult_cost"]=$sightseeing_adult_cost[$services_count];
        $services_array[$services_count]["sightseeing"]["sightseeing_additional_cost"]=$sightseeing_additional_cost[$services_count];
      $services_array[$services_count]["sightseeing"]["sightseeing_cost"]=$sightseeing_cost[$services_count];

      }
   
      if(!empty($transfer_id[$services_count]))
      { 
      $services_array[$services_count]["transfer"]["transfer_id"]=$transfer_id[$services_count];
      $services_array[$services_count]["transfer"]["transfer_name"]=$transfer_name[$services_count];
      $services_array[$services_count]["transfer"]["transfer_type"]=$transfer_type[$services_count];
      $services_array[$services_count]["transfer"]["transfer_from_city"]=$transfer_from_city[$services_count];
      $services_array[$services_count]["transfer"]["transfer_to_city"]=$transfer_to_city[$services_count];
      $services_array[$services_count]["transfer"]["transfer_from_airport"]=$transfer_from_airport[$services_count];
      $services_array[$services_count]["transfer"]["transfer_to_airport"]=$transfer_to_airport[$services_count];
      $services_array[$services_count]["transfer"]["transfer_pickup"]=$transfer_pickup[$services_count];
      $services_array[$services_count]["transfer"]["transfer_dropoff"]=$transfer_dropoff[$services_count];
      $services_array[$services_count]["transfer"]["transfer_vehicle_type"]=$transfer_vehicle_type[$services_count];
      $services_array[$services_count]["transfer"]["transfer_guide_id"]=$transfer_guide_id[$services_count];
      $services_array[$services_count]["transfer"]["transfer_guide_name"]=$transfer_guide_name[$services_count];
      $services_array[$services_count]["transfer"]["transfer_guide_cost"]=$transfer_guide_cost[$services_count];
      $services_array[$services_count]["transfer"]["transfer_cost"]=$transfer_cost[$services_count];
      $services_array[$services_count]["transfer"]["transfer_total_cost"]=$transfer_total_cost[$services_count];
       }

        if(isset($restaurant_id[$services_count]))
   {
    $restaurant_count=0;
    foreach($restaurant_id[$services_count] as $restaurant_id_key =>$restaurant_id_value)
    {
      if($restaurant_id_value!="")
      {
        
      $services_array[$services_count]['restaurant'][$restaurant_count]['restaurant_id']=$restaurant_id[$services_count][$restaurant_id_key];
      $services_array[$services_count]['restaurant'][$restaurant_count]['restaurant_name']=$restaurant_name[$services_count][$restaurant_id_key];
      $services_array[$services_count]['restaurant'][$restaurant_count]['restaurant_cost']=$restaurant_cost[$services_count][$restaurant_id_key];
       $services_array[$services_count]['restaurant'][$restaurant_count]['restaurant_food_for']=$restaurant_food_for[$services_count][$restaurant_id_key];
         foreach($restaurant_food_id[$services_count][$restaurant_id_key] as $restaurant_child_food_id_key =>  $restaurant_child_food_id_value)
      {

       $services_array[$services_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_id']=$restaurant_food_id[$services_count][$restaurant_id_key][$restaurant_child_food_id_key];

       $services_array[$services_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_name']=$restaurant_food_name[$services_count][$restaurant_id_key][$restaurant_child_food_id_key];

        $services_array[$services_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_category_id']=$restaurant_food_category_id[$services_count][$restaurant_id_key][$restaurant_child_food_id_key];

         $services_array[$services_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_unit']=$restaurant_food_unit[$services_count][$restaurant_id_key][$restaurant_child_food_id_key];

         $services_array[$services_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_qty']=$restaurant_food_qty[$services_count][$restaurant_id_key][$restaurant_child_food_id_key];

          $services_array[$services_count]['restaurant'][$restaurant_count]['food_details'][$restaurant_child_food_id_key]['food_price']=$restaurant_food_price[$services_count][$restaurant_id_key][$restaurant_child_food_id_key];

      }

       $restaurant_count++;
      }
     

    }

   }
    } 

    $itinerary_package_services=serialize($services_array);


    $insert_itinerary=new AgentSavedItinerary;
    $insert_itinerary->actual_itinerary_id=$itinerary_id;
    $insert_itinerary->from_date=$from_date;
    $insert_itinerary->to_date=$to_date;
    $insert_itinerary->customer_name=$customer_name;
    $insert_itinerary->customer_contact=$customer_contact;
    $insert_itinerary->customer_email=$customer_email;
    $insert_itinerary->no_of_adults=$no_of_adults;
    $insert_itinerary->no_of_children=$no_of_child;
    $insert_itinerary->child_age=serialize($child_age);
    // $insert_itinerary->rooms_count=serialize($rooms_count);
    $insert_itinerary->select_adults=serialize($select_adults);
    $insert_itinerary->select_child=serialize($select_child);
    $insert_itinerary->select_child_age=serialize($child_age);
      $insert_itinerary->activities_select_adults=serialize($activities_select_adults);
    $insert_itinerary->activities_select_child=serialize($activities_select_child);
    $insert_itinerary->activities_select_child_age=serialize($activities_select_child_age);
    $insert_itinerary->agent_itinerary_tour_name=$itinerary_tour_name;
    $insert_itinerary->agent_itinerary_tour_description=$itinerary_tour_description;
    $insert_itinerary->agent_itinerary_tour_days=$itinerary_tour_days;
    $insert_itinerary->agent_itinerary_package_countries =$itinerary_package_countries;
    $insert_itinerary->agent_itinerary_package_cities=$itinerary_package_cities;
    $insert_itinerary->agent_itinerary_package_services=$itinerary_package_services;
    $insert_itinerary->agent_itinerary_total_cost=$itinerary_total_cost;
    $insert_itinerary->agent_itinerary_created_by= $agent_id;
    if($insert_itinerary->save())
    {
      echo "success";
    }
    else
    {
      echo "fail";
    }
}



public function booking_success()
{
  $message=session::get('booking_message');
  return view('agent.booking-success')->with('message',$message);
}
public function booking_fail()
{
  $message=session::get('booking_message_fail');
  return view('agent.booking-fail')->with('message',$message);
}
public function agent_bookings()
{
  if(session()->has('travel_agent_id'))
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_bookings=Bookings::where('booking_agent_id',$agent_id)->where('booking_sightseeing_id',null)->where('itinerary_status',null)->get();
    return view('agent.my-bookings')->with(compact('fetch_bookings'));
  }
  else
  {
    return redirect()->route('agent');
  }
}

public function agent_bookings_tab(Request $request)
{

    $booking_type=$request->get('booking_type');
    $agent_id=session()->get('travel_agent_id');

    $fetch_bookings=Bookings::where('booking_agent_id',$agent_id)->where('booking_sightseeing_id',null)->where('itinerary_status',null);

    if($booking_type!="all" && $booking_type!="cancelled")
    {
      $fetch_bookings=$fetch_bookings->where("booking_admin_status",$booking_type)->where("booking_status","!=",2)->get();
    }
    else if($booking_type=="cancelled")
    {
$fetch_bookings=$fetch_bookings->where("booking_status",2)->get();
    }
    else
    {
       $fetch_bookings=$fetch_bookings->get();
    }
    $html='<div class="row">
    <div class="col-md-12">
    <div class="table-responsive">
    <table id="example_'.$booking_type.'" class="table table-condensed table-striped">
    <thead>
    <tr>
    <td>Booking ID</td>
    <td>Booking Type</td>
    <td>Type Name</td>
    <td>Name</td>
    <td>Email</td>
    <td>Mobile</td>
    <td>Booking Date</td>
    <td>Amount</td>
    <td>Status</td>
    <td>Remarks</td>
    <td>Attachments</td>
     <td>Full Payment</td>
    <td>Action</td>
    </tr>
    </thead>
    <tbody>';
    foreach($fetch_bookings as $bookings)
    {

      $html.='<tr>
      <td>'.$bookings->booking_sep_id.'</td>
      <td>'.ucwords($bookings->booking_type).'</td>
      <td>';
      $booking_type_id=$bookings->booking_type_id;
      if($bookings->booking_type=="activity")
      {
        $fetch_activity=ServiceManagement::searchActivity($booking_type_id);
        $html.=$fetch_activity['activity_name'];
      }
      else if($bookings->booking_type=="hotel")
      {
        $fetch_hotel=ServiceManagement::searchHotel($booking_type_id);
        $html.=$fetch_hotel['hotel_name'];
      }
      else if($bookings->booking_type=="guide")
      {
        $fetch_guide=ServiceManagement::searchGuide($booking_type_id);
        $html.=$fetch_guide['guide_first_name']." ".$fetch_guide['guide_last_name'];
      }
      else if($bookings->booking_type=="sightseeing")
      {
        $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($booking_type_id);
        $html.=$fetch_sightseeing['sightseeing_tour_name'];
      }
      else if($bookings->booking_type=="itinerary")
      {
        $fetch_itinerary=ServiceManagement::searchItinerary($booking_type_id);
        $html.=$fetch_itinerary['itinerary_tour_name'];
      }
      else if($bookings->booking_type=="transfer")
      {

        $html.=$bookings->booking_subject_name;
      }
      $html.='</td>
      <td>'.$bookings->customer_name.'</td>
      <td>'.$bookings->customer_email.'</td>
      <td>'.$bookings->customer_contact.'</td>
      <td>'.$bookings->booking_date.'</td>
      <td>'.$bookings->booking_currency.' '.$bookings->booking_amount.'</td>';
      if($bookings->booking_status==2)
      {
              $html.='<td id="status_'.$bookings->booking_id.'"><button class="btn btn-sm btn-info">Cancelled</button></td>';       
       }                           
     else if($bookings->booking_admin_status==0)
      {
         $html.='<td id="status_'.$bookings->booking_id.'"><button class="btn btn-sm btn-warning">Pending</button></td>';
      }
      else if($bookings->booking_admin_status==1)
      {
        $html.='<td id="status_'.$bookings->booking_id.'"><button class="btn btn-sm btn-success">Confirmed</button></td>';
      }
      else if($bookings->booking_admin_status==2)
      {
         $html.='<td id="status_'.$bookings->booking_id.'"><button class="btn btn-sm btn-danger">Rejected</button></td>';
      }


      $html.='<td>'.$bookings->booking_admin_message.'</td>
      <td><button id="attachments_'.$bookings->booking_sep_id.'" class="btn btn-rounded btn-primary btn-sm attachments">View Attachments</button></td>
      <td>';

      if($bookings->itinerary_status==0)
      {
        if($bookings->booking_final_amount_status==0)
        {
         $html.=' <button type="button" class="btn btn-sm btn-rounded btn-info">Pending</button>';
       }
       elseif($bookings->booking_final_amount_status==1)
       {
        $html.='<button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button>';
      }    
    }

                                    

       $html.='</td>
       <td> <a href="'.route('agent-invoice',['booking_id'=>$bookings->booking_sep_id]).'" class="btn btn-rounded btn-default btn-sm">Agent Invoice</a> &nbsp; <a href="'.route('customer-invoice',['booking_id'=>$bookings->booking_sep_id]).'" class="btn btn-rounded btn-default btn-sm">Customer Invoice</a> &nbsp;';
        if($bookings->booking_status==2)
        {

        }
       else if($bookings->booking_admin_status==0 && $bookings->booking_supplier_status==0)
       {
        $html.=' <button class="btn btn-sm btn-info cancel" id="cancel_'.$bookings->booking_id.'">Cancel</button>';
       }
       else if($bookings->booking_admin_status==0 && $bookings->booking_supplier_status==1 && $bookings->booking_type=="sightseeing")
       {
         $html.=' <button class="btn btn-sm btn-info cancel" id="cancel_'.$bookings->booking_id.'">Cancel</button>';
       }
       if($bookings->itinerary_status==0)
       {
        $html.='<button id="installment_'.$bookings->booking_sep_id.'" class="btn btn-rounded btn-primary btn-sm create_installment"';

        if($bookings->booking_admin_status!=1)
        {
          $html.=' style="display:none" ';
          }
          $html.='>Pay Amount</button>';
       }
                                    
                                   
        $html.='</td>
      </tr>';
    }
    $html.="</tbody>
    </table>
    </div>
    </div>
    </div>";

    echo $html;

 
}


public function agent_bookings_cancel(Request $request)
{
  $booking_id=$request->get('booking_id');
    $check_booking=Bookings::where('booking_id',$booking_id)->first();
    if(!empty($check_booking))
    {
      if($check_booking['booking_type']=="itinerary")
      {
        if($check_booking['booking_admin_status']!=0)
        {
          echo "process";
        }
        else
        {
           $booking_sep_id=$check_booking['booking_sep_id'];
        $update_booking=Bookings::where('booking_sep_id',$booking_sep_id)->update(["booking_status"=>2]);

        if($update_booking)
        {
          $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $agent_id=session()->get('travel_agent_id');
              $fetch_admin=Users::where('users_pid',0)->first();
              $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

              //Agent Cancellation Acknowledgement
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is successfully cancelled </p>';
            $data = array(
              'name' => $fetch_agent->agent_name,
              'email' =>$fetch_agent->company_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION');
            });


            $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by agent '.$fetch_agent->agent_name.'</p>';
            $data_admin= array(
              'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
              'email' =>$fetch_admin->users_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' CANCELLATION ON PORTAL');
            });

             $fetch_subsequent_booking=Bookings::where('booking_sep_id',$booking_id)->get();

             foreach($fetch_subsequent_booking as $subsequent_booking)
             {
              $booking_type=$subsequent_booking->booking_type;
              $booking_supplier_id=$subsequent_booking->booking_supplier_id;

              if($booking_type!="sightseeing")
              {
               if($booking_supplier_id!=null && $booking_supplier_id!="")
               {

                $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

                      //Supplier Cancellation Acknowledgement
                $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by agent/customer . Please login into your supplier portal to get more information :'.route('supplier').'</p>';
                $data_supplier= array(
                  'name' => $fetch_supplier->supplier_name,
                  'email' =>$fetch_supplier->company_email
                );
                Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier,$booking_type) {
                  $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
                  $m->to($data_supplier['email'], $data_supplier['name'])->subject(strtoupper($booking_type).' BOOKING CANCELLATION');
                });


              }

            }

          }










          echo "success";
        }
        else
          echo "fail";
        }
       

      }
      else if($check_booking['booking_type']=="sightseeing")
      {
        if($check_booking['booking_admin_status']!=0)
        {
          echo "process";
        }
        else
        {
        $update_booking=Bookings::where('booking_id',$booking_id)->orWhere('booking_sightseeing_id',$booking_id)->update(["booking_status"=>2]);

        if($update_booking)
        {
           $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $agent_id=session()->get('travel_agent_id');
              $fetch_admin=Users::where('users_pid',0)->first();
              $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

              //Agent Cancellation Acknowledgement
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is successfully cancelled </p>';
            $data = array(
              'name' => $fetch_agent->agent_name,
              'email' =>$fetch_agent->company_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION');
            });


            $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by agent '.$fetch_agent->agent_name.'</p>';
            $data_admin= array(
              'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
              'email' =>$fetch_admin->users_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' CANCELLATION ON PORTAL');
            });

             $fetch_subsequent_booking=Bookings::where('booking_sightseeing_id',$booking_id)->get();

             foreach($fetch_subsequent_booking as $subsequent_booking)
             {
              $booking_type=$subsequent_booking->booking_type;
               $booking_supplier_id=$subsequent_booking->booking_supplier_id;

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
                  //Supplier Cancellation Acknowledgement
            $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by agent/customer . Please login into your supplier portal to get more information :'.route('supplier').'</p>';
            $data_supplier= array(
              'name' => $fetch_supplier->supplier_name,
              'email' =>$fetch_supplier->company_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_supplier['email'], $data_supplier['name'])->subject('BOOKING CANCELLATION');
            });

             }
          



          echo "reload_success";
        } 
        else
          echo "fail";
      }

      }
      else
      {
        if($check_booking['booking_admin_status']!=0 || $check_booking['booking_supplier_status']!=0)
        {
          echo "process";
        }
        else
        {
          $update_booking=Bookings::where('booking_id',$booking_id)->update(["booking_status"=>2]);

          if($update_booking)
          {
             $booking_id=$check_booking['booking_sep_id'];
              $booking_type=$check_booking['booking_type'];

              $agent_id=session()->get('travel_agent_id');
              $fetch_admin=Users::where('users_pid',0)->first();
              $fetch_agent=Agents::where('agent_id',$agent_id)->first();

              $booking_supplier_id=$check_booking['booking_supplier_id'];

              $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();

              //Agent Cancellation Acknowledgement
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>Your booked '.ucfirst($booking_type).' with booking id :'.$booking_id.' is successfully cancelled </p>';
            $data = array(
              'name' => $fetch_agent->agent_name,
              'email' =>$fetch_agent->company_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data['email'], $data['name'])->subject('BOOKING CANCELLATION');
            });


            //Supplier Cancellation Acknowledgement
            $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by agent/customer . Please login into your supplier portal to get more information. '.route('supplier').'</p>';
            $data_supplier= array(
              'name' => $fetch_supplier->supplier_name,
              'email' =>$fetch_supplier->company_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_supplier['email'], $data_supplier['name'])->subject('BOOKING CANCELLATION');
            });


            $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been cancelled by agent '.$fetch_agent->agent_name.'</p>';
            $data_admin= array(
              'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
              'email' =>$fetch_admin->users_email
            );
            Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' CANCELLATION ON PORTAL');
            });
            echo "success";
          }
          else
            echo "fail";

      }
      }
    }
}

public function fetch_itineray(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_saved_itinerary=AgentSavedItinerary::where('agent_itinerary_created_by',$agent_id)->get();
    return view('agent.saved-itinerary-list')->with(compact('fetch_saved_itinerary'));
  }
  else
  {
    return redirect()->route('agent');
  }
}


public function download_itinerary(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
     $markup=$this->agent_markup();
    $itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
     $own_markup=$this->agent_own_markup();
    $own_itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $own_itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }

    $agent_id=session()->get('travel_agent_id');

     $countries=Countries::where("country_status",1)->get();

    $get_agent_details=Agents::where('agent_id',$agent_id)->first();
    $itinerary_id=$request->get('itinerary_id');
     $languages=languages::get();
    $fetch_saved_itinerary=AgentSavedItinerary::where('agent_itinerary_id',$itinerary_id)->where('agent_itinerary_created_by',$agent_id)->first();
    if(!empty($fetch_saved_itinerary))
    {
    $get_itinerary=SavedItinerary::where('itinerary_id',$fetch_saved_itinerary->actual_itinerary_id)->where('itinerary_status',1)->first();

    $pdf = PDF::loadView('pdf.agent-saved-itineray',["fetch_saved_itinerary"=>$fetch_saved_itinerary,"get_itinerary"=>$get_itinerary,"get_agent_details"=>$get_agent_details,"markup"=>$itinerary_markup,"own_markup"=>$own_itinerary_markup,"countries"=>$countries,"languages"=>$languages]);
    return $pdf->stream();
    // return view('pdf.agent-saved-itineray',["fetch_saved_itinerary"=>$fetch_saved_itinerary,"get_itinerary"=>$get_itinerary,"get_agent_details"=>$get_agent_details,"markup"=>$itinerary_markup,"own_markup"=>$own_itinerary_markup,"countries"=>$countries,"languages"=>$languages]);
    }
    else
    {
      return route()->back();
    }
    
  }
  else
  {
    return redirect()->route('agent');
  }
}

public function download_restaurant_itinerary(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
     $markup=$this->agent_markup();
    $itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
     $own_markup=$this->agent_own_markup();
    $own_itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $own_itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }

    $agent_id=session()->get('travel_agent_id');

     $countries=Countries::where("country_status",1)->get();

    $get_agent_details=Agents::where('agent_id',$agent_id)->first();
    $itinerary_id=$request->get('itinerary_id');
     $languages=languages::get();
    $fetch_saved_itinerary=AgentSavedItinerary::where('agent_itinerary_id',$itinerary_id)->where('agent_itinerary_created_by',$agent_id)->first();
    if(!empty($fetch_saved_itinerary))
    {
    $get_itinerary=SavedItinerary::where('itinerary_id',$fetch_saved_itinerary->actual_itinerary_id)->where('itinerary_status',1)->first();

    $pdf = PDF::loadView('pdf.agent-restaurant-saved-itinerary',["fetch_saved_itinerary"=>$fetch_saved_itinerary,"get_itinerary"=>$get_itinerary,"get_agent_details"=>$get_agent_details,"markup"=>$itinerary_markup,"own_markup"=>$own_itinerary_markup,"countries"=>$countries,"languages"=>$languages]);
    return $pdf->stream();
    // return view('pdf.agent-saved-itineray',["fetch_saved_itinerary"=>$fetch_saved_itinerary,"get_itinerary"=>$get_itinerary,"get_agent_details"=>$get_agent_details,"markup"=>$itinerary_markup,"own_markup"=>$own_itinerary_markup,"countries"=>$countries,"languages"=>$languages]);
    }
    else
    {
      return route()->back();
    }
    
  }
  else
  {
    return redirect()->route('agent');
  }
}

public function agent_invoice(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
  $countries=Countries::get();
  $agent_info=Agents::where('agent_id',session()->get('travel_agent_id'))->first();
  $booking_id=$request->get('booking_id');
  $fetch_data=Bookings::where("booking_sep_id",$booking_id)->get();
  $fetch_data=$fetch_data->toArray();
  $pdf = PDF::loadView('pdf.invoice',["data"=>$fetch_data,"countries"=>$countries,"customer_type"=>"agent","agent_info"=>$agent_info]);
   return $pdf->download('Agent-Invoice-'.$booking_id.".pdf");
}
  else
  {
    return redirect()->route('agent');
  }

  // return view('pdf.invoice',["data"=>$data]); 
}

public function customer_invoice(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
  $countries=Countries::get();
  $agent_info=Agents::where('agent_id',session()->get('travel_agent_id'))->first();
  $booking_id=$request->get('booking_id');
  $fetch_data=Bookings::where("booking_sep_id",$booking_id)->get();
  $fetch_data=$fetch_data->toArray();
  $pdf = PDF::loadView('pdf.invoice',["data"=>$fetch_data,"countries"=>$countries,"customer_type"=>"customer","agent_info"=>$agent_info]);
    return $pdf->download('Customer-Invoice-'.$booking_id.".pdf");
  // return view('pdf.invoice',["data"=>$data]); 
  }
  else
  {
    return redirect()->route('agent');
  }
}

public function exchange_rates(Request $request)
{
     $room_currency_array=array("USD","EUR");
    $currency_price_array=array();

    // if(session()->has('currencies'))
    // {
    //   $currency_price_array=session()->get('currencies');
    // }
    // else
    // {
     foreach($room_currency_array as $currency)
      {
           $new_currency=$currency;

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$new_currency."_".$this->base_currency;

        //  $conversion_price=$convertedPriceArray->$currency_attribute;
          
        //  $currency_price_array[$currency]=round($conversion_price,2);
        
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);

           // session()->get('currencies',$currency_price_array[$currency]);
         
      }
 
    // }
      $currency=Currency::orderBy('code','asc')->get();

      return view('agent.exchange-rates',["currency"=>$currency])->with(compact("currency_price_array",$currency_price_array));


}





//Payments
public function agent_get_payments(Request $request)
{
 
  $booking_id=$request->get('bookingid');
  $installment_id=$request->get('installmentid');
  $agent_id=session()->get('travel_agent_id');

  $get_booking=Bookings::where('booking_sep_id',$booking_id)->where('booking_admin_status',1)->where('booking_status',1)->first();
  if(!empty($get_booking))
  {
    if($get_booking->booking_agent_id==$agent_id)
    {
       $get_installment=BookingInstallment::where('booking_installment_id',$installment_id)->where('booking_id',$booking_id)->first();
    if(!empty($get_installment))
    {

      if($get_installment->booking_paid_status==1)
      {
        $payment_status=1;
      }
      else
      {
        $payment_status=0;
      }

      $total_wallet=0;
        $get_wallet=AgentWallet::select(\DB::raw("(COALESCE(sum(age_wallet_credit_amount), 0)-COALESCE(sum(age_wallet_debit_amount), 0)) as total_wallet_amount"))->where('age_wallet_agent_id',$agent_id)->where('age_wallet_status',1)->groupBy('age_wallet_agent_id')->first();

        if(!empty($get_wallet))
          $total_wallet=$get_wallet->total_wallet_amount;
        else
          $total_wallet=0;



      return view('agent.get-payments')->with(compact('get_booking','get_installment'))->with('payment_status',$payment_status)->with('total_wallet',$total_wallet);

    }
    else
    {
      return redirect()->route('agent-bookings');
    }

    }
    else
    {
        return redirect()->route('agent-bookings');
    }

  }
  else
  {
    return redirect()->route('agent-bookings');
  }

}


public function process_payments(Request $request)
{
  // print_r($request->all());
  
  $booking_id=$request->get('bookingid');
   $installmentid=$request->get('installmentid');
   $payment_mode=$request->get('payment_mode');
   $payment_info=$request->get('payment_info');
   $agent_id=session()->get('travel_agent_id');
 if(session()->has('travel_agent_id'))
 {
  if($request->has('payment_mode'))
  {

    if($payment_mode=="online")
    {
      $get_installment=BookingInstallment::where('booking_id',$booking_id)->where('booking_installment_id',$installmentid)->first();
        if(!empty($get_installment))
        {
          if($get_installment->booking_paid_status==0)
          {
            $payment_amount=$get_installment->booking_install_amount;

            // if(session()->has('on_payment_status'))
            // {
            //   Session::flash("payment-error","We cannot process your payment as your previous online payment is not completed.");
            // return redirect()->back();

            // }
            // else
            // {
             session()->put('on_payment_booking_id',$booking_id);
             session()->put('on_payment_booking_installment_id',$installmentid);
             session()->put('on_payment_amount',$payment_amount);
             session()->put('on_payment_status',0);
             return redirect()->route('process-online-payment');
           // }
           
             
    
          }
          else if($get_installment->booking_paid_status==1)
          {
            Session::flash("payment-error","We cannot process your payment as your payment has already been completed.");
            return redirect()->back();
          }
        }
        else
        {
         Session::flash("payment-error","No Payment Details found with given data");
          return redirect()->back(); 
        }
    }
    if($payment_mode=="wallet")
    {

      $get_installment=BookingInstallment::where('booking_id',$booking_id)->where('booking_installment_id',$installmentid)->first();
        if(!empty($get_installment))
        {
          if($get_installment->booking_paid_status==0)
          {
            $payment_amount=$get_installment->booking_install_amount;
             
            $get_wallet=AgentWallet::select(\DB::raw("(COALESCE(sum(age_wallet_credit_amount), 0)-COALESCE(sum(age_wallet_debit_amount), 0)) as total_wallet_amount"))->where('age_wallet_agent_id',$agent_id)->where('age_wallet_status',1)->groupBy('age_wallet_agent_id')->first();

            if(!empty($get_wallet))
              $total_wallet=$get_wallet->total_wallet_amount;
            else
              $total_wallet=0;

            if($total_wallet<$payment_amount)
            {
              Session::flash("payment-error","Wallet Balance is not enough to make payment");
              return redirect()->back();

            }
            else
            {
             $payment_mode_actual=explode("-",$payment_mode);
             $payment_mode=implode(" ",$payment_mode_actual);

             $update_array=array("booking_paid_status"=>1,
              "booking_paid_date"=>date('Y-m-d'),
              "booking_paid_time"=>date('H:i:s'),
              "booking_remarks"=>"Paid by ".strtoupper($payment_mode),
              "booking_payment_mode"=>strtoupper($payment_mode),
              "booking_confirm_status"=>1,
               "booking_confirm_remarks"=>"Approved",
            );
             $update_booking_payment=BookingInstallment::where('booking_installment_id',$installmentid)->update($update_array);
             if($update_booking_payment)
             {
               $insert_age_wallet=new AgentWallet;
               $insert_age_wallet->age_wallet_agent_id=$agent_id;
               $insert_age_wallet->age_wallet_debit_amount=round($payment_amount);
               $insert_age_wallet->age_wallet_remarks="Money Debited";   
               $insert_age_wallet->age_wallet_date=date('Y-m-d');
               $insert_age_wallet->age_wallet_time=date('H:i:s');
               $insert_age_wallet->age_wallet_approve_reject_remarks="Money Debited for payment of booking id #".$booking_id;
               $insert_age_wallet->save();

               //start check whether full booking amount is paid 
                $get_booking=Bookings::where('booking_sep_id',$booking_id)->where('itinerary_status',null)->first();
                if(!empty($get_booking))
                {
                  $total_amount=$get_booking->booking_agent_amount;
                }
                else
                {
                  $total_amount=0;
                }

                  $remaining_amount=$total_amount;

              $get_booking_installment=BookingInstallment::where('booking_id',$booking_id)->get();
            if(count($get_booking_installment)>0)
            {
               foreach($get_booking_installment as $installments)
               {
                if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                {
                   $remaining_amount=$remaining_amount-$installments->booking_install_amount;
                  
                }
               }
             }
             if($remaining_amount==0)
             {
              //if full amount is paid i.e. remaning amount is 0 then update final_amount_status of that booking
              $update_booking=Bookings::where('booking_sep_id',$booking_id)->update(["booking_final_amount_status"=>1]);
             }
             //end check whether full booking amount is paid 


          $fetch_agent=Agents::where('agent_id',$agent_id)->first();
          $fetch_admin=Users::where('users_pid',0)->first();
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>You have successfully paid GEL '.$payment_amount.' through '.$payment_mode.' against payment for booking id :'.$booking_id.'</p>
        ';
        $data = array(
          'name' => $fetch_agent->agent_name,
          'email' =>$fetch_agent->company_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data['email'], $data['name'])->subject('PAYMENT SUCCESSFUL');
        });

        $htmldata='<p>Dear Admin,</p><p>New payment of <b>GEL '.$payment_amount.'</b> has been received for <b>booking id('.$booking_id.')</b> from agent '.$fetch_agent->agent_name.' through '.$payment_mode.' </p>';
        $data_admin= array(
          'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
          'email' =>$fetch_admin->users_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data_admin['email'], $data_admin['name'])->subject('NEW PAYMENT RECEIVED');
        });


              Session::flash("payment-success","You have made payment successfully.");
              return redirect()->back();
            }
            else
            {
              Session::flash("payment-error","We cannot process your payment right now");
              return redirect()->back();
            }
          }

          }
          else if($get_installment->booking_paid_status==1)
          {
            Session::flash("payment-error","We cannot process your payment as your payment has already been completed.");
            return redirect()->back();
          }
        }
        else
        {
         Session::flash("payment-error","No Payment Details found with given data");
          return redirect()->back(); 
        }
    
    }
    else
    {
        $get_installment=BookingInstallment::where('booking_id',$booking_id)->where('booking_installment_id',$installmentid)->first();
        if(!empty($get_installment))
        {
          if($get_installment->booking_paid_status==0)
          {

            $payment_amount=$get_installment->booking_install_amount;
            $payment_mode_actual=explode("-",$payment_mode);
            $payment_mode=implode(" ",$payment_mode_actual);
              $payment_images=array();
//multifile uploading
  if($request->hasFile('payment_file'))
  {
    foreach($request->file('payment_file') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_activity = "payment-proof-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/agent_payments/';
        $file->move($dir1, $image_activity);
        $payment_images[]=$image_activity;
      }
    }
  }
  $payment_images=serialize($payment_images);

            $update_array=array("booking_paid_status"=>1,
                             "booking_paid_info"=>$payment_info,
                              "booking_paid_date"=>date('Y-m-d'),
                              "booking_paid_time"=>date('H:i:s'),
                              "booking_remarks"=>"Paid by ".strtoupper($payment_mode),
                              "booking_payment_mode"=>strtoupper($payment_mode),
                              "booking_payment_attachments"=>$payment_images
                            );
            $update_booking_payment=BookingInstallment::where('booking_installment_id',$installmentid)->update($update_array);
            if($update_booking_payment)
            {


          $fetch_agent=Agents::where('agent_id',$agent_id)->first();
          $fetch_admin=Users::where('users_pid',0)->first();
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>You have successfully paid <b>GEL '.$payment_amount.'</b> through <b>'.$payment_mode.'</b> against payment for <b>booking id :'.$booking_id.'</b>. Traveldoor will be notified about this payment and they will take further action of approving/rejecting it.</p>
        ';
        $data = array(
          'name' => $fetch_agent->agent_name,
          'email' =>$fetch_agent->company_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data['email'], $data['name'])->subject('PAYMENT SUCCESSFUL');
        });

        $htmldata='<p>Dear Admin,</p><p>New payment of <b>GEL '.$payment_amount.'</b> has been received for <b>booking id('.$booking_id.')</b> from agent '.$fetch_agent->agent_name.' through '.$payment_mode.'.</p>';
        $data_admin= array(
          'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
          'email' =>$fetch_admin->users_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data_admin['email'], $data_admin['name'])->subject('NEW PAYMENT RECEIVED');
        });


              Session::flash("payment-success","Payment Completed Successfully. Traveldoor will be notified about this payment and they will take further action");
            return redirect()->back();
            }
            else
            {
              Session::flash("payment-error","We cannot process your payment right now");
            return redirect()->back();
            }

          }
          else if($get_installment->booking_paid_status==1)
          {
            Session::flash("payment-error","We cannot process your payment as your payment has already been completed.");
            return redirect()->back();
          }
        }
        else
        {
         Session::flash("payment-error","No Payment Details found with given data");
          return redirect()->back(); 
        }
    }
  
  }
  else
  {
    Session::flash("payment-error","No Payment Mode Selected");
  return redirect()->back(); 
  }

}
else
{
  return redirect()->route('agent');
}
}

public function process_online_payment(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
    if(session()->has('on_payment_booking_id'))
    { 
      if(session()->has('on_payment_status') && session()->get('on_payment_status')==0)
      {
       $booking_id=session()->get('on_payment_booking_id');
       $booking_installment_id=session()->get('on_payment_booking_installment_id');
       $booking_amount=session()->get('on_payment_amount');

       $amount_pay=($booking_amount*100);
       $curl = curl_init();
       $post_fields = "command=a&amount=".$amount_pay."&currency=981&client_ip_addr=208.109.11.88
       &description=UFCTEST&msg_type=SMS";
       $submit_url = "https://ecommerce.ufc.ge:18443/ecomm2/MerchantHandler";
      Curl_setopt($curl, CURLOPT_SSLVERSION, 1); //0 
      curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
      curl_setopt($curl, CURLOPT_VERBOSE, '1');
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, '0');
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, '0');
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_TIMEOUT, 120);
      curl_setopt($curl, CURLOPT_SSLCERT, getcwd()."/assets/payment-cert/5302362.pem");
      curl_setopt($curl, CURLOPT_SSLKEYPASSWD,   'MKAjkFeBWC1z9AQk');
      curl_setopt($curl, CURLOPT_URL, $submit_url);
      $result = curl_exec($curl);
      $info = curl_getinfo($curl);
    if(curl_errno($curl))
    {
      echo 'curl error:' . curl_error($curl)."<BR>";
      die();
    }
    curl_close($curl);
    $transaction_id=substr($result,-28);
    if(strlen($transaction_id)==28)
    {
      $insert_online_payment=new OnlinePayment;
      $insert_online_payment->transaction_id=$transaction_id;
      $insert_online_payment->booking_id=$booking_id;
      $insert_online_payment->booking_installment_id=$booking_installment_id;
      $insert_online_payment->agent_id=session()->get('travel_agent_id');
      $insert_online_payment->amount=$booking_amount;
       $insert_online_payment->payment_return_url=route('agent-payment-processor');
        $insert_online_payment->create_date=date('Y-m-d');
         $insert_online_payment->create_time=date('H:i:s');

         if($insert_online_payment->save())
         {
           return view('agent.process-online-payment')->with('transaction_id',$transaction_id);
         }
         else
         {
          return redirect()->back();
         }
     
    }
    else
    {
      echo "Error : Try Again";
    }
  }
  else
  {
   Session::flash("payment-error","We cannot process your payment at the moment");
   return redirect()->route('agent-get-payments'); 
 }


}

    else
    {
      Session::flash("payment-error","We cannot process your payment as your previous online payment is not completed");
  return redirect()->route('agent-get-payments'); 
    }
  }

}

public function payment_processor(Request $request)
{
  $agent_id=session()->get('travel_agent_id');
  $get_payment=OnlinePayment::where('agent_id',$agent_id)->where('payment_status',0)->orderBy('online_payment_id','desc')->first();
  if(!empty($get_payment))
  {
    $trans_id=$get_payment->transaction_id;
    $payment_amount=$get_payment->amount;
     $curl = curl_init();
     $post_fields = "command=c&trans_id=".$trans_id.'&client_ip_addr=208.109.11.88';
        $submit_url = "https://ecommerce.ufc.ge:18443/ecomm2/MerchantHandler";
    Curl_setopt($curl, CURLOPT_SSLVERSION, 1); //0 
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
      curl_setopt($curl, CURLOPT_VERBOSE, '1');
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, '0');
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, '0');
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_TIMEOUT, 120);
      curl_setopt($curl, CURLOPT_SSLCERT, getcwd()."/assets/payment-cert/5302362.pem");
      curl_setopt($curl, CURLOPT_SSLKEYPASSWD,   'MKAjkFeBWC1z9AQk');
      curl_setopt($curl, CURLOPT_URL, $submit_url);
      $result = curl_exec($curl);
      $info = curl_getinfo($curl);
    if(curl_errno($curl))
    {
      echo 'curl error:' . curl_error($curl)."<BR>";
      die();
    }
    curl_close($curl);
    // echo $result;
    if(preg_match('/decline/',strtolower($result)))
    {
      $update_online_payment=OnlinePayment::where('online_payment_id',$get_payment->online_payment_id)->update(["payment_status"=>2,"payment_result"=>$result]);

        Session::flash("payment-error","Payment Declined .Try Again Later");
         return redirect()->route('agent-get-payments',["installmentid"=>$get_payment->booking_installment_id,"bookingid"=>$get_payment->booking_id]); 

      

    }
    else if(preg_match('/ok/',strtolower($result)) && preg_match('/authenticate/',strtolower($result)))
    {

      $update_online_payment=OnlinePayment::where('online_payment_id',$get_payment->online_payment_id)->update(["payment_status"=>1,"payment_result"=>$result]);
     
             $payment_mode="Online Card Payment";

             $update_array=array("booking_paid_status"=>1,
               "booking_paid_info"=>$result,
              "booking_paid_date"=>date('Y-m-d'),
              "booking_paid_time"=>date('H:i:s'),
              "booking_remarks"=>"Paid by ".strtoupper($payment_mode),
              "booking_payment_mode"=>strtoupper($payment_mode),
              "booking_confirm_status"=>1,
               "booking_confirm_remarks"=>"Approved",
            );
             $update_booking_payment=BookingInstallment::where('booking_installment_id',$get_payment->booking_installment_id)->update($update_array);

               $get_booking=Bookings::where('booking_sep_id',$get_payment->booking_id)->where('itinerary_status',null)->first();
                if(!empty($get_booking))
                {
                  $total_amount=$get_booking->booking_agent_amount;
                }
                else
                {
                  $total_amount=0;
                }

                  $remaining_amount=$total_amount;

              $get_booking_installment=BookingInstallment::where('booking_id',$get_payment->booking_id)->get();
            if(count($get_booking_installment)>0)
            {
               foreach($get_booking_installment as $installments)
               {
                if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                {
                   $remaining_amount=$remaining_amount-$installments->booking_install_amount;
                  
                }
               }
             }
             if($remaining_amount==0)
             {
              //if full amount is paid i.e. remaning amount is 0 then update final_amount_status of that booking
              $update_booking=Bookings::where('booking_sep_id',$get_payment->booking_id)->update(["booking_final_amount_status"=>1]);
             }
             //end check whether full booking amount is paid 




               $fetch_agent=Agents::where('agent_id',$agent_id)->first();
          $fetch_admin=Users::where('users_pid',0)->first();
             $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>You have successfully paid GEL '.$payment_amount.' through '.$payment_mode.' against payment for booking id :'.$get_payment->booking_id.'</p>
        ';
        $data = array(
          'name' => $fetch_agent->agent_name,
          'email' =>$fetch_agent->company_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data['email'], $data['name'])->subject('PAYMENT SUCCESSFUL');
        });

        $htmldata='<p>Dear Admin,</p><p>New payment of <b>GEL '.$payment_amount.'</b> has been received for <b>booking id('.$get_payment->booking_id.')</b> from agent '.$fetch_agent->agent_name.' through '.$payment_mode.' </p>';
        $data_admin= array(
          'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
          'email' =>$fetch_admin->users_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data_admin['email'], $data_admin['name'])->subject('NEW PAYMENT RECEIVED');
        });


              Session::flash("payment-success","You have made payment successfully.");
         return redirect()->route('agent-get-payments',["installmentid"=>$get_payment->booking_installment_id,"bookingid"=>$get_payment->booking_id]);
    }
     else if(preg_match('/fail/',strtolower($result)))
    {
      $update_online_payment=OnlinePayment::where('online_payment_id',$get_payment->online_payment_id)->update(["payment_status"=>2,"payment_result"=>$result]);

        Session::flash("payment-error","Payment Failed .Try Again Later");
         return redirect()->route('agent-get-payments',["installmentid"=>$get_payment->booking_installment_id,"bookingid"=>$get_payment->booking_id]);
    }
    else if(preg_match('/reverse/',strtolower($result)))
    {
      $update_online_payment=OnlinePayment::where('online_payment_id',$get_payment->online_payment_id)->update(["payment_status"=>2,"payment_result"=>$result]);

        Session::flash("payment-error","Transaction Reversed .Try Again Later");
     
         return redirect()->route('agent-get-payments',["installmentid"=>$get_payment->booking_installment_id,"bookingid"=>$get_payment->booking_id]);
    }
    else if(preg_match('/timeout/',strtolower($result)))
    {
      $update_online_payment=OnlinePayment::where('online_payment_id',$get_payment->online_payment_id)->update(["payment_status"=>2,"payment_result"=>$result]);

        Session::flash("payment-error","Transaction Timeout .Try Again Later");
         return redirect()->route('agent-get-payments',["installmentid"=>$get_payment->booking_installment_id,"bookingid"=>$get_payment->booking_id]);
    }

  }
  else
  {
    return redirect()->route('agent');
  }

}

public function agent_own_wallet(Request $request)
{
       if(session()->has('travel_agent_id'))
       {
        $agent_id=session()->get('travel_agent_id');
        $withdaw_yes=0;
            $month_numeric=date('m');
            $yearname=date('Y');
             $get_wallet=AgentWallet::where('age_wallet_agent_id',$agent_id)->orderBy('age_wallet_id','desc')->paginate(10);
            $get_commission_total=AgentWallet::select(\DB::raw("(COALESCE(sum(age_wallet_credit_amount), 0)-COALESCE(sum(age_wallet_debit_amount), 0)) as total_wallet_amount"))->where('age_wallet_agent_id',$agent_id)->where('age_wallet_status',1)->groupBy('age_wallet_agent_id')->first();

            $get_commission_total_withdraw=AgentWallet::select(\DB::raw("(COALESCE(sum(age_wallet_credit_amount), 0)-COALESCE(sum(age_wallet_debit_amount), 0)) as total_wallet_amount"))->where('age_wallet_agent_id',$agent_id)->where('age_wallet_status','!=',2)->groupBy('age_wallet_agent_id')->first();

             $get_commission_all=AgentWallet::select(\DB::raw("(COALESCE(sum(age_wallet_credit_amount), 0)) as amount_credited"),\DB::raw("(COALESCE(sum(age_wallet_debit_amount), 0)) as amount_withdrawn"))->where('age_wallet_agent_id',$agent_id)->where('age_wallet_status',1)->groupBy('age_wallet_agent_id')->first();
                 $get_commission_month=AgentWallet::select(\DB::raw("(COALESCE(sum(age_wallet_credit_amount), 0)) as amount_credited"),\DB::raw("(COALESCE(sum(age_wallet_debit_amount), 0)) as amount_withdrawn"))->where('age_wallet_agent_id',$agent_id)->where('age_wallet_month',$month_numeric)->where('age_wallet_year',$yearname)->where('age_wallet_status',1)->groupBy('age_wallet_agent_id')->first();

            if(!empty($get_commission_total))
                $total_amount=$get_commission_total->total_wallet_amount;
            else
               $total_amount=0;  

               if(!empty($get_commission_total_withdraw))
                $total_amount_withdraw=$get_commission_total_withdraw->total_wallet_amount;
            else
               $total_amount_withdraw=0;   

              if(!empty($get_commission_all))
             {
                $total_amount_credited_all=$get_commission_all->amount_credited;
                $total_amount_withdrawn_all=$get_commission_all->amount_withdrawn;
            }
            else
            {
                $total_amount_credited_all=0;
                $total_amount_withdrawn_all=0;
            }

             if(!empty($get_commission_month))
             {
                $total_amount_credited_month=$get_commission_month->amount_credited;
                $total_amount_withdrawn_month=$get_commission_month->amount_withdrawn;
            }
            else
            {
                $total_amount_credited_month=0;
                $total_amount_withdrawn_month=0;
            }




            
        return view('agent.my-wallet')->with(compact('get_wallet','total_amount','total_amount_credited_all','total_amount_withdrawn_all','total_amount_credited_month','total_amount_withdrawn_month','total_amount_withdraw','withdaw_yes'));
    }
    else
    {
        return redirect()->route('agent');
    }

}


public function booking_attachment_html(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
    $booking_id=$request->get('booking_id');
    $fetch_booking=Bookings::where('booking_sep_id',$booking_id)->first();
    if(!empty($fetch_booking))
    {
      
      if($fetch_booking->booking_attachments!="")
      {

       $attachments=unserialize($fetch_booking->booking_attachments);
       if(count($attachments)>0)
       {
        $html='<div id="previewImg_new" class="row" style="margin-bottom:40px">';
        $count=1;
        foreach($attachments as $attachment)
        {
         $html.=' <div class="col-md-2 already_attachments" id="already_attachments'.($count+1).'">
                             <span class="pull-right remove_already_attachments" title="Delete Attachment" id="remove_already_attachments'.($count+1).'" style="cursor:pointermargin-right: -20px;">  X </span>
                              <input type="hidden" name="upload_already_attachments[]" value="'.$attachment.'">
                             <a href="'.asset('assets/uploads/booking_attachments').'/'.$attachment.'" target="_blank" class="text-primary">Attachment '.$count.' </a> </div>';
          $count++;
        }
        $html.='</div>';

        $html.='<div class="input-group control-group increment" id="increment" >
          <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_attachment" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_attachment" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>';

        echo $html;



      }
      else
      {
        $html='<div id="previewImg_new" class="row"></div>
        <div class="input-group control-group increment" id="increment">
          <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_attachment" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_attachment" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>';

        echo $html;
      }


   }
   else
   {
    $html='<div id="previewImg_new" class="row"></div>
        <div class="input-group control-group increment" id="increment">
          <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_attachment" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_attachments[]" class="form-control" accept="image/*,application/pdf">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_attachment" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>';

        echo $html;
   }

    }
    else
    {
      echo "invalid";
    }

  }
  else
  {
    echo "user_session";
  }

}

public function booking_attachment_upload(Request $request)
{
  if(session()->has('travel_agent_id'))
  {
  $booking_id=$request->get('attachment_id');
  if(!empty($request->get('upload_already_attachments')))
    {
     $booking_attachments=$request->get('upload_already_attachments');
   }
   else
   {
    $booking_attachments=array();
  }

//multifile uploading
  if($request->hasFile('upload_attachments'))
  {
    foreach($request->file('upload_attachments') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_booking = time()."-".$image_name;
        $dir1 = 'assets/uploads/booking_attachments/';
        $file->move($dir1, $image_booking);
        $booking_attachments[]=$image_booking;
      }
    }
  }

  $booking_attachments=serialize($booking_attachments);

  $update_booking=Bookings::where('booking_sep_id',$booking_id)->update(["booking_attachments"=>$booking_attachments]);
  if($update_booking)
  {
    Session::flash('success-message',"This booking attachments are updated successfully");
  }
  else
  {
     Session::flash('error-message',"This booking attachments cannot be updated right now");
  }
  return redirect()->back();

}
else
{
   return redirect()->route('agent');
}

}

} //agent controller close