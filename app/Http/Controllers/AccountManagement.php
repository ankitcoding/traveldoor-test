<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\States;
use App\Cities;
use App\Currency;
use App\Suppliers;
use App\Activities;
use App\Activities_log;
use App\Transport;
use App\Transport_log;
use App\Hotels;
use App\Hotels_log;
use App\HotelMeal;
use App\Drivers;
use App\Drivers_log;
use App\Guides;
use App\Guides_log;
use App\SightSeeing;
use App\SightSeeing_log;
use App\Languages;
use App\FuelType;
use App\Vehicles;
use App\VehicleType;
use App\UserRights;
use App\Agents;
use App\SavedItinerary;
use Session;
use App\Bookings;
use App\BookingCustomer;
use App\BookingInstallment;
use App\Amenities;
use App\SubAmenities;
use App\AirportMaster;
use App\Transfers;
use App\TransferDetails;
use App\TourType;
use App\HotelType;
use App\ActivityType;
use App\Expense;
use App\Income;
use App\IncomeExpenseCategory;
use App\SettingTargetCommission;
use App\UserCommissions;
use PDF;
use Illuminate\Http\Request;
use Mail;
use App\Mail\SendMailable;
use Illuminate\Support\Facades\Schema;

class AccountManagement extends Controller
{

   private $currencyApiKey;
  private $base_currency;
  public function __construct()
  {
    date_default_timezone_set('Asia/Dubai');
    $this->currencyApiKey="f4f2d4f26341429dcf7e";
     $this->base_currency="GEL";
  }
private function rights($menu)
{
  $emp_id=session()->get('travel_users_id');
  $right_array=array();
  $employees=Users::where('users_id',$emp_id)->where('users_pid',0)->where('users_status',1)->first();
  if(!empty($employees))
  {
    $right_array['add']=1;
    $right_array['view']=1;
    $right_array['edit_delete']=1;
    $right_array['report']=1;
    $right_array['admin']=1;
    $right_array['admin_which']="add,view,edit_delete,report";
  }
  else
  {
    $employees=Users::where('users_id',$emp_id)->where('users_status',1)->first();
    if(!empty($employees))
    {
      $user_rights=UserRights::where('emp_id',$emp_id)->where('menu',$menu)->first();
      if(!empty($user_rights))
      {
        $right_array['add']=$user_rights->add_status;
        $right_array['view']=$user_rights->view_status;
        $right_array['edit_delete']=$user_rights->edit_del_status;
        $right_array['report']=$user_rights->report_status;
        $right_array['admin']=$user_rights->admin_status;
        if($user_rights->admin_which_status!="")
          $right_array['admin_which']=$user_rights->admin_which_status;
        else
          $right_array['admin_which']="No";
      }
      else
      {
        $right_array['add']=0;
        $right_array['view']=0;
        $right_array['edit_delete']=0;
        $right_array['report']=0;
        $right_array['admin']=0;
        $right_array['admin_which']="No";
      }
    }
    else
    {
      $right_array['add']=0;
      $right_array['view']=0;
      $right_array['edit_delete']=0;
      $right_array['report']=0;
      $right_array['admin']=0;
      $right_array['admin_which']="No";
    }
  }
  return $right_array;
}

public function monthly_report(Request $request)
{
  if(session()->has('travel_users_id'))
  {
    $user_id=session()->get('travel_users_id');
    $rights=$this->rights('monthly-report');

    return view('mains.monthly-report')->with(compact('rights'));
  }
  else
  {
     return redirect()->route('index');

  }

}

public function monthly_report_filters(Request $request)
{
 
    $month_full=$request->get('month');
    $month_full=explode("-",$month_full);
    $month_numeric=$month_full[0];
    $month_name=$month_full[1];
    $yearname=$request->get('year');

    $booking_details=array();
      $count=0;
      $total_bookings_profit=0;
       $total_office_expenses=0;
       $total_office_incomes=0;

      $get_bookings=Bookings::where('booking_sightseeing_id',null)->where('itinerary_status',null)->where('booking_complete_status',1)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(booking_complete_timestamp, "%Y")'),$yearname)->get();
       $actual_profit=0;
        foreach($get_bookings as $bookings)
        {

          if($bookings->booking_final_amount_status==1)
          {

            $booking_details[$count]["booking_id"]=$bookings->booking_sep_id;
            $booking_details[$count]["booking_type"]=$bookings->booking_type;
            $expenses_amt=0;
             $fetch_booking_expenses=Expense::where('expense_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_expenses as $expenses)
               {
                $expenses_amt+=$expenses->expense_amount;
               }

                $incomes_amt=0;
             $fetch_booking_incomes=Income::where('incomes_booking_id',$bookings->booking_sep_id)->get();

               foreach($fetch_booking_incomes as $incomes)
               {
                $incomes_amt+=$incomes->incomes_amount;
               }

           if($bookings->booking_type=="sightseeing")
               {
                 $booking_subject_name=unserialize($bookings->booking_subject_name);

                 if($booking_subject_name['tour_type']=="private")
                 {
                   $guide_cost=$booking_subject_name['guide_supplier_cost'];
                   $driver_cost=$booking_subject_name['driver_supplier_cost'];

                   $booking_supplier_amount=($guide_cost+$driver_cost);
                 }
                 else if($booking_subject_name['tour_type']=="group")
                 {
                    $booking_supplier_amount=0;
                 }

               }
               else
               {
                $booking_supplier_amount=$bookings->booking_supplier_amount;
               }
               $received_amount=0;
               $get_booking_installment=BookingInstallment::where('booking_id',$bookings->booking_sep_id)->get();

               if(count($get_booking_installment)>0)
               {


                $counter=1;
                foreach($get_booking_installment as $installments)
                {

                  if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                  {
                   $received_amount+=$installments->booking_install_amount;

                 }
               }
             }

            $get_profit=$received_amount-$booking_supplier_amount;

            $actual_profit+=(($get_profit+$incomes_amt)-$expenses_amt);
              $booking_details[$count]["booking_profit"]=$get_profit;
              $booking_details[$count]["booking_incomes"]=$incomes_amt;
              $booking_details[$count]["booking_expenses"]=$expenses_amt;
              $booking_details[$count]["booking_actual_profit"]=(($get_profit+$incomes_amt)-$expenses_amt);
               $booking_details[$count]["booking_role"]=$bookings->booking_role;
                $booking_details[$count]["booking_date"]=$bookings->booking_date;
                $booking_details[$count]["booking_time"]=$bookings->booking_time;
                 $booking_details[$count]["booking_complete_timestamp"]=$bookings->booking_complete_timestamp;
                 $booking_details[$count]["booking_sep_id"]=$bookings->booking_sep_id;

               $count++;
          }



        }
          $bookings_data='<table class="table table-bordered datatable">
          <thead>
          <tr>
          <th>Sr. no</th>
          <th>Booking ID</th>
          <th>Booking Type</th>
          <th>Booking Profit</th>
          <th>Incomes</th>
          <th>Expenses</th>
          <th>Actual Profit</th>
          <th>Booking Portal</th>
          <th>Booked On</th>
          <th>Completed On</th>';

          if($request->has('method'))
          {
          }
          else
          {
           $bookings_data.='<th>Action</th>';
         }
         

          $bookings_data.='</tr>
          </thead>
          <tbody>';
          $booking_count=1;
          foreach($booking_details as $bookings)
          {
            $bookings_data.='<tr>
            <td>'.$booking_count.'</td>
            <td>'.$bookings['booking_id'].'</td>
            <td>'.$bookings['booking_type'].'</td>
            <td>GEL '.$bookings['booking_profit'].'</td>
             <td>GEL '.$bookings['booking_incomes'].'</td>
            <td>GEL '.$bookings['booking_expenses'].'</td>
            <td>GEL '.$bookings['booking_actual_profit'].'</td>
            <td>'.$bookings['booking_role'].' PORTAL</td>
             <td>'.date('d/m/Y h:i a',strtotime($bookings['booking_date'].' '.$bookings['booking_time'])).'</td>
            <td>'.date('d/m/Y h:i a',strtotime($bookings['booking_complete_timestamp'])).'</td>';


            if($request->has('method'))
            {
            }
            else
            {
             $bookings_data.='<td><a class="btn btn-rounded btn-success btn-sm" href="'.route('booking-details').'?booking_id='.$bookings['booking_sep_id'].'" target="_blank">View Details</a></td>';
            }

            $bookings_data.='</tr>';
            $total_bookings_profit+=$bookings['booking_actual_profit'];
            $booking_count++;
          }

          $bookings_data.='</tbody>
          </table>';

          $get_office_incomes=Income::where('incomes_booking_id',null)->where(\DB::raw('DATE_FORMAT(incomes_occured_on, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(incomes_occured_on, "%Y")'),$yearname)->orderBy('incomes_occured_on','asc')->get();

          $incomes_data='<table class="table table-bordered datatable">
          <thead>
          <tr>
          <th>Sr. no</th>
          <th>Income Category</th>
          <th>Amount</th>
          <th>Occured on</th>
           <th>Remarks</th>
          </tr>
          </thead>
          <tbody>';
          $income_count=1;
          foreach($get_office_incomes as $incomes)
          {
            $incomes_data.='<tr>
            <td>'.$income_count.'</td>
            <td>'.$incomes->get_income_category['expense_category_name'].'</td>
            <td>GEL '.$incomes->incomes_amount.'</td>
            <td>'.date('d/m/Y h:i a',strtotime($incomes->incomes_occured_on)).'</td>
            <td>'.$incomes->incomes_remarks.'</td>
            </tr>';
            $total_office_incomes+=$incomes->incomes_amount;
            $income_count++;
          }

          $incomes_data.='</tbody>
          </table>';

           $get_office_expenses=Expense::where('expense_booking_id',null)->where(\DB::raw('DATE_FORMAT(expense_occured_on, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(expense_occured_on, "%Y")'),$yearname)->orderBy('expense_occured_on','asc')->get();

          $expenses_data='<table class="table table-bordered datatable">
          <thead>
          <tr>
          <th>Sr. no</th>
          <th>Expense Category</th>
          <th>Amount</th>
          <th>Occured on</th>
          <th>Remarks</th>
          </tr>
          </thead>
          <tbody>';
          $expenses_count=1;
          foreach($get_office_expenses as $expenses)
          {
            $expenses_data.='<tr>
            <td>'.$expenses_count.'</td>
            <td>'.$expenses->get_expense_category['expense_category_name'].'</td>
             <td>GEL '.$expenses->expense_amount.'</td>
            <td>'.date('d/m/Y h:i a',strtotime($expenses->expense_occured_on)).'</td>
            <td>'.$expenses->expense_remarks.'</td>
            </tr>';
            $total_office_expenses+=$expenses->expense_amount;
            $expenses_count++;
          }

          $expenses_data.='</tbody>
          </table>';



           $get_booking_incomes=Income::where('incomes_booking_id','!=',null)->where(\DB::raw('DATE_FORMAT(incomes_occured_on, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(incomes_occured_on, "%Y")'),$yearname)->orderBy('incomes_occured_on','asc')->get();

          $book_incomes_data='<table class="table table-bordered datatable">
          <thead>
          <tr>
          <th>Sr. no</th>
          <th>Booking ID</th>
          <th>Income Category</th>
          <th>Amount</th>
          <th>Occured on</th>
           <th>Remarks</th>
          </tr>
          </thead>
          <tbody>';
          $income_count=1;
          foreach($get_booking_incomes as $incomes)
          {
            $book_incomes_data.='<tr>
            <td>'.$income_count.'</td>
            <td>'.$incomes->incomes_booking_id.'</td>
            <td>'.$incomes->get_income_category['expense_category_name'].'</td>
            <td>GEL '.$incomes->incomes_amount.'</td>
            <td>'.date('d/m/Y h:i a',strtotime($incomes->incomes_occured_on)).'</td>
            <td>'.$incomes->incomes_remarks.'</td>
            </tr>';
            $income_count++;
          }

          $book_incomes_data.='</tbody>
          </table>';

           $get_booking_expenses=Expense::where('expense_booking_id','!=',null)->where(\DB::raw('DATE_FORMAT(expense_occured_on, "%m")'),$month_numeric)->where(\DB::raw('DATE_FORMAT(expense_occured_on, "%Y")'),$yearname)->orderBy('expense_occured_on','asc')->get();

          $book_expenses_data='<table class="table table-bordered datatable">
          <thead>
          <tr>
          <th>Sr. no</th>
           <th>Booking ID</th>
          <th>Expense Category</th>
          <th>Amount</th>
          <th>Occured on</th>
          <th>Remarks</th>
          </tr>
          </thead>
          <tbody>';
          $expenses_count=1;
          foreach($get_booking_expenses as $expenses)
          {
            $book_expenses_data.='<tr>
            <td>'.$expenses_count.'</td>
            <td>'.$expenses->expense_booking_id.'</td>
            <td>'.$expenses->get_expense_category['expense_category_name'].'</td>
             <td>GEL '.$expenses->expense_amount.'</td>
            <td>'.date('d/m/Y h:i a',strtotime($expenses->expense_occured_on)).'</td>
            <td>'.$expenses->expense_remarks.'</td>
            </tr>';
            $expenses_count++;
          }

          $book_expenses_data.='</tbody>
          </table>';
$total_profit_loss=($total_bookings_profit+$total_office_incomes)-$total_office_expenses;
 if($request->has('method'))
  {
     $rights=$this->rights('monthly-report');
    return view('mains.month-report-print')->with(compact('bookings_data','expenses_data','incomes_data','rights'))->with('month_name',$month_name)->with('yearname',$yearname)->with('total_bookings_profit',$total_bookings_profit)->with('total_office_expenses',$total_office_expenses)->with('total_office_incomes',$total_office_incomes)->with('total_profit_loss',$total_profit_loss);
  }
  else
  {
     $data=array();
 $data['bookings_data']=$bookings_data;
$data['office_expenses_data']=$expenses_data;
$data['office_incomes_data']=$incomes_data;
$data['booking_expenses_data']=$book_expenses_data;
$data['booking_incomes_data']=$book_incomes_data;
$data['total_bookings_profit']=$total_bookings_profit;
$data['total_office_expenses']=$total_office_expenses;
$data['total_office_incomes']=$total_office_incomes;
$data['total_profit_loss']=$total_profit_loss;

 echo json_encode($data);
    
  }

}

public function success(Request $request)
{
  return redirect()->route('agent-payment-processor');
}



}