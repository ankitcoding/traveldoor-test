<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\Cities;
use App\Currency;
use App\UserRights;
use App\Agents;
use App\Agents_log;
use App\Suppliers;
use App\Activities;
use App\Transport;
use App\Hotels;
use App\HotelRooms;
use App\HotelRoomSeasons;
use App\HotelRoomSeasonOccupancy;
use App\SavedItinerary;
use App\Bookings;
use App\Guides;
use App\Drivers;
use App\GuideExpense;
use App\Languages;
use App\SightSeeing;
use App\Vehicles;
use App\VehicleType;
use App\HotelType;
use App\TourType;
use App\ActivityType;
use App\AirportMaster;
use App\Amenities;
use App\SubAmenities;
use App\Transfers;
use App\TransferDetails;
use App\AgentSavedItinerary;
use App\HotelMeal;
use App\AgentWallet;
use App\BookingInstallment;
use App\Restaurants;
use App\RestaurantFood;
use App\RestaurantMenuCategory;
use App\RestaurantType;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use App\OnlinePayment;
use PDF;
use Session;
use Cookie;
use Mail;
class AgentGuideBooking extends Controller
{
  private $currencyApiKey;
  private $base_currency;
  public function __construct()
  {
    date_default_timezone_set('Asia/Dubai');
    $this->currencyApiKey="f4f2d4f26341429dcf7e";
     $this->base_currency="GEL";
  }
  private function agent_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_service_markup;
    }
    return $markup;
  }

  private function agent_own_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_own_service_markup;
    }
    return $markup;
  }
   public function guide_search()
  {
    if(session()->has('travel_agent_id'))
    {
      $countries=Countries::where('country_status',1)->get();
      $agent_id=session()->get('travel_agent_id');
      $get_guides=Guides::where('guide_status',1)->get();
      $languages=languages::get();
      return view('agent.guide-search')->with(compact('countries','agent_id','get_guides','languages'));
    }
    else
    {
      return redirect()->route('agent');
    }
  }

 public function fetchGuides(Request $request)
  {
    $markup=$this->agent_markup();
    $guide_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="guide")
        {
          if($get_individual_service[1]!="")
          {
            $guide_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }

    $own_markup=$this->agent_own_markup();
    $own_guide_markup=0;
    if($own_markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="guide")
        {
          if($get_individual_service[1]!="")
          {
            $own_guide_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }

    $languages=languages::get();
    $guide_expense=GuideExpense::get();
    $hotel_cost=0;
    $food_cost=0;
    foreach($guide_expense as $expense)
    {
      if($expense->guide_expense=="HOTEL COST")
      {
        $hotel_cost=$expense->guide_expense_cost;
      }
      else if($expense->guide_expense=="FOOD COST")
      {
        $food_cost=$expense->guide_expense_cost;
      }
    }
    $country_id=$request->get('country_id');
    $city_id=$request->get('city_id');
    $date_from=$request->get('date_from');
    $date_to=$request->get('date_to');
    session()->put('guide_country_id',$country_id);
    session()->put('guide_city_id',$city_id);
    session()->put('guide_from_date',$date_from);
    session()->put('guide_to_date',$date_to);
    $fetch_guides=Guides::where('guide_country',$country_id)->where('guide_city',$city_id);
    $fetch_guides=$fetch_guides->where('guide_approve_status',1)->where('guide_status',1)->orderBy(\DB::raw('-`guide_show_order`'), 'desc')->orderBy('guide_id','desc')->get();
    $html='
    <style>
    button.book-btn {
      background: red;
      border: none;
      border-radius: 50px;
      margin-top: 120px;
      padding: 10px 20px;
      text-align: center;
      color: white;
      width: 160px;
    }
    </style>
    <div class="row">';
    foreach($fetch_guides as $guides)
    {
      $operating_days=unserialize($guides->operating_weekdays);
      $operating_days_array=array();
      foreach($operating_days as $key=>$days)
      {
        if($days=="Yes")
        {
          $operating_days_array[]= ucwords($key);
        }
      }
      $check_blackout_during_dates=0;
      $get_dates_from=strtotime($date_from);
      $get_dates_to=strtotime($date_to);
      $blackout_days=explode(',',$guides->guide_blackout_dates);
      $date_diff=abs($get_dates_from - $get_dates_to)/60/60/24;
      $dates="";
      for($count=0;$count<=$date_diff;$count++)
      {
        $calculated_date=date("Y-m-d",strtotime("+$count day",strtotime($date_from)));
        $calculated_day=date("l",strtotime("+$count day",strtotime($date_from)));
        if(in_array($calculated_date, $blackout_days) || !in_array($calculated_day, $operating_days_array))
        {
          $check_blackout_during_dates++;
        }
      }
      if($check_blackout_during_dates<=0)
      {

           $get_guide_booking_count=Bookings::where('booking_type','guide')->where(function ($q) use($date_from,$date_to){
    $q->where(function ($q1) use($date_from){ 

      $q1->where('booking_selected_date',"<=",$date_from)
      ->where('booking_selected_to_date',">=",$date_from);
    })->orWhere(function ($q2) use($date_to){ 

      $q2->where('booking_selected_date',"<=",$date_to)
      ->where('booking_selected_to_date',">=",$date_to);
    });

})->where('booking_type_id',$guides->guide_id)->where('booking_admin_status','!=',2)->get();
      $past_bookings_array=$get_guide_booking_count->toArray();

      // echo "<pre>";
      // print_r($past_bookings_array);
      // echo "</pre>";

      if(count($past_bookings_array)<=0)
      {


        $price=$guides->guide_price_per_day;

        // $price+=$hotel_cost;
        // $price+=$food_cost;

        $guide_markup_cost=round(($price*$guide_markup)/100);
        $total_agent_cost=round(($price+$guide_markup_cost));
        $own_guide_markup_cost=round(($total_agent_cost*$own_guide_markup)/100);
        $total_cost=round(($total_agent_cost+$own_guide_markup_cost));

        $newid=base64_encode($guides->guide_id);
        $html.=' <div class="col-sm-6 col-md-4 col-lg-3 guides_rows">
        <div class="container-fluid-2 main-2">
        <div class="card profile">
        <div class="card-header profileName">
        '.ucwords($guides->guide_first_name).'
        <span class="isVerified">

        </span>
        <div class="isOnline">
        <small>GEL '.$total_cost.' /day</small>
        </div>
        </div>
        <div class="card-body profileBody">
        <div class="profilePic">';
        if($guides->guide_image!="" && $guides->guide_image!=null)
        {

          $html.='<img src="'.asset("assets/uploads/guide_images").'/'.$guides->guide_image.'" class="avatar">';

        }

        else

        {

          $html.='<img src="'.asset("assets/images/no-photo.png").'" class="avatar">';

        }
        $html.='

        </div>
        <!--<div class="profileInfo">
        <p>'. substr($guides->guide_description, 0, 40).'</p>
        </div>-->
        </div>
        <div class="card-footer actions">


        <div class="actionFirst">
        <p class="skill-a">Language Skills</p>
        <p class="title">';
        $languages_data=explode(",",$guides->guide_language);



        foreach($languages as $language)

        {

          if(in_array($language->language_id,$languages_data))

          {



            $html.='<span

            class="g-badge '.$language->language_name.'">'.$language->language_name.'</span>';

          }

        }
        $html.=' </p>
        </div>
        <div class="actionSecond">
        <a href="'.route('guide-details-view',["guide_id"=>urlencode(base64_encode($guides->guide_id))]).'" class="g-link" target="_blank"><button class="button-guide" target="_blank">Show Details</button></a>
        </div>
        </div>
        </div>
        <div class="card">
        </div>
        </div>
        </div>';
        }
      }
    }
    $html.="</div>";
    echo $html;
  }
    public function guide_details_view(Request $request,$guide_id)
  {
    if(session()->has('travel_agent_id'))
    {
      $markup=$this->agent_markup();
      $guide_markup=0;
      if($markup!="")
      {
        $get_all_services=explode("///",$markup);
        for($markup=0;$markup<count($get_all_services);$markup++)
        {
          $get_individual_service=explode("---",$get_all_services[$markup]);
          if($get_individual_service[0]=="guide")
          {
            if($get_individual_service[1]!="")
            {
              $guide_markup=$get_individual_service[1];
            }
            break;
          }
        }
      }

       $own_markup=$this->agent_own_markup();
    $own_guide_markup=0;
    if($own_markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="guide")
        {
          if($get_individual_service[1]!="")
          {
            $own_guide_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }

      $languages=languages::get();
      $countries=Countries::get();
      $guide_expense=GuideExpense::get();
      $hotel_cost=0;
      $food_cost=0;
      foreach($guide_expense as $expense)
      {
        if($expense->guide_expense=="HOTEL COST")
        {
          $hotel_cost=$expense->guide_expense_cost;
        }
        else if($expense->guide_expense=="FOOD COST")
        {
          $food_cost=$expense->guide_expense_cost;
        }

      }
      $languages_spoken=array();
      $mainid=urldecode(base64_decode($guide_id));
      $get_guides=Guides::where('guide_status',1)->where('guide_id',$mainid)->first();
      if($get_guides)
      {
        $languages_data=explode(",",$get_guides->guide_language);
        foreach($languages as $language)
        {
          if(in_array($language->language_id,$languages_data))
          {
            $languages_spoken[]=$language->language_name;
          }
        }

        $from_date=session()->get('guide_from_date');
        $to_date=session()->get('guide_to_date');
        $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $from_date.' 0:00:00');
        $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $to_date.' 0:00:00');
        $NoOfDays = $check_in_day->diffInDays($check_out_day);
        $NoOfDays=($NoOfDays+1);

        return view('agent.guide-detail-view')->with(compact('get_guides','languages_spoken','countries'))->with('from_date',$from_date)->with('to_date',$to_date)->with('no_of_days',$NoOfDays)->with('markup',$guide_markup)->with('own_markup',$own_guide_markup)->with('hotel_cost',$hotel_cost)->with('food_cost',$food_cost);
      }
      else
      {
        return redirect()->back();
      }
    }
    else
    {
      return redirect()->intended('agent');
    }
  }
 
  public function guide_booking(Request $request)
  {
    $agent_id=session()->get('travel_agent_id');
    $guide_id=$request->get('guide_id');
    $supplier_id=$request->get('supplier_id');
    $from_date=$request->get('from_date');
    $to_date=$request->get('to_date');
    $no_of_days=$request->get('no_of_days');
    $booking_guide_quantity=$request->get('booking_guide_quantity');
    $supplier_guide_price=$request->get('supplier_guide_price');
    $agent_guide_price=$request->get('agent_guide_price'); 
     $customer_guide_price=$request->get('customer_guide_price'); 
    $booking_markup_per=$request->get('markup');
    $own_markup=$request->get('own_markup');
    $guide_currency=$request->get('guide_currency');
    $booking_amount=round($customer_guide_price*$no_of_days);
    $booking_array=array("agent_id"=>$agent_id,
      "guide_id"=>$guide_id,
      "booking_supplier_id"=>$supplier_id,
      "booking_amount"=>$booking_amount,
      "from_date"=>$from_date,
      "to_date"=>$to_date,
      "no_of_days"=>$no_of_days,
      "booking_guide_quantity"=>$booking_guide_quantity,
      "supplier_guide_price"=>$supplier_guide_price,
      "agent_guide_price"=>$agent_guide_price,
      "customer_guide_price"=>$customer_guide_price,
      "guide_currency"=>$guide_currency,
      "booking_markup_per"=>$booking_markup_per,
      "booking_own_markup_per"=>$own_markup);
    $get_guides=Guides::where('guide_status',1)->where('guide_id',$guide_id)->first();
    $countries=Countries::get();
    return view('agent.booking-page')->with(compact('booking_array','get_guides','countries'))->with('booking_type','guide');
  }
  public function confirm_guide_booking(Request $request)
  {
                // echo "<pre>";
                    // print_r($request->all());
    $agent_id=session()->get('travel_agent_id');
    $fetch_admin=Users::where('users_pid',0)->first();
    $fetch_agent=Agents::where('agent_id',$agent_id)->first();
    $customer_name=$request->get('customer_name');
    $customer_email=$request->get('customer_email');  
    $customer_phone=$request->get('customer_phone');  
    $customer_country=$request->get('customer_country');
    $customer_address=$request->get('customer_address'); 
    $customer_remarks=$request->get('customer_remarks'); 
    $booking_whole_data=$request->get('booking_details');    
    $booking_details=unserialize($request->get('booking_details'));
    $guide_id=$booking_details['guide_id'];
    $booking_supplier_id=$booking_details['booking_supplier_id'];
    $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
    $booking_amount=$booking_details['booking_amount'];
    $from_date=$booking_details['from_date'];
    $to_date=$booking_details['to_date'];
    $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking_details['from_date'].' 0:00:00');
    $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking_details['to_date'].' 0:00:00');
    $NoOfdays = $check_in_day->diffInDays($check_out_day);
    $NoOfdays= $NoOfdays+1;
    $booking_guide_quantity=$booking_details['booking_guide_quantity'];
    $supplier_guide_price=$booking_details['supplier_guide_price'];
    $agent_guide_price=$booking_details['agent_guide_price'];
    $customer_guide_price=$booking_details['customer_guide_price'];
    $booking_markup_per=$booking_details['booking_markup_per'];
    $booking_own_markup_per=$booking_details['booking_own_markup_per'];
    $guide_expense=GuideExpense::get();
    $hotel_cost=0;
    $food_cost=0;
    foreach($guide_expense as $expense)
    {
      if($expense->guide_expense=="HOTEL COST")
      {
       $hotel_cost=$expense->guide_expense_cost;
     }
     else if($expense->guide_expense=="FOOD COST")
     {
      $food_cost=$expense->guide_expense_cost;
    }

  }

$booking_attachments=array();
//multifile uploading
  if($request->hasFile('customer_file'))
  {
    foreach($request->file('customer_file') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_activity = "booking-attachment-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/booking_attachments/';
        $file->move($dir1, $image_activity);
        $booking_attachments[]=$image_activity;
      }
    }
  }
  $booking_attachments=serialize($booking_attachments);




  // $other_expenses=array("hotel_cost"=>$hotel_cost,"food_cost"=>$food_cost);
   $other_expenses=array();
  $get_guides=Guides::where('guide_id',$guide_id)->where('guide_status',1)->first();
  if($get_guides)
  {
    $booking_currency=$booking_details['guide_currency'];
    $date=date("Y-m-d");
    $time=date("H:i:s");

    $booking_supplier_amount=($supplier_guide_price*$NoOfdays);
     $booking_agent_amount=($agent_guide_price*$NoOfdays);

    $get_latest_id=Bookings::latest()->value('booking_sep_id');
    $get_latest_id=$get_latest_id+1;
    $insert_booking=new Bookings;
    $insert_booking->booking_sep_id=$get_latest_id;
    $insert_booking->booking_type="guide";
    $insert_booking->booking_type_id=$guide_id;
    $insert_booking->booking_agent_id=$agent_id;
    $insert_booking->booking_role="AGENT";
    $insert_booking->booking_supplier_id=$booking_supplier_id;
    $insert_booking->customer_name=$customer_name;
    $insert_booking->customer_contact=$customer_phone;
    $insert_booking->customer_email=$customer_email;
    $insert_booking->customer_country=$customer_country;
    $insert_booking->customer_address=$customer_address;
    $insert_booking->booking_adult_count=$booking_guide_quantity;
    $insert_booking->booking_subject_days=$NoOfdays;
    $insert_booking->supplier_adult_price=$supplier_guide_price;
    $insert_booking->agent_adult_price=$agent_guide_price;
    $insert_booking->customer_adult_price=$customer_guide_price;
    $insert_booking->other_expenses=serialize($other_expenses);
    $insert_booking->booking_currency=$booking_currency;
    $insert_booking->booking_supplier_amount=$booking_supplier_amount;
    $insert_booking->booking_agent_amount=$booking_agent_amount;
    $insert_booking->booking_amount=$booking_amount;
    $insert_booking->booking_markup_per=$booking_markup_per;
    $insert_booking->booking_own_markup_per=$booking_own_markup_per;
    $insert_booking->booking_remarks=$customer_remarks;
    $insert_booking->booking_selected_date=$from_date;
    $insert_booking->booking_selected_to_date=$to_date;
    $insert_booking->booking_whole_data=$booking_whole_data;
    $insert_booking->booking_attachments=$booking_attachments;
    $insert_booking->booking_date=$date;
    $insert_booking->booking_time=$time;
    if($insert_booking->save())
    {
      $booking_id=$get_latest_id;
      $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>You have successfully booked guide for your customer on traveldoor[dot]ge portal . Your booking id is '.$booking_id.'.</p>
      ';
      $data = array(
        'name' => $fetch_agent->agent_name,
        'email' =>$fetch_agent->company_email
      );
      Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
        $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
        $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
      });
      $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new guide booking on traveldoor[dot]ge portal with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
      $data_supplier= array(
        'name' => $fetch_supplier->supplier_name,
        'email' =>$fetch_supplier->company_email
      );
      Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
        $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
        $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW GUIDE BOOKING');
      });
      $htmldata='<p>Dear Admin,</p><p>New Guide has been booked with booking id '.$booking_id.' by agent '.$fetch_agent->agent_name.' for the supplier '.$fetch_supplier->supplier_name.' </p>';
      $data_admin= array(
        'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
        'email' =>$fetch_admin->users_email
      );
      Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
        $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
        $m->to($data_admin['email'], $data_admin['name'])->subject('NEW GUIDE BOOKING ON PORTAL');
      });
      $message="You have successfully booked guide for your customer ".$customer_name;
      session()->put('booking_message',$message);
      return redirect()->route('booking-success');
    }
    else
    {
      $message="You selected guide cannot be booked right now";
      session()->put('booking_message_fail',$message);
      return redirect()->route('booking-fail');
    }
  }
  else
  {
    return redirect()->back();
  }
}
 
}