<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\Cities;
use App\Currency;
use App\UserRights;
use App\Agents;
use App\Agents_log;
use App\Suppliers;
use App\Activities;
use App\Transport;
use App\Hotels;
use App\HotelRooms;
use App\HotelRoomSeasons;
use App\HotelRoomSeasonOccupancy;
use App\SavedItinerary;
use App\Bookings;
use App\Guides;
use App\Drivers;
use App\GuideExpense;
use App\Languages;
use App\SightSeeing;
use App\Vehicles;
use App\VehicleType;
use App\HotelType;
use App\TourType;
use App\ActivityType;
use App\AirportMaster;
use App\Amenities;
use App\SubAmenities;
use App\Transfers;
use App\TransferDetails;
use App\AgentSavedItinerary;
use App\HotelMeal;
use App\AgentWallet;
use App\BookingInstallment;
use App\Restaurants;
use App\RestaurantFood;
use App\RestaurantMenuCategory;
use App\RestaurantType;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use App\OnlinePayment;
use PDF;
use Session;
use Cookie;
use Mail;
class AgentHotelBooking extends Controller
{
  private $currencyApiKey;
  private $base_currency;
  public function __construct()
  {
    date_default_timezone_set('Asia/Dubai');
    $this->currencyApiKey="f4f2d4f26341429dcf7e";
     $this->base_currency="GEL";
  }
  private function agent_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_service_markup;
    }
    return $markup;
  }

  private function agent_own_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_own_service_markup;
    }
    return $markup;
  }


  public function hotel_search(Request $request)
  {
    if(session()->has('travel_agent_id'))
    {
      $countries=Countries::where('country_status',1)->get();
      $agent_id=session()->get('travel_agent_id');
      $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();
      $hotel_name_data=Hotels::where('hotel_status',1)->get();
      if($request->has('modify'))
      {
        $country=$request->get('country');
        $city=$request->get('city');
        $checkin=$request->get('date_from');
        $checkout=$request->get('date_to');
        $adults=$request->get('no_of_adults');
        $child=$request->get('no_of_child');
        $room=$request->get('no_of_rooms');
        $star=$request->get('star');
        $child_age=$request->get('child_age');
        $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id",$country)->select("cities.*")->orderBy('cities.name','asc')->get();
        $hotel_array=array("selected_country"=>$country,
          "selected_city"=>$city,
          "checkin"=>$checkin,
          "checkout"=>$checkout,
          "adults"=>$adults,
          "child"=>$child,
          "room"=>$room,
          "star"=>$star,
          "hotel_child_age"=>$child_age
        );
        return view('agent.hotel-search')->with(compact('countries','agent_id','fetch_hotel_type','fetch_cities','hotel_array'));
      }
      else
      {
        return view('agent.hotel-search')->with(compact('countries','agent_id','fetch_hotel_type','hotel_name_data'));
      }
    }
    else
    {
      return redirect()->route('agent');
    }
  }
  public function fetchHotelNames(Request $request)
  {
    if(session()->has('checkin_date') && session()->has('hotel_adults'))
    {
    $date_from=session()->get('checkin_date');
    }
    else
    {
          $date_from=date('Y-m-d');
    }

    $search=$request->get('search_value');
    $fetchhotels=Hotels::where('hotel_name','like','%'.$search.'%')->where('booking_validity_from',"<=",$date_from)->where('booking_validity_to',">=",$date_from)->where('hotel_approve_status',1)->where('hotel_status',1)->limit(10)->get();
    $html="<option value=''>SELECT HOTEL</option>";
    if(count($fetchhotels)>0)
    {
      foreach($fetchhotels as $hotels)
      {
        $html.="<option value='".$hotels->hotel_country."--".$hotels->hotel_city."--".$hotels->hotel_name."'>".$hotels->hotel_name."</option>";
      }
    }
    echo $html;
  }

   public function fetchHotels(Request $request)
  {
    $markup=$this->agent_markup();
    $hotel_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="hotel")
        {
          if($get_individual_service[1]!="")
          {
            $hotel_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }

     $own_markup=$this->agent_own_markup();
    $own_hotel_markup=0;
    if($own_markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="hotel")
        {
          if($get_individual_service[1]!="")
          {
            $own_hotel_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
    $country_id=$request->get('country_id');
    $city_id=$request->get('city_id');
    $hotel_name=$request->get('hotel_name');
    $date_from=$request->get('date_from');
    $date_to=$request->get('date_to');
    $child_age=$request->get('child_age');
    $adults=$request->get('adults');
    $children=$request->get('children');
    $rooms=$request->get('rooms');
    $hotel_rating=$request->get('hotel_rating');
    $hotel_type=$request->get('hotel_type');
    session()->put('country_id',$country_id);
    session()->put('city_id',$city_id);
    session()->put('checkin_date',$date_from);
    session()->put('checkout_date',$date_to);
    session()->put('room_quantity',$rooms);
    session()->put('hotel_adults',$adults);
    session()->put('hotel_children',$children);
    session()->put('hotel_child_age',$child_age);
    session()->put('hotel_rating',$hotel_rating);
    session()->put('hotel_type',$hotel_type);


      $limit=10;
    if($request->has('offset'))
    {
        $offset=$request->get('offset');
    }
    else
    {
        $offset=0;
    }


    $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();
    $hotel_type_array=$fetch_hotel_type->toArray();
   
      $hotel_name=$request->get('hotel_name');
       if($hotel_name!="")
    {
      $hotel_name_array=explode("--",$hotel_name);
      $country_id=$hotel_name_array[0];
      $city_id=$hotel_name_array[1];
        session()->put('country_id',$country_id);
    session()->put('city_id',$city_id);
      $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id)->where('hotel_name',$hotel_name_array[2]);

    }
    else
    {
      $fetch_hotels=Hotels::where('hotel_country',$country_id)->where('hotel_city',$city_id);
    }
  

    if($date_from!="" )
    {
      $fetch_hotels=$fetch_hotels->where('booking_validity_from',"<=",$date_from)->where('booking_validity_to',">=",$date_from);
    }
  //  else if($date_from=="" && $date_to!="")
  //  {
  //   $fetch_hotels=$fetch_hotels->where('booking_validity_to',">=",$date_to);
  // }
  // else if($date_from!="" && $date_to!="")
  // {
  //   $fetch_hotels=$fetch_hotels->whereBetween('booking_validity_from',[$date_from,$date_to]);
  // }
    if($hotel_rating!="0")
    {
      $fetch_hotels=$fetch_hotels->where('hotel_rating',$hotel_rating);
    }
    if($hotel_type!="0")
    {
      $fetch_hotels=$fetch_hotels->where('hotel_type',$hotel_type);
    }
    $fetch_hotels=$fetch_hotels->where('hotel_approve_status',1)->where('hotel_status',1)->orderBy(\DB::raw('-`hotel_show_order`'), 'desc')->orderBy('hotel_id','desc')->offset($offset*$limit)->take($limit)->get();
    $html='
    <style>
    button.book-btn {
      background: red;
      border: none;
      border-radius: 50px;
      margin-top: 120px;
      padding: 10px 20px;
      text-align: center;
      color: white;
      width: 160px;
    }
    </style>
    <div class="row">';
    foreach($fetch_hotels as $hotels)
    {

      // $hotel_season_details=unserialize($hotels->hotel_season_details);

      //  $rate_allocation_details=unserialize($hotels->rate_allocation_details);
    $room_price_array=array();
     $room_currency_array=array();


     $hotel_rooms=HotelRooms::where('hotel_id',$hotels->hotel_id)->get();

     $hotel_currency=$hotels->hotel_currency;


     $room_min_price=0;
     foreach($hotel_rooms as $rooms_value)
     {

      if($hotel_currency==null)
      {
        $hotel_currency=$rooms_value->hotel_room_currency;
      }
      $get_hotel_seasons=HotelRoomSeasons::where('hotel_room_id_fk',$rooms_value->hotel_room_id)->where('hotel_room_season_validity_from','<=',$date_from)->where('hotel_room_season_validity_to','>=',$date_from)->get();

      foreach($get_hotel_seasons as $hotel_seasons)
      {
        $get_occupancy=HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();

        foreach($get_occupancy as $occupancy)
        {
          if($room_min_price==0)
          {
             $room_min_price=$occupancy->hotel_room_occupancy_price;
          }
          else if($occupancy->hotel_room_occupancy_price<$room_min_price)
          {
            $room_min_price=$occupancy->hotel_room_occupancy_price;
          }
        }
      }
     }

      // for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
      // {

      //   if($hotel_season_details[$rate_allocation_count]['booking_validity_from']<=$date_from && $hotel_season_details[$rate_allocation_count]['booking_validity_to']>=$date_from)
      //   {

      //   }
      //   else
      //   {
      //     continue;
      //   }
      //   for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
      //   {
      //     $room_price_array[]=$rate_allocation_details[$rate_allocation_count]['room_max'][$room_count];
      //     $room_currency_array[]=$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count];

      //   }
      // }



     
      $new_currency="";
      if($room_min_price>0)
      {

        $price=$room_min_price;
        if($hotel_currency!="GEL")
        {
          $new_currency= $hotel_currency;

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$new_currency."_".$this->base_currency;

        //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
        
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
         $price=round($price*$conversion_price,2);
          
        }
      $markup_cost=round(($price*$hotel_markup)/100);
      $total_agent_cost=round(($price+$markup_cost));

      $hotel_markup_cost=round(($total_agent_cost*$own_hotel_markup)/100);
      $total_cost=round(($total_agent_cost+$hotel_markup_cost));

      $newid=urlencode(base64_encode($hotels->hotel_id));
      $html.='<div class="col-md-12" style="margin:0 0 25px">
      <div class="hotel-list-div">

      <div class="hotel-div">
      <div class="hotel-img-div">
      <div class="flexslider2">
      <ul class="slides">';
      $hotelimage=unserialize($hotels->hotel_images);
       if(!empty($hotelimage))
      {

        $image_show_number=array_rand($hotelimage);
         $html.='<img class="img-slide" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[$image_show_number].'"
      alt="slide"  />';
      }
      else
      {
        $html.='<img class="img-slide" src="'.asset("assets/images/no-photo.png").'"
        alt="slide"  />';
      }
     
      $html.='</ul>
      </div>
      </div>
      <div class="hotel-details">
      <div class="heading-div">
      <p class="hotel-name">'.$hotels->hotel_name.'</p>
      <div class="rate-no">
      </div>
      </div>
      ';
      if(array_search($hotels->hotel_type,array_column($hotel_type_array,"hotel_type_id"))!==false)
      {
        $array_key=array_search($hotels->hotel_type,array_column($hotel_type_array,"hotel_type_id"));
        $html.='<a href="#" class="img-div">
        <i class=" fa fa-building-o"></i>
        </a><p class="hotel-type">'.$hotel_type_array[$array_key]['hotel_type_name'].'</p>';
      }

      $html.='<div class="heading-div">
      <div class="rating">';
      for($jt=1;$jt<=$hotels->hotel_rating;$jt++)
      {
        $html.='<span class="fa fa-star checked"></span>';
      }
      $html.='</div>
      </div>
      <br>
      <p class="time-info">'.$hotels->hotel_address.'</p>
      <div class="tags-div">';
      if($hotels->hotel_best_status==1)
      $html.='<p>Best Seller</p>';

      if($hotels->hotel_popular_status==1)
      $html.='<p>Popular</p>';
     
      $html.='</div>
      </div>
      </div>
      <div class="hotel-info-div">
      <p class="offer">GEL '.$total_cost.'</p>
      <p class="days">Per Night</p>
      <a href="'.route('hotel-detail',['hotel_id'=>$newid]).'" target="_blank"><button class="book-btn" style="cursor:pointer">Show Details</button></a>
      </div>
      </div>
      </div>';
    }

      }
      
    if(preg_match('/slides/',$html))
    {
      echo $html;
    }
    else
    {
      echo "<div class='row'>
      <div class='col-md-12'>
      <h4 class='text-center'>No Results Found</h4>
      </div>
      </div>";
    }
  }

  public function hotel_detail(Request $request,$hotel_id)
  {
    if(session()->has('travel_agent_id'))
    {
      $markup=$this->agent_markup();
      $hotel_markup=0;
      if($markup!="")
      {
        $get_all_services=explode("///",$markup);
        for($markup=0;$markup<count($get_all_services);$markup++)
        {
          $get_individual_service=explode("---",$get_all_services[$markup]);
          if($get_individual_service[0]=="hotel")
          {
            if($get_individual_service[1]!="")
            {
              $hotel_markup=$get_individual_service[1];
            }
            break;
          }
        }
      }

      $own_markup=$this->agent_own_markup();
      $own_hotel_markup=0;
      if($own_markup!="")
      {
        $get_all_services=explode("///",$own_markup);
        for($markup=0;$markup<count($get_all_services);$markup++)
        {
          $get_individual_service=explode("---",$get_all_services[$markup]);
          if($get_individual_service[0]=="hotel")
          {
            if($get_individual_service[1]!="")
            {
              $own_hotel_markup=$get_individual_service[1];
            }
            break;
          }
        }
      }

      $mainid=urldecode(base64_decode($hotel_id));
      $currency=Currency::get();
      $hotel_meal=HotelMeal::get();
      $countrycode=session()->get('travel_agent_country');
      $get_hotels=Hotels::where('hotel_status',1)->where('hotel_id',$mainid)->first();
      if(session()->get('checkin_date')!="" && session()->get('checkout_date')!="" )
      {
         $checkin_date=session()->get('checkin_date');
      $checkout_date=session()->get('checkout_date');
      }
      else
      {
        $checkin_date=date("Y-m-d");
        $checkout_date=date("Y-m-d",strtotime("+1 day",strtotime($checkin_date)));
      }
     
      $room_quantity=session()->get('room_quantity');
      $hotel_adults=session()->get('hotel_adults');
      $hotel_children=session()->get('hotel_children');
      $hotel_child_age=session()->get('hotel_child_age');
//hotel type
      $fetch_hotel_type=HotelType::where('hotel_type_id',$get_hotels['hotel_type'])->where('hotel_type_status',1)->first();
      $hotel_type="";
      if(!empty($fetch_hotel_type))
      {
        $hotel_type=$fetch_hotel_type['hotel_type_name'];
      }
//end hotel type
      if($checkin_date!="" && $checkout_date!="")
      {
        $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $checkin_date.' 0:00:00');
        $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $checkout_date.' 0:00:00');
        $NoOfNights = $check_in_day->diffInDays($check_out_day);
      }
      else
      {
        $NoOfNights=0;
      }

      // $hotel_season_details=unserialize($get_hotels['hotel_season_details']);

      //  $rate_allocation_details=unserialize($get_hotels['rate_allocation_details']);
     $room_currency_array=array();
      // for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
      // {

      //   for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
      //   {
      //     $room_currency_array[]=$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count];

      //   }
      // }
     $currency=$get_hotels['hotel_currency'];

      $hotel_rooms=HotelRooms::where('hotel_id',$get_hotels['hotel_id'])->get();
     $room_min_price=0;
     foreach($hotel_rooms as $rooms_value)
     {

      if($currency==null)
      {
        $currency=$rooms_value->hotel_room_currency;
      }
    }

      $currency_price_array=array();
        if($currency=="GEL")
        {

        }
        else
        {
           $new_currency=$currency;

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$new_currency."_".$this->base_currency;

        //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
          
        //  $currency_price_array[$currency]=round($conversion_price,2);
        $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);
        }
         

      $currency_price_array["GEL"]=1;
     
        


      $hotel_country=session()->get('country_id');
      $hotel_city=session()->get('city_id');

      $get_city_name=ServiceManagement::searchCities($hotel_city,$hotel_country);
      $city_name=$get_city_name['name'];


      //Hotel surroundings
      // if(empty(session()->get("famousPlaces")))
      // {

        $key="AIzaSyB6FvJGJ8F5aS7HR_GJt9Bz9q3SvTyhoRk";
        $queryString = http_build_query([
         'key' => $key,
         'query' => "points of interest in ".$city_name,
       ]);

        $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?".$queryString;


        $curlConnection = curl_init();
        curl_setopt($curlConnection, CURLOPT_URL, $url);
        curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, true);
        $famousPlaces = curl_exec($curlConnection);
        curl_close($curlConnection);
        $jsonArrayResponse = json_decode($famousPlaces);
        session()->put("famousPlaces",$jsonArrayResponse->results);


         $queryString = http_build_query([
         'key' => $key,
         'query' =>$city_name,
         'type' =>'airport',
       ]);

        $url = "https://maps.googleapis.com/maps/api/place/textsearch/json?".$queryString;


        $curlConnection = curl_init();
        curl_setopt($curlConnection, CURLOPT_URL, $url);
        curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, true);
        $famousPlaces = curl_exec($curlConnection);
        curl_close($curlConnection);
        $jsonArrayResponse = json_decode($famousPlaces);
        session()->put("closestAirport",$jsonArrayResponse->results);
      // }
    //End of Hotel surroundings


      $get_hotel_booking_count=Bookings::where('booking_type','hotel')->where(function ($q) use($checkin_date,$checkout_date){
    $q->where(function ($q1) use($checkin_date){ 

      $q1->where('booking_selected_date',"<=",$checkin_date)
      ->where('booking_selected_to_date',">=",$checkin_date);
    })->orWhere(function ($q2) use($checkout_date){ 

      $q2->where('booking_selected_date',"<=",$checkout_date)
      ->where('booking_selected_to_date',">=",$checkout_date);
    });

})->where('booking_type_id',$get_hotels['hotel_id'])->where('booking_admin_status','!=',2)->get();
      $past_bookings_array=$get_hotel_booking_count->toArray();


      return view('agent.hotel-detail-new')->with(compact('get_hotels'))->with('currency',$currency)->with('hotel_meal',$hotel_meal)->with('countrycode',$countrycode)->with('markup',$hotel_markup)->with('own_markup',$own_hotel_markup)->with('country',$hotel_country)->with('city',$hotel_city)->with('checkin',$checkin_date)->with('checkout',$checkout_date)->with('room_quantity',$room_quantity)->with('hotel_adults',$hotel_adults)->with('hotel_children',$hotel_children)->with('hotel_child_age',$hotel_child_age)->with('NoOfNights',$NoOfNights)->with("hotel_type",$hotel_type)->with('past_bookings_array',$past_bookings_array)->with('currency_price',$currency_price_array);
    }
    else
    {
      return redirect()->intended('agent');
    }
  }

  public function hotel_surroundings(Request $request)
  {
      $country=session()->get('country_id');
       $city=session()->get('city_id');
       $get_city_name=ServiceManagement::searchCities($city,$country);
      $city_name=$get_city_name['name'];
      $hotel_id=$request->get('id');
      $fetch_hotel=Hotels::where('hotel_id',$hotel_id)->first();

      $hotel_name=$fetch_hotel['hotel_name'];
      $hotel_address=$fetch_hotel['hotel_address'];

      $fetch_country=Countries::where('country_id',$country)->where('country_status',1)->first();
      $country_name=$fetch_country['country_name'];


      $famousPlaces = session()->get("famousPlaces");
      $famousHtml="  <div class='wrap-div'>
       <h2 class='wrap-heading'>What's nearby</h2>
         <div class='column-div'>";
       $count=0;
       foreach ($famousPlaces as $places) {
        $famousPlacesArray[$count]['place_id']=$places->place_id;
        $famousPlacesArray[$count]['name']=$places->name;
        $famousPlacesArray[$count]['address']=$places->formatted_address;
        $famousPlacesArray[$count]['rating']=$places->rating;
        $count++;
       }

       foreach($famousPlacesArray as $places)
       {

         $key="AIzaSyB6FvJGJ8F5aS7HR_GJt9Bz9q3SvTyhoRk";
        $queryString = http_build_query([
          'key' => $key,
         'origins' => $hotel_name." ".$hotel_address,
         'destinations' => $places['name']." ".$places['address'].",".$city_name.",".$country_name,
       ]);

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?".$queryString;


        $curlConnection = curl_init();
        curl_setopt($curlConnection, CURLOPT_URL, $url);
        curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, true);
        $distanceMatrix = curl_exec($curlConnection);
        curl_close($curlConnection);
        $jsonArrayResponse = json_decode($distanceMatrix);
        $famousHtml.='<div class="info-div">
       <p>'.$places['name'].'
        </p>
         <p class="distance">'.$jsonArrayResponse->rows[0]->elements[0]->distance->text.'
        </p>
        </div>';
       }
          $famousHtml.="</div></div>";


      $closestAirport = session()->get("closestAirport");
      $airportHtml="  <div class='wrap-div'>
       <h2 class='wrap-heading'>Closest Airport</h2>
         <div class='column-div'>";
       $count=0;
       foreach ($closestAirport as $places) {
        $closestAirportArray[$count]['place_id']=$places->place_id;
        $closestAirportArray[$count]['name']=$places->name;
        $closestAirportArray[$count]['address']=$places->formatted_address;
        $closestAirportArray[$count]['rating']=$places->rating;
        $count++;
       }

       foreach($closestAirportArray as $places)
       {

         $key="AIzaSyB6FvJGJ8F5aS7HR_GJt9Bz9q3SvTyhoRk";
        $queryString = http_build_query([
          'key' => $key,
         'origins' => $hotel_name." ".$hotel_address,
         'destinations' => $places['name']." ".$places['address'].",".$city_name.",".$country_name,
       ]);

        $url = "https://maps.googleapis.com/maps/api/distancematrix/json?".$queryString;


        $curlConnection = curl_init();
        curl_setopt($curlConnection, CURLOPT_URL, $url);
        curl_setopt($curlConnection, CURLOPT_RETURNTRANSFER, true);
        $distanceMatrix = curl_exec($curlConnection);
        curl_close($curlConnection);
        $jsonArrayResponse = json_decode($distanceMatrix);
        $airportHtml.='<div class="info-div">
       <p>'.$places['name'].'
        </p>
         <p class="distance">'.$jsonArrayResponse->rows[0]->elements[0]->distance->text.'
        </p>
        </div>';
       }
          $airportHtml.="</div></div>";




          $data=array("famousHtml"=>$famousHtml,
                      "airportHtml"=>$airportHtml);
          echo json_encode($data);

  }
  public function hotel_room_detail(Request $request)
  {
    $html='';
     $hotel_id=$request->get('hotel_id');
    $room_id=$request->get('room_id');
    $get_hotel_room=HotelRooms::where('hotel_id',$hotel_id)->where('hotel_room_id',$room_id)->first();
   if(!empty($get_hotel_room))
   {
    $hotelimage=unserialize($get_hotel_room->hotel_room_images);
 
    $html='
    <div class="container">
    <div class="row">
    <div class="col-md-7">
    <div class="flexslider1 flex-silder-div">
    <ul class="slides">';
    if(!empty($hotelimage))
    {

      for($images=0;$images< count($hotelimage);$images++)
      {
        if(count($hotelimage)==1)
        {

          $html.='<li
          data-thumb="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'">
          <img src="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'"
          alt="slide" />';


        }

        $html.='<li
        data-thumb="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'">
        <img src="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'"
        alt="slide" />
        </li>';

      }
    }
    else
    {

      $html.='<li
      data-thumb="'.asset('assets/images/no-photo.png').'">
      <img src="'.asset('assets/images/no-photo.png').'"
      alt="slide" />
      </li>';

    }
    $html.='</ul>
    </div>
    </div>
    <div class="col-md-5">
    <!--<div class="tags-div">
    <span class="tag"><i class="icon-tag fa fa-caret-right"></i>30m<sup>2</sup></span>
    <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Garden View</span>
    <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Landmark view</span>
    <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Garden View</span>
    <span class="tag"><i class="icon-tag fa fa-caret-right"></i>City view</span>
    <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Inner courtyard view</span>
    </div>-->';

      $html.= '<p><b>Room Size: </b>'.$get_hotel_room->hotel_room_size.'</p>';
  
    $html.='
    <!--<p class="bed-div">2 single beds <i class="fa fa-bed"></i></p>-->';
    if(!empty($get_hotel_room))
    {
      $room_amenities=unserialize($get_hotel_room->hotel_room_amenities);
      if(!empty($room_amenities))
      {
        $get_amenities=$room_amenities;
      }
      else
      {
        $get_amenities=array();
      }
    }
    else
    {
      $get_amenities=array();
    }
    if($get_amenities==null || $get_amenities=="")
    {
    }
    else
    {

      foreach($get_amenities as $amenities)
      {
        if(!empty($amenities[1]))
        {
          $html.='<div class="list-section">';
          $get_amenities_name=LoginController::fetchAmenitiesName($amenities[0]);
          $html.='<p class="list-heading">'.$get_amenities_name['amenities_name'].'</p>
          <ul class="list-ul">';

          foreach($amenities[1] as $sub_amenities)
          {

            $get_sub_amenities_name=LoginController::fetchSubAmenitiesName($sub_amenities,$amenities[0]);
            $html.='<li>'.$get_sub_amenities_name['sub_amenities_name'].'</li>';
          }
          $html.='</ul>
          </div>';
        }
      }
    }
    $html.='<!--  <p class="bed-div"><b>Smoking:</b> ​No smoking</p>
    <p class="bed-div"><b>Parking:</b> ​No Parking</p> -->
    </div>
    </div>
    </div>';
  }
  else
  {
    $html="";
  }
    echo $html;

    // $hotel_id=$request->get('hotel_id');
    // $season_index=$request->get('season_index');
    // $room_index=$request->get('room_index');
    // $get_hotels=Hotels::where('hotel_id',$hotel_id)->first();
    // $rate_allocation_details=unserialize($get_hotels->rate_allocation_details);
    // if(array_key_exists('room_images',$rate_allocation_details[$season_index]))
    // {
    //   if(!empty($rate_allocation_details[$season_index]['room_images'][$room_index]))
    //   {
    //     $hotelimage=$rate_allocation_details[$season_index]['room_images'][$room_index];
    //   }
    //   else
    //   {
    //     $hotelimage=array();
    //   }
    // }
    // else
    // {
    //   $hotelimage=array();
    // }
    // $html='
    // <div class="container">
    // <div class="row">
    // <div class="col-md-7">
    // <div class="flexslider1 flex-silder-div">
    // <ul class="slides">';
    // if(!empty($hotelimage))
    // {

    //   for($images=0;$images< count($hotelimage);$images++)
    //   {
    //     if(count($hotelimage)==1)
    //     {

    //       $html.='<li
    //       data-thumb="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'">
    //       <img src="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'"
    //       alt="slide" />';


    //     }

    //     $html.='<li
    //     data-thumb="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'">
    //     <img src="'.asset('assets/uploads/hotel_images').'/'.$hotelimage[$images].'"
    //     alt="slide" />
    //     </li>';

    //   }
    // }
    // else
    // {

    //   $html.='<li
    //   data-thumb="'.asset('assets/images/no-photo.png').'">
    //   <img src="'.asset('assets/images/no-photo.png').'"
    //   alt="slide" />
    //   </li>';

    // }
    // $html.='</ul>
    // </div>
    // </div>
    // <div class="col-md-5">
    // <!--<div class="tags-div">
    // <span class="tag"><i class="icon-tag fa fa-caret-right"></i>30m<sup>2</sup></span>
    // <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Garden View</span>
    // <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Landmark view</span>
    // <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Garden View</span>
    // <span class="tag"><i class="icon-tag fa fa-caret-right"></i>City view</span>
    // <span class="tag"><i class="icon-tag fa fa-caret-right"></i>Inner courtyard view</span>
    // </div>-->';
    // if(!empty($rate_allocation_details[$season_index]['room_size'][$room_index]))
    // {
    //   $html.= '<p><b>Room Size: </b>'.$rate_allocation_details[$season_index]['room_size'][$room_index].'</p>';
    // }
    // else
    // {
    // }
    // $html.='
    // <!--<p class="bed-div">2 single beds <i class="fa fa-bed"></i></p>-->';
    // if(array_key_exists('room_amenities',$rate_allocation_details[$season_index]))
    // {
    //   if(!empty($rate_allocation_details[$season_index]['room_amenities'][$room_index]))
    //   {
    //     $get_amenities=$rate_allocation_details[$season_index]['room_amenities'][$room_index];
    //   }
    //   else
    //   {
    //     $get_amenities=array();
    //   }
    // }
    // else
    // {
    //   $get_amenities=array();
    // }
    // if($get_amenities==null || $get_amenities=="")
    // {
    // }
    // else
    // {

    //   foreach($get_amenities as $amenities)
    //   {
    //     if(!empty($amenities[1]))
    //     {
    //       $html.='<div class="list-section">';
    //       $get_amenities_name=LoginController::fetchAmenitiesName($amenities[0]);
    //       $html.='<p class="list-heading">'.$get_amenities_name['amenities_name'].'</p>
    //       <ul class="list-ul">';

    //       foreach($amenities[1] as $sub_amenities)
    //       {

    //         $get_sub_amenities_name=LoginController::fetchSubAmenitiesName($sub_amenities,$amenities[0]);
    //         $html.='<li>'.$get_sub_amenities_name['sub_amenities_name'].'</li>';
    //       }
    //       $html.='</ul>
    //       </div>';
    //     }
    //   }
    // }
    // $html.='<!--  <p class="bed-div"><b>Smoking:</b> ​No smoking</p>
    // <p class="bed-div"><b>Parking:</b> ​No Parking</p> -->
    // </div>
    // </div>
    // </div>';
    // echo $html;
  }

    public function hotel_booking(Request $request)
  {
    
    $agent_id=session()->get('travel_agent_id');
    $hotel_id=$request->get('hotel_id');
    $supplier_id=$request->get('supplier_id');
    $checkin_date=$request->get('checkin_date');
    $checkout_date=$request->get('checkout_date');
    $no_of_days=$request->get('no_of_days');
    $hotel_adults=$request->get('hotel_adults');
    $hotel_children=$request->get('hotel_children');
    $hotel_child_age=$request->get('hotel_child_age');
    // $booking_room_quantity=$request->get('room_quantity');
    $booking_room_quantity=0;
   //  $room_type=$request->get('room_type');
   //  $room_count=$request->get('room_count');

   //  if(!empty($room_count))
   //  {
   //    $room_total_count=count($room_count);   
   //  }
   //  else
   //  {
   //    $room_total_count=0;
   //  }
   //  $hotel_room_detail=array();
   //  $supplier_total_price=0;
   //   $agent_total_price=0;
   //    $customer_total_price=0;
   //   $room_all_names=array();

   //   $hotel_total_cost=0;

   //  $room_detail_count=0;

   //  for($i=0;$i<$room_total_count;$i++)
   //  {
      
   //    $supplier_price=$request->get('room_price_'.$i);
   //  $room_name=$request->get('room_name_'.$i);
   //  $room_qty=$request->get('room_select_'.$i);
   //   $agent_room_price=$request->get('room_agent_price_markup_'.$i);
   //  $customer_room_price=$request->get('room_price_markup_'.$i);
   //  $room_currency=$request->get('room_price_currency_'.$i);
   //  $room_currency_conversion_rate=$request->get('room_price_convert_rate_'.$i);

   //   $room_extra_bed=$request->get('room_extra_bed_'.$i);
   //  $room_extra_bed_supplier_price=$request->get('room_extra_bed_price_'.$i);
   //  $room_extra_bed_agent_price=$request->get('room_agent_extra_bed_price_'.$i);
   //  $room_extra_bed_customer_price=$request->get('room_extra_bed_price_markup_'.$i);
   //    if($room_qty>0)
   //    {
   //        $booking_room_quantity+=$room_qty;
   //      $hotel_total_cost+=$customer_room_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_name']=$room_name;
   //     $room_all_names[]=$room_name;

   //     $hotel_room_detail[$room_detail_count]['room_qty']=$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_supplier_price']=$supplier_price;
   //     $supplier_total_price+=$supplier_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_agent_price']=$agent_room_price;
   //     $agent_total_price+=$agent_room_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_customer_price']=$customer_room_price;
   //     $customer_total_price+=$customer_room_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_currency']=$room_currency;
   //     $hotel_room_detail[$room_detail_count]['room_currency_conversion_rate']=$room_currency_conversion_rate;
   //     $hotel_room_detail[$room_detail_count]['room_currency_conversion_currency']="GEL";
   //     $hotel_room_detail[$room_detail_count]['room_cost']=$customer_room_price;

      

   //     if($room_extra_bed=="Yes")
   //     {
   //       $hotel_total_cost+=$room_extra_bed_customer_price*$room_qty;

   //      $hotel_room_detail[$room_detail_count]['room_extra_bed_supplier_price']=$room_extra_bed_supplier_price;
   //     $supplier_total_price+=$room_extra_bed_supplier_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_extra_bed_agent_price']=$room_extra_bed_agent_price;
   //     $agent_total_price+=$room_extra_bed_agent_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_extra_bed_customer_price']=$room_extra_bed_customer_price;
   //     $customer_total_price+=$room_extra_bed_customer_price*$room_qty;

   //      $hotel_room_detail[$room_detail_count]['room_cost']=($customer_room_price+$room_extra_bed_customer_price);

   //     }
   //      $room_detail_count++;
   //    }
   

   // }

    //  if(!empty($room_count))
   //  {
   //    $room_total_count=count($room_count);   
   //  }
   //  else
   //  {
   //    $room_total_count=0;
   //  }
   //  $hotel_room_detail=array();
   //  $supplier_total_price=0;
   //   $agent_total_price=0;
   //    $customer_total_price=0;
   //   $room_all_names=array();

   //   $hotel_total_cost=0;

   //  $room_detail_count=0;

   //  for($i=0;$i<$room_total_count;$i++)
   //  {
      
   //    $supplier_price=$request->get('room_price_'.$i);
   //  $room_name=$request->get('room_name_'.$i);
   //  $room_qty=$request->get('room_select_'.$i);
   //   $agent_room_price=$request->get('room_agent_price_markup_'.$i);
   //  $customer_room_price=$request->get('room_price_markup_'.$i);
   //  $room_currency=$request->get('room_price_currency_'.$i);
   //  $room_currency_conversion_rate=$request->get('room_price_convert_rate_'.$i);

   //   $room_extra_bed=$request->get('room_extra_bed_'.$i);
   //  $room_extra_bed_supplier_price=$request->get('room_extra_bed_price_'.$i);
   //  $room_extra_bed_agent_price=$request->get('room_agent_extra_bed_price_'.$i);
   //  $room_extra_bed_customer_price=$request->get('room_extra_bed_price_markup_'.$i);
   //    if($room_qty>0)
   //    {
   //        $booking_room_quantity+=$room_qty;
   //      $hotel_total_cost+=$customer_room_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_name']=$room_name;
   //     $room_all_names[]=$room_name;

   //     $hotel_room_detail[$room_detail_count]['room_qty']=$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_supplier_price']=$supplier_price;
   //     $supplier_total_price+=$supplier_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_agent_price']=$agent_room_price;
   //     $agent_total_price+=$agent_room_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_customer_price']=$customer_room_price;
   //     $customer_total_price+=$customer_room_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_currency']=$room_currency;
   //     $hotel_room_detail[$room_detail_count]['room_currency_conversion_rate']=$room_currency_conversion_rate;
   //     $hotel_room_detail[$room_detail_count]['room_currency_conversion_currency']="GEL";
   //     $hotel_room_detail[$room_detail_count]['room_cost']=$customer_room_price;

      

   //     if($room_extra_bed=="Yes")
   //     {
   //       $hotel_total_cost+=$room_extra_bed_customer_price*$room_qty;

   //      $hotel_room_detail[$room_detail_count]['room_extra_bed_supplier_price']=$room_extra_bed_supplier_price;
   //     $supplier_total_price+=$room_extra_bed_supplier_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_extra_bed_agent_price']=$room_extra_bed_agent_price;
   //     $agent_total_price+=$room_extra_bed_agent_price*$room_qty;

   //     $hotel_room_detail[$room_detail_count]['room_extra_bed_customer_price']=$room_extra_bed_customer_price;
   //     $customer_total_price+=$room_extra_bed_customer_price*$room_qty;

   //      $hotel_room_detail[$room_detail_count]['room_cost']=($customer_room_price+$room_extra_bed_customer_price);

   //     }
   //      $room_detail_count++;
   //    }
   

   // }
$room_id=$request->get('room_id');
     if(!empty($room_id))
    {
      $room_total_count=count($room_id);   
    }
    else
    {
      $room_total_count=0;
    }
    $hotel_room_detail=array();
    $supplier_total_price=0;
     $agent_total_price=0;
      $customer_total_price=0;
     $room_all_names=array();

     $hotel_total_cost=0;

    $room_detail_count=0;
    $booking_room_quantity_details=array();

    for($i=0;$i<$room_total_count;$i++)
    {
      $room_occupancy_id=$request->get('room_occupancy_id')[$i];
      for($occupancy=0;$occupancy<=count($room_occupancy_id);$occupancy++)
      {
        if(!isset($request->get("room_occupancy_id")[$i][$occupancy]))
        {
          continue;
        }

      $supplier_price=$request->get("room_price")[$i][$occupancy];
    $room_name=$request->get("room_name")[$i];
      $room_id=$request->get("room_id")[$i];
      $room_occupancy_id=$request->get("room_occupancy_id")[$i][$occupancy];
        $room_occupancy_qty=$request->get("room_occupancy_qty")[$i][$occupancy];
    $room_qty=$request->get("room_select")[$i][$occupancy];
     $agent_room_price=$request->get("room_agent_price_markup")[$i][$occupancy];
    $customer_room_price=$request->get("room_price_markup")[$i][$occupancy];
    $room_currency=$request->get("room_price_currency")[$i][$occupancy];
    $room_currency_conversion_rate=$request->get('room_price_convert_rate')[$i];

     $room_extra_bed="";
    if(!empty($request->get("room_extra_bed")[$i]))
    {
     $room_extra_bed=$request->get("room_extra_bed")[$i][$occupancy];
     $room_extra_bed_supplier_price=$request->get("room_extra_bed_price")[$i][$occupancy];
    $room_extra_bed_agent_price=$request->get("room_agent_extra_bed_price")[$i][$occupancy];
    $room_extra_bed_customer_price=$request->get("room_extra_bed_price_markup")[$i][$occupancy];
    }
    
      if($room_qty>0)
      {
          $booking_room_quantity+=$room_qty;

          $booking_room_quantity_details[]=$room_id."---".$room_occupancy_id."---".$room_occupancy_qty."---".$room_qty;
        $hotel_total_cost+=$customer_room_price*$room_qty;

        $hotel_room_detail[$room_detail_count]['room_id']=$room_id;
       $hotel_room_detail[$room_detail_count]['room_name']=$room_name;
       $room_all_names[]=$room_name;


        $hotel_room_detail[$room_detail_count]['room_occupancy_id']=$room_occupancy_id;
         $hotel_room_detail[$room_detail_count]['room_occupancy_qty']=$room_occupancy_qty;

       $hotel_room_detail[$room_detail_count]['room_qty']=$room_qty;

       $hotel_room_detail[$room_detail_count]['room_supplier_price']=$supplier_price;
       $supplier_total_price+=$supplier_price*$room_qty;

       $hotel_room_detail[$room_detail_count]['room_agent_price']=$agent_room_price;
       $agent_total_price+=$agent_room_price*$room_qty;

       $hotel_room_detail[$room_detail_count]['room_customer_price']=$customer_room_price;
       $customer_total_price+=$customer_room_price*$room_qty;

       $hotel_room_detail[$room_detail_count]['room_currency']=$room_currency;
       $hotel_room_detail[$room_detail_count]['room_currency_conversion_rate']=$room_currency_conversion_rate;
       $hotel_room_detail[$room_detail_count]['room_currency_conversion_currency']="GEL";
       $hotel_room_detail[$room_detail_count]['room_cost']=$customer_room_price;

      

       if($room_extra_bed=="Yes")
       {
         $hotel_total_cost+=$room_extra_bed_customer_price*$room_qty;

        $hotel_room_detail[$room_detail_count]['room_extra_bed_supplier_price']=$room_extra_bed_supplier_price;
       $supplier_total_price+=$room_extra_bed_supplier_price*$room_qty;

       $hotel_room_detail[$room_detail_count]['room_extra_bed_agent_price']=$room_extra_bed_agent_price;
       $agent_total_price+=$room_extra_bed_agent_price*$room_qty;

       $hotel_room_detail[$room_detail_count]['room_extra_bed_customer_price']=$room_extra_bed_customer_price;
       $customer_total_price+=$room_extra_bed_customer_price*$room_qty;

        $hotel_room_detail[$room_detail_count]['room_cost']=($customer_room_price+$room_extra_bed_customer_price);

       }
       
      }
       $room_detail_count++;

    }
   

   }





    $supplier_price=$supplier_total_price;
    $room_name=implode(",",$room_all_names);

     $booking_room_quantity_details_text=implode(",",$booking_room_quantity_details);
    $agent_room_price=$agent_total_price;
    $customer_room_price=$customer_total_price;
    $booking_markup_per=$request->get('markup');
    $booking_own_markup_per=$request->get('own_markup');
    $room_currency="GEL";
    
    $booking_markup_per=$request->get('markup');
    $booking_amount=$hotel_total_cost;
    $booking_array=array("agent_id"=>$agent_id,
      "hotel_id"=>$hotel_id,
      "booking_supplier_id"=>$supplier_id,
      "booking_amount"=>$booking_amount,
      "checkin_date"=>$checkin_date,
      "checkout_date"=>$checkout_date,
      "no_of_days"=>$no_of_days,
      "booking_adult_count"=>$hotel_adults,
      "booking_child_count"=>$hotel_children,
      "booking_child_age"=>$hotel_child_age,
      "booking_room_quantity"=>$booking_room_quantity,
      "booking_room_quantity_text"=> $booking_room_quantity_details_text,
      "supplier_price"=>$supplier_price,
      "room_name"=>$room_name,
      "agent_room_price"=>$agent_room_price,
      "customer_room_price"=>$customer_room_price,
      "room_currency"=>$room_currency,
      "booking_markup_per"=>$booking_markup_per,
      "booking_own_markup_per"=>$booking_own_markup_per,
      "hotel_room_detail"=>$hotel_room_detail);
    $get_hotel=Hotels::where('hotel_id',$hotel_id)->where('hotel_status',1)->first();
    $countries=Countries::get();
    return view('agent.booking-page')->with(compact('booking_array','get_hotel','countries'))->with('booking_type','hotel');
  }
  public function confirm_hotel_booking(Request $request)
  {
            // echo "<pre>";
            //     print_r($request->all());
            //     die();
    $agent_id=session()->get('travel_agent_id');
    $fetch_admin=Users::where('users_pid',0)->first();
    $fetch_agent=Agents::where('agent_id',$agent_id)->first();
    $customer_name=$request->get('customer_name');
    $customer_email=$request->get('customer_email');  
    $customer_phone=$request->get('customer_phone');  
    $customer_country=$request->get('customer_country');
    $customer_address=$request->get('customer_address'); 
    $customer_remarks=$request->get('customer_remarks'); 
    $booking_whole_data=$request->get('booking_details');  
    $booking_details=unserialize($request->get('booking_details'));
    $hotel_id=$booking_details['hotel_id'];
    $booking_supplier_id=$booking_details['booking_supplier_id'];
    $fetch_supplier=Suppliers::where('supplier_id',$booking_supplier_id)->first();
    $booking_amount=$booking_details['booking_amount'];
    $booking_date_checkin=$booking_details['checkin_date'];
    $booking_date_checkout=$booking_details['checkout_date'];
    $check_in_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking_details['checkin_date'].' 0:00:00');
    $check_out_day = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $booking_details['checkout_date'].' 0:00:00');
    $NoOfdays = $check_in_day->diffInDays($check_out_day);
    $booking_room_count=$booking_details['booking_room_quantity'];
    $booking_room_count_text=$booking_details['booking_room_quantity_text'];
    $booking_adult_count=$booking_details['booking_adult_count'];
    $booking_child_count=$booking_details['booking_child_count'];
    $booking_child_age=$booking_details['booking_child_age'];
    $booking_room_name=$booking_details['room_name'];
    $supplier_room_price=$booking_details['supplier_price'];
    $agent_room_price=$booking_details['agent_room_price'];
    $customer_room_price=$booking_details['customer_room_price'];
    $booking_markup_per=$booking_details['booking_markup_per'];
     $booking_own_markup_per=$booking_details['booking_own_markup_per'];
    $hotel_room_detail=$booking_details['hotel_room_detail'];

    $booking_attachments=array();
//multifile uploading
  if($request->hasFile('customer_file'))
  {
    foreach($request->file('customer_file') as $file)
    {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
      {
        $image_name=$file->getClientOriginalName();
        $image_activity = "booking-attachment-".time()."-".$image_name;
// request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
        $dir1 = 'assets/uploads/booking_attachments/';
        $file->move($dir1, $image_activity);
        $booking_attachments[]=$image_activity;
      }
    }
  }
  $booking_attachments=serialize($booking_attachments);

    $get_hotels=Hotels::where('hotel_id',$hotel_id)->where('hotel_status',1)->first();
    if($get_hotels)
    {

        $room_currency_array=array("USD","EUR");
    $currency_price_array=array();
      foreach($room_currency_array as $currency)
      {
           $new_currency=$currency;

        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
        //   $cURLConnection = curl_init();

        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        //   $convertedPrice = curl_exec($cURLConnection);
        //   curl_close($cURLConnection);

        //   $convertedPriceArray=json_decode($convertedPrice);
        //   $currency_attribute=$new_currency."_".$this->base_currency;

        //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
          
        //  $currency_price_array[$currency]=round($conversion_price,2);
          $conversion_price=0;
          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/'.$this->base_currency.'/1');
                $scriptDocument = new \DOMDocument();
                libxml_use_internal_errors(TRUE); //disable libxml errors
                if(!empty($fetch_html)){
                   //load
                   $scriptDocument->loadHTML($fetch_html);
                 
                //init DOMXPath
                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                  //get all the h2's with an id
                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                  //check
                  if($scriptRow->length > 0){
                    foreach($scriptRow as $row){
                           $conversion_price=round($row->nodeValue,2);
                    }
                  }
                 
                }
                 $currency_price_array[$currency]=round($conversion_price,2);
         
      }

      $currency_price_array["GEL"]=1;

      $booking_currency=$booking_details['room_currency'];
      $date=date("Y-m-d");
      $time=date("H:i:s");
      $get_latest_id=Bookings::latest()->value('booking_sep_id');
      $get_latest_id=$get_latest_id+1;
      $insert_booking=new Bookings;
      $insert_booking->booking_sep_id=$get_latest_id;
      $insert_booking->booking_type="hotel";
      $insert_booking->booking_type_id=$hotel_id;
      $insert_booking->booking_agent_id=$agent_id;
      $insert_booking->booking_role="AGENT";
      $insert_booking->booking_supplier_id=$booking_supplier_id;
      $insert_booking->customer_name=$customer_name;
      $insert_booking->customer_contact=$customer_phone;
      $insert_booking->customer_email=$customer_email;
      $insert_booking->customer_country=$customer_country;
      $insert_booking->customer_address=$customer_address;
      $insert_booking->booking_adult_count=$booking_adult_count;
      $insert_booking->booking_child_count=$booking_child_count;
      $insert_booking->booking_child_age=$booking_child_age;
      $insert_booking->booking_rooms_count=$booking_room_count;
        $insert_booking->booking_rooms_count_details=$booking_room_count_text;
      $insert_booking->booking_subject_name=$booking_room_name;
      $insert_booking->booking_subject_days=$NoOfdays;
      $insert_booking->supplier_adult_price=$supplier_room_price;
      $insert_booking->agent_adult_price=$agent_room_price;
      $insert_booking->customer_adult_price=$customer_room_price;
      $insert_booking->booking_currency=$booking_currency;
      $insert_booking->booking_currency_conversions=serialize($currency_price_array);
      $insert_booking->booking_supplier_amount=$supplier_room_price;
      $insert_booking->booking_agent_amount=$agent_room_price;
      $insert_booking->booking_amount=$booking_amount;
      $insert_booking->booking_markup_per=$booking_markup_per;
       $insert_booking->booking_own_markup_per=$booking_own_markup_per;
      $insert_booking->booking_remarks=$customer_remarks;
      $insert_booking->booking_selected_date=$booking_date_checkin;
      $insert_booking->booking_selected_to_date=$booking_date_checkout;
     $insert_booking->booking_other_info=serialize($hotel_room_detail);
      $insert_booking->booking_whole_data=$booking_whole_data;
       $insert_booking->booking_attachments=$booking_attachments;
      $insert_booking->booking_date=$date;
      $insert_booking->booking_time=$time;
      if($insert_booking->save())
      {
        $booking_id=$get_latest_id;
        $htmldata='<p>Dear '.$fetch_agent->agent_name.',</p><p>You have successfully booked hotel for your customer on traveldoor[dot]ge portal . Your booking id is '.$booking_id.'.</p>
        ';
        $data = array(
          'name' => $fetch_agent->agent_name,
          'email' =>$fetch_agent->company_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data['email'], $data['name'])->subject('BOOKING SUCCESSFUL');
        });
        $htmldata='<p>Dear '.$fetch_supplier->supplier_name.',</p><p>You have got new hotel booking on traveldoor[dot]ge portal with booking id '.$booking_id.'. Please login into your supplier portal to get more information.</p>';
        $data_supplier= array(
          'name' => $fetch_supplier->supplier_name,
          'email' =>$fetch_supplier->company_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_supplier) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data_supplier['email'], $data_supplier['name'])->subject('NEW HOTEL BOOKING');
        });
        $htmldata='<p>Dear Admin,</p><p>New hotel has been booked with booking id '.$booking_id.' by agent '.$fetch_agent->agent_name.' for the supplier '.$fetch_supplier->supplier_name.' </p>';
        $data_admin= array(
          'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
          'email' =>$fetch_admin->users_email
        );
        Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin) {
          $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
          $m->to($data_admin['email'], $data_admin['name'])->subject('NEW HOTEL BOOKING ON PORTAL');
        });
        $message="You have successfully booked hotel for your customer ".$customer_name;
        session()->put('booking_message',$message);
        return redirect()->route('booking-success');
      }
      else
      {
        $message="You selected hotel cannot be booked right now";
        session()->put('booking_message_fail',$message);
        return redirect()->route('booking-fail');
      }
    }
    else
    {
      return redirect()->back();
    }
  }

   public function hotel_list(Request $request)
  {
    if(session()->has('travel_agent_id'))
    {
      $countries=Countries::get();
      $cities=Cities::get();
      $agent_id=session()->get('travel_agent_id');
      $get_itinerary=SavedItinerary::where('itinerary_status',1)->paginate(9);
      $get_activities=Activities::where('activity_status',1)->paginate(9);
      $get_transport=Transport::where('transfer_status',1)->paginate(9);
      $get_hotels=Hotels::where('hotel_status',1)->get();
      $hotel_meal=HotelMeal::get();
      return view('agent.hotel-list')->with(compact('countries','cities','agent_id','get_itinerary','get_activities','get_transport','get_hotels','hotel_meal'));
    }
    else
    {
      return redirect()->intended('agent');
    }
  }


}