<?php

namespace App\Http\Controllers;

use App\Users;

use App\Menus;

use App\VehicleType;

use App\Vehicles;

use App\EnquiryType;

use App\TourType;

use App\HotelType;

use App\ActivityType;

use App\AirportMaster;

use App\HotelMeal;

use App\UserRights;

use App\Amenities;

use App\SubAmenities;

use App\Languages;

use App\VehicleWiseCost;

use App\FuelType;

use App\VehicleWiseCost_log;

use App\FuelType_log;

use App\GuideExpense;

use App\GuideExpense_log;

use App\CustomerMarkup;

use App\CustomerMarkup_log;

use App\Countries;
use App\States;
use App\Cities;

use App\SettingTargetCommission;
use App\Mail\SendMailable;
use Session;
use Mail;
use Illuminate\Http\Request;



class LoginController extends Controller

{
     public function __construct()
    {
        date_default_timezone_set('Asia/Dubai');
    }
    public function send_mail(Request $request){
         $htmldata='<p>Dear ,</p><p>hello sir</p>';
            $data = array(
              'name' => 'hello',
              'email' =>'rohanmahajan7209@gmail.com'
            );
            $check=Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data) {
              $m->from('rohanphp@netwebtechnologies.com', 'Traveldoor');
              $m->to($data['email'], $data['name'])->subject('Test mail');
            });
            if($check)
            {
                echo "success";
            }
            else
            {
                echo "fail";
            }
    }

     private function rights($menu)
    {
        $emp_id=session()->get('travel_users_id');
        $right_array=array();
        $employees=Users::where('users_id',$emp_id)->where('users_pid',0)->where('users_status',1)->first();
        if(!empty($employees))
        {
            $right_array['add']=1;
            $right_array['view']=1;
            $right_array['edit_delete']=1;
            $right_array['report']=1;
            $right_array['admin']=1;
            $right_array['admin_which']="add,view,edit_delete,report";
        }
        else
        {

            $employees=Users::where('users_id',$emp_id)->where('users_status',1)->first();
            if(!empty($employees))
            {
                $user_rights=UserRights::where('emp_id',$emp_id)->where('menu',$menu)->first();
                if(!empty($user_rights))
                {
                    $right_array['add']=$user_rights->add_status;
                    $right_array['view']=$user_rights->view_status;
                    $right_array['edit_delete']=$user_rights->edit_del_status;
                    $right_array['report']=$user_rights->report_status;
                    $right_array['admin']=$user_rights->admin_status;
                    if($user_rights->admin_which_status!="")
                        $right_array['admin_which']=$user_rights->admin_which_status;
                    else
                        $right_array['admin_which']="No";
                }
                else
                {
                    $right_array['add']=0;
                    $right_array['view']=0;
                    $right_array['edit_delete']=0;
                    $right_array['report']=0;
                    $right_array['admin']=0;
                    $right_array['admin_which']="No";
                }
            }
            else
            {
                $right_array['add']=0;
                $right_array['view']=0;
                $right_array['edit_delete']=0;
                $right_array['report']=0;
                $right_array['admin']=0;
                $right_array['admin_which']="No";
            }

        }

        return $right_array;

    }

    public function index()

    {

          if(session()->has('travel_users_id'))

          {



           return view('mains.home');

        }

        else

        {

         return view('mains.index');

        }

    }



    public function home()

    {

           if(session()->has('travel_users_id'))

          {



           return view('mains.home');

        }

        else

        {

         return redirect()->route('index');

        }

    }



    public function login_check(Request $request)

    {

    	$username=$request->get('username');

    	$password=$request->get('password');



    	$check_users=Users::where('users_username',$username)->where('users_password',md5($password))->first();

    	if(!empty($check_users))

    	{

    		if($check_users->users_role=="Admin")

    		{

    			session()->put('travel_users_id',$check_users->users_id);

    			session()->put('travel_username',$check_users->users_username);

    			session()->put('travel_email',$check_users->users_email);

    			session()->put('travel_fullame',$check_users->users_fname." ".$check_users->users_lname);

    			session()->put('travel_users_role',"Admin");

    			echo "success";

    		}

    		else

    		{

                $timestamp=date('Y-m-d H:i:s');

                $update_user=Users::where('users_username',$username)->update(["last_login"=> $timestamp]);

    			session()->put('travel_users_id',$check_users->users_id);

    			session()->put('travel_username',$check_users->users_username);

    			session()->put('travel_email',$check_users->users_email);

    			session()->put('travel_fullame',$check_users->users_fname." ".$check_users->users_lname);

    			session()->put('travel_users_role',$check_users->users_assigned_role);

    			echo "success";



    		}

    		

    	}

    	else

    	{

    		echo "fail";

    	}



    }

    public function logout()

    {

        Session::flush();

        return redirect()->intended('/');

    }



    public function menu(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('menu');
        $fetch_menu=Menus::where('menu_pid',0)->get();
        return view('mains.menu')->with(compact('fetch_menu','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function menu_insert(Request $request)

{

    $menu_pid=$request->get('menu_pid');

    $menu_name=$request->get('menu_name');

    $file_name=$request->get('file_name');

    if($request->get('new_window'))

        $new_window=$request->get('new_window');

    else

        $new_window=0;



    $check_menu=Menus::where('menu_name',$menu_name)->first();

    if(!empty($check_menu))

    {

        echo "exist";

    }

    else

    {

        $order=1;

        $check_menu_order=Menus::where("menu_pid",$menu_pid)->orderBy('menu_id','desc')->first();



        if(!empty($check_menu_order))

        {

            $order=$check_menu_order->order_id;

            $order++;

        }



        $emp_id=session()->has('travel_users_id');

        $menu_insert=new Menus;

        $menu_insert->menu_pid=$menu_pid;

        $menu_insert->order_id=$order;

        $menu_insert->menu_name=$menu_name;

        $menu_insert->menu_file=$file_name;

        $menu_insert->newwindow=$new_window;

        $menu_insert->emp_id=$emp_id;

        if($menu_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }





    }





}



public static function menu_fetch()

{

    // $menus=Menus::get();

    // return $menus;



    $fetch_menu=Menus::where('menu_pid',0)->orderBy('order_id','asc')->get();

    $fetch_sub_menu=Menus::where('menu_pid','!=',0)->orderBy('order_id','asc')->get();

    $emp_id=session()->get('travel_users_id');

    $usertype="";

    $rights=array();

    $admin_fetch=Users::where('users_id',$emp_id)->where('users_pid',0)->first();

    if(!empty($admin_fetch))

    {

        $usertype="admin";

    }

    else

    {

        $employee_fetch=Users::where('users_id',$emp_id)->first();

        if(!empty($employee_fetch))

        {

            $usertype="sub-user";

            $check_existing=UserRights::where('emp_id',$emp_id)->get();

            if(count($check_existing)>0)

            {

                $new_array=array();

                $count=0;

                foreach($check_existing as $array_values)

                {

                    $new_array[$count]['menu']=$array_values->menu;

                    $new_array[$count]['add_status']=$array_values->add_status;

                    $new_array[$count]['add_status']=$array_values->add_status;

                    $new_array[$count]['view_status']=$array_values->view_status;

                    $new_array[$count]['edit_del_status']=$array_values->edit_del_status;

                    $new_array[$count]['report_status']=$array_values->report_status;

                    $new_array[$count]['admin_status']=$array_values->admin_status;

                    $new_array[$count]['admin_which_status']=$array_values->admin_which_status;



                    $count++;

                }



                $rights=$new_array;



            }

            else

            {

                $rights[]="no_rights_available";

            }

        }

    }





    return array("fetch_menu"=>$fetch_menu,"sub_menu"=>$fetch_sub_menu,"rights"=>$rights,"type"=>$usertype);

}



    public function languages(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('languages');
        $fetch_languages=Languages::get();
        return view('mains.languages')->with(compact('fetch_languages','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function languages_insert(Request $request)

{

    $language_name=$request->get('language_name');

    $iso_639_no=$request->get('iso_639_no');

    $check_langauge=Languages::where('language_name',$language_name)->first();

    if(!empty($check_langauge))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $languages_insert=new Languages;

        $languages_insert->language_name=$language_name;

        $languages_insert->iso_639_no=$iso_639_no;

        $languages_insert->language_created_by=$emp_id;

        if($languages_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }





    }





}

 public function vehicle_type(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('vehicle-type');
        $fetch_vehicle_type=VehicleType::get();
        return view('mains.vehicle-type')->with(compact('fetch_vehicle_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function vehicle_type_insert(Request $request)

{

    $vehicle_type_name=$request->get('vehicle_type_name');
    $vehicle_type_min=$request->get('vehicle_type_min');
    $vehicle_type_max=$request->get('vehicle_type_max');

    $check_vehicle_type=VehicleType::where('vehicle_type_name',$vehicle_type_name)->first();

    if(!empty($check_vehicle_type))

    {

        echo "exist";

    }

    else
    {
       $vehicle_type_image=$request->get('vehicle_type_image');
       if($request->hasFile('vehicle_type_image'))
       {
              $vehicle_type_image=$request->file('vehicle_type_image');
              $extension=strtolower($request->vehicle_type_image->getClientOriginalExtension());
              if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
              {
                 $image_name=$request->file('vehicle_type_image')->getClientOriginalName();
                 $vehicle_image = "vehicle-type-".time().uniqid();
                 $dir1 = 'assets/uploads/vehicle_type_images/';
                 $request->file('vehicle_type_image')->move($dir1, $vehicle_image);
             }
             else
             {
              $vehicle_image = "";
            }
        }
        else
        {
            $vehicle_image = "";
        }

        $emp_id=session()->has('travel_users_id');

        $vehicle_type_insert=new VehicleType;

        $vehicle_type_insert->vehicle_type_name=$vehicle_type_name;
         $vehicle_type_insert->vehicle_type_min=$vehicle_type_min;
          $vehicle_type_insert->vehicle_type_max=$vehicle_type_max;
           $vehicle_type_insert->vehicle_type_image=$vehicle_image;

        $vehicle_type_insert->vehicle_type_created_by=$emp_id;

        if($vehicle_type_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function vehicle_type_update(Request $request)

{

    $vehicle_type_id=$request->get('vehicle_type_id');

   $vehicle_type_name=$request->get('vehicle_type_name');

  $vehicle_type_min=$request->get('vehicle_type_min');
    $vehicle_type_max=$request->get('vehicle_type_max');

     $vehicle_type_image1=$request->get('vehicle_type_image1');

    $check_vehicle_type=VehicleType::where('vehicle_type_name',$vehicle_type_name)->where('vehicle_type_id','!=',$vehicle_type_id)->first();

    if(!empty($check_vehicle_type))

    {

        echo "exist";

    }

    else
    {

            $edit_vehicle_type_image=$request->get('edit_vehicle_type_image');
          if($request->hasFile('edit_vehicle_type_image'))
          {
            $edit_vehicle_type_image=$request->file('edit_vehicle_type_image');
            $extension=strtolower($request->edit_vehicle_type_image->getClientOriginalExtension());
            if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
            {
             $image_name=$edit_vehicle_type_image->getClientOriginalName();
             $vehicle_image = "vehicle-type-".time().uniqid();
                            // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
             $dir1 = 'assets/uploads/vehicle_type_images/';
             $request->file('edit_vehicle_type_image')->move($dir1, $vehicle_image);
           }
           else
           {
            $vehicle_image = "";
          }
        }
        else
        {
          $vehicle_image = $vehicle_type_image1;
        }

        $emp_id=session()->has('travel_users_id');
        $update_vehicle_type_array=array("vehicle_type_name"=>$vehicle_type_name,
                                              "vehicle_type_min"=>$vehicle_type_min,
                                              "vehicle_type_max"=>$vehicle_type_max,
                                                "vehicle_type_image"=>$vehicle_image);
        $update_vehicle_type=VehicleType::where('vehicle_type_id',$vehicle_type_id)->update($update_vehicle_type_array);

        if($update_vehicle_type)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function hotel_meal(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('hotel-meal');
        $fetch_hotel_meal=HotelMeal::get();
        return view('mains.hotel-meal')->with(compact('fetch_hotel_meal','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function hotel_meal_insert(Request $request)

{

    $hotel_meal_name=$request->get('hotel_meal_name');

    $check_hotel_meal=HotelMeal::where('hotel_meals_name',$hotel_meal_name)->first();

    if(!empty($check_hotel_meal))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $hotel_meal_insert=new HotelMeal;

        $hotel_meal_insert->hotel_meals_name=$hotel_meal_name;

        $hotel_meal_insert->hotel_meals_created_by=$emp_id;

        if($hotel_meal_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }





    }


}

public function hotel_meal_update(Request $request)

{

    $hotel_meal_id=$request->get('hotel_meal_id');

   $hotel_meal_name=$request->get('hotel_meal_name');

    $check_hotel_meal=HotelMeal::where('hotel_meals_name',$hotel_meal_name)->where('hotel_meals_id','!=',$hotel_meal_id)->first();

    if(!empty($check_hotel_meal))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_hotel_meal_array=array("hotel_meals_name"=>$hotel_meal_name);
        $update_hotel_meal=HotelMeal::where('hotel_meals_id',$hotel_meal_id)->update($update_hotel_meal_array);

        if($update_hotel_meal)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}


public function fuel_type(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('fuel-type-cost');
        $fetch_fuel_type=FuelType::get();
        return view('mains.fuel-type-cost')->with(compact('fetch_fuel_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}

public function fuel_type_insert(Request $request)

{

    $fuel_type_name=$request->get('fuel_type_name');

    $fuel_type_cost=$request->get('fuel_type_cost');

    $check_fuel_type=FuelType::where('fuel_type',$fuel_type_name)->first();

    if(!empty($check_fuel_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $fuel_type_insert=new FuelType;

        $fuel_type_insert->fuel_type=$fuel_type_name;

         $fuel_type_insert->fuel_type_cost=$fuel_type_cost;

        $fuel_type_insert->fuel_type_created_by=$emp_id;

        if($fuel_type_insert->save())

        {
            $last_id=$fuel_type_insert->id;

           $fuel_type_log_insert=new FuelType_log;

           $fuel_type_log_insert->fuel_type_id=$last_id;

           $fuel_type_log_insert->fuel_type=$fuel_type_name;

           $fuel_type_log_insert->fuel_type_cost=$fuel_type_cost;

           $fuel_type_log_insert->fuel_type_created_by=$emp_id;

           $fuel_type_log_insert->fuel_type_operation="INSERT";

           $fuel_type_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function fuel_type_update(Request $request)

{

    $fuel_type_id=$request->get('fuel_type_id');

    $fuel_type_name=$request->get('fuel_type_name');

    $fuel_type_cost=$request->get('fuel_type_cost');

    $check_fuel_type=FuelType::where('fuel_type',$fuel_type_name)->where('fuel_type_id','!=',$fuel_type_id)->first();

    if(!empty($check_fuel_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_fuel_type_array=array("fuel_type_cost"=>$fuel_type_cost);
        $update_fuel_type=FuelType::where('fuel_type_id',$fuel_type_id)->update($update_fuel_type_array);

        if($update_fuel_type)

        {
            $fuel_type_log_insert=new FuelType_log;

           $fuel_type_log_insert->fuel_type_id=$fuel_type_id;

           $fuel_type_log_insert->fuel_type=$fuel_type_name;

           $fuel_type_log_insert->fuel_type_cost=$fuel_type_cost;

           $fuel_type_log_insert->fuel_type_created_by=$emp_id;

           $fuel_type_log_insert->fuel_type_operation="UPDATE";

           $fuel_type_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function vehicle_type_cost(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('vehicle-type-cost');
       $fetch_vehicle_type=VehicleType::get();
        $fetch_vehicle_type_cost=VehicleWiseCost::join('vehicle_type','vehicle_type.vehicle_type_id',"=","vehicle_wise_cost.vehicle_type_id")->select("vehicle_wise_cost.*","vehicle_type.vehicle_type_name")->get();
        return view('mains.vehicle-type-cost')->with(compact('fetch_vehicle_type_cost','fetch_vehicle_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}

public function vehicle_type_cost_insert(Request $request)

{

    $vehicle_type_id=$request->get('vehicle_type_name');

    $vehicle_type_cost=$request->get('vehicle_type_cost');

    $check_vehicle_type_cost=VehicleWiseCost::where('vehicle_type_id',$vehicle_type_id)->first();

    if(!empty($check_vehicle_type_cost))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $vehicle_type_cost_insert=new VehicleWiseCost;

        $vehicle_type_cost_insert->vehicle_type_id=$vehicle_type_id;

         $vehicle_type_cost_insert->vehicle_type_cost=$vehicle_type_cost;

        $vehicle_type_cost_insert->vehicle_cost_created_by=$emp_id;

        if($vehicle_type_cost_insert->save())

        {
            $last_id=$vehicle_type_cost_insert->id;

           $vehicle_type_cost_log_insert=new VehicleWiseCost_log;

           $vehicle_type_cost_log_insert->vehicle_cost_id=$last_id;

           $vehicle_type_cost_log_insert->vehicle_type_id=$vehicle_type_id;

           $vehicle_type_cost_log_insert->vehicle_type_cost=$vehicle_type_cost;

           $vehicle_type_cost_log_insert->vehicle_cost_created_by=$emp_id;

           $vehicle_type_cost_log_insert->vehicle_type_operation="INSERT";

           $vehicle_type_cost_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function vehicle_type_cost_update(Request $request)

{

    $vehicle_type_cost_id=$request->get('vehicle_type_cost_id');

   $vehicle_type_id=$request->get('vehicle_type_name');

    $vehicle_type_cost=$request->get('vehicle_type_cost');

    $check_vehicle_type_cost=VehicleWiseCost::where('vehicle_type_id',$vehicle_type_id)->where('vehicle_cost_id','!=',$vehicle_type_cost_id)->first();

    if(!empty($check_vehicle_type_cost))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_vehicle_type_cost_array=array("vehicle_type_cost"=>$vehicle_type_cost);
        $update_vehicle_type_cost=VehicleWiseCost::where('vehicle_cost_id',$vehicle_type_cost_id)->update($update_vehicle_type_cost_array);

        if($update_vehicle_type_cost)

        {
            $vehicle_type_cost_log_insert=new VehicleWiseCost_log;

           $vehicle_type_cost_log_insert->vehicle_cost_id=$vehicle_type_cost_id;

           $vehicle_type_cost_log_insert->vehicle_type_id=$vehicle_type_id;

           $vehicle_type_cost_log_insert->vehicle_type_cost=$vehicle_type_cost;

           $vehicle_type_cost_log_insert->vehicle_cost_created_by=$emp_id;

           $vehicle_type_cost_log_insert->vehicle_type_operation="UPDATE";

           $vehicle_type_cost_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function amenities(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('amenities');
        $fetch_amenities=Amenities::get();
        return view('mains.amenities')->with(compact('fetch_amenities','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function amenities_insert(Request $request)

{

    $amenities_name=$request->get('amenities_name');

    $check_amenities=Amenities::where('amenities_name',$amenities_name)->first();

    if(!empty($check_amenities))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $amenities_insert=new Amenities;

        $amenities_insert->amenities_name=$amenities_name;

        $amenities_insert->amenities_created_by=$emp_id;

        if($amenities_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }





    }





}

public function update_amenities_active(Request $request)
{
     $id=$request->get('amenities_id');

  $action_perform=$request->get('action_perform');



 if($action_perform=="active")

  {

    $update_activity=Amenities::where('amenities_id',$id)->update(["amenities_status"=>1]);

    if($update_activity)

    {

      echo "success";

    }

    else

    {

      echo "fail";

    }

  }

  else if($action_perform=="inactive")

  {

  $update_activity=Amenities::where('amenities_id',$id)->update(["amenities_status"=>0]);

   if($update_activity)

   {

    echo "success";

  }

  else

  {

    echo "fail";

  }

}

else

{

  echo "fail";

}

}


public function update_roomamenities_active(Request $request)
{
     $id=$request->get('amenities_id');

  $action_perform=$request->get('action_perform');



 if($action_perform=="active")

  {

    $update_activity=Amenities::where('amenities_id',$id)->update(["amenities_room_status"=>1]);

    if($update_activity)

    {

      echo "success";

    }

    else

    {

      echo "fail";

    }

  }

  else if($action_perform=="inactive")

  {

  $update_activity=Amenities::where('amenities_id',$id)->update(["amenities_room_status"=>0]);

   if($update_activity)

   {

    echo "success";

  }

  else

  {

    echo "fail";

  }

}

else

{

  echo "fail";

}

}


public static function fetchAmenitiesName($amenities_id)
{

$id=$amenities_id;

 $fetch_amenities=Amenities::where('amenities_id',$id)->first();

 return $fetch_amenities;
}

public static function fetchSubAmenitiesName($sub_amenities_id,$amenities_id)
{

$sub_amenities_id=$sub_amenities_id;
$amenities_id=$amenities_id;

 $fetch_sub_amenities=SubAmenities::where('sub_amenities_id',$sub_amenities_id)->where('amenities_id',$amenities_id)->first();

 return $fetch_sub_amenities;
}




public function sub_amenities(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('sub_amenities');
         $fetch_amenities=Amenities::where('amenities_status',1)->get();
        $fetch_sub_amenities=SubAmenities::get();
        return view('mains.sub-amenities')->with(compact('fetch_sub_amenities','fetch_amenities','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function sub_amenities_insert(Request $request)

{
    $amenities_id=$request->get('amenities_id');

    $sub_amenities_name=$request->get('sub_amenities_name');

    $check_sub_amenities=SubAmenities::where('sub_amenities_name',$sub_amenities_name)->first();

    if(!empty($check_sub_amenities))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $sub_amenities_insert=new SubAmenities;

         $sub_amenities_insert->amenities_id=$amenities_id;

        $sub_amenities_insert->sub_amenities_name=$sub_amenities_name;

        $sub_amenities_insert->sub_amenities_created_by=$emp_id;

        if($sub_amenities_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }





    }





}

public function update_sub_amenities_active(Request $request)
{
     $id=$request->get('sub_amenities_id');

  $action_perform=$request->get('action_perform');



 if($action_perform=="active")

  {

    $update_activity=SubAmenities::where('sub_amenities_id',$id)->update(["sub_amenities_status"=>1]);

    if($update_activity)

    {

      echo "success";

    }

    else

    {

      echo "fail";

    }

  }

  else if($action_perform=="inactive")

  {

  $update_activity=SubAmenities::where('sub_amenities_id',$id)->update(["sub_amenities_status"=>0]);

   if($update_activity)

   {

    echo "success";

  }

  else

  {

    echo "fail";

  }

}

}

public function guide_expense(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('fuel-type-cost');
        $fetch_guide_expense=GuideExpense::get();
        return view('mains.guide-expense-cost')->with(compact('fetch_guide_expense','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}

public function guide_expense_insert(Request $request)

{

    $guide_expense_name=$request->get('guide_expense_name');

    $guide_expense_cost=$request->get('guide_expense_cost');

    $check_guide_expense=GuideExpense::where('guide_expense',$guide_expense_name)->first();

    if(!empty($check_guide_expense))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $guide_expense_insert=new GuideExpense;

        $guide_expense_insert->guide_expense=$guide_expense_name;

         $guide_expense_insert->guide_expense_cost=$guide_expense_cost;

        $guide_expense_insert->guide_expense_created_by=$emp_id;

        if($guide_expense_insert->save())

        {
            $last_id=$guide_expense_insert->id;

           $guide_expense_log_insert=new GuideExpense_log;

           $guide_expense_log_insert->guide_expense_id=$last_id;

           $guide_expense_log_insert->guide_expense=$guide_expense_name;

           $guide_expense_log_insert->guide_expense_cost=$guide_expense_cost;

           $guide_expense_log_insert->guide_expense_created_by=$emp_id;

           $guide_expense_log_insert->guide_expense_operation="INSERT";

           $guide_expense_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function guide_expense_update(Request $request)

{

    $guide_expense_id=$request->get('guide_expense_id');

    $guide_expense_name=$request->get('guide_expense_name');

    $guide_expense_cost=$request->get('guide_expense_cost');

    $check_guide_expense=GuideExpense::where('guide_expense',$guide_expense_name)->where('guide_expense_id','!=',$guide_expense_id)->first();

    if(!empty($check_guide_expense))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_guide_expense_array=array("guide_expense_cost"=>$guide_expense_cost);
        $update_guide_expense=GuideExpense::where('guide_expense_id',$guide_expense_id)->update($update_guide_expense_array);

        if($update_guide_expense)

        {
            $guide_expense_log_insert=new GuideExpense_log;

           $guide_expense_log_insert->guide_expense_id=$guide_expense_id;

           $guide_expense_log_insert->guide_expense=$guide_expense_name;

           $guide_expense_log_insert->guide_expense_cost=$guide_expense_cost;

           $guide_expense_log_insert->guide_expense_created_by=$emp_id;

           $guide_expense_log_insert->guide_expense_operation="UPDATE";

           $guide_expense_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}



 public function airport_master(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('airport-master');
        $fetch_airport_master=AirportMaster::get();
        return view('mains.airport-masters')->with(compact('fetch_airport_master','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function airport_master_insert(Request $request)

{

    $airport_master_name=$request->get('airport_master_name');

    $check_airport_master=AirportMaster::where('airport_master_name',$airport_master_name)->first();

    if(!empty($check_airport_master))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $airport_master_insert=new AirportMaster;

        $airport_master_insert->airport_master_name=$airport_master_name;

        $airport_master_insert->airport_master_created_by=$emp_id;

        if($airport_master_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function airport_master_update(Request $request)
{
     $airport_master_id=$request->get('airport_master_id');
    $airport_master_name=$request->get('airport_master_name');

    $check_airport_master=AirportMaster::where('airport_master_name',$airport_master_name)->where("airport_master_id","!=",$airport_master_id)->first();

    if(!empty($check_airport_master))

    {

        echo "exist";

    }

    else
    {

        $update_airport_master_array=array("airport_master_name"=>$airport_master_name);
        $update_airport_master=AirportMaster::where('airport_master_id',$airport_master_id)->update($update_airport_master_array);

        if($update_airport_master)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

 public static function searchAirports($airport_id)
  {

    $fetch_airport=AirportMaster::where('airport_master_id',$airport_id)->first();
    return $fetch_airport;
  }

public function enquiry_type(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('enquiry-type');
        $fetch_enquiry_type=EnquiryType::get();
        return view('mains.enquiry-type')->with(compact('fetch_enquiry_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function enquiry_type_insert(Request $request)

{

    $enquiry_type_name=$request->get('enquiry_type_name');

    $check_enquiry_type=EnquiryType::where('enquiry_type_name',$enquiry_type_name)->first();

    if(!empty($check_enquiry_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $enquiry_type_insert=new EnquiryType;

        $enquiry_type_insert->enquiry_type_name=$enquiry_type_name;

        $enquiry_type_insert->enquiry_type_created_by=$emp_id;

        if($enquiry_type_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function enquiry_type_update(Request $request)
{

    $enquiry_type_id=$request->get('enquiry_type_id');
    $enquiry_type_name=$request->get('enquiry_type_name');

    $check_enquiry_type=EnquiryType::where('enquiry_type_name',$enquiry_type_name)->where('enquiry_type_id',"!=",$enquiry_type_id)->first();

    if(!empty($check_enquiry_type))

    {

        echo "exist";

    }

    else
    {


        $update_enquiry_type_array=array("enquiry_type_name"=>$enquiry_type_name);
        $update_enquiry_type=EnquiryType::where('enquiry_type_id',$enquiry_type_id)->update($update_enquiry_type_array);

        if($update_enquiry_type)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}



public function tour_type(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('tour-type');
        $fetch_tour_type=TourType::get();
        return view('mains.tour-type-sightseeing')->with(compact('fetch_tour_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function tour_type_insert(Request $request)

{

    $tour_type_name=$request->get('tour_type_name');

    $check_tour_type=TourType::where('tour_type_name',$tour_type_name)->first();

    if(!empty($check_tour_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $tour_type_insert=new TourType;

        $tour_type_insert->tour_type_name=$tour_type_name;

        $tour_type_insert->tour_type_created_by=$emp_id;

        if($tour_type_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function tour_type_update(Request $request)

{

    $tour_type_id=$request->get('tour_type_id');

   $tour_type_name=$request->get('tour_type_name');

    $check_tour_type=TourType::where('tour_type_name',$tour_type_name)->where('tour_type_id','!=',$tour_type_id)->first();

    if(!empty($check_tour_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_tour_type_array=array("tour_type_name"=>$tour_type_name,);
        $update_tour_type=TourType::where('tour_type_id',$tour_type_id)->update($update_tour_type_array);

        if($update_tour_type)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}


public function hotel_type(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('hotel-type');
        $fetch_hotel_type=HotelType::get();
        return view('mains.hotel-type')->with(compact('fetch_hotel_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function hotel_type_insert(Request $request)

{

    $hotel_type_name=$request->get('hotel_type_name');

    $check_hotel_type=HotelType::where('hotel_type_name',$hotel_type_name)->first();

    if(!empty($check_hotel_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $hotel_type_insert=new HotelType;

        $hotel_type_insert->hotel_type_name=$hotel_type_name;

        $hotel_type_insert->hotel_type_created_by=$emp_id;

        if($hotel_type_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function hotel_type_update(Request $request)

{

    $hotel_type_id=$request->get('hotel_type_id');

   $hotel_type_name=$request->get('hotel_type_name');

    $check_hotel_type=HotelType::where('hotel_type_name',$hotel_type_name)->where('hotel_type_id','!=',$hotel_type_id)->first();

    if(!empty($check_hotel_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_hotel_type_array=array("hotel_type_name"=>$hotel_type_name,);
        $update_hotel_type=HotelType::where('hotel_type_id',$hotel_type_id)->update($update_hotel_type_array);

        if($update_hotel_type)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function activity_type(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('activity-type');
        $fetch_activity_type=ActivityType::get();
        return view('mains.activity-type')->with(compact('fetch_activity_type','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function activity_type_insert(Request $request)

{

    $activity_type_name=$request->get('activity_type_name');

    $check_activity_type=ActivityType::where('activity_type_name',$activity_type_name)->first();

    if(!empty($check_activity_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $activity_type_insert=new ActivityType;

        $activity_type_insert->activity_type_name=$activity_type_name;

        $activity_type_insert->activity_type_created_by=$emp_id;

        if($activity_type_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function activity_type_update(Request $request)

{

    $activity_type_id=$request->get('activity_type_id');

   $activity_type_name=$request->get('activity_type_name');

    $check_activity_type=ActivityType::where('activity_type_name',$activity_type_name)->where('activity_type_id','!=',$activity_type_id)->first();

    if(!empty($check_activity_type))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_activity_type_array=array("activity_type_name"=>$activity_type_name,);
        $update_activity_type=ActivityType::where('activity_type_id',$activity_type_id)->update($update_activity_type_array);

        if($update_activity_type)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function vehicle(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('vehicle');
         $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
        $fetch_vehicle=Vehicles::get();
        return view('mains.vehicle')->with(compact('fetch_vehicle_type','fetch_vehicle','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function vehicle_insert(Request $request)

{
    $vehicle_type_id=$request->get('vehicle_type_id');

    $vehicle_name=$request->get('vehicle_name');

    $check_vehicle_name=Vehicles::where('vehicle_name',$vehicle_name)->first();

    if(!empty($check_vehicle_name))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $vehicle_name_insert=new Vehicles;

         $vehicle_name_insert->vehicle_type_id=$vehicle_type_id;

        $vehicle_name_insert->vehicle_name=$vehicle_name;

        $vehicle_name_insert->vehicle_created_by=$emp_id;

        $vehicle_name_insert->vehicle_created_role=$emp_id;

        if($vehicle_name_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }





    }





}

public function update_vehicle_active(Request $request)
{
    $id=$request->get('vehicle_id');

  $action_perform=$request->get('action_perform');



 if($action_perform=="active")

  {

    $update_vehicle=Vehicles::where('vehicle_id',$id)->update(["vehicle_status"=>1]);

    if($update_vehicle)

    {

      echo "success";

    }

    else

    {

      echo "fail";

    }

  }

  else if($action_perform=="inactive")

  {

  $update_vehicle=Vehicles::where('vehicle_id',$id)->update(["vehicle_status"=>0]);

   if($update_vehicle)

   {

    echo "success";

  }

  else

  {

    echo "fail";

  }

}

}

public function fetchVehicle(Request $request)
{
    $vehicle_type_id=$request->get('vehicle_type_id');

    $get_vehicle=Vehicles::where('vehicle_type_id',$vehicle_type_id)->where('vehicle_status',1)->orderBy('vehicle_name','asc')->get();

    $html='<option value="0" hidden>LIST OF VEHICLES</option>';
    foreach($get_vehicle as $vehicle)
    {
        $html.='<option value="'.$vehicle->vehicle_id.'">'.strtoupper($vehicle->vehicle_name).'</option>';
    }
    echo $html;

}

public function cities(Request $request)
{
      if(session()->has('travel_users_id'))
       {
        $rights=$this->rights('cities');
         $fetch_countries=Countries::where('country_status',1)->get();
        return view('mains.cities')->with(compact('fetch_countries','rights'));

    }

    else

    {

        return redirect()->route('index');

    }


}

public function get_all_cities_tab(Request $request)
{
    $rights=$this->rights('cities');
    $country_id=$request->get('country_id');
    $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $country_id)->select("cities.*")->orderBy('cities.name','asc')->get(); 
    $html='<table id="example1" class="table table-bordered table-striped datatable">
    <thead>
    <tr>
    <th>ID</th>
    <th>CITIES NAME</th>
    <td>ACTION</td>
    </tr>
    </thead>
    <tbody>';

    foreach($fetch_cities as $cities)
    {
        $html.='<tr id="cities_id_'.$cities->id.'">
        <td>'.$cities->id.'</td>
        <td id="cities_name_'.$cities->id.'">'.$cities->name.'</td>
        <td><button id="edit_'.$cities->id.'" class="btn btn-default btn-rounded edit_cities"><i class="fa fa-pencil"></i></button> &nbsp; <button id="delete_'.$cities->id.'" class="btn btn-default btn-rounded delete_cities"><i class="fa fa-trash"></i></button></td>
        </tr>';
    }

    $html.=" <tbody></table>";

    echo $html;


}

public function cities_update(Request $request)
{
    $city_id=$request->get('cities_id');
     $city_name=$request->get('cities_name');

     $check_city=Cities::where('name',$city_name)->where('id','!=',$city_id)->first();

     if(!empty($check_city))

     {

        echo "exist";

    }

    else
    {
       $update_array=array("name"=>$city_name);

       $update=Cities::where('id',$city_id)->update($update_array);
       if($update)
       {
        echo "success";
    }
    else
    {
        echo "fail";
    }
}
}

public function cities_delete(Request $request)
{
    $city_id=$request->get('cities_id');

    $delete=Cities::where('id',$city_id)->delete();

    if($delete)
    {
        echo "success";
    }
    else
    {
        echo "fail";
    }

}   


public function customer_markup(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('customer-markup');
        $fetch_customer_markup=CustomerMarkup::get();
        return view('mains.customer-markup')->with(compact('fetch_customer_markup','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}

public function customer_markup_insert(Request $request)

{

    $customer_markup_name=$request->get('customer_markup_name');

    $customer_markup_cost=$request->get('customer_markup_cost');

    $check_customer_markup=CustomerMarkup::where('customer_markup',$customer_markup_name)->first();

    if(!empty($check_customer_markup))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $customer_markup_insert=new CustomerMarkup;

        $customer_markup_insert->customer_markup=$customer_markup_name;

        $customer_markup_insert->customer_markup_cost=$customer_markup_cost;

        $customer_markup_insert->customer_markup_created_by=$emp_id;

        if($customer_markup_insert->save())

        {
            $last_id=$customer_markup_insert->id;

           $customer_markup_log_insert=new CustomerMarkup_log;

           $customer_markup_log_insert->customer_markup_id=$last_id;

           $customer_markup_log_insert->customer_markup=$customer_markup_name;

           $customer_markup_log_insert->customer_markup_cost=$customer_markup_cost;

           $customer_markup_log_insert->customer_markup_created_by=$emp_id;

           $customer_markup_log_insert->customer_markup_operation="INSERT";

           $customer_markup_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function customer_markup_update(Request $request)

{

    $customer_markup_id=$request->get('customer_markup_id');

    $customer_markup_name=$request->get('customer_markup_name');

    $customer_markup_cost=$request->get('customer_markup_cost');

    $check_customer_markup=CustomerMarkup::where('customer_markup',$customer_markup_name)->where('customer_markup_id','!=',$customer_markup_id)->first();

    if(!empty($check_customer_markup))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_customer_markup_array=array("customer_markup_cost"=>$customer_markup_cost);
        $update_customer_markup=CustomerMarkup::where('customer_markup_id',$customer_markup_id)->update($update_customer_markup_array);

        if($update_customer_markup)

        {
           $customer_markup_log_insert=new CustomerMarkup_log;

           $customer_markup_log_insert->customer_markup_id=$customer_markup_id;

           $customer_markup_log_insert->customer_markup=$customer_markup_name;

           $customer_markup_log_insert->customer_markup_cost=$customer_markup_cost;

           $customer_markup_log_insert->customer_markup_created_by=$emp_id;

           $customer_markup_log_insert->customer_markup_operation="UPDATE";

           $customer_markup_log_insert->save();

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}





public function target_commission(Request $request)

    {

       if(session()->has('travel_users_id'))

       {

        $rights=$this->rights('target-commission');
        $fetch_target_commission=SettingTargetCommission::get();
        return view('mains.target_commission')->with(compact('fetch_target_commission','rights'));

    }

    else

    {

        return redirect()->route('index');

    }

}



public function target_commission_insert(Request $request)

{

  $users_id=session()->get('travel_users_id');
    $st_amount=$request->get('target_amount');

    $st_commission_per=$request->get('target_commission');

    $check_target_commission=SettingTargetCommission::where('st_amount',$st_amount)->first();

    if(!empty($check_target_commission))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');

        $target_commission_insert=new SettingTargetCommission;

        $target_commission_insert->st_amount=$st_amount;

        $target_commission_insert->st_commission_per=$st_commission_per;

        $target_commission_insert->st_created_by=$users_id;

        if($target_commission_insert->save())

        {

            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}

public function target_commission_update(Request $request)

{

    $st_commission_id=$request->get('target_commission_id');

   $st_amount=$request->get('target_amount');

    $st_commission_per=$request->get('target_commission');

    $check_target_commission=SettingTargetCommission::where('st_amount',$st_amount)->where('st_commission_id','!=',$st_commission_id)->first();

    if(!empty($check_target_commission))

    {

        echo "exist";

    }

    else
    {

        $emp_id=session()->has('travel_users_id');
        $update_target_commission_array=array("st_amount"=>$st_amount,"st_commission_per"=>$st_commission_per);
        $update_target_commission=SettingTargetCommission::where('st_commission_id',$st_commission_id)->update($update_target_commission_array);

        if($update_target_commission)
        {
            echo "success";

        }

        else

        {

            echo "fail";

        }

    }

}


}

