<?php

namespace App\Http\Controllers;

use Session;
use App\Users;
use App\Countries;
use App\States;
use App\Cities;
use App\Currency;
use App\Suppliers;
use App\Activities;
use App\Transport;
use App\Hotels;
use App\HotelRooms;
use App\Guides;
use App\Guides_log;
use App\Languages;
use App\Vehicles;
use App\VehicleType;
use App\HotelMeal;
use App\Amenities;
use App\SightSeeing;
use App\SubAmenities;
use App\Transfers;
use App\TransferDetails;

use App\Restaurants;
use App\Restaurants_log;
use App\RestaurantType;
use App\RestaurantFood;
use App\RestaurantMenuCategory;

use App\AirportMaster;
use Illuminate\Http\Request;
use App\Bookings;
use App\Drivers;
use App\Drivers_log;
use App\HotelType;
use App\ActivityType;
use App\SupplierWallet;
use Mail;
use App\Mail\SendMailable;

class SupplierController extends Controller
{
 public function __construct()
 {
  date_default_timezone_set('Asia/Dubai');
}
public function index()
{
  if(session()->has('travel_supplier_id'))
  {
    return view('supplier.home');
  }
  else
  {
   return view('supplier.index');
 }

}
public function supplier_login_check(Request $request)
{
 $username=$request->get('username');
 $password=$request->get('password');

 $check_users=Suppliers::where('company_email',$username)->where('supplier_password',md5($password))->first();
 if($check_users)
 {
  if($check_users->supplier_status=='1')
  {
   session()->put('travel_supplier_id',$check_users->supplier_id);
   session()->put('travel_email_supplier',$check_users->company_email);
   session()->put('travel_supplier_fullame',$check_users->supplier_name);
   session()->put('travel_supplier_type',$check_users->supplier_service_type);
session()->put('travel_supplier_logo',$check_users->supplier_logo);
   session()->put('travel_users_role',"Supplier");
   echo "success";
 }
 else
 {
   echo "inactive";
 }



}
else
{
  echo "fail";
}

}
public function supplier_logout(Request $request)
{
  Session::forget('travel_supplier_id');
  Session::forget('travel_supplier_type');
  Session::forget('travel_email_supplier');
  if(!Session::has('travel_supplier_id'))

  {

    return redirect()->intended('/supplier');

  }
}
public function supplier_home(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {

   return view('supplier.home');
 }
 else
 {
   return redirect()->route('supplier');
 }
}

public function register_supplier(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {
  return view('supplier.home');

}
else
{
  $countries=Countries::where('country_status',1)->get();
  $currency=Currency::get();
  return view('supplier.register-supplier')->with(compact('countries','currency'));
}


}

public function insert_supplier(Request $request)
{
  $supplier_name=$request->get('supplier_name');
  $company_name=$request->get('company_name');
  $email_id=$request->get('email_id');
  $contact_number=$request->get('contact_number');
  $check_supplier=Suppliers::where('company_email',$email_id)->orWhere('company_contact',$contact_number)->get();
  if(count($check_supplier)>0)
  {

    echo "exist";
  }
  else
  {

   $fax_number=$request->get('fax_number');
   $supplier_reference_id=$request->get('supplier_reference_id');
   $address=$request->get('address');
   $supplier_country=$request->get('supplier_country');
   $supplier_city=$request->get('supplier_city');
   $corporate_reg_no=$request->get('corporate_reg_no');
   $corporate_description=$request->get('corporate_description');
   $skype_id=$request->get('skype_id');
   $fuel_info=$request->get('fuel_info');
   $operating_hrs_from=$request->get('operating_hrs_from');
   $operating_hrs_to=$request->get('operating_hrs_to');
   $week_monday=$request->get('week_monday');	
   $week_tuesday=$request->get('week_tuesday');
   $week_wednesday=$request->get('week_wednesday');
   $week_thursday=$request->get('week_thursday');
   $week_friday=$request->get('week_friday');
   $week_saturday=$request->get('week_saturday');
   $week_sunday=$request->get('week_sunday');
   $supplier_opr_currency=$request->get('supplier_opr_currency');
   $supplier_opr_countries=$request->get('supplier_opr_countries');
   $blackout_days=$request->get('blackout_days');
   $account_number=$request->get('account_number');
   $bank_name=$request->get('bank_name');
   $bank_swift=$request->get('bank_swift');
   $bank_iban=$request->get('bank_iban');
   $bank_currency=$request->get('bank_currency');
   $service_type=$request->get('service_type');
   $contact_person_name=$request->get('contact_person_name');
   $contact_person_number=$request->get('contact_person_number');	
   $contact_person_email=$request->get('contact_person_email');
   $supplier_certificate_file=$request->get('supplier_certificate_file');
   $supplier_logo_file=$request->get('supplier_logo_file');

   if($request->hasFile('supplier_certificate_file'))
   {
    $supplier_certificate_file=$request->file('supplier_certificate_file');
    $extension=strtolower($request->supplier_certificate_file->getClientOriginalExtension());
    if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
    {
      $certificate_supplier = "certificate-".time().'.'.$request->file('supplier_certificate_file')->getClientOriginalExtension();

                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
      $dir = 'assets/uploads/supplier_certificates/';

      $request->file('supplier_certificate_file')->move($dir, $certificate_supplier);
    }
    else
    {
      $certificate_supplier = "";
    }
  }
  else
  {
    $certificate_supplier = "";
  }

  if($request->hasFile('supplier_logo_file'))
  {
    $supplier_logo_file=$request->file('supplier_logo_file');
    $extension=strtolower($request->supplier_logo_file->getClientOriginalExtension());
    if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
    {
      $logo_supplier = "logo-".time().'.'.$request->file('supplier_logo_file')->getClientOriginalExtension();

                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
      $dir1 = 'assets/uploads/supplier_logos/';

      $request->file('supplier_logo_file')->move($dir1, $logo_supplier);
    }
    else
    {
      $logo_supplier = "";
    }
  }
  else
  {
    $logo_supplier = "";
  }

  $operating_weekdays=array("monday"=>$week_monday,
   "tuesday"=>$week_tuesday,
   "wednesday"=>$week_wednesday,
   "thursday"=>$week_thursday,
   "friday"=>$week_friday,
   "saturday"=>$week_saturday,
   "sunday"=>$week_sunday);

  $operating_weekdays=serialize($operating_weekdays);
  $supplier_opr_currency=implode(",",$supplier_opr_currency);
  $supplier_opr_countries=implode(",",$supplier_opr_countries);

  $service_type=implode(",",$service_type);
  $supplier_bank_details=array();
  for($bank_count=0;$bank_count<count($account_number);$bank_count++)
  {
    $supplier_bank_details[$bank_count]['account_number']=$account_number[$bank_count];
    $supplier_bank_details[$bank_count]['bank_name']=$bank_name[$bank_count];
    $supplier_bank_details[$bank_count]['bank_ifsc']=$bank_swift[$bank_count];
    $supplier_bank_details[$bank_count]['bank_iban']=$bank_iban[$bank_count];
    $supplier_bank_details[$bank_count]['bank_currency']=$bank_currency[$bank_count];

  }
  $contact_persons=array();
  for($contact_count=0;$contact_count<count($contact_person_name);$contact_count++)
  {
    $contact_persons[$contact_count]['contact_person_name']=$contact_person_name[$contact_count];
    $contact_persons[$contact_count]['contact_person_number']=$contact_person_number[$contact_count];
    $contact_persons[$contact_count]['contact_person_email']=$contact_person_email[$contact_count];

  }
  $supplier_password_hint=$request->get('acc_password');
  $supplier_password=md5($supplier_password_hint);

  $supplier_bank_details=serialize($supplier_bank_details);
  $contact_persons=serialize($contact_persons);
  $supplier=new Suppliers;
  $supplier->supplier_name=$supplier_name;
  $supplier->company_name=$company_name;
  $supplier->company_email=$email_id;
  $supplier->supplier_password=$supplier_password;
  $supplier->supplier_password_hint=$supplier_password_hint;
  $supplier->company_contact=$contact_number;
  $supplier->company_fax=$fax_number;
  $supplier->supplier_ref_id=$supplier_reference_id;
  $supplier->address=$address;
  $supplier->supplier_country=$supplier_country;
  $supplier->supplier_city=$supplier_city;
  $supplier->corporate_reg_no=$corporate_reg_no;
  $supplier->corporate_desc=$corporate_description;
  $supplier->skype_id=$skype_id;
  $supplier->fuel_info=$fuel_info; 	
  $supplier->operating_hrs_from=$operating_hrs_from;
  $supplier->operating_hrs_to=$operating_hrs_to;	
  $supplier->operating_weekdays=$operating_hrs_to;
  $supplier->operating_weekdays=$operating_weekdays;
  $supplier->certificate_corp=$certificate_supplier;	
  $supplier->supplier_logo=$logo_supplier;
  $supplier->supplier_opr_currency=$supplier_opr_currency;
  $supplier->supplier_opr_countries=$supplier_opr_countries;	
  $supplier->blackout_dates=$blackout_days;	
  $supplier->supplier_bank_details=$supplier_bank_details;
  $supplier->supplier_service_type=$service_type;
  $supplier->contact_persons=$contact_persons;
  $supplier->supplier_status=0;	
  if($supplier->save())
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}


}

public function supplier_profile(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $supplier_id=session()->get('travel_supplier_id');
    $currency=Currency::get();
    $countries=Countries::where('country_status',1)->get();

    $get_supplier=Suppliers::where('supplier_id',$supplier_id)->first();
    $supplier_id=base64_encode(base64_encode($supplier_id));
    if($get_supplier)
    {
     return view('supplier.supplier-profile')->with(compact('get_supplier','countries','currency'))->with('supplier_id',$supplier_id);
   }
   else
   {
     return redirect()->back();
   }
 }
 else
 {
  return redirect()->route('supplier');
}
}

public function supplier_profile_edit(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {
  $supplier_id=session()->get('travel_supplier_id');
  $currency=Currency::get();
  $countries=Countries::where('country_status',1)->get();

  $get_supplier=Suppliers::where('supplier_id',$supplier_id)->first();
  $supplier_id=base64_encode(base64_encode($supplier_id));
  if($get_supplier)
  {
    return view('supplier.supplier-profile-edit')->with(compact('get_supplier','countries','currency'))->with('supplier_id',$supplier_id);
  }
  else
  {
    return redirect()->back();
  }
}
else
{
  return redirect()->route('supplier');
}
}
public function supplier_profile_update(Request $request)
{
 $supplier_id=session()->get('travel_supplier_id');

 $check_supplier=Suppliers::where('supplier_id',$supplier_id)->first();
 if(!$check_supplier)
 {
  echo "fail";
}
else
{
  $certificate_data=$check_supplier->certificate_corp;
  $logo_data=$check_supplier->supplier_logo;

  $supplier_name=$request->get('supplier_name');
  $company_name=$request->get('company_name');
  $email_id=$request->get('email_id');
  $contact_number=$request->get('contact_number');
  $fax_number=$request->get('fax_number');
  $supplier_reference_id=$request->get('supplier_reference_id');
  $address=$request->get('address');
  $supplier_country=$request->get('supplier_country');
  $supplier_city=$request->get('supplier_city');
  $corporate_reg_no=$request->get('corporate_reg_no');
  $corporate_description=$request->get('corporate_description');
  $skype_id=$request->get('skype_id');
  $fuel_info=$request->get('fuel_info');
  $operating_hrs_from=$request->get('operating_hrs_from');
  $operating_hrs_to=$request->get('operating_hrs_to');
  $week_monday=$request->get('week_monday'); 
  $week_tuesday=$request->get('week_tuesday');
  $week_wednesday=$request->get('week_wednesday');
  $week_thursday=$request->get('week_thursday');
  $week_friday=$request->get('week_friday');
  $week_saturday=$request->get('week_saturday');
  $week_sunday=$request->get('week_sunday');
  $supplier_opr_currency=$request->get('supplier_opr_currency');
  $supplier_opr_countries=$request->get('supplier_opr_countries');
  $blackout_days=$request->get('blackout_days');
  $account_number=$request->get('account_number');
  $bank_name=$request->get('bank_name');
  $bank_swift=$request->get('bank_swift');
  $bank_iban=$request->get('bank_iban');
  $bank_currency=$request->get('bank_currency');
  $service_type=$request->get('service_type');
  $contact_person_name=$request->get('contact_person_name');
  $contact_person_number=$request->get('contact_person_number'); 
  $contact_person_email=$request->get('contact_person_email');
  $supplier_certificate_file=$request->get('supplier_certificate_file');
  $supplier_logo_file=$request->get('supplier_logo_file');

  if($request->hasFile('supplier_certificate_file'))
  {
    $supplier_certificate_file=$request->file('supplier_certificate_file');
    $extension=strtolower($request->supplier_certificate_file->getClientOriginalExtension());
    if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
    {
      $certificate_supplier = "certificate-".time().'.'.$request->file('supplier_certificate_file')->getClientOriginalExtension();

                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
      $dir = 'assets/uploads/supplier_certificates/';

      $request->file('supplier_certificate_file')->move($dir, $certificate_supplier);
    }
    else
    {
      $certificate_supplier = "";
    }
  }
  else
  {
    $certificate_supplier=$certificate_data;
  }


  if($request->hasFile('supplier_logo_file'))
  {
    $supplier_logo_file=$request->file('supplier_logo_file');
    $extension=strtolower($request->supplier_logo_file->getClientOriginalExtension());
    if($extension=="png" || $extension=="jpg" || $extension=="jpeg" || $extension=="pdf")
    {
      $logo_supplier = "logo-".time().'.'.$request->file('supplier_logo_file')->getClientOriginalExtension();

                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
      $dir1 = 'assets/uploads/supplier_logos/';

      $request->file('supplier_logo_file')->move($dir1, $logo_supplier);
    }
    else
    {
      $logo_supplier = "";
    }
  }
  else
  {
    $logo_supplier=$logo_data;
  }


  $operating_weekdays=array("monday"=>$week_monday,
    "tuesday"=>$week_tuesday,
    "wednesday"=>$week_wednesday,
    "thursday"=>$week_thursday,
    "friday"=>$week_friday,
    "saturday"=>$week_saturday,
    "sunday"=>$week_sunday);

  $operating_weekdays=serialize($operating_weekdays);
  $supplier_opr_currency=implode(",",$supplier_opr_currency);
  $supplier_opr_countries=implode(",",$supplier_opr_countries);

  $service_type=implode(",",$service_type);
  $supplier_bank_details=array();
  for($bank_count=0;$bank_count<count($account_number);$bank_count++)
  {
    $supplier_bank_details[$bank_count]['account_number']=$account_number[$bank_count];
    $supplier_bank_details[$bank_count]['bank_name']=$bank_name[$bank_count];
    $supplier_bank_details[$bank_count]['bank_ifsc']=$bank_swift[$bank_count];
    $supplier_bank_details[$bank_count]['bank_iban']=$bank_iban[$bank_count];
    $supplier_bank_details[$bank_count]['bank_currency']=$bank_currency[$bank_count];

  }
  $contact_persons=array();
  for($contact_count=0;$contact_count<count($contact_person_name);$contact_count++)
  {
    $contact_persons[$contact_count]['contact_person_name']=$contact_person_name[$contact_count];
    $contact_persons[$contact_count]['contact_person_number']=$contact_person_number[$contact_count];
    $contact_persons[$contact_count]['contact_person_email']=$contact_person_email[$contact_count];

  }

  $supplier_bank_details=serialize($supplier_bank_details);
  $contact_persons=serialize($contact_persons);

  $update_data=array("supplier_name"=>$supplier_name,
    "company_name"=>$company_name,
    "company_email"=>$email_id,
    "company_contact"=>$contact_number,
    "company_fax"=>$fax_number,
    // "supplier_ref_id"=>$supplier_reference_id,
    "address"=>$address,
    "supplier_country"=>$supplier_country,
    "supplier_city"=>$supplier_city,
    "corporate_reg_no"=>$corporate_reg_no,
    "corporate_desc"=>$corporate_description,
    "skype_id"=>$skype_id, 
    "fuel_info"=>$fuel_info,   
    "operating_hrs_from"=>$operating_hrs_from,
    "operating_hrs_to"=>$operating_hrs_to,  
    "operating_weekdays"=>$operating_hrs_to,
    "operating_weekdays"=>$operating_weekdays,
    "certificate_corp"=>$certificate_supplier,  
    "supplier_logo"=>$logo_supplier,
    "supplier_opr_currency"=>$supplier_opr_currency,
    "supplier_opr_countries"=>$supplier_opr_countries,  
    "blackout_dates"=>$blackout_days,  
    "supplier_bank_details"=>$supplier_bank_details,
    "supplier_service_type"=>$service_type,
    "contact_persons"=>$contact_persons
  );


  $update_query=Suppliers::where('supplier_id',$supplier_id)->update($update_data);
  if($update_query)
  {
    session()->put('travel_supplier_type',$service_type);
    echo "success";
  }
  else
  {
    echo "fail";
  }
}

}


public function supplier_hotel(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {
  $supplier_id=session()->get('travel_supplier_id');	
  $countries=Countries::where('country_status',1)->get();
  $cities=Cities::get();
  $emp_id=session()->get('travel_supplier_id');
  $get_hotels=Hotels::where('hotel_created_by',$supplier_id)->where('hotel_create_role','Supplier')->get();
  $get_suppliers=Suppliers::get();
  return view('supplier.supplier-hotel')->with(compact('countries','cities','get_suppliers','get_hotels'));
}
else
{
 return redirect()->route('supplier');
}
}
public function supplier_create_hotel(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::get();
   $suppliers=Suppliers::get();
   $hotel_meal=HotelMeal::get();
   $supplier_id=session()->get('travel_supplier_id');
   $supplier_name=session()->get('travel_supplier_fullame');
   $fetch_amenities=Amenities::where('amenities_status',1)->get();
   $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();
   $fetch_sub_amenities=SubAmenities::where('sub_amenities_status',1)->get();
   return view('supplier.create-hotel')->with(compact('countries','currency','supplier_id','supplier_name','hotel_meal','fetch_amenities','fetch_sub_amenities','fetch_hotel_type'));
 }
 else
 {
  return redirect()->route('supplier');
}
}
public function supplier_edit_hotel($hotel_id)
{
 if(session()->has('travel_supplier_id'))
 {
  $countries=Countries::where('country_status',1)->get();
  $currency=Currency::get();
  $suppliers=Suppliers::get();
  $hotel_meal=HotelMeal::get();
  $get_hotels=Hotels::where('hotel_id',$hotel_id)->where('hotel_create_role',"Supplier")->first();
  $fetch_amenities=Amenities::get();
  $fetch_hotel_type=HotelType::where('hotel_type_status',1)->get();
  $fetch_sub_amenities=SubAmenities::get();
  if($get_hotels)
  {
   $supplier_id=$get_hotels->supplier_id;

   $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

   $supplier_countries=$get_supplier_countries->supplier_opr_countries;

   $countries_data=explode(',', $supplier_countries);

   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_hotels->hotel_country)->select("cities.*")->orderBy('cities.name','asc')->get();
   $hotel_currency="";
   $hotel_currency_fetch=HotelRooms::where('hotel_id',$get_hotels->hotel_id)->first();
   if(!empty($hotel_currency_fetch))
   {
    $hotel_currency=$hotel_currency_fetch->hotel_room_currency;
  }

  return view('supplier.edit-hotel')->with(compact('countries','currency','cities','suppliers','get_hotels',"countries_data","get_supplier_countries","hotel_meal","fetch_amenities","fetch_sub_amenities","fetch_hotel_type","hotel_currency"));
}
else
{
  return redirect()->route('supplier-hotel');
}

}
else
{
  return redirect()->route('supplier');
}
}


    //activity
public function supplier_activity(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {		
  $supplier_id=session()->get('travel_supplier_id');	
  $countries=Countries::where('country_status',1)->get();
  $cities=Cities::get();
  $emp_id=session()->get('travel_supplier_id');
  $get_activites=Activities::where('activity_created_by',$supplier_id)->where('activity_role','Supplier')->get();
  $get_transport=Transport::get();
  $get_suppliers=Suppliers::get();
  $hotel_meal=HotelMeal::get();
  return view('supplier.supplier-activity')->with(compact('countries','cities','get_activites','get_suppliers','get_transport','hotel_meal'));
}
else
{
  return redirect()->route('supplier');
}
}
public function supplier_create_activity(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
   $supplier_id=session()->get('travel_supplier_id');
   $supplier_name=session()->get('travel_supplier_fullame');
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::get();
   $fetch_activity_type=ActivityType::where('activity_type_status',1)->get();
   return view('supplier.create-activity')->with(compact('countries','currency','supplier_id','fetch_activity_type','supplier_name'));
 }
 else
 {
   return redirect()->route('supplier');
 }
}
public function supplier_activity_details($activity_id)
{
 if(session()->has('travel_supplier_id'))
 {
  $countries=Countries::where('country_status',1)->get();
  $currency=Currency::get();
  $suppliers=Suppliers::get();
  $get_activity=Activities::where('activity_id',$activity_id)->first();
  if($get_activity)
  {
    $supplier_id=$get_activity->supplier_id;

    $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

    $supplier_countries=$get_supplier_countries->supplier_opr_countries;

    $countries_data=explode(',', $supplier_countries);

    $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_activity->activity_country)->select("cities.*")->orderBy('cities.name','asc')->get();

    return view('supplier.activity-details-view')->with(compact('countries','currency','cities','suppliers','get_activity',"countries_data"));
  }
  else
  {
    return redirect()->route('supplier-activity');
  }

}
else
{
 return redirect()->route('supplier');
}
}
	//edit activity
public function supplier_edit_activity ($activity_id)
{
 if(session()->has('travel_supplier_id'))
 {
  $countries=Countries::where('country_status',1)->get();
  $currency=Currency::get();

  $get_activity=Activities::where('activity_id',$activity_id)->where('activity_role',"Supplier")->first();
  if($get_activity)
  {
    $supplier_id=$get_activity->supplier_id;

    $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

    $supplier_countries=$get_supplier_countries->supplier_opr_countries;

    $countries_data=explode(',', $supplier_countries);


    $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_activity->activity_country)->select("cities.*")->orderBy('cities.name','asc')->get();
    $fetch_activity_type=ActivityType::where('activity_type_status',1)->get();
    return view('supplier.edit-activity')->with(compact('countries','currency','cities','get_activity',"countries_data","fetch_activity_type","get_supplier_countries"));
  }
  else
  {
    return redirect()->route('supplier-activity');
  }

}
else
{
 return redirect()->route('supplier');
}

}
	  //transport
public function supplier_transport(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
   $supplier_id=session()->get('travel_supplier_id');	
   $countries=Countries::where('country_status',1)->get();
   $cities=Cities::get();
   $emp_id=session()->get('travel_supplier_id');

   $get_transport=Transport::where('transfer_created_by',$supplier_id)->where('transfer_create_role','Supplier')->get();

   $get_suppliers=Suppliers::get();
   return view('supplier.supplier-transport')->with(compact('countries','cities','get_suppliers','get_transport'));
 }
 else
 {
   return redirect()->route('supplier');
 }

}
public function supplier_create_transport(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::get();
    $suppliers=Suppliers::get();
    $vehicle_type=VehicleType::get();
    $supplier_id=session()->get('travel_supplier_id');
    $supplier_name=session()->get('travel_supplier_fullame');
    return view('supplier.create-transport')->with(compact('countries','currency','supplier_id','supplier_name','vehicle_type'));
  }
  else
  {
    return redirect()->route('supplier');
  }
}
public function supplier_transport_details($transport_id)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::get();
    $suppliers=Suppliers::get();
    $vehicle_type=VehicleType::get();
    $get_transport=Transport::where('transport_id',$transport_id)->first();
    if($get_transport)
    {
      $supplier_id=$get_transport->supplier_id;

      $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

      $supplier_countries=$get_supplier_countries->supplier_opr_countries;

      $countries_data=explode(',', $supplier_countries);

      return view('supplier.transport-details-view')->with(compact('countries','currency','cities','suppliers','get_transport','countries_data','vehicle_type'));
    }
    else
    {
      return redirect()->back();
    }

  }
  else
  {
    return redirect()->route('supplier');
  }
}
public function supplier_edit_transport($transport_id)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::get();
    $suppliers=Suppliers::get();
    $vehicle_type=VehicleType::get();
    $get_transport=Transport::where('transport_id',$transport_id)->first();
    if($get_transport)
    {
     $supplier_id=$get_transport->supplier_id;

     $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

     $supplier_countries=$get_supplier_countries->supplier_opr_countries;

     $countries_data=explode(',', $supplier_countries);

     $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_transport->transfer_country)->select("cities.*")->orderBy('cities.name','asc')->get();

     return view('supplier.edit-transport')->with(compact('countries','currency','cities','get_supplier_countries','get_transport',"countries_data","vehicle_type"));
   }
   else
   {
    return redirect()->back();
  }

}
else
{
  return redirect()->route('supplier');
}
}
	   //hotel details
public function supplier_hotel_details($hotel_id)
{
  if(session()->has('travel_supplier_id'))
  {
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::get();
   $suppliers=Suppliers::get();
   $get_hotels=Hotels::where('hotel_id',$hotel_id)->first();
   $hotel_meal=HotelMeal::get();
   if($get_hotels)
   {
     $supplier_id=$get_hotels->supplier_id;

     $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

     $supplier_countries=$get_supplier_countries->supplier_opr_countries;

     $countries_data=explode(',', $supplier_countries);

     return view('supplier.hotel-details-view')->with(compact('countries','currency','cities','suppliers','get_hotels','hotel_meal','countries_data'));
   }
   else
   {
     return redirect()->back();
   }

 }
 else
 {
   return redirect()->route('supplier');
 }

}

	   // Driver Code for supplier start
public function driver_management(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $emp_id=session()->get('travel_supplier_id');
    $get_drivers=Drivers::where(function($query) use($emp_id){
      $query->where('driver_supplier_id',$emp_id);
    })->orWhere(function($query1) use($emp_id){
      $query1->where('driver_created_by',$emp_id)->where('driver_role',"Supplier");
    })->get();
    
    return view('supplier.driver-management')->with(compact('get_drivers','countries'));
  }
  else
  {
    return redirect()->route('supplier');
  }

}
public function create_driver(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $supplier_id=session()->get('travel_supplier_id');

    $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

    $supplier_countries=$get_supplier_countries->supplier_opr_countries;
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    $countries_data=explode(',', $supplier_countries);
    $languages=Languages::get();
    return view('supplier.create-driver')->with(compact('countries','get_supplier_countries',"countries_data",'languages','fetch_vehicle_type'));
  }
  else
  {
    return redirect()->route('supplier');
  }
}
public function insert_driver(Request $request)
{

  $driver_created_by=session()->get('travel_supplier_id');
  $driver_first_name=$request->get('driver_first_name');
  $driver_last_name=$request->get('driver_last_name');
  $driver_contact=$request->get('contact_number');
  $driver_address=$request->get('address');
  $check_drivers=Drivers::where('driver_contact',$driver_contact)->get();
  if(count($check_drivers)>0)
  {

    echo "exist";
  }
  else
  {

   $driver_country=$request->get('driver_country');

   $driver_city=$request->get('driver_city');

   $driver_language=$request->get('driver_language');

   $driver_language=implode(',',$driver_language);

   $driver_price_per_day=$request->get('driver_price_per_day');
   $driver_food_cost=$request->get('driver_food_cost');
   $driver_hotel_cost=$request->get('driver_hotel_cost');

   $driver_description=$request->get('description');

   $week_monday=$request->get('week_monday');

   $week_tuesday=$request->get('week_tuesday');

   $week_wednesday=$request->get('week_wednesday');

   $week_thursday=$request->get('week_thursday');

   $week_friday=$request->get('week_friday');

   $week_saturday=$request->get('week_saturday');

   $week_sunday=$request->get('week_sunday');



   $operating_weekdays=array("monday"=>$week_monday,

    "tuesday"=>$week_tuesday,

    "wednesday"=>$week_wednesday,

    "thursday"=>$week_thursday,

    "friday"=>$week_friday,

    "saturday"=>$week_saturday,

    "sunday"=>$week_sunday);



   $operating_weekdays=serialize($operating_weekdays);

   $driver_blackout_dates=$request->get('blackout_days');



   $driver_nationality=$request->get('driver_nationality');

   $driver_markup=$request->get('driver_markup');

   $driver_amount=$request->get('driver_amount');


   $vehicle_type_array=$request->get('vehicle_type');
   $vehicle_type=implode(",",$vehicle_type_array);

   $vehicle_array=$request->get('vehicle');
   $vehicle=implode(",",$vehicle_array);

   $vehicle_info_array=$request->get('vehicle_info');
   $vehicle_info=implode("---",$vehicle_info_array);



   $vehicle_images=$request->file('vehicle_images');  

        //multifile uploading
   $vehicle_images_array=array();

   if(count($vehicle_images)>0)
   {
     $vehicle_images_array_indexes=array_keys($vehicle_images); 
   }



   for($i=0;$i<count($vehicle_images);$i++)
   {
    $actual_index=$vehicle_images_array_indexes[$i];
    if(!empty($vehicle_images[$actual_index]))
    {
      $vehicle_image_index=$vehicle_images[$actual_index];
      if(!empty($vehicle_image_index))
      {

       foreach($vehicle_image_index as $file)
       {
        $extension=strtolower($file->getClientOriginalExtension());
        if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
        {
          $image_name=$file->getClientOriginalName();
          $image_sightseeing = "driver-vehicle-".time()."-".$image_name;
                        // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
          $dir1 = 'assets/uploads/driver_vehicle_images/';
          $file->move($dir1, $image_sightseeing);
          $vehicle_images_array[$actual_index][]=$image_sightseeing;
        }
      }
    }
  }

}

$vehicle_images_array=serialize($vehicle_images_array);



$nationality_markup_details=array();

for($nation_count=0;$nation_count<count($driver_nationality);$nation_count++)

{

  $nationality_markup_details[$nation_count]['driver_nationality']=$driver_nationality[$nation_count];

  $nationality_markup_details[$nation_count]['driver_markup']=$driver_markup[$nation_count];

  $nationality_markup_details[$nation_count]['driver_amount']=$driver_amount[$nation_count];

}



$nationality_markup_details=serialize($nationality_markup_details);



$driver_validity_from=$request->get('driver_validity_from');

$driver_validity_to=$request->get('driver_validity_to');

$driver_tourname=$request->get('driver_tourname');  

$driver_cost_four=$request->get('driver_cost_four');

$driver_cost_seven=$request->get('driver_cost_seven');

$driver_cost_twenty=$request->get('driver_cost_twenty');

$driver_duration=$request->get('driver_duration');





$driver_tariff=array();

for($transport_count=0;$transport_count<count($driver_tourname);$transport_count++)

{

  $driver_tariff[$transport_count]['driver_validity_from']=$driver_validity_from[$transport_count];

  $driver_tariff[$transport_count]['driver_validity_to']=$driver_validity_to[$transport_count];

  $driver_tariff[$transport_count]['driver_tourname']=$driver_tourname[$transport_count];

  $driver_tariff[$transport_count]['driver_cost_four']=$driver_cost_four[$transport_count];

  $driver_tariff[$transport_count]['driver_cost_seven']=$driver_cost_seven[$transport_count];

  $driver_tariff[$transport_count]['driver_cost_twenty']=$driver_cost_twenty[$transport_count];

  $driver_tariff[$transport_count]['driver_duration']=$driver_duration[$transport_count];



}



$driver_tariff=serialize($driver_tariff);


if($request->has('tour_name'))
{
  $tour_name=$request->get('tour_name');

  $tour_vehiclename=$request->get('tour_vehiclename');
  $tour_driver_cost=$request->get('tour_guide_cost');

  $driver_tours_cost=array();
  for($cost_count=0;$cost_count<count($tour_name);$cost_count++)

  {

   if(!empty($tour_name[$cost_count]))
   {
    $driver_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
    $driver_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
    $driver_tours_cost[$cost_count]["tour_driver_cost"]=$tour_driver_cost[($tour_name[$cost_count]-1)];
  }
}


}
else
{
  $driver_tours_cost=array();
}


$driver_tours_cost=serialize($driver_tours_cost);







$driver_logo_file=$request->get('driver_logo_file');



if($request->hasFile('driver_logo_file'))

{

  $driver_logo_file=$request->file('driver_logo_file');

  $extension=strtolower($request->driver_logo_file->getClientOriginalExtension());

  if($extension=="png" || $extension=="jpg" || $extension=="jpeg")

  {

    $driver_image = "driver-".time().'.'.$request->file('driver_logo_file')->getClientOriginalExtension();



                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);

    $dir1 = 'assets/uploads/driver_images/';



    $request->file('driver_logo_file')->move($dir1, $driver_image);

  }

  else

  {

    $driver_image = "";

  }

}

else

{

  $driver_image = "";

}



$driver_inclusions=$request->get('driver_inclusions');

$driver_exclusions=$request->get('driver_exclusions');

$driver_cancel_policy=$request->get('driver_cancellation');

$driver_terms_conditions=$request->get('driver_terms_conditions');

$driver=new Drivers;
$driver->driver_first_name=$driver_first_name;
$driver->driver_last_name=$driver_last_name;
$driver->driver_contact=$driver_contact;
$driver->driver_address=$driver_address;
$driver->driver_supplier_id=$driver_created_by;
$driver->driver_country=$driver_country;
$driver->driver_city=$driver_city;
$driver->driver_tours_cost=$driver_tours_cost;
$driver->driver_language=$driver_language;
$driver->driver_price_per_day=$driver_price_per_day;
$driver->driver_description=$driver_description;
$driver->operating_weekdays=$operating_weekdays;
$driver->driver_blackout_dates=$driver_blackout_dates;
$driver->nationality_markup_details=$nationality_markup_details;
$driver->driver_tariff=$driver_tariff;
$driver->driver_food_cost=$driver_food_cost;
$driver->driver_hotel_cost=$driver_hotel_cost;
$driver->driver_inclusions=$driver_inclusions;
$driver->driver_exclusions=$driver_exclusions;
$driver->driver_cancel_policy=$driver_cancel_policy;
$driver->driver_terms_conditions=$driver_terms_conditions;
$driver->driver_image=$driver_image;
$driver->driver_vehicle_type=$vehicle_type;
$driver->driver_vehicle=$vehicle;
$driver->driver_vehicle_info=$vehicle_info;
$driver->driver_vehicle_images= $vehicle_images_array;
$driver->driver_created_by=$driver_created_by;
$driver->driver_status=0;
$driver->driver_approve_status=0;
$driver->driver_role="Supplier";
if($driver->save())
{
  $last_id=$driver->id;
  $driver_log=new Drivers_log;
  $driver_log->driver_id=$last_id;
  $driver_log->driver_first_name=$driver_first_name;
  $driver_log->driver_last_name=$driver_last_name;
  $driver_log->driver_contact=$driver_contact;
  $driver_log->driver_address=$driver_address;
  $driver_log->driver_supplier_id=$driver_created_by;
  $driver_log->driver_country=$driver_country;
  $driver_log->driver_city=$driver_city;
  $driver_log->driver_tours_cost=$driver_tours_cost;
  $driver_log->driver_language=$driver_language;
  $driver_log->driver_price_per_day=$driver_price_per_day;
  $driver_log->driver_description=$driver_description;
  $driver_log->operating_weekdays=$operating_weekdays;
  $driver_log->driver_blackout_dates=$driver_blackout_dates;
  $driver_log->nationality_markup_details=$nationality_markup_details;
  $driver_log->driver_tariff=$driver_tariff;
  $driver_log->driver_food_cost=$driver_food_cost;
  $driver_log->driver_hotel_cost=$driver_hotel_cost;
  $driver_log->driver_inclusions=$driver_inclusions;
  $driver_log->driver_exclusions=$driver_exclusions;
  $driver_log->driver_cancel_policy=$driver_cancel_policy;
  $driver_log->driver_terms_conditions=$driver_terms_conditions;
  $driver_log->driver_image=$driver_image;
  $driver_log->driver_vehicle_type=$vehicle_type;
  $driver_log->driver_vehicle=$vehicle;
  $driver_log->driver_vehicle_info=$vehicle_info;
  $driver_log->driver_vehicle_images= $vehicle_images_array;
  $driver_log->driver_created_by=$driver_created_by;
  $driver_log->driver_role="Supplier";
  $driver_log->driver_status=0;
  $driver_log->driver_operation_performed="INSERT";
  $driver_log->save();
  echo "success";
}
else
{
  echo "fail";
}
}

}
public function edit_driver($driver_id)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $get_drivers=Drivers::where('driver_id',$driver_id)->first();
    $languages=Languages::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    if($get_drivers)
    {
      $supplier_id=$get_drivers->driver_created_by;

      $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

      $supplier_countries=$get_supplier_countries->supplier_opr_countries;

      $countries_data=explode(',', $supplier_countries);
      $city_id=$get_drivers->driver_city;
      // $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->where(function($query) use( $city_id){
      //   $query->where('sightseeing_city_from', $city_id)->orWhereRaw('FIND_IN_SET(?,sightseeing_city_between)', [ $city_id])->orWhere('sightseeing_city_to', $city_id);
      // })->get();

      $get_cities=Cities::where('id',$city_id)->first();
      if($get_cities->name=="Tbilisi")
      {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->get();
     }
     else if($get_cities->name=="Kutaisi" || $get_cities->name=="Batumi")
     {
      $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->where(function($query) use( $city_id){
        $query->where('sightseeing_city_from', $city_id)->orWhere('sightseeing_city_to', $city_id);
      })->get();
    }
    else
    {
     $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->where(function($query) use( $city_id){
      $query->where('sightseeing_city_from', $city_id)->where('sightseeing_city_to', $city_id);
    })->get();

   }

       // $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_drivers->driver_country)->get();
   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_drivers->driver_country)->select("cities.*")->orderBy('cities.name','asc')->get();

   return view('supplier.edit-driver')->with(compact('countries','cities','get_supplier_countries','get_drivers',"countries_data",'languages','fetch_vehicle_type','fetch_sightseeing'));
 }
 else
 {
   return redirect()->route('supplier-driver');
 }




}
else
{
  return redirect()->route('supplier');
}

}

public function update_driver(Request $request)
{
  $driver_id=$request->get('driver_id');
  $driver_created_by=session()->get('travel_supplier_id');
  $driver_first_name=$request->get('driver_first_name');
  $driver_last_name=$request->get('driver_last_name');
  $driver_contact=$request->get('contact_number');
  $driver_address=$request->get('address');
  $check_drivers=Drivers::where('driver_contact',$driver_contact)->where('driver_id','!=',$driver_id)->get();
  if(count($check_drivers)>0)
  {

    echo "exist";
  }
  else
  {
   $driver_image_get=Drivers::where('driver_id',$driver_id)->first();
   $logo_data=$driver_image_get['driver_image'];

   $driver_country=$request->get('driver_country');

   $driver_city=$request->get('driver_city');

   $driver_language=$request->get('driver_language');

   $driver_language=implode(',',$driver_language);

   $driver_price_per_day=$request->get('driver_price_per_day');

   $driver_food_cost=$request->get('driver_food_cost');
   $driver_hotel_cost=$request->get('driver_hotel_cost');

   $driver_description=$request->get('description');

   $week_monday=$request->get('week_monday');

   $week_tuesday=$request->get('week_tuesday');

   $week_wednesday=$request->get('week_wednesday');

   $week_thursday=$request->get('week_thursday');

   $week_friday=$request->get('week_friday');

   $week_saturday=$request->get('week_saturday');

   $week_sunday=$request->get('week_sunday');



   $operating_weekdays=array("monday"=>$week_monday,

    "tuesday"=>$week_tuesday,

    "wednesday"=>$week_wednesday,

    "thursday"=>$week_thursday,

    "friday"=>$week_friday,

    "saturday"=>$week_saturday,

    "sunday"=>$week_sunday);



   $operating_weekdays=serialize($operating_weekdays);

   $driver_blackout_dates=$request->get('blackout_days');



   $driver_nationality=$request->get('driver_nationality');

   $driver_markup=$request->get('driver_markup');

   $driver_amount=$request->get('driver_amount');


   $vehicle_type_array=$request->get('vehicle_type');
   $vehicle_type=implode(",",$vehicle_type_array);

   $vehicle_array=$request->get('vehicle');
   $vehicle=implode(",",$vehicle_array);

   $vehicle_info_array=$request->get('vehicle_info');
   $vehicle_info=implode("---",$vehicle_info_array);
   $vehicle_already_images=$request->get('vehicle_already_images');

   $vehicle_images=$request->file('vehicle_images');  

        //multifile uploading
   $vehicle_images_array=array();


   if(count($vehicle_already_images)>0)
   {
     $vehicle_already_images_array_indexes=array_keys($vehicle_already_images); 
   }

   

   for($i=0;$i<count($vehicle_already_images);$i++)
   {
     $actual_index=$vehicle_already_images_array_indexes[$i];
     if(!empty($vehicle_already_images[$actual_index]))
     {
      for($j=0;$j<count($vehicle_already_images[$actual_index]);$j++)
      {
        $vehicle_images_array[$actual_index][]=$vehicle_already_images[$actual_index][$j];
      }
    }
    
  }

  if(count($vehicle_images)>0)
  {
   $vehicle_images_array_indexes=array_keys($vehicle_images); 
 }

 for($i=0;$i<count($vehicle_images);$i++)
 {
  $actual_index=$vehicle_images_array_indexes[$i];
  if(!empty($vehicle_images[$actual_index]))
  {
   $vehicle_image_index=$vehicle_images[$actual_index];

   if(!empty($vehicle_image_index))
   {

     foreach($vehicle_image_index as $file)
     {
      $extension=strtolower($file->getClientOriginalExtension());
      if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
      {
        $image_name=$file->getClientOriginalName();
        $image_sightseeing = "driver-vehicle-".time()."-".$image_name;
        $dir1 = 'assets/uploads/driver_vehicle_images/';
        $file->move($dir1, $image_sightseeing);
        $vehicle_images_array[$actual_index][]=$image_sightseeing;
      }
    }
  }
}

}

$vehicle_images_array=serialize($vehicle_images_array);

$nationality_markup_details=array();

for($nation_count=0;$nation_count<count($driver_nationality);$nation_count++)

{

  $nationality_markup_details[$nation_count]['driver_nationality']=$driver_nationality[$nation_count];

  $nationality_markup_details[$nation_count]['driver_markup']=$driver_markup[$nation_count];

  $nationality_markup_details[$nation_count]['driver_amount']=$driver_amount[$nation_count];

}



$nationality_markup_details=serialize($nationality_markup_details);



$driver_validity_from=$request->get('driver_validity_from');

$driver_validity_to=$request->get('driver_validity_to');

$driver_tourname=$request->get('driver_tourname');  

$driver_cost_four=$request->get('driver_cost_four');

$driver_cost_seven=$request->get('driver_cost_seven');

$driver_cost_twenty=$request->get('driver_cost_twenty');

$driver_duration=$request->get('driver_duration');





$driver_tariff=array();

for($transport_count=0;$transport_count<count($driver_tourname);$transport_count++)

{

  $driver_tariff[$transport_count]['driver_validity_from']=$driver_validity_from[$transport_count];

  $driver_tariff[$transport_count]['driver_validity_to']=$driver_validity_to[$transport_count];

  $driver_tariff[$transport_count]['driver_tourname']=$driver_tourname[$transport_count];

  $driver_tariff[$transport_count]['driver_cost_four']=$driver_cost_four[$transport_count];

  $driver_tariff[$transport_count]['driver_cost_seven']=$driver_cost_seven[$transport_count];

  $driver_tariff[$transport_count]['driver_cost_twenty']=$driver_cost_twenty[$transport_count];

  $driver_tariff[$transport_count]['driver_duration']=$driver_duration[$transport_count];



}



$driver_tariff=serialize($driver_tariff);

if($request->has('tour_name'))
{
  $tour_name=$request->get('tour_name');

  $tour_vehiclename=$request->get('tour_vehiclename');
  $tour_driver_cost=$request->get('tour_guide_cost');

  $driver_tours_cost=array();
  for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
  {

    if(!empty($tour_name[$cost_count]))
    {
      $driver_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
      $driver_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
      $driver_tours_cost[$cost_count]["tour_driver_cost"]=$tour_driver_cost[($tour_name[$cost_count]-1)];
    }
  }


}
else
{
  $driver_tours_cost=array();
}


$driver_tours_cost=serialize($driver_tours_cost);

$driver_logo_file=$request->get('driver_logo_file');

if($request->hasFile('driver_logo_file'))
{
  $driver_logo_file=$request->file('driver_logo_file');
  $extension=strtolower($request->driver_logo_file->getClientOriginalExtension());
  if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
  {
    $driver_image = "driver-".time().'.'.$request->file('driver_logo_file')->getClientOriginalExtension();

                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
    $dir1 = 'assets/uploads/driver_images/';

    $request->file('driver_logo_file')->move($dir1, $driver_image);
  }
  else
  {
    $driver_image = "";
  }
}
else
{
  $driver_image = $logo_data;
}


$driver_inclusions=$request->get('driver_inclusions');

$driver_exclusions=$request->get('driver_exclusions');

$driver_cancel_policy=$request->get('driver_cancellation');

$driver_terms_conditions=$request->get('driver_terms_conditions');

$update_array=array("driver_first_name"=>$driver_first_name,
  "driver_last_name"=>$driver_last_name,
  "driver_contact"=>$driver_contact,
  "driver_address"=>$driver_address,
  "driver_supplier_id"=>$driver_created_by,
  "driver_country"=>$driver_country,
  "driver_city"=>$driver_city,
  "driver_tours_cost"=>$driver_tours_cost,
  "driver_language"=>$driver_language,
  "driver_price_per_day"=>$driver_price_per_day,
  "driver_description"=>$driver_description,
  "operating_weekdays"=>$operating_weekdays,
  "driver_blackout_dates"=>$driver_blackout_dates,
  "nationality_markup_details"=>$nationality_markup_details,
  "driver_tariff"=>$driver_tariff,
  "driver_food_cost"=>$driver_food_cost,
  "driver_hotel_cost"=>$driver_hotel_cost,
  "driver_inclusions"=>$driver_inclusions,
  "driver_exclusions"=>$driver_exclusions,
  "driver_cancel_policy"=>$driver_cancel_policy,
  "driver_terms_conditions"=>$driver_terms_conditions,
  "driver_image"=>$driver_image,
  "driver_vehicle_type"=>$vehicle_type,
  "driver_vehicle"=>$vehicle,
  "driver_vehicle_info"=>$vehicle_info,
  "driver_vehicle_images"=>$vehicle_images_array);

$update_driver=Drivers::where('driver_id',$driver_id)->update($update_array);
if($update_driver)
{
  $driver_log=new Drivers_log;
  $driver_log->driver_id=$driver_id;
  $driver_log->driver_first_name=$driver_first_name;
  $driver_log->driver_last_name=$driver_last_name;
  $driver_log->driver_contact=$driver_contact;
  $driver_log->driver_address=$driver_address;
  $driver_log->driver_supplier_id=$driver_created_by;
  $driver_log->driver_country=$driver_country;
  $driver_log->driver_city=$driver_city;
  $driver_log->driver_tours_cost=$driver_tours_cost;
  $driver_log->driver_language=$driver_language;
  $driver_log->driver_price_per_day=$driver_price_per_day;
  $driver_log->driver_description=$driver_description;
  $driver_log->operating_weekdays=$operating_weekdays;
  $driver_log->driver_blackout_dates=$driver_blackout_dates;
  $driver_log->nationality_markup_details=$nationality_markup_details;
  $driver_log->driver_tariff=$driver_tariff;
  $driver_log->driver_food_cost=$driver_food_cost;
  $driver_log->driver_hotel_cost=$driver_hotel_cost;
  $driver_log->driver_inclusions=$driver_inclusions;
  $driver_log->driver_exclusions=$driver_exclusions;
  $driver_log->driver_cancel_policy=$driver_cancel_policy;
  $driver_log->driver_terms_conditions=$driver_terms_conditions;
  $driver_log->driver_image=$driver_image;
  $driver_log->driver_vehicle_type=$vehicle_type;
  $driver_log->driver_vehicle=$vehicle;
  $driver_log->driver_vehicle_info=$vehicle_info;
  $driver_log->driver_vehicle_images= $vehicle_images_array;
  $driver_log->driver_created_by=$driver_created_by;
  $driver_log->driver_role="Supplier";
  $driver_log->driver_operation_performed="UPDATE";
  $driver_log->save();
  echo "success";
}
else
{
  echo "fail";
}
}

}
public function driver_details($driver_id)
{
 if(session()->has('travel_supplier_id'))
 {
  $countries=Countries::where('country_status',1)->get();
  $get_drivers=Drivers::where('driver_id',$driver_id)->first();
  $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
  $languages=Languages::get();
  if($get_drivers)
  {
   $supplier_id=$get_drivers->driver_created_by;

   $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

   $supplier_countries=$get_supplier_countries->supplier_opr_countries;

   $countries_data=explode(',', $supplier_countries);

   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_drivers->driver_country)->select("cities.*")->orderBy('cities.name','asc')->get();

   return view('supplier.driver-details-view')->with(compact('countries','cities','get_supplier_countries','get_drivers',"countries_data","fetch_vehicle_type","languages"))->with('driver_id',$driver_id);
 }
 else
 {
  return redirect()->back();
}

}
else
{
  return redirect()->route('supplier');
}
}


    //  Driver Code for supplier end --


public function guide_management(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $emp_id=session()->get('travel_supplier_id');
    $get_guides=Guides::where(function($query) use($emp_id){
      $query->where('guide_supplier_id',$emp_id);
    })->orWhere(function($query1) use($emp_id){
      $query1->where('guide_created_by',$emp_id)->where('guide_role',"Supplier");
    })->get();

    return view('supplier.guide-management')->with(compact('get_guides','countries'));
  }
  else
  {
    return redirect()->route('supplier');
  }

}
public function create_guide(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $supplier_id=session()->get('travel_supplier_id');

    $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

    $supplier_countries=$get_supplier_countries->supplier_opr_countries;

    $countries_data=explode(',', $supplier_countries);
    $languages=Languages::get();
    return view('supplier.create-guide')->with(compact('countries','get_supplier_countries',"countries_data",'languages'));
  }
  else
  {
    return redirect()->route('supplier');
  }
}
public function insert_guide(Request $request)
{

  $guide_created_by=session()->get('travel_supplier_id');
  $guide_first_name=$request->get('guide_first_name');
  $guide_last_name=$request->get('guide_last_name');
  $guide_contact=$request->get('contact_number');
  $guide_address=$request->get('address');
  $check_guides=Guides::where('guide_contact',$guide_contact)->get();
  if(count($check_guides)>0)
  {

    echo "exist";
  }
  else
  {

   $guide_country=$request->get('guide_country');

   $guide_city=$request->get('guide_city');

   $guide_language=$request->get('guide_language');

   $guide_language=implode(',',$guide_language);

   $guide_price_per_day=$request->get('guide_price_per_day');
   $guide_food_cost=$request->get('guide_food_cost');
   $guide_hotel_cost=$request->get('guide_hotel_cost');

   $guide_description=$request->get('description');

   $week_monday=$request->get('week_monday');

   $week_tuesday=$request->get('week_tuesday');

   $week_wednesday=$request->get('week_wednesday');

   $week_thursday=$request->get('week_thursday');

   $week_friday=$request->get('week_friday');

   $week_saturday=$request->get('week_saturday');

   $week_sunday=$request->get('week_sunday');



   $operating_weekdays=array("monday"=>$week_monday,

    "tuesday"=>$week_tuesday,

    "wednesday"=>$week_wednesday,

    "thursday"=>$week_thursday,

    "friday"=>$week_friday,

    "saturday"=>$week_saturday,

    "sunday"=>$week_sunday);



   $operating_weekdays=serialize($operating_weekdays);

   $guide_blackout_dates=$request->get('blackout_days');



   $guide_nationality=$request->get('guide_nationality');

   $guide_markup=$request->get('guide_markup');

   $guide_amount=$request->get('guide_amount');



   $nationality_markup_details=array();

   for($nation_count=0;$nation_count<count($guide_nationality);$nation_count++)

   {

    $nationality_markup_details[$nation_count]['guide_nationality']=$guide_nationality[$nation_count];

    $nationality_markup_details[$nation_count]['guide_markup']=$guide_markup[$nation_count];

    $nationality_markup_details[$nation_count]['guide_amount']=$guide_amount[$nation_count];

  }



  $nationality_markup_details=serialize($nationality_markup_details);



  $guide_validity_from=$request->get('guide_validity_from');

  $guide_validity_to=$request->get('guide_validity_to');

  $guide_tourname=$request->get('guide_tourname');  

  $guide_cost_four=$request->get('guide_cost_four');

  $guide_cost_seven=$request->get('guide_cost_seven');

  $guide_cost_twenty=$request->get('guide_cost_twenty');

  $guide_duration=$request->get('guide_duration');





  $guide_tariff=array();

  for($transport_count=0;$transport_count<count($guide_tourname);$transport_count++)

  {

    $guide_tariff[$transport_count]['guide_validity_from']=$guide_validity_from[$transport_count];

    $guide_tariff[$transport_count]['guide_validity_to']=$guide_validity_to[$transport_count];

    $guide_tariff[$transport_count]['guide_tourname']=$guide_tourname[$transport_count];

    $guide_tariff[$transport_count]['guide_cost_four']=$guide_cost_four[$transport_count];

    $guide_tariff[$transport_count]['guide_cost_seven']=$guide_cost_seven[$transport_count];

    $guide_tariff[$transport_count]['guide_cost_twenty']=$guide_cost_twenty[$transport_count];

    $guide_tariff[$transport_count]['guide_duration']=$guide_duration[$transport_count];



  }



  $guide_tariff=serialize($guide_tariff);


  if($request->has('tour_name'))
  {
    $tour_name=$request->get('tour_name');

    $tour_vehiclename=$request->get('tour_vehiclename');
    $tour_guide_cost=$request->get('tour_guide_cost');

    $guide_tours_cost=array();
    for($cost_count=0;$cost_count<count($tour_name);$cost_count++)

    {

      $guide_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
      $guide_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
      $guide_tours_cost[$cost_count]["tour_guide_cost"]=$tour_guide_cost[($tour_name[$cost_count]-1)];
    }


  }
  else
  {
    $guide_tours_cost=array();
  }


  $guide_tours_cost=serialize($guide_tours_cost);



  if($request->has('airport_name'))
  {
   $airport_name=$request->get('airport_name');
   $airport_vehicle_name=$request->get('airport_vehicle_name');
   $airport_guide_inside_cost=$request->get('airport_guide_inside_cost');
   $airport_guide_outside_cost=$request->get('airport_guide_outside_cost');
   $guide_airports_cost=array();
   for($cost_count=0;$cost_count<count($airport_name);$cost_count++)
   {
    $guide_airports_cost[$cost_count]["airport_name"]=$airport_name[$cost_count];
    $guide_airports_cost[$cost_count]["airport_vehicle_name"]=$airport_vehicle_name[($airport_name[$cost_count]-1)];
    $guide_airports_cost[$cost_count]["airport_guide_inside_cost"]=$airport_guide_inside_cost[($airport_name[$cost_count]-1)];
    $guide_airports_cost[$cost_count]["airport_guide_outside_cost"]=$airport_guide_outside_cost[($airport_name[$cost_count]-1)];

  }

}
else
{
  $guide_airports_cost=array();
}

$guide_airports_cost=serialize($guide_airports_cost);



$guide_logo_file=$request->get('guide_logo_file');



if($request->hasFile('guide_logo_file'))

{

  $guide_logo_file=$request->file('guide_logo_file');

  $extension=strtolower($request->guide_logo_file->getClientOriginalExtension());

  if($extension=="png" || $extension=="jpg" || $extension=="jpeg")

  {

    $guide_image = "guide-".time().'.'.$request->file('guide_logo_file')->getClientOriginalExtension();



                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);

    $dir1 = 'assets/uploads/guide_images/';



    $request->file('guide_logo_file')->move($dir1, $guide_image);

  }

  else

  {

    $guide_image = "";

  }

}

else

{

  $guide_image = "";

}



$guide_inclusions=$request->get('guide_inclusions');

$guide_exclusions=$request->get('guide_exclusions');

$guide_cancel_policy=$request->get('guide_cancellation');

$guide_terms_conditions=$request->get('guide_terms_conditions');

$guide=new Guides;
$guide->guide_first_name=$guide_first_name;
$guide->guide_last_name=$guide_last_name;
$guide->guide_contact=$guide_contact;
$guide->guide_address=$guide_address;
$guide->guide_supplier_id=$guide_created_by;
$guide->guide_country=$guide_country;
$guide->guide_city=$guide_city;
$guide->guide_tours_cost=$guide_tours_cost;
$guide->guide_airports_cost=$guide_airports_cost;
$guide->guide_language=$guide_language;
$guide->guide_price_per_day=$guide_price_per_day;
$guide->guide_description=$guide_description;
$guide->operating_weekdays=$operating_weekdays;
$guide->guide_blackout_dates=$guide_blackout_dates;
$guide->nationality_markup_details=$nationality_markup_details;
$guide->guide_tariff=$guide_tariff;
$guide->guide_food_cost=$guide_food_cost;
$guide->guide_hotel_cost=$guide_hotel_cost;
$guide->guide_inclusions=$guide_inclusions;
$guide->guide_exclusions=$guide_exclusions;
$guide->guide_cancel_policy=$guide_cancel_policy;
$guide->guide_terms_conditions=$guide_terms_conditions;
$guide->guide_image=$guide_image;
$guide->guide_created_by=$guide_created_by;
$guide->guide_status=0;
$guide->guide_approve_status=0;
$guide->guide_role="Supplier";
if($guide->save())
{
  $last_id=$guide->id;
  $guide_log=new Guides_log;
  $guide_log->guide_id=$last_id;
  $guide_log->guide_first_name=$guide_first_name;
  $guide_log->guide_last_name=$guide_last_name;
  $guide_log->guide_contact=$guide_contact;
  $guide_log->guide_address=$guide_address;
  $guide_log->guide_supplier_id=$guide_created_by;
  $guide_log->guide_country=$guide_country;
  $guide_log->guide_city=$guide_city;
  $guide_log->guide_tours_cost=$guide_tours_cost;
  $guide_log->guide_airports_cost=$guide_airports_cost;
  $guide_log->guide_language=$guide_language;
  $guide_log->guide_price_per_day=$guide_price_per_day;
  $guide_log->guide_description=$guide_description;
  $guide_log->operating_weekdays=$operating_weekdays;
  $guide_log->guide_blackout_dates=$guide_blackout_dates;
  $guide_log->nationality_markup_details=$nationality_markup_details;
  $guide_log->guide_tariff=$guide_tariff;
  $guide_log->guide_food_cost=$guide_food_cost;
  $guide_log->guide_hotel_cost=$guide_hotel_cost;
  $guide_log->guide_inclusions=$guide_inclusions;
  $guide_log->guide_exclusions=$guide_exclusions;
  $guide_log->guide_cancel_policy=$guide_cancel_policy;
  $guide_log->guide_terms_conditions=$guide_terms_conditions;
  $guide_log->guide_image=$guide_image;
  $guide_log->guide_created_by=$guide_created_by;
  $guide_log->guide_status=0;
  $guide_log->guide_role="Supplier";
  $guide_log->guide_operation_performed="INSERT";
  $guide_log->save();
  echo "success";
}
else
{
  echo "fail";
}
}

}
public function edit_guide($guide_id)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $get_guides=Guides::where('guide_id',$guide_id)->first();
    $languages=Languages::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    if($get_guides)
    {
      $supplier_id=$get_guides->guide_created_by;

      $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

      $supplier_countries=$get_supplier_countries->supplier_opr_countries;

      $countries_data=explode(',', $supplier_countries);
      $city_id=$get_guides->guide_city;
      // $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->where(function($query) use( $city_id){
      //   $query->where('sightseeing_city_from', $city_id)->orWhereRaw('FIND_IN_SET(?,sightseeing_city_between)', [ $city_id])->orWhere('sightseeing_city_to', $city_id);
      // })->get();

      $get_cities=Cities::where('id',$city_id)->first();
      if($get_cities->name=="Tbilisi")
      {
       $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->get();
     }
     else if($get_cities->name=="Kutaisi" || $get_cities->name=="Batumi")
     {
      $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->where(function($query) use( $city_id){
        $query->where('sightseeing_city_from', $city_id)->orWhere('sightseeing_city_to', $city_id);
      })->get();
    }
    else
    {
     $fetch_sightseeing=SightSeeing::where('sightseeing_country',$get_guides->guide_country)->where(function($query) use( $city_id){
      $query->where('sightseeing_city_from', $city_id)->where('sightseeing_city_to', $city_id);
    })->get();

   }

   $fetch_airports=AirportMaster::get();

   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_guides->guide_country)->select("cities.*")->orderBy('cities.name','asc')->get();

   return view('supplier.edit-guide')->with(compact('countries','cities','get_supplier_countries','get_guides',"countries_data",'languages','fetch_vehicle_type','fetch_sightseeing','fetch_airports'));
 }
 else
 {
  return redirect()->route('supplier-guide');
}




}
else
{
  return redirect()->route('supplier');
}

}

public function update_guide(Request $request)
{
  $guide_id=$request->get('guide_id');
  $guide_created_by=session()->get('travel_supplier_id');
  $guide_first_name=$request->get('guide_first_name');
  $guide_last_name=$request->get('guide_last_name');
  $guide_contact=$request->get('contact_number');
  $guide_address=$request->get('address');
  $check_guides=Guides::where('guide_contact',$guide_contact)->where('guide_id','!=',$guide_id)->get();
  if(count($check_guides)>0)
  {

    echo "exist";
  }
  else
  {
   $guide_image_get=Guides::where('guide_id',$guide_id)->first();
   $logo_data=$guide_image_get['guide_image'];

   $guide_country=$request->get('guide_country');

   $guide_city=$request->get('guide_city');

   $guide_language=$request->get('guide_language');

   $guide_language=implode(',',$guide_language);

   $guide_price_per_day=$request->get('guide_price_per_day');

   $guide_food_cost=$request->get('guide_food_cost');
   $guide_hotel_cost=$request->get('guide_hotel_cost');

   $guide_description=$request->get('description');

   $week_monday=$request->get('week_monday');

   $week_tuesday=$request->get('week_tuesday');

   $week_wednesday=$request->get('week_wednesday');

   $week_thursday=$request->get('week_thursday');

   $week_friday=$request->get('week_friday');

   $week_saturday=$request->get('week_saturday');

   $week_sunday=$request->get('week_sunday');



   $operating_weekdays=array("monday"=>$week_monday,

    "tuesday"=>$week_tuesday,

    "wednesday"=>$week_wednesday,

    "thursday"=>$week_thursday,

    "friday"=>$week_friday,

    "saturday"=>$week_saturday,

    "sunday"=>$week_sunday);



   $operating_weekdays=serialize($operating_weekdays);

   $guide_blackout_dates=$request->get('blackout_days');



   $guide_nationality=$request->get('guide_nationality');

   $guide_markup=$request->get('guide_markup');

   $guide_amount=$request->get('guide_amount');



   $nationality_markup_details=array();

   for($nation_count=0;$nation_count<count($guide_nationality);$nation_count++)

   {

    $nationality_markup_details[$nation_count]['guide_nationality']=$guide_nationality[$nation_count];

    $nationality_markup_details[$nation_count]['guide_markup']=$guide_markup[$nation_count];

    $nationality_markup_details[$nation_count]['guide_amount']=$guide_amount[$nation_count];

  }



  $nationality_markup_details=serialize($nationality_markup_details);



  $guide_validity_from=$request->get('guide_validity_from');

  $guide_validity_to=$request->get('guide_validity_to');

  $guide_tourname=$request->get('guide_tourname');  

  $guide_cost_four=$request->get('guide_cost_four');

  $guide_cost_seven=$request->get('guide_cost_seven');

  $guide_cost_twenty=$request->get('guide_cost_twenty');

  $guide_duration=$request->get('guide_duration');





  $guide_tariff=array();

  for($transport_count=0;$transport_count<count($guide_tourname);$transport_count++)

  {

    $guide_tariff[$transport_count]['guide_validity_from']=$guide_validity_from[$transport_count];

    $guide_tariff[$transport_count]['guide_validity_to']=$guide_validity_to[$transport_count];

    $guide_tariff[$transport_count]['guide_tourname']=$guide_tourname[$transport_count];

    $guide_tariff[$transport_count]['guide_cost_four']=$guide_cost_four[$transport_count];

    $guide_tariff[$transport_count]['guide_cost_seven']=$guide_cost_seven[$transport_count];

    $guide_tariff[$transport_count]['guide_cost_twenty']=$guide_cost_twenty[$transport_count];

    $guide_tariff[$transport_count]['guide_duration']=$guide_duration[$transport_count];



  }



  $guide_tariff=serialize($guide_tariff);

  if($request->has('tour_name'))
  {
    $tour_name=$request->get('tour_name');
    $tour_vehiclename=$request->get('tour_vehiclename');
    $tour_guide_cost=$request->get('tour_guide_cost');

    $guide_tours_cost=array();
    for($cost_count=0;$cost_count<count($tour_name);$cost_count++)
    {

      $guide_tours_cost[$cost_count]["tour_name"]=$tour_name[$cost_count];
      $guide_tours_cost[$cost_count]["tour_vehicle_name"]=$tour_vehiclename[($tour_name[$cost_count]-1)];
      $guide_tours_cost[$cost_count]["tour_guide_cost"]=$tour_guide_cost[($tour_name[$cost_count]-1)];
    }


  }
  else
  {
    $guide_tours_cost=array();
  }


  $guide_tours_cost=serialize($guide_tours_cost);

  if($request->has('airport_name'))
  {
    $airport_name=$request->get('airport_name');
    $airport_vehicle_name=$request->get('airport_vehicle_name');
    $airport_guide_inside_cost=$request->get('airport_guide_inside_cost');
    $airport_guide_outside_cost=$request->get('airport_guide_outside_cost');
    $guide_airports_cost=array();
    for($cost_count=0;$cost_count<count($airport_name);$cost_count++)
    {
      $guide_airports_cost[$cost_count]["airport_name"]=$airport_name[$cost_count];
      $guide_airports_cost[$cost_count]["airport_vehicle_name"]=$airport_vehicle_name[($airport_name[$cost_count]-1)];
      $guide_airports_cost[$cost_count]["airport_guide_inside_cost"]=$airport_guide_inside_cost[($airport_name[$cost_count]-1)];
      $guide_airports_cost[$cost_count]["airport_guide_outside_cost"]=$airport_guide_outside_cost[($airport_name[$cost_count]-1)];

    }

  }
  else
  {
    $guide_airports_cost=array();
  }

  $guide_airports_cost=serialize($guide_airports_cost);


  $guide_logo_file=$request->get('guide_logo_file');

  if($request->hasFile('guide_logo_file'))
  {
    $guide_logo_file=$request->file('guide_logo_file');
    $extension=strtolower($request->guide_logo_file->getClientOriginalExtension());
    if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
    {
      $guide_image = "guide-".time().'.'.$request->file('guide_logo_file')->getClientOriginalExtension();

                // request()->agent_logo->storeAs(public_path('consultant-images'), $imageName);
      $dir1 = 'assets/uploads/guide_images/';

      $request->file('guide_logo_file')->move($dir1, $guide_image);
    }
    else
    {
      $guide_image = "";
    }
  }
  else
  {
    $guide_image = $logo_data;
  }


  $guide_inclusions=$request->get('guide_inclusions');

  $guide_exclusions=$request->get('guide_exclusions');

  $guide_cancel_policy=$request->get('guide_cancellation');

  $guide_terms_conditions=$request->get('guide_terms_conditions');

  $update_array=array("guide_first_name"=>$guide_first_name,
    "guide_last_name"=>$guide_last_name,
    "guide_contact"=>$guide_contact,
    "guide_address"=>$guide_address,
    "guide_supplier_id"=>$guide_created_by,
    "guide_country"=>$guide_country,
    "guide_city"=>$guide_city,
    "guide_tours_cost"=>$guide_tours_cost,
    "guide_airports_cost"=>$guide_airports_cost,
    "guide_language"=>$guide_language,
    "guide_price_per_day"=>$guide_price_per_day,
    "guide_description"=>$guide_description,
    "operating_weekdays"=>$operating_weekdays,
    "guide_blackout_dates"=>$guide_blackout_dates,
    "nationality_markup_details"=>$nationality_markup_details,
    "guide_tariff"=>$guide_tariff,
    "guide_food_cost"=>$guide_food_cost,
    "guide_hotel_cost"=>$guide_hotel_cost,
    "guide_inclusions"=>$guide_inclusions,
    "guide_exclusions"=>$guide_exclusions,
    "guide_cancel_policy"=>$guide_cancel_policy,
    "guide_terms_conditions"=>$guide_terms_conditions,
    "guide_image"=>$guide_image);

  $update_guide=Guides::where('guide_id',$guide_id)->update($update_array);
  if($update_guide)
  {
    $guide_log=new Guides_log;
    $guide_log->guide_id=$guide_id;
    $guide_log->guide_first_name=$guide_first_name;
    $guide_log->guide_last_name=$guide_last_name;
    $guide_log->guide_contact=$guide_contact;
    $guide_log->guide_address=$guide_address;
    $guide_log->guide_supplier_id=$guide_created_by;
    $guide_log->guide_country=$guide_country;
    $guide_log->guide_city=$guide_city;
    $guide_log->guide_tours_cost=$guide_tours_cost;
    $guide_log->guide_airports_cost=$guide_airports_cost;
    $guide_log->guide_language=$guide_language;
    $guide_log->guide_price_per_day=$guide_price_per_day;
    $guide_log->guide_description=$guide_description;
    $guide_log->operating_weekdays=$operating_weekdays;
    $guide_log->guide_blackout_dates=$guide_blackout_dates;
    $guide_log->nationality_markup_details=$nationality_markup_details;
    $guide_log->guide_tariff=$guide_tariff;
    $guide_log->guide_food_cost=$guide_food_cost;
    $guide_log->guide_hotel_cost=$guide_hotel_cost;
    $guide_log->guide_inclusions=$guide_inclusions;
    $guide_log->guide_exclusions=$guide_exclusions;
    $guide_log->guide_cancel_policy=$guide_cancel_policy;
    $guide_log->guide_terms_conditions=$guide_terms_conditions;
    $guide_log->guide_image=$guide_image;
    $guide_log->guide_created_by=$guide_created_by;
    $guide_log->guide_role="Supplier";
    $guide_log->guide_operation_performed="UPDATE";
    $guide_log->save();
    echo "success";
  }
  else
  {
    echo "fail";
  }
}

}

public function guide_details($guide_id)
{
 if(session()->has('travel_supplier_id'))
 {
  $countries=Countries::where('country_status',1)->get();
  $get_guides=Guides::where('guide_id',$guide_id)->first();
  $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
  $languages=Languages::get();
  if($get_guides)
  {
   $supplier_id=$get_guides->guide_created_by;

   $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

   $supplier_countries=$get_supplier_countries->supplier_opr_countries;

   $countries_data=explode(',', $supplier_countries);

   $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_guides->guide_country)->select("cities.*")->orderBy('cities.name','asc')->get();

   return view('supplier.guide-details-view')->with(compact('countries','cities','get_supplier_countries','get_guides',"countries_data","fetch_vehicle_type","languages"))->with('guide_id',$guide_id);
 }
 else
 {
  return redirect()->back();
}

}
else
{
  return redirect()->route('supplier');
}
}


public function supplier_transfer(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $supplier_id=session()->get('travel_supplier_id');  
    $countries=Countries::where('country_status',1)->get();
    $cities=Cities::get();
    $emp_id=session()->get('travel_supplier_id');
    $get_transfer=Transfers::where('transfer_created_by',$supplier_id)->where('transfer_role','Supplier')->get();
    $get_suppliers=Suppliers::get();
    return view('supplier.supplier-transfer')->with(compact('countries','cities','get_suppliers','get_transfer'));
  }
  else
  {
    return redirect()->route('supplier');
  }
}
public function supplier_create_transfer(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
   $countries=Countries::where('country_status',1)->get();
   $currency=Currency::get();
   $suppliers=Suppliers::get();
   $supplier_id=session()->get('travel_supplier_id');
   $supplier_name=session()->get('travel_supplier_fullame');
   $fetch_airports=AirportMaster::get();
   $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
   return view('supplier.create-transfer')->with(compact('countries','currency','supplier_id','supplier_name','fetch_airports','fetch_vehicle_type'));
 }
 else
 {
  return redirect()->route('supplier');
}
}
public function supplier_edit_transfer($transfer_id)
{
  if(session()->has('travel_supplier_id'))
  {
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::get();
    $suppliers=Suppliers::get();
    $fetch_vehicles=Vehicles::get();
    $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
    $get_transfer=Transfers::where('transfer_id',$transfer_id)->where('transfer_role','Supplier')->first();
    if($get_transfer)
    {
      $supplier_id=$get_transfer->supplier_id;
      $get_transfer_details=TransferDetails::where('transfer_id',$transfer_id)->get();
      $fetch_airports=AirportMaster::get();
      $get_supplier_countries=Suppliers::where('supplier_id',$supplier_id)->first();

      $supplier_countries=$get_supplier_countries->supplier_opr_countries;

      $countries_data=explode(',', $supplier_countries);

      $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_transfer->transfer_country)->where('city_status',1)->select("cities.*")->orderBy('cities.name','asc')->get();

      return view('supplier.edit-transfer')->with(compact('countries','currency','fetch_cities','suppliers','get_transfer',"countries_data","get_supplier_countries","get_transfer_details","fetch_airports",'fetch_vehicle_type','fetch_vehicles'));
    }
    else
    {
     return redirect()->route('supplier-transfer');
   }

 }
 else
 {
  return redirect()->route('supplier');
}
}
public function supplier_transfer_details($transfer_id)
{

 if(session()->has('travel_supplier_id'))
 {
  $countries=Countries::where('country_status',1)->get();
  $suppliers=Suppliers::where('supplier_status',1)->get();
  $emp_id=session()->get('travel_supplier_id');
  $fetch_vehicles=Vehicles::get();
  $fetch_vehicle_type=VehicleType::where('vehicle_type_status',1)->get();
  $get_transfer=Transfers::where('transfer_id',$transfer_id)->first();


  if(!empty($get_transfer))
  {
    $get_transfer_details=TransferDetails::where('transfer_id',$transfer_id)->get();
    $fetch_airports=AirportMaster::get();
    $supplier_id=$get_transfer->supplier_id;
    $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
    $supplier_countries=$get_supplier_countries->supplier_opr_countries;
    $countries_data=explode(',', $supplier_countries);
    $fetch_cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_transfer->transfer_country)->select("cities.*")->orderBy('cities.name','asc')->get();
    return view('supplier.transfer-details-view')->with(compact('countries','suppliers','fetch_cities','suppliers','get_transfer','countries_data','get_transfer_details','fetch_airports','fetch_vehicle_type','fetch_vehicles'));
  }
  else
  {
    return redirect()->back();
  }
}
else
{
  return redirect()->route('supplier');
}

}




public function create_restaurant(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $supplier_id=session()->get('travel_supplier_id');
    $supplier_name=session()->get('travel_supplier_fullame');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $fetch_restaurant_type=RestaurantType::where('restaurant_type_status',1)->get();
    return view('supplier.create-restaurant')->with(compact('countries','currency','fetch_restaurant_type','supplier_id','supplier_name'));
  }
  else
  {
   return redirect()->route('supplier');
 }
}

public function edit_restaurant($restaurant_id)
{
  if(session()->has('travel_supplier_id'))
  {
    $supplier_id=session()->get('travel_supplier_id');
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $fetch_restaurant_type=RestaurantType::where('restaurant_type_status',1)->get();
    
    $get_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->where('supplier_id', $supplier_id)->first();


    if(!empty($get_restaurant))
    {
      $supplier_id=$get_restaurant->supplier_id;
      $get_supplier_countries=Suppliers::where('supplier_status',1)->where('supplier_id',$supplier_id)->first();
      $supplier_countries=$get_supplier_countries->supplier_opr_countries;
      $countries_data=explode(',', $supplier_countries);
      $cities=Cities::join("states","states.id","=","cities.state_id")->where("states.country_id", $get_restaurant->restaurant_country)->select("cities.*")->orderBy('cities.name','asc')->get();
      return view('supplier.edit-restaurant')->with(compact('countries','currency','cities','get_restaurant',"countries_data","fetch_restaurant_type","get_supplier_countries"));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
   return redirect()->route('supplier');
 }
}


public function restaurant_management(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {
  $supplier_id=session()->get('travel_supplier_id');  
  $get_restaurants=Restaurants::where('supplier_id',$supplier_id)->get();
  return view('supplier.restaurant-management')->with(compact('get_restaurants'));
}
else
{
 return redirect()->route('supplier');
}
}

public function restaurant_details($restaurant_id)
{
  if(session()->has('travel_supplier_id'))
  {
     $supplier_id=session()->get('travel_supplier_id');  
    $countries=Countries::where('country_status',1)->get();
    $currency=Currency::orderBy('code','asc')->get();
    $fetch_restaurant_type=RestaurantType::where('restaurant_type_status',1)->get();
    
    $get_restaurant=Restaurants::where('restaurant_id',$restaurant_id)->where('supplier_id',$supplier_id)->first();
    if(!empty($get_restaurant))
    {
      return view('supplier.restaurant-detail-view')->with(compact('countries','currency','get_restaurant',"fetch_restaurant_type"));
    }
    else
    {
      return redirect()->back();
    }
  }
  else
  {
   return redirect()->route('supplier');
 }
}

public function food_management(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {
  $supplier_id=session()->get('travel_supplier_id');
  $get_restaurants=Restaurants::where('supplier_id',$supplier_id)->select('restaurant_id')->get();
  $get_restaurants_array=array_column($get_restaurants->toArray(), 'restaurant_id');
  $get_food=RestaurantFood::whereIn("restaurant_id_fk",$get_restaurants_array)->orderBy('restaurant_food_id','asc')->get();
  return view('supplier.food-management')->with(compact('get_food'));
}
else
{
 return redirect()->route('supplier');
}
}


public function create_food(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
    $supplier_id=session()->get('travel_supplier_id');
    $fetch_restaurants=Restaurants::where('supplier_id',$supplier_id)->get();
    $menu_categories=RestaurantMenuCategory::get();
    return view('supplier.create-food')->with(compact('fetch_restaurants','menu_categories'));
  }
  else
  {
   return redirect()->route('supplier');
 }
}

public function edit_food($food_id)
{
  if(session()->has('travel_supplier_id'))
  {

    $supplier_id=session()->get('travel_supplier_id');
    
    $get_food=RestaurantFood::where('restaurant_food_id',$food_id)->first();
    if(!empty($get_food))
    {
      $check_restaurant=Restaurants::where('restaurant_id',$get_food->restaurant_id_fk)->where('supplier_id',$supplier_id)->first();
      if(!empty($check_restaurant))
      {
       $fetch_restaurants=Restaurants::where('supplier_id',$supplier_id)->get();
       $menu_categories=RestaurantMenuCategory::get();

       return view('supplier.edit-food')->with(compact('get_food','fetch_restaurants','menu_categories'));
     }
     else
     {
      return redirect()->back();
    }

    
  }
  else
  {
    return redirect()->back();
  }
}
else
{
  return redirect()->route('supplier');
}
}

public function food_details($food_id)
{
  if(session()->has('travel_supplier_id'))
  {

    $supplier_id=session()->get('travel_supplier_id');
    
    $get_food=RestaurantFood::where('restaurant_food_id',$food_id)->where('food_created_by',$supplier_id)->where('food_role','=','Supplier')->first();
    if(!empty($get_food))
    {
      $check_restaurant=Restaurants::where('restaurant_id',$get_food->restaurant_id_fk)->where('supplier_id',$supplier_id)->first();
      if(!empty($check_restaurant))
      {
       $fetch_restaurants=Restaurants::where('supplier_id',$supplier_id)->get();
       $menu_categories=RestaurantMenuCategory::get();

       return view('supplier.food-detail-view')->with(compact('get_food','fetch_restaurants','menu_categories'));
     }
     else
     {
      return redirect()->back();
    }

    
  }
  else
  {
    return redirect()->back();
  }
}
else
{
  return redirect()->route('supplier');
}
}


public function supplier_bookings()
{
  if(session()->has('travel_supplier_id'))
  {
    $supplier_id=session()->get('travel_supplier_id');
    $fetch_bookings=Bookings::whereRaw("find_in_set('".$supplier_id."',booking_supplier_id)")->get();
    $fetch_activity=Activities::where('activity_status',1)->get();
    return view('supplier.bookings')->with(compact('fetch_bookings'));
  }
  else
  {
    return redirect()->route('supplier');
  }

}

public function supplier_approve_booking(Request $request)
{
  $action_perform=$request->get('action_perform');
  $booking_id=$request->get('booking_id');
  $remarks=$request->get('remarks');

  if($action_perform=="approve")
  {
   $check_booking=Bookings::where('booking_id',$booking_id)->first();
   if(!empty($check_booking))
   {

     if($check_booking['booking_status']==2)
     {
      echo "cancel";
    }
    else
    {
     $update_booking=Bookings::where('booking_id',$booking_id)->update(["booking_supplier_status"=>1,"booking_supplier_message"=>$remarks]);

     if($update_booking)
     {
      $booking_id=$check_booking['booking_sep_id'];
      $booking_type=$check_booking['booking_type'];

      $supplier_id=session()->get('travel_supplier_id');
      $fetch_admin=Users::where('users_pid',0)->first();

              // $booking_supplier_id=$check_booking['booking_supplier_id'];

      $fetch_supplier=Suppliers::where('supplier_id',$supplier_id)->first();

      $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' has been confirmed by supplier '.$fetch_supplier->supplier_name.' with remarks <b>'.$remarks.'</b></p>';
      $data_admin= array(
        'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
        'email' =>$fetch_admin->users_email
      );
      Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
        $m->from('sarbjitphp@netwebtechnologies.com', 'Traveldoor');
        $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' CONFIRMED ON PORTAL');
      });



      echo "success";
    }
    else
    {
      echo "fail";
    }
  }
  
}
}
else
{
 $check_booking=Bookings::where('booking_id',$booking_id)->first();
 if(!empty($check_booking))
 {

   if($check_booking['booking_status']==2)
   {
    echo "cancel";
  }
  else
  {
   $update_booking=Bookings::where('booking_id',$booking_id)->update(["booking_supplier_status"=>2,"booking_supplier_message"=>$remarks]);

   if($update_booking)
   {
     $booking_id=$check_booking['booking_sep_id'];
     $booking_type=$check_booking['booking_type'];

     $supplier_id=session()->get('travel_supplier_id');
     $fetch_admin=Users::where('users_pid',0)->first();

              // $booking_supplier_id=$check_booking['booking_supplier_id'];

     $fetch_supplier=Suppliers::where('supplier_id',$supplier_id)->first();

     $htmldata='<p>Dear Admin,</p><p>'.ucfirst($booking_type).' booked with booking id '.$booking_id.' is rejected by supplier '.$fetch_supplier->supplier_name.' with remarks <b>'.$remarks.'</b></p>';
     $data_admin= array(
      'name' => $fetch_admin->users_fname." ".$fetch_admin->users_lname,
      'email' =>$fetch_admin->users_email
    );
     Mail::send('email.htmldata', ['htmldata' => $htmldata], function ($m) use ($data_admin,$booking_type) {
      $m->from('sarbjitphp@netwebtechnologies.com', 'Traveldoor');
      $m->to($data_admin['email'], $data_admin['name'])->subject(strtoupper($booking_type).' REJECTED ON PORTAL');
    });
     echo "success";
   }
   else
   {
    echo "fail";
  }
}

}

}
}


public function supplier_own_wallet(Request $request)
{
 if(session()->has('travel_supplier_id'))
 {
  $supplier_id=session()->get('travel_supplier_id');
  $withdaw_yes=1;
  $month_numeric=date('m');
  $yearname=date('Y');
  $get_wallet=SupplierWallet::where('supp_wallet_supplier_id',$supplier_id)->orderBy('supp_wallet_id','desc')->paginate(10);
  $get_commission_total=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_credit_amount), 0)-COALESCE(sum(supp_wallet_debit_amount), 0)) as total_wallet_amount"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_status',1)->groupBy('supp_wallet_supplier_id')->first();

  $get_commission_total_withdraw=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_credit_amount), 0)-COALESCE(sum(supp_wallet_debit_amount), 0)) as total_wallet_amount"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_status','!=',2)->groupBy('supp_wallet_supplier_id')->first();

  $get_commission_credited_all=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_credit_amount), 0)) as amount_credited"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_status',1)->groupBy('supp_wallet_supplier_id')->first();

  $get_commission_withdraw_all=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_debit_amount), 0)) as amount_withdrawn"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_remarks','Money Withdrawn')->where('supp_wallet_status',1)->groupBy('supp_wallet_supplier_id')->first();

  $get_commission_deducted_all=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_debit_amount), 0)) as amount_deducted"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_remarks','Money Deducted')->where('supp_wallet_status',1)->groupBy('supp_wallet_supplier_id')->first();


  $get_commission_credited_month=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_credit_amount), 0)) as amount_credited"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_month',$month_numeric)->where('supp_wallet_year',$yearname)->where('supp_wallet_status',1)->groupBy('supp_wallet_supplier_id')->first();

  $get_commission_withdraw_month=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_debit_amount), 0)) as amount_withdrawn"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_month',$month_numeric)->where('supp_wallet_year',$yearname)->where('supp_wallet_remarks','Money Withdrawn')->where('supp_wallet_status',1)->groupBy('supp_wallet_supplier_id')->first();

  $get_commission_deducted_month=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_debit_amount), 0)) as amount_deducted"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_month',$month_numeric)->where('supp_wallet_year',$yearname)->where('supp_wallet_remarks','Money Deducted')->where('supp_wallet_status',1)->groupBy('supp_wallet_supplier_id')->first();


  if(!empty($get_commission_total))
    $total_amount=$get_commission_total->total_wallet_amount;
  else
   $total_amount=0;  

 if(!empty($get_commission_total_withdraw))
  $total_amount_withdraw=$get_commission_total_withdraw->total_wallet_amount;
else
 $total_amount_withdraw=0;   

if(!empty($get_commission_credited_all))
  $total_amount_credited_all=$get_commission_credited_all->amount_credited;
else
 $total_amount_credited_all=0;

if(!empty($get_commission_withdraw_all))
  $total_amount_withdraw_all=$get_commission_withdraw_all->amount_withdrawn;
else
 $total_amount_withdraw_all=0;

if(!empty($get_commission_deducted_all))
  $total_amount_deducted_all=$get_commission_deducted_all->amount_deducted;
else
 $total_amount_deducted_all=0;


if(!empty($get_commission_credited_month))
  $total_amount_credited_month=$get_commission_credited_month->amount_credited;
else
 $total_amount_credited_month=0;

if(!empty($get_commission_withdraw_month))
  $total_amount_withdraw_month=$get_commission_withdraw_month->amount_withdrawn;
else
 $total_amount_withdraw_month=0;

if(!empty($get_commission_deducted_month))
  $total_amount_deducted_month=$get_commission_deducted_month->amount_deducted;
else
 $total_amount_deducted_month=0;





return view('supplier.my-wallet')->with(compact('get_wallet','total_amount','total_amount_credited_all','total_amount_withdraw_all','total_amount_deducted_all','total_amount_credited_month','total_amount_withdraw_month','total_amount_deducted_month','total_amount_withdraw','withdaw_yes'));
}
else
{
  return redirect()->route('supplier');
}

}

public function withdraw_wallet(Request $request)
{
  if(session()->has('travel_supplier_id'))
  {
   $supplier_id=session()->get('travel_supplier_id');
   $withdraw_amount=$request->get('withdraw_amount');
   $month_numeric=date('m');
   $yearname=date('Y');
   
   $get_wallet=SupplierWallet::where('supp_wallet_supplier_id',$supplier_id)->orderBy('supp_wallet_id','desc')->paginate(10);
   $get_commission_total=SupplierWallet::select(\DB::raw("(COALESCE(sum(supp_wallet_credit_amount), 0)-COALESCE(sum(supp_wallet_debit_amount), 0)) as total_wallet_amount"))->where('supp_wallet_supplier_id',$supplier_id)->where('supp_wallet_status','!=',2)->groupBy('supp_wallet_supplier_id')->first();

   if(!empty($get_commission_total))
    $total_amount=$get_commission_total->total_wallet_amount;
  else
   $total_amount=0;   
 if($withdraw_amount>$total_amount)
 {
  echo "exceed_amount";
}
else if($withdraw_amount<=0)
{
 echo "less_amount";
}
else if($withdraw_amount<=$total_amount)
{
  $insert_wallet=new SupplierWallet;
  $insert_wallet->supp_wallet_supplier_id=$supplier_id;
  $insert_wallet->supp_wallet_debit_amount=round($withdraw_amount);
  $insert_wallet->supp_wallet_month=$month_numeric;
  $insert_wallet->supp_wallet_year=$yearname;
  $insert_wallet->supp_wallet_date=date('Y-m-d');
  $insert_wallet->supp_wallet_date=date('H:i:s');
  $insert_wallet->supp_wallet_remarks="Money Withdrawn";
  $insert_wallet->supp_wallet_status=0;
  if($insert_wallet->save())
  {
    echo "success";
  }
  else
  {
    echo "fail";
  }
}
}
else
{
  echo "invalid_user";
}

}


}

