<?php
namespace App\Http\Controllers;
use App\Users;
use App\Countries;
use App\Cities;
use App\Currency;
use App\UserRights;
use App\Agents;
use App\Agents_log;
use App\Suppliers;
use App\Activities;
use App\Transport;
use App\Hotels;
use App\HotelRooms;
use App\HotelRoomSeasons;
use App\HotelRoomSeasonOccupancy;
use App\SavedItinerary;
use App\Bookings;
use App\Guides;
use App\Drivers;
use App\GuideExpense;
use App\Languages;
use App\SightSeeing;
use App\Vehicles;
use App\VehicleType;
use App\HotelType;
use App\TourType;
use App\ActivityType;
use App\AirportMaster;
use App\Amenities;
use App\SubAmenities;
use App\Transfers;
use App\TransferDetails;
use App\AgentSavedItinerary;
use App\HotelMeal;
use App\AgentWallet;
use App\BookingInstallment;
use App\Restaurants;
use App\RestaurantFood;
use App\RestaurantMenuCategory;
use App\RestaurantType;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use App\OnlinePayment;
use PDF;
use Session;
use Cookie;
use Mail;
class CustomRestaurantItinerary extends Controller
{
private function agent_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_service_markup;
    }
    return $markup;
  }

  private function agent_own_markup()
  {
    $agent_id=session()->get('travel_agent_id');
    $fetch_agent_markup=Agents::where('agent_id',$agent_id)->first();
    $markup="";
    if($fetch_agent_markup)
    {
      $markup=$fetch_agent_markup->agent_own_service_markup;
    }
    return $markup;
  }
  public function restaurant_itinerary()
  {
  	if(session()->has('travel_agent_id'))
    {
      $countries=Countries::where('country_status',1)->get();
      $agent_id=session()->get('travel_agent_id');
      return view('agent.restaurant-itinerary')->with(compact('countries','agent_id'));
    }
    else
    {
      return redirect()->route('agent');
    }
  }
  public function restaurant_itinerary_details_view(Request $request)
  {
  		// echo "<pre>";
  		// print_r($request->all());
  		// die();	
  		$markup=$this->agent_markup();
    $itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
    $own_markup=$this->agent_own_markup();
    $own_itinerary_markup=0;
    if($markup!="")
    {
      $get_all_services=explode("///",$own_markup);
      for($markup=0;$markup<count($get_all_services);$markup++)
      {
        $get_individual_service=explode("---",$get_all_services[$markup]);
        if($get_individual_service[0]=="itinerary")
        {
          if($get_individual_service[1]!="")
          {
            $own_itinerary_markup=$get_individual_service[1];
          }
          break;
        }
      }
    }
  		$itinerary_date_from=$request->get('select_date');
  		$no_of_days=$request->get('no_of_days');
  		$countries=Countries::where('country_status',1)->get();
  		$selected_country=$request->get('restaurant_country');
  		$selected_city=$request->get('restaurant_city');
  	  return view('agent.restaurant_itinerary_details')->with(compact('countries','agent_id','itinerary_date_from','no_of_days','selected_country','selected_city'))->with('markup',$itinerary_markup)->with('own_markup',$own_itinerary_markup)->with('no_of_days',$no_of_days)->with('itinerary_date_from',$itinerary_date_from);
  }


}