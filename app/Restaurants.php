<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Restaurants extends Model
{
   protected $table="restaurants";

   public function getRestaurantType()
   {
   	 return $this->belongsTo('App\RestaurantType', 'restaurant_type','restaurant_type_id');
   }
    public function getSupplier()
   {
   	return $this->belongsTo('App\Suppliers', 'supplier_id','supplier_id');
   }
   public function getCity()
   {
   	return $this->belongsTo('App\Cities', 'restaurant_city','id');
   }
   public function getCountry()
   {
   	return $this->belongsTo('App\Countries', 'restaurant_country','country_id');
   }
}
