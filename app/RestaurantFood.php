<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantFood extends Model
{
   protected $table="restaurants_food";

   public function getMenuCategory()
   {  
   	 return $this->belongsTo('App\RestaurantMenuCategory', 'menu_category_id_fk','restaurant_menu_category_id');
   }

   public function getRestaurant()
   {  
       return $this->belongsTo('App\Restaurants', 'restaurant_id_fk','restaurant_id');
   }
}
