<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drivers extends Model
{
    protected $table="drivers";

      public function getSupplier()
   {
   	return $this->belongsTo('App\Suppliers', 'driver_supplier_id','supplier_id');
   }
    public function getCity()
   {
   	return $this->belongsTo('App\Cities', 'driver_city','id');
   }
   public function getCountry()
   {
   	return $this->belongsTo('App\Countries', 'driver_country','country_id');
   }
}