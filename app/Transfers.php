<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transfers extends Model
{
     protected $table="transfers";

     public function vehicleType()
     {
     	return $this->belongsTo("App\VehicleType","transfer_vehicle_type","vehicle_type_id");
     }

      public function vehicle()
     {
     	return $this->belongsTo("App\Vehicles","transfer_vehicle","vehicle_id");
     }

      public function getSupplier()
   {
    return $this->belongsTo('App\Suppliers', 'supplier_id','supplier_id');
   }

       public function country()
     {
     	return $this->belongsTo("App\Countries","transfer_country","country_id");
     }
}
