<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentSavedItinerary extends Model
{
    protected $table="agent_saved_itineraries";
}
