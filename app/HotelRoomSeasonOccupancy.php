<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HotelRoomSeasonOccupancy extends Model
{
   protected $table="hotel_room_season_occupancy_price";
   protected $primaryKey = 'hotel_room_occupancy_id';
}
