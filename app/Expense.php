<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $table="expenses";

    public function get_expense_category()
    {
    	return $this->belongsTo('App\IncomeExpenseCategory','expense_category_id','expense_category_id');
    }
}
