<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedItineraryLog extends Model
{
    protected $table="saved_itineraries_log";
}
