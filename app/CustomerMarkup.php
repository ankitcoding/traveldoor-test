<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerMarkup extends Model
{
    protected $table="customer_markup";
}
