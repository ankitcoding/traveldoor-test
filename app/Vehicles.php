<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    protected $table="vehicle";

    public function vehicle_type()
    {
    	return $this->belongsto('App\VehicleType','vehicle_type_id','vehicle_type_id');
    }
}
