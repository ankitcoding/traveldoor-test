<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantMenuCategory extends Model
{
    protected $table="restaurant_menu_category";
}
