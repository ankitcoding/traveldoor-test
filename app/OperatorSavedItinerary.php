<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OperatorSavedItinerary extends Model
{
    protected $table="operator_saved_itineraries";
}
