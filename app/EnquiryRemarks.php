<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquiryRemarks extends Model
{
    protected $table="enquiry_remarks";
    
    public function getUser()
    {
        return $this->belongsTo('App\Users', 'given_by', 'users_id');
    }
}
