<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingTargetCommission extends Model
{
    protected $table="setting_target_commission";
}
