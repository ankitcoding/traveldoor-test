<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EnquiryType extends Model
{
    protected $table="enquiry_type";
}
