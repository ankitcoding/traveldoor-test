<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guides extends Model
{
    protected $table="guides";

     public function getSupplier()
   {
   	return $this->belongsTo('App\Suppliers', 'guide_supplier_id','supplier_id');
   }
    public function getCity()
   {
   	return $this->belongsTo('App\Cities', 'guide_city','id');
   }
   public function getCountry()
   {
   	return $this->belongsTo('App\Countries', 'guide_country','country_id');
   }

}
