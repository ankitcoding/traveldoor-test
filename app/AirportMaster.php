<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AirportMaster extends Model
{
    protected $table="airport_masters";
}
