<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SightSeeing extends Model
{
    protected $table="sightseeing";
    public function getFromCity()
   {
   	return $this->belongsTo('App\Cities', 'sightseeing_city_from','id');
   }
   public function getToCity()
   {
   	return $this->belongsTo('App\Cities', 'sightseeing_city_to','id');
   }
   public function getCountry()
   {
   	return $this->belongsTo('App\Countries', 'sightseeing_country','country_id');
   }
}
