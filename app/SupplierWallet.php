<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierWallet extends Model
{
    protected $table="supplier_wallet";
}
