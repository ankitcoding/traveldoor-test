<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activities extends Model
{
   protected $table="activities";

   public function getActivityType()
   {
   	 return $this->belongsTo('App\ActivityType', 'activity_type','activity_type_id');
   }
    public function getSupplier()
   {
   	return $this->belongsTo('App\Suppliers', 'supplier_id','supplier_id');
   }
   public function getCity()
   {
   	return $this->belongsTo('App\Cities', 'activity_city','id');
   }
   public function getCountry()
   {
   	return $this->belongsTo('App\Countries', 'activity_country','country_id');
   }
}
