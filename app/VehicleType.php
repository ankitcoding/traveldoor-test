<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleType extends Model
{
    protected $table="vehicle_type";

    public function getVehicles()
    {
    	 return $this->hasMany('App\Vehicles', 'vehicle_type_id','vehicle_type_id');
    }
}

