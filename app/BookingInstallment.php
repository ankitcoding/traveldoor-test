<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingInstallment extends Model
{
   protected $table="booking_installment";
}
