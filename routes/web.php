<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/','LoginController@index')->name('index');
Route::get('/home','LoginController@home')->name('home');
Route::post('/login-check','LoginController@login_check')->name('login-check');
Route::get('/logout','LoginController@logout')->name('logout');

// menus
Route::get('/menu','LoginController@menu')->name('menu');
Route::post('/menu-insert','LoginController@menu_insert')->name('menu-insert');

// languages
Route::get('/languages','LoginController@languages')->name('languages');
Route::post('/languages-insert','LoginController@languages_insert')->name('languages-insert');

// vehicle type
Route::get('/vehicle-type','LoginController@vehicle_type')->name('vehicle-type');
Route::post('/vehicle-type-insert','LoginController@vehicle_type_insert')->name('vehicle-type-insert');
Route::post('/vehicle-type-update','LoginController@vehicle_type_update')->name('vehicle-type-update');

//vehicle
Route::get('/vehicle','LoginController@vehicle')->name('vehicle');
Route::get('/fetchVehicle','LoginController@fetchVehicle')->name('fetchVehicle');
Route::post('/vehicle-insert','LoginController@vehicle_insert')->name('vehicle-insert');
Route::post('/update-vehicle-active','LoginController@update_vehicle_active')->name('update-vehicle-active');

// enquiry type
Route::get('/enquiry-type','LoginController@enquiry_type')->name('enquiry-type');
Route::post('/enquiry-type-insert','LoginController@enquiry_type_insert')->name('enquiry-type-insert');
Route::post('/enquiry-type-update','LoginController@enquiry_type_update')->name('enquiry-type-update');

// airport master
Route::get('/airport-master','LoginController@airport_master')->name('airport-master');
Route::post('/airport-master-insert','LoginController@airport_master_insert')->name('airport-master-insert');
Route::get('/searchAirports','ServiceManagement@searchAirports')->name('searchAirports');
Route::post('/airport-master-update','LoginController@airport_master_update')->name('airport-master-update');

// vehicle type cost
Route::get('/vehicle-type-cost','LoginController@vehicle_type_cost')->name('vehicle-type-cost');
Route::post('/vehicle-type-cost-insert','LoginController@vehicle_type_cost_insert')->name('vehicle-type-cost-insert');
Route::post('/vehicle-type-cost-update','LoginController@vehicle_type_cost_update')->name('vehicle-type-cost-update');

// fuel type cost
Route::get('/fuel-type-cost','LoginController@fuel_type')->name('fuel-type-cost');
Route::post('/fuel-type-cost-insert','LoginController@fuel_type_insert')->name('fuel-type-cost-insert');
Route::post('/fuel-type-cost-update','LoginController@fuel_type_update')->name('fuel-type-cost-update');

// hotel meals
Route::get('/hotel-meal','LoginController@hotel_meal')->name('hotel-meal');
Route::post('/hotel-meal-insert','LoginController@hotel_meal_insert')->name('hotel-meal-insert');
Route::post('/hotel-meal-update','LoginController@hotel_meal_update')->name('hotel-meal-update');

// amenities
Route::get('/amenities','LoginController@amenities')->name('amenities');
Route::get('/fetchAmenitiesName','LoginController@fetchAmenitiesName')->name('fetchAmenitiesName');
Route::post('/amenities-insert','LoginController@amenities_insert')->name('amenities-insert');
Route::post('/update-amenities-active','LoginController@update_amenities_active')->name('update-amenities-active');
Route::post('/update-roomamenities-active','LoginController@update_roomamenities_active')->name('update-roomamenities-active');

// sub amenities

Route::get('/sub-amenities','LoginController@sub_amenities')->name('sub-amenities');
Route::get('/fetchSubAmenitiesName','LoginController@fetchSubAmenitiesName')->name('fetchSubAmenitiesName');
Route::post('/sub-amenities-insert','LoginController@sub_amenities_insert')->name('sub-amenities-insert');
Route::post('/update-sub-amenities-active','LoginController@update_sub_amenities_active')->name('update-sub-amenities-active');

//guide cost
Route::get('/guide-expense-cost','LoginController@guide_expense')->name('guide-expense-cost');
Route::post('/guide-expense-cost-insert','LoginController@guide_expense_insert')->name('guide-expense-cost-insert');
Route::post('/guide-expense-cost-update','LoginController@guide_expense_update')->name('guide-expense-cost-update');

// sightseeing tour type
Route::get('/tour-type','LoginController@tour_type')->name('tour-type');
Route::post('/tour-type-insert','LoginController@tour_type_insert')->name('tour-type-insert');
Route::post('/tour-type-update','LoginController@tour_type_update')->name('tour-type-update');

// hotel type
Route::get('/hotel-type','LoginController@hotel_type')->name('hotel-type');
Route::post('/hotel-type-insert','LoginController@hotel_type_insert')->name('hotel-type-insert');
Route::post('/hotel-type-update','LoginController@hotel_type_update')->name('hotel-type-update');

// activity type
Route::get('/activity-type','LoginController@activity_type')->name('activity-type');
Route::post('/activity-type-insert','LoginController@activity_type_insert')->name('activity-type-insert');
Route::post('/activity-type-update','LoginController@activity_type_update')->name('activity-type-update');



// expense category
Route::get('/expense-category','ServiceManagement@expense_category')->name('expense-category');
Route::post('/expense-category-insert','ServiceManagement@expense_category_insert')->name('expense-category-insert');
Route::post('/expense-category-update','ServiceManagement@expense_category_update')->name('expense-category-update');



// income category
Route::get('/income-category','ServiceManagement@income_category')->name('income-category');
Route::post('/income-category-insert','ServiceManagement@income_category_insert')->name('income-category-insert');
Route::post('/income-category-update','ServiceManagement@income_category_update')->name('income-category-update');


// target commission
Route::get('/target-commission','LoginController@target_commission')->name('target-commission');
Route::post('/target-commission-insert','LoginController@target_commission_insert')->name('target-commission-insert');
Route::post('/target-commission-update','LoginController@target_commission_update')->name('target-commission-update');


// cities
Route::get('/cities','LoginController@cities')->name('cities');
Route::get('/get-all-cities-tab','LoginController@get_all_cities_tab')->name('get-all-cities-tab');
Route::post('/cities-update','LoginController@cities_update')->name('cities-update');
Route::post('/cities-delete','LoginController@cities_delete')->name('cities-delete');

//Enquiry
Route::get('/create-enquiry','EnquiryController@create_enquiry')->name('create-enquiry');
Route::post('/insert-enquiry','EnquiryController@insert_enquiry')->name('insert-enquiry');
Route::get('/view-enquiry','EnquiryController@view_enquiry')->name('view-enquiry');
Route::get('/fetch-enquiry-remarks','EnquiryController@fetch_enquiry_remarks')->name('fetch-enquiry-remarks');
Route::post('/insert-enquiry-remarks','EnquiryController@insert_enquiry_remarks')->name('insert-enquiry-remarks');

Route::get('/search-enquiry','EnquiryController@search_enquiry')->name('search-enquiry');
Route::get('/edit-enquiry/{enq_id}','EnquiryController@edit_enquiry')->name('edit-enquiry');
Route::post('/update-enquiry','EnquiryController@update_enquiry')->name('update-enquiry');
Route::get('/enquiry-details/{enq_id}','EnquiryController@enquiry_details')->name('enquiry-details');
Route::post('/insert-comments','EnquiryController@insert_comments')->name('insert-comments');

Route::get('/assign-enquiry','EnquiryController@assign_enquiry')->name('assign-enquiry');

Route::get('/search_mobile_enquiry','EnquiryController@search_mobile_enquiry')->name('search_mobile_enquiry');
Route::get('/enq_data_search','EnquiryController@enq_data_search')->name('enq_data_search');
//enquiry followup
Route::get('/view-enquiry-followup','EnquiryController@view_enquiry_followup')->name('view-enquiry-followup');
Route::get('/enquiry_view_filter_followup','EnquiryController@enquiry_view_filter_followup')->name('enquiry_view_filter_followup');
//Enquiry filters
Route::get('/enquiry_view_filter','EnquiryController@enquiry_view_filter')->name('enquiry_view_filter');
Route::get('/enquiry_search_filter','EnquiryController@enquiry_search_filter')->name('enquiry_search_filter');
//USER
Route::get('/user-management','UserManagement@user_management')->name('user-management');
Route::get('/create-user','UserManagement@create_user')->name('create-user');
Route::post('/insert-user','UserManagement@insert_user')->name('insert-user');
Route::get('/edit-user/{users_id}','UserManagement@edit_user')->name('edit-user');
Route::post('/update-user','UserManagement@update_user')->name('update-user');
Route::get('/user-details/{users_id}','UserManagement@user_details')->name('user-details');

//USER RIGHTS
Route::get('/user-rights','UserManagement@user_rights')->name('user-rights');
Route::post('/user-rights-insert','UserManagement@user_rights_insert')->name('user-rights-insert');
Route::get('/user-rights-fetch','UserManagement@user_rights_fetch')->name('user-rights-fetch');

//SUPPLIER
Route::get('/supplier-management','SupplierManagement@supplier_management')->name('supplier-management');
Route::get('/create-supplier','SupplierManagement@create_supplier')->name('create-supplier');
Route::post('/insert-supplier','SupplierManagement@insert_supplier')->name('insert-supplier');
Route::get('/edit-supplier/{supplier_id}','SupplierManagement@edit_supplier')->name('edit-supplier');
Route::post('/update-supplier','SupplierManagement@update_supplier')->name('update-supplier');
Route::get('/supplier-details/{supplier_id}','SupplierManagement@supplier_details')->name('supplier-details');
Route::post('/update-supplier-active','SupplierManagement@update_supplier_active_inactive')->name('update-supplier-active');
Route::post('/supplier-change-password','SupplierManagement@change_password')->name('supplier-change-password');




//AGENTS
Route::get('/agent-management','AgentManagement@agent_management')->name('agent-management');
Route::get('/create-agent','AgentManagement@create_agent')->name('create-agent');
Route::post('/insert-agent','AgentManagement@insert_agent')->name('insert-agent');
Route::get('/edit-agent/{agent_id}','AgentManagement@edit_agent')->name('edit-agent');
Route::post('/update-agent','AgentManagement@update_agent')->name('update-agent');
Route::get('/agent-details/{agent_id}','AgentManagement@agent_details')->name('agent-details');
Route::post('/update-agent-active','AgentManagement@update_agent_active_inactive')->name('update-agent-active');
Route::post('/agent-change-password','AgentManagement@change_password')->name('agent-change-password');




//SERVICE MANAGEMENT HELPERS
Route::get('/search-supplier-country','ServiceManagement@search_supplier_country')->name('search-supplier-country');
Route::get('/search-country-cities','ServiceManagement@search_country_cities')->name('search-country-cities');
Route::get('/search-country-itinerary-cities','ServiceManagement@search_country_itinerary_cities')->name('search-country-itinerary-cities');
Route::get('/searchCities','ServiceManagement@searchCities')->name('searchCities');
Route::get('/searchSightseeingTour','ServiceManagement@searchSightseeingTour')->name('searchSightseeingTour');
Route::get('/searchSightseeingTourName','ServiceManagement@searchSightseeingTourName')->name('searchSightseeingTourName');
Route::get('/searchTransfers','ServiceManagement@searchTransfers')->name('searchTransfers');
Route::get('/searchAirports','ServiceManagement@searchAirports')->name('searchAirports');
Route::get('/searchAirportsCost','ServiceManagement@searchAirportsCost')->name('searchAirportsCost');

Route::get('/searchFuelCost','ServiceManagement@searchFuelCost')->name('searchFuelCost');
Route::get('/searchActivity','ServiceManagement@searchActivity')->name('searchActivity');
Route::get('/searchHotel','ServiceManagement@searchHotel')->name('searchHotel');
Route::get('/searchGuide','ServiceManagement@searchGuide')->name('searchGuide');
Route::get('/searchDriver','ServiceManagement@searchDriver')->name('searchDriver');
Route::get('/searchItinerary','ServiceManagement@searchItinerary')->name('searchItinerary');
Route::get('/searchSupplier','ServiceManagement@searchSupplier')->name('searchSupplier');
Route::get('/searchAgent','ServiceManagement@searchAgent')->name('searchAgent');
Route::get('/service-management','ServiceManagement@service_management')->name('service-management');
Route::get('/fetch-service-management-tab','ServiceManagement@fetch_service_management_tab')->name('fetch-service-management-tab');

//ACTIVITY
Route::get('/create-activity','ServiceManagement@create_activity')->name('create-activity');
Route::post('/insert-activity','ServiceManagement@insert_activity')->name('insert-activity');
Route::get('/edit-activity/{activity_id}','ServiceManagement@edit_activity')->name('edit-activity');
Route::post('/update-activity','ServiceManagement@update_activity')->name('update-activity');
Route::get('/activity-details/{activity_id}','ServiceManagement@activity_details')->name('activity-details');
Route::post('/update-activity-approval','ServiceManagement@update_activity_approval')->name('update-activity-approval');
Route::post('/update-activity-active','ServiceManagement@update_activity_active_inactive')->name('update-activity-active');
Route::post('/update-activity-bestseller','ServiceManagement@update_activity_bestseller')->name('update-activity-bestseller');
Route::post('/update-activity-popular','ServiceManagement@update_activity_popular')->name('update-activity-popular');
Route::post('/sort-activity','ServiceManagement@sort_activity')->name('sort-activity');

//TRANSPORT
Route::get('/create-transport','ServiceManagement@create_transport')->name('create-transport');
Route::post('/insert-transport','ServiceManagement@insert_transport')->name('insert-transport');
Route::get('/edit-transport/{transport_id}','ServiceManagement@edit_transport')->name('edit-transport');
Route::post('/update-transport','ServiceManagement@update_transport')->name('update-transport');
Route::get('/transport-details/{transport_id}','ServiceManagement@transport_details')->name('transport-details');
Route::post('/update-transport-approval','ServiceManagement@update_transport_approval')->name('update-transport-approval');
Route::post('/update-transport-active','ServiceManagement@update_transport_active_inactive')->name('update-transport-active');

//HOTELS
Route::get('/create-hotel','ServiceManagement@create_hotel')->name('create-hotel');
Route::post('/insert-hotel','ServiceManagement@insert_hotel')->name('insert-hotel');
Route::get('/edit-hotel/{hotel_id}','ServiceManagement@edit_hotel')->name('edit-hotel');
Route::get('/edit-old-hotel/{hotel_id}','ServiceManagement@edit_old_hotel')->name('edit-old-hotel');
Route::post('/update-hotel','ServiceManagement@update_hotel')->name('update-hotel');
Route::get('/hotel-details/{hotel_id}','ServiceManagement@hotel_details')->name('hotel-details');
Route::post('/update-hotel-approval','ServiceManagement@update_hotel_approval')->name('update-hotel-approval');
Route::post('/update-hotel-active','ServiceManagement@update_hotel_active_inactive')->name('update-hotel-active');
Route::post('/update-hotel-bestseller','ServiceManagement@update_hotel_bestseller')->name('update-hotel-bestseller');
Route::post('/update-hotel-popular','ServiceManagement@update_hotel_popular')->name('update-hotel-popular');
Route::post('/sort-hotel','ServiceManagement@sort_hotel')->name('sort-hotel');

//SIGHTSEEING
Route::get('/create-sightseeing','ServiceManagement@create_sightseeing')->name('create-sightseeing');
Route::post('/insert-sightseeing','ServiceManagement@insert_sightseeing')->name('insert-sightseeing');
Route::get('/edit-sightseeing/{sightseeing_id}','ServiceManagement@edit_sightseeing')->name('edit-sightseeing');
Route::post('/update-sightseeing','ServiceManagement@update_sightseeing')->name('update-sightseeing');
Route::get('/sightseeing-details/{sightseeing_id}','ServiceManagement@sightseeing_details')->name('sightseeing-details');
// Route::post('/update-sightseeing-approval','ServiceManagement@update_sightseeing_approval')->name('update-sightseeing-approval');
Route::post('/update-sightseeing-active','ServiceManagement@update_sightseeing_active_inactive')->name('update-sightseeing-active');
Route::post('/update-sightseeing-bestseller','ServiceManagement@update_sightseeing_bestseller')->name('update-sightseeing-bestseller');
Route::post('/update-sightseeing-popular','ServiceManagement@update_sightseeing_popular')->name('update-sightseeing-popular');
Route::post('/sort-sightseeing','ServiceManagement@sort_sightseeing')->name('sort-sightseeing');

//GUIDE
Route::get('/guide-management','ServiceManagement@guide_management')->name('admin-guide-management');
Route::get('/create-guide','ServiceManagement@create_guide')->name('admin-create-guide');
Route::post('/insert-guide','ServiceManagement@insert_guide')->name('admin-insert-guide');
Route::get('/edit-guide/{guide_id}','ServiceManagement@edit_guide')->name('admin-edit-guide');
Route::post('/update-guide','ServiceManagement@update_guide')->name('admin-update-guide');
Route::get('/guide-details/{guide_id}','ServiceManagement@guide_details')->name('admin-guide-details');
Route::post('/update-guide-approval','ServiceManagement@update_guide_approval')->name('admin-update-guide-approval');
Route::post('/update-guide-active','ServiceManagement@update_guide_active_inactive')->name('admin-update-guide-active');
Route::post('/update-guide-bestseller','ServiceManagement@update_guide_bestseller')->name('admin-update-guide-bestseller');
Route::post('/update-guide-popular','ServiceManagement@update_guide_popular')->name('update-guide-popular');
Route::post('/sort-guide','ServiceManagement@sort_guide')->name('sort-guide');

//Itinerary
Route::get('/saved-itinerary','ItinerariesController@saved_itinerary')->name('saved-itinerary');
Route::get('/create-itinerary','ItinerariesController@create_itinerary')->name('create-itinerary');
Route::get('/itinerary-get-hotels','ItinerariesController@itinerary_get_hotels')->name('itinerary-get-hotels');
Route::get('/itinerary-get-activities','ItinerariesController@itinerary_get_activities')->name('itinerary-get-activities');
Route::get('/itinerary-get-sightseeing','ItinerariesController@itinerary_get_sightseeing')->name('itinerary-get-sightseeing');
Route::get('/itinerary-get-sightseeing-details','ItinerariesController@itinerary_get_sightseeing_details')->name('itinerary-get-sightseeing-details');
Route::get('/itinerary-get-transfer','ItinerariesController@itinerary_get_transfer')->name('itinerary-get-transfer');
Route::get('/fetch-services','ServiceManagement@fetch_services')->name('fetch-services');
Route::post('/insert-itinerary','ItinerariesController@insert_itinerary_new')->name('insert-itinerary');
Route::get('/edit-itinerary/{itinerary_id}','ItinerariesController@edit_itinerary_new')->name('edit-itinerary');
Route::post('/update-itinerary','ItinerariesController@update_itinerary_new')->name('update-itinerary');
Route::get('/itinerary-details/{itinerary_id}','ItinerariesController@itinerary_details')->name('itinerary-details');
Route::post('/update-itinerary-active','ItinerariesController@update_itinerary_active_inactive')->name('update-itinerary-active');
Route::post('/update-itinerary-bestseller','ItinerariesController@update_itinerary_bestseller')->name('update-itinerary-bestseller');
Route::post('/update-itinerary-popular','ServiceManagement@update_itinerary_popular')->name('update-itinerary-popular');
Route::post('/sort-itinerary','ItinerariesController@sort_itinerary')->name('sort-itinerary');


//TRANSFERS
Route::get('/create-transfer','ServiceManagement@create_transfer')->name('create-transfer');
Route::post('/insert-transfer','ServiceManagement@insert_transfer')->name('insert-transfer');
Route::get('/fetchAirportTransferData','ServiceManagement@fetchAirportTransferData')->name('fetchAirportTransferData');
Route::get('/fetchCityTransferData','ServiceManagement@fetchCityTransferData')->name('fetchCityTransferData');
Route::get('/fetchToCityTransferData','ServiceManagement@fetchToCityTransferData')->name('fetchToCityTransferData');
Route::get('/fetchToAirportTransferData','ServiceManagement@fetchToAirportTransferData')->name('fetchToAirportTransferData');
Route::get('/edit-transfer/{transfer_id}','ServiceManagement@edit_transfer')->name('edit-transfer');
Route::post('/update-transfer','ServiceManagement@update_transfer')->name('update-transfer');
Route::get('/transfer-details/{hotel_id}','ServiceManagement@transfer_details')->name('transfer-details');
Route::post('/update-transfer-approval','ServiceManagement@update_transfer_approval')->name('update-transfer-approval');
Route::post('/update-transfer-active','ServiceManagement@update_transfer_active_inactive')->name('update-transfer-active');
// Route::post('/update-hotel-bestseller','ServiceManagement@update_hotel_bestseller')->name('update-hotel-bestseller');
// Route::post('/sort-hotel','ServiceManagement@sort_hotel')->name('sort-hotel');
	

//TRANSFER
Route::get('/create-driver','ServiceManagement@create_driver')->name('create-driver');
Route::post('/insert-driver','ServiceManagement@insert_driver')->name('insert-driver');
Route::get('/edit-driver/{driver_id}','ServiceManagement@edit_driver')->name('edit-driver');
Route::post('/update-driver','ServiceManagement@update_driver')->name('update-driver');
Route::get('/driver-details/{driver_id}','ServiceManagement@driver_details')->name('driver-details');
Route::post('/update-driver-approval','ServiceManagement@update_driver_approval')->name('update-driver-approval');
Route::post('/update-driver-active','ServiceManagement@update_driver_active_inactive')->name('update-driver-active');
// Route::post('/update-driver-bestseller','ServiceManagement@update_driver_bestseller')->name('admin-update-driver-bestseller');
Route::post('/sort-driver','ServiceManagement@sort_driver')->name('sort-driver');


Route::get('/agent-bookings','ServiceManagement@agent_bookings')->name('bookings');
Route::get('/view-sub-bookings/{booking_id}','ServiceManagement@agent_sub_bookings')->name('agent-sub-bookings');
Route::get('/agent-bookings-filters','ServiceManagement@agent_bookings_filters')->name('agent-bookings-filters');
Route::get('/admin-agent-invoice','ServiceManagement@agent_invoice')->name('admin-agent-invoice');
Route::get('/admin-customer-invoice','ServiceManagement@customer_invoice')->name('admin-customer-invoice');
Route::post('/approve-booking','ServiceManagement@approve_agent_booking')->name('approve-booking');
Route::get('/customer-bookings','ServiceManagement@customer_bookings')->name('customer-bookings');


Route::post('/booking-complete','ServiceManagement@booking_complete')->name('booking-complete');
Route::get('/booking-details','ServiceManagement@agent_bookings_details')->name('booking-details');

Route::post('/bookings-cancel','ServiceManagement@agent_bookings_cancel')->name('bookings-cancel');

//fetch booking attachments 
Route::get('/booking-attachments','ServiceManagement@booking_attachment_html')->name('booking-attachments');
//update booking attachments
Route::post('/booking-attachments','ServiceManagement@booking_attachment_upload')->name('booking-attachments');


Route::get('/pdf','ServiceManagement@pdf')->name('pdf');

//customer markup
Route::get('/customer-markup','LoginController@customer_markup')->name('customer-markup');
Route::post('/customer-markup-insert','LoginController@customer_markup_insert')->name('customer-markup-insert');
Route::post('/customer-markup-update','LoginController@customer_markup_update')->name('customer-markup-update');



//accounting
Route::get('/exchange-rates','ServiceManagement@exchange_rates')->name('exchange-rates');
Route::get('/get-exchange-rates','ServiceManagement@get_exchange_rates')->name('get-exchange-rates');

Route::get('/get-installment','ServiceManagement@get_booking_and_installment_detail')->name('get-booking-installment-detail');

Route::post('/insert-installment','ServiceManagement@insert_installment')->name('insert-installment');

//expenses
Route::get('/booking-expenses','ServiceManagement@view_booking_expenses')->name('booking-expenses');
Route::get('/add-booking-expenses','ServiceManagement@add_booking_expenses')->name('add-booking-expenses');
Route::get('/edit-booking-expenses/{expense_id}','ServiceManagement@edit_booking_expenses')->name('edit-booking-expenses');


Route::get('/office-expenses','ServiceManagement@view_office_expenses')->name('office-expenses');
Route::get('/add-office-expenses','ServiceManagement@add_office_expenses')->name('add-office-expenses');
Route::get('/edit-office-expenses/{expense_id}','ServiceManagement@edit_office_expenses')->name('edit-office-expenses');


Route::post('/insert-expenses','ServiceManagement@insert_expenses')->name('insert-expenses');
Route::post('/update-expenses','ServiceManagement@update_expenses')->name('update-expenses');
Route::get('/expense-details/{expense_id}','ServiceManagement@expense_details')->name('expense-details');


//incomes
Route::get('/booking-incomes','ServiceManagement@view_booking_incomes')->name('booking-incomes');
Route::get('/add-booking-incomes','ServiceManagement@add_booking_incomes')->name('add-booking-incomes');
Route::get('/edit-booking-incomes/{income_id}','ServiceManagement@edit_booking_incomes')->name('edit-booking-incomes');

Route::get('/office-incomes','ServiceManagement@view_office_incomes')->name('office-incomes');
Route::get('/add-office-incomes','ServiceManagement@add_office_incomes')->name('add-office-incomes');
Route::get('/edit-office-incomes/{income_id}','ServiceManagement@edit_office_incomes')->name('edit-office-incomes');

Route::post('/insert-incomes','ServiceManagement@insert_incomes')->name('insert-incomes');
Route::post('/update-incomes','ServiceManagement@update_incomes')->name('update-incomes');
Route::get('/income-details/{income_id}','ServiceManagement@income_details')->name('income-details');


// user commissions and wallet
Route::get('/user-commissions','UserManagement@user_commissions')->name('user-commissions');
Route::get('/user-commissions-filter','UserManagement@user_commissions_filter')->name('user-commissions-filter');
Route::get('/user-commissions-details','UserManagement@user_commissions_details')->name('user-commissions-details');
Route::post('/pay-commissions','UserManagement@pay_commissions')->name('pay-commissions');

Route::get('/users-wallet','UserManagement@user_wallet')->name('users-wallet');
Route::get('/my-wallet','UserManagement@user_own_wallet')->name('my-wallet');
Route::post('/withdraw-wallet','UserManagement@withdraw_wallet')->name('withdraw-wallet');

Route::post('/users-operation','UserManagement@users_operation')->name('users-operation');


Route::get('/get-withdrawals-detail','UserManagement@get_withdrawals_detail')->name('get-withdrawals-detail');
Route::post('/withdrawals-approval','UserManagement@withdrawals_approval')->name('withdrawals-approval');

//supplier payment and wallet
Route::post('/pay-supplier-booking','SupplierManagement@pay_supplier_booking')->name('pay-supplier-booking');

Route::get('/suppliers-wallet','SupplierManagement@suppliers_wallet')->name('suppliers-wallet');
Route::get('/my-wallet-supplier','SupplierManagement@own_wallet_supplier')->name('my-wallet-supplier');

Route::post('/suppliers-operation','SupplierManagement@suppliers_operation')->name('suppliers-operation');

Route::get('/get-withdrawals-supplier-detail','SupplierManagement@get_withdrawals_supplier_detail')->name('get-withdrawals-supplier-detail');
Route::post('/withdrawals-supplier-approval','SupplierManagement@withdrawals_supplier_approval')->name('withdrawals-supplier-approval');
//group intinary
Route::get('/create-group-itinerary','GroupItinerariesController@create_group_itinerary')->name('create-group-itinerary');
Route::get('/group-hotel-search','GroupItinerariesController@group_hotel_search')->name('group-hotel-search');
Route::get('/hotel-group-room-search','GroupItinerariesController@hotel_group_room_search')->name('hotel-group-room-search');
//end group initnary



Route::get('/agents-wallet','AgentManagement@agents_wallet')->name('agents-wallet');
Route::get('/my-wallet-agent','AgentManagement@own_wallet_agent')->name('my-wallet-agent');

Route::post('/agents-operation','AgentManagement@agents_operation')->name('agents-operation');

//payments confirmation
Route::post('/approve-payment','ServiceManagement@approve_agent_payment')->name('approve-payment');


Route::get('/monthly-report','AccountManagement@monthly_report')->name('monthly-report');
Route::post('/monthly-report','AccountManagement@monthly_report_filters')->name('monthly-report');
Route::get('/monthly-report-print','AccountManagement@monthly_report_filters')->name('monthly-report-print');
Route::get('/success','AccountManagement@success')->name('success');





//Restaurants
//restaurant type
Route::get('/restaurant-type','RestaurantController@restaurant_type')->name('restaurant-type');
Route::post('/restaurant-type-insert','RestaurantController@restaurant_type_insert')->name('restaurant-type-insert');
Route::post('/restaurant-type-update','RestaurantController@restaurant_type_update')->name('restaurant-type-update');

//restaurant-menu-category
Route::get('/restaurant-menu-category','RestaurantController@restaurant_menu_category')->name('restaurant-menu-category');
Route::post('/restaurant-menu-category-insert','RestaurantController@restaurant_menu_category_insert')->name('restaurant-menu-category-insert');
Route::post('/restaurant-menu-category-update','RestaurantController@restaurant_menu_category_update')->name('restaurant-menu-category-update');



Route::get('/restaurant-management','RestaurantController@restaurant_management')->name('restaurant-management');

//restaurants
Route::get('/create-restaurant','RestaurantController@create_restaurant')->name('create-restaurant');
Route::post('/insert-restaurant','RestaurantController@insert_restaurant')->name('insert-restaurant');
Route::get('/edit-restaurant/{restaurant_id}','RestaurantController@edit_restaurant')->name('edit-restaurant');
Route::post('/update-restaurant','RestaurantController@update_restaurant')->name('update-restaurant');
Route::get('/restaurant-details/{restaurant_id}','RestaurantController@restaurant_details')->name('restaurant-details');
Route::post('/update-restaurant-approval','RestaurantController@update_restaurant_approval')->name('update-restaurant-approval');
Route::post('/update-restaurant-active','RestaurantController@update_restaurant_active_inactive')->name('update-restaurant-active');
Route::post('/update-restaurant-bestseller','RestaurantController@update_restaurant_bestseller')->name('update-restaurant-bestseller');
Route::post('/update-restaurant-popular','RestaurantController@update_restaurant_popular')->name('update-restaurant-popular');
Route::post('/sort-restaurant','RestaurantController@sort_restaurant')->name('sort-restaurant');

//foods
Route::get('/food-management','FoodController@food_management')->name('food-management');
Route::get('/create-food','FoodController@create_food')->name('create-food');
Route::post('/insert-food','FoodController@insert_food')->name('insert-food');
Route::get('/edit-food/{food_id}','FoodController@edit_food')->name('edit-food');
Route::post('/update-food','FoodController@update_food')->name('update-food');
Route::get('/food-details/{food_id}','FoodController@food_details')->name('food-details');
Route::post('/update-food-approval','FoodController@update_food_approval')->name('update-food-approval');
Route::post('/update-food-active','FoodController@update_food_active_inactive')->name('update-food-active');

//suppliers
Route::group(['prefix' => 'supplier'], function() 
{
		Route::get('/','SupplierController@index')->name('supplier');
		Route::get('/supplier-home','SupplierController@supplier_home')->name('supplier-home');
		Route::post('/supplier-login-check','SupplierController@supplier_login_check')->name('supplier-login-check');
		Route::get('/supplier-logout','SupplierController@supplier_logout')->name('supplier-logout');
		//suppliermanagemant


		Route::get('/my-wallet','SupplierController@supplier_own_wallet')->name('my-wallet-own-supplier');
		Route::post('/withdraw-wallet','SupplierController@withdraw_wallet')->name('withdraw-wallet-own-supplier');


		Route::get('/register-supplier','SupplierController@register_supplier')->name('register-supplier');
		Route::post('/insert-new-supplier','SupplierController@insert_supplier')->name('insert-new-supplier');
		Route::get('/supplier-profile','SupplierController@supplier_profile')->name('supplier-profile');
		Route::get('/supplier-profile-edit','SupplierController@supplier_profile_edit')->name('supplier-profile-edit');
		Route::post('/supplier-profile-update','SupplierController@supplier_profile_update')->name('supplier-profile-update');

		//hotel
		Route::get('/supplier-hotel','SupplierController@supplier_hotel')->name('supplier-hotel');
		Route::get('/supplier-create-hotel','SupplierController@supplier_create_hotel')->name('supplier-create-hotel');
		Route::get('/supplier-hotel-details/{hotel_id}','SupplierController@supplier_hotel_details')->name('supplier-hotel-details');
		Route::get('/supplier-edit-hotel/{hotel_id}','SupplierController@supplier_edit_hotel')->name('supplier-edit-hotel');

		//Activity
		Route::get('/supplier-activity','SupplierController@supplier_activity')->name('supplier-activity');
		Route::get('/supplier-create-activity','SupplierController@supplier_create_activity')->name('supplier-create-activity');
		Route::get('/supplier-activity-details/{activity_id}','SupplierController@supplier_activity_details')->name('supplier-activity-details');
		Route::get('/supplier-edit-activity/{activity_id}','SupplierController@supplier_edit_activity')->name('supplier-edit-activity');
		//Transport
		Route::get('/supplier-transport','SupplierController@supplier_transport')->name('supplier-transport');
		Route::get('/supplier-create-transport','SupplierController@supplier_create_transport')->name('supplier-create-transport');
		Route::get('/supplier-transport-details/{transport_id}','SupplierController@supplier_transport_details')->name('supplier-transport-details');
		Route::get('/supplier-edit-transport/{transport_id}','SupplierController@supplier_edit_transport')->name('supplier-edit-transport');

		//GUIDE
		Route::get('/guide-management','SupplierController@guide_management')->name('supplier-guide');
		Route::get('/create-guide','SupplierController@create_guide')->name('supplier-create-guide');
		Route::post('/insert-guide','SupplierController@insert_guide')->name('supplier-insert-guide');
		Route::get('/edit-guide/{guide_id}','SupplierController@edit_guide')->name('supplier-edit-guide');
		Route::post('/update-guide','SupplierController@update_guide')->name('supplier-update-guide');
		Route::get('/guide-details/{guide_id}','SupplierController@guide_details')->name('supplier-guide-details');
		Route::post('/update-guide-active','SupplierController@update_guide_active_inactive')->name('supplier-update-guide-active');

		//Transfer
		Route::get('/supplier-transfer','SupplierController@supplier_transfer')->name('supplier-transfer');
		Route::get('/supplier-create-transfer','SupplierController@supplier_create_transfer')->name('supplier-create-transfer');
		Route::get('/supplier-transfer-details/{transfer_id}','SupplierController@supplier_transfer_details')->name('supplier-transfer-details');
		Route::get('/supplier-edit-transfer/{transfer_id}','SupplierController@supplier_edit_transfer')->name('supplier-edit-transfer');


		Route::get('/supplier-bookings','SupplierController@supplier_bookings')->name('supplier-bookings');

		Route::post('/supplier-approve-booking','SupplierController@supplier_approve_booking')->name('supplier-approve-booking');
		
        // Route::get('/driver-management','SupplierController@driver_management')->name('driver-management');
        // Route::get('/create-driver','SupplierController@create_driver')->name('create-driver');
        // Route::post('/insert-driver','SupplierController@insert_driver')->name('insert-driver');
        // Route::get('/edit-driver/{driver_id}','SupplierController@edit_driver')->name('edit-driver');
        // Route::post('/update-driver','SupplierController@update_driver')->name('update-driver');
        // Route::get('/driver-details/{driver_id}','SupplierController@driver_details')->name('driver-details');
        // Route::post('/update-driver-approval','SupplierController@update_driver_approval')->name('update-driver-approval');
        // Route::post('/update-driver-active','SupplierController@update_driver_active_inactive')->name('update-driver-active');
        Route::get('/driver-management','SupplierController@driver_management')->name('supplier-driver');
		Route::get('/create-driver','SupplierController@create_driver')->name('supplier-create-driver');
		Route::post('/insert-driver','SupplierController@insert_driver')->name('supplier-insert-driver');
		Route::get('/edit-driver/{guide_id}','SupplierController@edit_driver')->name('supplier-edit-driver');
		Route::post('/update-driver','SupplierController@update_driver')->name('supplier-update-driver');
		Route::get('/driver-details/{guide_id}','SupplierController@driver_details')->name('supplier-driver-details');
		Route::post('/update-driver-active','SupplierController@update_driver_active_inactive')->name('supplier-update-driver-active');


//restaurants
Route::get('/restaurant-management','SupplierController@restaurant_management')->name('supplier-restaurant');
Route::get('/create-restaurant','SupplierController@create_restaurant')->name('supplier-create-restaurant');
Route::get('/edit-restaurant/{restaurant_id}','SupplierController@edit_restaurant')->name('supplier-edit-restaurant');
Route::get('/restaurant-details/{restaurant_id}','SupplierController@restaurant_details')->name('supplier-restaurant-details');

Route::get('/food-management','SupplierController@food_management')->name('supplier-food');
Route::get('/create-food','SupplierController@create_food')->name('supplier-create-food');
Route::get('/edit-food/{food_id}','SupplierController@edit_food')->name('supplier-edit-food');
Route::get('/food-details/{food_id}','SupplierController@food_details')->name('supplier-food-details');


});


//agent check
Route::group(['prefix' => 'agent'], function() 
{
		Route::get('/','AgentController@agent_index')->name('agent');
		Route::get('/agent-home','AgentController@agent_home')->name('agent-home');
		Route::post('/agent-login-check','AgentController@agent_login_check')->name('agent-login-check');
		Route::get('/agent-logout','AgentController@agent_logout')->name('agent-logout');
		Route::get('/agent-markup','AgentController@enter_agent_markup')->name('agent-markup');
		Route::post('/agent-markup-update','AgentController@agent_markup_update')->name('agent-markup-update');

		Route::get('/register-agent','AgentController@register_agent')->name('register-agent');
		Route::post('/insert-new-agent','AgentController@insert_agent')->name('insert-new-agent');
		Route::get('/agent-profile','AgentController@agent_profile')->name('agent-profile');
		Route::get('/agent-profile-edit','AgentController@agent_profile_edit')->name('agent-profile-edit');
		Route::post('/agent-profile-update','AgentController@agent_profile_update')->name('agent-profile-update');

		Route::get('/my-wallet','AgentController@agent_own_wallet')->name('my-wallet-own-agent');

		Route::get('/hotel-search','AgentHotelBooking@hotel_search')->name('hotel-search');
		Route::get('fetchHotelNames','AgentHotelBooking@fetchHotelNames')->name('fetchHotelNames');
		Route::get('/activity-search','AgentActivityBooking@activity_search')->name('activity-search');
		Route::get('/guide-search','AgentGuideBooking@guide_search')->name('guide-search');
		Route::get('/transportation-search','AgentController@transportation_search')->name('transportation-search');
		Route::get('/sightseeing-search','AgentSightseeingBooking@sightseeing_search')->name('sightseeing-search');
		Route::get('/itinerary-search','AgentController@itinerary_search')->name('itinerary-search');
		Route::get('/transfer-search','AgentTransferBooking@transfer_search')->name('transfer-search');
		Route::get('/restaurant-search','AgentRestaurantBooking@restaurant_search')->name('restaurant-search');

		//hotel list
		Route::get('/hotel-list','AgentController@hotel_list')->name('hotel-list');
		

		//Activity
		Route::get('fetchActivities','AgentActivityBooking@fetchActivities')->name('fetchActivities');
		Route::get('fetchActivitiesPricing','AgentActivityBooking@fetchActivitiesPricing')->name('fetchActivitiesPricing');
		Route::get('fetchActivitiesAvailability','AgentActivityBooking@fetchActivitiesAvailability')->name('fetchActivitiesAvailability');
		Route::get('/activity-details-view/{activity_id}','AgentActivityBooking@activity_details_view')->name('activity-details-view');
		Route::post('/activity-booking','AgentActivityBooking@activity_booking')->name('activity-booking');
		Route::post('/confirm-activity-booking','AgentActivityBooking@confirm_activity_booking')->name('confirm-activity-booking');

		//Hotel

		Route::get('fetchHotels','AgentHotelBooking@fetchHotels')->name('fetchHotels');
		Route::get('/hotel-detail/{hotel_id}','AgentHotelBooking@hotel_detail')->name('hotel-detail');
		Route::post('/hotel-surroundings','AgentHotelBooking@hotel_surroundings')->name('hotel-surroundings');
		Route::get('/hotel-room-detail','AgentHotelBooking@hotel_room_detail')->name('hotel-room-detail');
		Route::post('/hotel-booking','AgentHotelBooking@hotel_booking')->name('hotel-booking');
		Route::post('/confirm-hotel-booking','AgentHotelBooking@confirm_hotel_booking')->name('confirm-hotel-booking');

		//Guide
		Route::get('fetchGuides','AgentGuideBooking@fetchGuides')->name('fetchGuides');
		Route::get('/guide-details-view/{guide_id}','AgentGuideBooking@guide_details_view')->name('guide-details-view');
	    Route::post('/guide-booking','AgentGuideBooking@guide_booking')->name('guide-booking');
	    Route::post('/confirm-guide-booking','AgentGuideBooking@confirm_guide_booking')->name('confirm-guide-booking');

	    	//Sightseeing
		Route::get('fetchSightseeing','AgentSightseeingBooking@fetchSightseeing')->name('fetchSightseeing');
		Route::get('/sightseeing-details-view/{sightseeing_id}','AgentSightseeingBooking@sightseeing_details_view')->name('sightseeing-details-view');
		Route::get('fetchGuidesSightseeing','AgentSightseeingBooking@fetchGuidesSightseeing')->name('fetchGuidesSightseeing');
		Route::get('fetchGuidesDetails','AgentSightseeingBooking@fetchGuidesDetails')->name('fetchGuidesDetails');
		Route::get('fetchDriversSightseeing','AgentSightseeingBooking@fetchDriversSightseeing')->name('fetchDriversSightseeing');
		Route::get('fetchDriversDetails','AgentSightseeingBooking@fetchDriversDetails')->name('fetchDriversDetails');
	    Route::post('/sightseeing-booking','AgentSightseeingBooking@sightseeing_booking')->name('sightseeing-booking');
	    Route::post('/confirm-sightseeing-booking','AgentSightseeingBooking@confirm_sightseeing_booking')->name('confirm-sightseeing-booking');

	    //Itinerary
		Route::get('fetchItinerary','AgentController@fetchItinerary')->name('fetchItinerary');
		Route::get('/itinerary-details-view/{itinerary_id}','AgentController@itinerary_details_view')->name('itinerary-details-view');
		Route::get('/itinerary-get-hotels','AgentController@itinerary_get_hotels')->name('agent-itinerary-get-hotels');
		Route::get('/itinerary-get-hotel-details','AgentController@itinerary_get_hotel_details')->name('agent-itinerary-get-hotels-details');
		Route::get('/itinerary-get-activities','AgentController@itinerary_get_activities')->name('agent-itinerary-get-activities');
		Route::get('/itinerary-get-transfer','AgentController@itinerary_get_transfer')->name('agent-itinerary-get-transfer');

		Route::get('/itinerary-get-sightseeing','AgentController@itinerary_get_sightseeing')->name('agent-itinerary-get-sightseeing');
		Route::post('itinerary-save','AgentController@itinerary_save')->name('itinerary-save');
		Route::post('/itinerary-booking-whole-cost','AgentController@itinerary_booking_whole_cost')->name('itinerary-booking-whole-cost');
		Route::post('/itinerary-booking','AgentController@itinerary_booking')->name('itinerary-booking');
		Route::get('/itinerary-get-restaurant','AgentController@itinerary_get_restaurant')->name('agent-itinerary-get-restaurant');
		Route::get('/itinerary-get-restaurant-details','RestaurantController@itinerary_get_restaurant_details')->name('agent-itinerary-get-restaurant-details');
		Route::post('/confirm-itinerary-booking','AgentController@confirm_itinerary_booking')->name('confirm-itinerary-booking');


		Route::get('restaurant-itinerary','CustomRestaurantItinerary@restaurant_itinerary')->name('restaurant-itinerary');
		Route::post('restaurant-itinerary-details','CustomRestaurantItinerary@restaurant_itinerary_details_view')->name('restaurant-itinerary-details');


		  //Transfers
		Route::post('fetchTransfer','AgentTransferBooking@fetchTransfer')->name('fetchTransfer');
		Route::get('fetchGuidesTransfer','AgentTransferBooking@fetchGuidesTransfer')->name('fetchGuidesTransfer');
		Route::get('/transfer-details-view/{transfer_id}','AgentTransferBooking@transfer_details_view')->name('transfer-details-view');
		Route::post('/transfer-booking','AgentTransferBooking@transfer_booking')->name('transfer-booking');
		Route::post('/confirm-transfer-booking','AgentTransferBooking@confirm_transfer_booking')->name('confirm-transfer-booking');




		//Restaurants
		Route::post('fetchRestaurant','AgentRestaurantBooking@fetchRestaurant')->name('fetchRestaurant');
		Route::get('/restaurant-details-view/{restaurant_id}','AgentRestaurantBooking@restaurant_details_view')->name('restaurant-details-view');
		Route::post('/restaurant-booking','AgentRestaurantBooking@restaurant_booking')->name('restaurant-booking');
		Route::post('/confirm-restaurant-booking','AgentRestaurantBooking@confirm_restaurant_booking')->name('confirm-restaurant-booking');



		Route::get('/fetch-saved-itineray','AgentController@fetch_itineray')->name('fetch-saved-itineray');
		Route::get('/download-saved-itineray','AgentController@download_itinerary')->name('download-saved-itineray');
		Route::get('/download-saved-restaurant-itineray','AgentController@download_restaurant_itinerary')->name('download-saved-restaurant-itineray');

		Route::get('/booking-success','AgentController@booking_success')->name('booking-success');
		Route::get('/booking-fail','AgentController@booking_fail')->name('booking-fail');

		//bookings
		Route::get('/agent-bookings','AgentController@agent_bookings')->name('agent-bookings');
		//payments
		Route::get('/get-payments','AgentController@agent_get_payments')->name('agent-get-payments');
		Route::post('/process-payments','AgentController@process_payments')->name('process-payments');
		Route::get('/process-online-payment','AgentController@process_online_payment')->name('process-online-payment');
		Route::get('/payment-processor','AgentController@payment_processor')->name('agent-payment-processor');


		Route::get('/agent-invoice','AgentController@agent_invoice')->name('agent-invoice');
		Route::get('/customer-invoice','AgentController@customer_invoice')->name('customer-invoice');
		Route::get('/agent-bookings-tab','AgentController@agent_bookings_tab')->name('agent-bookings-tab');
		Route::post('/agent-bookings-cancel','AgentController@agent_bookings_cancel')->name('agent-bookings-cancel');


		//fetch booking attachments 
Route::get('/booking-attachments','AgentController@booking_attachment_html')->name('booking-attachments-agent');
//update booking attachments
Route::post('/booking-attachments','AgentController@booking_attachment_upload')->name('booking-attachments-agent');


		Route::get('/exchange-rates','AgentController@exchange_rates')->name('agent-exchange-rates');

});

//oprtator check
Route::group(['prefix' => 'operator'], function() 
{
		Route::get('/','OperatorController@users_index')->name('operator');
		Route::get('/operator-home','OperatorController@users_home')->name('operator-home');
		Route::post('/operator-login-check','OperatorController@users_login_check')->name('operator-login-check');
		Route::get('/operator-logout','OperatorController@users_logout')->name('operator-logout');
		Route::get('/operator-markup','OperatorController@enter_users_markup')->name('operator-markup');
		Route::post('/operator-markup-update','OperatorController@users_markup_update')->name('operator-markup-update');

		Route::get('/register-operator','OperatorController@register_operator')->name('register-operator');
		Route::post('/insert-new-operator','OperatorController@insert_operator')->name('insert-new-operator');
		Route::get('/operator-profile','OperatorController@users_profile')->name('operator-profile');
		Route::get('/operator-profile-edit','OperatorController@users_profile_edit')->name('operator-profile-edit');
		Route::post('/operator-profile-update','OperatorController@users_profile_update')->name('operator-profile-update');

		Route::get('/my-wallet','OperatorController@users_own_wallet')->name('my-wallet-own-operator');

		Route::get('/hotel-search','OperatorHotelBooking@hotel_search')->name('hotel-search-operator');
		Route::get('fetchHotelNames','OperatorHotelBooking@fetchHotelNames')->name('fetchHotelNames-operator');
		Route::get('/activity-search','OperatorActivityBooking@activity_search')->name('activity-search-operator');
		Route::get('/guide-search','OperatorGuideBooking@guide_search')->name('guide-search-operator');
		Route::get('/transportation-search','OperatorController@transportation_search')->name('transportation-search-operator');
		Route::get('/sightseeing-search','OperatorSightseeingBooking@sightseeing_search')->name('sightseeing-search-operator');
		Route::get('/itinerary-search','OperatorController@itinerary_search')->name('itinerary-search-operator');
		Route::get('/transfer-search','OperatorTransferBooking@transfer_search')->name('transfer-search-operator');
		Route::get('/restaurant-search','OperatorRestaurantBooking@restaurant_search')->name('restaurant-search-operator');

		//Activity
		Route::get('fetchActivities','OperatorActivityBooking@fetchActivities')->name('fetchActivities-operator');
		Route::get('fetchActivitiesPricing','OperatorActivityBooking@fetchActivitiesPricing')->name('fetchActivitiesPricing-operator');
		Route::get('fetchActivitiesAvailability','OperatorActivityBooking@fetchActivitiesAvailability')->name('fetchActivitiesAvailability-operator');
		Route::get('/activity-details-view/{activity_id}','OperatorActivityBooking@activity_details_view')->name('activity-details-view-operator');
		Route::post('/activity-booking','OperatorActivityBooking@activity_booking')->name('activity-booking-operator');
		Route::post('/confirm-activity-booking','OperatorActivityBooking@confirm_activity_booking')->name('confirm-activity-booking-operator');

		//Hotel

		Route::get('fetchHotels','OperatorHotelBooking@fetchHotels')->name('fetchHotels-operator');
		Route::get('/hotel-detail/{hotel_id}','OperatorHotelBooking@hotel_detail')->name('hotel-detail-operator');
		Route::post('/hotel-surroundings','OperatorHotelBooking@hotel_surroundings')->name('hotel-surroundings-operator');
		Route::get('/hotel-room-detail','OperatorHotelBooking@hotel_room_detail')->name('hotel-room-detail-operator');
		Route::post('/hotel-booking','OperatorHotelBooking@hotel_booking')->name('hotel-booking-operator');
		Route::post('/confirm-hotel-booking','OperatorHotelBooking@confirm_hotel_booking')->name('confirm-hotel-booking-operator');

		//Guide
		Route::get('fetchGuides','OperatorGuideBooking@fetchGuides')->name('fetchGuides-operator');
		Route::get('/guide-details-view/{guide_id}','OperatorGuideBooking@guide_details_view')->name('guide-details-view-operator');
	    Route::post('/guide-booking','OperatorGuideBooking@guide_booking')->name('guide-booking-operator');
	    Route::post('/confirm-guide-booking','OperatorGuideBooking@confirm_guide_booking')->name('confirm-guide-booking-operator');

	    	//Sightseeing
		Route::get('fetchSightseeing','OperatorSightseeingBooking@fetchSightseeing')->name('fetchSightseeing-operator');
		Route::get('/sightseeing-details-view/{sightseeing_id}','OperatorSightseeingBooking@sightseeing_details_view')->name('sightseeing-details-view-operator');
		Route::get('fetchGuidesSightseeing','OperatorSightseeingBooking@fetchGuidesSightseeing')->name('fetchGuidesSightseeing-operator');
		Route::get('fetchGuidesDetails','OperatorSightseeingBooking@fetchGuidesDetails')->name('fetchGuidesDetails-operator');
		Route::get('fetchDriversSightseeing','OperatorSightseeingBooking@fetchDriversSightseeing')->name('fetchDriversSightseeing-operator');
		Route::get('fetchDriversDetails','OperatorSightseeingBooking@fetchDriversDetails')->name('fetchDriversDetails-operator');
	    Route::post('/sightseeing-booking','OperatorSightseeingBooking@sightseeing_booking')->name('sightseeing-booking-operator');
	    Route::post('/confirm-sightseeing-booking','OperatorSightseeingBooking@confirm_sightseeing_booking')->name('confirm-sightseeing-booking-operator');

	    //Itinerary
		Route::get('fetchItinerary','OperatorController@fetchItinerary')->name('fetchItinerary-operator');
		Route::get('/itinerary-details-view/{itinerary_id}','OperatorController@itinerary_details_view')->name('itinerary-details-view-operator');
		Route::get('/itinerary-get-hotels','OperatorController@itinerary_get_hotels')->name('operator-itinerary-get-hotels');
		Route::get('/itinerary-get-hotel-details','OperatorController@itinerary_get_hotel_details')->name('operator-itinerary-get-hotels-details');
		Route::get('/itinerary-get-activities','OperatorController@itinerary_get_activities')->name('operator-itinerary-get-activities');
		Route::get('/itinerary-get-transfer','OperatorController@itinerary_get_transfer')->name('operator-itinerary-get-transfer');

		Route::get('/itinerary-get-sightseeing','OperatorController@itinerary_get_sightseeing')->name('operator-itinerary-get-sightseeing');
		Route::post('itinerary-save','OperatorController@itinerary_save')->name('itinerary-save-operator');
		Route::post('/itinerary-booking-whole-cost','OperatorController@itinerary_booking_whole_cost')->name('itinerary-booking-whole-cost-operator');
		Route::post('/itinerary-booking','OperatorController@itinerary_booking')->name('itinerary-booking-operator');
		Route::get('/itinerary-get-restaurant','OperatorController@itinerary_get_restaurant')->name('operator-itinerary-get-restaurant');
		Route::get('/itinerary-get-restaurant-details','RestaurantController@itinerary_get_restaurant_details')->name('operator-itinerary-get-restaurant-details');
		Route::post('/confirm-itinerary-booking','OperatorController@confirm_itinerary_booking')->name('confirm-itinerary-booking-operator');





		//Group Itinerary
		Route::get('fetchGroupItinerary','OperatorController@fetchGroupItinerary')->name('fetchGroupItinerary-operator');
		Route::get('/group-itinerary-details-view/{itinerary_id}','OperatorController@group_itinerary_details_view')->name('group-itinerary-details-view-operator');
		Route::get('/group-itinerary-get-hotels','OperatorController@group_itinerary_get_hotels')->name('operator-group-itinerary-get-hotels');
		Route::get('/group-itinerary-get-hotel-details','OperatorController@group_itinerary_get_hotel_details')->name('operator-group-itinerary-get-hotels-details');
		Route::get('/group-itinerary-get-activities','OperatorController@group_itinerary_get_activities')->name('operator-group-itinerary-get-activities');
		Route::get('/group-itinerary-get-transfer','OperatorController@group_itinerary_get_transfer')->name('operator-group-itinerary-get-transfer');

		Route::get('/group-itinerary-get-sightseeing','OperatorController@group_itinerary_get_sightseeing')->name('operator-group-itinerary-get-sightseeing');
		Route::post('group-itinerary-save','OperatorController@group_itinerary_save')->name('group-itinerary-save-operator');
		Route::post('/group-itinerary-booking-whole-cost','OperatorController@group_itinerary_booking_whole_cost')->name('group-itinerary-booking-whole-cost-operator');
		Route::post('/group-itinerary-booking','OperatorController@group_itinerary_booking')->name('group-itinerary-booking-operator');
		Route::get('/group-itinerary-get-restaurant','OperatorController@group_itinerary_get_restaurant')->name('operator-group-itinerary-get-restaurant');
		Route::get('/group-itinerary-get-restaurant-details','RestaurantController@group_itinerary_get_restaurant_details')->name('operator-group-itinerary-get-restaurant-details');
		Route::post('/group-confirm-itinerary-booking','OperatorController@confirm_group_itinerary_booking')->name('confirm-group-itinerary-booking-operator');




		Route::get('restaurant-itinerary','CustomRestaurantItinerary@restaurant_itinerary')->name('restaurant-itinerary-operator');
		Route::post('restaurant-itinerary-details','CustomRestaurantItinerary@restaurant_itinerary_details_view')->name('restaurant-itinerary-details-operator');


		  //Transfers
		Route::post('fetchTransfer','OperatorTransferBooking@fetchTransfer')->name('fetchTransfer-operator');
		Route::get('fetchGuidesTransfer','OperatorTransferBooking@fetchGuidesTransfer')->name('fetchGuidesTransfer-operator');
		Route::get('/transfer-details-view/{transfer_id}','OperatorTransferBooking@transfer_details_view')->name('transfer-details-view-operator');
		Route::post('/transfer-booking','OperatorTransferBooking@transfer_booking')->name('transfer-booking-operator');
		Route::post('/confirm-transfer-booking','OperatorTransferBooking@confirm_transfer_booking')->name('confirm-transfer-booking-operator');




		//Restaurants
		Route::post('fetchRestaurant','OperatorRestaurantBooking@fetchRestaurant')->name('fetchRestaurant-operator');
		Route::get('/restaurant-details-view/{restaurant_id}','OperatorRestaurantBooking@restaurant_details_view')->name('restaurant-details-view-operator');
		Route::post('/restaurant-booking','OperatorRestaurantBooking@restaurant_booking')->name('restaurant-booking-operator');
		Route::post('/confirm-restaurant-booking','OperatorRestaurantBooking@confirm_restaurant_booking')->name('confirm-restaurant-booking-operator');



		Route::get('/fetch-saved-itineray','OperatorController@fetch_itineray')->name('fetch-saved-itineray-operator');
		Route::get('/download-saved-itineray','OperatorController@download_itinerary')->name('download-saved-itineray-operator');
		Route::get('/download-saved-restaurant-itineray','OperatorController@download_restaurant_itinerary')->name('download-saved-restaurant-itineray-operator');

		Route::get('/booking-success','OperatorController@booking_success')->name('booking-success-operator');
		Route::get('/booking-fail','OperatorController@booking_fail')->name('booking-fail-operator');

		//bookings
		Route::get('/operator-bookings','OperatorController@users_bookings')->name('operator-bookings');
		//payments
		Route::get('/get-payments','OperatorController@users_get_payments')->name('operator-get-payments');
		Route::post('/process-payments','OperatorController@process_payments')->name('process-payments-operator');
		Route::get('/process-online-payment','OperatorController@process_online_payment')->name('process-online-payment-operator');
		Route::get('/payment-processor','OperatorController@payment_processor')->name('operator-payment-processor');


		Route::get('/operator-invoice','OperatorController@users_invoice')->name('operator-invoice');
		Route::get('/customer-invoice','OperatorController@customer_invoice')->name('customer-invoice-operator');
		Route::get('/operator-bookings-tab','OperatorController@users_bookings_tab')->name('operator-bookings-tab');
		Route::post('/operator-bookings-cancel','OperatorController@users_bookings_cancel')->name('operator-bookings-cancel');


		//fetch booking attachments 
Route::get('/booking-attachments','OperatorController@booking_attachment_html')->name('booking-attachments-operator');
//update booking attachments
Route::post('/booking-attachments','OperatorController@booking_attachment_upload')->name('booking-attachments-operator');


		Route::get('/exchange-rates','OperatorController@exchange_rates')->name('operator-exchange-rates');


});

