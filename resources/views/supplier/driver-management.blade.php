<?php

use App\Http\Controllers\ServiceManagement;

?>
@include('supplier.includes.top-header')

<style>

    .iti-flag {

        width: 20px;

        height: 15px;

        box-shadow: 0px 0px 1px 0px #888;

        background-image: url("{{asset('assets/images/flags.png')}}") !important;

        background-repeat: no-repeat;

        background-color: #DBDBDB;

        background-position: 20px 0

    }



    div#cke_1_contents {

        height: 250px !important;

    }

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('supplier.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('supplier.includes.nav')

	<div class="content">

	<!-- Content Header (Page header) -->

	<div class="content-header">

		<div class="d-flex align-items-center">

			<div class="mr-auto">

				<h3 class="page-title">Driver Management</h3>

				<div class="d-inline-block align-items-center">

					<nav>

						<ol class="breadcrumb">

							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

							<li class="breadcrumb-item" aria-current="page">Dashboard</li>

							<li class="breadcrumb-item active" aria-current="page">Driver Management

							</li>

						</ol>

					</nav>

				</div>

			</div>

		 
		</div>

	</div>





	<div class="row">

	 <div class="col-12">

			<div class="box">





				<div class="box-body">



					<div class="row">

						<!--  <div class="col-sm-6 col-md-5">
                            <div class="input-group my-colorpicker2">

                                <input type="text" class="form-control" placeholder="Search...">

                                <div class="input-group-addon">
                                    <i class="fa fa-search"></i>
                                </div>
                            </div>
                        </div>
 -->


				
						<div class="col-sm-6 col-md-2">

							<div class="form-group">



								<a href="{{route('supplier-create-driver')}}"><button type="button"

										class="btn btn-rounded btn-success">Create Driver</button>

								</a>

							</div>

						</div>
						<!--  <div class="col-sm-6 col-md-2">
                            <div class="form-group">
                                <a href="#"><button type="button" class="btn btn-rounded btn-success">Upload
                                        File</button></a>
                                </a>
                            </div>


                        </div>
                        <div class="col-sm-6 col-md-2">
                            <div class="form-group">

                                <a href="#">
                                    <button type="button" class="btn btn-rounded btn-success">Export Data</button></a>
                                </a>
                            </div>
                        </div>
 -->
					

						

						<div class="col-12">

							<div class="box">



								<!-- /.box-header -->

								<div class="box-body" style="padding:0">

									<div class="table-responsive">

                                        <div class="row">

                                        <table id="example1" class="table table-bordered table-striped">

											<thead>

												<tr>

													<th>ID</th>

													<th>Name</th>

													<th>Contact</th>

													<th>Address</th>

													<th>City</th>

													<th>Country</th>
												
													<th>Status</th>

													<th>Approve Status</th>

													<th>Action</th>



												</tr>

											</thead>

											<tbody>

												@foreach($get_drivers as $driver)

												<tr id="tr_{{$driver->driver_id}}">

													<td>{{$driver->driver_id}}</td>

													<td>{{$driver->driver_first_name}} {{$driver->driver_last_name}}</td>

													<td>{{$driver->driver_contact}}</td>

													<td>{{$driver->driver_address}}</td>

													<td>
														<?php
														$fetch_city=ServiceManagement::searchCities($driver->driver_city,$driver->driver_country);
														echo $fetch_city['name'];
														?>
													</td>

													<td>
														@foreach($countries as $country)
															@if($country->country_id==$driver->driver_country)
													{{$country->country_name}}
															@endif

														@endforeach
														</td>
											
														<td>
															@if($driver->driver_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_supplier_{{$driver->driver_id}}">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_supplier_{{$driver->driver_id}}">InActive</button>
															@endif
														</td>
														<td>
															@if($driver->driver_approve_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary">Approved</button>
															@elseif($driver->driver_approve_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-default">Rejected</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button>
															@endif
														</td>
			

													<td>

														<a href="{{route('supplier-driver-details',['driver_id'=>$driver->driver_id])}}" target="_blank"><i class="fa fa-eye"></i></a>

														<a href="{{route('supplier-edit-driver',['driver_id'=>$driver->driver_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>

													</td>

												</tr>

												@endforeach





											</tbody>



										</table>

                                        </div>

									

									</div>

								</div>

								<!-- /.box-body -->

							</div>

						</div>

					</div>

				</div>


				<!-- /.row -->

			</div>

			<!-- /.box-body -->

		</div>



		<!-- /.box -->



	</div>



</div>

</div>

</div>

@include('supplier.includes.footer')

@include('supplier.includes.bottom-footer')

<script>

	$(document).ready(function()

	{

		$('#date_range').daterangepicker({

			locale: {

				format: 'YYYY-MM-DD',

				cancelLabel: 'Clear'

			}

		});

	});

</script>
 




</body>





</html>
