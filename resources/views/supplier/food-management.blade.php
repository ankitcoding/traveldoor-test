<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('supplier.includes.top-header')
<style>
    div#cke_1_contents {

        height: 250px !important;

    }
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('supplier.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('supplier.includes.nav')

	<div class="content">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title">Food / Drinks Management</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item" aria-current="page">Dashboard</li>
							<li class="breadcrumb-item active" aria-current="page">Food / Drinks Management
							</li>
						</ol>
					</nav>
				</div>
			</div>

		</div>
	</div>


	<div class="row">

		<div class="col-12">
			<div class="box">

				<div class="box-body">
					<!-- Nav tabs -->
					<!-- <ul class="nav nav-pills mb-20">
						<li class=" nav-item"> <a href="#navpills-restaurant" id="navpills-restaurant-link" class="nav-link show active" data-toggle="tab"
								aria-expanded="false">Restaurants</a> </li>

					</ul> -->
					<!-- Tab panes -->
					<!-- <div class="tab-content">
						<div id="navpills-restaurant" class="tab-pane show active"> -->
							<div class="row">
								<div class="col-sm-6 col-md-3">
									<div class="form-group">

										<a href="{{route('supplier-create-food')}}"><button type="button"
												class="btn btn-rounded btn-success">Create New Food / Drinks</button></a>
									</div>
								</div>
								
								<div class="col-12">
									<div class="box">

										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table  id="restaurant_table" class="table table-bordered table-striped datatable">
													<thead>
														<tr>
															<th>S. No.</th>
															<th style="display:none">FOOD / DRINKS ID</th>
															<th>Name</th>
															<th>Price</th>
															<th>Discounted Price</th>
															<th>Delivery</th>
															<th>Featured</th>
															<th>Restaurant</th>
															<th>Menu Category</th>
															<th>Status</th>
															<th>Approval</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														@php
														$srno=1;
														@endphp
															@foreach($get_food as $food)

												<tr id="tr_{{$food->restaurant_food_id}}">
													<td style="cursor: all-scroll;">{{$srno}}</td>
													<td style="display:none">{{$food->restaurant_food_id}}</td>
													<td>{{$food->food_name}}</td>
													<td>GEL {{$food->food_price}}</td>
													<td>@if($food->food_discounted_price!=null) GEL {{$food->food_discounted_price}} @else - @endif</td>
													<td>@if($food->food_available_for_delivery=="yes") <button type="button" class="btn btn-sm btn-primary">YES</button> @else <button type="button" class="btn btn-sm btn-danger">NO</button> @endif</td>
													<td>@if($food->food_featured=="yes") <button type="button" class="btn btn-sm btn-primary">YES</button> @else <button type="button" class="btn btn-sm btn-danger">NO</button> @endif</td>
													<td>{{$food->getRestaurant->restaurant_name}}</td>
													<td>{{$food->getMenuCategory->restaurant_menu_category_name}}</td>
									
														<td>
															@if($food->food_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" >Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" >InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($food->food_approval_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary">Approved</button>
															@elseif($food->food_approval_status==0)
															<button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button>
															@elseif($food->food_approval_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger">Rejected</button>
															@endif

															</td>
													<td>
														<a href="{{route('supplier-food-details',['food_id'=>$food->restaurant_food_id])}}" target="_blank"><i class="fa fa-eye"></i></a>
														
														<a href="{{route('supplier-edit-food',['food_id'=>$food->restaurant_food_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>
													</td>

												</tr>
												@php
														$srno++;
														@endphp

												@endforeach

													</tbody>

												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
								


							</div>
						<!-- </div>
					</div> -->
				</div>


				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
		

		<!-- /.box -->

	</div>

</div>
</div>

</div>

</div>
@include('supplier.includes.footer')

@include('supplier.includes.bottom-footer')
<script>
	$(document).ready(function()
	{
		$(".datatable").DataTable();
	})
	

</script>
</body>
</html>
