<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('supplier.includes.top-header')
<style>
    div#cke_1_contents {

        height: 250px !important;

    }

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('supplier.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('supplier.includes.nav')

	<div class="content">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title">Transfers</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item" aria-current="page">Dashboard</li>
							<li class="breadcrumb-item active" aria-current="page">Transfers
							</li>
						</ol>
					</nav>
				</div>
			</div>
			<!-- <div class="right-title">
				<div class="dropdown">
					<button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
							class="mdi mdi-dots-horizontal"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
						<a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
						<a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
						<a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
						<div class="dropdown-divider"></div>
						<button type="button" class="btn btn-rounded btn-success">Submit</button>
					</div>
				</div>
			</div> -->
		</div>
	</div>


	<div class="row">

		<div class="col-12">
			<div class="box">

				<div class="box-body">
					
					<div class="tab-content">
						<div id="navpills-1" class="tab-pane show active">
							<div class="row">


								<div class="col-sm-6 col-md-3">
									<div class="form-group">

										<a href="{{route('supplier-create-transfer')}}"><button type="button"
												class="btn btn-rounded btn-success">Create New Transfer</button></a>
									</div>
								</div>


								<div class="col-12">
									<div class="box">

										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table id="transfer_table" class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>ID</th>
															<th>Transfer Name</th>
															<th>Description</th>
															<th>Transfer Type</th>
															<th>No. of Transfers</th>
															<th>Country</th>
															<th>Status</th>
															<th>Approval</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
														@foreach($get_transfer as $transfer)
													<tr id="tr_{{$transfer->transfer_id}}">

	
													<td>{{$transfer->transfer_id}}</td>
													<td>{{$transfer->transfer_name}}</td>
													<td>{{$transfer->transfer_description}}</td>
													<td>
														@if($transfer->transfer_type="airport")
														Airport Transfer
														@else
														Normal Transfer
														@endif</td>
													<td>{{$transfer->no_of_transfer_available}}</td>
													<td>
														@foreach($countries as $country)
															@if($country->country_id==$transfer->transfer_country)
													{{$country->country_name}}
															@endif

														@endforeach
														</td>
														<td>
															@if($transfer->transfer_status==1)
															Active
															@else
															Inactive
															@endif
														</td>
														<td style="white-space: nowrap;">
														@if($transfer->transfer_approve_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" >Approved</button>
															@elseif($transfer->transfer_approve_status==0)
															<button type="button" class="btn btn-sm btn-rounded btn-warning" >Pending</button>
															@elseif($transfer->transfer_approve_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" >Rejected</button>
															@endif
															</td>


													<td>
														<a href="{{route('supplier-transfer-details',['transfer_id'=>$transfer->transfer_id])}}" target="_blank"><i class="fa fa-eye"></i></a>
														<a href="{{route('supplier-edit-transfer',['transfer_id'=>$transfer->transfer_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>

													</td>

												</tr>

												@endforeach
													</tbody>
												</table>

											</div>
										</div>
										<!-- /.box-body -->
									</div>
								</div>


							</div>
						</div>
						
					</div>
				</div>


				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>

		<!-- /.box -->

	</div>

</div>
</div>

</div>

</div>
@include('supplier.includes.footer')

@include('supplier.includes.bottom-footer')
</body>
</html>
