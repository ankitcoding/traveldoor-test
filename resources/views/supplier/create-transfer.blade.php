@include('supplier.includes.top-header')
<style>
    header.main-header {
        background: url("{{ asset('assets/images/color-plate/theme-purple.jpg') }}");
    }
/*  div#cke_1_contents {
height: 250px !important;
}*/
img.plus-icon {
    margin: 0 2px;
    display: inline !important;
}

</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('supplier.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('supplier.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Transfer</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                                <li class="breadcrumb-item" aria-current="page">Home</li>
                                                <li class="breadcrumb-item active" aria-current="page">Create New
                                                    Transfer
                                                </li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>

                            </div>
                        </div>
                      
                        <div class="row">
                            <div class="col-12">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">CREATE TRANSFER</h4>
                                    </div>
                                    <div class="box-body">
                                        <form id="transfer_form" enctype="multipart/form-data" method="POST">
                                            {{csrf_field()}}
                                              <input type="hidden" name="transfer_role" id="transfer_role" value="supplier">
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <label>TRANSFER NAME <span class="asterisk">*</span></label>
                                                        <input type="text" class="form-control" placeholder="FIRST NAME "
                                                        name="transfer_name" id="transfer_name">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                </div>
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <label>DESCRIPTION<span class="asterisk">*</span></label>
                                                        <textarea rows="5" cols="5" class="form-control"
                                                        placeholder="DESCRIPTION" name="transfer_description" id="transfer_description"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                </div>
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                    <label for="transfer_supplier_name">OWN SUPPLIER </label>
                                                    <input type="text" id="supplier_name_1" name="supplier_name_1" class="form-control" placeholder="Supplier Name" value="{{$supplier_name}}" readonly>
                                                    <input type="hidden" id="transfer_supplier_name" name="transfer_supplier_name" value="{{$supplier_id}}">
                                                  
                                                </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                </div>
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>COUNTRY <span class="asterisk">*</span></label>
                                                                <select class="form-control select2" name="transfer_country"
                                                                id="transfer_country" style="width: 100%;">
                                                                <option selected="selected" hidden value="0">SELECT
                                                                COUNTRY</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="transfer_type">Transfer Type <span class="asterisk">*</span></label>
                                                    <select id="transfer_type" name="transfer_type" class="form-control " style="width: 100%;">
                                                        <option value="0" hidden>SELECT TRANSFER TYPE</option>
                                                         <option value="from-airport">From Airport Transfer</option>
                                                        <option value="to-airport">To Airport Transfer</option>
                                                        <option value="city">City Transfer</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                     
                                        <div class="row mb-10">
                                            <div class="col-md-12">
                                             <div class="transfer_div" id="transfer_div__1">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>VEHICLE TYPE<span class="asterisk">*</span></label>
                                                     <select  class="form-control vehicle_type select2" style="width: 100%;" name="vehicle_type[]" id="vehicle_type__1">
                                                        <option value="0" hidden>SELECT VEHICLE TYPE</option>
                                                       @foreach($fetch_vehicle_type as $vehicle_type)
                                                        <option value="{{$vehicle_type->vehicle_type_id}}">{{$vehicle_type->vehicle_type_name}}</option>
                                                       @endforeach
                                                    </select>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>SELECT VEHICLE<span class="asterisk">*</span></label>
                                                     <select class="form-control vehicle select2" name="vehicle[]" style="width: 100%;" id="vehicle__1">
                                                        <option value="0" hidden>LIST OF VEHICLES</option>
                                                    </select>

                                                </div>
                                                 <div class="col-md-2">
                                                      <label>VEHICLE INFO <span class="asterisk">*</span></label>
                                                        <input type="text" class="form-control vehicle_info" placeholder="VEHICLE INFO "
                                                        name="vehicle_info[]" id="vehicle_info__1">
                                                </div>
                                                 <div class="col-md-3">
                                                      <label>VEHICLE NOTE </label>
                                                        <textarea  class="form-control vehicle_note" placeholder="VEHICLE NOTE"
                                                        name="vehicle_note[]" id="vehicle_note__1" rows=3></textarea>
                                                </div>
                                                <div class="col-md-2">
                                                      <label>VEHICLE IMAGES <span class="asterisk">*</span></label>
                                                        <input type="file" class="form-control  vehicle_images" name="vehicle_images[0][]" id="vehicle_images__1" multiple="multiple" accept="image/jpeg,image/jpg,image/png">
                                                        <small>Max Limit : 5</small>
                                                </div>
                                                <div class="col-md-12 preview_images">
                                                </div>
                                        
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                           <div id="airport_city_transfer_div_loader" style="display:none;">
                                            <h4 class='text-center'><strong>Loading...</strong></h4>
                                        </div>

                                        <div class="row mb-10" id="airport_city_transfer_div" style="display:none;">
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row mb-10">
                                                    <div class="col-sm-12">
                                                        <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                        </div>
                                                        <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                            <i class="fa fa-plus-circle"></i> INCLUSIONS </h4>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="box">
                                                                <!-- /.box-header -->
                                                                <div class="box-body">
                                                                    <textarea class="form-control" id="transfer_inclusions" name="transfer_inclusions"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row mb-10">
                                                        <div class="col-sm-12">
                                                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                            </div>
                                                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                                <i class="fa fa-plus-circle"></i> EXCLUSIONS </h4>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="box">
                                                                    <!-- /.box-header -->
                                                                    <div class="box-body">
                                                                        <textarea class="form-control" id="transfer_exclusions" name="transfer_exclusions"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row mb-10">
                                                            <div class="col-sm-12">
                                                                <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                                </div>
                                                                <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                                    <i class="fa fa-plus-circle"></i> CANCELLATION POLICY</h4>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="box">
                                                                        <!-- /.box-header -->
                                                                        <div class="box-body">
                                                                            <textarea class="form-control" id="transfer_cancellation" name="transfer_cancellation"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row mb-10">
                                                                <div class="col-sm-12">
                                                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                                    </div>
                                                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                                        <i class="fa fa-plus-circle"></i> TERMS AND CONDITIONS </h4>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="box">
                                                                            <!-- /.box-header -->
                                                                            <div class="box-body">
                                                                                <textarea class="form-control" id="transfer_terms_conditions" name="transfer_terms_conditions"></textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-10">
                                                            <div class="col-md-12">
                                                                <div class="box-header with-border"
                                                                style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
                                                                <button type="button" id="create_transfer"
                                                                class="btn btn-rounded btn-primary mr-10">Save</button>
                                                                <button type="button" id="discard_transfer"
                                                                class="btn btn-rounded btn-primary">Discard</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
        @include('supplier.includes.footer')
        @include('supplier.includes.bottom-footer')
    <script>
        function previewFile(data) {
            if (data == "logo") {
                var preview = document.getElementById('logo_preview');
                var file = document.querySelector('input[name="transfer_logo_file"]').files[0];
            } else {
                var preview = document.getElementById('certificate_preview');
                var file = document.querySelector('input[name="transfer_certificate_file"]').files[0];
            }
            var reader = new FileReader();
            reader.onloadend = function () {
                preview.src = reader.result;
                preview.style.display = "block";
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }
    </script>
      <script>
        var filePreview = function(input, id) {
            if (input.files) {
               $('#'+id).parent().parent().find(".preview_images").empty();
               var filesAmount = input.files.length;

               for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $('#'+id).parent().parent().find(".preview_images").append('<img src="'+event.target.result+'" width="150" height="150"/> &nbsp;');
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    };
    $(document).on("change",".vehicle_images",function()
    {         
        var id=this.id;
        if (parseInt($("#"+id).get(0).files.length) > 5){
                  alert("You are only allowed to upload a maximum of 5 files");
                  $("#"+id).val("");
                  $('#'+id).parent().parent().find(".preview_images").empty();
               }
               else
               {
                        filePreview(this,id);
               }

    });
    </script>
    <script>
        function dateshow()
        {
            var date = new Date();
            date.setDate(date.getDate());
            $('.transfer_validity_from').datepicker({
                autoclose:true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                startDate:date
            });
            $('.transfer_validity_from').on('change', function () {
                var date_from_id=this.id;
                var date_id=date_from_id.split("transfer_validity_from");
                var date_from = $("#transfer_validity_from"+date_id[1]).datepicker("getDate");
                var date_to = $("#transfer_validity_to"+date_id[1]).datepicker("getDate");
                if(!date_to)
                {
                    $("#transfer_validity_to"+date_id[1]).datepicker("setDate",date_from);
                }
                else if(date_to.getTime()<date_from.getTime())
                {
                    $("#transfer_validity_to"+date_id[1]).datepicker("setDate",date_from);
                }
            });
            $('.transfer_validity_to').datepicker({
                autoclose:true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                startDate:date
            });
            $('.transfer_validity_to').on('change', function () {
                var date_to_id=this.id;
                var date_id=date_to_id.split("transfer_validity_to");
                var date_from = $("#transfer_validity_from"+date_id[1]).datepicker("getDate");
                var date_to = $("#transfer_validity_to"+date_id[1]).datepicker("getDate");
                if(!date_from)
                {
                    $("#transfer_validity_from"+date_id[1]).datepicker("setDate",date_to);
                }
                else if(date_to.getTime()<date_from.getTime())
                {
                    $("#transfer_validity_from"+date_id[1]).datepicker("setDate",date_to);
                }
            });
        }
        $(document).ready(function()
        {
            CKEDITOR.replace('transfer_exclusions');
            CKEDITOR.replace('transfer_inclusions');
            CKEDITOR.replace('transfer_cancellation');
            CKEDITOR.replace('transfer_terms_conditions');
            $('.select2').select2();
            var date = new Date();
            date.setDate(date.getDate());
            $('#blackout_days').datepicker({
                multidate: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                startDate:date
            });
            dateshow();
        });
    </script>
    <script>
        $(document).ready(function()
        {

            var supplier_id=$("#transfer_supplier_name").val();
             if(supplier_id=="")
        {

        }
        else
        {
            $.ajax({
                url:"{{route('search-supplier-country')}}",
                type:"GET",
                data:{"supplier_id":supplier_id},
                success:function(response)
                {
                    $("#transfer_country").html(response);
                    $('#transfer_country').select2();
                    $("#transfer_country").prop("disabled",false);
                }
            });
        }
        

        });
        $(document).on("change","#transfer_supplier_name",function()
        {
            if($("#transfer_supplier_name").val()!="0")
            {
                var supplier_id=$(this).val();
                $.ajax({
                    url:"{{route('search-supplier-country')}}",
                    type:"GET",
                    data:{"supplier_id":supplier_id},
                    success:function(response)
                    {
                        $("#transfer_country").html(response);
                        $('#transfer_country').select2();
                        $("#transfer_country").prop("disabled",false);
                    }
                });
            }
        });
        $(document).on("change","#transfer_type",function()
        {
            var transfer_type=$(this).val();
            $("#airport_city_transfer_div_loader").show();
            $("#airport_city_transfer_div").html("");

           if(transfer_type=="from-airport")
            {
                var country_id=$("#transfer_country").val();
                $.ajax({
                    url:"{{route('fetchAirportTransferData')}}",
                    type:"GET",
                    data:{"country_id":country_id},
                    success:function(response)
                    {
                        $("#airport_city_transfer_div").html(response);
                        $("#transfer_from__1").select2();
                        $("#transfer_to__1").select2();
                        $("#airport_city_transfer_div").show();
                    },
                    complete: function() {
                     $("#airport_city_transfer_div_loader").hide();
                 }
                });
            }
            else if(transfer_type=="to-airport")
            {
                var country_id=$("#transfer_country").val();
                $.ajax({
                    url:"{{route('fetchCityTransferData')}}",
                    type:"GET",
                    data:{"country_id":country_id},
                    success:function(response)
                    {
                        $("#airport_city_transfer_div").html(response);
                        $("#transfer_from__1").select2();
                        $("#transfer_to__1").select2();
                        $("#airport_city_transfer_div").show();
                    },
                    complete: function() {
                     $("#airport_city_transfer_div_loader").hide();
                 }
                });
            }
            else if(transfer_type=="city")
            {
                var country_id=$("#transfer_country").val();
                $.ajax({
                    url:"{{route('fetchCityTransferData')}}",
                    type:"GET",
                    data:{"country_id":country_id},
                    success:function(response)
                    {
                        $("#airport_city_transfer_div").html(response);
                        $("#transfer_from__1").select2();
                        $("#transfer_to__1").select2();
                        $("#airport_city_transfer_div").show();
                         
                    },
                    complete: function() {
                     $("#airport_city_transfer_div_loader").hide();
                 }
                });
            }
            else
            {

            }


        });
        $(document).on("change",".transfer_from",function()
        {   
          var transfer_type=$("#transfer_type").val();
          var id=$(this).attr('id');
          var actual_id=id.split("__")[1];
          var city_id=$(this).val();
          var country_id=$("#transfer_country").val();
          if(transfer_type!="to-airport")
          {
           $.ajax({
            url:"{{route('fetchToCityTransferData')}}",
            type:"GET",
            data:{"country_id":country_id,
            "city_id":city_id,
            "index":actual_id},
            success:function(response)
            {
                $('#'+id).parent().parent().find('.destination_city_div').html(response);
                $("#airport_city_transfer_div").show();
                
            },
            complete: function() {
               $("#airport_city_transfer_div_loader").hide();
           }
       });
       }
       else
       {
           $.ajax({
            url:"{{route('fetchToAirportTransferData')}}",
            type:"GET",
            data:{"country_id":country_id,
            "city_id":city_id,
            "index":actual_id},
            success:function(response)
            {
                $('#'+id).parent().parent().find('.destination_city_div').html(response);
                $("#airport_city_transfer_div").show();
                
            },
            complete: function() {
               $("#airport_city_transfer_div_loader").hide();
           }
       });

       }
   });
    
        $(document).on("click",".add_more_city_transfer",function()
        {
        var clone_policies = $(".transfer_div:last").clone();
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var newer_id = $(".transfer_div:last").attr("id");
        new_id = newer_id.split('transfer_div');
        old_id=parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;

        clone_policies.find("select[name='transfer_from[]']").attr("id", "transfer_from__" + new_id).val("0").select2();
          clone_policies.find(".select2-container").slice(1).remove();
           clone_policies.find("select[name='transfer_from[]']").parent().parent().parent().attr("id","transfer_div" + new_id);
          clone_policies.find(".destination_city_div").empty();
      
        $("#transfer_div"+old_id).find(".add_more_city_transfer_div").html("");
        clone_policies.find(".vehicle_type_cost").each(function()
        {
            $(this).attr("name","transfer_vehiclecost["+old_id+"][]").val("0");

        });
        $("#transfer_div"+old_id).find(".add_more_city_transfer_div").html("");
        if(old_id>1)
        {
        // $("#transfer_div"+old_id).find(".add_more_city_transfer_div").append('<img id="remove_city_transfer'+old_id+'" class="remove_city_transfer minus-icon" src="'+minus_url+'" style="margin-left: auto;">');
        }
        clone_policies.find(".add_more_city_transfer_div").html('');
        clone_policies.find(".add_more_city_transfer_div").append(' <img id="remove_city_transfer'+new_id+'" class="remove_city_transfer minus-icon"  src="'+minus_url+'"   style="margin-left: auto;"> <img id="add_more_city_transfer'+new_id+'" class="add_more_city_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;"> ');
        $(".transfer_div:last").after(clone_policies);
        
        });
         $(document).on("click", ".remove_city_transfer", function () {
            var id = this.id;
            var split_id = id.split('remove_city_transfer');
            $("#transfer_div" + split_id[1]).remove();
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var last_id = $(".transfer_div:last").attr("id");
            old_id = last_id.split('transfer_div');
            old_id=parseInt(old_id[1]);
            if(old_id>1)
            {
                $("#transfer_div"+old_id).find(".add_more_city_transfer_div").html("");
                $("#transfer_div"+old_id).find(".add_more_city_transfer_div").append('<img id="remove_city_transfer'+old_id+'" class="remove_city_transfer minus-icon"  src="'+minus_url+'"   style="margin-left: auto;"> <img id="add_more_city_transfer'+old_id+'" class="add_more_city_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;">');
            }
            else
            {
                $("#transfer_div"+old_id).find(".add_more_city_transfer_div").html("");
                $("#transfer_div"+old_id).find(".add_more_city_transfer_div").append(' <img id="add_more_city_transfer'+old_id+'" class="add_more_city_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;">');
            }
        });
    </script>
    <script>
        $("#transfer_country").on("change", function () {
            if ($(this).val() != "0") {
                $("#city_div").show();
            }
        });
        $("#discard_transfer").on("click", function ()
        {
            window.history.back();
        });
        $(document).on("click", "#create_transfer", function ()
        {
            var transfer_name = $("#transfer_name").val();
            var transfer_description = $("#transfer_description").val();
            var transfer_supplier_name = $("#transfer_supplier_name").val();
            var transfer_country = $("#transfer_country").val();
            var transfer_type = $("#transfer_type").val();
            var transfer_inclusions= CKEDITOR.instances.transfer_inclusions.getData();
            var transfer_exclusions=CKEDITOR.instances.transfer_exclusions.getData();
            var transfer_cancellation=CKEDITOR.instances.transfer_cancellation.getData();
            var transfer_terms_conditions=CKEDITOR.instances.transfer_terms_conditions.getData();
            if (transfer_name.trim() == "")
            {
                $("#transfer_name").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_name").css("border", "1px solid #9e9e9e");
            }
            if (transfer_supplier_name == "0")
            {
                $("#transfer_supplier_name").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_supplier_name").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }
            if (transfer_country.trim() == "0")
            {
                $("#transfer_country").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_country").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }
            if (transfer_type.trim() == "0")
            {
                $("#transfer_type").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_type").css("border", "1px solid #9e9e9e");
            }

            if(transfer_type.trim() != "0")
            {
                var transfer_from_error=0;
                var transfer_to_error=0;
                var vehicle_type_cost_error=0;
                $("select[name='transfer_from[]']").each(function()
                {
                    if($(this).val()=="0")
                    {
                        $(this).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
                        $(this).parent().find(".select2-selection").focus();
                        transfer_from_error++;
                    }
                    else
                    {
                      $(this).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
                  }
              });
                $("select[name='transfer_to[]']").each(function()
                {
                    if($(this).val()=="0")
                    {
                        $(this).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
                        $(this).parent().find(".select2-selection").focus();
                        transfer_to_error++;
                    }
                    else
                    {
                      $(this).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
                  }
              });
                $(".vehicle_type_cost").each(function()
                {
                    if($(this).val()=="")
                    {
                        $(this).css("border", "1px solid #cf3c63");
                        $(this).focus();
                        vehicle_type_cost_error++;
                    }
                    else
                    {
                        $(this).css("border", "1px solid #9e9e9e");
                    }
                });
            }

              var vehicle_type_error=0;

             $("select[name='vehicle_type[]']").each(function()
                {
                    if($(this).val()=="0")
                    {
                        $(this).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
                        $(this).parent().find(".select2-selection").focus();
                        vehicle_type_error++;
                    }
                    else
                    {
                      $(this).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
                    }
                });

              var vehicle_error=0;
               $("select[name='vehicle[]']").each(function()
                {
                    if($(this).val()=="0")
                    {
                        $(this).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
                        $(this).parent().find(".select2-selection").focus();
                        vehicle_error++;
                    }
                    else
                    {
                      $(this).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
                    }
                });

                if ($("#vehicle_info__1").val().trim() == "")
            {
                $("#vehicle_info__1").css("border", "1px solid #cf3c63");
            } else
            {
                $("#vehicle_info__1").css("border", "1px solid #9e9e9e");
            }

            
            if (transfer_name.trim() == "") {
                $("#transfer_name").focus();
            }  else if (transfer_supplier_name == "0") {
                $("#transfer_supplier_name").parent().find(".select2-selection").focus();
            } else if (transfer_country.trim() == "0") {
                $("#transfer_country").parent().find(".select2-selection").focus();
            } 
            else if (transfer_type.trim() == "0") {
                $("#transfer_type").focus();
            } 
            else if (transfer_from_error > 0) {
            }
            else if (transfer_to_error > 0) {
            }
            else if (vehicle_type_cost_error > 0) {
            }
             else if (vehicle_type_error > 0) {
            }
             else if (vehicle_error > 0) {
            }
            else if($("#vehicle_info__1").val().trim() == "")
            {
               $("#vehicle_info__1").focus(); 
            }
            else
            {
                $("#create_transfer").prop("disabled",true);
                var formdata = new FormData($("#transfer_form")[0]);
                formdata.append("transfer_inclusions",transfer_inclusions);
                formdata.append("transfer_exclusions",transfer_exclusions);
                formdata.append("transfer_cancellation",transfer_cancellation);
                formdata.append("transfer_terms_conditions",transfer_terms_conditions);
                $.ajax({
                    url: "{{route('insert-transfer')}}",
                    enctype: 'multipart/form-data',
                    data: formdata,
                    type: "POST",
                    processData: false,
                    contentType: false,
                    success: function (response)
                    {
                        if (response.indexOf("success") != -1)
                        {
                            swal({
                                title: "Success",
                                text: "Transfer Created Successfully !",
                                type: "success"
                            },
                            function () {
                                location.reload();
                            });
                        } else if (response.indexOf("fail") != -1)
                        {
                            swal("ERROR", "Transfer cannot be inserted right now! ");
                        }
                        $("#create_transfer").prop("disabled", false);
                    }
                });
            }
        });

$(document).on("change",".vehicle_type",function()
{
    var vehicle_type_id=$(this).val();
    var id=$(this).attr("id").split("__")[1];

    $.ajax({
        url:"{{route('fetchVehicle')}}",
        type:"GET",
        data:{"vehicle_type_id":vehicle_type_id,},
        success:function(response)
        {
         $("#vehicle__"+id).html(response);    
     }
 });

});
    </script>
</body>
</html>
</body>
</html>