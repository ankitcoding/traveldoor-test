<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('supplier.includes.top-header')
<style>
    .carousel-item img {
        height: 100%;
    }
    .carousel-item {
        height: 100%;
    }
    p.start_price {
        margin: 0;
    }
    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }
    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;
        border-radius: 5px;
    }
    .hotel_detail {
        height: 150px;
    }
    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }
    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }
    .booking_detail {
        padding: 15px;
    }
    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }
    td p {
        margin: 0;
    }
    td {
        background: gainsboro;
    }
    table {
        border-collapse: separate;
    }
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #59d25a;
    }
    a.panel-title {
        position: relative !important;
        background: #dfffe3;
        color: green !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #3ca23d; */
    }
    .panel-title {
        display: block;
        margin-top: 0;
        margin-bottom: 0;
        padding: 1.25rem;
        font-size: 18px;
        color: #4d4d4d;
        height: 48px;
    }
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }
    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }
    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
        width: auto;
        max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #279628;
        z-index: 999;
        transform: none !important;
    }
    .tab-content {
        margin-top: 10px;
    }
    div.panel-heading {
        border: 1px solid #59d25a !important;
    }
    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
    div#carousel-example-generic-captions {
        width: 100%;
    }
    .table tr td, .table tr th {
        white-space: nowrap !important;
        padding: 10px 20px !important;
        background: white !important;
    }
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('supplier.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('supplier.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">My Bookings</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item active" aria-current="page">My Bookings
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="box">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="booking_table" class="table table-striped table-condensed">
                                                <thead>
                                                    <tr>
                                                        <td>Booking ID</td>
                                                        <td>Booking Type</td>
                                                        <td>Type Name</td>
                                                        <td>Name</td>
                                                        <td>Email</td>
                                                        <td>Mobile</td>
                                                        <td>Booking Date</td>
                                                        <td>Amount</td>
                                                        <td>Admin's Approval</td>
                                                        <td>Admin's Remarks</td>
                                                        <td>Your Remarks</td>
                                                        <td>Action</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($fetch_bookings as $bookings)
                                                    @if($bookings->booking_type=="sightseeing")
                                                    @continue
                                                    @endif
                                                    <tr>
                                                        <td>{{$bookings->booking_sep_id}}</td>
                                                        <td>{{ucwords($bookings->booking_type)}}</td>
                                                        <td>@php
                                                            $booking_type_id=$bookings->booking_type_id;
                                                            if($bookings->booking_type=="activity")
                                                            {
                                                                $fetch_activity=ServiceManagement::searchActivity($booking_type_id);
                                                                echo $fetch_activity['activity_name'];
                                                            }
                                                            elseif($bookings->booking_type=="hotel")
                                                            {
                                                                $fetch_hotel=ServiceManagement::searchHotel($booking_type_id);
                                                                echo $fetch_hotel['hotel_name'];
                                                            }
                                                            else if($bookings->booking_type=="guide")
                                                            {
                                                                $fetch_guide=ServiceManagement::searchGuide($booking_type_id);
                                                                echo $fetch_guide['guide_first_name']." ".$fetch_guide['guide_last_name'];
                                                            }
                                                            else if($bookings->booking_type=="driver")
                                                            {
                                                                $fetch_driver=ServiceManagement::searchDriver($booking_type_id);
                                                                echo $fetch_driver['driver_first_name']." ".$fetch_driver['driver_last_name'];
                                                            }
                                                            else if($bookings->booking_type=="sightseeing")
                                                            {
                                                                $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($booking_type_id);
                                                                echo $fetch_sightseeing['sightseeing_tour_name'];
                                                            }
                                                            else if($bookings->booking_type=="restaurant")
                                                        {
                                                            $fetch_restaurant=ServiceManagement::searchRestaurant($booking_type_id);
                                                            echo $fetch_restaurant['restaurant_name'];
                                                        }
                                                            else if($bookings->booking_type=="transfer")
                                                            {
                                                                
                                                                echo $bookings->booking_subject_name;
                                                            }
                                                            
                                                        @endphp</td>
                                                        <td>{{$bookings->customer_name}}</td>
                                                        <td>{{$bookings->customer_email}}</td>
                                                        <td>{{$bookings->customer_contact}}</td>
                                                        <td>{{$bookings->booking_date}}</td>
                                                        <td>{{$bookings->booking_currency}} {{$bookings->booking_supplier_amount}}
                                                            @php
                                                            // $adult_price=$bookings->supplier_adult_price*$bookings->booking_adult_count;
                                                            //$child_price=$bookings->supplier_child_price*$bookings->booking_child_count;
                                                            //$total_price=$adult_price+$child_price;
                                                            //echo $total_price;
                                                        @endphp</td>
                                                        @if($bookings->booking_status==2)
                                                        <td><button class="btn btn-sm btn-info">Cancelled</button></td>
                                                        @elseif($bookings->booking_admin_status==0)
                                                        <td><button class="btn btn-sm btn-warning">Pending</button></td>
                                                        @elseif($bookings->booking_admin_status==1)
                                                        <td><button class="btn btn-sm btn-success">Confirmed</button></td>
                                                        @elseif($bookings->booking_admin_status==2)
                                                        <td><button class="btn btn-sm btn-danger">Rejected</button></td>
                                                        @endif
                                                        <td>{{$bookings->booking_admin_message}}</td>
                                                        <td id="remarks_booking_{{$bookings->booking_id}}">{{$bookings->booking_supplier_message}}</td>
                                                        <td>
                                                            <span>
                                                                @if($bookings->booking_status==2)
                                                                <button type="button" class="btn btn-sm btn-rounded btn-info" disabled="disabled">Cancelled</button>
                                                                @elseif($bookings->booking_admin_status==2)
                                                                <button class="btn btn-sm btn-danger" disabled="disabled">Rejected</button>
                                                                @elseif($bookings->booking_supplier_status==1)
                                                                <button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
                                                                @elseif($bookings->booking_supplier_status==0)
                                                                <button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_booking_{{$bookings->booking_id}}">Approve</button>
                                                                <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_booking_{{$bookings->booking_id}}">Reject</button>
                                                                @elseif($bookings->booking_supplier_status==2)
                                                                <button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
                                                                @endif
                                                            </span>
                                                            <a href="javascript:void(0)" class="btn btn-rounded btn-success btn-sm">View</a></td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('supplier.includes.footer')
        @include('supplier.includes.bottom-footer')
        <div class="modal" id="actionModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Peform Operation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="action_id" id="action_id">
                                <div class="form-group">
                                    <label for="remarks" id="remarks_label">Remarks for Approval</label>
                                    <textarea name="remarks" id="remarks" class="form-control" rows="5"></textarea>
                                </div>
                                
                                
                                <button type="button" id="actionButton" class="btn btn-primary pull-right">Submit</button>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function()
            {
                $('#booking_table').DataTable();
                $(document).on("click",".approve",function()
                {
                    var id=this.id;
                    $("#action_id").attr("value",id);
                    $("#actionModal").find(".modal-title").text("Perform Approval Operation");
                    $("#remarks_label").text("Remarks for Approval");
                    $("#remarks").val("");
                    $("#actionModal").modal("show");
                });
                $(document).on("click",".reject",function()
                {
                    var id=this.id;
                    $("#action_id").attr("value",id);
                    $("#actionModal").find(".modal-title").text("Perform Rejection Operation");
                    $("#remarks_label").text("Remarks for Rejection");
                    $("#remarks").val("");
                    $("#actionModal").modal("show");
                });
                $(document).on("click",'#actionButton',function()
                {
                    var id=$("#action_id").val();
                    var actual_id=id.split("_");
                    var action_perform=actual_id[0];
                    var action_id=actual_id[2];
                    var remarks=$("#remarks").val();
                    swal({
                        title: "Are you sure?",
                        text: "You want to "+action_perform+" this booking !",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Go Ahead",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    }, function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url:"{{route('supplier-approve-booking')}}",
                                type:"POST",
                                data:{"_token":"{{csrf_token()}}",
                                "action_perform":action_perform,
                                "booking_id":action_id,
                                "remarks":remarks},
                                success:function(response)
                                {
                                    if(action_perform=="approve")
                                    {
                                        if(response.indexOf("success")!=-1)
                                        {
                                            $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');
                                            $("#remarks_booking_"+action_id).text(remarks);
                                            swal("Approved!", "Selected Booking has been approved.", "success");
                                        }
                                        else if(response.indexOf("cancel")!=-1)
                                        {
                                            
                                            swal("Error", "This booking is cancelled by customer , so it cannot be approved", "error");
                                        }
                                        else
                                        {
                                            swal("Error", "Unable to perform this operation", "error");
                                        }
                                    }
                                    else
                                    {
                                        if(response.indexOf("success")!=-1)
                                        {
                                            $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');
                                            $("#remarks_booking_"+action_id).text(remarks);
                                            swal("Rejected!", "Selected Booking has been rejected.", "success");
                                        }
                                        else if(response.indexOf("cancel")!=-1)
                                        {
                                            
                                            swal("Error", "This booking is cancelled by customer , so it cannot be rejected", "error");
                                        }
                                        else
                                        {
                                            swal("Error", "Unable to perform this operation", "error");
                                        }
                                    }
                                    $("#actionModal").modal("hide");
                                    
                                }
                            });
                        } else {
                            swal("Cancelled", "Operation Cancelled", "error");
                        }
                    });
                });
});
</script>
</body>
</html>