

@include('supplier.includes.top-header')

<style>

	.iti-flag {

		width: 20px;

		height: 15px;

		box-shadow: 0px 0px 1px 0px #888;

		background-image: url("{{asset('assets/images/flags.png')}}") !important;

		background-repeat: no-repeat;

		background-color: #DBDBDB;

		background-position: 20px 0

	}



	div#cke_1_contents {

		height: 250px !important;

	}

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

	<div class="wrapper">

		@include('supplier.includes.top-nav')

		<div class="content-wrapper">

			<div class="container-full clearfix position-relative">	

				@include('supplier.includes.nav')

				<div class="content">

					<!-- Content Header (Page header) -->

					<div class="content-header">

						<div class="d-flex align-items-center">

							<div class="mr-auto">

								<h3 class="page-title">Restaurants</h3>

								<div class="d-inline-block align-items-center">

									<nav>

										<ol class="breadcrumb">

											<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

											<li class="breadcrumb-item" aria-current="page">Home</li>

											<li class="breadcrumb-item active" aria-current="page">View Restaurant Details

											</li>

										</ol>

									</nav>

								</div>

							</div>
							</div>

						</div>


						<div class="row">







							<div class="col-12">

								<div class="box">

									<div class="box-body">

										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_name"><strong>RESTAURANT NAME :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_name"> @if($get_restaurant->restaurant_name!="" && $get_restaurant->restaurant_name!="0" && $get_restaurant->restaurant_name!=null){{$get_restaurant->restaurant_name}} @else No Data Available @endif </p>

											</div>

										</div>


										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_type"><strong>RESTAURANT TYPE :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_type"> @if($get_restaurant->restaurant_type!="" && $get_restaurant->restaurant_type!="0" && $get_restaurant->restaurant_type!=null)
													{{$get_restaurant->getrestaurantType->restaurant_type_name}}
													 @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_owner_name"><strong>OWNER NAME :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_owner_name"> @if($get_restaurant->restaurant_owner_name!="" && $get_restaurant->restaurant_owner_name!="0" && $get_restaurant->restaurant_owner_name!=null){{$get_restaurant->restaurant_owner_name}} @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_email_address"><strong>EMAIL ADDRESS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_email_address"> @if($get_restaurant->restaurant_email!="" && $get_restaurant->restaurant_email!="0" && $get_restaurant->restaurant_email!=null){{$get_restaurant->restaurant_email}} @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_contact"><strong>CONTACT NUMBER :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_contact"> @if($get_restaurant->restaurant_contact!="" && $get_restaurant->restaurant_contact!="0" && $get_restaurant->restaurant_contact!=null){{$get_restaurant->restaurant_contact}} @else No Data Available @endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_address"><strong>RESTAURANT ADDRESS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_address"> @if($get_restaurant->restaurant_address!="" && $get_restaurant->restaurant_address!="0" && $get_restaurant->restaurant_address!=null){{$get_restaurant->restaurant_address}} @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="supplier_id"><strong>SUPPLIER NAME :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="supplier_id"> @if($get_restaurant->supplier_id!="" && $get_restaurant->supplier_id!=null)
													{{$get_restaurant->getSupplier->supplier_name}}
												@else No Data Available @endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_country"><strong>COUNTRY :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_country"> @if($get_restaurant->restaurant_country!="" && $get_restaurant->restaurant_country!=null)
													{{$get_restaurant->getCountry->country_name}}
												@else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_city"><strong>CITY :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_city"> @if($get_restaurant->restaurant_city!="" && $get_restaurant->restaurant_city!=null)
													{{$get_restaurant->getCity->name}}
												@else No Data Available @endif </p>

											</div>

										</div>
								
										<div class="row">

											<div class="col-md-3">

												<label for="validity_fromdate"><strong>RESTAURANT AVAILABILITY :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="validity_fromdate"> @if($get_restaurant->validity_fromdate!="" && $get_restaurant->validity_fromdate!="0" && $get_restaurant->validity_fromdate!=null) @php echo date('d-F-Y',strtotime($get_restaurant->validity_fromdate)) @endphp @else No Data Available @endif To  @if($get_restaurant->validity_todate!="" && $get_restaurant->validity_todate!="0" && $get_restaurant->validity_todate!=null) @php echo date('d-F-Y',strtotime($get_restaurant->validity_todate)) @endphp @else No Data Available @endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="validity_fromtime"><strong>TIMINGS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="validity_fromtime"> @if($get_restaurant->validity_fromtime!="" && $get_restaurant->validity_fromtime!="0" && $get_restaurant->validity_fromtime!=null) {{$get_restaurant->validity_fromtime}} @else No Data Available @endif To  @if($get_restaurant->validity_totime!="" && $get_restaurant->validity_totime!="0" && $get_restaurant->validity_totime!=null) {{$get_restaurant->validity_totime}} @else No Data Available @endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="operating_weekdays"><strong>WORKING DAYS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="operating_weekdays"> @if($get_restaurant->operating_weekdays!="" && $get_restaurant->operating_weekdays!=null)
													@php
													$weekdays=unserialize($get_restaurant->operating_weekdays);
													$show_days=array();
													foreach($weekdays as $key=>$value)
													{
														if($value=="Yes")
														{
															array_push($show_days,ucfirst($key));
														}
													}

													echo implode(" ,",$show_days);
													@endphp
													 @else No Data Available @endif 
													 </p>

											</div>

										</div>


										<div class="row">

											<div class="col-md-3">

												<label for="no_of_tables"><strong>NO. OF TABLES :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="no_of_tables"> @if($get_restaurant->no_of_tables!="" && $get_restaurant->no_of_tables!="0" && $get_restaurant->no_of_tables!=null){{$get_restaurant->no_of_tables}} @else No Data Available @endif </p>

											</div>

										</div>

											<div class="row">

											<div class="col-md-3">

												<label for="restaurant_status"><strong>ACTIVE STATUS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_status">@if($get_restaurant->restaurant_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" >InActive</button>
															@endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_approve_status"><strong>APPROVE STATUS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_approve_status">
													@if($get_restaurant->restaurant_approve_status==1)
													<button type="button" class="btn btn-sm btn-rounded btn-primary">Approved</button>
													@elseif($get_restaurant->restaurant_approve_status==0)
													<button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button>
													@else
													<button type="button" class="btn btn-sm btn-rounded btn-default" >Rejected</button>
													@endif 
											</p>

											</div>

										</div>
				

									
										<div class="row" style="display: none;">

											<div class="col-md-3">

												<label for="operating_weekdays"><strong>CURRENCY :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="operating_weekdays"> @if($get_restaurant->restaurant_currency!="" && $get_restaurant->restaurant_currency!=null)
													@foreach($currency as $curr)

											 		@if($curr->code==$get_restaurant->restaurant_currency)
											 		{{$curr->code}} ({{$curr->name}})
													
											 		@endif
											 		@endforeach
													 @else No Data Available @endif 
													 </p>

											</div>

										</div>
											<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="blackout_days"></i> BLACKOUT DAYS

												</h4>

											</div>



										</div>
										<div id="blackout_days_details">
											@if($get_restaurant->restaurant_blackout_dates!="" && $get_restaurant->restaurant_blackout_dates!=null)
											<div class="row">
											@php
											$blackout_dates=explode(',',$get_restaurant->restaurant_blackout_dates);	
											
											for($black=0;$black< count($blackout_dates);$black++)
											{
												@endphp
												
													<div class="col-md-2">

												<label for="blackout_dates{{$black}}"><strong>DAY {{($black+1)}} :</strong></label>

											</div>

											<div class="col-md-2">

												<p class="" id="blackout_dates{{$black}}">
												 @php
												echo date('d-m-Y',strtotime($blackout_dates[$black]));
												 @endphp </p>

											</div>

												@php
											}
											@endphp
										</div>
											@else
											No Data Available 
											@endif
										</div>


										<div class="row mb-10" style="display: none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="restaurant_inclusions"></i> INCLUSIONS

												</h4>

											</div>



										</div>
										<div class="row" id="restaurant_inclusions_details" style="display: none" >

											<div class="col-md-12">
												<textarea id="restaurant_inclusions_data">
										 @if($get_restaurant->restaurant_inclusions!="" && $get_restaurant->restaurant_inclusions!=null) {{$get_restaurant->restaurant_inclusions}} @else No Data Available @endif
										 </textarea>

											</div>

										</div>

											
										<div class="row mb-10" style="display: none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="restaurant_exclusions"></i> EXCLUSIONS

												</h4>

											</div>



										</div>
										<div class="row" id="restaurant_exclusions_details" style="display: none" >

											
											<div class="col-md-12">
												<textarea id="restaurant_exclusions_data">
										 @if($get_restaurant->restaurant_exclusions!="" && $get_restaurant->restaurant_exclusions!=null) {{$get_restaurant->restaurant_exclusions}} @else No Data Available @endif
										</textarea>

											</div>

										</div>

										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="restaurant_description"></i> DESCRIPTION

												</h4>

											</div>



										</div>
										<div class="row" id="restaurant_description_details" >

											<div class="col-md-12">
												<textarea id="restaurant_description_data">
										 @if($get_restaurant->restaurant_description!="" && $get_restaurant->restaurant_description!=null) {{$get_restaurant->restaurant_description}} @endif
										 </textarea>

											</div>

										</div>
										<div class="row mb-10" style="display: none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="restaurant_cancel_policy"></i> CANCELLATION POLICY

												</h4>

											</div>



										</div>
										<div class="row" id="restaurant_cancel_policy_details" style="display: none">

											<div class="col-md-12">
												<textarea id="restaurant_cancel_policy_data">
										 @if($get_restaurant->restaurant_cancel_policy!="" && $get_restaurant->restaurant_cancel_policy!=null) {{$get_restaurant->restaurant_cancel_policy}} @else No Data Available @endif
										</textarea>

											</div>

										</div>
										<div class="row mb-10" style="display: none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="restaurant_terms_conditions"></i> TERMS AND CONDITIONS

												</h4>

											</div>



										</div>
										<div class="row" id="restaurant_terms_conditions_details" style="display: none">

											<div class="col-md-12">
												<textarea id="restaurant_terms_conditions_data">
										 @if($get_restaurant->restaurant_terms_conditions!="" && $get_restaurant->restaurant_terms_conditions!=null) {{$get_restaurant->restaurant_terms_conditions}} @else No Data Available @endif
										</textarea>

											</div>

										</div>
										<br>
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="restaurant_images"></i> RESTAURANT IMAGES

												</h4>

											</div>



										</div>
										<div class="row" id="restaurant_images_details">

											@php
											$get_restaurant_images=unserialize($get_restaurant->restaurant_images);
											for($images=0;$images< count($get_restaurant_images);$images++)
											{
												@endphp
												<div class='col-md-3'>
													<img class='upload_ativity_images_preview' src='{{ asset("assets/uploads/restaurant_images") }}/{{$get_restaurant_images[$images]}}' width=150 height=150 class="img img-thumbnail" />

												</div>
												@php
											}
											@endphp

										</div>
										<br>
										<div class="row mb-10">
											<div class="col-md-12">
												<div class="box-header with-border"
												style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
												<button type="button" id="discard_restaurant" class="btn btn-rounded btn-primary">BACK</button>
												<a href="{{route('supplier-edit-restaurant',['restaurant_id'=>$get_restaurant->restaurant_id])}}" id="update_restaurant" class="btn btn-rounded btn-primary mr-10">EDIT</a>
											</div>
										</div>
									</div>





									<!-- /.row -->

								</div>

								<!-- /.box-body -->

							</div>
							<!-- /.box -->
						</div>
					</div>
				</div>
			</div>
		</div>

		@include('supplier.includes.footer')

		@include('supplier.includes.bottom-footer')



		<script>
			$(document).ready(function()
			{
				CKEDITOR.replace('restaurant_description_data', {readOnly:true});
				CKEDITOR.replace('restaurant_exclusions_data', {readOnly:true});
				CKEDITOR.replace('restaurant_inclusions_data',{readOnly:true});
				CKEDITOR.replace('restaurant_cancel_policy_data',{readOnly:true});
				CKEDITOR.replace('restaurant_terms_conditions_data',{readOnly:true});

			});
			$(document).on("click","#discard_restaurant",function()
			{
				window.history.back();

			});
			$(document).on("click","#restaurant_inclusions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#restaurant_inclusions_details").toggle();



			});
			$(document).on("click","#restaurant_exclusions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#restaurant_exclusions_details").toggle();



			});

			$(document).on("click","#restaurant_description",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#restaurant_description_details").toggle();



			});
			$(document).on("click","#restaurant_cancel_policy",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#restaurant_cancel_policy_details").toggle();



			});

	$(document).on("click","#restaurant_terms_conditions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#restaurant_terms_conditions_details").toggle();



			});

			$(document).on("click","#blackout_days",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#blackout_days_details").toggle();



			});



			$(document).on("click","#restaurant_images",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#restaurant_images_details").toggle();



			});
		</script>



	</body>





	</html>

