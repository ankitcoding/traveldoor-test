

      <!-- Left side column. contains the logo and sidebar -->

      <aside class="main-sidebar">

          <!-- sidebar-->

          <section class="sidebar">

            <!-- sidebar menu-->

            <ul class="sidebar-menu" data-widget="tree">



              



              <li class="link-a">

                <a href="{{route('supplier-home')}}">

                  <i class="ti-dashboard"></i>

                  <span>Home</span>

                  

                </a>

                

              </li>  
 

                <?php
                $service_type=session()->get('travel_supplier_type');

                $services=explode(',',$service_type);

                for($count=0;$count< count($services);$count++)
                {
                  if($services[$count]=="transportation")
                  {
                    $url="supplier-transport";
                  }
                  else if($services[$count]=="restaurant")
                  {
                    $url_restaurant="supplier-restaurant";
                    $url_food="supplier-food";
                    echo '<li class="treeview 5_menu">

                <a href="#">
                    <i class="fa fa-cogs"></i>
                    <span>Restaurants</span>
                    <span class="pull-right-container"><i class="fa fa-angle-right pull-right"></i></span>
                </a>
                <ul class="treeview-menu" style="display: none;">

                    <li><a href="'.route("$url_restaurant").'"><i class="fa fa-circle-o"></i> Restaurant Management</a></li>
                     <li><a href="'.route("$url_food").'"><i class="fa fa-circle-o"></i> Food / Drinks Management</a></li>
            </ul>
        </li>';

                  }
                  else
                  {
                    $url="supplier-".$services[$count];
                        echo '<li>
                  <a href="'.route("$url").'">
                    <i class="ti-email"></i> <span>'.ucwords($services[$count]).'</span>
                  </a>
                </li>';
                  }
                  

              

                }
                ?>

                  <li class="link-a">

                  <a href="{{route('supplier-bookings')}}">

                    <i class="ti-ticket"></i>

                    <span>Bookings</span>

                    

                  </a>

                  

                </li> 

                 <li class="link-a">

                  <a href="{{route('my-wallet-own-supplier')}}">

                    <i class="ti-wallet"></i>

                    <span>My Wallet</span>

                    

                  </a>

                
                </li> 




            </ul>

          </section>

      </aside>