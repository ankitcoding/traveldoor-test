@include('supplier.includes.top-header')
<style>
    header.main-header {
          background: url("{{ asset('assets/images/color-plate/theme-purple.jpg') }}");
    }

    .iti-flag {
        width: 20px;
        height: 15px;
        box-shadow: 0px 0px 1px 0px #888;
        background-image: url("flags.png") !important;
        background-repeat: no-repeat;
        background-color: #DBDBDB;
        background-position: 20px 0
    }

    div#cke_1_contents {
        height: 250px !important;
    }

    table#calendar-demo {
        width: 100%;
        height: 275px !important;
        min-height: 275px !important;
        overflow: hidden;
    }

    .calendar-wrapper.load {
        width: 100%;
        height: 276px;
    }

    .calendar-date-holder .calendar-dates .date.month a {
        display: block;
        padding: 17px 0 !important;
    }

    .calendar-date-holder {
        width: 100% !important;
    }

    section.calendar-head-card {
        display: none;
    }

    .calendar-container {
        border: 1px solid #cccccc;
        height: 276px !important;
    }

    img.plus-icon {
        margin: 0 2px;
        display: inline !important;
    }

    @media screen and (max-width:400px) {
        .calendar-date-holder .calendar-dates .date a {
            text-decoration: none;
            display: block;
            color: inherit;
            padding: 3px !important;
            margin: 1px;
            outline: none;
            border: 2px solid transparent;
            transition: all .3s;
            -o-transition: all .3s;
            -moz-transition: all .3s;
            -webkit-transition: all .3s;
        }
    }
     div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
div#loaderModal {
    background: #0000005c;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('supplier.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('supplier.includes.nav')

                <div class="content">

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Restaurant Management</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                            <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                            <li class="breadcrumb-item" aria-current="page">Restaurant Management</li>
                            <li class="breadcrumb-item active" aria-current="page">Create New Restaurant
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
           <!--  <div class="right-title">
                <div class="dropdown">
                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
                            class="mdi mdi-dots-horizontal"></i></button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Restaurant</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
                        <div class="dropdown-divider"></div>
                        <button type="button" class="btn btn-rounded btn-success">Submit</button>
                    </div>
                </div>
            </div> -->
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Create Restaurant</h4>
                </div>
                <div class="box-body">
                    <form id="restaurant_form" encytpe="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row mb-10">
                        <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                            <input type="hidden" name="restaurant_role" id="restaurant_role" value="supplier">
                        <label for="restaurant_type">RESTAURANT TYPE <span class="asterisk">*</span></label>
                         <select id="restaurant_type" name="restaurant_type" class="form-control">
                            <option value="0">--SELECT RESTAURANT TYPE--</option>
                            @foreach($fetch_restaurant_type as $restaurant_type)
                            <option value="{{$restaurant_type->restaurant_type_id}}">{{$restaurant_type->restaurant_type_name}}</option>
                            @endforeach
                        </select>
                        </div>
                        </div>
                        </div> 

                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="restaurant_name">RESTAURANT NAME <span class="asterisk">*</span></label>
                                <input type="text" id="restaurant_name" name="restaurant_name" class="form-control" placeholder="RESTAURANT NAME  ">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                             <div class="form-group">
                                <label for="restaurant_name">OWNER NAME<span class="asterisk">*</span></label>
                                <input type="text" id="restaurant_owner_name" name="restaurant_owner_name" class="form-control" placeholder="RESTAURANT OWNER">
                            </div>
                        </div>
                    </div>
                     <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="restaurant_name">RESTAURANT EMAIL ADDRESS <small>(Optional)</small></label>
                                <input type="text" id="restaurant_email_address" name="restaurant_email_address" class="form-control" placeholder="EMAIL ADDRESS">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                             <div class="form-group">
                                <label for="restaurant_name">RESTAURANT CONTACT NUMBER <span class="asterisk">*</span></label>
                                <input type="text" id="restaurant_contact_number" name="restaurant_contact_number" class="form-control" placeholder="CONTACT NUMBER">
                            </div>
                        </div>
                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="restaurant_location">RESTAURANT ADDRESS <span class="asterisk">*</span></label>
                                <input type="text" class="form-control" placeholder="RESTAURANT ADDRESS" id="restaurant_address" name="restaurant_address">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                        <label for="supplier_name">SUPPLIER <span class="asterisk">*</span></label>
                                        <input type="text" id="supplier_name1" name="supplier_name1" class="form-control" placeholder="Supplier Name" value="{{$supplier_name}}" readonly>
                                        <input type="hidden" id="supplier_name" name="supplier_name" value="{{$supplier_id}}">

                                      
                                    </div>

                        </div>
                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="restaurant_country">COUNTRY <span class="asterisk">*</span></label>
                                        <select id="restaurant_country" name="restaurant_country" class="form-control select2" style="width: 100%;" >
                                            <option selected="selected">SELECT COUNTRY</option>
                                        </select>
                                    </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group" id="acitvity_city_div" style="display:none">
                                        <label for="restaurant_city">CITY <span class="asterisk">*</span></label>
                                        <select id="restaurant_city" name="restaurant_city" class="form-control select2" style="width: 100%;">
                                            <option selected="selected">SELECT CITY</option>
                                        </select>
                            </div>

                        </div>

                    </div>
                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>RESTAURANT AVAILABILITY<span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="text" placeholder="FROM"
                                                    class="form-control pull-right datepicker" id="validity_operation_from" name="validity_operation_from" readonly="readonly">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <div class="input-group date">
                                                <input type="text" placeholder="TO"
                                                    class="form-control pull-right datepicker" id="validity_operation_to" name="validity_operation_to" readonly="readonly">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                  <label>RESTAURANT TIMINGS<span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6">
                                         <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker1" id="restaurant_time_from" name="restaurant_time_from">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker1" id="restaurant_time_to" name="restaurant_time_to">

                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                    </div>
                            </div>

                        </div>

                    </div>
                </div>
                   <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="no_of_tables">NO. OF TABLES <span class="asterisk">*</span></label>
                                        <select id="no_of_tables" name="no_of_tables" class="form-control select2" style="width: 100%;">
                                            <option value="0" hidden>Select No. of Tables</option>
                                            @for($i=1;$i<=100;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                        <label for="available_for_delivery_label">AVAILABLE FOR DELIVERY</label>
                                        <br>
                                        <input type="checkbox" name="restaurant_available_for_delivery" id="restaurant_available_for_delivery"  value="yes" class="checkbox-col-primary">
                                          <label for="restaurant_available_for_delivery">&nbsp;</label>
                            </div>

                        </div>
                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="row mb-10">


                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>IS ALL DAYS <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">

                                            <input type="radio" id="radio_10"
                                                class="with-gap radio-col-primary week_all_days"  name="is_all_days" value="Yes">
                                            <label for="radio_10">Yes </label>
                                            <input type="radio" id="radio_11"
                                                class="with-gap radio-col-primary week_all_days" name="is_all_days" value="No">
                                            <label for="radio_11">No</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>MONDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input  type="radio" id="radio_20"
                                                class="with-gap radio-col-primary weekdays_yes " name="week_monday" value="Yes">
                                            <label for="radio_20">Yes </label>
                                            <input type="radio" id="radio_21"
                                                class="with-gap radio-col-primary weekdays_no " name="week_monday" value="No">
                                            <label for="radio_21">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>TUESDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_30"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_tuesday" value="Yes">
                                            <label for="radio_30">Yes </label>
                                            <input type="radio" id="radio_31"
                                                class="with-gap radio-col-primary weekdays_no" name="week_tuesday" value="No">
                                            <label for="radio_31">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>WEDNESDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_40"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_wednesday" value="Yes">
                                            <label for="radio_40">Yes </label>
                                            <input type="radio" id="radio_41"
                                                class="with-gap radio-col-primary weekdays_no" name="week_wednesday" value="No">
                                            <label for="radio_41">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>THURSDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_50"
                                                class="with-gap radio-col-primary weekdays_yes"  name="week_thursday" value="Yes">
                                            <label for="radio_50">Yes </label>
                                            <input type="radio" id="radio_51"
                                                class="with-gap radio-col-primary weekdays_no"  name="week_thursday" value="No">
                                            <label for="radio_51">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>FRIDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_60"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_friday" value="Yes">
                                            <label for="radio_60">Yes </label>
                                            <input type="radio" id="radio_61"
                                                class="with-gap radio-col-primary weekdays_no" name="week_friday" value="No">
                                            <label for="radio_61">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>SATURDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_70"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_saturday" value="Yes">
                                            <label for="radio_70">Yes </label>
                                            <input type="radio" id="radio_71"
                                                class="with-gap radio-col-primary weekdays_no" name="week_saturday" value="No">
                                            <label for="radio_71">No</label>
                                        </div>
                                    </div>
                                <div class="row">
                                        <div class="col-md-6">
                                            <label>SUNDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">

                                            <input type="radio" id="radio_80"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_sunday" value="Yes">
                                            <label for="radio_80">Yes </label>
                                            <input type="radio" id="radio_81"
                                                class="with-gap radio-col-primary weekdays_no" name="week_sunday" value="No">
                                            <label for="radio_81">No</label>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>




                    </div>

                    <div class="row mb-10" style="display: none">

                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                        <label for="restaurant_currency">CURRENCY <span class="asterisk">*</span></label>
                                        <select class="form-control select2" style="width: 100%;" id="restaurant_currency" name="restaurant_currency">
                                             <option value="0" hidden>SELECT CURRENCY</option>
                                            @foreach($currency as $curr)
                                                <option value="{{$curr->code}}">{{$curr->code}} ({{$curr->name}})</option>
                                            @endforeach
                                        </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>

                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>BLACKOUT DAYS</label>
                                <div class="col-sm-12 col-md-12" style="padding:0">
                                    <button type="button" class="btn btn-rounded btn-primary mr-10"
                                        data-toggle="collapse" data-target="#demo2">Add
                                       Blackout Days</button>

                                    <div id="demo2" class="collapse">
                                        <div class="row mt-15 mb-10">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">

                                                    <div class="input-group date">
                                                        <input type="text" placeholder="BLACKOUT DATES" class="form-control pull-right datepicker" id="blackout_days" name="blackout_days">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->

                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <!-- <div class="col-sm-12 col-md-12">
                                            <img class="plus-icon" style="display: block;" src="{{ asset('assets/images/add_icon.png') }}">
                                        </div> -->
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12" style="display: none;">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> INCLUSIONS <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                             <textarea class="form-control" id="restaurant_inclusions" name="restaurant_inclusions"></textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> EXCLUSIONS <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <textarea class="form-control" id="restaurant_exclusions" name="restaurant_exclusions"></textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                         <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> RESTAURANT DESCRIPTION <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <textarea class="form-control" id="restaurant_description" name="restaurant_description"></textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                    </div>
                    <div class="row" style="display: none">
                        <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> CANCELLATION POLICY <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                           <textarea class="form-control" id="restaurant_cancellation" name="restaurant_cancellation"></textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> TERMS AND CONDITIONS <span class="asterisk">*</span> </h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <textarea class="form-control" id="restaurant_terms_conditions" name="restaurant_terms_conditions"></textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="img_group">
                            <label>RESTAURANT IMAGES</label>
                        <div class="input-group control-group increment" id="increment">
          <input type="file" name="upload_ativity_images[]" class="form-control upload_ativity_images">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_restaurant_image" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_ativity_images[]" class="form-control upload_ativity_images">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_restaurant_image" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>
                        <!-- ngRepeat: (itemindex,item) in temp_loop.enquiry_comment_attachment track by $index -->
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                    </div>
                     <div id="previewImg" class="row">
                        </div>
                </div>
                <div class="row mb-10">
                    <div class="col-md-12">
                        <div class="box-header with-border"
                            style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
                            <button type="button" id="save_restaurant" class="btn btn-rounded btn-primary mr-10">Save</button>
                            <button type="button" id="discard_restaurant" class="btn btn-rounded btn-primary">Discard</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>

</div>
</div>
</div>
</div>
   @include('supplier.includes.footer')

        @include('supplier.includes.bottom-footer')
        <div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
                <script type="text/javascript">
    $(document).ready(function() {
       $(document).on("click",".add_more_restaurant_image",function(){ 
        var id=$(this).parent().parent().attr("id");
        var actual_id=id.split("increment");

        if(actual_id[1]=="")
        {
             var html = $("#clone").html();
          $("#increment").after(html);
        }
        else
        {
              var html = $("#clone"+actual_id[1]).html();
          $("#"+id).after(html);

        }
         
      });
      $("body").on("click",".remove_more_restaurant_image",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
<script>

$(document).ready(function()
{
    // document.getElementById('upload_ativity_images').addEventListener('change', handleFileSelect, false);
    CKEDITOR.replace('restaurant_exclusions');
    CKEDITOR.replace('restaurant_inclusions');
     CKEDITOR.replace('restaurant_description');
    CKEDITOR.replace('restaurant_cancellation');
    CKEDITOR.replace('restaurant_terms_conditions');
    $('.select2').select2();
    var date = new Date();
    date.setDate(date.getDate());

    $('#validity_operation_from').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#validity_operation_from").datepicker("getDate");
        var date_to = $("#validity_operation_to").datepicker("getDate");

        if(!date_to)
        {
            $('#validity_operation_to').datepicker("setDate",date_from);
        }
        else if(date_to<date_from)
        {
            $('#validity_operation_to').datepicker("setDate",date_from);
        }
    });

    $('#validity_operation_to').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#validity_operation_from").datepicker("getDate");
        var date_to = $("#validity_operation_to").datepicker("getDate");

        if(!date_from)
        {
            $('#validity_operation_from').datepicker("setDate",date_to);
        }
        else if(date_to<date_from)
        {
            $('#validity_operation_from').datepicker("setDate",date_to);
        }
    });
    
    $('#blackout_days').datepicker({
     multidate: true,
     todayHighlight: true,
     format: 'yyyy-mm-dd',
     startDate:date
 });
    $('.timepicker1').timepicker({
         defaultTime: 'current',
        showInputs: false,
        minuteStep: 5,
        timeFormat: 'HH:mm:ss',
         template: 'dropdown'
    });

     $('.availability_dates').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    });


});
    
    $(document).on("change","input[name='is_all_days']",function()
    {
        if($(this).is(":checked"))
        {
            if($("input[name='is_all_days']:checked").val()=="Yes")
            {
               $(".weekdays_yes").prop("checked",true);
            }
            else
            {
                $(".weekdays_no").prop("checked",true);
            }
        }
    });
</script>
<script>
    $(document).ready(function(){
        var supplier_id=$('#supplier_name').val();
        if(supplier_id=="")
        {

        }
        else
        {
            $.ajax({
                url:"{{route('search-supplier-country')}}",
                type:"GET",
                data:{"supplier_id":supplier_id},
                success:function(response)
                {
                    $("#restaurant_country").html(response);
                    $('#restaurant_country').select2();
                    $("#restaurant_country").prop("disabled",false);

                     $("#restaurant_city").html("");

                }
            });
        }
    });
    $(document).on("change","#restaurant_country",function()
    {
         if($("#restaurant_country").val()!="0")
        {
            var country_id=$(this).val();
            $.ajax({
                url:"{{route('search-country-cities')}}",
                type:"GET",
                data:{"country_id":country_id},
                success:function(response)
                {
                    $("#restaurant_city").html(response);
                    $('#restaurant_city').select2();
                      $("#acitvity_city_div").show();
                }
            });
        }
    });
</script>
<script>
    $(document).on("click","#save_restaurant",function()
    {
        var restaurant_type=$("#restaurant_type").val();
        var restaurant_name=$("#restaurant_name").val();
        var restaurant_owner_name=$("#restaurant_owner_name").val();
        var supplier_name=$("#supplier_name").val();
        var restaurant_address=$("#restaurant_address").val();
        var restaurant_contact_number=$("#restaurant_contact_number").val();
        var restaurant_country=$("#restaurant_country").val();
        var restaurant_city=$("#restaurant_city").val();
        var validity_operation_from=$("#validity_operation_from").val();
        var validity_operation_to=$("#validity_operation_to").val();
        var restaurant_time_from=$("#restaurant_time_from").val();
        var restaurant_time_to=$("#restaurant_time_to").val();
        var is_all_days = $("input[name='is_all_days']:checked").val();
        var week_monday = $("input[name='week_monday']:checked").val();
        var week_tuesday = $("input[name='week_tuesday']:checked").val();
        var week_wednesday = $("input[name='week_wednesday']:checked").val();
        var week_thursday = $("input[name='week_thursday']:checked").val();
        var week_friday = $("input[name='week_friday']:checked").val();
        var week_saturday = $("input[name='week_saturday']:checked").val();
        var week_sunday = $("input[name='week_sunday']:checked").val();
        var restaurant_currency=$("#restaurant_currency").val();
        var restaurant_inclusions= CKEDITOR.instances.restaurant_inclusions.getData();
        var restaurant_exclusions=CKEDITOR.instances.restaurant_exclusions.getData();
        var restaurant_description=CKEDITOR.instances.restaurant_description.getData();
        var restaurant_cancellation=CKEDITOR.instances.restaurant_cancellation.getData();
        var restaurant_terms_conditions=CKEDITOR.instances.restaurant_terms_conditions.getData();


        if (restaurant_type.trim() == "0")
        {
            $("#restaurant_type").css("border", "1px solid #cf3c63");

        } else

        {
            $("#restaurant_type").css("border", "1px solid #9e9e9e");
        }

        if (restaurant_name.trim() == "")
        {
            $("#restaurant_name").css("border", "1px solid #cf3c63");

        } else

        {
            $("#restaurant_name").css("border", "1px solid #9e9e9e");
        }

        if (restaurant_owner_name.trim() == "")
        {
            $("#restaurant_owner_name").css("border", "1px solid #cf3c63");

        } else

        {
            $("#restaurant_owner_name").css("border", "1px solid #9e9e9e");
        }

        if (supplier_name == "0")
        {
            $("#supplier_name").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

        } else

        {
           $("#supplier_name").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
       }
       if (restaurant_address.trim() == "")
       {
         $("#restaurant_address").css("border", "1px solid #cf3c63");

     } else

     {
      $("#restaurant_address").css("border", "1px solid #9e9e9e");
  }
  if (restaurant_contact_number.trim() == "")
       {
         $("#restaurant_contact_number").css("border", "1px solid #cf3c63");

     } else

     {
      $("#restaurant_contact_number").css("border", "1px solid #9e9e9e");
  }
  if (restaurant_country == "0")
  {
    $("#restaurant_country").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

} else

{
   $("#restaurant_country").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
}
if (restaurant_city == "0")
{
    $("#restaurant_city").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

} else

{
   $("#restaurant_city").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
}
if (validity_operation_from.trim() == "")
{
    $("#validity_operation_from").css("border", "1px solid #cf3c63");

} else

{
    $("#validity_operation_from").css("border", "1px solid #9e9e9e");
}
if (validity_operation_to.trim() == "")
{
    $("#validity_operation_to").css("border", "1px solid #cf3c63");

} else

{
    $("#validity_operation_to").css("border", "1px solid #9e9e9e");
}
if (restaurant_time_from.trim() == "")
{
    $("#restaurant_time_from").css("border", "1px solid #cf3c63");

} else

{
    $("#restaurant_time_from").css("border", "1px solid #9e9e9e");
}
if (restaurant_time_to.trim() == "")
{
    $("#restaurant_time_to").css("border", "1px solid #cf3c63");

} else

{
    $("#restaurant_time_to").css("border", "1px solid #9e9e9e");
}
if (!is_all_days) {
    $("input[name='is_all_days']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='is_all_days']").parent().css("border", "1px solid white");
}
if (!week_monday) {
    $("input[name='week_monday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_monday']").parent().css("border", "1px solid white");
}
if (!week_tuesday) {
    $("input[name='week_tuesday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_tuesday']").parent().css("border", "1px solid white");
}
if (!week_wednesday) {
    $("input[name='week_wednesday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_wednesday']").parent().css("border", "1px solid white");
}
if (!week_thursday) {
    $("input[name='week_thursday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_thursday']").parent().css("border", "1px solid white");
}
if (!week_friday) {
    $("input[name='week_friday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_friday']").parent().css("border", "1px solid white");
}
if (!week_saturday) {
    $("input[name='week_saturday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_saturday']").parent().css("border", "1px solid white");
}
if (!week_sunday) {
    $("input[name='week_sunday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_sunday']").parent().css("border", "1px solid white");
}

if (restaurant_description.trim() == "")
{
    $("#cke_restaurant_description").css("border", "1px solid #cf3c63");

} else

{
    $("#cke_restaurant_description").css("border", "1px solid #9e9e9e");
}


if(restaurant_type.trim() == "0")
{
    $("#restaurant_type").focus();
}
else if(restaurant_name.trim() == "")
{
    $("#restaurant_name").focus();
}
else if(supplier_name=="0")
{
  $("#supplier_name").parent().find(".select2-selection").focus();  
} 
else if(restaurant_address.trim()=="")
{
  $("#restaurant_address").focus();  
}
else if(restaurant_country=="0")
{
  $("#restaurant_country").parent().find(".select2-selection").focus();  
} 
else if(restaurant_city=="0")
{
  $("#restaurant_city").parent().find(".select2-selection").focus();  
}

else if(validity_operation_from.trim()=="")
{
  $("#validity_operation_from").focus();  
}
else if(validity_operation_to.trim()=="")
{
  $("#validity_operation_to").focus();  
}
else if(restaurant_time_from.trim()=="")
{
  $("#restaurant_time_to").focus();  
}
else if(restaurant_time_to.trim()=="")
{
  $("#restaurant_time_to").focus();  
} 
else if (!is_all_days) {
    $("input[name='is_all_days']").focus();
} 
else if (!week_monday) {
    $("input[name='week_monday']").focus();
} 
else if (!week_tuesday) {
    $("input[name='week_tuesday']").focus();
} 
else if (!week_wednesday) {
    $("input[name='week_wednesday']").focus();
} 
else if (!week_thursday) {
    $("input[name='week_thursday']").focus();
}
else if (!week_friday) {
    $("input[name='week_friday']").focus();
} 
else if (!week_saturday) {
    $("input[name='week_saturday']").focus();
} 
else if (!week_sunday) {
    $("input[name='week_sunday']").focus();
}
else if(restaurant_description.trim()=="")
{
  $("#cke_restaurant_description").attr("tabindex","100").focus();  
}
else
{
    $("#save_restaurant").prop("disabled", true);
    $("#loaderModal").modal("show");
    var formdata=new FormData($("#restaurant_form")[0]);
     formdata.append("restaurant_description",restaurant_description);
    formdata.append("restaurant_inclusions",restaurant_inclusions);
    formdata.append("restaurant_exclusions",restaurant_exclusions);
    formdata.append("restaurant_cancellation",restaurant_cancellation);
    formdata.append("restaurant_terms_conditions",restaurant_terms_conditions);
    $.ajax({
        url:"{{route('insert-restaurant')}}",
        enctype:"multipart/form-data",
        type:"POST",
        data:formdata,
        contentType: false,
        processData: false,
        success:function(response)
        {
            if (response.indexOf("exist") != -1)
            {
                swal("Already Exist!",
                    "Restaurant already exists");

            } else if (response.indexOf("success") != -1)

            {
                swal({
                    title: "Success",
                    text: "Restaurant Created Successfully !",
                    type: "success"
                },

                function () {

                    location.reload();

                });
            } else if (response.indexOf("fail") != -1)
            {
                swal("ERROR", "Restaurant cannot be inserted right now! ");
            }
            $("#save_restaurant").prop("disabled", false);
             $("#loaderModal").modal("hide");
        }
    });
}
});
    $(document).on("click","#discard_restaurant",function()
    {
        window.history.back();

    });
</script>
<script>
    $(document).on("change",".weekdays_yes,.weekdays_no",function()
    {
        var count=0;
        $(".weekdays_yes").each(function()
        {
            if($(this).is(":checked"))
            {
               count++;
            }
        });

        if(count==7)
        {
            $("input[name=is_all_days][value='Yes']").prop("checked",true);
        }
        else
        {
             $("input[name=is_all_days][value='No']").prop("checked",true);
        }

    });

     $(document).on("change",".upload_ativity_images",function()
        {      
            var imageSize = this;
            var count=0;
            for (var i = 0; i < imageSize.files.length; i++) {
                var image_Size = imageSize.files[i].size;
                    if(image_Size > 250000)
                    {
                       alert("Try to upload files less than 250KB!"); 
                        $(this).val("");
                        break;
                       count++;
                   }
               }
      });
</script>
</body>


</html>
