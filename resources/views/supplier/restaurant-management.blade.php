<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('supplier.includes.top-header')
<style>
    div#cke_1_contents {

        height: 250px !important;

    }
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('supplier.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('supplier.includes.nav')

	<div class="content">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title">Restaurants</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item" aria-current="page">Dashboard</li>
							<li class="breadcrumb-item active" aria-current="page">Restaurants
							</li>
						</ol>
					</nav>
				</div>
			</div>

		</div>
	</div>


	<div class="row">

		<div class="col-12">
			<div class="box">

				<div class="box-body">
					<!-- Nav tabs -->
					<!-- <ul class="nav nav-pills mb-20">
						<li class=" nav-item"> <a href="#navpills-restaurant" id="navpills-restaurant-link" class="nav-link show active" data-toggle="tab"
								aria-expanded="false">Restaurants</a> </li>

					</ul> -->
					<!-- Tab panes -->
					<!-- <div class="tab-content">
						<div id="navpills-restaurant" class="tab-pane show active"> -->
							<div class="row">
									
								<div class="col-sm-6 col-md-3">
									<div class="form-group">

										<a href="{{route('supplier-create-restaurant')}}"><button type="button"
												class="btn btn-rounded btn-success">Create New Restaurant</button></a>
									</div>
								</div>
							

								
								<div class="col-12">
									<div class="box">

										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table  id="restaurant_table" class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>S. No.</th>
															<th style="display:none">Restaurant ID</th>
															<th>Restaurant Name</th>
															<th>Address</th>
															<th>Contact</th>
															<th>City</th>
															<th>Country</th>
															<th>Supplier Name</th>
															<th>Available for Delivery</th>
											
															<th>Status</th>
															<th>Approval</th>
															
															
															<th>Action</th>
															
														</tr>
													</thead>
													<tbody>
														@php
														$srno=1;
														@endphp
															@foreach($get_restaurants as $restaurants)

												<tr id="tr_{{$restaurants->restaurant_id}}">
													<td style="cursor: all-scroll;">{{$srno}}</td>
													<td style="display:none">{{$restaurants->restaurant_id}}</td>
													<td>{{$restaurants->restaurant_name}}</td>
													<td>{{$restaurants->restaurant_address}}</td>
													<td>{{$restaurants->restaurant_contact}}</td>
													<td>{{$restaurants->getCity->name}}</td>
													<td>{{$restaurants->getCountry->country_name}}</td>
													<td>{{$restaurants->getSupplier->supplier_name}}</td>
													<td>@if($restaurants->restaurant_available_for_delivery=="yes") <button type="button" class="btn btn-sm btn-primary">YES</button> @else NO @endif</td>
										
														<td>
															@if($restaurants->restaurant_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" >Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" >InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($restaurants->restaurant_approve_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary">Approved</button>
															@elseif($restaurants->restaurant_approve_status==0)
														<button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button>
															@elseif($restaurants->restaurant_approve_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger">Rejected</button>
															@endif


															</td>
													

														
											

													<td>
															<a href="{{route('supplier-restaurant-details',['restaurant_id'=>$restaurants->restaurant_id])}}" target="_blank"><i class="fa fa-eye"></i></a>

														<a href="{{route('supplier-edit-restaurant',['restaurant_id'=>$restaurants->restaurant_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>

														

													</td>

												</tr>
												@php
														$srno++;
														@endphp

												@endforeach

													</tbody>

												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
								

							</div>
						<!-- </div>
					</div> -->
				</div>


				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
	

		<!-- /.box -->

	</div>

</div>
</div>

</div>

</div>
@include('supplier.includes.footer')

@include('supplier.includes.bottom-footer')
<div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
<script>
	$(".datatable").DataTable();


    var table_restaurant = $('#restaurant_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[25, 50, 100,150,200,250,500,-1], [25, 50, 100,150,200,250,500,"All"]]
    });
 
    table_restaurant.on( 'row-reorder', function ( e, diff1, edit1 ) {
        var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
            var rowData = table_restaurant.row( diff1[i].node ).data();
 			new_data.push({restaurant_id:rowData[1],new_order:diff1[i].newData});
        }

  // console.log(new_data);
        if(new_data.length>0)
        {
          $.ajax({
        	url:"{{route('sort-restaurant')}}",
        	type:"POST",
        	data:{"_token":"{{csrf_token()}}",
        	"new_data":new_data},
        	success:function(response)
        	{
        		console.log(response);
        	}
        });	
        }
      
        
   
    });
	$(document).on("click",'.approve',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="restaurant")
		{

			swal({
				title: "Are you sure?",
				text: "You want to approve this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-restaurant-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" >Approved</button>');

								swal("Approved!", "Selected restaurant has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.reject',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="restaurant")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-restaurant-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" >Rejected</button>');

								swal("Rejected!", "Selected restaurant has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
	});

	$(document).on("click",'.active',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="restaurant")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-restaurant-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_restaurant_'+action_id+'">Active</button>');

								swal("Activated!", "Selected restaurant has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
	});
	$(document).on("click",'.inactive',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="restaurant")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-restaurant-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_restaurant_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected restaurant has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
	});



$(document).on("click",'.best_seller_yes',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="restaurant")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-restaurant-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_restaurant_'+action_id+'">Active</button>');

								swal("Activated!", "Selected restaurant's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.best_seller_no',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="restaurant")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-restaurant-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_restaurant_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected restaurant's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
	});






$(document).on("click",'.popular_yes',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="restaurant")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-restaurant-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_restaurant_'+action_id+'">Active</button>');

								swal("Activated!", "Selected restaurant's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.popular_no',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="restaurant")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this restaurant !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-restaurant-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_restaurant_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected restaurant's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
	});

</script>
</body>
</html>
