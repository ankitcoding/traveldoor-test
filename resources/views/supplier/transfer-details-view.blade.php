<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('supplier.includes.top-header')

<style>

	.iti-flag {

		width: 20px;

		height: 15px;

		box-shadow: 0px 0px 1px 0px #888;

		background-image: url("{{asset('assets/images/flags.png')}}") !important;

		background-repeat: no-repeat;

		background-color: #DBDBDB;

		background-position: 20px 0

	}



	div#cke_1_contents {

		height: 250px !important;

	}

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

	<div class="wrapper">

		@include('supplier.includes.top-nav')

		<div class="content-wrapper">

			<div class="container-full clearfix position-relative">	

				@include('supplier.includes.nav')

				<div class="content">

					<!-- Content Header (Page header) -->

					<div class="content-header">

						<div class="d-flex align-items-center">

							<div class="mr-auto">

								<h3 class="page-title">Transfer</h3>

								<div class="d-inline-block align-items-center">

									<nav>

										<ol class="breadcrumb">

											<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

											<li class="breadcrumb-item" aria-current="page">Dashboard</li>

											<li class="breadcrumb-item active" aria-current="page">View Transfer Details

											</li>

										</ol>

									</nav>

								</div>

							</div>

							</div>

						</div>

						<div class="row">
							<div class="col-12">

								<div class="box">

									<div class="box-body">
												<div class="row">

													<div class="col-md-3">

														<label for="transfer_name"><strong>TRANSFER NAME :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="transfer_name"> @if($get_transfer->transfer_name!="" && $get_transfer->transfer_name!="0" && $get_transfer->transfer_name!=null){{$get_transfer->transfer_name}} @endif 
														 </p>

													</div>

												</div>
												<div class="row">

													<div class="col-md-3">

														<label for="supplier_id"><strong>SUPPLIER NAME :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="supplier_id"> @if($get_transfer->supplier_id!="" && $get_transfer->supplier_id!=null)
															@foreach($suppliers as $supplier)
															@if($get_transfer->supplier_id==$supplier->supplier_id)
															{{$supplier->supplier_name}}
															@endif 
															@endforeach 
														@else No Data Available @endif </p>

													</div>

												</div>



												<div class="row">

													<div class="col-md-3">

														<label for="transfer_country"><strong>COUNTRY :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="transfer_country"> @if($get_transfer->transfer_country!="" && $get_transfer->transfer_country!=null)
															@foreach($countries as $country)
															@if(in_array($country->country_id,$countries_data))
															@if($country->country_id==$get_transfer->transfer_country)
															{{$country->country_name}}
															@endif
															@endif
															@endforeach
														@else No Data Available @endif </p>

													</div>

												</div>

												<!-- <div class="row">

													<div class="col-md-3">

														<label for="guide_city"><strong>CITY :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="guide_city"> @if($get_transfer->guide_city!="" && $get_transfer->guide_city!=null)
															<?php
															$fetch_city=ServiceManagement::searchCities($get_transfer->guide_city,$get_transfer->guide_country);

															echo $fetch_city['name'];
															?>
														@else No Data Available @endif </p>

													</div>

												</div> -->

												<div class="row">
													<div class="col-md-3">

														<label for="transfer_description"><strong>DESCRIPTION :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="transfer_description"> @if($get_transfer->transfer_description!="" && $get_transfer->transfer_description!=null)
															{{$get_transfer->transfer_description}}
														@else No Data Available @endif </p>

													</div>

												</div>
												<div class="row">

													<div class="col-md-3">

														<label for="transfer_type"><strong> TRANSFER TYPE:</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="transfer_type"> 
														@if($get_transfer->transfer_type!="" && $get_transfer->transfer_type!="0" && $get_transfer->transfer_type!=null)

															@if($get_transfer->transfer_type=="airport")
															Airport Transfer
															@else
															Normal Transfer
															@endif

														@endif </p>

													</div>

												</div>
													<div class="row mb-10">
													<div class="col-md-3">

														<label for="no_of_transfers"><strong>NO OF TRANSFERS :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="no_of_transfers"> @if($get_transfer->no_of_transfer_available!="" && $get_transfer->no_of_transfer_available!=null)
															{{$get_transfer->no_of_transfer_available}}
														@else No Data Available @endif </p>

													</div>

												</div>
												<div class="row mb-10">
													<div class="col-md-3">

														<label for="vehicle_type"><strong>VEHICLE TYPE:</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="vehicle_type"> @if($get_transfer->transfer_vehicle_type!="" && $get_transfer->transfer_vehicle_type!=null)
															 @foreach($fetch_vehicle_type as $vehicle_type)
		                                                        @if($vehicle_type->vehicle_type_id==$get_transfer->transfer_vehicle_type)
		                                                        {{$vehicle_type->vehicle_type_name}} 
		                                                        @endif
                                                            @endforeach
														@else No Data Available @endif </p>

													</div>

												</div>
												<div class="row mb-10">
													<div class="col-md-3">

														<label for="vehicle"><strong>VEHICLE:</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="vehicle"> @if($get_transfer->transfer_vehicle!="" && $get_transfer->transfer_vehicle!=null)
															 @foreach($fetch_vehicles as $vehicles)
		                                                        @if($vehicles->vehicle_type_id==$get_transfer->transfer_vehicle_type)
		                                                         @if($vehicles->vehicle_id==$get_transfer->transfer_vehicle)
		                                                        {{$vehicles->vehicle_name}}
		                                                        @endif
		                                                        @endif
                                                           @endforeach
														@else No Data Available @endif </p>

													</div>

												</div>
												<div class="row mb-10">
													<div class="col-md-3">

														<label for="vehicle"><strong>VEHICLE INFO :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="vehicle"> @if($get_transfer->transfer_vehicle_info!="" && $get_transfer->transfer_vehicle_info!=null)
															 {{$get_transfer->transfer_vehicle_info}}
														@else No Data Available @endif </p>

													</div>

												</div>
													<div class="row mb-10">
													<div class="col-md-3">

														<label for="vehicle"><strong>VEHICLE NOTE :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="vehicle"> @if($get_transfer->transfer_vehicle_note!="" && $get_transfer->transfer_vehicle_note!=null)
															 {{$get_transfer->transfer_vehicle_note}}
														@else No Data Available @endif </p>

													</div>

												</div>
												<div class="row mb-10">
													<div class="col-md-3">

														<label for="vehicle"><strong>VEHICLE IMAGES :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="vehicle"> @if($get_transfer->transfer_vehicle_images!="" && $get_transfer->transfer_vehicle_images!=null)
															@php
															$vehicle_images=unserialize($get_transfer->transfer_vehicle_images);
															 if(!empty($vehicle_images[0]))
															 {
															foreach($vehicle_images[0] as $images)
															{
																@endphp
																<img src="{{asset('assets/uploads/vehicle_images')}}/{{$images}}" alt="Vehicle Image" width=150  height=150>

																@php	
															}
														}
															@endphp
														@else No Data Available @endif </p>

													</div>

												</div>
											<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="transfer_cost"></i> TRANSFERS COST

												</h4>

											</div>

										</div>
										<div class="row" id="transfer_cost_details">
											<div class="col-md-12">
                  
                                                @if(count($get_transfer)<=0)
                                                <div class='row'><p class='text-center'><b>No Transfers Available</b></p></div>
                                                @else

                                                @if($get_transfer->transfer_type=="from-airport")

                                                <table class="table table-striped table-condensed">
                                                	<tr>
                                                		<th>From Airport</th>
                                                		<th>To City</th>
                                                		<th>Vehicle Cost</th>
                                                	</tr>
		         
		                                              @foreach($get_transfer_details as $transfer_details)
		                                                @php
		                                              $transfer_vehicle_tariff=unserialize($transfer_details->transfer_vehicle_tariff);
		                                              @endphp
		                                            	<tr>
		                                              @foreach($fetch_airports as $airports)
		                                              @if($airports->airport_master_id==$transfer_details->from_city_airport)
		                                               <td rowspan={{(count($transfer_vehicle_tariff[0])+1)}}>{{$airports->airport_master_name}}</td>
		                                                @endif
		                                              @endforeach

		                                               <td rowspan={{(count($transfer_vehicle_tariff[0])+1)}}>
		                                              	@php
		                                              	$fetch_city=ServiceManagement::searchCities($transfer_details->to_city_airport,$get_transfer->transfer_country);
														echo $fetch_city['name'];
														@endphp
														</td>
														<td>{{$transfer_details->transfer_vehicle_cost}}</td>
		                                          		</tr>
		                                            	@endforeach
                                                </table>


                                                @elseif($get_transfer->transfer_type=="to-airport")
                                                <table class="table table-striped table-condensed">
                                                	<tr>
                                                		<th>From City</th>
                                                		<th>To Airport</th>
                                                		<th>Vehicle Cost</th>
                                                	</tr>
		         
		                                              @foreach($get_transfer_details as $transfer_details)
		                                                @php
		                                              $transfer_vehicle_tariff=unserialize($transfer_details->transfer_vehicle_tariff);
		                                              @endphp
		                                            	<tr>
		                                             <td rowspan={{(count($transfer_vehicle_tariff[0])+1)}}>
		                                              	@php
		                                              	$fetch_city=ServiceManagement::searchCities($transfer_details->from_city_airport,$get_transfer->transfer_country);
														echo $fetch_city['name'];
														@endphp
														</td>

		                                               @foreach($fetch_airports as $airports)
		                                              @if($airports->airport_master_id==$transfer_details->to_city_airport)
		                                               <td rowspan={{(count($transfer_vehicle_tariff[0])+1)}}>{{$airports->airport_master_name}}</td>
		                                                @endif
		                                              @endforeach
													

		                                              <td>{{$transfer_details->transfer_vehicle_cost}}</td>
		                                          		</tr>
		                                            	@endforeach
                                                </table>
                                                @else

                                                <table class="table table-striped table-condensed">
                                                	<tr>
                                                		<th>From City</th>
                                                		<th>To City</th>
                                                		<th>Vehicle Cost</th>
                                                	</tr>
		         
		                                              @foreach($get_transfer_details as $transfer_details)
		                                                @php
		                                              $transfer_vehicle_tariff=unserialize($transfer_details->transfer_vehicle_tariff);
		                                              @endphp
		                                            	<tr>
		                                             <td rowspan={{(count($transfer_vehicle_tariff[0])+1)}}>
		                                              	@php
		                                              	$fetch_city=ServiceManagement::searchCities($transfer_details->from_city_airport,$get_transfer->transfer_country);
														echo $fetch_city['name'];
														@endphp
														</td>

		                                               <td rowspan={{(count($transfer_vehicle_tariff[0])+1)}}>
		                                              	@php
		                                              	$fetch_city=ServiceManagement::searchCities($transfer_details->to_city_airport,$get_transfer->transfer_country);
														echo $fetch_city['name'];
														@endphp
														</td>

		                                              <td>{{$transfer_details->transfer_vehicle_cost}}</td>
		                                          		</tr>
		                                            	@endforeach
                                                </table>
                                                @endif
                                                
                                                @endif

                                            </div>
										</div>

									
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="transfer_inclusions"></i> INCLUSIONS

												</h4>

											</div>



										</div>
										<div class="row" id="transfer_inclusions_details" >

											<div class="col-md-12">
												<textarea id="transfer_inclusions_data">
										 @if($get_transfer->transfer_inclusions!="" && $get_transfer->transfer_inclusions!=null) {{$get_transfer->transfer_inclusions}} @else No Data Available @endif
										 </textarea>

											</div>

										</div>

											
										<div class="row mb-10" >

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="transfer_exclusions"></i> EXCLUSIONS

												</h4>

											</div>



										</div>
										<div class="row" id="transfer_exclusions_details">

											
											<div class="col-md-12">
												<textarea id="transfer_exclusions_data">
										 @if($get_transfer->transfer_exclusions!="" && $get_transfer->transfer_exclusions!=null) {{$get_transfer->transfer_exclusions}} @else No Data Available @endif
										</textarea>

											</div>

										</div>
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="transfer_cancellation"></i> CANCELLATION POLICY

												</h4>

											</div>



										</div>
										<div class="row" id="transfer_cancellation_details">

											<div class="col-md-12">
												<textarea id="transfer_cancellation_data">
										 @if($get_transfer->transfer_cancellation!="" && $get_transfer->transfer_cancellation!=null) {{$get_transfer->transfer_cancellation}} @else No Data Available @endif
										</textarea>

											</div>

										</div>
										<div class="row mb-10">
											<div class="col-md-12">
												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >
													<i class="fa fa-minus-circle" id="transfer_terms_and_conditions"></i> TERMS AND CONDITIONS
												</h4>
											</div>
										</div>
										<div class="row" id="transfer_terms_and_conditions_details">
											<div class="col-md-12">
												<textarea id="transfer_terms_and_conditions_data">
										 @if($get_transfer->transfer_terms_and_conditions!="" && $get_transfer->transfer_terms_and_conditions!=null) {{$get_transfer->transfer_terms_and_conditions}} @else No Data Available @endif
										</textarea>
											</div>
										</div>
												<br>
												<div class="row mb-10">
													<div class="col-md-12">
														<div class="box-header with-border"
														style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
														<button type="button" id="discard_transfer" class="btn btn-rounded btn-primary">BACK</button>
														<a href="{{route('supplier-edit-transfer',['transfer'=>$get_transfer->transfer_id])}}" id="update_transfer" class="btn btn-rounded btn-primary mr-10">EDIT</a>
													</div>
												</div>
											</div>





											<!-- /.row -->


										<!-- /.box-body -->

									</div>


								</div>
								<!-- /.box -->
							</div>
						</div>
					
					</div>
			</div>
		</div>

		@include('supplier.includes.footer')

		@include('supplier.includes.bottom-footer')



		<script>
			$(document).on("click","#discard_transfer",function()
			{
				window.history.back();

			});
			$(document).ready(function()
			{
				CKEDITOR.replace('transfer_exclusions_data', {readOnly:true});
				CKEDITOR.replace('transfer_inclusions_data',{readOnly:true});
				CKEDITOR.replace('transfer_cancellation_data',{readOnly:true});
				CKEDITOR.replace('transfer_terms_and_conditions_data',{readOnly:true});

			});

			$(document).on("click","#transfer_cost",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#transfer_cost_details").toggle();



			});
			$(document).on("click","#transfer_inclusions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#transfer_inclusions_details").toggle();



			});
			$(document).on("click","#transfer_exclusions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#transfer_exclusions_details").toggle();



			});
			$(document).on("click","#transfer_cancellation",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#transfer_cancellation_details").toggle();



			});

	$(document).on("click","#transfer_terms_and_conditions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#transfer_terms_and_conditions_details").toggle();



			});




		</script>



	</body>





	</html>

