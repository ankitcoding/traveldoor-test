<?php
use App\Http\Controllers\ServiceManagement;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        * {
            font-family: 'ABeeZee', sans-serif;
        }
        .page-break {
            page-break-after: always;
        }
    </style>
    <style>
        * {

            font-size: 13px !important;
        }
        .title-name {
            text-transform: capitalize;
            font-size: 18px !important;
            color: #0d04b7;
            margin: 0;
            padding: 15px;
            border-top: 1px solid gainsboro;
            border-bottom: 1px solid gainsboro;
        }
        .description {
            color: #5c6975;
            font-size: 1rem;
            font-style: normal;
            font-weight: 400;
            font-family: "Nunito Sans", sans-serif;
            line-height: 1.5;
            text-align: justify !important;
        }
        .description p{
            text-align: justify !important;
        }
        p{
            text-align: justify !important;
        }
        .t-tab {
            padding: 10px;
            border-radius: 5px;
            margin-top: 20px;
            clear: both;
        }
        p.days {
            color: black;
            font-size: 18px;
            font-weight: 700;
        }
        p.title-2 {
            color: black;
            margin-bottom: 5px;
        }
        span.span-i {
            color: #673ab7;
            font-size: 14px !important;
        }
        .heading-div.c-h-div {
            display: flex;
            justify-content: space-between;
        }
        .t-div {
            display: inline-block;
            padding: 10px;
            width: auto;
            border-radius: 5px;
            width: 100%;
        }
        p.city {
            margin-bottom: 0;
            font-size: 19px;
            color: #e86e08;
            margin: 0;
        }
        .t-tab label {
            color: #ffffff;
            font-size: 20px;
            margin-bottom: 10px !important;
            display: block;
            background: #673AB7;
            padding: 10px;
            border-radius: 3px;
        }
        div#transfer_select__1 {
            width: 100%;
        }
        .hotel-div {
            display: block;
        }
        .hotel-img-div {
            float: left;
            width: 35%;
        }
        .d-label {
            background: #303046;
            color: white;
            display: block;
            padding: 10px 10px;
            margin-bottom: 15px;
            border-radius: 3px;
        }
        div.modal {
            z-index: 9999;
        }
        div#demo {
            margin-top: 25px;
            height: 300px;
        }
        img.cstm-hotel-image {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .day-count {
            background: white;
            margin-bottom: 20px;
            padding: 10px;
            border: 1px solid gainsboro;
            border-radius: 5px;
            clear: both;
        }
        .row {
            display: flex;
            justify-content: space-between;
        }
        .hotel-details {
            float: left;
            width: 50%;
        }
        .col-md-12 {
            width: 100%;
        }
        .hotel-list-div {
            width: 100%;
            clear: both;
            background: #e6d9ff;
        }
        p.info.cstm-hotel-address {
            margin: 0;
        }

        p.policy-heading {
            color: #5a33ae;
            font-size: 13px !important;
            font-family: "Nunito Sans", sans-serif;
            margin: 0;
        }
        p.policy-name {
            background: white;
            display: inline-block;
            padding: 8px 20px;
            color: #5a33ae;
            font-size: 16px !important;
            font-weight: 600;

            margin: 0;
            font-family: "Nunito Sans", sans-serif;
            margin-top: 10px;
        }
        p.policy-description {
            margin: 10px;
            font-family: "Nunito Sans", sans-serif;
        }
        .rating-r div {
            float: left;
            width: 50%;
        }
        .rating-r {
            width: 100%;
        }
        p.hotel-name.cstm-hotel-name,
        p.info.cstm-hotel-address {
            text-transform: capitalize;
        }
        .agent-data {
            float: right;
            background: #ffffff;
            padding: 15px;
            width: 30%;
            text-align: right;
            border-radius: 5px;
            margin-bottom: 20px;


        }
        p.trip-duration {
            color: #9C27B0;
            background: #fadcff;
            padding: 5px 15px;
            display: inline-block;
        }
        p.agent-name {
            color: #673ab7;
            font-size: 18px !IMPORTANT;
            margin: 0;
            text-align: right !important;
            white-space: pre-wrap;
        }
        p.agent-no {
            color: #000;
            font-weight: 700;
            text-align: right !important;
            white-space: pre-wrap;
        }
        p.agent-mail {
            color: #7f7f7f;
            text-align: right !important;
            white-space: pre-wrap;

        }
        .price-p {
            color: #5a33ae;
            font-size: 17px !important;
            text-align: right;
            float: right;
            background: #decffd;
            padding: 5px 20px;
            margin-bottom: 5px;
            margin-top: -5px;
        }
        p.day-info {
            font-size: 15px !important;
            color:white;
            font-weight: 400;
            border-bottom: 1px solid ;
            border-top:1px solid;
            padding-bottom: 5px;
            padding-top: 5px;
            padding-left:5px;
            margin-top: 5px;
            background-color:#2e6fd8;
            font-weight:600;
        }
        ul{
            padding:0 !important;
            list-style: none !important;
        }
        ul li{
            padding:5px 0 !important;
        }
        .overview .heading {
            text-transform: uppercase;
            text-align: center;
            color: #5a33ae;
            font-size: 18px !important;
            font-weight: 400;
            /*border-bottom: 1px solid;*/
            padding-bottom: 3px;
        }
        .heading {
            text-transform: uppercase;
            text-align: center;
            color: #5a33ae;
            font-size: 18px !important;
            font-weight: 400;
            /*border-bottom: 1px solid;*/
            padding-bottom: 3px;
        }
        .name{
            text-transform: capitalize;

            color: #5a33ae;
            font-size: 16px !important;
            font-weight: 400;

            margin-bottom: 5px;
            margin-top: 0;
        }
        ul.overview-list li {
            color: gray;
            list-style: none;
            padding:1px 0;
            font-weight: 100;
            clear: both;
        }
        *{
            box-sizing: border-box;
        }
        ul.overview-list li span,ul-icon {
            display: inline-block;
            background: #5a33ae;
            color: aliceblue;
            padding: 3px 3px 3px 3px;
            border: 1px solid transparent;
            border-radius: 20px 20px 20px 20px;
            margin-right: 10px;
            margin-top: 5px;
            width: 13px;
            height: 13px;
            font-size: 10px !important;
            text-align: center;
        }
        .ul-icon{
            display: inline-block;
            background: #5a33ae;
            color: white;
            padding: 0 3px 5px;
            border: 1px solid transparent;

            margin-right: 5px;
            margin-top: 4px;
            margin-bottom: 5px;

            height: 10px;
            text-align: center;
        }
        img.overview-img {
            width: 270px;
            height: 170px;
            float: left;
            margin-right: 20px;
            border-radius: 5px;
        }
        .i-deatail {
            float: right;
            width: 55%;
            text-align: left;
            margin-left: 10px;
        }
    
        ul.i-ul {
            list-style: none;
        }
        ul.i-ul li{
            color: gray;
            list-style: none;
            display: inline;
            padding:2px 0;
            padding-right: 20px;
            font-weight: 100;
        }
        .i-ul{
            margin: 0;
            padding: 0;
            margin-bottom:5px;
        }
        .description-i{
            color: gray;
            padding:1px 0;
            font-weight: 100;
            font-size: 12px !important;

        }
        .des-ul{
            padding: 0;
            margin-bottom: 50px;
            list-style: none;
        }
        .des-ul li{
            color: gray;
            padding:2px 0;
            font-weight: 100;
            font-size: 12px !important;
            background-color:black;

        }
        .overview{
            margin-bottom: 5px;
        }
        .more-img{
            margin-top: 3px;
        }
        .more-img img{
            width: 20%;
            height: 45px;
            margin-right: 10px;
            border-radius: 5px;
        }
        .hr{
            width:100%;
            background:gainsboro;
            height:1px;
            border:none;
            margin-top:45px;
            margin-bottom: 20px;
            clear: both;
        }
        .name-p{
            background: #5a33ae;
            color: white;
            padding: 5px 20px;
            border-radius: 5px;
            display: inline-block;
        }
        .policy-div {
            background: #decffd;
            padding: 10px;
            border-radius: 5px;
            margin-bottom: 20px;
        }
        p.policy-heading {
            color: #5a33ae;
            font-size: 15px !important;
            margin: 0;
        }
        p.policy-name {
            background: white;
            display: inline-block;
            padding: 5px 20px;
            color: #5a33ae;
            font-size: 13px !important;
            font-weight: 600;
            margin: 0;
            margin-top: 5px;
        }
        p.policy-description {
            margin: 10px ;
            font-size: 11px !important;
            font-weight: 100;
            color: #272727;
        }
        .city-info{
            color:gray;
        }
        h1.company-title {
            text-align: center;
            font-size: 24px !important;
            color: #0d04b7;
            font-weight: 400;
            font-family: fantasy !important;
            margin-bottom: 0 !important;
        }
        .banner-img{
            width: 100%;
            margin-bottom: 20px;
        }
        .li-img{
            display: inline-block;
            width: 55px;
            height: 45px;
            margin-left: 10px;
            border-radius: 3px;
            margin-top:1px;
            float: right;

            text-align: right;
        }
        .address{
            color:gray;
            text-align: center !important;
            margin-top: 10px !important;
        }
        .div-pdf{
            border: 1px solid gainsboro;
            padding: 10px;
        }
        .transfer-guide
        {
            width:100%;
            margin-top:20px;
            margin-bottom:15px;
            clear: both;
            float:left;
        }


        .new-ul
        {
            list-style-type:none;
        }
        .new-ul li span
        {
            background-color:#5a33ae;
            color:white;
        }
         /*.attaction   ul
         {
            width:50% !important;
         }*/
/*      .attaction   ul > li , .dropdown-menu li span ,.dropdown-menu li a {*/
/*    display:inline;*/
/*    display:inline-block;*/
/*  padding:2px;*/
/*  letter-spacing: 0px;*/
/*   word-break: break-all;*/
/*    word-wrap: break-word;*/
/*    white-space: normal;*/
/*font-size:12px;*/
/*}*/
       /* .attaction ul{
            width:100% !important;
            text-align:left;
        }
        .attaction ul li{
            margin-bottom:5px;
            display:block !important;
padding:7px 10px !important;
            background:#5a33ae !important;
        }

        .attaction ul li *{
            width:auto !important;

            color:white !important;
        }*/
        
        .attraction-item
        {
            color:white;
            background:#5a33ae !important;
            padding:10px;
            margin-right:6px;
        }

    </style>
</head>
<body>
@php
$inclusion_array=array();
@endphp

<img src="{{ asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}" width=90 height=90
     style="float:left;margin-left:5px;margin-top:5px">

<div style="float:left;width:65%;text-align:center">

    <h1 class="company-title">{{ strtoupper($get_agent_details->company_name) }}</h1>
    <p class="address">{{$get_agent_details->address}},<?php
        $fetch_city_name=ServiceManagement::searchCities($get_agent_details->agent_city,$get_agent_details->agent_country);
        echo $fetch_city_name['name'];
        ?>,
        @foreach($countries as $country)
        @if($country->country_id==$get_agent_details->agent_country)
        {{$country->country_name}}
        @endif
        @endforeach</p>

</div>



<div stytle="float:left">
    <div class="agent-data">
        <p class="agent-name">{{$get_agent_details->agent_name}}</p>
        <p class="agent-no">{{$get_agent_details->company_contact}}</p>
        <p class="agent-mail">{{$get_agent_details->company_email}}</p>
    </div>
</div>


<div class="div-pdf">



    <div class="basic-info" style="clear: both;margin-top:15px">
        @php
        $total_whole_itinerary_cost=$fetch_saved_itinerary->agent_itinerary_total_cost;
        $markup_cost=round(($total_whole_itinerary_cost*$markup)/100);
        //$total_cost=round(($total_whole_itinerary_cost+$markup_cost));
        $total_agent_cost=round(($total_whole_itinerary_cost+$markup_cost));

        $own_itinerary_markup_cost=round(($total_agent_cost*$own_markup)/100);
        $total_cost=round(($total_agent_cost+$own_itinerary_markup_cost));
        @endphp
        <p class="title-name">{{$get_itinerary->itinerary_tour_name}}<span  class="price-p">GEL {{$total_cost}}</span></p>
        <p class="trip-duration">Trip duration: {{$get_itinerary->itinerary_tour_days}} @if($get_itinerary->itinerary_tour_days>1) Nights   @else Night @endif {{($get_itinerary->itinerary_tour_days+1)}}
            @php $tour_days=$get_itinerary->itinerary_tour_days+1; @endphp @if($tour_days>1) Days @else Day @endif</p>
        <!-- <img src="http://web.traveldoor.ge/wp-content/uploads/2019/05/ryfjy8sOmAOYbor1SvUGMokaJ1hc5S-900x500.jpg" class="banner-img"> -->
        @php
        $banner_image=unserialize($get_itinerary->itinerary_image);
        @endphp
        @if(!empty($banner_image))
        <img src="{{asset('assets/uploads/itinerary_images')}}/{{$banner_image[0]}}" class="banner-img">
        @endif


        <p class="description"><?php echo $get_itinerary->itinerary_tour_description; ?></p>
    </div>
    @php
    $itinerary_package_countries=explode(",",$fetch_saved_itinerary->agent_itinerary_package_countries);
    $itinerary_package_cities=explode(",",$fetch_saved_itinerary->agent_itinerary_package_cities);
    $itinerary_package_title=unserialize($get_itinerary->itinerary_package_title);
    $itinerary_package_description=unserialize($get_itinerary->itinerary_package_description);
    $itinerary_package_services=unserialize($fetch_saved_itinerary->agent_itinerary_package_services);
    @endphp
    <div class="overview" style="clear:both">
        <h3 class="heading">Overview</h3>
        @php
        $start_day=$fetch_saved_itinerary->from_date;
        @endphp
        @for($days_count=0;$days_count<=$fetch_saved_itinerary->agent_itinerary_tour_days;$days_count++)
        <div class="overview-detail">
            <p class="day-info" style="margin-bottom:0 !important">{{date('D, M d,Y',strtotime("+$days_count days",strtotime($start_day)))}} (DAY {{($days_count+1)}})</p>
            <ul class="overview-list" style="width:100%">
                @php
                $listcount=1;
                @endphp
                @if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_id']))
                @php
                $fetch_transfer=ServiceManagement::searchTransfers($itinerary_package_services[$days_count]['transfer']['transfer_id']);
                $transfer_name="";
                if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="from-airport")
                {
                $fetch_from_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_from_airport']);
                $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);
                $transfer_name="From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];
                }
                else if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="to-airport")
                {
                $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);
                $fetch_to_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_to_airport']);
                $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];
                }
                else
                {
                $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);
                $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);
                $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];
                }
                $show_overview_transfer_image="";
                $transfer_images=unserialize($fetch_transfer['transfer_vehicle_images']);
                if(!empty($transfer_images[0]))
                {
                $transfer_img_count=0;
                foreach($transfer_images[0] as $images)
                {
                if($transfer_img_count<=3)
                {
                $show_overview_transfer_image.='<img src="'.asset('assets/uploads/vehicle_images').'/'.$images.'" class="li-img" />';
                }
                $transfer_img_count++;
                }

                }

                echo '<li style="width:100%"><span style="border-radius: 50px !important;display:none">'. $listcount.'</span> Transfer: '.$transfer_name.' '.$show_overview_transfer_image.'</li>';
                $inclusion_array[]="Transfer";
                $listcount++;
                @endphp
                @endif


                @if($days_count<$get_itinerary->itinerary_tour_days)
                @if(!empty($itinerary_package_services[$days_count]['hotel']['hotel_id']) && $itinerary_package_services[$days_count]['hotel']['hotel_no_of_days']!="0")
                @php
                $fetch_hotel=ServiceManagement::searchHotel($itinerary_package_services[$days_count]['hotel']['hotel_id']);

                $hotel_images=unserialize($fetch_hotel['hotel_images']);
                $show_overview_hotel_image="";
                if(!empty($hotel_images))
                {
                $hotel_img_count=0;
                foreach($hotel_images as $images)
                {
                if($hotel_img_count<3)
                {
                $show_overview_hotel_image.='<img src="'.asset('assets/uploads/hotel_images').'/'.$images.'" class="li-img" />';
                }

                $hotel_img_count++;
                }
                }

                echo '<li><span style="border-radius: 50px !important;display:none">'.$listcount.'</span> Accomodation: '. $fetch_hotel['hotel_name'].' '.$show_overview_hotel_image.'</li>';
                $inclusion_array[]="Hotel";
                $listcount++;
                @endphp
                @endif
                @endif


                @if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))
                @php
                $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);

                $sightseeing_images=unserialize($fetch_sightseeing['sightseeing_images']);

                $show_overview_sightseeing_image="";
                if(!empty($sightseeing_images))
                {
                $sightseeing_img_count=0;
                foreach($sightseeing_images as $images)
                {
                if($sightseeing_img_count<3)
                {
                $show_overview_sightseeing_image.='<img src="'.asset('assets/uploads/sightseeing_images').'/'.$images.'" class="li-img" />';
                }

                $sightseeing_img_count++;
                }
                }

                echo '<li><span style="border-radius: 50px !important;display:none">'.$listcount.'</span>  Sightseeing:'. $fetch_sightseeing['sightseeing_tour_name'].' '. $show_overview_sightseeing_image.'</li>';
                $inclusion_array[]="Sightseeing";
                $listcount++;
                @endphp
                @endif


                @if(!empty($itinerary_package_services[$days_count]['activity']['activity_id']))
                @php
                $activity_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);
                @endphp
                @for($activity_counter=0;$activity_counter < $activity_count;$activity_counter++)
                @if($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]=="")
                @continue
                @endif
                @php
                $fetch_activity=ServiceManagement::searchActivity($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]);

                $activity_images=unserialize($fetch_activity['activity_images']);

                $show_overview_activity_image="";
                if(!empty($activity_images))
                {
                $activity_img_count=0;
                foreach($activity_images as $images)
                {
                if($activity_img_count<3)
                {
                $show_overview_activity_image.='<img src="'.asset('assets/uploads/activities_images').'/'.$images.'" class="li-img" />';
                }

                $activity_img_count++;
                }
                }




                @endphp
                <li><span style="border-radius: 50px !important;display:none">{{$listcount}}</span> Activity: {{$fetch_activity['activity_name']}} @php echo $show_overview_activity_image @endphp</li>
                @php
                $inclusion_array[]="Activity";
                $listcount++;
                @endphp
                @endfor
                @endif

                @if($listcount==1)
                <li> Nothing Planned: Spend Your Day At Leisure.</li>
                @endif
            </ul>
        </div>
        @endfor
    </div>
    <div class="page-break"></div>
    <h3 class="heading">ITINERARY</h3>
    @php
    $show_guide_id="";
    $show_driver_id="";
    @endphp

    @for($days_count=0;$days_count<=$fetch_saved_itinerary->agent_itinerary_tour_days;$days_count++)
    <div class="overview" style="clear:both">

        <div class="overview-detail">
            @php
            $itinerary_date_from=date('Y-m-d',strtotime($start_day));
            $services_count=1;
            @endphp
            <p class="day-info">{{date('D, M d,Y',strtotime("+$days_count days",strtotime($start_day)))}} (DAY {{($days_count+1)}})</p>
            <p class="city-info">
                <?php
                $fetch_city_name=ServiceManagement::searchCities($itinerary_package_cities[$days_count],$itinerary_package_countries[$days_count]);
                echo $fetch_city_name['name'];
                ?>,
                @foreach($countries as $country)
                @if($country->country_id==$itinerary_package_countries[$days_count])
                {{$country->country_name}}
                @endif
                @endforeach</p>
            @if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_id']))
            @php
            $fetch_transfer=ServiceManagement::searchTransfers($itinerary_package_services[$days_count]['transfer']['transfer_id']);
            $transfer_images=unserialize($fetch_transfer['transfer_vehicle_images']);
             $services_count++;
            @endphp
            <p class="name-p">Transfer</p>
            <div class="over-parent">
                @if(!empty($transfer_images[0][0]))
                <img src="{{ asset('assets/uploads/vehicle_images')}}/{{$transfer_images[0][0]}}" class="overview-img" />
                @else
                <img  class="overview-img" src="{{ asset('assets/images/no-photo.png')}}"
                      style="width:100%">
                @endif

                <div class="i-deatail">
                    <p class="name">@php
                        $transfer_name="";
                        if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="from-airport")
                        {
                        $fetch_from_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_from_airport']);
                        $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);
                        $transfer_name="From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];
                        echo "From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];
                        }
                        else if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="to-airport")
                        {
                        $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);
                        $fetch_to_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_to_airport']);
                        $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];
                        echo "From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];
                        }
                        else
                        {
                        $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);
                        $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);
                        $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];
                        echo "From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];
                        }
                        @endphp</p>
                    <ul class="i-ul" style="margin-bottom:5px;">
                        <!--  <li><span class="ul-icon">1</span>1 hour</li>
                            <li><span class="ul-icon">2</span>2 adults</li> -->
                    </ul>
                    <div class="more-img">
                        @if(!empty($transfer_images[0]))
                        @foreach($transfer_images[0] as $images)
                        @if($loop->iteration>1)
                        <img src="{{ asset('assets/uploads/vehicle_images')}}/{{$images}}" style="margin-top:10px">
                        @endif
                        @endforeach
                        @else
                        <img src="{{ asset('assets/images/no-photo.png')}}" style="margin-top:100%">
                        @endif
                    </div>
                    <p class="description-i">@php  echo $fetch_transfer->transfer_vehicle_note @endphp</p>
                    @if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_guide_id']) && $itinerary_package_services[$days_count]['transfer']['transfer_guide_id']!="")
            <div class="transfer-guide">
                <b> Guide :</b>
                @php

                $get_guide=ServiceManagement::searchGuide($itinerary_package_services[$days_count]['transfer']['transfer_guide_id']);
                if(!empty($get_guide))
                {
                $guide_image=$get_guide->guide_image;
                $guide_languages=$get_guide->guide_language;

                $language_spoken=explode(",",$guide_languages);
                }
                else
                {
                $guide_image="";
                $guide_languages="";
                $language_spoken=array();
                }
                @endphp
                <p>
                @if($guide_image!="")
                <img src="{{ asset('assets/uploads/guide_images')}}/{{$guide_image}}" alt="Guide" width=70 height=70>
                @else
                <img src="{{ asset('assets/images/no-photo.png')}}" alt="" width=50 height=50>
                @endif
                {{strstr($itinerary_package_services[$days_count]['transfer']['transfer_guide_name']," ",true)}}
                    &nbsp; <span><b>Languages:</b>
                            @foreach($languages as $language)
                            @if(in_array($language->language_id,$language_spoken))
                            {{$language->language_name}},
                            @endif
                            @endforeach</span></p>
                @php
                $inclusion_array[]="Guide";
                @endphp
            </div>
            @endif
             
                </div>
               
               </div>
            <hr class="hr">
            @endif
            @if($days_count<$get_itinerary->itinerary_tour_days)
            @if(!empty($itinerary_package_services[$days_count]['hotel']['hotel_id']) && $itinerary_package_services[$days_count]['hotel']['hotel_no_of_days']!="0")

            @php
            $fetch_hotel=ServiceManagement::searchHotel($itinerary_package_services[$days_count]['hotel']['hotel_id']);
            $hotel_images=unserialize($fetch_hotel['hotel_images']);
              $services_count++;
            @endphp
            <p class="name-p">{{$fetch_hotel->getHotelType['hotel_type_name']}}</p>
            <div class="over-parent">
                @if(!empty($hotel_images[0]))
                <img src="{{ asset('assets/uploads/hotel_images')}}/{{$hotel_images[0]}}" class="overview-img" />
                @else
                <img class="cstm-hotel-image" src="{{ asset('assets/images/no-photo.png')}}"
                     style="width:100%">
                @endif

                <div class="i-deatail">
                    <p class="name">{{$fetch_hotel->hotel_name}}</p>
                    <ul class="i-ul" style="margin-bottom:5px;">
                        <li><span class="ul-icon">@php  echo $fetch_hotel->hotel_rating @endphp-Star</span></li>
                    </ul>
                    @php
                    $hotel_room_details=$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'];
                    @endphp
                    <p><b>Room Quantity</b> :
                        @foreach($hotel_room_details as $room_detail)
                        {{$room_detail['room_qty']}} {{$room_detail['room_name']}} <br>
                        @endforeach
                    </p>
                    <p><b>Checkin Date </b>: <?php echo date('d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from)));
                        $checkin_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from)));
                        ?></p>
                    <p><b>Checkout Date </b>: <?php
                        echo date('d M Y',strtotime("+".($itinerary_package_services[$days_count]['hotel']['hotel_no_of_days'])." days",strtotime($checkin_date)));
                        $checkout_date=date('Y-m-d',strtotime("+".($itinerary_package_services[$days_count]['hotel']['hotel_no_of_days'])." days",strtotime($checkin_date)));
                        ?></p>
                    <?php
                    // <div class="more-img">
                    //     @foreach($hotel_images as $images)
                    //     @if($loop->iteration>1)
                    //     <img src="{{ asset('assets/uploads/hotel_images')}}/{{$images}}" style="margin-top:10px">
                    //     @endif
                    //     @endforeach
                    // </div>
                    ?>
                </div>
                <!--  <p class="description-i"> -->
                @php //echo $fetch_hotel->hotel_description;
                @endphp
                <!-- </p> -->
            </div>
            <hr class="hr">
            @endif
            @endif
            @if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))

            <p class="name-p">Sightseeing</p>
            @php
            $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);
            $sightseeing_images=unserialize($fetch_sightseeing['sightseeing_images']);
              $services_count++;
            @endphp
            <div class="over-parent">
                @if(!empty($sightseeing_images[0]))
                <img class="overview-img" src="{{ asset('assets/uploads/sightseeing_images')}}/{{$sightseeing_images[0]}}">
                @else
                <img  class="overview-img" src="{{ asset('assets/images/no-photo.png')}}">
                @endif
                <div class="i-deatail">
                    <p class="name">@php echo $fetch_sightseeing['sightseeing_tour_name'] @endphp</p>
                    <ul class="i-ul" style="margin-bottom:5px;">
                        <li><span class="ul-icon">{{$fetch_sightseeing->sightseeing_distance_covered}} KMS</span></li>
                        <li><span class="ul-icon">{{$fetch_sightseeing->sightseeing_duration}} HOURS</span></li>
                    </ul>
                    <div class="more-img">
                        @foreach($sightseeing_images as $images)
                        @if($loop->iteration>1)
                        <img src="{{ asset('assets/uploads/sightseeing_images')}}/{{$images}}" style="margin-top:10px">
                        @endif
                        @endforeach
                    </div>
                </div>
                <p class="description-i"><b>Description :</b>@php echo trim($fetch_sightseeing->sightseeing_tour_desc); @endphp </p>
                <div class="attaction"><p class="description-i"><b>Attractions :</b>
                @php 
               if(substr_count($fetch_sightseeing->sightseeing_attractions,"</li>")>=2)
                {
                    $attractions_array=explode("</li>", trim($fetch_sightseeing->sightseeing_attractions));
                foreach($attractions_array as $attraction_item)
                {
                    if(trim(strip_tags($attraction_item))!="")
                    {
                     echo " <span class='attraction-item'>".trim(strip_tags($attraction_item))."</span> ";
                    }
               
                }
                }
                else
                {
                 $attractions=strip_tags($fetch_sightseeing->sightseeing_attractions);
                $attractions_array=explode(",", trim($attractions));
                foreach($attractions_array as $attraction_item)
                {
                    if(trim($attraction_item)!="")
                    {
                echo " <span class='attraction-item'>".trim($attraction_item)."</span> ";
            }
                }
                }
                @endphp
                </p></div>

                <!--      <ul class="new-ul">-->

                <!--<li><span class="new-ul-span">@php echo trim($fetch_sightseeing->sightseeing_attractions); @endphp</span></li>-->

                <!--      </ul>-->

                <!--      </p>-->
                <div>
                    <div style="float:left;width:50%">
                        @if($itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_id']!=$show_guide_id)
                        <b> Guide :</b>
                        @php

                        $get_guide=ServiceManagement::searchGuide($itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_id']);
                        if(!empty($get_guide))
                        {
                        $guide_image=$get_guide->guide_image;
                        $guide_languages=$get_guide->guide_language;

                        $language_spoken=explode(",",$guide_languages);
                        }
                        else
                        {
                        $guide_image="";
                        $guide_languages="";
                        $language_spoken=array();

                        }

                        $show_guide_id=$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_id'];
                        @endphp

                        <p>
                            @if($guide_image!="")
                            <img src="{{ asset('assets/uploads/guide_images')}}/{{$guide_image}}" alt="Guide" width=50 height=50>
                            @else
                            <img src="{{ asset('assets/images/no-photo.png')}}" alt="" width=50 height=50>
                            @endif
                            <br>
                            {{strstr($itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_name']," ",true)}}
                            &nbsp;
                            <span><b>Languages:</b>
                            @foreach($languages as $language)
                            @if(in_array($language->language_id,$language_spoken))
                            {{$language->language_name}},
                            @endif
                            @endforeach</span></p>
                        @php
                        $inclusion_array[]="Guide";
                        @endphp
                        @endif
                    </div>
                    <div style="float:left;width:50%">
                        @if($itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_id']!=$show_driver_id)
                        <b> Driver :</b>
                        @php
                        $get_driver=ServiceManagement::searchDriver($itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_id']);
                        if(!empty($get_driver))
                        {
                        $driver_image=$get_driver->driver_image;
                        $driver_languages=$get_driver->driver_language;

                        $driver_language_spoken=explode(",",$driver_languages);
                        }
                        else
                        {
                        $driver_image="";
                        $driver_languages="";
                        $driver_language_spoken=array();

                        }

                        $show_driver_id=$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_id'];

                        @endphp

                        <p>
                            @if($driver_image!="")
                            <img src="{{ asset('assets/uploads/driver_images')}}/{{$driver_image}}" alt="Driver" width=50 height=50>
                            @else
                            <img src="{{ asset('assets/images/no-photo.png')}}" alt="" width=50 height=50>
                            @endif
                            <br>
                            {{strstr($itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_name']," ",true)}}
                            &nbsp; 
                            <span><b>Languages:</b>
                            @foreach($languages as $language)
                            @if(in_array($language->language_id,$driver_language_spoken))
                            {{$language->language_name}},
                            @endif
                            @endforeach </span> </p>
                            @php
                            $inclusion_array[]="Driver";
                            @endphp
                            @endif
                    </div>
                  </div>
                </div>
                  <hr class="hr">
                    @endif         
            @php
            $activity_id_status=0;
             if(isset($itinerary_package_services[$days_count]['activity']['activity_id']))
                                                {
            $check_activity_id_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);

            if($check_activity_id_count==1)
            {
            if($itinerary_package_services[$days_count]['activity']['activity_id'][0]!="")
            {
            $activity_id_status++;
            }

            }
            else
            {
            for($check=0;$check< $check_activity_id_count; $check++)
            {
            if($itinerary_package_services[$days_count]['activity']['activity_id'][$check]!="")
            {
            $activity_id_status++;
            }
            }
            }
        }
            @endphp
            @if($activity_id_status>0)
            <p class="name-p">Activity</p>
            @php
            $activity_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);
            @endphp
            @for($activity_counter=0;$activity_counter < $activity_count;$activity_counter++)
            @if($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]=="")
            @continue
            @endif
            <div class="over-parent">
                @php
                  $services_count++;
                $fetch_activity=ServiceManagement::searchActivity($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]);
                $activity_images=unserialize($fetch_activity['activity_images']);
                @endphp
                @if(!empty($activity_images[0]))
                <img class="overview-img" src="{{ asset('assets/uploads/activities_images')}}/{{$activity_images[0]}}">
                @else
                <img class="overview-img" src="{{ asset('assets/images/no-photo.png')}}">
                @endif
                <div class="i-deatail">
                    <p class="name">{{$fetch_activity['activity_name']}}</p>
                    <ul class="i-ul" style="margin-bottom:4px;">
                        <li><span class="ul-icon">{{$fetch_activity['activity_location']}}</span></li>
                        @if($fetch_activity->getActivityType['activity_type_name']!="" && $fetch_activity->getActivityType['activity_type_name']!=null)
                        <li><span class="ul-icon">{{$fetch_activity->getActivityType['activity_type_name']}}</span></li>
                        @endif
                    </ul>
                    <div class="more-img">
                        @foreach($activity_images as $images)
                        @if($loop->iteration>1)
                        <img src="{{ asset('assets/uploads/activities_images')}}/{{$images}}" style="margin-top:10px">
                        @endif
                        @endforeach
                    </div>
                </div>
                <p class="description-i">@php echo $fetch_activity->activity_description; @endphp</p>
            </div>
            @endfor
            @endif
              @if($services_count==1)
             <p class="name-p">Spend Your Day At Leisure.</p>
            @endif
        </div>
    </div>
    @endfor
    <!-- <div class="overview" style="clear:both">
        <div class="overview-detail">
            <p class="day-info">Fri, Sep 06, 2019 (DAY 1)</p>
            <div class="over-parent">
                <img src="{{asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}" class="overview-img" />
                <div class="i-deatail">
                    <p class="name">The Inn At The Roman Forum-Small Luxury Hotels</p>
                    <ul class="i-ul">
                        <li><span class="ul-icon">1</span>Via degli Ibernesi</li>
                        <li><span class="ul-icon">2</span>2 nights</li>
                        <li><span class="ul-icon">3</span>2 adults</li>
                    </ul>
                    <div class="more-img">
                        <img src="{{asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}">
                        <img src="{{asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}">
                        <img src="{{asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}">
                        <img src="{{asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}">
                    </div>
                </div>
                <ul class="des-ul">
                    <li>Double Superior</li>
                    <li>Breakfast Included</li>
                    <li>Description</li>
                    <li>This 4 star hotel is located in the city centre of Rome.</li>
                </ul>
            </div>
        </div>
    </div>
    <hr class="hr"> -->
    <!--     <div class="overview" style="clear:both">
        <div class="overview-detail">
            <p class="day-info">Fri, Sep 06, 2019 (DAY 1)</p>
            <div class="over-parent">
                <div class="i-deatail-2">
                    <p class="name">The Inn At The Roman Forum-Small Luxury Hotels</p>
                    <ul class="i-ul">
                        <li><span class="ul-icon">1</span>Via degli Ibernesi</li>
                        <li><span class="ul-icon">2</span>2 nights</li>
                        <li><span class="ul-icon">3</span>2 adults</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <hr class="hr"> -->
    <div class="policies">
        <p class="policy-heading">Our Policies</p>
        <div class="policy-div">
            <p class="policy-name">Inclusion</p>
            <p class="policy-description">{{ implode(" ,",array_unique($inclusion_array,SORT_STRING))}}</p>
        </div>
        <div class="policy-div">
            <p class="policy-name">Exclusions</p>
            <p class="policy-description">@php echo $get_itinerary->itinerary_exclusions; @endphp</p>
        </div>
        <div class="policy-div">
            <p class="policy-name">Terms And Conditions</p>
            <p class="policy-description">@php echo $get_itinerary->itinerary_terms_and_conditions; @endphp</p>
        </div>
        <div class="policy-div">
            <p class="policy-name">Cancellation Policy</p>
            <p class="policy-description">@php echo $get_itinerary->itinerary_cancellation; @endphp</p>
        </div>
    </div>
</div>
</body>
</html>