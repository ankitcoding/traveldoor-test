<?php
use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\LoginController;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
    content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <style>
        *{
            font-family: Segoe UI;
        }
        body {
            font-family: 'Roboto', sans-serif;
        }
        .container {
            width: 100%;
        }
        .row {
            margin-right: -15px;
            margin-left: -15px;
        }
        .row:after,
        .row:before {
            display: table;
            content: " ";
            clear: both;
        }
        .col-md-6 {
            width: 46%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .i-heading {
            font-size: 40px;
            font-weight: bolder;
            color: #598fe7;
        }
        .i-span {
            font-size: 38px;
            color: #340f82;
            font-weight: bolder;
        }
        .h-confirm {
            font-size: 25px;
        }
        .col-md-7 {
            width: 58.33333333%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .col-md-8 {
            width: 60.66666667%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .col-md-9 {
            width: 75%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .col-md-10 {
            width: 83.33333333%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .col-md-11 {
            width: 91.66666667%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .col-md-12 {
            width: 100%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .col-md-1 {
            width: 8.33333333%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .col-md-4 {
            width: 29%;
            float: left;
            padding-left: 15px;
            padding-right: 15px;
        }
        .v-icon {
            width: 30px;
            margin-top: 5px;
            text-align: right;
            margin-right: 0;
        }
        .pdf-footer {
            margin-top: 30px;
        }
        .pdf-footer .earphone img {
            width: 30px;
        }
        .pdf-footer .earphone {
            float: left;
            width: 40px;
        }
        .pdf-footer .support {
            margin-left: 50px;
        }
        .pdf-footer .support .small-text {
            font-size: 12px;
            color: #666;
        }
        .pdf-footer .support .text-bold {
            font-size: 14px;
            color: #000;
            white-space: nowrap;
        }
        .c-icon {
            width: 25px;
            margin-top: 7px;
        }
        .hr-style {
            border: none;
            border-top: 1px solid rgb(220, 220, 220);
            width: 100%
        }
        .alert1 {
            color: #721c24;
            background-color: #f8d7da;
            border-color: #f5c6cb;
            padding: 0.75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid #f5c6cb;
            border-radius: 0.25rem;
            font-size: 14px;
        }
        .alert2 {
            color: #155724;
            background-color: #d4edda;
            border-color: #c3e6cb;
            padding: 0.75rem 1.25rem;
            margin-bottom: 1rem;
            border: 1px solid #c3e6cb;
            border-radius: 0.25rem;
            font-size: 14px;
        }
        .caret-img {
            width: 9px;
            margin-right: 3px;
        }
        li.t-policy {
            padding: 10px;
            color: #4e4e4e;
            font-weight: normal;
            font-size: 13px;
        }
        .table th,
        .table td {
            padding: 0.3rem 0.50rem;
            vertical-align: top;
            border: 1px solid #0A51A1;
            text-transform:capitalize;
            text-align:left;
        }
        th {
            text-align: left;
        }
        td {
            text-align: right;
        }
        th {
            display: table-cell;
            vertical-align: inherit;
            text-align: left;
        }
        .table {
            width: 100%;
            margin-bottom: 1rem;
            font-size: 13px;
            color: #212529;
        }
        table {
            border-collapse: collapse;
        }
        ul {
            list-style: none;
        }
        ul li::before {
            content: "\2022";
            color: #340f82;
            font-weight: bolder;
            display: inline-block;
            font-size: 17px;
            width: 1em;
            margin-left: -1em;
            margin-top: 3px;
        }
        p.t-info {
            font-size: 15px;
        }
        .l-icon {
            width: 150px;
            padding: 20px 0;
        }
        .table th {
            color: #FFF;
            background: #0A51A1;
            text-transform:uppercase;
        }
        .bottom-footer
        {
            background:#0A51A1;
        }
        .pdf-footer
        {
            position:absolute;
            bottom:0;
        }
    </style>
    <div class="container" style="margin-top: -10px;">
        <div class="row" style="margin-top: -30px;">
                <!-- <div class="col-md-6"><img src="https://www.traveldoor.ge/images/traveldoor_logo.png" class="l-icon"></div>
                <div class="col-md-6">
                    <p class="h-confirm" style="text-align:right;"> &nbsp;</p>
                </div> -->
            </div>
            <div class="container" style="font-size: 15px">
                <div class="row">
                    <div class="col-md-6" style="width:30%;"><div style="margin-top:20px;">
                        @if($customer_type=="agent")
                        <img src="https://www.traveldoor.ge/images/traveldoor_logo.png" class="l-icon">
                        @else
                        <img src="{{ asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}" height=90
                        class="l-icon">
                        @endif
                    </div></div>
                    <div class="col-md-6" style="width:62%;">
                        <div style="background: #0A51A1; text-align:right;color:#FFF; padding: 10px;">
                            @if($customer_type=="agent")
                            <h4 style="font-size:30px; margin-top:0px; margin-bottom:0px;"> Traveldoor </h4>
                            <p><b>Address:</b> {{ ucwords(strtolower('7 TSOTNE DADIANI STREET, "KARVASLA" BUSINESS CENTER, 3RD FLOOR, OFFICE NO. B308/1, 0101 TBILISI, GEORGIA'))}}</p>
                            @else
                            <h4 style="font-size:30px; margin-top:0px; margin-bottom:0px;"> {{$agent_info['company_name']}} </h4>
                            <p><b>Address:</b> {{ ucwords(strtolower($agent_info['address']))}}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" style="font-size: 14px">
                <div class="row" style="border: 2px solid #0A51A1;margin:10px 0 !important;border-radius:5px;">
                    <div class="col-md-6">
                        <p><b>Invoice To:</b></p>
                        @if($customer_type=="agent")
                        <p> {{$agent_info['agent_name']}} <br>{{$agent_info['company_name']}}<br> {{$agent_info['address']}} <br>
                            @foreach($countries as $country)
                            @if($country->country_id==$agent_info['agent_country'])
                            {{$country->country_name}}
                            @endif
                        @endforeach , {{$agent_info['company_contact']}}</p>
                        @else
                        <p> {{$data[0]['customer_name']}} <br>{{$data[0]['customer_address']}} <br>
                            @foreach($countries as $country)
                            @if($country->country_id==$data[0]['customer_country'])
                            {{$country->country_name}}
                            @endif
                        @endforeach , {{$data[0]['customer_contact']}}</p>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div style="text-align:right">
                            <p><b>Invoice no:</b> {{$data[0]['booking_sep_id']}}</p>
                            <p><b>Booking ID:</b> {{$data[0]['booking_sep_id']}}</p>
                            <p style="color: #333"><b>Date & Time:</b> {{ date("d/m/Y",strtotime($data[0]['booking_date']))}}  @php echo date("h:i a",strtotime(date('Y-m-d')."".$data[0]['booking_time'])); @endphp</p>                            
                        </div>
                        
                    </div>
                </div>
            </div>
            @php
            $booking_adult_count= $data[0]['booking_adult_count'] ? $data[0]['booking_adult_count'] : 0;
            $booking_child_count= $data[0]['booking_child_count'] ? $data[0]['booking_child_count'] : 0;
            $booking_infant_count= $data[0]['booking_infant_count'] ? $data[0]['booking_infant_count'] : 0;
            $total_pax=($booking_adult_count+ $booking_child_count+$booking_infant_count);
            
            $total_amount=0;
            if($customer_type=="agent")
            {
                $total_amount=$data[0]['booking_agent_amount'];
            }
            else
            {
                $total_amount=$data[0]['booking_amount'];
            }
            
            @endphp
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table">
                            <thead>
                                <tr>
                                    
                                    <th>Service </th>
                                    <th>Description</th>
                                    <th>No of Pax (Adults,Child)</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($data[0]['booking_type']=="activity")
                                @php
                                $fetch_activity=ServiceManagement::searchActivity($data[0]['booking_type_id']);
                                $activity_name=$fetch_activity['activity_name'];
                              //$activity_type=$fetch_activity['getActivityType']['activity_type_name'];
                                @endphp
                                <tr>
                                    <td>ACTIVITY</td>
                                    <td>{{$activity_name}}</td>
                                    <td>{{$total_pax}} ({{$booking_adult_count}},{{$booking_child_count}})</td>
                                    <td>{{$total_amount}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                                @elseif($data[0]['booking_type']=="hotel")
                                <?php
                                $fetch_hotel=ServiceManagement::searchHotel($data[0]['booking_type_id']);
                                $hotel_name=$fetch_hotel['hotel_name'];
                                $hotel_type=$fetch_hotel['getHotelType']['hotel_type_name'];
                                $booking_other_info=unserialize($data[0]['booking_other_info']);
                                $room_info_array=array();
                                foreach($booking_other_info as $room_info)
                                {
                                    $room_info_array[]=$room_info['room_qty']." ".$room_info['room_name'];
                                }
                                $room_info=implode(" , ",$room_info_array);
                                ?>
                                <tr>
                                    <td>HOTEL ({{$hotel_type}})</td>
                                    <td>{{$hotel_name}} <br> <b>Rooms</b> : {{$room_info}} <br> <b>No. of Nights</b> : {{$data[0]['booking_subject_days']}}</td>
                                      <td>{{$total_pax}} ({{$booking_adult_count}},{{$booking_child_count}})</td>
                                    <td>{{$total_amount}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                
                                @elseif($data[0]['booking_type']=="guide")
                                @php
                                $fetch_guide=ServiceManagement::searchGuide($data[0]['booking_type_id']);
                                $guide_name=$fetch_guide['guide_first_name']." ".$fetch_guide['guide_last_name'];
                                @endphp
                                <tr>
                                    <td>GUIDE</td>
                                    <td>{{$guide_name}} <br> <b>Duration</b> : {{$data[0]['booking_subject_days']}} Days</td>
                                    <td>-</td>
                                    <td>{{$total_amount}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @elseif($data[0]['booking_type']=="transfer")
                                @php
                                $transfer_name=$data[0]['booking_subject_name'];
                                $booking_other_info=unserialize($data[0]['booking_other_info']);
                                $transfer_type="";
                                if($booking_other_info['transfer_type']=="city")
                                {
                                    $transfer_type="City Transfer";
                                }
                                else
                                {
                                    $transfer_type="Airport Transfer";
                                }
                                @endphp
                                <tr>
                                    <td>{{strtoupper($transfer_type)}}</td>
                                    <td>{{$transfer_name}} <br> @if($booking_other_info['transfer_type']=="from-airport")<b>Pickup Location : </b>@php $to_city=LoginController::searchAirports($booking_other_info['from_airport']); echo $to_city['airport_master_name']; @endphp<br><b>DropOff Location : </b>@if($booking_other_info['transfer_dropoff_location']!="" && $booking_other_info['transfer_dropoff_location']!=null ){{$booking_other_info['transfer_dropoff_location']}} @else NA @endif
                                        @elseif($booking_other_info['transfer_type']=="to-airport")<b>Pickup Location : </b>@if($booking_other_info['transfer_pickup_location']!="" && $booking_other_info['transfer_pickup_location']!=null ){{$booking_other_info['transfer_pickup_location']}} @else NA @endif <br> <b>DropOff Location : </b>@php $to_city=LoginController::searchAirports($booking_other_info['to_airport']); echo $to_city['airport_master_name']; @endphp @else<b>Pickup Location : </b>{{$booking_other_info['transfer_pickup_location']}} <br> <b>DropOff Location : </b>{{$booking_other_info['transfer_dropoff_location']}} @endif
                                    </td>
                                      <td>{{$total_pax}} ({{$booking_adult_count}},{{$booking_child_count}})</td>
                                    <td>{{$total_amount}}</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @elseif($data[0]['booking_type']=="sightseeing")
                                @php
                                $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($data[0]['booking_type_id']);
                                $sightseeing_name=$fetch_sightseeing['sightseeing_tour_name'];
                                $sightseeing_duration=$fetch_sightseeing['sightseeing_duration'];
                                $booking_subject_name=unserialize($data[0]['booking_subject_name']);
                                if($booking_subject_name['tour_type']=="private")
                                {
                                    $fetch_guide=ServiceManagement::searchGuide($booking_subject_name['guide_id']);
                                    $guide_name=$fetch_guide['guide_first_name'];
                                    $fetch_driver=ServiceManagement::searchDriver($booking_subject_name['driver_id']);
                                    $driver_name=$fetch_driver['driver_first_name'];
                                }
                                @endphp
                                <tr>
                                    <td>SIGHTSEEING</td>
                                    <td>{{$sightseeing_name}} (@if($booking_subject_name['tour_type']=="private") <b>PRIVATE</b> @else <b>GROUP</b> @endif)<br> <b>Duration</b> : {{$sightseeing_duration}} Hours</td>
                                      <td>{{$total_pax}} ({{$booking_adult_count}},{{$booking_child_count}})</td>
                                    <td>{{$total_amount}}</td>
                                </tr>
                                @if($booking_subject_name['tour_type']=="private")
                                <tr>
                                    <td><b>Includes :</b></td>
                                    <td><b>GUIDE : </b>{{$guide_name}} <br> <b>DRIVER : </b>{{$driver_name}}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @else
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endif
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @elseif($data[0]['booking_type']=="itinerary")
                                @php
                                $fetch_itinerary=ServiceManagement::searchItinerary($data[0]['booking_type_id']);
                                $itinerary_name=$fetch_itinerary['itinerary_tour_name'];
                                @endphp
                                <tr>
                                    <td>ITINERARY</td>
                                    <td>{{$itinerary_name}} ({{ date("d/m/Y",strtotime($data[0]['booking_selected_date']))}} - {{ date("d/m/Y",strtotime($data[0]['booking_selected_to_date']))}} )</td>
                                     <td>{{$total_pax}} ({{$booking_adult_count}},{{$booking_child_count}})</td>
                                    <td>{{$total_amount}}</td>
                                </tr>
                                @foreach($data as $itinerary)
                                @if($itinerary['booking_type']=="activity")
                                @php
                                $fetch_activity=ServiceManagement::searchActivity($itinerary['booking_type_id']);
                                $activity_name=$fetch_activity['activity_name'];
                               //$activity_type=$fetch_activity['getActivityType']['activity_type_name'];

                                $total_adult_count=$itinerary['booking_adult_count'] ? $itinerary['booking_adult_count'] : 0;
                                $total_child_count=$itinerary['booking_child_count'] ? $itinerary['booking_child_count'] : 0;
                                $total_activity_pax=($total_adult_count+$total_child_count);
                                @endphp
                                <tr>
                                    <td>ACTIVITY</td>
                                    <td>{{$activity_name}}</td>
                                    <td>{{$total_activity_pax}} ({{$total_adult_count}},{{$total_child_count}})</td>
                                    <td></td>
                                </tr>
                                @elseif($itinerary['booking_type']=="hotel")
                                <?php
                                $fetch_hotel=ServiceManagement::searchHotel($itinerary['booking_type_id']);
                                $hotel_name=$fetch_hotel['hotel_name'];
                                $hotel_type=$fetch_hotel['getHotelType']['hotel_type_name'];
                                $booking_other_info=unserialize($itinerary['booking_other_info']);
                                $room_info_array=array();
                                foreach($booking_other_info as $room_info)
                                {
                                    $room_info_array[]=$room_info['room_qty']." ".$room_info['room_name'];
                                }
                                $room_info=implode(" , ",$room_info_array);
                                ?>
                                <tr>
                                    <td>HOTEL ({{$hotel_type}})</td>
                                    <td>{{$hotel_name}} <br> <b>Rooms</b> : {{$room_info}} <br> <b>No. of Nights</b> : {{$itinerary['booking_subject_days']}}</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @elseif($itinerary['booking_type']=="transfer")
                                @php
                                $transfer_name=$itinerary['booking_subject_name'];
                                $booking_other_info=unserialize($itinerary['booking_other_info']);
                                $transfer_type="";
                                if($booking_other_info['transfer_type']=="city")
                                {
                                    $transfer_type="City Transfer";
                                }
                                else
                                {
                                    $transfer_type="Airport Transfer";
                                }
                                @endphp
                                <tr>
                                    <td>{{strtoupper($transfer_type)}}</td>
                                    <td>{{$transfer_name}} <br> @if($booking_other_info['transfer_type']=="from-airport")<b>Pickup Location : </b>@php $to_city=LoginController::searchAirports($booking_other_info['from_airport']); echo $to_city['airport_master_name']; @endphp<br><b>DropOff Location : </b>@if($booking_other_info['transfer_dropoff_location']!="" && $booking_other_info['transfer_dropoff_location']!=null ){{$booking_other_info['transfer_dropoff_location']}} @else NA @endif
                                        @elseif($booking_other_info['transfer_type']=="to-airport")<b>Pickup Location : </b>@if($booking_other_info['transfer_pickup_location']!="" && $booking_other_info['transfer_pickup_location']!=null ){{$booking_other_info['transfer_pickup_location']}} @else NA @endif <br> <b>DropOff Location : </b>@php $to_city=LoginController::searchAirports($booking_other_info['to_airport']); echo $to_city['airport_master_name']; @endphp @else<b>Pickup Location : </b>{{$booking_other_info['transfer_pickup_location']}} <br> <b>DropOff Location : </b>{{$booking_other_info['transfer_dropoff_location']}} @endif
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @elseif($itinerary['booking_type']=="sightseeing")
                                @php
                                $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary['booking_type_id']);
                                $sightseeing_name=$fetch_sightseeing['sightseeing_tour_name'];
                                $sightseeing_duration=$fetch_sightseeing['sightseeing_duration'];
                                $booking_subject_name=unserialize($itinerary['booking_subject_name']);
                                if($booking_subject_name['tour_type']=="private")
                                {
                                    $fetch_guide=ServiceManagement::searchGuide($booking_subject_name['guide_id']);
                                    $guide_name=$fetch_guide['guide_first_name'];
                                    $fetch_driver=ServiceManagement::searchDriver($booking_subject_name['driver_id']);
                                    $driver_name=$fetch_driver['driver_first_name'];
                                }
                                @endphp
                                <tr>
                                    <td>SIGHTSEEING</td>
                                    <td>{{$sightseeing_name}} (@if($booking_subject_name['tour_type']=="private") <b>PRIVATE</b> @else <b>GROUP</b> @endif) <b>Duration</b> :{{$sightseeing_duration}} Hours @if($booking_subject_name['tour_type']=="private") <b>GUIDE :</b>{{$guide_name}}, <b>DRIVER :</b> {{$driver_name}} @endif</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @endif
                                @endforeach
                                @endif
                                
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
            <div class="container" style="clear:both;margin-bottom:25px;">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table" style="width:50%;margin-left:auto;">
                            <tbody>
                                <tr>
                                    <th>Total amount</th>
                                    <td>{{$data[0]['booking_currency']}} {{$total_amount}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div>
            <br>
            @php
            $bank_details=unserialize($agent_info['agent_bank_details']);
            @endphp
            <div class="container" style="clear:both; margin-bottom:20px; margin-top:25px;">
                <div class="row">
                    <div class="col-md-6">
                        @if($customer_type=="agent")
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>BANK</th>
                                    <td style="text-align:left;">TBC BANK</td>
                                </tr>
                                <tr>
                                    <th>A/C NAME</th>
                                    <td style="text-align:left;">LLC TRAVEL DOOR</td>
                                </tr>
                                <tr>
                                    <th>A/C</th>
                                    <td style="text-align:left;">GE56 TB73 0543 6120 1000 02</td>
                                </tr>
                                <tr>
                                    <th>SWIFT</th>
                                    <td style="text-align:left;">TBCBGE22</td>
                                </tr>
                            </tbody>
                        </table>
                        @else
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th>BANK</th>
                                    <td style="text-align:left;">{{$bank_details[0]['bank_name']}}</td>
                                </tr>
                                <tr>
                                    <th>A/C</th>
                                    <td style="text-align:left;">{{$bank_details[0]['account_number']}}</td>
                                </tr>
                                <tr>
                                    <th>SWIFT</th>
                                    <td style="text-align:left;">{{$bank_details[0]['bank_ifsc']}}</td>
                                </tr>
                            </tbody>
                        </table>
                        @endif
                       <!--  <p style="color:red; text-align:center;">Please note that amount is Net and any transaction fee has to
                        be covered by Sender. Advance amount is non-refundable in case of cancellation</p> -->
                    </div>
                    <div class="col-md-6">
                        <hr style="border:none;border-bottom: 1px solid rgb(179, 179, 179); margin-top:60px;">
                    </div>
                    
                </div>
            </div>
            
            <div class="pdf-footer">
                <div class="row">
                    <div class="col-md-12">
                         <p style="color:red; text-align:center;font-size:12px">Please note that amount is Net and any transaction fee has to
                        be covered by Sender.Advance amount is non-refundable in case of cancellation</p>
                        <hr style="border:none;border-bottom: 1px solid rgb(179, 179, 179);">
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-12" style="text-align:center">
                        @if($customer_type=="agent")
                        <img src="https://www.traveldoor.ge/images/traveldoor_logo.png" style="width: 120px; margin-bottom:10px;" class="img-fluid d-block mx-auto">
                        @else
                        <img src="{{ asset('assets/uploads/agent_logos')}}/{{session()->get('travel_agent_logo')}}" style="width: 120px;height:50px; margin-bottom:10px;" class="img-fluid d-block mx-auto">
                        @endif
                        
                    </div>
                </div>
                
                <div class="bottom-footer">
                    <div class="row">
                        @if($customer_type=="agent")
                        <div class="col-md-4" style="width:33.333%; float:left;"> <p style="color:#FFF; font-size: 13px;text-align: left; margin-left:20px;"><b> Email</b> <a href="#" style="color:#FFF; text-decoration:none;">office@traveldoor.ge</a></p></div>
                        <div class="col-md-4" style="width:33.333%; float:left;"> <p style="color:#FFF; font-size: 13px;text-align: center;"><b>Ph</b> +995 322 35 35 99</p></div>
                        <div class="col-md-4" style="width:33.333%; float:left;"> <p style="color:#FFF; font-size: 13px;text-align: left;"><span
                            style="margin: 5px!important;"> <b>Website:</b> traveldoor.ge </span></p></div>
                            @else
                            <div class="col-md-4" style="width:33.333%; float:left;"> <p style="color:#FFF; font-size: 13px;text-align: left; margin-left:20px;"><b> Email</b> <a href="#" style="color:#FFF; text-decoration:none;">{{$agent_info['company_email']}}</a></p></div>
                            <div class="col-md-4" style="width:33.333%; float:left;"> <p style="color:#FFF; font-size: 13px;text-align: center;"><b>Ph</b> {{$agent_info['company_contact']}}</p></div>
                            <div class="col-md-4" style="width:33.333%; float:left;"> <p style="color:#FFF; font-size: 13px;text-align: left;"><span
                                style="margin: 5px!important;"> <b>Website:</b> </span></p></div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <style>
                    .t-addr {
                        font-weight: 500;
                        color: blue;
                    }
                </style>
            </body>
            </html>