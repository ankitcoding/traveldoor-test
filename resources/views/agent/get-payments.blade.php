<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('agent.includes.top-header')

<style>

    .carousel-item img {
        height: 100%;
    }

    .carousel-item {
        height: 100%;
    }

    p.start_price {
        margin: 0;
    }

    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }

    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;

        border-radius: 5px;
    }

    .hotel_detail {
        height: 150px;
    }

    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }

    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }

    .booking_detail {
        padding: 15px;
    }

    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }

    td p {
        margin: 0;
    }

    td {
        background: gainsboro;
    }

    table {
        border-collapse: separate;
    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #59d25a;
    }

    a.panel-title {
        position: relative !important;
        background: #dfffe3;
        color: green !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #3ca23d; */
    }
    .panel-title {
        display: block;
        margin-top: 0;
        margin-bottom: 0;
        padding: 1.25rem;
        font-size: 18px;
        color: #4d4d4d;
        height: 48px;
    }
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }

    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }

    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
        width: auto;
        max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #279628;
        z-index: 999;
        transform: none !important;
    }

    .tab-content {
        margin-top: 10px;
    }

    div.panel-heading {
        border: 1px solid #59d25a !important;
    }

    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
    div#carousel-example-generic-captions {
        width: 100%;
    }
    div#loaderModal .modal-content {
        background: transparent;
        box-shadow: none;
    }
    div#loaderModal .modal-dialog {
        margin-top: 17%;
    }
    img.loader-img {
        display: block;
        margin: auto;
        width: auto;
        height: 50px;
    }
    div#loaderModal {
        background: #0000005c;
    }
    .table tr td, .table tr th {
        white-space: nowrap !important;
        padding: 10px 20px !important;
        background: white !important;
    }
    .alert-green
    {
        border-color: #28a745 !important;
        background-color: #28a745 !important;
        color: #fff !important;
    }
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('agent.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">	

                @include('agent.includes.nav')

                <div class="content">



                    <div class="content-header">

                        <div class="d-flex align-items-center">

                            <div class="mr-auto">

                                <h3 class="page-title">Payments</h3>

                                <div class="d-inline-block align-items-center">

                                    <nav>

                                        <ol class="breadcrumb">

                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                                            <li class="breadcrumb-item" aria-current="page">Home</li>

                                            <li class="breadcrumb-item active" aria-current="page">Payments

                                            </li>

                                        </ol>

                                    </nav>

                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="row">
                      <div class="box">
                          <div class="box-body">
                            <h5>PAYMENT DETAILS</h5>
                            <div class="row">
                             <div class="col-md-6">
                                 <div class="table-responsive">
                                    <table class="table table-bordered table-condensed table-striped">
                                        <tr>
                                            <th>BOOKING ID</th>
                                            <td>{{$get_booking->booking_sep_id}}</td>
                                        </tr>
                                        <tr>
                                            <th>BOOKING TYPE</th>
                                            <td>{{strtoupper($get_booking->booking_type)}}</td>
                                        </tr>
                                        <tr>
                                            <th>AGENT</th>
                                            <td>{{strtoupper(session()->get('travel_agent_fullame'))}}</td>
                                        </tr>
                                        <tr>
                                            <th>AMOUNT</th>
                                            <td><b>{{$get_booking->booking_currency}} {{$get_installment->booking_install_amount}}</b></td>
                                        </tr>
                                        <tr>
                                            <th>PAYMENT STATUS</th>
                                            <td>@if($payment_status==1) <button class="btn btn-sm btn-primary">Paid</button> @else <button class="btn btn-sm btn-warning">Pending</button> @endif</td>
                                        </tr>
                                        @if($payment_status==1)
                                        <tr>
                                            <th>ADMIN APPROVAL</th>
                                            <td>@if($get_installment->booking_confirm_status==1) <button class="btn btn-sm btn-primary">Approved</button> @elseif($get_installment->booking_confirm_status==2) <button class="btn btn-sm btn-danger">Rejected</button> @else <button class="btn btn-sm btn-warning">Pending</button> @endif</td>
                                        </tr>
                                        <tr>
                                            <th>ADMIN REMARKS</th>
                                            <td>{{$get_installment->booking_confirm_remarks}}</td>
                                        </tr>
                                        @endif
                                    </table>
                                </div>
                            </div>
                        </div> 
                         @if(session()->has("payment-error"))
                        <p class="alert alert-info mt-20">{{session()->get("payment-error")}}</p>
                        @endif

                        @if(session()->has("payment-success"))
                        <p class="alert alert-green mt-20">{{session()->get("payment-success")}}</p>
                        @endif
                        @if($payment_status==0)
                        <form action="{{route('process-payments')}}" method="post" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{csrf_field()}}
                                        <input type="hidden" name="bookingid" value="{{$get_booking->booking_sep_id}}">
                                        <input type="hidden" name="installmentid" value="{{$get_installment->booking_installment_id}}">
                                        <label for="payment_mode" class="form-label">Payment Mode</label>
                                        <br>
                                        <input type="radio" name="payment_mode" id="payment_mode_1" value="online" required="required">
                                        <label for="payment_mode_1">Online Card Payment</label><br>
                                        <input type="radio" name="payment_mode" id="payment_mode_2" value="wallet">
                                        <label for="payment_mode_2">My Wallet <strong>(GEL {{$total_wallet}})</strong></label><br>
                                        <input type="radio" name="payment_mode" id="payment_mode_3" value="moneygram">
                                        <label for="payment_mode_3">Moneygram</label><br>
                                        <input type="radio" name="payment_mode" id="payment_mode_4" value="western-union">
                                        <label for="payment_mode_4">Western Union</label><br>
                                         <input type="radio" name="payment_mode" id="payment_mode_5" value="cash" >
                                        <label for="payment_mode_5">Cash Payment</label><br>
                                        <input type="radio" name="payment_mode" id="payment_mode_6" value="bank-transfer" >
                                        <label for="payment_mode_6">Bank Transfer</label><br>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group" id="payment_info_div">
                                    <input type="text" name="payment_info" id="payment_info" class="form-control" placeholder="Transaction ID / Reference ID / Any other proof of payment" required="required">
                                    <br>
                                    <label for="payment_file">Proof of Payment <small>(Optional)</small></label>
                                    <input type="file" name="payment_file[]" id="payment_file" class="form-control" multiple accept="image/*">
                                 </div>
                                </div>
                                 

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary" id="make_payment" >Proceed</button>


                                </div>
                            </div>
                        </form>
                        @endif
                       
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>


@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
<script>
    $(document).ready(function()
    {
        $('#example_all').DataTable();

        $(document).on("change","input[name='payment_mode']",function()
        {
            if($(this).val()!="online" && $(this).val()!="wallet" )
            {
                $("#payment_info_div").show();
                $("#payment_info").attr("required","required");
            }
            else
            {
                 $("#payment_info_div").hide();
                $("#payment_info").removeAttr("required");
                $("#payment_file,#payment_info").val('');
            }

        });

    });
</script>
</body>
</html>
