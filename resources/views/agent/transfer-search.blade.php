@include('agent.includes.top-header')

<style>
   div.overlay {
        position: absolute;
        width: 100%;
        bottom: 0;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #5d4ecbbd !important;
        border-top-right-radius: 0 !important;
    }

    div.overlay-2 {
        position: absolute;
        width: 100%;
        top: 0;

        opacity: 0;
        bottom: 0;
        z-index: 99;
        transition: .5s ease;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #000000bd !important;
        border-top-right-radius: 0 !important;
    }

    .img-div:hover div.overlay-2 {
        transition: .5s ease;

        opacity: 1;
    }

    .img-div:hover .ribbon2 {
        display: none;
    }

    .img-div:hover::before,
    .img-div:hover::after {
        border: none
    }

    .left {
        float: left;
        width: 50%;
    }

    p.from {
        color: white;
        text-align: right;
        padding: 5px 10px 0;
        font-size: 18px;
        margin: 0;
    }

    span.price-span {
        display: block;
        color: white;
        text-align: right;
        padding: 0 10px;
    }

    img.h-img {
        width: 100%;
    }

   p.h-head {
    color: white;
    font-size: 15px;
    padding: 5px 10px;
    margin: 0;
    z-index: 9999;
    position: absolute;
    top: 0;
    left: 0;
    color: white;
    background: #E91E63;
    width: 100%;
}

    span.s-span {
        color: white;
        padding: 0 10px;
    }

    .img-div {
        border: 1px solid #756fcc;
        padding: 2px;
        border-radius: 5px;
        position: relative;
        z-index: 9;
        margin-bottom: 42px;
        transition: .5s ease;
    }

    /*  .img-div:before {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c70 transparent transparent;
        top: 56px;
        left: -14px;
        
        z-index: 0;
    }

    .img-div:after {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c7000 #402c70 transparent;
        top: -14px;
        left: 58px;
        
        z-index: -37;
    } */

    .ribbon2:before {
        content: "";
        position: absolute;
        top: 0;
        padding: 10px;
        /* margin: 5px; */
        left: 0;
        width: 100px;
        height: 20px;
        background: #5225c5;
        transform: rotate(135deg) translate(28px, 1px);
    }

    .ribbon2 {
        position: absolute;
        top: -7px;
        left: -7px;
        width: 100px;
        height: 100px;
        /* position: absolute; */
        overflow: hidden;
        /* background: aliceblue; */
    }

    button.hover-btn {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        border: none;
        background: #ffffff;
        color: #5225c5;
        padding: 7px 15px;
        cursor: pointer;
        border-radius: 5px;
    }

    @media screen and (max-width:1170px) {
        p.h-head {
            color: white;
            font-size: 15px;
            padding: 0 10px;
            margin: 0;
        }

        p.from {
            color: white;
            text-align: right;
            padding: 0px 10px 0;
            font-size: 15px;
            margin: 0;
        }

        span.s-span {
            color: white;
            padding: 0 10px;
            font-size: 11px;
        }

        span.price-span {
            display: block;
            color: white;
            text-align: right;
            padding: 0 10px;
            font-size: 12px;
        }
    }

     .table-activity-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
  }

   .carousel-item img {

        height: 100%;

    }



    .carousel-item {

        height: 100%;

    }



    p.start_price {

        margin: 0;

    }



    p.country_name.ng-binding {

        font-size: 20px;

        margin: 0;

    }



    .book_card {

        /* padding: 15px; */

        background: #fefeff;

        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);

        margin-bottom: 50px;



        border-radius: 5px;

    }



    .hotel_detail {

        height: 150px;

    }



    a.moredetail.ng-scope {

        background: gainsboro;

        padding: 7px 10px;

        /* margin-bottom: 10px; */

    }



    .booking_label {

        background: #5d53ce;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;

        color: white;

        padding: 10px 15px;

    }



    .booking_detail {

        padding: 15px;

    }



    a.btn.btn-outline.btn-circle.book_btn1 {

        background: #E91E63;

        border-radius: 5px !IMPORTANT;

        padding: 5px 20px;

        width: auto !important;

        height: auto;

        line-height: 2;

        color: white;

    }



    td p {

        margin: 0;

    }



    td {

        background: gainsboro;

    }



    table {

        border-collapse: separate;

    }



    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: 1px solid #59d25a;

    }



    a.panel-title {

        position: relative !important;

        background: #dfffe3;

        color: green !important;

        padding: 13px 20px 13px 85px !important;

        /* border-bottom: 1px solid #3ca23d; */

    }

.panel-title {

    display: block;

    margin-top: 0;

    margin-bottom: 0;

    padding: 1.25rem;

    font-size: 18px;

    color: #4d4d4d;

    height: 48px;

}

    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: none !important;

    }



    .panel-body {

        background: white;

        /* border: 1px solid #59d25a; */

        padding: 10px !important;

    }



    a.panel-title:before {

        content: attr(title) !important;

        position: absolute !important;

        top: 0px !important;

        opacity: 1 !important;

        left: 0 !important;

        padding: 12px 10px;

           width: auto;

    max-width: 250px;

        text-align: center;

        color: white;

        font-family: inherit !important;

        height: 48px;

        background: #279628;

        z-index: 999;

        transform: none !important;

    }


/*
    .tab-content {

        margin-top: 10px;

    }*/



    div.panel-heading {

        border: 1px solid #59d25a !important;

    }



    .panel {

        border-top: none !important;

        margin-bottom: 5px !important;

    }

div#carousel-example-generic-captions {

    width: 100%;

}

/*daman css*/

    .hotel-div {

        width:75%;

        float:left;

        padding: 7px;

        display: flex;

        background: white;

    }



 .hotel-list-div {
    clear: both;
    display: block;
    border: 1px solid #c6bee6;
    border-radius: 5px;
    position: relative;
    /* clear: both; */
    height: 250px;
}

    .hotel-img-div {

       

        width: 40% !important;

        float:left;

    }



    .hotel-details {

     float:left;

        width: 60%;

        padding: 0 15px;

    }



   .hotel-info-div {
    width: 25%;
    float: left;
    padding: 15px;
    background:#dadeff;
    text-align: right;
    height: 100%;
}



    .checked {

        color: orange;

    }



    span.hotel-s {

        border: 1px solid #F44336;

        padding: 2px 5px;

        display: inline-block;

        color: #f44336;

        font-size: 12px;

        margin: 0 10px 0 0;

    }



    p.hotel-name {

        font-size: 22px;

        margin: 10px 0 0;

        color: black;

        float: left;

    }



    .rate-no {

        float: right;

    }



    .rate-no {

        float: right;

        display: block;

        margin-top: 14px;

        background: #2d3134;

        color: white;

        padding: 1px 8px;

        font-size: 12px;

        border-radius: 5px;

    }



    .heading-div {

        clear: both;

    }



    .rating {

        display: block;

        float: left;

        list-style: none;

        margin: 0;

        padding: 0;

    }



    p.r-number {

        float: right;

    }



    p.time-info {

        color: #4CAF50;

    }



    p.info {

        clear: both;

        margin: 0;

    }



    span.tag-item {

        background: #ffcbcd;

        padding: 5px 10px;

        border-radius: 5px;

        color: #ff4e54;

    }



    .inclusions {

        margin: 20px 0;

    }



    span.inclusion-item {

        padding: 10px 10px 0 0;

        color: #644ac9;

    }



    img.icon-i {

        width: auto;

        height: 20px;

    }



    p.include-p {

        color: black;

    }



    .inclusion-p {

        color: green

    }



    p.price-p span {

        background: #ee2128;

        color: white;

        padding: 3px 5px 3px 9px;

        border-radius: 5px;

        position: relative;

        z-index: 9999;

        border-top-right-radius: 5px;

        border-bottom-right-radius: 5px;

        margin-left: 20px;

        border-bottom-left-radius: 4px;

        font-size: 12px;

    }



    p.price-p span:before {

        content: "";

        width: 15px;

        height: 14.5px;

        background: #ee2128;

        position: absolute;

        transform: rotate(45deg);

        top: 3.5px;

        left: -6px;

        border-radius: 0px 0px 0px 3px;

        z-index: -1;

    }



    p.price-p span:after {

        content: "";

        background: white;

        width: 4px;

        height: 4px;

        position: absolute;

        top: 50%;

        left: 0px;

        transform: translateY(-50%);

        border-radius: 50%;

    }



    p.price-p {

        color: #ee2128;

    }



    p.tax {

        font-size: 12px;

        margin: 0;

    }



    p.days {

        font-size: 12px;

        margin: 0;

    }



    p.offer {

        font-size: 25px;

        color: #323995;
        font-weight: bold;

        margin: 0;

    }



    p.cut-price {

        margin: 0;

        text-decoration: line-through;

        font-size: 19px;

    }



    .login-a {

        color: #0088ff;

        font-size: 15px;

        font-weight: bold;

        margin-top: 10px;

        display: block;

    }

    @media screen and (max-width:1200px){

        p.hotel-name {

    font-size: 17px;

    margin: 10px 0 0;

    color: black;

    float: left;

}

p.r-number {

    float: right;

    font-size: 12px;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: block;

}

p.cut-price {

    margin: 0;

    text-decoration: line-through;

    font-size: 16px;

}

p.offer {

    font-size: 20px;

    color: #323995;

    font-weight: bold;

    margin: 0;

}

.login-a {

    color: #0088ff;

    font-size: 13px;

    font-weight: bold;

    margin-top: 10px;

    display: block;

}

    }

    @media screen and (max-width:1200px){

        .hotel-div {

    width: 100%;

    float: left;

    padding: 7px;

    display: flex;

    background: white;

}
button.book-btn {
    background: white;
    border: none;
    border-radius: 50px;
    margin-top: 120px;
    padding: 10px 20px;
    text-align: center;
    color: #323995;
    width: 160px;
}
.hotel-info-div {

    width: 100%;

    height: 100%;

    float: left;

    padding: 15px;

    background:#dadeff;

    text-align: left;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: inline;

}

    }

    @media screen and (max-width:992px){

        p.hotel-name {

    font-size: 17px;

    margin: 10px 0 0;

    color: black;

    float: none;

}

.rate-no {

    float: none;

    display: inline;

    margin-top: 14px;

    background: #2d3134;

    color: white;

    padding: 1px 8px;

    font-size: 12px;

    border-radius: 5px;

}


.rating {

    display: block;

    float: none;

    list-style: none;

    margin: 0;

    padding: 0;

}

p.r-number {

    float: none;

    font-size: 12px;

}

.hotel-details {

    float: none !important;

    width: 100% !important;

    padding: 0 15px;

    margin-top: 20px;

}

.hotel-list-div {

    display: block;

    border: 1px solid #c6bee6;

    border-radius: 5px;

    position: relative;

    /* clear: both; */

    height: 101%;

}

.hotel-div {

    width: 100%;

    float: none;

    padding: 7px;

    display: block;

    background: white;

}

.hotel-img-div {

    width: 100% !important;

    float: none !important;

    display: block !important;

}

a.flex-prev,a.flex-next {

    display: none;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: block;

}

    }

    .img-slide{

            height: 200px;

    width: 100%;

    }

    .flex-control-thumbs {

    margin: 5px 0 0;

    position: static;

    overflow: hidden;

    width: 100%;

    height: 50px;
}
.form-group label {
    color: #313b96;
}
  .table-transfer-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
  }

/*------------ start 1-29-2020---------*/
.selection-div {
    border: none;
    padding: 0;
    background:#dadeff !important;
    border-radius: 5px;
    margin-bottom: 33px !IMPORTANT;
    background: none;
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    height: auto;
}
ul.nav.nav-pills.mb-20 {
    background-color: #2727e07a;
}
#airport_adults{

}
/*.selection-div >.tab-content >.row.mb-10 {
    position: relative;
    background: #2727e07a;
    top: 0;
    left: 14px;
    width: 100%;
    height: 140px;
    bottom: 0;
    padding-top: 26px;
    border-radius: 5px;
    margin-bottom: 0 !important;
}*/
div#navpills-1 {
    position: relative;
  
    /*top: -20px;*/
    left: 0px;
    width: 100%;
    height: auto;
    bottom: 0;
    /* padding-top: 26px; */
    /* border-radius: 5px; */
    /* margin-bottom: 0 !important; */
}
.theme-rosegold .nav-pills > li > a.active:hover, .theme-rosegold .nav-pills > li > a.active:focus {
    border-top-color: #ffffff;
    background-color: #ffffff !important;
    color: #ffffff;
}
.theme-rosegold .btn-success {
    border-color: #F44336;
    background-color: #F44336;
    color: #ffffff;
    min-width: 110px;
}
.select2-container--default .select2-selection--single {
    border: 1px solid #323a95;
    border-radius: 22px;
    /* padding: 6px 12px; */
    /* height: 34px; */
    color: white !important;
    background: #ffffff !important;
}

.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #2f3c95;
    line-height: 28px;
}
.input-group.date input {
    background: #ffffff !important;
    border-color: #323a95;
    border-width: 1px;
    color: black;
    border-radius: 25px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    padding-left: 10px;
    border-right: 0px;
}
.theme-rosegold .btn-success:hover, .theme-rosegold .btn-success:active, .theme-rosegold .btn-success:focus, .theme-rosegold .btn-success.active{
    background-color: #F44336 !important;
    border-color: #F44336 !important;
    color: #fff;
}
.row.mb-10 {
    padding-left: 56px;
}
select#airport_adults, #airport_child {
    background-color: #ffffff;
    border-radius: 22px;
    color: #323a95;
    border-color: #323a95;
}
input#airport_select_time {
    background-color: #ffffff;
    /* border-radius: 22px; */
    border-bottom-left-radius: 17px;
    color: #323a95;
    border-top-left-radius: 17px;
    border: 1px solid #323a95;
    border-right: 0px !important;
}
button#search_airport_transfer {
    background: #323a95;
    border: none;
}
.input-group .input-group-addon {
    border-radius: 5px 0px 0px 5px;
    border-color: #323a95;
    border-left: 0px;
    color: white;
    border-top-right-radius: 20px !important;
    border-bottom-right-radius: 20px !important;
    background-color: #323a95;
}


.cls_right {
    width: 100%;
    text-align: right;
}
.mb-20 {
    margin-bottom: 0px !important;
}
/*------------ end 1-29-2020---------*/

ul.transfer-tabs li a.active {
    border: none;
    background: #005C97;
    background: -webkit-linear-gradient(to right, #363795, #005C97);
    background: linear-gradient(to right, #363795, #005C97);
    border-radius: 50px;
}
ul.nav.nav-pills.transfer-tabs li a {
    border: 1px solid gainsboro;
    border-radius: 50px;
    margin-right: 15px;
}

.transfer-tabs {
    background: no-repeat;
    margin-bottom: 20px;
}
.theme-rosegold .btn-success {
    border-color: #303a95;
    background-color: #323995;
    color: #ffffff;
    min-width: 110px;
}
div#navpills-2,div#navpills-1 {
    padding: 15px 20px;
}
    </style>

    <body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

        <div class="wrapper">

            @include('agent.includes.top-nav')

            <div class="content-wrapper">

                <div class="container-full clearfix position-relative"> 

                    @include('agent.includes.nav')

                    <div class="content">



                        <div class="content-header">

                            <div class="d-flex align-items-center">

                                <div class="mr-auto">

                                    <h3 class="page-title">Transfers</h3>

                                    <div class="d-inline-block align-items-center">

                                        <nav>

                                            <ol class="breadcrumb">

                                                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                                                <li class="breadcrumb-item active" aria-current="page">Home

                                                </li>

                                            </ol>

                                        </nav>

                                    </div>

                                </div>

        </div>

    </div>


    <div class="row">
      <div class="box">
          <div class="box-body">

             <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body" id="tabs">
                             
                            <ul class="nav nav-pills transfer-tabs ">
                                <li class="nav-item"> <a href="#navpills-1" class="nav-link show " data-toggle="tab"
                                aria-expanded="true">Airport Transfer</a> </li>
                                <li class="nav-item"> <a href="#navpills-2" class="nav-link show active" data-toggle="tab"
                                aria-expanded="true">City Transfer</a> </li>
                               
                            </ul>
                            <div class="selection-div">
                             
                    <!-- Tab panes -->
                            <div class="tab-content">
                        <div id="navpills-1" class="tab-pane show ">
                             <div class="row" >
                                <h5 style="color:red" id="airport_error"></h5>
                             </div>
                            <div class="row">

                                 <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="transfer_airport_country">COUNTRY <span class="asterisk">*</span></label>
                                    <select id="transfer_airport_country" name="transfer_airport_country" class="form-control select2" style="width: 100%;">
                                        <option selected="selected" value="0">SELECT COUNTRY</option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-2" >
                                    <div class="form-group" >
                                    <label for="transfer_airport">PICKUP TYPE <span class="asterisk">*</span></label>
                                    <select id="pickup_type" name="pickup_type" class="form-control select2" style="width: 100%;">
                                       <option value="from_airport">From Airport</option>
                                       <option value="to_airport">To Airport</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3" id="airport_div">

                                    <div class="form-group" >
                                        <label for="transfer_airport">AIRPORT <span class="asterisk">*</span></label>
                                        <select id="transfer_airport" name="transfer_airport" class="form-control select2" style="width: 100%;">
                                        <option selected="selected" value="0">SELECT AIRPORT</option>
                                        @foreach($fetch_airports as $airports)
                                        <option value="{{$airports->airport_master_id}}">{{$airports->airport_master_name}}</option>

                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3" id="city_div">
                                    <div class="form-group">
                                        <label for="transfer_airport_city">CITY <span class="asterisk">*</span></label>
                                        <select id="transfer_airport_city" name="transfer_airport_city" class="form-control select2" style="width: 100%;">
                                        <option selected="selected" value="0">SELECT CITY</option>
                                        </select>
                                    </div>
                                    <div class="form-group airport_city_location_div" style="display:none">
                                        <label id="transfer_airport_city_location_label" for="transfer_airport_city_location">Drop off Location <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control map-input" id="transfer_airport_city_location" name="transfer_airport_city_location" placeholder="Drop off Location" style="width: 100%;">
                                          <input type="hidden" name="transfer_airport_city_location_latitude" id="transfer_airport_city_location-latitude" value="0" />
                                            <input type="hidden" name="transfer_airport_city_location_longitude" id="transfer_airport_city_location-longitude" value="0" />
                                      
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">

                                    <div class="form-group">
                                        <label for="airport_date_from">SELECT DATE <span class="asterisk">*</span></label>
                                        <div class="input-group date">
                                            <input type="text" placeholder="FROM"
                                        class="form-control pull-right datepicker" id="airport_date_from" name="airport_date_from" readonly="readonly">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                               
                                <div class="col-sm-12 col-md-12 col-lg-2">

                                    <div class="form-group">
                                        <label for="airport_select_time">Select Time <span class="asterisk">*</span></label>
                                        <div class="input-group bootstrap-timepicker timepicker">
                                        <input type="text" placeholder="Select Time"
                                                                class="form-control pull-right" id="airport_select_time" name="airport_select_time">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                        </div>
                                    </div>
                                
                                </div>

                            </div>
                            <div class="row">
                                   <div class="col-sm-12 col-md-12 col-lg-3">
                                    <div class="form-group">
                                        <label for="airport_adults">Adults<span class="asterisk">*</span></label>
                                        <select class="form-control" id="airport_adults" name="airport_adults">
                                            <option value="">Select</option>
                                            @for($i=1;$i<=50;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-2">
                                    <div class="form-group">
                                        <label for="airport_child">Child <span class="asterisk">*</span></label>
                                        <select class="form-control" id="airport_child" name="airport_child">
                                            <option value="">Select</option>
                                            @for($i=0;$i<=30;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div id="airport_child_age_div" class="row">
                            </div>
                                                 
                            
                            <div class="row">
                               
                                    <div class="col-sm-12 col-md-12 col-lg-11">
                                        <div class="form-group">
                                        <br>
                                            <button id="search_airport_transfer" class="btn btn-rounded  btn-success">Search</button>
                                        </div>
                                    </div>
                               
                            </div>

                        </div>
                        <div id="navpills-2" class="tab-pane show active">
                             <div class="row" >
                                <h5 style="color:red" id="outstation_error"></h5>
                             </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-12 col-lg-3">
                                    <div class="form-group">
                                        <label for="transfer_outstation_country">COUNTRY <span class="asterisk">*</span></label>
                                        <select id="transfer_outstation_country" name="transfer_outstation_country" class="form-control select2" style="width: 100%;">
                                        <option selected="selected" value="0">SELECT COUNTRY</option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">

                                    <div class="form-group">
                                        <label for="transfer_outstation_from_city">FROM CITY <span class="asterisk">*</span></label>
                                        <select id="transfer_outstation_from_city" name="transfer_outstation_from_city" class="form-control select2" style="width: 100%;">
                                        <option selected="selected" value="0">SELECT CITY</option>
                                        </select>
                                    </div>
                                    <div class="form-group outstation_pickup_location_div" style="display:none">
                                        <label for="transfer_outstation_pickup_location">Pick Up Location <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control map-input" id="transfer_outstation_pickup_location" name="transfer_outstation_pickup_location" placeholder="Pick Up Location" style="width: 100%;">
                                          <input type="hidden" name="transfer_outstation_pickup_location_latitude" id="transfer_outstation_pickup_location-latitude" value="0" />
                                            <input type="hidden" name="transfer_outstation_pickup_location_longitude" id="transfer_outstation_pickup_location-longitude" value="0" />
                                      
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">
                                    <div class="form-group">
                                        <label for="transfer_outstation_to_city">TO CITY <span class="asterisk">*</span></label>
                                        <select id="transfer_outstation_to_city" name="transfer_outstation_to_city" class="form-control select2" style="width: 100%;">
                                        <option selected="selected">SELECT CITY</option>
                                        </select>
                                    </div>
                                    <div class="form-group outstation_city_location_div" style="display:none">
                                        <label for="transfer_outstation_city_location">Drop off Location <span class="asterisk">*</span></label>
                                        <input type="text" class="form-control  map-input" id="transfer_outstation_city_location" name="transfer_outstation_city_location" placeholder="Drop off Location" style="width: 100%;">
                                            <input type="hidden" name="transfer_outstation_city_location_latitude" id="transfer_outstation_city_location-latitude" value="0" />
                                            <input type="hidden" name="transfer_outstation_city_location_longitude" id="transfer_outstation_city_location-longitude" value="0" />
                                      
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 
                            <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <label for="outstation_date_from">SELECT DATE <span class="asterisk">*</span></label>
                                    <div class="input-group date">
                                        <input type="text" placeholder="FROM"
                                        class="form-control pull-right datepicker" id="outstation_date_from" name="outstation_date_from" readonly="readonly">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->

                                </div>
                            </div>
                                 <div class="col-sm-12 col-md-12 col-lg-3">
                                     <div class="form-group">
                                    <label for="outstation_select_time">Select Time <span class="asterisk">*</span></label>
                                    <div class="input-group bootstrap-timepicker timepicker">
                                <input type="text" placeholder="Select Time" class="form-control pull-right" id="outstation_select_time" name="outstation_select_time">
                                  <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                </div>
                                    
                                </div>
                               
                            </div>
                        </div>
                        <div class="row">
                              <div class="col-sm-12 col-md-12 col-lg-3">
                              <div class="form-group">
                                    <label for="outstation_adults">Adults<span class="asterisk">*</span></label>
                                    <select class="form-control" id="outstation_adults" name="outstation_adults">
                                        <option value="">Select</option>
                                        @for($i=1;$i<=50;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                               </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="outstation_child">Child <span class="asterisk">*</span></label>
                                    <select class="form-control" id="outstation_child" name="outstation_child">
                                        <option value="">Select</option>
                                        @for($i=0;$i<=30;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="outstation_child_age_div" class="row">
                                                 
                        </div>
                         <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="form-group">
                                    <br>
                                    <button id="search_outstation_transfer" class="btn btn-rounded  btn-success">Search</button>


                                </div>
                    
                            </div>

                        </div>
                    </div> 
                    </div>

                    </div>

                    <div id="transfer_div">

                    </div>
                    <div id="address-map-container" style="width:100%;height:400px;display:none">
    <div style="width: 100%; height: 100%" id="address-map"></div>
</div>
                    <div class="text-center table-transfer-loader" style="display: none">
                      <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                      <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 15 ; 0 -15; 0 15" 
                        repeatCount="indefinite" 
                        begin="0.1"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 10 ; 0 -10; 0 10" 
                        repeatCount="indefinite" 
                        begin="0.2"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 5 ; 0 -5; 0 5" 
                        repeatCount="indefinite" 
                        begin="0.3"/>
                    </circle>
                </svg>
            </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
 <script>

     var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('address-map'), {
          center: {lat: -34.397, lng: 150.644},
          zoom: 8
        });

        initialize();
      }

  function initialize() {



    // $('form').on('keyup keypress', function(e) {
       
    //     var keyCode = e.keyCode || e.which;

    //     if (keyCode === 13) {

    //         e.preventDefault();

    //         return false;

    //     }

    // });

    const locationInputs = document.getElementsByClassName("map-input");



    const autocompletes = [];

    const geocoder = new google.maps.Geocoder;


    for (let i = 0; i < locationInputs.length; i++) {



        const input = locationInputs[i];

        const fieldKey = input.id.replace("-input", "");

        const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';



        const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || -33.8688;

        const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || 151.2195;



        const map = new google.maps.Map(document.getElementById('address-map'), {

            center: {lat: latitude, lng: longitude},

            zoom: 13

        });

        const marker = new google.maps.Marker({

            map: map,

            position: {lat: latitude, lng: longitude},

        });



        marker.setVisible(isEdit);



        const autocomplete = new google.maps.places.Autocomplete(input);

        autocomplete.key = fieldKey;

        autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});

    }




    for (let i = 0; i < autocompletes.length; i++) {

        const input = autocompletes[i].input;

        const autocomplete = autocompletes[i].autocomplete;

        const map = autocompletes[i].map;

        const marker = autocompletes[i].marker;



        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            marker.setVisible(false);

            const place = autocomplete.getPlace();



            geocoder.geocode({'placeId': place.place_id}, function (results, status) {

                if (status === google.maps.GeocoderStatus.OK) {

                    const lat = results[0].geometry.location.lat();

                    const lng = results[0].geometry.location.lng();

                    setLocationCoordinates(autocomplete.key, lat, lng);

                }

            });



            if (!place.geometry) {

                window.alert("No details available for input: '" + place.name + "'");

                input.value = "";

                return;

            }
            



            if (place.geometry.viewport) {

                map.fitBounds(place.geometry.viewport);

            } else {

                map.setCenter(place.geometry.location);

                map.setZoom(17);

            }

            marker.setPosition(place.geometry.location);

            marker.setVisible(true);



        });

    }

}



function setLocationCoordinates(key, lat, lng) {

    const latitudeField = document.getElementById(key + "-" + "latitude");

    const longitudeField = document.getElementById(key + "-" + "longitude");

    latitudeField.value = lat;

    longitudeField.value = lng;

}
    </script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgIHK6tG4ltnx3wnDfmqbg2_bfJeU07fY&libraries=places&callback=initMap" async defer></script>
<script>
    $(document).ready(function()
    {
        $('.select2').select2();
        var date = new Date();
        date.setDate(date.getDate());
        $('#outstation_date_from').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        });

        $('#airport_date_from').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        });



// Time Picker Initialization
$('#airport_select_time').timepicker({
    defaultTime: 'current',
    minuteStep: 1,
     showInputs: false,
});
$('#outstation_select_time').timepicker({
    defaultTime: 'current',
    minuteStep: 1,
     showInputs: false,
});
 $(document).on("change","#transfer_airport_country",function()
        {
         if($("#transfer_airport_country").val()!="0")
         {
             var country_id=$("#transfer_airport_country").val();
              $.ajax({
                url:"{{route('search-country-cities')}}",
                type:"GET",
                data:{"country_id":country_id},
                success:function(response)
                {
                    $("#transfer_airport_city").html(response);  
                     $('#transfer_airport_city').select2();


                }
            });

        }

    });

 $(document).on("change","#transfer_outstation_country",function()
        {
         if($("#transfer_outstation_country").val()!="0")
         {
             var country_id=$("#transfer_outstation_country").val();
              $.ajax({
                url:"{{route('search-country-cities')}}",
                type:"GET",
                data:{"country_id":country_id},
                success:function(response)
                {
                   
                    $("#transfer_outstation_from_city").html(response);
                    $("#transfer_outstation_to_city").html(response);
                    $('#transfer_outstation_from_city').select2();
                    $('#transfer_outstation_to_city').select2();

                }
            });

        }

    });

            


        //change airport and city location based upon pickup type
        $(document).on("change","#pickup_type",function()
        {

            var airport_div=$("#airport_div").html();
            var city_div=$("#city_div").html();
            
            $("#airport_div").html(city_div);
            $("#city_div").html(airport_div);

            if($(this).val()=="from_airport")
            {
                $("#transfer_airport_city_location_label").html("Drop Off Locaton <span class='asterisk'>*</span>")
                $("#transfer_airport_city_location").attr("placeholder","Drop Off Location");
            }
            else
            {
                $("#transfer_airport_city_location_label").html("Pick Up Locaton <span class='asterisk'>*</span>")
                $("#transfer_airport_city_location").attr("placeholder","Pick Up Location");
            }

            $("#transfer_airport,#transfer_airport_city").select2();

            $("#airport_div").find(".select2-container").slice(1).remove();
            $("#city_div").find(".select2-container").slice(1).remove();
            $("#airport_city_location_div").hide();
          
        });

        $(document).on("change","#transfer_airport_city",function()
        {
            if($(this).val()!="0")
            {
                $(".airport_city_location_div").show();
            }
            else
            {
                $(".airport_city_location_div").hide();
            }
        });

        $(document).on("change","#transfer_outstation_from_city",function()
        {
            if($(this).val()!="0")
            {
                $(".outstation_pickup_location_div").show();
            }
            else
            {
                 $(".outstation_pickup_location_div").hide();
            }
        });

        $(document).on("change","#transfer_outstation_to_city",function()
        {
            if($(this).val()!="0")
            {
                $(".outstation_city_location_div").show();
            }
            else
            {
                 $(".outstation_city_location_div").hide();
            }
        });


    //Airport Transfer search filters

    $(document).on("click","#search_airport_transfer",function()
    {
        var pickup_type=$("#pickup_type").val();
        var country_id=$("#transfer_airport_country").val();
        var transfer_airport=$("#transfer_airport").val();
        var transfer_airport_city=$("#transfer_airport_city").val();
          var airport_date_from=$("#airport_date_from").val();
           var airport_select_time=$("#airport_select_time").val();

           var transfer_airport_city_location=$("#transfer_airport_city_location").val();
           var airport_adults=$("#airport_adults").val();
           var airport_child=$("#airport_child").val();
           var error=0;
           var errors_array=[];
          if(country_id=="0")
          {
            errors_array.push("<li>Please Select Country</li>");
            error++;
          }
           if(transfer_airport=="0")
          {
            errors_array.push("<li>Please Select Airport</li>");
            error++;
          }
           if(transfer_airport_city=="0")
          {
            errors_array.push("<li>Please Select City</li>");
            error++;
          }
           if(airport_date_from=="")
          {
            errors_array.push("<li>Please Select Date</li>");
            error++;
          }
           if(airport_select_time=="")
          {
            errors_array.push("<li>Please Select Time</li>");
            error++;
          }
          if(transfer_airport_city_location.trim()=="")
          {
            errors_array.push("<li>Please Enter Drop off/Pick up Location</li>");
            error++;
          }
           if(airport_adults.trim()=="")
          {
            errors_array.push("<li>Please Select No. of Adults</li>");
            error++;
          }
          if(airport_child.trim()=="")
          {
            errors_array.push("<li>Please Select No. of Children</li>");
            error++;
          }
           airport_child_age_array=[];
          var count_child_age=0;
          $(".airport_child_age").each(function()
          {
            airport_child_age_array.push($(this).val());
            if ($(this).val().trim()=="") {
                if(count_child_age==0)
                {
                    errors_array.push("<li>One or more children age is empty</li>");
                    error++;
                }

                count_child_age++;
            }
            else
            {

            }
          });

          if(error>0)
          {

            $("#airport_error").html("<ul>"+errors_array.join("")+"</ul>");
            $("#airport_error").focus();
             $('html, body').animate({
            scrollTop: $("#airport_error").offset().top
        }, 800);   
          }
          else
          {
            $("#airport_error").text("");
            $("#transfer_div").html("");
            $(".table-transfer-loader").show();
            $.ajax({
                url:"{{route('fetchTransfer')}}",
                data:{
                    "_token":"{{csrf_token()}}",
                    "transfer_type":"airport",
                    "pickup_type":pickup_type,
                "country_id":country_id,
                "transfer_airport":transfer_airport,
                "transfer_airport_city":transfer_airport_city,
                 "airport_date_from":airport_date_from,
                  "airport_select_time":airport_select_time,
                  "transfer_airport_city_location":transfer_airport_city_location,
                  "airport_adults":airport_adults,
                  "airport_child":airport_child,
                  "airport_child_age":airport_child_age_array
            },
            type:"POST",
            success:function(response)
            {
                $("#transfer_div").html(response);
                $('html, body').animate({
            scrollTop: $("#transfer_div").offset().top
        }, 800);   
                $(".table-transfer-loader").hide();
            }

        })
        }


    });

    $(document).on("click","#search_outstation_transfer",function()
    {
        var transfer_outstation_from_city=$("#transfer_outstation_from_city").val();
        var country_id=$("#transfer_outstation_country").val();
        var transfer_outstation_pickup_location=$("#transfer_outstation_pickup_location").val();
        var transfer_outstation_to_city=$("#transfer_outstation_to_city").val();
          var transfer_outstation_city_location=$("#transfer_outstation_city_location").val();
           var outstation_date_from=$("#outstation_date_from").val();
            var outstation_select_time=$("#outstation_select_time").val();
             var outstation_adults=$("#outstation_adults").val();
           var outstation_child=$("#outstation_child").val();

          var error=0;
           var errors_array=[];
          if(country_id=="0")
          {
            errors_array.push("<li>Please Select Country</li>");
            error++;
          }
            if(transfer_outstation_from_city=="0")
          {
            errors_array.push("<li>Please Enter From City</li>");
            error++;
          }
           if(transfer_outstation_pickup_location=="")
          {
            errors_array.push("<li>Please Enter Pickup Location</li>");
            error++;
          }
         
           if(transfer_outstation_to_city=="")
          {
            errors_array.push("<li>Please Enter To City</li>");
            error++;
          }
           if(transfer_outstation_city_location=="")
          {
            errors_array.push("<li>Please Enter Dropoff Location</li>");
            error++;
          }
    
           if(outstation_date_from=="")
          {
            errors_array.push("<li>Please Select Date</li>");
            error++;
          }
          if(outstation_select_time=="")
          {
            errors_array.push("<li>Please Select Time</li>");
            error++;
          }
         
           if(outstation_adults.trim()=="")
          {
            errors_array.push("<li>Please Select No. of Adults</li>");
            error++;
          }
          if(outstation_child.trim()=="")
          {
            errors_array.push("<li>Please Select No. of Children</li>");
            error++;
          }

          outstation_child_age_array=[];
          var count_child_age=0;
          $(".outstation_child_age").each(function()
          {
            outstation_child_age_array.push($(this).val());
            if ($(this).val().trim()=="") {
                if(count_child_age==0)
                {
                    errors_array.push("<li>One or more children age is empty</li>");
                    error++;
                }

                count_child_age++;
            }
            else
            {

            }
          });

          if(error>0)
          {

            $("#outstation_error").html("<ul>"+errors_array.join("")+"</ul>");
             $('html, body').animate({
            scrollTop: $("#outstation_error").offset().top
        }, 800);   
          }
          else
          {
            $("#outstation_error").text("");
            $("#transfer_div").html("");
            $(".table-transfer-loader").show();
            $.ajax({
                url:"{{route('fetchTransfer')}}",
                data:{
                    "_token":"{{csrf_token()}}",
                    "transfer_type":"city",
                    "from_city":transfer_outstation_from_city,
                "country_id":country_id,
                "pickup_location":transfer_outstation_pickup_location,
                "to_city":transfer_outstation_to_city,
                 "dropoff_location":transfer_outstation_city_location,
                  "outstation_date_from":outstation_date_from,
                  "outstation_select_time":outstation_select_time,
                  "outstation_adults":outstation_adults,
                  "outstation_child":outstation_child,
                  "outstation_child_age":outstation_child_age_array

            },
            type:"POST",
            success:function(response)
            {
                $("#transfer_div").html(response);
                    $('html, body').animate({
            scrollTop: $("#transfer_div").offset().top
        }, 800);   
                $(".table-transfer-loader").hide();
            }

        })
        }


    });

});
     $(document).on("change","#outstation_child",function()
{
    var html_data="";
    var child_count=$(this).val();
    for($i=1;$i<=child_count;$i++)
    {
        html_data+='<div class="col-md-2"> <div class="form-group"><label for="outstation_child_age'+$i+'" style="color:black">Child Age '+$i+' <span class="asterisk" >*</span></label><input type="text" id="outstation_child_age'+$i+'" name="outstation_child_age[]" class="form-control outstation_child_age"  onkeypress="javascript:return validateNumber(event)" maxlength=2 style="color:black" required></div></div>'
    }
    $("#outstation_child_age_div").html(html_data);
    $("#outstation_child_age_div").show();
});
        $(document).on("change","#airport_child",function()
{
    var html_data="";
    var child_count=$(this).val();
    for($i=1;$i<=child_count;$i++)
    {
        html_data+='<div class="col-md-2"> <div class="form-group"><label for="outstation_child_age'+$i+'" style="color:black">Child Age '+$i+' <span class="asterisk" >*</span></label><input type="text" id="airport_child_age'+$i+'" name="airport_child_age[]" class="form-control airport_child_age"  onkeypress="javascript:return validateNumber(event)" maxlength=2 style="color:black" required></div></div>'
    }
    $("#airport_child_age_div").html(html_data);
    $("#airport_child_age_div").show();
});
</script>


</body>
</html>
