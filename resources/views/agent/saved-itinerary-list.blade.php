<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('agent.includes.top-header')

<style>
    
    .carousel-item img {
        height: 100%;
    }

    .carousel-item {
        height: 100%;
    }

    p.start_price {
        margin: 0;
    }

    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }

    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;

        border-radius: 5px;
    }

    .hotel_detail {
        height: 150px;
    }

    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }

    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }

    .booking_detail {
        padding: 15px;
    }

    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }

    td p {
        margin: 0;
    }

    td {
        background: gainsboro;
    }

    table {
        border-collapse: separate;
    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #59d25a;
    }

    a.panel-title {
        position: relative !important;
        background: #dfffe3;
        color: green !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #3ca23d; */
    }
.panel-title {
    display: block;
    margin-top: 0;
    margin-bottom: 0;
    padding: 1.25rem;
    font-size: 18px;
    color: #4d4d4d;
    height: 48px;
}
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }

    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }

    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
           width: auto;
    max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #279628;
        z-index: 999;
        transform: none !important;
    }

    .tab-content {
        margin-top: 10px;
    }

    div.panel-heading {
        border: 1px solid #59d25a !important;
    }

    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
div#carousel-example-generic-captions {
    width: 100%;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('agent.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('agent.includes.nav')

	<div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">Saved Packages</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Home</li>

                            <li class="breadcrumb-item active" aria-current="page">Saved Packages

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

        </div>

    </div>

   <div class="row">
      <div class="box">
          <div class="box-body">
            <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                <table id="example1" class="table table-striped">
                    <thead>
                        <tr>
                            <td>ID</td>
                            <td>Itinerary Name</td>
                            <td>Customer Name</td>
                             <td>Customer Email</td>
                              <td>Customer Contact</td>
                               <td>Adults</td>
                                <td>Children</td>
                            <td>Night/Days</td>
                            <td>Created Date</td>
                            <td>Action</td>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($fetch_saved_itinerary as $saved_itineray)
                        <tr>
                             <td>{{$loop->iteration}}</td>
                            <td>{{$saved_itineray->agent_itinerary_tour_name}}</td>
                            <td>{{$saved_itineray->customer_name}}</td>
                             <td>{{$saved_itineray->customer_email}}</td>
                              <td>{{$saved_itineray->customer_contact}}</td>
                              <td>{{$saved_itineray->no_of_adults}}</td>
                               <td>{{$saved_itineray->no_of_children}} (
                            @php
                            $child_age=unserialize($saved_itineray->child_age);
                            $child_age_array=array();
                            if(!empty($child_age) && count($child_age)>0)
                            {
                               for($i=0;$i< count($child_age);$i++)
                               {
                                $child_age_array[]=$child_age[$i];
                               }

                               echo implode(",",$child_age_array);
                            }
                            @endphp )</td>
                            <td>{{$saved_itineray->agent_itinerary_tour_days}}/{{($saved_itineray->agent_itinerary_tour_days+1)}}</td>
                            <td style="white-space: nowrap;">{{date('d-M-Y h:i a',strtotime($saved_itineray->created_at))}}</td>
                            <td style="white-space: nowrap;"><a href="{{route('itinerary-details-view',['itinerary_id'=>$saved_itineray->actual_itinerary_id,'saved'=>urlencode(base64_encode(base64_encode($saved_itineray->agent_itinerary_id)))])}}"  class="btn btn-rounded btn-primary btn-sm">Book</a>
                            <a href="{{route('download-saved-itineray',['itinerary_id'=>$saved_itineray->agent_itinerary_id])}}" class="btn btn-rounded btn-primary btn-sm" target="_blank">Download Full Itinerary</a>
                             <a href="{{route('download-saved-restaurant-itineray',['itinerary_id'=>$saved_itineray->agent_itinerary_id])}}" class="btn btn-rounded btn-primary btn-sm" target="_blank">Download Restaurant Itinerary</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            </div>
        
      </div>
   </div>

</div>

</div>

</div>
</div>
</div>


@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
</body>
</html>
