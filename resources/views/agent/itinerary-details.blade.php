<?php
use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\AgentController;
?>
@include('agent.includes.top-header')

<style>
    .hotel-div {
        width: 90%;
        float: left;
        display: flex;
        background: white;
    }

    .hotel-list-div {
        display: block;

        border-radius: 5px;
        position: relative;
        /* clear: both; */
        height: 101%;
    }

    .hotel-img-div {

        width: 40% !important;
        float: left;
    }

    .hotel-details {
        float: left;
        width: 60%;
        padding: 0 15px;
    }

    .hotel-info-div {
        width: 10%;
        float: left;


        text-align: right;
        height: 100%;
    }

    .checked {
        color: orange;
    }

    span.hotel-s {
        border: 1px solid #F44336;
        padding: 2px 5px;
        display: inline-block;
        color: #f44336;
        font-size: 12px;
        margin: 0 10px 0 0;
    }

    p.hotel-name {
        font-size: 16px;
        margin: 10px 0 0;
        color: black;
        float: left;
    }


    .rate-no {
        float: right;
    }

    .rate-no {
        float: right;
        display: block;
        margin-top: 14px;
        background: #2d3134;
        color: white;
        padding: 1px 8px;
        font-size: 12px;
        border-radius: 5px;
    }

    .heading-div {
        clear: both;
    }

    .rating {
    display: flex;
    list-style: none;
    margin: 0;
    justify-content: space-between;
    padding: 0;
    width: 100%;
}

    p.r-number {
        float: right;
    }

    p.time-info {
        color: #4CAF50;
    }

    p.info {
        clear: both;
        margin: 0;
    }

    span.tag-item {
        background: #ffcbcd;
        padding: 5px 10px;
        border-radius: 5px;
        color: #ff4e54;
    }

    .inclusions {
        margin: 20px 0;
    }

    span.inclusion-item {
        padding: 10px 10px 0 0;
        color: #644ac9;
    }

    img.icon-i {
        width: auto;
        height: 20px;
    }

    p.include-p {
        color: black;
    }

    .inclusion-p {
        color: green
    }

    p.price-p span {
        background: #ee2128;
        color: white;
        padding: 3px 5px 3px 9px;
        border-radius: 5px;
        position: relative;
        z-index: 9999;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        margin-left: 20px;
        border-bottom-left-radius: 4px;
        font-size: 12px;
    }

    p.price-p span:before {
        content: "";
        width: 15px;
        height: 14.5px;
        background: #ee2128;
        position: absolute;
        transform: rotate(45deg);
        top: 3.5px;
        left: -6px;
        border-radius: 0px 0px 0px 3px;
        z-index: -1;
    }

    p.price-p span:after {
        content: "";
        background: white;
        width: 4px;
        height: 4px;
        position: absolute;
        top: 50%;
        left: 0px;
        transform: translateY(-50%);
        border-radius: 50%;
    }

    p.price-p {
        color: #ee2128;
    }

    p.tax {
        font-size: 12px;
        margin: 0;
    }

    p.days {
        font-size: 12px;
        margin: 0;
    }

    p.offer {
        font-size: 25px;
        color: black;
        font-weight: bold;
        margin: 0;
    }

    p.cut-price {
        margin: 0;
        text-decoration: line-through;
        font-size: 19px;
    }

    .login-a {
        color: #0088ff;
        font-size: 14px;
        font-weight: 600;
        margin-top: 10px;
        display: block;
    }

    @media screen and (max-width:1200px) {
        p.hotel-name {
            font-size: 15px;
            margin: 10px 0 0;
            color: black;
            float: left;
        }

        p.r-number {
            float: right;
            font-size: 12px;
        }

        span.inclusion-item {
            padding: 10px 10px 0 0;
            color: #644ac9;
            display: block;
        }

        p.cut-price {
            margin: 0;
            text-decoration: line-through;
            font-size: 16px;
        }

        p.offer {
            font-size: 20px;
            color: black;
            font-weight: bold;
            margin: 0;
        }

        .login-a {
            color: #0088ff;
            font-size: 13px;
            font-weight: 600;
            margin-top: 10px;
            display: block;
        }
    }

    @media screen and (max-width:1200px) {
        .hotel-div {
            width: 100%;
            float: left;

            display: flex;
            background: white;
        }

        .hotel-info-div {
            width: 100%;
            height: 100%;
            float: left;



            text-align: left;
        }

        span.inclusion-item {
            padding: 10px 10px 0 0;
            color: #644ac9;
            display: inline;
        }
    }

    @media screen and (max-width:992px) {
        p.hotel-name {
            font-size: 17px;
            margin: 10px 0 0;
            color: black;
            float: none;
        }

        .rate-no {
            float: none;
            display: inline;
            margin-top: 14px;
            background: #2d3134;
            color: white;
            padding: 1px 8px;
            font-size: 12px;
            border-radius: 5px;
        }
        p.r-number {
            float: none;
            font-size: 12px;
        }

        .hotel-details {
            float: none !important;
            width: 100% !important;
            padding: 0 15px;
            margin-top: 20px;
        }

        .hotel-list-div {
            display: block;

            border-radius: 5px;
            position: relative;
            /* clear: both; */
            height: 101%;
        }

        .hotel-div {
            width: 100%;
            float: none;

            display: block;
            background: white;
        }

        .hotel-img-div {
            width: 100% !important;
            float: none !important;
            display: block !important;
        }

        a.flex-prev,
        a.flex-next {
            display: none;
        }

        span.inclusion-item {
            padding: 10px 10px 0 0;
            color: #644ac9;
            display: block;
        }
    }

        .packagePriceContainer {
            border: 1px solid gainsboro;
            border-radius: 5px;
        }

        .flexOne {
            background: #c7fffa;
            padding: 10px;
            border-radius: 5px;
            color: #009688;
        }

        .flexOne p {
            margin-bottom: 5px;
        }

        p.oldPrice {
            text-decoration: line-through;
            font-size: 17px;
            color: #6b6b6b;
            margin-bottom: 0;
        }

        p.c-price {
            color: #852d94;
            font-size: 24px;
            margin-bottom: 0;
        }

        span.font12 {
            font-size: 13px;
            margin-left: 5px;
            color: #616161;
        }

        .priceContainer {
            padding: 10px;
        }

        .wdth70 {
            position: absolute;
            top: 20px;
            right: 36px;
            background: #77d6cd;
            color: white;
            font-size: 12px;
            padding: 5px;
        }

        .viewOffers {
            /* margin-top: 10px; */
            padding: 0 15px;
            border-bottom: 1px solid gainsboro;
        }

        button#bookMyPackage {
            background: #009688;
            color: white;
            padding: 8px 15px;
            display: block;
            width: 120px;
            margin: 13px auto;
            text-align: center;
            border-radius: 21px;
            cursor: pointer;
        }

        img.h-img {
            width: 100%;
            height: 100%;
            border: 1px solid gainsboro;
            border-radius: 5px;
        }

        .viewOffers a {
            color: #59c789;
            font-size: 16px;
        }

        h4.head {
            margin-top: 16px;
            text-transform: capitalize;
            font-size: 22px;
            color: #009688;
        }

        p.itenerary {
            border-bottom: 2px solid #359ff4;
    font-size: 17px;
    color: #2196F3;
    padding-bottom: 5px;
        }

        .nav-pills .nav-link.active,
        .nav-pills .show>.nav-link {
            color: #009688;
            background-color: #ffffff;
            border-radius: 50px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);

        }

        a.nav-link {
            border-radius: 50px !IMPORTANT;
            margin-right: 10px;
            padding: 6px 21px;
            text-align: center;
            font-size: 16px;
        }

        .tab-content {
            margin-top: 20px;
        }

        .t-tab {
    border: 1px solid #d0d0d0;
    padding: 5px;
    border-radius: 5px;
    margin-bottom: 30px;
    margin-left: 25px;
    margin-top: 55px;
    position: relative;
    border-top-left-radius: 0;
}
.t-tab a {
    color: #009688;
}

        p.days {
            color: black;
            font-size: 18px;
            font-weight: 700;
        }

        p.title-1 {
            margin-bottom: 0;
            color: black;
            font-size: 16px;
        }

        p.title-2 {
            color: black;
            margin-bottom: 5px;
        }

        p.p-i {
            color: black;
        }

        span.span-i {
            color: gray;
            font-size: 12px;
        }

        a.remove {
            color: #359ff4;
            font-weight: 600;


        }

        a.chng {
            color: #359ff4;
            font-weight: 600;
        }

        .change {
            display: flex;
            justify-content: space-between;
        }

        .hr {
            border-right: 2px solid gainsboro;
        }

        .heading-div.c-h-div {
            display: flex;
            justify-content: space-between;
        }
        .t-div {
            display: inline-block;
    background: #ffe2ca;
    padding: 7px 20px;
    width: auto !important;
    border-radius: 5px;
    margin: 15px 15px 0;
    
}
div#activity_select__2__1 {
    margin: 0 !important;
}
img.cstm-activity-image {
    width: 100%;
    height: 150px;
}
button#save_itinerary {
    font-size: 13px;
    background: #ffe2ca;
    color: #ff5722;
    border-color: #fdd5c9;
    border-radius: 50px;
}
.theme-rosegold .nav-pills > li > a.active {
    border-top-color: #ffe2ca;
    background-color: #ffe2ca !important;
    color: #FF5722;
}
.theme-rosegold .nav-pills > li > a:hover {
    border-top-color: #ffe2ca !important;
    background-color: #ffe2ca !important;
    color: #FF5722 !important;
}
button#save_itinerary:hover {
    font-size: 13px;
    background: #ffe2ca !important;
    color: #ff5722 !important;
    border-color: #fdd5c9 !important;
    border-radius: 50px;
}
        p.country {
            min-width: 150px;
            margin-bottom: 0;
            font-size: 20px;
            color: #e3aa00;
        }
        p.city {
    margin-bottom: 0;
    font-size: 13px;
    color: #e86e08;
}
        .d-label
        {
                background: #303046;
    color: white;
    padding: 5px 10px;
    margin-bottom: 15px;
    border-radius: 3px;
        }
        .relative.latoBold.font16.lineHeight18.appendBottom20 {
    color: #ffffff;
    font-size: 14px;
    background: #199a8d;
    text-transform: capitalize;
    letter-spacing: 1px;
    padding-bottom: 5px;
    margin-bottom: 15px;
    display: inline-block;
    text-align: center;
    padding: 10px 20px;
    border-radius: 43px;
}
.accordContent {
    background: #c7fffa;
    padding: 15px;
    margin-bottom: 30px;
    border-radius: 5px;
    margin-left: 30px;
    position: relative;
}
.accordContent:before {
    position: absolute;
    top: 50%;
    content: "";
    left: -15px;
    transform: translateY(-50%);
    width: 15px;
    height: 1px;
    background: #c1c1c1;
}
.parent-div-acc div.accordContent:first-child:after {
    position: absolute;
    top: 5px;
    content: "";
    left: -16px;
    transform: translateY(-50%);
    width: 1px;
    height: 82%;
    background: #c1c1c1;
}
.accordContent:after {
    position: absolute;
    top: -16px;
    content: "";
    left: -16px;
    transform: translateY(-50%);
    width: 1px;
    height: 82px;
    background: #c1c1c1;
}
hr.h-hr {
    border-bottom: 1px solid gainsboro !important;
    display: block;
    width: 100%;
    margin-bottom: 30px;
}
label#add_more_rooms
{
    font-size: 12px;
    cursor:pointer;
    margin-top:10px;
}
label.remove_more_rooms {
    color: white;
    font-size: 14px;
    cursor: pointer;
    float: right;
}
div.modal {
    z-index: 9999;
}
div#demo {
    margin-top: 25px;
    height: 300px;
}
.carousel-inner {
    height: 100%;
}
.carousel-item {
    height: 100%;
}
button#go_back_hotel_btn {
    background: #ffcbda;
    border: 1px solid #ffbed0;
    color: #c51e4e;
    border-radius: 0;
}
button#go_back_sightseeing_btn {
    background: #ffcbda;
    border: 1px solid #ffbed0;
    color: #c51e4e;
    border-radius: 0;
}

h2.title {
    margin: 0;
}
p.paragraph {
    margin: 0;
}
h3.amenty-title {
    font-size: 18px;
}
p.blackfont {
    margin: 0;
}
.go-icon {
    font-size: 20px;
    margin-right: 10px;
}
span.text-lt {
    /*max-width: 35%;*/
    width: 40%;
}
.select_activity,.select_hotel,.select_sightseeing
{
    cursor:pointer;
}
img.cstm-hotel-image {
    width: 100%;
    height: 100%;
    object-fit: cover;
}
.day-count {
    background: white;
    margin-bottom: 20px;
    padding: 10px;
    border: 1px solid gainsboro;
    border-radius: 5px;
}
.select_hotel,
    .select_sightseeing,
    .select_activity {
        cursor: pointer !important;
    }

    .color-tiles-text {
        background: #573a9e85;
    }

    .grid-images img {
    width: 30%;
    margin: 10px;
    border: 1px solid #bdbdbd;
    padding: 4px;
    border-radius: 5px;
}
.grid-images {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
}
p.para-h {
    color: #9C27B0;
    font-size: 18px;
    border-bottom: 1px dotted #9C27B0;
}
p.address {
    background: #c7fffa;
    padding: 5px 10px;
    border-radius: 5px;
    color: #009688;
    font-size: 13px;
    display: inline-block;
    margin: 5px 0;
}
.t-tab label {
    color: #545454;
    font-size: 13px;
    padding: 5px 15px;
    /* border-radius: 22px; */
    position: absolute;
    background: #d0d0d0;
    top: -30px;
    left: -1px;
}
p.star-p i {
    background: #ffccbc;
    color: #ff5722;
    padding: 7px;
    border-radius: 14px;
}
p.star-p {
    color: #FF5722;
    font-size: 15px;
    font-weight: 600;
    border-bottom: 1px solid lightgrey;
    padding-bottom: 8px;
}
.cstm-alert-notice{
    background: #e4e4e4 !important;
    color: #2f2f2f;
    border: none;
}
.box .overlay {
    z-index: 50;
    background: rgba(103, 58, 183, 0.39);
    border-radius: 5px;
}
p.color-tiles-text {
    background: #e1e1e1;
    position: absolute;
    width: 100%;
    top: 100%;
    left: 0;
    padding: 5px 10px;
    height: auto;
    display: flex;
    justify-content: center;
    color: #0088ff !important;
    border-radius: 5px;
    align-items: center;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    font-size: 15px;
}
.bg-div {
    background: url(https://crm.traveldoor.ge/dev-custom/assets/images/vehicle-type.jpg)!important;
    height: 130px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 65px;
    position: relative;
}
.theme-rosegold .alert-success {
    border-color: #ffffff;
    background-color: #e0e0e0 !important;
    color: #353535;
}
div.hotels_select .box.box-body.bg-dark.pull-up,.activity_select .box.box-body.bg-dark.pull-up,.sightseeing_select .box.box-body.bg-dark.pull-up,.transfer_select .box.box-body.bg-dark.pull-up{
    height: 115px;
    margin-bottom: 42px;
    margin-top: 20px;
}
.bg-dark {
  /*  background: url("{{asset('assets/images/vehicle-type.jpg')}}")!important;*/
    height: 130px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 65px;
    position: relative;
}
label.tile-label {
    position: absolute !important;
    top: 8px !important;
    left: 20px;
    border-color: white;
    z-index: 999999;
    color: red !important;
}
a.details.text-light {
    color: #673AB7 !important;
    font-weight: 600;
}
div#selectSightseeingModal .modal-content ,div#selectRestaurantModal .modal-content{
    height: 700px;
}
div#selectSightseeingModal .modal-dialog,div#selectRestaurantModal .modal-dialog {
    margin-top: 1%;
}
div#selectSightseeingModal .modal-content .modal-body,div#selectRestaurantModal .modal-content .modal-body {
    padding: 10px;
}
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-track ,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #fbe1ff;
}
div#selectSightseeingModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus ,div#selectRestaurantModal .modal-footer button,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus{
    background: white;
    color: #9c27b0;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-thumb ,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar-thumb{
    background: #9C27B0;
    border-radius: 10px;
}

/* Handle on hover */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #555;
}
div#selectSightseeingModal .modal-lg ,div#selectRestaurantModal .modal-lg{
    max-width: 787px;
}
div#selectSightseeingModal .modal-xl ,div#selectRestaurantModal .modal-xl{
    max-width: 1000px;
}
label.days_no {
    color: white;
    font-size: 15px;
    background: #FF5722;
    padding: 7px 20px;
    border-radius: 5px;
    width: 100px;
}
.days_div .select2-container--default .select2-selection--single {
    border: 1px solid #ffc5b2;
    border-radius: 20px;
    padding: 6px 12px;
    height: 34px;
}
.days_div p.color-tiles-text {
    background: #ff5722;
    position: absolute;
    width: 100%;
    top: 100%;
    left: 0;
    padding: 5px 10px;
    height: auto;
    display: flex;
    justify-content: center;
    color: #ffffff !important;
    border-radius: 5px;
    align-items: center;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    font-size: 15px;
}
button.sightseeing_details,button.sightseeing_details:hover,button.sightseeing_details:focus {
    background: #9C27B0 !important;
    border: none;
    padding: 5px 9px;
    color:white;
    font-size: 11px;
    border-radius: 16px;
}
.select_sightseeing .card .card-body {
    background: #fbe4ff;
}
.select_sightseeing .card {
    border: 1px solid #f7caff;
    padding: 2px;
    min-height: 245px;
}

.select_activity .card .card-body {
    background: #ffe7df;
}

.select_activity .card {
    border: 1px solid #fdc4b1;
    padding: 2px;
    min-height: 240px;
}
.select_activity .card .card-body a{
    background: #FF5722 !important;
    border: none;
    color:white;
    padding: 5px 9px;
    font-size: 11px;
    border-radius: 16px;
}
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}


div#selectHotelModal .modal-content {
    height: 700px;
}
div#selectHotelModal .modal-dialog {
    margin-top: 1%;
}
div#selectHotelModal .modal-content .modal-body {
    padding: 10px;
}
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #edffd9;
}
div#selectHotelModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #8BC34A;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #8BC34A;
    border-radius: 10px;
}

/* Handle on hover */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #8BC34A;
}
div#selectHotelModal .modal-lg {
    max-width: 787px;
}
div#selectHotelModal .modal-xl {
    max-width: 1000px;
}




div#selectActivityModal .modal-content {
    height: 700px;
}
div#selectActivityModal .modal-dialog {
    margin-top: 1%;
}
div#selectActivityModal .modal-content .modal-body {
    padding: 10px;
}
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #fdc4b1;
}
div#selectActivityModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #FF5722;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #FF5722;
    border-radius: 10px;
}

/* Handle on hover */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #e83a00;
}
div#selectActivityModal .modal-lg{
    max-width: 787px;
}
div#selectActivityModal .modal-xl {
    max-width: 1000px;
}
div#selectHotelModal .card-title {
    color: #8BC34A;
    text-transform: capitalize;
    font-size: 16px !important;
    border-bottom: 1px dotted #8BC34A;
}
table#room_type_table tr td, table#room_type_table tr th {
    padding: 5px;
    font-size: 13px;
}
table#room_type_table tr th:last-child {
    width: 100px !important;
}
table#room_type_table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #dee2e6;
}
.select_hotel .card .card-body {
    background: #edffd9 !IMPORTANT;
}
.select_hotel .card {
    border: 1px solid #d0e8b5;
    padding: 0px;
    max-height: 100%;
}
.select_hotel .card a {
    background: #8BC34A;
    border-color: #8BC34A;
    padding: 4px 10px;
    font-size: 13px;
}
table#room_type_table a {
    background: none;
    padding: 0;
}
.select_hotel {
    margin-bottom: 35px;
}


div#selectHotelModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #8BC34A;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #8BC34A;
    border-radius: 10px;
}

/* Handle on hover */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #8BC34A;
}

.modal-filter {
    background: #dbfdfa;
    padding: 10px;
    border-radius: 5px;
}
.modal-filter {
    background: #dbfdfa;
    padding: 10px;
    border: 1px solid #bef2ed;
    border-radius: 5px;
}
.modal-filter select,.modal-filter input,.modal-filter select:focus,.modal-filter input:focus {
    border: 1px solid #9ed9e2;
    border-radius: 50px;
}
button#search_transfer {
    background: #00BCD4;
    border: none;
    font-size: 14px;
    padding: 5px 15px;
    display: block;
    margin-top: 17px;
}
p.price {
    color: #9C27B0;
    font-size: 20px;
    padding: 0;
    text-align: right;
    border: 1px dashed;
    padding: 5px 10px;
    width: 130px;
    display: block;
    margin-left: auto;
}
select.type-select ,select.type-select:focus{
    border: 1px solid #f9d0ff;
    border-radius: 4px;
    width: 50%;
    margin-bottom: 20px;
    background: #fbe1ff;
    color: #9C27B0;
}
select.type-select:focu option{
   
    background: #fff;
    color: #9C27B0;
}
.modal-filter label {
    color: #00bcd4;
    margin-top: 10px;
}
div#transfer_div p:first-child {
    background: #ffe1e1;
    padding: 10px;
    color: #f44336;
    margin: 20px 0;
    border-radius: 5px;
}
.select_transfer .card .card-body {
    background: #dbfdfa;
}
.select_transfer .card {
    border: 1px solid #a6e3ea;
}

div#selectTransferModal .modal-content {
    height: 700px;
}
div#selectTransferModal .modal-dialog {
    margin-top: 1%;
}
div#selectTransferModal .modal-content .modal-body {
    padding: 10px;
}
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #a6e3ea;
}
div#selectTransferModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #00bcd4;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #00bcd4;
    border-radius: 10px;
}

/* Handle on hover */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #00b0ca;
}
div#selectTransferModal .modal-lg{
    max-width: 787px;
}
div#selectTransferModal .modal-xl {
    max-width: 1000px;
}
.select_transfer .card .card-body h4.card-title {
    color: #00bcd4;
    font-weight: 600;
}

button.btn.btn-success.btn-rounded.change_date.pull-right {
    background: #FF5722;
    border: none;
    margin-top: 21px;
}
.days_div .form-group {
    margin-bottom: 0;
}
div#loaderModal {
    background: #0000005c;
}
  .table-sightseeing-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
  }
  .add_activity,.add_restaurant
  {
    margin-top:10px;
  }
  .sticky-sidebar
  {
    max-height: 660px;overflow-y: auto;overflow-x: hidden;
  }
.transfer_guide_remove
{
background-color: red !important; 
}

#restaurant_food_select_info_head
{
    text-align: right;
    display: none;
}

  </style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('agent.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('agent.includes.nav')

                <div class="content">



                    <div class="content-header">

                        <div class="d-flex align-items-center">

                            <div class="mr-auto">

                                <h3 class="page-title">Package Details</h3>

                                <div class="d-inline-block align-items-center">

                                    <nav>

                                        <ol class="breadcrumb">

                                            <li class="breadcrumb-item"><a href="#"><i
                                                        class="mdi mdi-home-outline"></i></a></li>

                                            <li class="breadcrumb-item" aria-current="page">Home </li>

                                            <li class="breadcrumb-item active" aria-current="page">Package Details

                                            </li>

                                        </ol>

                                    </nav>

                                </div>

                            </div>

                                </div>
                            </div>
                             <div class="row">
        <div class="col-12">
            <form id="itinerary_form" action="{{route('itinerary-booking')}}" method="post">
                {{ csrf_field() }}
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                {{-- <div class="col-md-7">
                                    <img class="h-img"
                                        src="https://imgak.mmtcdn.com/hp-images/new/cities/6555/phuket12_520x194.jpg">
                                </div>
                                <div class="col-md-5">
                                    <img class="h-img"
                                        src="https://imgak.mmtcdn.com/hp-images/new/cities/6555/phuket10_520x194.jpg">
                                </div> --}}
                               
                                   <?php
                                   $total_whole_itinerary_cost=0;
                                    $warning_count=0;
                                   ?>
                                <div class="col-md-12">
                                    <h4 class="head">{{$get_itinerary->itinerary_tour_name}}</h4>
                                    <p class="des"><?php echo $get_itinerary->itinerary_tour_description; ?>
                                    </p>
                                     <div class="row">
                                     <div class="col-12 col-sm-6 col-md-6 col-lg-6" style="display:none">
                                      <label class="d-label"> <span><i class="fa fa-calendar"></i> Dates</span> - <span id="date_pik"><?php
                                        echo date('D, d M Y',strtotime($itinerary_date_from))." - ".date('D, d M Y',strtotime("+".($get_itinerary->itinerary_tour_days-1)." days",strtotime($itinerary_date_from)));
                                      ?></span></label>
                                     
                                         </div>

                                          <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                               <label class="d-label"> <span><i class="fa fa-car"></i>  Itinerary </span> - {{$get_itinerary->itinerary_tour_days}} @if($get_itinerary->itinerary_tour_days>1) Nights @else Night @endif {{($get_itinerary->itinerary_tour_days+1)}} 

                                                @php $tour_days=$get_itinerary->itinerary_tour_days+1; @endphp @if($tour_days>1) Days @else Day @endif</label>
                                         <!--   <label> <span><i class="fa fa-users"></i> Travellers</span> - 3 Adults, 0 Child</label> -->
                                         <input type="hidden" name="itinerary_id" value="{{$get_itinerary->itinerary_id}}">
                                          @if(isset($saved))
                                            <input type="hidden" name="itinerary_tour_name" value="{{$fetch_saved_itinerary->agent_itinerary_tour_name}}">
                                          <input type="hidden" name="itinerary_tour_description" value="{{$fetch_saved_itinerary->agent_itinerary_tour_description}}">
                                          
                                           <input type="hidden" name="no_of_days" value="{{$fetch_saved_itinerary->agent_itinerary_tour_days}}">
                                         <input type="hidden" name="from_date" value="{{$fetch_saved_itinerary->from_date}}">
                                            <input type="hidden" name="to_date" value="<?php echo date('Y-m-d',strtotime("+".($get_itinerary->itinerary_tour_days)." days",strtotime($fetch_saved_itinerary->from_date))) ?>">
                                            @php
                                            $itinerary_date_from=$fetch_saved_itinerary->from_date;
                                            @endphp
                                           @else
                                            <input type="hidden" name="itinerary_tour_name" value="{{$get_itinerary->itinerary_tour_name}}">
                                          <input type="hidden" name="itinerary_tour_description" value="{{$get_itinerary->itinerary_tour_description}}">
                                          
                                           <input type="hidden" name="no_of_days" value="{{$get_itinerary->itinerary_tour_days}}">
                                         <input type="hidden" name="from_date" value="{{date('Y-m-d',strtotime($itinerary_date_from))}}">
                                            <input type="hidden" name="to_date" value="<?php echo date('Y-m-d',strtotime("+".($get_itinerary->itinerary_tour_days)." days",strtotime($itinerary_date_from))) ?>">
                                            @endif
                                         </div>

                                      </div>
                                    <p class="itenerary">ITINERARY</p>
                                    <ul class="nav nav-pills">
                                        <li class="nav-item">
                                            <a class="nav-link active" data-toggle="pill" href="#all">Day Plan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#Hotels">Hotels</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#Activity">Activity</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#Site">Sightseeing</a>
                                        </li>
                                         <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#Transfer">Transfers</a>
                                        </li>
                                          <li class="nav-item">
                                            <a class="nav-link" data-toggle="pill" href="#Restaurants">Restaurants</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div class="tab-pane container active" id="all">
                                            @php
                                            if(isset($saved))
                                            {
                                            $itinerary_package_countries=explode(",",$fetch_saved_itinerary->agent_itinerary_package_countries);
                                            $itinerary_package_cities=explode(",",$fetch_saved_itinerary->agent_itinerary_package_cities);
                                            $itinerary_package_title=unserialize($get_itinerary->itinerary_package_title);
                                            $itinerary_package_description=unserialize($get_itinerary->itinerary_package_description);
                                            $itinerary_package_services=unserialize($fetch_saved_itinerary->agent_itinerary_package_services);

                                             }
                                             else
                                             {       
                                            $itinerary_package_countries=explode(",",$get_itinerary->itinerary_package_countries);
                                            $itinerary_package_cities=explode(",",$get_itinerary->itinerary_package_cities);
                                            $itinerary_package_title=unserialize($get_itinerary->itinerary_package_title);
                                            $itinerary_package_description=unserialize($get_itinerary->itinerary_package_description);
                                            $itinerary_package_services=unserialize($get_itinerary->itinerary_package_services);
                                            }

                                            $activity_pax_count_array=array();

                                            @endphp
                                            @for($days_count=0;$days_count<=$get_itinerary->itinerary_tour_days;$days_count++)
                                            <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;" id="days_header{{($days_count+1)}}"> Day {{($days_count+1)}} - 
                                             @if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))

                                             @php 
                                              $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);
                                             echo trim($fetch_sightseeing['sightseeing_tour_name'],","); @endphp 

                                             @elseif(!empty($itinerary_package_services[$days_count]['transfer']['transfer_id']))
                                                Transfer :
                                                @php

                                                $fetch_transfer=ServiceManagement::searchTransfers($itinerary_package_services[$days_count]['transfer']['transfer_id']);

                                            
                                                if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="from-airport") 
                                                {
                                                    $fetch_from_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_from_airport']);

                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                                                    echo "From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];

                                                }
                                                else if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="to-airport")
                                                {

                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                    $fetch_to_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_to_airport']);

                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];
                                                }
                                                else
                                                {
                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);
                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];

                                                }
                                                @endphp
                                            @else
                                                Nothing planned

                                            @endif <span class="fa fa-angle-down text-right"></span></button>
                                            <div class="day-count collapse" id="days_count{{($days_count+1)}}">
                                                <p class="days" style="display:none">Day {{($days_count+1)}} - 
                                             @if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))

                                             @php 
                                              $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);
                                             echo trim($fetch_sightseeing['sightseeing_tour_name'],","); @endphp 

                                             @elseif(!empty($itinerary_package_services[$days_count]['transfer']['transfer_id']))
                                                Transfer :
                                                @php

                                                $fetch_transfer=ServiceManagement::searchTransfers($itinerary_package_services[$days_count]['transfer']['transfer_id']);

                                            
                                                if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="from-airport") 
                                                {
                                                    $fetch_from_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_from_airport']);

                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                                                    echo "From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];

                                                }
                                                else if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="to-airport")
                                                {

                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                    $fetch_to_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_to_airport']);

                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];
                                                }
                                                else
                                                {
                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);
                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];

                                                }
                                                @endphp
                                            @else
                                                Nothing planned

                                            @endif</p>
                                               
                                            @if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))
                                           
                                             @php  $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);
                                             @endphp 
                                              <div class="des">{!! $fetch_sightseeing['sightseeing_attractions'] !!}  </div>

                                               @elseif(!empty($itinerary_package_services[$days_count]['transfer']['transfer_id']))

                                               <div class="des"></div>
                                             @else
                                               <div class="des">
                                             Spend time at leisure
                                                </div>
                                            @endif
                                                <div class="t-div">
                                                    <!-- <p class="country"></p> -->
                                                     <p class="city"><?php 
                                                     $fetch_city_name=ServiceManagement::searchCities($itinerary_package_cities[$days_count],$itinerary_package_countries[$days_count]);
                                                     echo $fetch_city_name['name'];


                                                     ?>,
                                                      @foreach($countries as $country)
                                                        @if($country->country_id==$itinerary_package_countries[$days_count])
                                                        {{$country->country_name}}
                                                        @endif
                                                        @endforeach </p>
                                                         <input type="hidden" id="days_country__{{($days_count+1)}}" name="days_country[{{$days_count}}]" value="{{$itinerary_package_countries[$days_count]}}">

                                                      <input type="hidden" id="days_city__{{($days_count+1)}}"  name="days_city[{{$days_count}}]" value="{{$itinerary_package_cities[$days_count]}}">
                                                        <input type="hidden" id="days_city_name__{{($days_count+1)}}"  name="days_city_name[{{$days_count}}]" value="{{$fetch_city_name['name']}}">
                                                      <?php $days_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?>
                                                      <input type="hidden" id="days_dates__{{($days_count+1)}}" name="days_dates[]" value="{{$days_date}}">
                                                </div>
                                                 @if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_id']))
                                                <div class="t-tab">
                                                    <label>Transfers</label>
                                                    <div class="row">
                                                        @php
                                                        $fetch_transfer=ServiceManagement::searchTransfers($itinerary_package_services[$days_count]['transfer']['transfer_id']);

                                                        $transfer_images=unserialize($fetch_transfer['transfer_vehicle_images']);

                                                        @endphp
                                                        <div class="col-md-12 transfer_select" id="transfer_select__{{($days_count+1)}}" style="margin:0">
                                                           
                                                                <div class="hotel-list-div">
                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($transfer_images[0][0]))
                                                                            <img class="cstm-transfer-image" src="{{ asset('assets/uploads/vehicle_images')}}/{{$transfer_images[0][0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img  class="cstm-transfer-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-transfer-name">@php
                                                                                    $transfer_name="";
                                                                                if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="from-airport") 
                                                                                {
                                                                                    $fetch_from_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_from_airport']);

                                                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                                                                                $transfer_name="From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];

                                                                                    echo "From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];
                                                                                   
                                                                                }
                                                                                else if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="to-airport")
                                                                                {
                                                                                   
                                                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                                                    $fetch_to_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_to_airport']);

                                                                                     $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];

                                                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];

                                                                                }
                                                                                else
                                                                                {

                                                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                                                                                     $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];


                                                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];

                                                                                }


                                                                                @endphp
                                                                               </p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-transfer-car">@php echo $itinerary_package_services[$days_count]['transfer']['transfer_name']; @endphp </p>
                                                                          <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                     <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                                @php 
                                                                                $price=$itinerary_package_services[$days_count]['transfer']['transfer_cost'];
                                                                                $total_transfer_cost=round($price);
                                                                             
                                                                                @endphp
                                                                             <input type="hidden" class="transfer_id" id="transfer_id__{{($days_count+1)}}" name="transfer_id[{{$days_count}}]" value="{{$fetch_transfer['transfer_id'] }}">
                                                                            <input type="hidden" class="transfer_name" name="transfer_name[{{$days_count}}]"  id="transfer_name__{{($days_count+1)}}" value="{{$transfer_name}}">
                                                                             <input type="hidden" class="transfer_car_name" name="transfer_car_name[{{$days_count}}]"  id="transfer_car_name__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['transfer']['transfer_name']}}">
                                                                             <input type="hidden" class="transfer_type" name="transfer_type[{{$days_count}}]"  id="transfer_type__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['transfer']['transfer_type']}}">
                                                                             <input type="hidden" class="transfer_from_city" name="transfer_from_city[{{$days_count}}]"  id="transfer_from_city__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['transfer']['transfer_from_city']}}">
                                                                             <input type="hidden" class="transfer_to_city" name="transfer_to_city[{{$days_count}}]"  id="transfer_to_city__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['transfer']['transfer_to_city']}}">
                                                                              <input type="hidden" class="transfer_from_airport" name="transfer_from_airport[{{$days_count}}]"  id="transfer_from_airport__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['transfer']['transfer_from_airport']}}">
                                                                             <input type="hidden" class="transfer_to_airport" name="transfer_to_airport[{{$days_count}}]"  id="transfer_to_airport__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['transfer']['transfer_to_airport']}}">
                                                                              <input type="hidden" class="transfer_pickup" name="transfer_pickup[{{$days_count}}]"  id="transfer_pickup__{{($days_count+1)}}" value="@if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_pickup'])){{$itinerary_package_services[$days_count]['transfer']['transfer_pickup']}}@endif">
                                                                              <input type="hidden" class="transfer_dropoff" name="transfer_dropoff[{{$days_count}}]"  id="transfer_dropoff__{{($days_count+1)}}" value="@if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_dropoff'])){{$itinerary_package_services[$days_count]['transfer']['transfer_dropoff']}}@endif">

                                                                               <input type="hidden" class="transfer_vehicle_type" name="transfer_vehicle_type[{{$days_count}}]"  id="transfer_vehicle_type__{{($days_count+1)}}" value="@if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_vehicle_type'])){{$itinerary_package_services[$days_count]['transfer']['transfer_vehicle_type']}}@else{{$fetch_transfer['transfer_vehicle_type']}}@endif">
                                                                             
                                                                              <input type="hidden" class="transfer_guide_id" name="transfer_guide_id[{{$days_count}}]" id="transfer_guide_id__{{($days_count+1)}}" value="@if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_guide_id'])){{$itinerary_package_services[$days_count]['transfer']['transfer_guide_id']}}@endif">
                                                                              <input type="hidden" class="transfer_guide_name" name="transfer_guide_name[{{$days_count}}]" id="transfer_guide_name__{{($days_count+1)}}" value="@if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_guide_name'])){{$itinerary_package_services[$days_count]['transfer']['transfer_guide_name']}}@endif">
                                                                              <input type="hidden" class="transfer_guide_cost" name="transfer_guide_cost[{{$days_count}}]" id="transfer_guide_cost__{{($days_count+1)}}" value="@if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_guide_cost'])){{$itinerary_package_services[$days_count]['transfer']['transfer_guide_cost']}}@endif">

                                                                              @php

                                                                              if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_guide_cost']))
                                                                              {
                                                                                $total_transfer_full_cost=$total_transfer_cost+$itinerary_package_services[$days_count]['transfer']['transfer_guide_cost'];
                                                                              }
                                                                              else
                                                                              {
                                                                                  $total_transfer_full_cost=$total_transfer_cost;
                                                                              }
                                                                              
                                                                              @endphp
                                                                              <input type="hidden" class="transfer_cost" name="transfer_cost[{{$days_count}}]" id="transfer_cost__{{($days_count+1)}}" value="{{$total_transfer_cost}}">
                                                                              <input type="hidden" class="calc_cost transfer_total_cost" name="transfer_total_cost[{{$days_count}}]" id="transfer_total_cost__{{($days_count+1)}}" value="{{$total_transfer_full_cost}}">

                                                                              @php
                                                                                 $total_whole_itinerary_cost+=($total_transfer_full_cost);

                                                                              @endphp
                                                                           

                                                                                </div>
                                                                                   </div>
                                                                               <!--   <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div>

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>
                                                                            <p class="title-2">Superior Room </p> -->

                                                            @if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_guide_id']))
                                                            <button type="button" class="btn btn-sm btn-primary select_transfer_guide_btn" id="select_transfer_guide_btn__{{($days_count+1)}}" style="display:none">Select Guide</button>
                                                            <br>
                                                            <span class="selected_transfer_guide_label" id="selected_transfer_guide_label__{{($days_count+1)}}" ><strong>Selected Guide :</strong></span>
                                                            <button type="button" class="btn btn-sm transfer_guide_remove"  id="transfer_guide_remove__{{($days_count+1)}}">Remove</button>
                                                            <p class="selected_transfer_guide_name" id="selected_transfer_guide_name__{{($days_count+1)}}">@if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_guide_name'])){{$itinerary_package_services[$days_count]['transfer']['transfer_guide_name']}}@endif</p>
                                                            @else
                                                              <button type="button" class="btn btn-sm btn-primary select_transfer_guide_btn" id="select_transfer_guide_btn__{{($days_count+1)}}">Select Guide</button>
                                                            <br>
                                                            <span class="selected_transfer_guide_label" id="selected_transfer_guide_label__{{($days_count+1)}}" style="display:none"><strong>Selected Guide :</strong></span>
                                                            <button type="button" class="btn btn-sm transfer_guide_remove"  id="transfer_guide_remove__{{($days_count+1)}}" style="display:none">Remove</button>
                                                            <p class="selected_transfer_guide_name" id="selected_transfer_guide_name__{{($days_count+1)}}"></p>
                                                            @endif


                                                                        </div>

                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                         <a href="#" class="login-a change_transfer" id="change_transfer__{{$days_count+1}}">Change</a>
                                                                   
                                                                         <a href="javascript:void(0)" class="login-a remove_transfer" id="remove_transfer__{{$days_count+1}}">Remove</a>
                                                                       

                                                                    </div>


                                                                </div>


                                                        </div>

                                                    </div>

                                                </div>
                                                @endif
                                                @if($days_count<$get_itinerary->itinerary_tour_days)

                                                @if(!empty($itinerary_package_services[$days_count]['hotel']['hotel_id']) && $itinerary_package_services[$days_count]['hotel']['hotel_no_of_days']!="0")
                                                <div class="t-tab">
                                                    <label>Accomodation/Hotel</label>
                                                    <div class="row">
                                                        @php
                                                        $fetch_hotel=ServiceManagement::searchHotel($itinerary_package_services[$days_count]['hotel']['hotel_id']);

                                                        $hotel_images=unserialize($fetch_hotel['hotel_images']);
                                                  @endphp  
                                                        <div class="col-md-12 hotels_select" id="hotels_select__{{($days_count+1)}}" style="margin:0">
                                                        
                                                                <div class="hotel-list-div">
                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($hotel_images[0]))
                                                                            <img class="cstm-hotel-image" src="{{ asset('assets/uploads/hotel_images')}}/{{$hotel_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img class="cstm-hotel-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="ens">
                                                                                <div class="rating cstm-hotel-rating"
                                                                                    style="display:inline-block;">
                                                                                    @for($stars=1;$stars<=$fetch_hotel['hotel_rating'];$stars++)
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    @endfor

                                                                                </div>
                                                                            </div>

                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-hotel-name">@php echo $fetch_hotel['hotel_name'] @endphp</p>

                                                                            </div>


                                                                            <p class="info cstm-hotel-address">@php echo $fetch_hotel['hotel_address'] @endphp</p>
                                                                          <div class="heading-div c-h-div">
                                                                            <?php 
                                                                            $checkin_print_date=date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from)));
                                                                            $checkin_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); 

                                                                            $checkout_print_date=date('D, d M Y',strtotime("+".($itinerary_package_services[$days_count]['hotel']['hotel_no_of_days'])." days",strtotime($checkin_date)));

                                                                            $checkout_date=date('Y-m-d',strtotime("+".($itinerary_package_services[$days_count]['hotel']['hotel_no_of_days'])." days",strtotime($checkin_date)));
                                                                                       ?>
                                                                                <div class="rating" style="margin: 10px 0;">
                                                                                    <br> 
                                                                                  
                                                                                    <div>
                                                                                        <span class="span-i">Checkout Date</span>
                                                                                        <p class="title-2">{{$checkout_print_date}}</p>
                                                                                    </div>
                                                                                      <div>
                                                                                        <span class="span-i">Checkin Date</span>
                                                                                        <p class="title-2">{{$checkin_print_date}}</p>
                                                                                    </div>
                                                                                       
                                                                                       
                                                                                   </div>
                                                                                <!--  <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div> -->

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>

                                                                            <?php
                                                                             $check_hotels_query=App\Hotels::where('hotel_id',$fetch_hotel['hotel_id'])->where('hotel_approve_status',1)->where('hotel_status',1)->where('booking_validity_from','<=',$checkin_date)->where('booking_validity_to','>=',$checkin_date)->get();


                                                                            
                                                                           $room_avail=0;

                                                                           // $hotel_season_details=unserialize($fetch_hotel['hotel_season_details']);

                                                                           //     $rate_allocation_details=unserialize($fetch_hotel['rate_allocation_details']);

                                                                           //    for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
                                                                           //    {

                                                                           //      if($hotel_season_details[$rate_allocation_count]['booking_validity_from']<=$checkin_date && $hotel_season_details[$rate_allocation_count]['booking_validity_to']>=$checkin_date)
                                                                           //      {
                                                                           //          $room_avail++;
                                                                           //      }
                                                                           //      else
                                                                           //      {
                                                                           //        continue;
                                                                           //      }
                                                                           //  }
                                                                              $hotel_room_id="";
                                                                                     $hotel_room_name="";
                                                                                     $hotel_occupancy_id="";
                                                                                     $hotel_occupancy_qty="";
                                                                                     $hotel_cost="";
                                                                           $rooms_found=0;
                                                                        $hotel_room_name="1-".$itinerary_package_services[$days_count]['hotel']['room_name'];
                                                                           $hotel_cost=$itinerary_package_services[$days_count]['hotel']['hotel_cost'];

                                                                           if(isset($itinerary_package_services[$days_count]['hotel']['room_id']))
                                                                           {
                                                                            // $check_hotel_room=App\HotelRooms::join('hotel_room_season_occupancy_price',' hotel_room_season_occupancy_price.hotel_room_id_fk','=','hotel_rooms.hotel_room_id')->where('hotel_rooms.hotel_id',$fetch_hotel['hotel_id'])->where('hotel_rooms.hotel_room_id',$itinerary_package_services[$days_count]['hotel']['room_id'])->where('hotel_room_season_occupancy_price.hotel_room_occupancy_id',$itinerary_package_services[$days_count]['hotel']['room_occupancy_id'])->first();
                                                                            $check_hotel_room=App\HotelRooms::where('hotel_id',$fetch_hotel['hotel_id'])->where('hotel_room_id',$itinerary_package_services[$days_count]['hotel']['room_id'])->first();

                                                                            if(!empty($check_hotel_room))
                                                                            {
                                                                                $check_hotel_occupancy=App\HotelRoomSeasonOccupancy::where('hotel_room_occupancy_id',$itinerary_package_services[$days_count]['hotel']['room_occupancy_id'])->where('hotel_room_id_fk',$itinerary_package_services[$days_count]['hotel']['room_id'])->first();
                                                                                 if(!empty($check_hotel_occupancy))
                                                                                 {
                                                                                     $get_hotel_seasons=App\HotelRoomSeasons::where('hotel_room_id_fk',$itinerary_package_services[$days_count]['hotel']['room_id'])->where('hotel_room_season_id',$check_hotel_occupancy->hotel_room_season_id_fk)->where('hotel_room_season_validity_from',"<=",$checkin_date)->where('hotel_room_season_validity_to',">=",$checkin_date)->first();
                                                                                    
                                                                                     if(!empty($get_hotel_seasons))
                                                                                     {

                                                                                       $hotel_room_id=$itinerary_package_services[$days_count]['hotel']['room_id'];
                                                                                       $hotel_room_name="1-".$itinerary_package_services[$days_count]['hotel']['room_name']."-(".$check_hotel_occupancy->hotel_room_occupancy_qty." Occupancy)";

                                                                                       $hotel_occupancy_id=$itinerary_package_services[$days_count]['hotel']['room_occupancy_id'];
                                                                                       $hotel_occupancy_qty=$check_hotel_occupancy->hotel_room_occupancy_qty; 
                                                                                       $hotel_cost=$check_hotel_occupancy->hotel_room_occupancy_price;

                                                                                       $rooms_found++;
                                                                                       $room_avail++;

                                                                                       
                                                                                   }
                                                                                   else
                                                                                   {
                                                                                       $rooms_found++;

                                                                                   }

    
                                                                                 }
  
                                                                            }
                                                                        
                                                                           }

                                                                           if($rooms_found==0)
                                                                           {
                                                                            $hotel_rooms=App\HotelRooms::where('hotel_id',$fetch_hotel['hotel_id'])->get();

                                                                                     $hotel_currency=$fetch_hotel['hotel_currency'];

                                                                                     $hotel_room_id="";
                                                                                     $hotel_room_name="";
                                                                                     $hotel_occupancy_id="";
                                                                                     $hotel_occupancy_qty="";
                                                                                     $hotel_cost="";
                                                                                     $room_min_price=0;
                                                                                     foreach($hotel_rooms as $rooms_value)
                                                                                     {

                                                                                      if($hotel_currency==null)
                                                                                      {
                                                                                        $hotel_currency=$rooms_value->hotel_room_currency;
                                                                                      }
                                                                                      $get_hotel_seasons=App\HotelRoomSeasons::where('hotel_room_id_fk',$rooms_value->hotel_room_id)->where('hotel_room_season_validity_from',"<=",$checkin_date)->where('hotel_room_season_validity_to',">=",$checkin_date)->get();

                                                                                      foreach($get_hotel_seasons as $hotel_seasons)
                                                                                      {
                                                                                        $get_occupancy=App\HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();

                                                                                        foreach($get_occupancy as $occupancy)
                                                                                        {
                                                                                          if($room_min_price==0)
                                                                                          {
                                                                                           $room_min_price=$occupancy->hotel_room_occupancy_price;
                                                                                            $hotel_room_name=ucwords($rooms_value->hotel_room_class)." ".ucwords($rooms_value->hotel_room_type)." Room";
                                                                                        $hotel_room_id=$rooms_value->hotel_room_id;
                                                                                         $hotel_room_name="1-".$hotel_room_name."-(".$occupancy->hotel_room_occupancy_qty." Occupancy)";
                                                                                        $hotel_occupancy_id=$occupancy->hotel_room_occupancy_id;
                                                                                         $hotel_occupancy_qty=$occupancy->hotel_room_occupancy_qty;
                                                                                         $room_avail++;

                                                                                       }
                                                                                       else if($occupancy->hotel_room_occupancy_price<$room_min_price)
                                                                                       {
                                                                                        $room_min_price=$occupancy->hotel_room_occupancy_price;
                                                                                        $hotel_room_name=ucwords($rooms_value->hotel_room_class)." ".ucwords($rooms_value->hotel_room_type)." Room";

                                                                                         $hotel_room_name="1-".$hotel_room_name."-(".$occupancy->hotel_room_occupancy_qty." Occupancy)";
                                                                                        $hotel_room_id=$rooms_value->hotel_room_id;
                                                                                        $hotel_occupancy_id=$occupancy->hotel_room_occupancy_id;
                                                                                         $hotel_occupancy_qty=$occupancy->hotel_room_occupancy_qty;
                                                                                         $room_avail++;
                                                                                    }
                                                                                }
                                                                                      }
                                                                     }

                                                                      $new_currency="";
                                                                       if($room_min_price>0)
                                                                      {

                                                                        $price=$room_min_price;
                                                                        if($hotel_currency!="GEL")
                                                                        {
                                                                          $new_currency= $hotel_currency;

                                                                        //   $url="https://free.currconv.com/api/v7/convert?apiKey=".$this->currencyApiKey."&compact=ultra&q=".$new_currency."_".$this->base_currency;
                                                                        //   $cURLConnection = curl_init();

                                                                        //   curl_setopt($cURLConnection, CURLOPT_URL, $url);
                                                                        //   curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

                                                                        //   $convertedPrice = curl_exec($cURLConnection);
                                                                        //   curl_close($cURLConnection);

                                                                        //   $convertedPriceArray=json_decode($convertedPrice);
                                                                        //   $currency_attribute=$new_currency."_".$this->base_currency;

                                                                        //  $conversion_price=round($convertedPriceArray->$currency_attribute, 2);
                                                                        
                                                                          $conversion_price=0;
                                                                          $fetch_html = file_get_contents('https://www.exchange-rates.org/converter/'.$new_currency.'/GEL/1');
                                                                                $scriptDocument = new \DOMDocument();
                                                                                libxml_use_internal_errors(TRUE); //disable libxml errors
                                                                                if(!empty($fetch_html)){
                                                                                   //load
                                                                                   $scriptDocument->loadHTML($fetch_html);
                                                                                 
                                                                                //init DOMXPath
                                                                                  $scriptDOMXPath = new \DOMXPath($scriptDocument);
                                                                                  //get all the h2's with an id
                                                                                  $scriptRow = $scriptDOMXPath->query('//span[@id="ctl00_M_lblToAmount"]');
                                                                                  //check
                                                                                  if($scriptRow->length > 0){
                                                                                    foreach($scriptRow as $row){
                                                                                           $conversion_price=round($row->nodeValue,2);
                                                                                    }
                                                                                  }
                                                                                 
                                                                                }
                                                                         $price=round($price*$conversion_price,2);
                                                                          
                                                                        }




                                                                        $total_cost=round($price);

                                                                        $hotel_cost= $total_cost;


                                                                           }
                                                                       }


                                                                   




                                                                            if(count($check_hotels_query)<=0)
                                                                            {
                                                                                $warning_count++;
                                                                                echo "<p style='color:red' id='hotel_warning__".($days_count+1)."'>Please click edit/change in order to select another hotel, as current one is not available for your dates</p>";
                                                                           }
                                                                            else if($room_avail==0)
                                                                            {
                                                                                $warning_count++;
                                                                                echo "<p style='color:red' id='hotel_warning__".($days_count+1)."'>Please click edit/change in order to select another room/rooms, as current one is not available for your dates</p>";
                                                                           }

                                                                        

                                                                            ?>
                                                                            <p class="title-2 cstm-hotel-room-type">
                                                                            @if(isset($saved)) {{str_replace("-"," ",$itinerary_package_services[$days_count]['hotel']['room_name'])}} @else {{str_replace("-"," ",$hotel_room_name)}}@endif</p>

                                                                            <a href="#" class="login-a change_hotel cstm-change-hotel" id="change_hotel__{{$fetch_hotel['hotel_id']}}_{{$days_count+1}}">Change Room</a>
                                                                             <input type="hidden" class="hotel_id" id="hotel_id__{{($days_count+1)}}" name="hotel_id[{{$days_count}}]" value="{{$fetch_hotel['hotel_id']}}">
                                                                            <input type="hidden" class="hotel_name" id="hotel_name__{{($days_count+1)}}" name="hotel_name[{{$days_count}}]" value="{{$fetch_hotel['hotel_name'] }}">
                                                                              <input type="hidden" class="room_name" id="room_name__{{($days_count+1)}}" name="room_name[{{$days_count}}]" value='@if(isset($saved)) {{$itinerary_package_services[$days_count]["hotel"]["room_name"]}} @else {{$hotel_room_name}}@endif'>
                                                                               <input type="hidden" class="room_qty" id="room_qty__{{($days_count+1)}}" name="room_qty[{{$days_count}}]" value='@if(isset($saved)) {{$itinerary_package_services[$days_count]["hotel"]["room_qty"]}} @else{{"1"}}@endif'>
                                                                              @php
                                                                                    
                                                                                    if(isset($saved))
                                                                                    {
                                                                                        $hotel_cost=0;
                                                                                    //$hotel_cost=$itinerary_package_services[$days_count]['hotel']['hotel_cost'];
                                                                                     $hotel_room_detail=$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'];

                                                                                     $total_room_cost=0;
                                                                                     

                                                                                    for($i_prev=0;$i_prev< count($hotel_room_detail);$i_prev++)
                                                                                    {
                                                                                        $total_room_cost+=$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i_prev]['room_cost'];

                                                                                    }
                                                                                    $hotel_cost=$total_room_cost;
                                                                           
                                                                                    
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        if(trim($hotel_cost)!="" && $hotel_cost!=null)
                                                                                        {
                                                                                            $hotel_cost=($hotel_cost*$itinerary_package_services[$days_count]['hotel']['hotel_no_of_days']); 
                                                                                        }
                                                                                        else
                                                                                        {
                                                                                           $hotel_cost=0; 
                                                                                        }
                                                                                       

                                                                                    }
                                                                              @endphp
                                                                            <input type="hidden" class="calc_cost hotel_cost"  id="hotel_cost__{{($days_count+1)}}" name="hotel_cost[{{$days_count}}]" value="{{$hotel_cost}}">
                                                                            <input type="hidden" class="hotel_no_of_days" id="hotel_no_of_days__{{($days_count+1)}}" name="hotel_no_of_days[{{$days_count}}]" value="{{$itinerary_package_services[$days_count]['hotel']['hotel_no_of_days']}}">
                                                                              <input type="hidden" class="hotel_checkin" id="hotel_checkin__{{($days_count+1)}}" name="hotel_checkin[{{$days_count}}]" value="{{$checkin_date}}">
                                                                                <input type="hidden" class="hotel_checkout" id="hotel_checkout__{{($days_count+1)}}" name="hotel_checkout[{{$days_count}}]" value="{{$checkout_date}}">

                                                                                <div id="hotel_room_detail__{{($days_count+1)}}">
                                                                                    @php
                                                                                    if(!isset($saved))
                                                                                    {
                                                                                        @endphp
                                                                                <input type="hidden" class="hotel_room_name" id="hotel_room_name__{{($days_count+1)}}" name="hotel_room_name[{{$days_count}}][]" value="{{$hotel_room_name}}">

                                                                                <input type="hidden" class="hotel_room_qty" id="hotel_room_qty__{{($days_count+1)}}" name="hotel_room_qty[{{$days_count}}][]" value="1">

                                                                                 <input type="hidden" class="hotel_room_id" id="hotel_room_id__{{($days_count+1)}}" name="hotel_room_id[{{$days_count}}][]" value="{{$hotel_room_id}}">

                                                                                 <input type="hidden" class="hotel_occupancy_id" id="hotel_occupancy_id__{{($days_count+1)}}" name="hotel_occupancy_id[{{$days_count}}][]" value="{{$hotel_occupancy_id}}">

                                                                                <input type="hidden" class="hotel_occupancy_qty" id="hotel_occupancy_qty__{{($days_count+1)}}" name="hotel_occupancy_qty[{{$days_count}}][]" value="{{$hotel_occupancy_qty}}">

                                                                                <input type="hidden" class="hotel_room_cost" id="hotel_room_cost__{{($days_count+1)}}" name="hotel_room_cost[{{$days_count}}][]" value="{{$hotel_cost}}">
                                                                                        @php
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                     $hotel_room_detail=$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'];
                                                                                     @endphp

                                                                                    @for($i=0;$i< count($hotel_room_detail);$i++)
                                                                                <input type="hidden" class="hotel_room_name" id="hotel_room_name__{{($days_count+1)}}" name="hotel_room_name[{{$days_count}}][]" value="{{$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_name']}}">

                                                                                <input type="hidden" class="hotel_room_qty" id="hotel_room_qty__{{($days_count+1)}}" name="hotel_room_qty[{{$days_count}}][]" value="{{$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_qty']}}">


                                                                                <input type="hidden" class="hotel_room_id" id="hotel_room_id__{{($days_count+1)}}" name="hotel_room_id[{{$days_count}}][]" value="@if(isset($itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_id'])){{$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_id']}}@else{{$hotel_room_id}} @endif">

                                                                                 <input type="hidden" class="hotel_occupancy_id" id="hotel_occupancy_id__{{($days_count+1)}}" name="hotel_occupancy_id[{{$days_count}}][]" value="@if(isset($itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_id'])){{$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_id']}}@else{{$hotel_occupancy_id}} @endif">

                                                                                <input type="hidden" class="hotel_occupancy_qty" id="hotel_occupancy_qty__{{($days_count+1)}}" name="hotel_occupancy_qty[{{$days_count}}][]" value="@if(isset($itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_qty'])){{$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_occupancy_qty']}}@else{{$hotel_occupancy_qty}} @endif">


                                                                                <input type="hidden" class="hotel_room_cost" id="hotel_room_cost__{{($days_count+1)}}" name="hotel_room_cost[{{$days_count}}][]" value="{{$itinerary_package_services[$days_count]['hotel']['hotel_room_detail'][$i]['room_cost']}}">
                                                                                     @endfor

                                                                                     @php   
                                                                                 }
                                                                                 @endphp
                                                                             </div>

                                                                            @php 
                                                                            $total_whole_itinerary_cost+=$hotel_cost;
                                                                            @endphp

                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                         
                                                                         <a href="#" class="login-a change_hotel" id="change_hotel__{{$days_count+1}}">Edit/Change</a>
                                                                         <a href="{{route('hotel-detail',['hotelid'=>base64_encode($fetch_hotel['hotel_id']),'itinerary'=>1])}}" class="login-a show_hotel" id="show_hotel__{{$days_count+1}}" target="_blank">Details</a>
                                                                        
                                                                    </div>

                                                                </div>

                                                        </div>


                                                    </div>
                                                </div>
                                                @endif
                                                 @endif

                                               

                                                @if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))
                                                <div class="t-tab">
                                                     <label>Sightseeing</label>
                                                    <div class="row">
                                                        @php
                                                        $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);

                                                        $sightseeing_images=unserialize($fetch_sightseeing['sightseeing_images']);
                                                        @endphp
                                                        <div class="col-md-12 sightseeing_select" id="sightseeing_select__{{($days_count+1)}}" style="margin:0">
                                                           
                                                                <div class="hotel-list-div">
                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($sightseeing_images[0]))
                                                                            <img class="cstm-sightseeing-image" src="{{ asset('assets/uploads/sightseeing_images')}}/{{$sightseeing_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img  class="cstm-sightseeing-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-sightseeing-name">@php echo $fetch_sightseeing['sightseeing_tour_name'] @endphp (@php echo $fetch_sightseeing['sightseeing_distance_covered'] @endphp KMS)</p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-sightseeing-address"><i class="fa fa-map-marker"></i>
                                                                        <?php
                                                                         $get_from_city=ServiceManagement::searchCities($fetch_sightseeing['sightseeing_city_from'],$itinerary_package_countries[$days_count]);
                                                                                       echo $get_from_city['name']."-";

                                                                                        if($fetch_sightseeing['sightseeing_city_between']!=null && $fetch_sightseeing['sightseeing_city_between']!="")
                                                                                        {
                                                                                       $all_between_cities=explode(",",$fetch_sightseeing['sightseeing_city_between']);
                                                                                       for($cities=0;$cities< count($all_between_cities);$cities++)
                                                                                       {
                                                                                        $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$itinerary_package_countries[$days_count]);
                                                                                         echo $fetch_city['name']."-";
                                                                                      }
                                                                                    }
                                                                  
                                                            
                                                                                       $get_from_city=ServiceManagement::searchCities($fetch_sightseeing['sightseeing_city_to'],$itinerary_package_countries[$days_count]);
                                                                                        echo  $get_from_city['name'];

                                                                        ?>
                                                                </p>
                                                                          <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                     <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                                           @php 

                                                                                           if($itinerary_package_services[$days_count]['sightseeing']['sightseeing_tour_type']=="private")
                                                                                           {
                                                                                            $price=$fetch_sightseeing['sightseeing_adult_cost'];
                                                                                           $price+=round($fetch_sightseeing['sightseeing_food_cost']);
                                                                                           $price+=round($fetch_sightseeing['sightseeing_hotel_cost']);

                                                                                           $total_sightseeing_cost=round($price+$fetch_sightseeing['sightseeing_additional_cost']);
                                                                                           $total_adult_cost=round($price);
                                                                                           }
                                                                                           else
                                                                                           {
                                                                                             $price=$fetch_sightseeing['sightseeing_group_adult_cost'];
                                                                                             $total_adult_cost=round($price);
                                                                            
                                                                                              $total_sightseeing_cost=round($price+$fetch_sightseeing['sightseeing_additional_cost']);
                                                                                           }

                                                                                             //$fetchGuidesCost=AgentController::fetchGuidesCost($fetch_sightseeing['sightseeing_id'],$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_id'],$itinerary_package_services[$days_count]['sightseeing']['sightseeing_vehicle_type'],$itinerary_date_from);



                                                                                           //$fetchDriversCost=AgentController::fetchDriversCost($fetch_sightseeing['sightseeing_id'],$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_id'],$itinerary_package_services[$days_count]['sightseeing']['sightseeing_vehicle_type'],$itinerary_date_from);


                                                                                       
                                                                           
                                                                            @endphp


                                                     <?php

                                                     $guide_not_available=0;

                                                     $driver_not_available=0;
                                                     $check_guide_avail=App\Bookings::where('booking_type','guide')->where(function ($q) use($days_date){
                                                        $q->where(function ($q1) use($days_date){ 

                                                            $q1->where('booking_selected_date',"<=",$days_date)
                                                          ->where('booking_selected_to_date',">=",$days_date);
                                                      })->orWhere(function ($q2) use($days_date){ 

                                                          $q2->where('booking_selected_date',"<=",$days_date)
                                                          ->where('booking_selected_to_date',">=",$days_date);
                                                      });

                                                  })->where('booking_type_id',$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_id'])->where('booking_admin_status','!=',2)->get();
                                                     if(count($check_guide_avail)>0){
                                                        $guide_not_available=1;
                                                    }


                                                     $check_driver_avail=App\Bookings::where('booking_type','driver')->where(function ($q) use($days_date){
                                                        $q->where(function ($q1) use($days_date){ 

                                                            $q1->where('booking_selected_date',"<=",$days_date)
                                                          ->where('booking_selected_to_date',">=",$days_date);
                                                      })->orWhere(function ($q2) use($days_date){ 

                                                          $q2->where('booking_selected_date',"<=",$days_date)
                                                          ->where('booking_selected_to_date',">=",$days_date);
                                                      });

                                                  })->where('booking_type_id',$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_id'])->where('booking_admin_status','!=',2)->get();
                                                     if(count($check_driver_avail)>0){
                                                        $driver_not_available=1;
                                                    }


                                                     ?>
                                                                        
                                                                             <input type="hidden" class="sightseeing_id" id="sightseeing_id__{{($days_count+1)}}" name="sightseeing_id[{{$days_count}}]" value="{{$fetch_sightseeing['sightseeing_id'] }}">
                                                                            <input type="hidden" class="sightseeing_name" name="sightseeing_name[{{$days_count}}]"  id="sightseeing_name__{{($days_count+1)}}" value="{{$fetch_sightseeing['sightseeing_tour_name']}}">
                                                                             <input type="hidden" class="sightseeing_tour_type" name="sightseeing_tour_type[{{$days_count}}]"  id="sightseeing_tour_type__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_tour_type']}}">
                                                                             <input type="hidden" class="sightseeing_vehicle_type" name="sightseeing_vehicle_type[{{$days_count}}]"  id="sightseeing_vehicle_type__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_vehicle_type']}}">

                                                                            <input type="hidden" class="sightseeing_guide_id" name="sightseeing_guide_id[{{$days_count}}]"  id="sightseeing_guide_id__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_id']}}">

                                                                                <input type="hidden" class="sightseeing_guide_name" name="sightseeing_guide_name[{{$days_count}}]"  id="sightseeing_guide_name__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_name']}}">
                                                                          
                                                            <input type="hidden" class="sightseeing_guide_cost" name="sightseeing_guide_cost[{{$days_count}}]"  id="sightseeing_guide_cost__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_cost']}}">

                                                            <input type="hidden" class="sightseeing_driver_id" name="sightseeing_driver_id[{{$days_count}}]"  id="sightseeing_driver_id__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_id']}}">

                                                            <input type="hidden" class="sightseeing_driver_name" name="sightseeing_driver_name[{{$days_count}}]"  id="sightseeing_driver_name__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_name']}}">

                                                            <input type="hidden" class="sightseeing_driver_cost" name="sightseeing_driver_cost[{{$days_count}}]"  id="sightseeing_driver_cost__{{($days_count+1)}}" value="{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_cost']}}">

                                                            <input type="hidden" class="sightseeing_adult_cost" name="sightseeing_adult_cost[{{$days_count}}]"  id="sightseeing_adult_cost__{{($days_count+1)}}" value="{{$total_adult_cost}}">
                                                            <input type="hidden" class="sightseeing_additional_cost" name="sightseeing_additional_cost[{{$days_count}}]"  id="sightseeing_additional_cost__{{($days_count+1)}}" value="{{$fetch_sightseeing['sightseeing_additional_cost']}}">
                                                    <input type="hidden" class="sightseeing_cities" name="sightseeing_cities[{{$days_count}}]"  id="sightseeing_cities__{{($days_count+1)}}" value="@if($fetch_sightseeing['sightseeing_city_between']!=""){{$fetch_sightseeing['sightseeing_city_between']}},{{$fetch_sightseeing['sightseeing_city_to']}}@elseif($fetch_sightseeing['sightseeing_city_to']!=""){{$fetch_sightseeing['sightseeing_city_to']}}@else{{$fetch_sightseeing['sightseeing_city_from']}}@endif">

                                                             @php
                                                                           $total_sightseeing_cost+=$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_cost'];
                                                                             $total_sightseeing_cost+=$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_cost'];
                                                                             //$total_sightseeing_cost+=$fetchGuidesCost;
                                                                             //$total_sightseeing_cost+=$fetchDriversCost;
                                                                             @endphp

                                                                            <input type="hidden" class="calc_cost sightseeing_cost" name="sightseeing_cost[{{$days_count}}]" id="sightseeing_cost__{{($days_count+1)}}" value="{{$total_sightseeing_cost}}">
                                                                            @php
                                                                             $total_whole_itinerary_cost+=($total_sightseeing_cost);
                                                                            @endphp

                                                                                </div>
                                                                                   </div>
                                                                                    <button type="button" class="btn btn-sm btn-primary view_guide_btn" id="view_{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_guide_id']}}">View Guide</button>
                                                                             <button type="button" class="btn btn-sm btn-primary view_driver_btn" id="view_{{$itinerary_package_services[$days_count]['sightseeing']['sightseeing_driver_id']}}">View Driver</button>
                                                                               <!--   <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div>

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>
                                                                            <p class="title-2">Superior Room </p> -->
                                                                           <?php

                                                                            // if($fetchGuidesCost==0 || $fetchDriversCost==0)
                                                                            // {
                                                                                //echo "<p style='color:red' id='sightseeing_warning__".($days_count+1)."'>We suggest you to change this sightseeing as guide or driver either not available or their prices are not present.</p>";
                                                                           // }

                                                                           if($guide_not_available==1 && $driver_not_available==1)
                                                                            {
                                                                                $warning_count++;
                                                                               echo "<p style='color:red' id='sightseeing_warning__".($days_count+1)."'>Please click change in order to select another guide and driver , as current one is not available for your dates</p>";
                                                                           }
                                                                           else if($guide_not_available==1)
                                                                           {
                                                                             $warning_count++;
                                                                               echo "<p style='color:red' id='sightseeing_warning__".($days_count+1)."'>Please click change in order to select another guide, as current one is not available for your dates</p>";  
                                                                           }
                                                                           else if($driver_not_available==1)
                                                                           {
                                                                             $warning_count++;
                                                                               echo "<p style='color:red' id='sightseeing_warning__".($days_count+1)."'>Please click change in order to select another driver, as current one is not available for your dates</p>";  
                                                                           }
                                                                           ?>
                                                                        </div>

                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                        
                                                                         <a href="#" class="login-a change_sightseeing" id="change_sightseeing__{{$days_count+1}}">Change</a>
                                                                           <a href="javascript:void(0)" class="login-a remove_sightseeing" id="remove_sightseeing__{{$days_count+1}}">Remove</a>
                                                                          <a href="{{route('sightseeing-details-view',['sightseeing_id'=>base64_encode($fetch_sightseeing['sightseeing_id']),'itinerary'=>1])}}" class="login-a shows_sightseeing" id="shows_sightseeing__{{$days_count+1}}" target="_blank">Details</a>

                                                                         
                                                                         
                                                                    </div>

                                                                </div>


                                                        </div>

                                                    </div>

                                                    </div>
                                                    <div class="text-center"><button type="button" class="btn btn-md btn-primary add_sightseeing" id="add_sightseeing__{{($days_count+1)}}" style="display:none">+ Add Sightseeing</button></div>
                                                @else
                                                 <div class="t-tab" style="display:none">
                                                     <label>Sightseeing</label>
                                                    <div class="row">
                                            
                                                        <div class="col-md-12 sightseeing_select" id="sightseeing_select__{{($days_count+1)}}" style="margin:0">
                                                           
                                                                <div class="hotel-list-div">
                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                           
                                                                                  <img  class="cstm-sightseeing-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                               

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-sightseeing-name"></p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-sightseeing-address"><i class="fa fa-map-marker"></i>
                                                                       
                                                                </p>
                                                                          <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                     <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                                          
                                                                             <input type="hidden" class="sightseeing_id" id="sightseeing_id__{{($days_count+1)}}" name="sightseeing_id[{{$days_count}}]" value="">
                                                                            <input type="hidden" class="sightseeing_name" name="sightseeing_name[{{$days_count}}]"  id="sightseeing_name__{{($days_count+1)}}" value="">
                                                                             <input type="hidden" class="sightseeing_tour_type" name="sightseeing_tour_type[{{$days_count}}]"  id="sightseeing_tour_type__{{($days_count+1)}}" value="">
                                                                             <input type="hidden" class="sightseeing_vehicle_type" name="sightseeing_vehicle_type[{{$days_count}}]"  id="sightseeing_vehicle_type__{{($days_count+1)}}" value="">

                                                                            <input type="hidden" class="sightseeing_guide_id" name="sightseeing_guide_id[{{$days_count}}]"  id="sightseeing_guide_id__{{($days_count+1)}}" value="">

                                                                                <input type="hidden" class="sightseeing_guide_name" name="sightseeing_guide_name[{{$days_count}}]"  id="sightseeing_guide_name__{{($days_count+1)}}" value="">
                                                                          
                                                            <input type="hidden" class="sightseeing_guide_cost" name="sightseeing_guide_cost[{{$days_count}}]"  id="sightseeing_guide_cost__{{($days_count+1)}}" value="0">

                                                            <input type="hidden" class="sightseeing_driver_id" name="sightseeing_driver_id[{{$days_count}}]"  id="sightseeing_driver_id__{{($days_count+1)}}" value="">

                                                            <input type="hidden" class="sightseeing_driver_name" name="sightseeing_driver_name[{{$days_count}}]"  id="sightseeing_driver_name__{{($days_count+1)}}" value="">

                                                            <input type="hidden" class="sightseeing_driver_cost" name="sightseeing_driver_cost[{{$days_count}}]"  id="sightseeing_driver_cost__{{($days_count+1)}}" value="0">

                                                            <input type="hidden" class="sightseeing_adult_cost" name="sightseeing_adult_cost[{{$days_count}}]"  id="sightseeing_adult_cost__{{($days_count+1)}}" value="0">
                                                              <input type="hidden" class="sightseeing_additional_cost" name="sightseeing_additional_cost[{{$days_count}}]"  id="sightseeing_additional_cost__{{($days_count+1)}}" value="0">
                                                              <input type="hidden" class="sightseeing_cities" name="sightseeing_cities[{{$days_count}}]"  id="sightseeing_cities__{{($days_count+1)}}" value="0">
                                                    

                                                                            <input type="hidden" class="calc_cost sightseeing_cost" name="sightseeing_cost[{{$days_count}}]" id="sightseeing_cost__{{($days_count+1)}}" value="0">

                                                                                </div>
                                                                                   </div>
                                                                                   <button type="button" class="btn btn-sm btn-primary view_guide_btn">View Guide</button>
                                                                             <button type="button" class="btn btn-sm btn-primary view_driver_btn">View Driver</button>
                                                                               <!--   <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div>

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>
                                                                            <p class="title-2">Superior Room </p> -->
                                                            
                                                                        </div>

                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                        
                                                                         <a href="#" class="login-a change_sightseeing" id="change_sightseeing__{{$days_count+1}}">Change</a>
                                                                           <a href="javascript:void(0)" class="login-a remove_sightseeing" id="remove_sightseeing__{{$days_count+1}}">Remove</a>
                                                                          <a href="" class="login-a shows_sightseeing" id="shows_sightseeing__{{$days_count+1}}" target="_blank">Details</a>

                                                                         
                                                                         
                                                                    </div>

                                                                </div>


                                                        </div>

                                                    </div>

                                                    </div>
                                                    <div class="text-center"><button type="button" class="btn btn-md btn-primary add_sightseeing" id="add_sightseeing__{{($days_count+1)}}">+ Add Sightseeing</button></div>
                                                
                                                
                                                @endif

                                                @php
                                                $activity_id_status=0;
                                                if(isset($itinerary_package_services[$days_count]['activity']['activity_id']))
                                                {
                                                    $check_activity_id_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);

                                                if($check_activity_id_count==1)
                                                {
                                                    if($itinerary_package_services[$days_count]['activity']['activity_id'][0]!="")
                                                    {
                                                        $activity_id_status++;
                                                    }
    
                                                }
                                                else
                                                {
                                                    for($check=0;$check< $check_activity_id_count; $check++)
                                                    {
                                                        if($itinerary_package_services[$days_count]['activity']['activity_id'][$check]!="")
                                                        {
                                                             $activity_id_status++;
                                                        }
                                                    }
                                                } 
                                                }

@endphp
                                                
                                                @if($activity_id_status>0)
                                                  <div class="t-tab">
                                                    <label>Activities</label>
                                                    @php
                                                        $activity_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);
                                                    @endphp
                                                    @for($activity_counter=0;$activity_counter < $activity_count;$activity_counter++)
                                                        @if($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]=="")
                                                            @continue
                                                        @endif
                                                     @php
                                                     $activity_pax_count_array[$days_count][$activity_counter]['exists']=1;
                                                     @endphp

                                                     <?php

                                                     $activity_not_available=0;
                                                     $check_activity_avail=App\Activities::where('activity_id',$itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter])->where('validity_fromdate','<=',$days_date)->where('validity_todate','>=',$days_date)->get();
                                                     if(count($check_activity_avail)<=0){
                                                        $activity_not_available=1;
                                                     }


                                                     ?>
                                                    <div class="row">
                                                        @php
                                                        $fetch_activity=ServiceManagement::searchActivity($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]);
                                                        $adult_age="";
                                                        $child_age="";
                                                        $age_group_details=unserialize($fetch_activity['age_group_details']);
                                                        if(!empty($age_group_details))
                                                        {
                                                            $adult_age=$age_group_details['adults'];
                                                            $child_age=$age_group_details['child'];

                                                        }
                                                        if(!empty($adult_age) && $adult_age['allowed']=="yes") 
                                                        {
                                                         $activity_pax_count_array[$days_count][$activity_counter]['adult_price_details']=unserialize($fetch_activity['adult_price_details']);
                                                     }
                                                     else{
                                                     $activity_pax_count_array[$days_count][$activity_counter]['adult_price_details']=array();

                                                 }

                                                 if(!empty($child_age) && $child_age['allowed']=="yes") 
                                                 { 
                                                   $activity_pax_count_array[$days_count][$activity_counter]['child_price_details']=unserialize($fetch_activity['child_price_details']);
                                               }
                                               else
                                               {
                                                $activity_pax_count_array[$days_count][$activity_counter]['child_price_details']=array(); 
                                            }
                                                        $activity_images=unserialize($fetch_activity['activity_images']);
                                                        @endphp
                                                        <div class="col-md-12 activity_select activity_select__{{$days_count+1}}" id="activity_select__{{$days_count+1}}__{{($activity_counter+1)}}"  @if($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]=="") style="margin:0 0 25px;display:none" @else style="margin:0 0 25px" @endif>
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($activity_images[0]))
                                                                            <img class="cstm-activity-image" src="{{ asset('assets/uploads/activities_images')}}/{{$activity_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img class="cstm-activity-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-activity-name"> {{$fetch_activity['activity_name']}}</p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-activity-address"><i class="fa fa-map-marker"></i> {{$fetch_activity['activity_location']}}</p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                    </div>
                                                                                        @php 
                                                                                         $adult_price=0;
                                                                                      $adult_price_details=unserialize($fetch_activity['adult_price_details']);

                                                                                      if(!empty($adult_price_details))
                                                                                      {
                                                                                         $adult_price=$adult_price_details[0]['adult_pax_price'];
                                                                                      }
                                                                                      else
                                                                                      {
                                                                                        $adult_price=0;
                                                                                      }
                                                                                              $total_activity_cost=round($adult_price);
                                                                                            $total_whole_itinerary_cost+=($total_activity_cost);
                                                                                            @endphp
                                                                                         <input type="hidden" class="activity_id" id="activity_id__{{($days_count+1)}}__{{($activity_counter+1)}}" name="activity_id[{{$days_count}}][]" value="{{$fetch_activity['activity_id']}}">
                                                                            <input type="hidden" class="activity_name" name="activity_name[{{$days_count}}][]"  id="activity_name__{{($days_count+1)}}__{{($activity_counter+1)}}" value="{{$fetch_activity['activity_name']}}">
                                                                                        <input type="hidden" class="calc_cost activity_cost" id="activity_cost__{{($days_count+1)}}__{{($activity_counter+1)}}"  
                                                                                        name="activity_cost[{{$days_count}}][]"
                                                                                        value="{{$total_activity_cost}}">


                                                                                             
                                                                                </div>
                                                                                   </div>
                                                                                   <?php
        
                                                                            if($activity_not_available==1 || $activity_not_available=="1")
                                                                            {
                                                                                $warning_count++;
                                                                                echo "<p style='color:red' id='activities_warning__".($days_count+1)."__".($activity_counter+1)."'>Please click change in order to select another activity, as current one is not available for your dates</p>";
                                                                           }
                                                                           ?>
                                                                                     @if(isset($saved))
                                        @php
                                   $activities_select_adults=unserialize($fetch_saved_itinerary->activities_select_adults);
                                 $activities_select_child=unserialize($fetch_saved_itinerary->activities_select_child);
                                  $activities_select_child_age=unserialize($fetch_saved_itinerary->activities_select_child_age);
                                  @endphp

                                                                                       <div class="row activities_pax_div activities_pax_div__{{($days_count+1)}}" id="activity_pax_div__{{($days_count+1)}}__{{($activity_counter+1)}}" @if(!empty($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'])) style="margin-top: 10px;" @else  style="margin-top: 10px;display:none" @endif>
                                         <div class="col-md-6">

                                          @php

                                        
                                        if(!empty($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details']))
                                        {
                                            $min_adult=$activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'][0]['adult_min_pax'];
                                        $max_adult=$activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'][0]['adult_max_pax'];
                                         foreach($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'] as $adult_price_i)
                                         {
                                            if($adult_price_i['adult_min_pax']<$min_adult)
                                            {
                                                $min_adult=$adult_price_i['adult_min_pax'];
                                            }
                                            if($adult_price_i['adult_max_pax']>$max_adult)
                                            {
                                                $max_adult=$adult_price_i['adult_max_pax'];
                                            }

                                         }

                                        }
                                        else
                                        {
                                         $min_adult=0;
                                         $max_adult=0; 
                                        }
                                        
                                        @endphp
                                            <select name="activities_select_adults[{{$days_count}}][]" id="activities_select_adults__{{($days_count+1)}}__{{($activity_counter+1)}}" class="form-control select_activities_adults"  @if(!empty($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'])) required="required" 
                                        @endif>
                                                <option value="">Adults</option>
                                                <option value="0" @if(!empty($activities_select_adults[$days_count]) && $activities_select_adults[$days_count][$activity_counter]=='0') selected="selected" @endif>0</option>
                                                @for($i=$min_adult;$i<=$max_adult;$i++)
                                                <option value="{{$i}}" @if(!empty($activities_select_adults[$days_count]) && $activities_select_adults[$days_count][$activity_counter]==$i) selected="selected" @endif>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                             @php
                                          
                                             if(!empty($activity_pax_count_array[$days_count][$activity_counter]['child_price_details']))
                                            {
                                            $min_child=$activity_pax_count_array[$days_count][$activity_counter]['child_price_details'][0]['child_min_pax'];
                                            $max_child=$activity_pax_count_array[$days_count][$activity_counter]['child_price_details'][0]['child_max_pax'];
                                             foreach($activity_pax_count_array[$days_count][$activity_counter]['child_price_details'] as $child_price_i)
                                             {  
                                                if($child_price_i['child_min_pax']<$min_child)
                                                {
                                                    $min_child=$child_price_i['child_min_pax'];
                                                }
                                                if($child_price_i['child_max_pax']>$max_child)
                                                {
                                                    $max_child=$child_price_i['child_max_pax'];
                                                }

                                             }
                                         }
                                         else
                                         {
                                             $min_child=0;
                                            $max_child=0;

                                         }
                                            @endphp
                                            <select name="activities_select_child[{{$days_count}}][]" id="activities_select_child__{{($days_count+1)}}__{{($activity_counter+1)}}" class="form-control select_activities_child" @if(!empty($activity_pax_count_array[$days_count][$activity_counter]['child_price_details'])) required="required" 
                                        @endif>

                                                <option value="">Child</option>
                                                @if($min_child>0)
                                                <option value="0" @if(!empty($activities_select_child[$days_count]) && $activities_select_child[$days_count][$activity_counter]=="0") selected="selected" @endif>0</option>
                                                @endif
                                                @for($i=$min_child;$i<=$max_child;$i++)
                                                <option value="{{$i}}" @if(!empty($activities_select_child[$days_count]) && $activities_select_child[$days_count][$activity_counter]==$i) selected="selected" @endif>{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                            <div class="col-md-12">
                                                <br>    
                                                <div class="row activities_child_age_div" id="activities_child_age_div__{{($days_count+1)}}__{{($activity_counter+1)}}">
                                                      @for($child_age=1;$child_age<=$activities_select_child[$days_count][$activity_counter];$child_age++)
                                                    <div class="col-md-6"> <div class="form-group"><input type="number" min=0 id="activities_select_child__{{($days_count+1)}}__{{($activity_counter+1)}}__{{$child_age}}" name="activities_select_child_age[{{($days_count)}}][{{($activity_counter)}}][]" class="form-control activities_select_child_age" value="{{$activities_select_child_age[$days_count][$activity_counter][($child_age-1)]}}" placeholder="Child Age {{$child_age}}" required></div></div>
                                                    @endfor

                                                
                                            </div>
                                            </div>
                                            

                                 </div>

                                 @else 

                                 <div class="row activities_pax_div activities_pax_div__{{($days_count+1)}}" id="activity_pax_div__{{($days_count+1)}}__{{($activity_counter+1)}}" @if(!empty($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'])) style="margin-top: 10px;" @else  style="margin-top: 10px;display:none" @endif>
                                         <div class="col-md-6">
                                          @php

                                        
                                        if(!empty($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details']))
                                        {
                                            $min_adult=$activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'][0]['adult_min_pax'];
                                        $max_adult=$activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'][0]['adult_max_pax'];
                                         foreach($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'] as $adult_price_i)
                                         {
                                            if($adult_price_i['adult_min_pax']<$min_adult)
                                            {
                                                $min_adult=$adult_price_i['adult_min_pax'];
                                            }
                                            if($adult_price_i['adult_max_pax']>$max_adult)
                                            {
                                                $max_adult=$adult_price_i['adult_max_pax'];
                                            }

                                         }

                                        }
                                        else
                                        {
                                         $min_adult=0;
                                         $max_adult=0; 
                                        }
                                        
                                        @endphp
                                            <select name="activities_select_adults[{{$days_count}}][]" id="activities_select_adults__{{($days_count+1)}}__{{($activity_counter+1)}}" class="form-control select_activities_adults"  @if(!empty($activity_pax_count_array[$days_count][$activity_counter]['adult_price_details'])) required="required" 
                                        @endif>
                                                <option value="">Adults</option>
                                                <option value="0">0</option>
                                                @for($i=$min_adult;$i<=$max_adult;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                             @php
                                          
                                             if(!empty($activity_pax_count_array[$days_count][$activity_counter]['child_price_details']))
                                            {
                                            $min_child=$activity_pax_count_array[$days_count][$activity_counter]['child_price_details'][0]['child_min_pax'];
                                            $max_child=$activity_pax_count_array[$days_count][$activity_counter]['child_price_details'][0]['child_max_pax'];
                                             foreach($activity_pax_count_array[$days_count][$activity_counter]['child_price_details'] as $child_price_i)
                                             {  
                                                if($child_price_i['child_min_pax']<$min_child)
                                                {
                                                    $min_child=$child_price_i['child_min_pax'];
                                                }
                                                if($child_price_i['child_max_pax']>$max_child)
                                                {
                                                    $max_child=$child_price_i['child_max_pax'];
                                                }

                                             }
                                         }
                                         else
                                         {
                                             $min_child=0;
                                            $max_child=0;

                                         }
                                            @endphp
                                            <select name="activities_select_child[{{$days_count}}][]" id="activities_select_child__{{($days_count+1)}}__{{($activity_counter+1)}}" class="form-control select_activities_child" @if(!empty($activity_pax_count_array[$days_count][$activity_counter]['child_price_details'])) required="required" 
                                        @endif>

                                                <option value="">Child</option>
                                                @if($min_child>0)
                                                <option value="0">0</option>
                                                @endif
                                                @for($i=$min_child;$i<=$max_child;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                            <div class="col-md-12">
                                                <br>    
                                                <div class="row activities_child_age_div" id="activities_child_age_div__{{($days_count+1)}}__{{($activity_counter+1)}}">
                                                
                                            </div>
                                            </div>
                                            

                                 </div>



                                 @endif
                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_activity" id="change_activity__{{$days_count+1}}_{{($activity_counter+1)}}">Change</a>

                                                                         <a href="javascript:void(0)" class="login-a remove_activity" id="remove_activity__{{$days_count+1}}__{{($activity_counter+1)}}">Remove</a>

                                                                         <a href="{{route('activity-details-view',['activity_id'=>$fetch_activity['activity_id'],'itinerary'=>1])}}" class="login-a show_activity" id="show_activity__{{$days_count+1}}__{{($activity_counter+1)}}" target="_blanks">Details</a>
                                                                        
                                                                    </div>

                                                                </div>


                                                        </div>

                                                    </div>
                                                    @endfor
                                                    
                                                </div>
                                                @else
                                                  @php
                                                     //$activity_pax_count_array[$days_count][0]['exists']=1;
                                                     //$activity_pax_count_array[$days_count][0]['adult_price_details']=array();
                                                     //$activity_pax_count_array[$days_count][0]['child_price_details']=array();
                                                     @endphp
                                                  <div class="t-tab" style="display:none">
                                                    <label>Activities</label>
                                                     <div class="row">
                                                        <div class="col-md-12 activity_select__{{$days_count+1}}" id="activity_select__{{$days_count+1}}__1" style="margin:0 0 25px;display:none">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                                  <img class="cstm-activity-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-activity-name"></p>
                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-activity-address"><i class="fa fa-map-marker"></i></p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                    </div>
                                                                                         <input type="hidden" class="activity_id" id="activity_id__{{($days_count+1)}}__1" name="activity_id[{{$days_count}}][]" value="">
                                                                            <input type="hidden" class="activity_name" name="activity_name[{{$days_count}}][]"  id="activity_name__{{($days_count+1)}}__1" value="">
                                                                                        <input type="hidden" class="calc_cost activity_cost" id="activity_cost__{{($days_count+1)}}__1"  
                                                                                        name="activity_cost[{{$days_count}}][]"
                                                                                        value="0">


                                                                                             
                                                                                </div>
                                                                                   </div>
                                                                                   <div class="row activities_pax_div activities_pax_div__1" id="activity_pax_div__{{($days_count+1)}}__1" style="margin-top: 10px;display:none">
                                         <div class="col-md-6">
                                          @php
                                         $min_adult=0;
                                         $max_adult=0; 
                                        
                                        @endphp
                                            <select name="activities_select_adults[{{$days_count}}][]" id="activities_select_adults__{{($days_count+1)}}__1" class="form-control select_activities_adults">
                                                <option value="">Adults</option>
                                                <option value="0">0</option>
                                                @for($i=$min_adult;$i<=$max_adult;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                             @php
                                            $min_child=0;
                                            $max_child=0;
                                            @endphp
                                            <select name="activities_select_child[{{$days_count}}][]" id="activities_select_child__{{($days_count+1)}}__1" class="form-control select_activities_child">

                                                <option value="">Child</option>
                                                @if($min_child>0)
                                                <option value="0">0</option>
                                                @endif
                                                @for($i=$min_child;$i<=$max_child;$i++)
                                                <option value="{{$i}}">{{$i}}</option>
                                                @endfor
                                            </select>
                                        </div>
                                         
                                            <div class="col-md-12">
                                                 <br>
                                                <div class="row activities_child_age_div" id="activities_child_age_div__{{($days_count+1)}}__1">
                                                
                                            </div>
                                            </div>
                                            

                                 </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_activity" id="change_activity__{{$days_count+1}}_1">Change</a>

                                                                         <a href="javascript:void(0)" class="login-a remove_activity" id="remove_activity__{{$days_count+1}}__1">Remove</a>

                                                                         <a href="" class="login-a show_activity" id="show_activity__{{$days_count+1}}__1" target="_blanks">Details</a>
                                                                        
                                                                    </div>

                                                                </div>


                                                        </div>
                                                    </div>
                                                </div>


                                                @endif
                                                 <div class="text-center"><button type="button" class="btn btn-md btn-primary add_activity" id="add_activity__{{($days_count+1)}}">+ Add Activity</button></div>
                                            @if(!empty($itinerary_package_services[$days_count]['restaurant']))
                                            <div class="t-tab">
                                                    <label>Restaurants</label>
                                                     <div class="row">
                                                        @php
                                                        $restaurant_counter=0;
                                                        @endphp
                                                        @foreach($itinerary_package_services[$days_count]['restaurant'] as $restaurant_key => $restaurant_value)
                                                       <?php
                                                        $get_restaurant=App\Restaurants::where('restaurant_id',$restaurant_value['restaurant_id'])->first();
                                                        $restaurant_warning=0;
                                                        if(!empty($get_restaurant))
                                                        {
                                                            if($get_restaurant->validity_fromdate>$itinerary_date_from || $get_restaurant->validity_todate<$itinerary_date_from )
                                                            $restaurant_warning++;
                                                        }

                                                        $restaurant_images=unserialize($get_restaurant->restaurant_images);
                                                        ?>
                                                        <div class="col-md-12 restaurant_select__{{$days_count+1}}" id="restaurant_select__{{$days_count+1}}__{{($restaurant_counter+1)}}" style="margin:0 0 25px;">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                                     @if(!empty($restaurant_images[0]))
                                                                                 <img class="cstm-restaurant-image" src="{{ asset('assets/uploads/restaurant_images')}}/{{$restaurant_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                   <img class="cstm-restaurant-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-restaurant-name">{{$restaurant_value['restaurant_name']}}</p>
                                                                            </div>

                                                                            <p class="info"></p>
                                                                              <p class="address cstm-restaurant-address"><i class="fa fa-map-marker">{{$get_restaurant['restaurant_address']}}</i></p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div class="row">
                                                                                          <div class="col-md-12">
                                                                                        <span class="span-i">SELECTED FOOD / DRINK :</span>
                                                                                        @php 
                                                                                        $selected_food=array();
                                                                                        @endphp
                                                                                         @foreach($itinerary_package_services[$days_count]['restaurant'][ $restaurant_key]['food_details'] as $food_detail_key => $food_detail_value)
                                                                                         <?php
                                                                                         $selected_food[]=$food_detail_value['food_name']." (".$food_detail_value['food_qty']." X ".$food_detail_value['food_unit'].")";
                                                                                         ?>
                                                                                         @endforeach
                                                                                         <p class="title-2 cstm-restaurant-food-type">{{implode(" , ",$selected_food)}}</p>
                                                                                     </div>
                                                                                     <div class="col-md-6">
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                                  <div class="col-md-6">
                                                                                     <span class="span-i">FOOD / DRINK FOR :</span>
                                                                                    <p class="title-2 cstm-restaurant-food-purpose">@if(isset($restaurant_value['restaurant_food_for']))
                                                                                    For {{ucfirst($restaurant_value['restaurant_food_for']) ?? 'N/A'}}
                                                                                    @else 
                                                                                    For N/A
                                                                                    @endif </p>
                                                                                </div>
                                                                                <?php
                                                                                    if($restaurant_warning>0)
                                                                                    {
                                                                                        $warning_count++;
                                                                                        echo " <div class='col-md-12'><p style='color:red' id='restaurant_warning__".($days_count+1)."__".($restaurant_counter+1)."'>Please click edit/change in order to select another restaurant, as current one is not available for your dates</p></div>";
                                                                                   }
                                                                        
                                                                                    ?>
                                                                                    </div>
                                                                                    
                                                                                         <input type="hidden" class="restaurant_id" id="restaurant_id__{{($days_count+1)}}__{{($restaurant_counter+1)}}" name="restaurant_id[{{$days_count}}][]" value="{{$restaurant_value['restaurant_id']}}">
                                                                            <input type="hidden" class="restaurant_name" name="restaurant_name[{{$days_count}}][]"  id="restaurant_name__{{($days_count+1)}}__{{($restaurant_counter+1)}}" value="{{$restaurant_value['restaurant_name']}}">
                                                                                        <input type="hidden" class="calc_cost restaurant_cost" id="restaurant_cost__{{($days_count+1)}}__{{($restaurant_counter+1)}}"
                                                                                        name="restaurant_cost[{{$days_count}}][]"
                                                                                        value="{{$restaurant_value['restaurant_cost']}}">
                                                                                        <input type="hidden" class="restaurant_food_for" name="restaurant_food_for[{{$days_count}}][]"  id="restaurant_food_for__{{($days_count+1)}}__{{($restaurant_counter+1)}}" value="@isset($restaurant_value['restaurant_food_for']){{$restaurant_value['restaurant_food_for']}}@endisset">
                                                                                                       
                                                                                </div>
                                                                                   </div>
                                                                        </div>
                                                                          <div class="restaurant_food_detail" id="restaurant_food_detail__{{($days_count+1)}}__{{($restaurant_counter+1)}}">
                                                                              @foreach($itinerary_package_services[$days_count]['restaurant'][ $restaurant_key]['food_details'] as $food_detail_key => $food_detail_value)
                                                                               <input type="hidden" class="restaurant_food_name" name="restaurant_food_name[{{$days_count}}][{{$restaurant_counter}}][]" value="{{$food_detail_value['food_name']}}">
                                                                               <input type="hidden" class="restaurant_food_qty" name="restaurant_food_qty[{{$days_count}}][{{$restaurant_counter}}][]" value="{{$food_detail_value['food_qty']}}">
                                                                               <input type="hidden" class="restaurant_food_price" name="restaurant_food_price[{{$days_count}}][{{$restaurant_counter}}][]" value="{{$food_detail_value['food_price']}}">
                                                                               <input type="hidden" class="restaurant_food_id" name="restaurant_food_id[{{$days_count}}][{{$restaurant_counter}}][]" value="{{$food_detail_value['food_id']}}">
                                                                               <input type="hidden" class="restaurant_food_category_id" name="restaurant_food_category_id[{{$days_count}}][{{$restaurant_counter}}][]" value="{{$food_detail_value['food_category_id']}}">
                                                                               <input type="hidden" class="restaurant_food_unit" name="restaurant_food_unit[{{$days_count}}][{{$restaurant_counter}}][]" value="{{$food_detail_value['food_unit']}}">
                                                                              @endforeach
                                                                          </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_restaurant" id="change_restaurant__{{$days_count+1}}_{{($restaurant_counter+1)}}">Change</a>

                                                                         <a href="javascript:void(0)" class="login-a remove_restaurant" id="remove_restaurant__{{$days_count+1}}__{{($restaurant_counter+1)}}">Remove</a>

                                                                         <a href="" class="login-a show_restaurant" id="show_restaurant__{{$days_count+1}}__{{($restaurant_counter+1)}}" target="_blanks">Details</a>

                                                                         @php
                                                                         $total_whole_itinerary_cost+=$restaurant_value['restaurant_cost'];
                                                                         @endphp
                                                                        
                                                                    </div>

                                                                </div>
                                                        </div>

                                                        <?php
                                                        $restaurant_counter++;
                                                        ?>
                                                        @endforeach
                                                    </div>
                                            </div>

                                            @else
                                             <div class="t-tab" style="display:none">
                                                    <label>Restaurants</label>
                                                     <div class="row">
                                                        <div class="col-md-12 restaurant_select__{{$days_count+1}}" id="restaurant_select__{{$days_count+1}}__1" style="margin:0 0 25px;display:none">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                                  <img class="cstm-restaurant-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-restaurant-name"></p>
                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-restaurant-address"><i class="fa fa-map-marker"></i></p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div class="row">
                                                                                         <div class="col-md-12">
                                                                                        <span class="span-i">SELECTED FOOD / DRINK :</span>
                                                                                         <p class="title-2 cstm-restaurant-food-type"></p>
                                                                                     </div>
                                                                                     <div class="col-md-6">
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                                    <div class="col-md-6">
                                                                                    <span class="span-i">FOOD / DRINK FOR :</span>
                                                                                     <p class="title-2 cstm-restaurant-food-purpose"></p>
                                                                                 </div>
                                                                                    </div>
                                                                                         <input type="hidden" class="restaurant_id" id="restaurant_id__{{($days_count+1)}}__1" name="restaurant_id[{{$days_count}}][]" value="">
                                                                            <input type="hidden" class="restaurant_name" name="restaurant_name[{{$days_count}}][]"  id="restaurant_name__{{($days_count+1)}}__1" value="">
                                                                                        <input type="hidden" class="calc_cost restaurant_cost" id="restaurant_cost__{{($days_count+1)}}__1"  
                                                                                        name="restaurant_cost[{{$days_count}}][]"
                                                                                        value="0">
                                                                                         <input type="hidden" class="restaurant_food_for" name="restaurant_food_for[{{$days_count}}][]"  id="restaurant_food_for__{{($days_count+1)}}__1" value="">
                                                                                                       
                                                                                </div>
                                                                                   </div>
                                                                        </div>
                                                                          <div class="restaurant_food_detail" id="restaurant_food_detail__{{($days_count+1)}}__1"></div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_restaurant" id="change_restaurant__{{$days_count+1}}_1">Change</a>

                                                                         <a href="javascript:void(0)" class="login-a remove_restaurant" id="remove_restaurant__{{$days_count+1}}__1">Remove</a>

                                                                         <a href="" class="login-a show_restaurant" id="show_restaurant__{{$days_count+1}}__1" target="_blanks">Details</a>
                                                                        
                                                                    </div>

                                                                </div>


                                                        </div>
                                                    </div>
                                            </div>
                                            @endif
                                                 <div class="text-center"><button type="button" class="btn btn-md btn-primary add_restaurant" id="add_restaurant__{{($days_count+1)}}">+ Add Restaurant</button></div>
                                             </div>
                                            @endfor

                                             
                                             

                                            
                                           <!--  <div class="day-count">
                                                <p class="days">Day 2 - Arrival in Phuket</p>
                                                <p class="des">Suited for Couples and Family|A complimentary island tour
                                                    on day 2|
                                                    No “visa on arrival” fee till 30th Oct|Relaxed Itinerary with days
                                                    of Leisure
                                                </p>
                                                <div class="t-tab">
                                                    <div class="row">
                                                        <div class="col-md-12" style="margin:0 0 25px">
                                                            <a href="hotel-detail.php" style="display: block;">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">

                                                                            <img src="https://r1imghtlak.mmtcdn.com/fa023a780b5f11e9a0ec0242ac110002.jpg?&downsize=*:675&crop=900:675;56,0&output-format=jpg"
                                                                                style="width:100%">
                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="ens">
                                                                                <span class="hotel-s">4.0/5</span>
                                                                                <div class="rating"
                                                                                    style="display:inline-block;">
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    <span class="fa fa-star"></span>
                                                                                    <span class="fa fa-star"></span>

                                                                                </div>
                                                                            </div>

                                                                            <div class="heading-div">
                                                                                <p class="hotel-name">Quality Inn Ocean
                                                                                    Palms
                                                                                    Goa</p>

                                                                            </div>


                                                                            <p class="info">Calangute</p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <span class="span-i">DATES</span>
                                                                                    <p class="title-2">Mon, 16 Mar 2020
                                                                                        -
                                                                                        Fri,
                                                                                        20 Mar 2020</p>
                                                                                </div>
                                                                                <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div>

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>
                                                                            <p class="title-2">Superior Room </p>

                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">

                                                                        <a href="#" class="login-a">
                                                                            Change</a>

                                                                    </div>

                                                                </div>

                                                            </a>

                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="t-tab">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <img class="t-img"
                                                                src="https://imgak.mmtcdn.com/holidays/images/dynamicDetails/private_transfer.png">
                                                        </div>
                                                        <div class="col-md-5">
                                                            <p class="title-1">Airport to hotel in Phuket</p>
                                                            <p class="title-2"><b>Private Transfer</b></p>
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <span class="span-i">DURATION</span>
                                                                    <p class="p-i">2 hrs</p>
                                                                </div>
                                                                <div class="col-md-6">
                                                                    <span class="span-i">INCLUDES</span>
                                                                    <p class="p-i">Include Private Transfer</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="change">
                                                                <a href="#" class="remove">Remove</a>
                                                                <span class="hr"></span>
                                                                <a href="#" class="chng">Change</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="t-tab">
                                                    <div class="row">
                                                        <div class="col-md-12" style="margin:0 0 25px">
                                                            <a href="hotel-detail.php" style="display: block;">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">

                                                                            <img src="https://r1imghtlak.mmtcdn.com/fa023a780b5f11e9a0ec0242ac110002.jpg?&downsize=*:675&crop=900:675;56,0&output-format=jpg"
                                                                                style="width:100%">
                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="ens">
                                                                                <span class="hotel-s">4.0/5</span>
                                                                                <div class="rating"
                                                                                    style="display:inline-block;">
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    <span class="fa fa-star"></span>
                                                                                    <span class="fa fa-star"></span>

                                                                                </div>
                                                                            </div>

                                                                            <div class="heading-div">
                                                                                <p class="hotel-name">Quality Inn Ocean
                                                                                    Palms
                                                                                    Goa</p>

                                                                            </div>


                                                                            <p class="info">Calangute</p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <span class="span-i">DATES</span>
                                                                                    <p class="title-2">Mon, 16 Mar 2020
                                                                                        -
                                                                                        Fri,
                                                                                        20 Mar 2020</p>
                                                                                </div>
                                                                                <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div>

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>
                                                                            <p class="title-2">Superior Room </p>

                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">

                                                                        <a href="#" class="login-a">
                                                                            Change</a>

                                                                    </div>

                                                                </div>

                                                            </a>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div> -->

                                        </div>
                                        <div class="tab-pane container fade" id="Hotels">
                                            @for($days_count=0;$days_count<$get_itinerary->itinerary_tour_days;$days_count++)
                                             @if(!empty($itinerary_package_services[$days_count]['hotel']['hotel_id']) && $itinerary_package_services[$days_count]['hotel']['hotel_no_of_days']!="0")
                                            <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_hotel_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;">Day {{($days_count+1)}}</button>
                                                
                                            <div class="day-count collapse" id="days_hotel_count{{($days_count+1)}}" >
                                                <p class="days" style="display: none">Day {{($days_count+1)}} - {{$itinerary_package_title[$days_count]}}</p>  
                                                <div class="t-tab">
                                                    <div class="row">
                                                        @php
                                                        $fetch_hotel=ServiceManagement::searchHotel($itinerary_package_services[$days_count]['hotel']['hotel_id']);

                                                        $hotel_images=unserialize($fetch_hotel['hotel_images']);
                                                  @endphp  
                                                        <div class="col-md-12 hotels_indiv_select" id="hotels_indiv_select__{{($days_count+1)}}" style="margin:0 0 25px">
                                                           <a href="#" target="_blank" style="display: block;">
                                                                <div class="hotel-list-div">
                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($hotel_images[0]))
                                                                            <img class="cstm-hotel-indiv-image" src="{{ asset('assets/uploads/hotel_images')}}/{{$hotel_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img class="cstm-hotel-indiv-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="ens">
                                                                                <div class="rating cstm-hotel-indiv-rating"
                                                                                    style="display:inline-block;">
                                                                                    @for($stars=1;$stars<=$fetch_hotel['hotel_rating'];$stars++)
                                                                                    <span
                                                                                        class="fa fa-star checked"></span>
                                                                                    @endfor

                                                                                </div>
                                                                            </div>

                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-hotel-indiv-name">@php echo $fetch_hotel['hotel_name'] @endphp</p>

                                                                            </div>


                                                                            <p class="info cstm-hotel-indiv-address">@php echo $fetch_hotel['hotel_address'] @endphp</p>
                                                                          <div class="heading-div c-h-div">
                                                                            <?php 
                                                                            $checkin_print_date=date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from)));
                                                                            $checkin_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); 

                                                                            $checkout_print_date=date('D, d M Y',strtotime("+".($itinerary_package_services[$days_count]['hotel']['hotel_no_of_days'])." days",strtotime($checkin_date)));

                                                                            $checkout_date=date('Y-m-d',strtotime("+".($itinerary_package_services[$days_count]['hotel']['hotel_no_of_days'])." days",strtotime($checkin_date)));
                                                                                       ?>
                                                                                <div class="rating" style="margin: 10px 0;">
                                                                                    <br> 
                                                                                  
                                                                                    <div>
                                                                                        <span class="span-i">Checkout Date</span>
                                                                                        <p class="title-2">{{$checkout_print_date}}</p>
                                                                                    </div>
                                                                                      <div>
                                                                                        <span class="span-i">Checkin Date</span>
                                                                                        <p class="title-2">{{$checkin_print_date}}</p>
                                                                                    </div>
                                                                                       
                                                                                       
                                                                                   </div>
                                                                                <!--  <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div> -->

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>
                                                                            <p class="title-2 cstm-hotel-indiv-room-type">{{$itinerary_package_services[$days_count]['hotel']['room_name']}}</p>
                                                                            <a href="#" class="login-a change_hotel cstm-change-hotel-indiv" id="change_hotel__{{$fetch_hotel['hotel_id']}}_{{$days_count+1}}">Change Room</a>
                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                        <a href="#" class="login-a change_hotel" id="change_hotel__{{$days_count+1}}">Edit/Change</a>
                                                                         <a href="{{route('hotel-detail',['hotelid'=>base64_encode($fetch_hotel['hotel_id']),'itinerary'=>1])}}" class="login-a show_hotel"  id="show_indiv_hotel__{{$days_count+1}}" target="_blank">Details</a>
                                                                        
                                                                    </div>

                                                                </div>

                                                            </a>

                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            @endfor
                                        </div>
                                        <div class="tab-pane container fade" id="Activity">
                                             @for($days_count=0;$days_count<=$get_itinerary->itinerary_tour_days;$days_count++)
                                               @php
                                                $activity_id_status=0;
                                                 if(isset($itinerary_package_services[$days_count]['activity']['activity_id']))
                                                {
                                                $check_activity_id_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);

                                                if($check_activity_id_count==1)
                                                {
                                                    if($itinerary_package_services[$days_count]['activity']['activity_id'][0]!="")
                                                    {
                                                        $activity_id_status++;
                                                    }
    
                                                }
                                                else
                                                {
                                                    for($check=0;$check< $check_activity_id_count; $check++)
                                                    {
                                                        if($itinerary_package_services[$days_count]['activity']['activity_id'][$check]!="")
                                                        {
                                                             $activity_id_status++;
                                                        }
                                                    }
                                                }
                                            }

@endphp
                                                
                                                @if($activity_id_status>0)
                                             <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_activity_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;">Day {{($days_count+1)}}</button>
                                                
                                            <div class="day-count collapse" id="days_activity_count{{($days_count+1)}}" >
                                                <p class="days" style="display: none">Day {{($days_count+1)}} - {{$itinerary_package_title[$days_count]}}</p>  
                                                
                                                  <div class="t-tab">
                                                    @php
                                                        $activity_count=count($itinerary_package_services[$days_count]['activity']['activity_id']);
                                                    @endphp
                                                    @for($activity_counter=0;$activity_counter < $activity_count;$activity_counter++)
                                                     @if($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]=="")
                                                            @continue
                                                        @endif
                                                    <div class="row">
                                                        @php
                                                        $fetch_activity=ServiceManagement::searchActivity($itinerary_package_services[$days_count]['activity']['activity_id'][$activity_counter]);

                                                        $activity_images=unserialize($fetch_activity['activity_images']);
                                                        @endphp
                                                        <div class="col-md-12 activity_indiv_select activity_indiv_select__{{$days_count+1}}" id="activity_indiv_select__{{$days_count+1}}__{{($activity_counter+1)}}" style="margin:0 0 25px">
                                                            <a href="#" target="_blank" style="display: block;">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($activity_images[0]))
                                                                            <img class="cstm-activity-indiv-image" src="{{ asset('assets/uploads/activities_images')}}/{{$activity_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img class="cstm-activity-indiv-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-activity-indiv-name"> {{$fetch_activity['activity_name']}}</p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-activity-indiv-address"><i class="fa fa-map-marker"></i> {{$fetch_activity['activity_location']}}</p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?>
                                                                                       </p>
                                                                                   </div>
                                                                                </div>
                                                                                   </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_activity" id="change_activity__{{$days_count+1}}_{{($activity_counter+1)}}">Change</a>

                                                                          <a href="javascript:void(0)" class="login-a remove_activity" id="remove_activity__{{$days_count+1}}__{{($activity_counter+1)}}">Remove</a>
                                                                           <a href="{{route('activity-details-view',['activity_id'=>$fetch_activity['activity_id'],'itinerary'=>1])}}" class="login-a show_activity" id="show_indiv_activity__{{$days_count+1}}__{{($activity_counter+1)}}" target="_blanks">Details</a>
                                                                         
                                                                    </div>

                                                                </div>

                                                            </a>

                                                        </div>

                                                    </div>
                                                    @endfor
                                                    
                                                </div>
                                               
                                            </div>
                                             @else
                                              <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_activity_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;">Day {{($days_count+1)}}</button>
                                                
                                            <div class="day-count collapse" id="days_activity_count{{($days_count+1)}}" style="display: none">
                                                <p class="days" style="display: none">Day {{($days_count+1)}} - {{$itinerary_package_title[$days_count]}}</p>  
                                                
                                                  <div class="t-tab">
                                    
                                                    <div class="row">
                                                        <div class="col-md-12 activity_indiv_select activity_indiv_select__{{$days_count+1}}" id="activity_indiv_select__{{$days_count+1}}__1" style="margin:0 0 25px">
                                                            <a href="#" target="_blank" style="display: block;">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            
                                                                                  <img class="cstm-activity-indiv-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                            

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-activity-indiv-name"></p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-activity-indiv-address"><i class="fa fa-map-marker"></i></p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?>
                                                                                       </p>
                                                                                   </div>
                                                                                </div>
                                                                                   </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_activity" id="change_activity__{{$days_count+1}}_1">Change</a>
                                                                          <a href="javascript:void(0)" class="login-a remove_activity" id="remove_activity__{{$days_count+1}}__1">Remove</a>
                                                                           <a href="" class="login-a show_activity" id="show_indiv_activity__{{$days_count+1}}__1" target="_blanks">Details</a>
                                                                         
                                                                    </div>

                                                                </div>

                                                            </a>

                                                        </div>

                                                    </div>
                                                </div>
                                               
                                            </div>


                                             @endif
                                            @endfor
                                        </div>
                                        <div class="tab-pane container fade" id="Site">
                                            @for($days_count=0;$days_count<=$get_itinerary->itinerary_tour_days;$days_count++)
                                             @if(!empty($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']))
                                             <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_sighstseeing_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;">Day {{($days_count+1)}}</button>
                                                
                                            <div class="day-count collapse" id="days_sighstseeing_count{{($days_count+1)}}" >
                                         <p class="days" style="display: none">Day {{($days_count+1)}} - {{$itinerary_package_title[$days_count]}}</p>  
                                                
                                             <div class="t-tab">
                                                    <div class="row">
                                                        @php
                                                        $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($itinerary_package_services[$days_count]['sightseeing']['sightseeing_id']);

                                                        $sightseeing_images=unserialize($fetch_sightseeing['sightseeing_images']);
                                                        @endphp
                                                        <div class="col-md-12 sightseeing_indiv_select" id="sightseeing_indiv_select__{{($days_count+1)}}" style="margin:0 0 25px">
                                                            <a href="#" target="_blank" style="display: block;">
                                                                <div class="hotel-list-div">
                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($sightseeing_images[0]))
                                                                            <img class="cstm-sightseeing-indiv-image" src="{{ asset('assets/uploads/sightseeing_images')}}/{{$sightseeing_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img  class="cstm-sightseeing-indiv-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-sightseeing-indiv-name">@php echo $fetch_sightseeing['sightseeing_tour_name'] @endphp (@php echo $fetch_sightseeing['sightseeing_distance_covered'] @endphp KMS)</p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-sightseeing-indiv-address"><i class="fa fa-map-marker"></i>
                                                                        <?php
                                                                         $get_from_city=ServiceManagement::searchCities($fetch_sightseeing['sightseeing_city_from'],$itinerary_package_countries[$days_count]);
                                                                                       echo $get_from_city['name']."-";

                                                                                        if($fetch_sightseeing['sightseeing_city_between']!=null && $fetch_sightseeing['sightseeing_city_between']!="")
                                                                                        {
                                                                                       $all_between_cities=explode(",",$fetch_sightseeing['sightseeing_city_between']);
                                                                                       for($cities=0;$cities< count($all_between_cities);$cities++)
                                                                                       {
                                                                                        $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$itinerary_package_countries[$days_count]);
                                                                                         echo $fetch_city['name']."-";
                                                                                      }
                                                                                    }
                                                                  
                                                            
                                                                                       $get_from_city=ServiceManagement::searchCities($fetch_sightseeing['sightseeing_city_to'],$itinerary_package_countries[$days_count]);
                                                                                        echo  $get_from_city['name'];

                                                                        ?>
                                                                </p>
                                                                <div class="heading-div c-h-div">
                                                                    <div class="rating">
                                                                        <br>  
                                                                        <div>
                                                                            <span class="span-i">DATE</span>
                                                                            <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?>
                                                                        </p>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                                               <!--   <div>
                                                                                    <span class="span-i">INCLUDES</span>
                                                                                    <p class="title-2">Breakfast</p>
                                                                                </div>

                                                                            </div>
                                                                            <span class="span-i">ROOM TYPE</span>
                                                                            <p class="title-2">Superior Room </p> -->

                                                                        </div>

                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                      
                                                                         <a href="#" class="login-a change_sightseeing" id="change_sightseeing__{{$days_count+1}}">Change</a>

                                                                          <a href="javascript:void(0)" class="login-a remove_sightseeing" id="remove_sightseeing__{{$days_count+1}}">Remove</a>
                                                                           <a href="{{route('sightseeing-details-view',['sightseeing_id'=>base64_encode($fetch_sightseeing['sightseeing_id']),'itinerary'=>1])}}" class="login-a shows_sightseeing" id="shows_indiv_sightseeing__{{$days_count+1}}" target="_blank">Details</a>
                                                                      

                                                                    </div>

                                                                </div>

                                                            </a>

                                                        </div>

                                                    </div>

                                                </div>
                                               
                                             
                                            </div>
                                             @endif
                                            @endfor
                                        </div>
                                        <div class="tab-pane container fade" id="Transfer">
                                            @for($days_count=0;$days_count<=$get_itinerary->itinerary_tour_days;$days_count++)
                                             @if(!empty($itinerary_package_services[$days_count]['transfer']['transfer_id']))
                                              <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_transfer_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;">Day {{($days_count+1)}}</button>
                                                
                                            <div class="day-count collapse" id="days_transfer_count{{($days_count+1)}}" >
                                         <p class="days" style="display: none">Day {{($days_count+1)}} - {{$itinerary_package_title[$days_count]}}</p>  
                                                <div class="t-tab">
                                                    <div class="row">
                                                        @php
                                                        $fetch_transfer=ServiceManagement::searchTransfers($itinerary_package_services[$days_count]['transfer']['transfer_id']);

                                                        $transfer_images=unserialize($fetch_transfer['transfer_vehicle_images']);

                                                        @endphp
                                                        <div class="col-md-12 transfer_select" id="transfer_indiv_select__{{($days_count+1)}}" style="margin:0">
                                                           
                                                                <div class="hotel-list-div">
                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                            @if(!empty($transfer_images[0][0]))
                                                                            <img class="cstm-indiv-transfer-image" src="{{ asset('assets/uploads/vehicle_images')}}/{{$transfer_images[0][0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                  <img  class="cstm-indiv-transfer-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-indiv-transfer-name">@php
                                                                                    $transfer_name="";
                                                                                if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="from-airport") 
                                                                                {
                                                                                    $fetch_from_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_from_airport']);

                                                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                                                                                $transfer_name="From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];

                                                                                    echo "From ".$fetch_from_airport['airport_master_name']." to ".$fetch_to_city['name'];
                                                                                   
                                                                                }
                                                                                else if($itinerary_package_services[$days_count]['transfer']['transfer_type']=="to-airport")
                                                                                {
                                                                                   
                                                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                                                    $fetch_to_airport=ServiceManagement::searchAirports($itinerary_package_services[$days_count]['transfer']['transfer_to_airport']);

                                                                                     $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];

                                                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_airport['airport_master_name'];

                                                                                }
                                                                                else
                                                                                {

                                                                                    $fetch_from_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_from_city'],$fetch_transfer['transfer_country']);

                                                                                    $fetch_to_city=ServiceManagement::searchCities($itinerary_package_services[$days_count]['transfer']['transfer_to_city'],$fetch_transfer['transfer_country']);

                                                                                     $transfer_name="From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];


                                                                                    echo "From ".$fetch_from_city['name']." to ".$fetch_to_city['name'];

                                                                                }
                                                                                @endphp
                                                                               </p>

                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-indiv-transfer-car">@php echo $itinerary_package_services[$days_count]['transfer']['transfer_name']; @endphp </p>
                                                                          <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                     <div>
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                        
                                                                                </div>
                                                                                   </div>
                                                                        </div>

                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                         <a href="#" class="login-a change_transfer" id="change_transfer__{{$days_count+1}}">Change</a>
                                                                   
                                                                         <a href="javascript:void(0)" class="login-a remove_transfer" id="remove_transfer__{{$days_count+1}}">Remove</a>
                                                                       

                                                                    </div>

                                                                </div>


                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                                @endif
                                            @endfor
                                        </div>
                                         <div class="tab-pane container fade" id="Restaurants">
                                            @for($days_count=0;$days_count<=$get_itinerary->itinerary_tour_days;$days_count++)
                                              <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_restaurant_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;">Day {{($days_count+1)}}</button>
                                                
                                            <div class="day-count collapse" id="days_restaurant_count{{($days_count+1)}}" >
                                         <p class="days">Day {{($days_count+1)}} - {{$itinerary_package_title[$days_count]}}</p>  
                                          @if(!empty($itinerary_package_services[$days_count]['restaurant']))
                                            <div class="t-tab">
                                                    <label>Restaurants</label>
                                                     <div class="row">
                                                        @php
                                                        $restaurant_counter=0;
                                                        @endphp
                                                        @foreach($itinerary_package_services[$days_count]['restaurant'] as $restaurant_key => $restaurant_value)
                                                       <?php
                                                        $get_restaurant=App\Restaurants::where('restaurant_id',$restaurant_value['restaurant_id'])->first();
                                                        $restaurant_warning=0;
                                                        if(!empty($get_restaurant))
                                                        {
                                                            if($get_restaurant->validity_fromdate>$itinerary_date_from || $get_restaurant->validity_todate<$itinerary_date_from )
                                                            $restaurant_warning++;
                                                        }

                                                        $restaurant_images=unserialize($get_restaurant->restaurant_images);
                                                        ?>
                                                        <div class="col-md-12 restaurant_indiv_select__{{$days_count+1}}" id="restaurant_indiv_select__{{$days_count+1}}__{{($restaurant_counter+1)}}" style="margin:0 0 25px;">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                                     @if(!empty($restaurant_images[0]))
                                                                                 <img class="cstm-restaurant-indiv-image" src="{{ asset('assets/uploads/restaurant_images')}}/{{$restaurant_images[0]}}"
                                                                                style="width:100%">
                                                                                @else
                                                                                   <img class="cstm-restaurant-indiv-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">
                                                                                @endif

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-restaurant-indiv-name">{{$restaurant_value['restaurant_name']}}</p>
                                                                            </div>

                                                                            <p class="info"></p>
                                                                              <p class="address cstm-restaurant-indiv-address"><i class="fa fa-map-marker">{{$get_restaurant['restaurant_address']}}</i></p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div class="row">
                                                                                           <div class="col-md-12">
                                                                                        <span class="span-i">SELECTED FOOD / DRINK :</span>
                                                                                        @php 
                                                                                        $selected_food=array();
                                                                                        @endphp
                                                                                         @foreach($itinerary_package_services[$days_count]['restaurant'][ $restaurant_key]['food_details'] as $food_detail_key => $food_detail_value)
                                                                                         <?php
                                                                                         $selected_food[]=$food_detail_value['food_name']." (".$food_detail_value['food_qty']." X ".$food_detail_value['food_unit'].")";
                                                                                         ?>
                                                                                         @endforeach
                                                                                         <p class="title-2 cstm-restaurant-indiv-food-type">{{implode(" , ",$selected_food)}}</p>
                                                                                     </div>
                                                                                     <div class="col-md-6">
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                                 <div class="col-md-6">
                                                                                     <span class="span-i">FOOD / DRINK FOR :</span>
                                                                                    <p class="title-2 cstm-restaurant-indiv-food-purpose">@if(isset($restaurant_value['restaurant_food_for']))
                                                                                    For {{ucfirst($restaurant_value['restaurant_food_for']) ?? 'N/A'}}
                                                                                    @else 
                                                                                    For N/A
                                                                                    @endif </p>
                                                                                </div>
                                                                                <?php
                                                                                    if($restaurant_warning>0)
                                                                                    {
                                                                                        $warning_count++;
                                                                                        echo " <div class='col-md-12'><p style='color:red' id='restaurant_warning__".($days_count+1)."__".($restaurant_counter+1)."'>Please click edit/change in order to select another restaurant, as current one is not available for your dates</p></div>";
                                                                                   }
                                                                        
                                                                                    ?>
                                                                                    </div>
                                                                                       
                                                                                                       
                                                                                </div>
                                                                                   </div>
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_restaurant" id="change_restaurant__{{$days_count+1}}_{{($restaurant_counter+1)}}">Change</a>

                                                                         <a href="javascript:void(0)" class="login-a remove_restaurant" id="remove_restaurant__{{$days_count+1}}__{{($restaurant_counter+1)}}">Remove</a>

                                                                         <a href="" class="login-a show_restaurant" id="show_restaurant__{{$days_count+1}}__{{($restaurant_counter+1)}}" target="_blanks">Details</a>

                                                                
                                                                        
                                                                    </div>

                                                                </div>
                                                        </div>

                                                        <?php
                                                        $restaurant_counter++;
                                                        ?>
                                                        @endforeach
                                                    </div>
                                            </div>

                                            @else
                                                <div class="t-tab"  style="display: none">
                                                    <div class="row">
                                                        <div class="col-md-12 restaurant_indiv_select__{{$days_count+1}}" id="restaurant_indiv_select__{{$days_count+1}}__1" style="margin:0 0 25px;display:none">
                                                                <div class="hotel-list-div">

                                                                    <div class="hotel-div">
                                                                        <div class="hotel-img-div">
                                                                                  <img class="cstm-restaurant-indiv-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                style="width:100%">

                                                                        </div>
                                                                        <div class="hotel-details">
                                                                            <div class="heading-div">
                                                                                <p class="hotel-name cstm-restaurant-indiv-name"></p>
                                                                            </div>


                                                                            <p class="info"></p>
                                                                              <p class="address cstm-restaurant-indiv-address"><i class="fa fa-map-marker"></i></p>
                                                                            <div class="heading-div c-h-div">
                                                                                <div class="rating">
                                                                                    <br>  
                                                                                    <div class="row">
                                                                                        <div class="col-md-12">
                                                                                          <span class="span-i">SELECTED FOOD / DRINK :</span>
                                                                                         <p class="title-2 cstm-restaurant-indiv-food-type"></p>
                                                                                     </div>
                                                                                     <div class="col-md-6">
                                                                                    <span class="span-i">DATE</span>
                                                                                    <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                </div>
                                                                                 <div class="col-md-6">
                                                                                     <span class="span-i">FOOD / DRINK FOR :</span>
                                                                                     <p class="title-2 cstm-restaurant-indiv-food-purpose"></p>
                                                                                    </div>
                                                                                </div>
            
                                                                                </div>
                                                                                   </div>
                                                                                
                                                                        </div>
                                                                    </div>
                                                                    <div class="hotel-info-div">
                                                                       
                                                                         <a href="#" class="login-a change_restaurant" id="change_restaurant__{{$days_count+1}}_1">Change</a>

                                                                         <a href="javascript:void(0)" class="login-a remove_restaurant" id="remove_restaurant__{{$days_count+1}}__1">Remove</a>

                                                                         <a href="" class="login-a show_restaurant" id="show_restaurant__{{$days_count+1}}__1" target="_blanks">Details</a>
                                                                        
                                                                    </div>

                                                                </div>


                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                               
                                            @endfor
                                        </div>
                                    </div>
                                </div>
                                <hr class="h-hr">
                                <div class="col-md-12">
                                    <h2 style="color: #FF5722;
                                    font-weight: 700;
                                    font-size: 21px;
                                    margin-bottom: 20px;
                                    border-bottom: 1px solid gainsboro;
                                    padding-bottom: 5px;">Our Policies</h2>
                                    <div class="minHeight100vh">
                                        <div class="appendTop20">
                                            <div class=" relative  latoBold font16 lineHeight18 appendBottom20 active">
                                                Exclusions</div>
                                                <div class="parent-div-acc">
                                                    <div class="accordContent paddingB20 lineHeight18">
                                                        <?php echo $get_itinerary->itinerary_exclusions; ?>
                                                       </div>
                                                </div>
                                           
                                            <div class=" relative  latoBold font16 lineHeight18 appendBottom20 active">
                                                Terms and Conditions</div>
                                                <div class="parent-div-acc">
                                            <div class="accordContent paddingB20 lineHeight18">
                                                  <?php echo $get_itinerary->itinerary_terms_and_conditions; ?>
                                            </div>
                                                </div>
                                            <div class=" relative  latoBold font16 lineHeight18 appendBottom20 active">
                                                Cancellation Policy</div>
                                                <div class="parent-div-acc">
                                            <div class="accordContent paddingB20 lineHeight18">
                                                       <?php echo $get_itinerary->itinerary_cancellation; ?>
                                            </div>
                                                </div>
                                        </div>
                                    </div>
                                     @if(!isset($saved))
                                    <div class="row">
            <div class="col-md-3">
                <button type="button" class="btn btn-primary" id="save_itinerary">Save this Package</button>
            </div>
        </div>
        @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="sticky-top sticky-sidebar">
                            <div class="packagePriceContainer">
                                <div class="priceContainer">
                                    <div class="flexOne">
                                       <!--  <p class="oldPrice">₹&nbsp;55,087</p> -->
                                       @php
                                       $markup_cost=round(($total_whole_itinerary_cost*$markup)/100);
                                        $total_agent_cost=round(($total_whole_itinerary_cost+$markup_cost));

                                         $own_itinerary_markup_cost=round(($total_agent_cost*$own_markup)/100);
                                        $total_cost=round(($total_agent_cost+$own_itinerary_markup_cost));

                                       @endphp
                                        <p class="c-price">GEL <span id="total_cost_text">{{$total_cost}}</span><span class="font12"></span></p>
                                        <input type="hidden" name="total_cost" id="total_cost" value="{{$total_cost}}">
                                        <input type="hidden" name="markup_per" id="markup_per"  value="{{$markup}}">
                                         <input type="hidden" name="own_markup_per" id="own_markup_per"  value="{{$own_markup}}">
                                         <input type="hidden" name="total_cost_w_agent_markup" id="total_cost_w_agent_markup" value="{{$total_agent_cost}}">
                                         <input type="hidden" name="total_cost_w_markup" id="total_cost_w_markup" value="{{$total_whole_itinerary_cost}}">
                                        <p class="font10 appendTop5">(Taxes are included in this price)</p>
                                    </div>
                                   <!--  <div class="wdth70"><span class="orangeGrad">27% OFF</span></div> -->
                                     @if(!isset($saved))
                               <!--  <div class="row rooms_div" id="rooms_div__1" style="margin-top: 10px;">
                                    <div class="col-md-3">
                                        <span class="rooms_text">Room 1</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="1">
                                    </div>
                                     <div class="col-md-4">
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3" >3</option>
                                            <option value="4" >4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="select_child[]" class="form-control select_child" required="required">
                                            <option value="">Child</option>
                                            <option value="0">0</option>
                                           <option value="1">1</option>
                                           <option value="2">2</option>
                                           <option value="3">3</option>
                                        </select>
                                    </div>
                                    <div class="add_more_div">
                                       
                                    </div>
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row child_age_div" style="display: none">

                                            
                                        </div>
                                        </div>
                                        

                                 </div>
 -->
                                 <div class="row rooms_div" id="rooms_div__1" style="margin-top: 10px;">
                                   <!--  <div class="col-md-3">
                                        <span class="rooms_text">Room 1</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="1">
                                    </div> -->
                                     <div class="col-md-4"><label for="select_adults">Adults</label>
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                            @for($adults=1;$adults<=50;$adults++)
                                            <option value="{{$adults}}" @if($adults==1) selected="selected" @endif>{{$adults}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="select_child">Child</label>
                                        <select name="select_child[]" class="form-control select_child" required="required">
                                            <option value="">Child</option>
                                            @for($child=0;$child<=50;$child++)
                                            <option value="{{$child}}">{{$child}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="add_more_div">
                                       
                                    </div>
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row child_age_div" style="display: none">

                                            
                                        </div>
                                        </div>
                                        

                                 </div>
                                 @else
                                 @php
                                 $select_adults=unserialize($fetch_saved_itinerary->select_adults);
                                 $select_child=unserialize($fetch_saved_itinerary->select_child);
                                  $select_child_age=unserialize($fetch_saved_itinerary->select_child_age);
                                  @endphp

                                  @if(empty($select_adults))
                                  <div class="row rooms_div" id="rooms_div__1" style="margin-top: 10px;">
                                    <!-- <div class="col-md-3">
                                        <span class="rooms_text">Room 1</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="1">
                                    </div> -->
                                     <div class="col-md-4"><label for="select_adults">Adults</label>
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                             @for($adults=1;$adults<=50;$adults++)
                                            <option value="{{$adults}}" @if($adults==1) selected="selected" @endif>{{$adults}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="select_child">Child</label>
                                        <select name="select_child[]" class="form-control select_child" required="required">
                                            <option value="">Child</option>
                                            @for($child=0;$child<=50;$child++)
                                            <option value="{{$child}}">{{$child}}</option>
                                            @endfor
                                        </select>                                    </div>
                                    <div class="add_more_div">
                                       
                                    </div>
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row child_age_div" style="display: none">

                                            
                                        </div>
                                        </div>
                                        

                                 </div>
                                 @else
                                 @php
                                 $child_count_age=0;
                                 @endphp
                                 @for($i=0;$i<1;$i++)
                                  <div class="row rooms_div" id="rooms_div__{{($i+1)}}" style="margin-top: 10px;">
                                   <!--  <div class="col-md-3">
                                        <span class="rooms_text">Room {{($i+1)}}</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="{{($i+1)}}">
                                    </div> -->
                                     <div class="col-md-4"><label for="select_adults">Adults</label>
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                            @for($j=1;$j<=50;$j++)
                                            <option value="{{$j}}" @if($select_adults[$i]==$j) selected="selected" @endif>{{$j}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                          <label for="select_child">Child</label>
                                        <select name="select_child[]" class="form-control select_child" required="required">
                                            <option value="">Child</option>
                                            @for($c=0;$c<=50;$c++)
                                            <option value="{{$c}}"  @if($select_child[$i]==$c) selected="selected" @endif>{{$c}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="add_more_div">
                                        @if($i>0)

                                        <label class="remove_more_rooms" id="remove_more_rooms__{{($i+1)}}">x</label>
                                        @endif
                                       
                                    </div>
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row child_age_div" @if($select_child[$i]<=0) style="display: none" @endif>
                                                @for($age=1;$age<=$select_child[$i];$age++)
                                                <div class="col-md-6"> <div class="form-group"><label for="child_age{{$age}}" style="color:black">Child Age {{$age}}</label><input type="number" min="0" id="child_age{{$age}}" name="child_age[]" class="form-control child_age" style="color:black" value="{{$select_child_age[($child_count_age+$age)-1]}}" required=""></div></div>

                                                @php
                                                $child_count_age++;
                                                @endphp
                                                @endfor
                                            
                                        </div>
                                        </div>
                                 </div>



                                 @endfor
                                 @endif
                                 @endif
                               <!--   <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6 text-right">
                                         <label id="add_more_rooms"><i class="fa fa-plus plus-icon"></i> Add room</label>
                                    </div>
                                 </div> -->
                                </div>
                                  
                           
                                <!-- <div class="viewOffers"><a href="javascript:void(0);">View All offers applied</a></div> -->
                                <div class="btnContainer"><button type="submit" class="primaryBtn"
                                        id="bookMyPackage">Book</button></div>
                                    <button style="display:none" type="button" id="calculate_cost"></button>
                                    <input type="hidden" id="warning_count" value="{{$warning_count}}">
                                <div class="hide"><a href="javascript:void(0);" id="create_quote_id">Create Quote</a>
                                </div>
                                <div class="hide"><a href="//holidayz.makemytrip.com/holidays/generateCrm"
                                        target="_blank">Crm Pax Association</a></div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>
         
        </div>

       
    </div>
                      

    </div>

    </div>

    </div>

</div>
    @include('agent.includes.footer')
    @include('agent.includes.bottom-footer')
    @include('agent.itinerary-details-scripts')      

</body>
    </html>