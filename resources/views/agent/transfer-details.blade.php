<?php
use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\LoginController;
?>
@include('agent.includes.top-header')



<style>
    img.transfer-img {
        width: 20%;
        height: 180px;
        object-fit: cover;
        object-position: center;
        margin: 10px;
        border-radius: 5px;
    }
    div#myModal .modal-dialog {
    max-width: max-content;
    margin: 1.75rem auto;
}
    img.transfer-img:nth-child(1){
        width: 50%;
    }
    img.transfer-img:nth-child(2){
        width: 44%;
    }
    img.transfer-img:nth-child(3){
        width: 50%;
    }
    img.transfer-img:nth-child(4){
        width: 44%;
    }
    img.transfer-img:nth-child(5){
        width: 50%;
    }
    img.transfer-img:nth-child(6){
        width: 44%;
    }

    .img-div-tranfer {
        display: flex;
        flex-wrap: wrap;
        align-items: flex-start;
    }
    a.panel-title {
        padding-top: 0 !important;
    }
   

button.close.light-btn {
    position: absolute;
    width: 25px;
    height: 25px;
    right: -11px;
    top: -10px;
    background: black;
    opacity: 1;
    z-index: 999;
    text-shadow: none;
    color: white;
    border-radius: 50px;
}
img#img-lightbox {
    width: auto;
    height: auto;
    max-width: initial;
    max-height: 600px;
}
div#myModal .modal-body {
    padding: 10px;
}
.bg-dark {
    
    height: 130px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 65px;
    position: relative;
}
label.tile-label {
    position: absolute !important;
    top: 8px !important;
    left: 20px;
    border-color: white;
    z-index: 9999;
    color: red !important;
}
.box .overlay {
    z-index: 50;
    background: rgba(103, 58, 183, 0.39);
    border-radius: 5px;
}
   span.info-s {
        color: white;
        background: #909090;
        font-size: 12px;
        padding: 2px 2px;
        width: 16px;
        font-weight: 800;
        height: 16px;
        border-radius: 50%;
        position: absolute;
        box-sizing: border-box;
        left: 80%;
        display: inline-block;
        cursor: auto;
    }
      .table-sightseeing-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
  }
  p.font-size-26.text-center.color-tiles-text a {
    color: #673AB7 !IMPORTANT;
}
p.font-size-26.text-center.color-tiles-text {
    background: #e1e1e1;
    position: absolute;
    width: 100%;
    top: 100%;
    left: 0;
    padding: 5px 10px;
    height: auto;
    display: flex;
    justify-content: center;
    color: #0088ff !important;
    border-radius: 5px;
    align-items: center;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
 a.details i {
        font-size: 19px;
        margin-right: 7px;
        color: orange;
    }

    p.span-f i {
        margin-right: 5px;
    }

    a.details {
        color: #3F51B5;
        font-size: 16px;
    }
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('agent.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('agent.includes.nav')

                <div class="content">



                    <div class="content-header">

                        <div class="d-flex align-items-center">

                            <div class="mr-auto">

                                <h3 class="page-title">Transfer Details</h3>

                                <div class="d-inline-block align-items-center">

                                    <nav>

                                        <ol class="breadcrumb">

                                            <li class="breadcrumb-item"><a href="#"><i
                                                class="mdi mdi-home-outline"></i></a></li>

                                                <li class="breadcrumb-item" aria-current="page">Home

                                                    <li class="breadcrumb-item active" aria-current="page">Transfer Details

                                                    </li>

                                                </ol>

                                            </nav>

                                        </div>

                                    </div>

                                </div>

                            </div>
                            @php
                            if(isset($get_transfer->vehicleType))
                            {
                              $vehicle_type=$get_transfer->vehicleType->vehicle_type_name;  
                            }
                            else
                            {
                                 $vehicle_type="NA";
                            }

                            if(isset($get_transfer->vehicle))
                            {
                                $vehicle_name=$get_transfer->vehicle->vehicle_name;
                            }
                            else
                            {
                                 $vehicle_name="NA";
                            }

                          
                            $vehicle_images=unserialize($get_transfer->transfer_vehicle_images);
                            @endphp
                            <div class="row">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row mb-20" style="margin-bottom:60px !important">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-12" style="padding:0;overflow:hidden">
                                                        <div class="slider-box">
                                                            <div class="heading-name">
                                                                @if($vehicle_name!="NA")
                                                                <p class="country_name ng-binding">
                                                                    {{strtoupper($vehicle_name)}}
                                                                </p>
                                                                @endif
                                                            </div>
                                                            <div class="img-div-tranfer">
                                                                @foreach($vehicle_images[0] as $images)
                                                                <img src="{{ asset('assets/uploads/vehicle_images')}}/{{$images}}" class="transfer-img">
                                                                @endforeach

                                                            </div>
                                                        </div>

                                                        <p class="hotel-type">{{$vehicle_type}}</p>
                                                        <p class="para-a"><b>VEHICLE NOTE :  </b>{{$get_transfer->transfer_vehicle_note}}
                                                        </p>
                                                    </div>
                                                    <div class="col-md-12" style="margin-top:30px;">

                                                        <div class="box">

                                                            <div class="box-body accor-div">
                                                                <!-- Nav tabs -->
                                                                <ul class="nav nav-tabs customtab" role="tablist">
                                                                <!--   <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Itenary"
                                                                        role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span
                                                                            class="hidden-xs-down">Itenary</span></a> </li>
                                                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Blackout" role="tab"><span
                                                                            class="hidden-sm-up"><i class="ion-person"></i></span> <span
                                                                            class="hidden-xs-down">Blackout</span></a> </li> -->
                                                                            @if($get_transfer->transfer_inclusions!="")
                                                                            <li class="nav-item"> <a class="nav-link active"
                                                                                data-toggle="tab" href="#Inclusion" role="tab"><span
                                                                                class="hidden-sm-up"><i
                                                                                class="ion-email"></i></span> <span
                                                                                class="hidden-xs-down">Inclusions</span></a> </li>
                                                                                @endif

                                                                                 @if($get_transfer->transfer_exclusions!="")

                                                                                <li class="nav-item"> <a class="nav-link " data-toggle="tab"
                                                                                    href="#Exclusion" role="tab"><span
                                                                                    class="hidden-sm-up"><i class="ion-home"></i></span>
                                                                                    <span class="hidden-xs-down">Exclusions</span></a> </li>
                                                                                     @endif

                                                                                      @if($get_transfer->transfer_cancellation!="")

                                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab"
                                                                                        href="#Cancellation" role="tab"><span
                                                                                        class="hidden-sm-up"><i
                                                                                        class="ion-person"></i></span>
                                                                                        <span class="hidden-xs-down">Cancellation</span></a>
                                                                                    </li>
                                                                                    @endif
                                                                                    

                                                                                    @if($get_transfer->transfer_terms_and_conditions!="")
                                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab"
                                                                                        href="#tc" role="tab"><span class="hidden-sm-up"><i
                                                                                            class="ion-email"></i></span>
                                                                                            <span class="hidden-xs-down">T
                                                                                            & C</span></a> </li>
                                                                                            @endif
                                                                                        </ul>
                                                                                        <!-- Tab panes -->
                                                                                        <div class="tab-content accord-content">

                                                                                            <div class="tab-pane" id="Blackout" role="tabpanel">
                                                                                                <div class="row">
                                                                                                    <div class="col-md-12">
                                                                                                        <div class="tab-content" style="margin-top:0">
                                                                                                            <div id="navpills-1" class="tab-pane active">
                                                                                                                <!-- Categroy 1 -->
                                                                                                                <div class=" tab-pane animation-fade active"
                                                                                                                id="category-1" role="tabpanel">
                                                                                                                <div class="panel-group panel-group-simple panel-group-continuous"
                                                                                                                id="accordion8"
                                                                                                                aria-multiselectable="true"
                                                                                                                role="tablist">
                                                                                                                <!-- Question 1 -->
                                                                                                                <div class="panel">
                                                                                                                    <div class="panel-heading"
                                                                                                                    id="question-1" role="tab">
                                                                                                                    <a class="panel-title"
                                                                                                                    title="Day 1"
                                                                                                                    aria-controls="answer-1"
                                                                                                                    aria-expanded="true"
                                                                                                                    data-toggle="collapse"
                                                                                                                    href="#answer-1"
                                                                                                                    data-parent="#accordion8">
                                                                                                                    Blackout
                                                                                                                </a>
                                                                                                            </div>
                                                                                                            <div class="panel-collapse collapse show"
                                                                                                            id="answer-1"
                                                                                                            aria-labelledby="question-1"
                                                                                                            role="tabpanel">
                                                                                                            <div class="panel-body">
                                                                                                                Nothing To Show
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>

                                                                                                </div>
                                                                                            </div>
                                                                                            <!-- End Categroy 1 -->
                                                                                        </div>

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                          @if($get_transfer->transfer_inclusions!="")
                                                                        <div class="tab-pane active" id="Inclusion" role="tabpanel">
                                                                            <div class="row">
                                                                                <div class="col-md-12">
                                                                                    <div class="tab-content" style="margin-top:0">
                                                                                        <div id="navpills-1" class="tab-pane active">
                                                                                            <!-- Categroy 1 -->
                                                                                            <div class=" tab-pane animation-fade active"
                                                                                            id="category-1" role="tabpanel">
                                                                                            <div class="panel-group panel-group-simple panel-group-continuous"
                                                                                            id="accordion2"
                                                                                            aria-multiselectable="true"
                                                                                            role="tablist">
                                                                                            <!-- Question 1 -->
                                                                                            <div class="panel">
                                                                                                <div class="panel-heading"
                                                                                                id="question-1" role="tab">
                                                                                                <a class="panel-title"
                                                                                                title="Inclusions"
                                                                                                aria-controls="answer-1"
                                                                                                aria-expanded="true"
                                                                                                data-toggle="collapse"
                                                                                                href="#answer-1"
                                                                                                data-parent="#accordion2">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="panel-collapse collapse show"
                                                                                        id="answer-1"
                                                                                        aria-labelledby="question-1"
                                                                                        role="tabpanel">
                                                                                        <div class="panel-body">
                                                                                         @php echo $get_transfer->transfer_inclusions; @endphp
                                                                                     </div>
                                                                                 </div>
                                                                             </div>

                                                                         </div>
                                                                     </div>

                                                                 </div>

                                                             </div>
                                                         </div>
                                                     </div>
                                                 </div>
                                                 @endif

                                                    @if($get_transfer->transfer_exclusions!="")
                                                 <div class="tab-pane" id="Exclusion" role="tabpanel">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="tab-content" style="margin-top:0">
                                                                <div id="navpills-1" class="tab-pane active">
                                                                    <div class=" tab-pane animation-fade active"
                                                                    id="category-1" role="tabpanel">
                                                                    <div class="panel-group panel-group-simple panel-group-continuous"
                                                                    id="accordion2"
                                                                    aria-multiselectable="true"
                                                                    role="tablist">
                                                                    <div class="panel">
                                                                        <div class="panel-heading"
                                                                        id="question-1" role="tab">
                                                                        <a class="panel-title"
                                                                        title="Exclusions"
                                                                        aria-controls="answer-1"
                                                                        aria-expanded="true"
                                                                        data-toggle="collapse"
                                                                        href="#answer-1"
                                                                        data-parent="#accordion2">
                                                                    </a>
                                                                </div>
                                                                <div class="panel-collapse collapse show"
                                                                id="answer-1"
                                                                aria-labelledby="question-1"
                                                                role="tabpanel">
                                                                <div class="panel-body">
                                                                  @php echo $get_transfer->transfer_exclusions; 
                                                                  @endphp
                                                              </div>
                                                          </div>
                                                      </div>

                                                  </div>
                                              </div>
                                          </div>

                                      </div>
                                  </div>
                              </div>
                          </div>
                          @endif
                           @if($get_transfer->transfer_cancellation!="")
                          <div class="tab-pane" id="Cancellation" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab-content" style="margin-top:0">
                                        <div id="navpills-1" class="tab-pane active">
                                            <div class=" tab-pane animation-fade active"
                                            id="category-1" role="tabpanel">
                                            <div class="panel-group panel-group-simple panel-group-continuous"
                                            id="accordion2"
                                            aria-multiselectable="true"
                                            role="tablist">
                                            <div class="panel">
                                                <div class="panel-heading"
                                                id="question-1" role="tab">
                                                <a class="panel-title"
                                                title="Cancellation"
                                                aria-controls="answer-1"
                                                aria-expanded="true"
                                                data-toggle="collapse"
                                                href="#answer-1"
                                                data-parent="#accordion2">
                                            </a>
                                        </div>
                                        <div class="panel-collapse collapse show"
                                        id="answer-1"
                                        aria-labelledby="question-1"
                                        role="tabpanel">
                                        <div class="panel-body">
                                            @php echo $get_transfer->transfer_cancellation;
                                            @endphp
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
     
       
        <button type="button" class="close light-btn" data-dismiss="modal">&times;</button>
     

      <!-- Modal body -->
      <div class="modal-body">
        <img id="img-lightbox">
      </div>

      <!-- Modal footer -->
      

    </div>
  </div>
</div>
    @endif
    @if($get_transfer->transfer_terms_and_conditions!="")
    <div class="tab-pane" id="tc" role="tabpanel">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content" style="margin-top:0">
                    <div id="navpills-1" class="tab-pane active">
                        <!-- Categroy 1 -->
                        <div class=" tab-pane animation-fade active"
                        id="category-1" role="tabpanel">
                        <div class="panel-group panel-group-simple panel-group-continuous"
                        id="accordion2"
                        aria-multiselectable="true"
                        role="tablist">
                        <!-- Question 1 -->
                        <div class="panel">
                            <div class="panel-heading"
                            id="question-1" role="tab">
                            <a class="panel-title"
                            title="Terms And Conditions"
                            aria-controls="answer-1"
                            aria-expanded="true"
                            data-toggle="collapse"
                            href="#answer-1"
                            data-parent="#accordion2">
                        </a>
                    </div>
                    <div class="panel-collapse collapse show"
                    id="answer-1"
                    aria-labelledby="question-1"
                    role="tabpanel">
                    <div class="panel-body">
                     @php echo $get_transfer->transfer_terms_and_conditions;
                     @endphp
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>

</div>
</div>
</div>
</div>
@endif
</div>
</div>
</div>

</div>

@if($get_transfer->transfer_type!="city")
<div class="col-md-12">
    <div class="row" style="margin-top:20px" id="vehicle_type_div">

                                @foreach($get_vehicles as $vehicles)
                                @if($get_transfer->transfer_vehicle_type==$vehicles->vehicle_type_id)
                                       <!--  <i class="fa fa-user"></i> -->
                                       <div class="col-md-4 text-center parent-tile" style="cursor: pointer;display:none">
                                        <div class="box box-body bg-dark pull-up" @if($vehicles->vehicle_type_image!="")
                                        style="background: url({{ asset('assets/uploads/vehicle_type_images') }}/{{$vehicles->vehicle_type_image}});background-size: cover;background-repeat: no-repeat;" 
                                        @else 
                                        style="background: url({{asset('assets/images/vehicle-type.jpg')}});background-size: contain;background-repeat: no-repeat;" @endif>
                                            <div class="overlay"></div>
                                            <br>
                                            <p class="font-size-26 text-center color-tiles-text"> <a href="#" class="details text-light" >
                                              {{$vehicles->vehicle_type_name}}
                                              <input type="hidden" id="vehcile_type_name_{{$vehicles->vehicle_type_id}}" name="vehcile_type_name_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_name}}" ></a></p>
                                              <br>
                                          </div>
                                          <input type="radio" class="with-gap radio-col-primary vehcile_type_show" name="vehcile_type" id="vehcile_type_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_id}}" data-minvalue="{{$vehicles->vehicle_type_min}}" data-maxvalue="{{$vehicles->vehicle_type_max}}" checked="checked">
                                          <label for="vehcile_type_{{$vehicles->vehicle_type_id}}" class="tile-label"></label>
                                      </div>
                                    @endif        
                                @endforeach                               

                       
                    </div>
                    <hr>
                    <br>

                     <p class="star-p" id="guide_div_head" style="display:none"> <i class="fa fa-star star"></i> Select Guide for Transfer
                                </p>
                    <div class="col-md-12" id="guide_div" style="display:none">
                    </div>
                     <div class="text-center table-sightseeing-loader" style="display: none">
                      <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                      <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 15 ; 0 -15; 0 15" 
                        repeatCount="indefinite" 
                        begin="0.1"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 10 ; 0 -10; 0 10" 
                        repeatCount="indefinite" 
                        begin="0.2"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 5 ; 0 -5; 0 5" 
                        repeatCount="indefinite" 
                        begin="0.3"/>
                    </circle>
                </svg>
            </div>
</div>




        <div class="modal" id="guideModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Guide Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height:600px;overflow-y: auto;overflow-x: hidden;" id="guide_details_div">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div>
@endif













</div>

</div>
<style>
    .a-price {
        color: #363795;
        font-size: 40px;
        line-height: 1;
        font-weight: 600;
        position: relative;
    }

    span.from {
        color: #363795;
        font-size: 14px;
        display: block;
        margin-bottom: 10px;
    }

    .price-b {
        border: 3px dashed #363795;
        padding: 10px 15px;
        text-align: center;
        margin-bottom: 20px;
    }

    .heading-name p {
        color: #ffffff;
        background: linear-gradient(to right, #363795, #005C97);
        font-size: 16px !IMPORTANT;
        border: none;
        padding: 5px 10px;
        border-radius: 0 10px 10px 0;
        text-align: center;
        position: absolute;
        z-index: 99;
        min-width: 167px;
        transform: translate(-6px, 7px);
    }

    .heading-name p:before {
        content: "";
        position: absolute;
        border-width: 10px;
        border-style: solid;
        border-color: #2f2a2a00 #da204f transparent #00000000;
        top: 0;
        transform: translate(-83px, 24px);
    }


    .slider-box {
        padding: 6px;
        overflow: hidden !important;
    }

    .accor-div {
        display: flex;
        padding: 0;
        justify-content: space-between;
        align-items: flex-start;
    }

    .heading-name {
        border-bottom: none;
        margin-bottom: 0px;
        margin-top: 0;
    }

    .hotel_detail {
        background: #d0d3ff;
        padding: 15px;
        margin: 20px 0;
    }

    .panel-body p {
        color: #363795;
        font-size: 16px;
        text-transform: capitalize;
        
    }

    .panel-body {
        background: #f2f2f2;
    }

    .input-group-addon {
        border-color: gainsboro !important;
        border-radius: 0 !important;
        background: gainsboro !important;
    }

    input#select_date,
    select#adult_count,
    select#child_count {
        border-radius: 0;
        border-color: gainsboro;
        background: white;
    }

    span.info-s {
        color: white;
        background: #909090;
        font-size: 12px;
        padding: 2px 2px;
        width: 16px;
        font-weight: 800;
        height: 16px;
        border-radius: 50%;
        position: absolute;
        box-sizing: border-box;
        left: 80%;
        display: inline-block;
        cursor: auto;
    }

    ul.nav.nav-tabs.customtab li a.active {
        background: linear-gradient(to right, #363795, #005C97);
        border-radius: 5px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
        border: none;
    }
    .theme-rosegold .nav > li > a:hover, .theme-rosegold .nav > li > a:active, .theme-rosegold .nav > li > a:focus {
        color: #0d5497;
    }
    p.hotel-type {
        display: block;
        padding: 3px 10px 3px 0;
        margin-left: 17px;
        font-size: 20px;
        background: white;
        text-align: left;
        margin-bottom: 0;
        border: 1px solid white;
        color: #095697;
        border-bottom: 2px solid;
    }
    p.year-p {
        display: inline-block;
        padding-left: 45px;
        color: #303b95;
        font-size: 16px;
    }
    .form-group label {
        font-weight: 500;
        font-size: 13px;
    }  
    ul.nav.nav-tabs.customtab li a {
        border: 1px solid gainsboro;
        border-radius: 5px;
        padding: 8px 15px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
    }

    .tab-content.accord-content {
        flex: 0 0 75%;
        margin: 0;
    }

    ul.nav.nav-tabs.customtab li {
        margin: 0 0 10px;
        position: relative;
    }

    .panel-body {
        background: #d0d3ff;
        padding: 20px !important;
        color: #363795;
        height: auto;
        border: none !important;
        min-height: 155px;
        border-radius: 0;
    }

    ul.nav.nav-tabs.customtab {
        border: none;
        flex-direction: column;
        flex: 0 0 20%;
        text-align: left;
    }
    p.para-a {
    margin-left: 17px;
    margin-top: 10px;
}
img.transfer-img {
    cursor: pointer;
}
</style>
<div class="col-md-4">
    <div class="row">
        <div class="col-md-12">
            <div class="price-b">
                <span class="from"><span class=" ng-binding"> GEL</span></span>
                <div class="a-price">
                    <span id="total_price_text">@php 
                        $price=$get_transfer->transfer_vehicle_cost;
                        $transfer_markup_cost=round(($price*$markup)/100);
                        $total_agent_cost=round(($price+$transfer_markup_cost));

                        $own_transfer_markup_cost=round(($total_agent_cost*$own_markup)/100);
                        $total_cost=round(($total_agent_cost+$own_transfer_markup_cost));

                        echo $total_cost;
                        @endphp</span>
                </div>
            </div>
            <form id="transfer_booking_form" method="post"
            action="{{route('transfer-booking')}}">
            {{csrf_field()}}

            <div class="book_card">


                <div class="booking_detail" style="padding:0 !important">



                    <div class="">
                        <input type="hidden" name="markup" value="{{$markup}}">
                        <input type="hidden" name="own_markup" value="{{$own_markup}}">
                        <input type="hidden" name="transfer_id"
                        value="{{$get_transfer->transfer_id}}">
                        <input type="hidden" name="transfer_details_id"
                        value="{{$get_transfer->transfer_details_id}}">
                         <input type="hidden" name="transfer_type"
                        value="{{$get_transfer->transfer_type}}">
                        <input type="hidden" name="transfer_date"
                        value="{{session()->get('transfer_date')}}">
                        <input type="hidden" name="transfer_time"
                        value="{{session()->get('transfer_time')}}">
                        <input type="hidden" name="supplier_id"
                        value="{{$get_transfer->supplier_id}}">
                         <input type="hidden" name="adult_count" 
                        value="{{session()->get('transfer_adult_count')}}">
                         <input type="hidden" name="child_count" 
                        value="{{session()->get('transfer_child_count')}}">
                        <input type="hidden" name="transfer_child_age" 
                        value="{{serialize(session()->get('transfer_child_age'))}}">
                        <input type="hidden" name="agent_transfer_price" value="@php 
                        $price=$get_transfer->transfer_vehicle_cost;
                        $transfer_markup_cost=round(($price*$markup)/100);
                        $total_cost=round(($price+$transfer_markup_cost));
                        echo $total_cost;
                        @endphp">
                          <input type="hidden" name="customer_transfer_price" value="@php 
                        $price=$get_transfer->transfer_vehicle_cost;
                        $transfer_markup_cost=round(($price*$markup)/100);
                        $total_agent_cost=round(($price+$transfer_markup_cost));

                        $own_transfer_markup_cost=round(($total_agent_cost*$own_markup)/100);
                        $total_cost=round(($total_agent_cost+$own_transfer_markup_cost));

                        echo $total_cost;
                        @endphp">
                         <input type="hidden" name="vehicle_count" 
                        value="1">
                        <input type="hidden" name="supplier_transfer_price" 
                        value="{{$get_transfer->transfer_vehicle_cost}}">
                        <input type="hidden" name="total_price" id="total_price"
                        value="@php 
                        $price=$get_transfer->transfer_vehicle_cost;
                        $transfer_markup_cost=round(($price*$markup)/100);
                        $total_agent_cost=round(($price+$transfer_markup_cost));

                        $own_transfer_markup_cost=round(($total_agent_cost*$own_markup)/100);
                        $total_cost=round(($total_agent_cost+$own_transfer_markup_cost));

                        echo $total_cost;
                        @endphp">

                    </div>
                    <div class="">

                        <div class="form-group">
                            <label for="select_date">VEHICLE INFO :<span
                                class="asterisk">*</span></label>
                                <p class="year-p"> {{$get_transfer->transfer_vehicle_info}}
                                    <!-- /.input group -->

                                </div>

                            </div>




                            <div class="">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="adult_count">ADULT 
                                            </label>
                                        </div>
                                        <div class="col-md-7">
                                            {{session()->get('transfer_adult_count')}}
                                        </div>


                                    </div>

                                </div>

                            </div>



                            <div class="">

                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <label for="child_count">CHILD </label>
                                        </div>
                                        <div class="col-md-7">
                                          {{session()->get('transfer_child_count')}}
                                          @if(session()->get('transfer_child_count')>0)
                                          (@php $child_age=session()->get('transfer_child_age');
                                          echo implode(",",$child_age);
                                          @endphp)
                                          @endif
                                      </div>


                                  </div>

                              </div>

                          </div>




                          <div class="hotel_detail">
                        @if($get_transfer->transfer_type=="city")

                           <p class="para"><b>FROM CITY :  </b>
                            @php
                            $from_city=ServiceManagement::searchCities(session()->get('transfer_from_city'),$get_transfer->transfer_country);
                            echo $from_city['name'];
                            @endphp
                        </p>
                        <p class="para"><b>PICK UP LOCATION :  </b>{{session()->get('transfer_pickup_location')}}
                        </p>
                        <p class="para"><b>TO CITY :  </b>
                            @php
                            $to_city=ServiceManagement::searchCities(session()->get('transfer_to_city'),$get_transfer->transfer_country);
                            echo $to_city['name'];
                            @endphp
                        </p>
                        <p class="para"><b>DROP OFF LOCATION :  </b>{{session()->get('transfer_dropoff_location')}}
                        </p>


                        @elseif($get_transfer->transfer_type=="from-airport")


                         <p class="para"><b>FROM AIRPORT :  </b>
                              @php
                            $to_city=LoginController::searchAirports(session()->get('transfer_from_city_airport'));
                            echo $to_city['airport_master_name'];
                            @endphp
                              <input type="hidden" name="airport_id" id="airport_id" value="{{$to_city['airport_master_id']}}">

                        </p>

                        <p class="para"><b>TO CITY :  </b>
                            @php
                            $to_city=ServiceManagement::searchCities(session()->get('transfer_to_city_airport'),$get_transfer->transfer_country);
                            echo $to_city['name'];
                            @endphp
                             <input type="hidden" name="city_id" id="city_id" value="{{$to_city['id']}}">
                        </p>

                        <p class="para"><b>DROP OFF LOCATION :  </b>{{session()->get('transfer_location')}}
                        </p>


                        @else
                        <p class="para"><b>FROM CITY :  </b>
                            @php
                            $from_city=ServiceManagement::searchCities(session()->get('transfer_from_city_airport'),$get_transfer->transfer_country);
                            echo $from_city['name'];
                            @endphp
                             <input type="hidden" name="city_id" id="city_id" value="{{$from_city['id']}}">
                        </p>

                         <p class="para"><b>PICK UP LOCATION :  </b>{{session()->get('transfer_location')}}
                        </p>

                        <p class="para"><b>TO AIRPORT :  </b>
                             @php
                            $to_city=LoginController::searchAirports(session()->get('transfer_to_city_airport'));
                            echo $to_city['airport_master_name'];
                            @endphp
                             <input type="hidden" name="airport_id" id="airport_id" value="{{$to_city['airport_master_id']}}">
                        </p>

                        @endif
                        <p class="para"><b>COUNTRY  :
                        </b>{{$get_transfer->country->country_name}}</p>

                    </div>
                    @if($get_transfer->transfer_type!="city")
                    <div>
                         <label for="with_guide_head">Do you want Guide with transfer ?</label><br>
                        <input type="checkbox" name="with_guide" id="with_guide" value="Yes">
                          <label for="with_guide">Yes</label>
                       
                    </div>
                    @endif
                        <br>

                    <div class="book_btn">
                       <input type="hidden" name="selected_vehicle_type_id" value="0">
                       <input type="hidden" name="selected_vehicle_type_name" value="">
                       <input type="hidden" name="selected_guide_id" value="0">
                       <input type="hidden" name="selected_guide_name" value="">
                       <input type="hidden" name="selected_guide_supplier_cost" value="">
                       <input type="hidden" name="selected_guide_agent_cost" value="0">
                       <input type="hidden" name="selected_guide_customer_cost" value="0">
                       <input type="hidden" id="selected_transfer_date" name="selected_transfer_date" value="{{$transfer_date}}">
                        <button type="submit" id="activity_book_btn"
                        class="btn btn-rounded btn-primary mr-10">BOOK</button>
                    </div>


                </div>
            </div>
        </form>
        
    </div>
</div>

</div>
{{-- start --}}

</div>
</div>
</div>

</div>

</div>




</div>
</div>
</div>


@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
<script>
    
    var img=document.getElementsByClassName("transfer-img")
    var array=[].slice.call(img);
   
    array.forEach((v)=>{
         console.log(v)
        v.setAttribute("data-toggle","modal")
         v.setAttribute("data-target","#myModal")
        v.addEventListener("click",function(){
           
            var src1=this.src;
            document.getElementById("img-lightbox").src=src1

        })
    })
</script>
@if($get_transfer->transfer_type!="city")
<script>
       var tile=document.getElementsByClassName("parent-tile")
        
        for(var i=0;i<tile.length;i++){
            tile[i].addEventListener("click",function(){
                this.lastElementChild.click();  
            });
        }
       

        //on vehcile type change , get the respective guides and their cost
        $(document).on("change","input[name='vehcile_type']",function()
        {
            var airport_id=$("input[name='airport_id']").val();
            var city_id=$("input[name='city_id']").val();
            var vehicle_type_id=$(this).val();
             var vehicle_type_name=$("#vehcile_type_name_"+vehicle_type_id).val();
              var select_date=$("#selected_transfer_date").val();
              
            $("input[name='selected_vehicle_type_id']").val(vehicle_type_id);
              $("input[name='selected_vehicle_type_name']").val(vehicle_type_name);
              $(".table-sightseeing-loader").show();

                    $("#guide_div").html("");
            $.ajax({
                url:"{{route('fetchGuidesTransfer')}}",
                data:{"airport_id":airport_id,
                        "vehicle_type_id":vehicle_type_id,
                        "transfer_date":select_date,
                        "city_id":city_id},
                type:"GET",
                success:function(response)
                {
                    $("#guide_div").html(response);
                    $(".table-sightseeing-loader").hide();

                    //empty guide id and cost because guides data refreshed
                    $("input[name='selected_guide_id']").val("");
                    $("input[name='selected_guide_supplier_cost']").val(0);
                     $("input[name='selected_guide_agent_cost']").val(0);
                      $("input[name='selected_guide_customer_cost']").val(0);
                      $("input[name='selected_guide_name']").val("");


                }
            }).done(function(){
                var guideDiv=$(".guide-div");
                guideDiv.on("click",function(){
                 
                    this.getElementsByClassName("tile-label")[0].click()
                })
            });
           
        });
     
              $(document).on("click",".view_guide",function()
             {
                var id=$(this).attr("id").split("_")[1];

                $.ajax({
                    url:"{{route('fetchGuidesDetails')}}",
                    type:"GET",
                    data:{"guide_id":id},
                    success:function(response)
                    {
                        $("#guide_details_div").html(response);

                        $("#guideModal").modal("show");
                        $('#guide_details_div').animate({
                            scrollTop: $('#guide_details_div').position().top
                        }, 'slow');
                    }

                });
             });
       
           $("input[name='vehcile_type']").trigger("change");
  function roundLikePHP(num, dec){
  var num_sign = num >= 0 ? 1 : -1;
  return parseFloat((Math.round((num * Math.pow(10, dec)) + (num_sign * 0.0001)) / Math.pow(10, dec)).toFixed(dec));
}


             //get the cost and guide that is selected
           $(document).on("change","input[name='guide']",function()
        {
            var guide_id=$(this).val();
            var guide_customer_cost=$("#guide_price_"+guide_id).text();
            var guide_agent_cost=$("#guide_agent_cost_"+guide_id).text();
            var guide_supplier_cost=$("#guide_cost_"+guide_id).text();
             var guide_name=$("#guide_name_"+guide_id).text();
            var discount = parseFloat($("#discount").val());

            

            $("input[name='selected_guide_id']").val(guide_id);
             $("input[name='selected_guide_supplier_cost']").val(guide_supplier_cost);
              $("input[name='selected_guide_agent_cost']").val(guide_agent_cost);
              $("input[name='selected_guide_customer_cost']").val(guide_customer_cost);
              $("input[name='selected_guide_name']").val(guide_name);

               

            var total_price = $("input[name='customer_transfer_price']").val();

            var total_price_after_guide = parseFloat(parseFloat(guide_customer_cost) + parseFloat(total_price));
            
                var total_price_after_guide_1=roundLikePHP(total_price_after_guide,0);

                $("#total_price").val(total_price_after_guide_1);


                $("#total_price_text").text(total_price_after_guide_1);

          
        });

           $(document).on("change","#with_guide",function()
           {
                if($(this).prop("checked")==true)
                {
                   $("#guide_div_head").show();
                   $("#guide_div").show();
                     $("input[name='selected_guide_id']").val("");
                    $("input[name='selected_guide_supplier_cost']").val(0);
                     $("input[name='selected_guide_agent_cost']").val(0);
                      $("input[name='selected_guide_customer_cost']").val(0);
                      $("input[name='selected_guide_name']").val("");

                       $("input[name='guide']").prop("checked",false);

                      var actual_price=$("input[name='customer_transfer_price']").val();
                      $("#total_price").val(actual_price);
                      $("#total_price_text").text(actual_price);
                }
                else
                {
                    $("#guide_div_head").hide();
                     $("#guide_div").hide();
                     $("input[name='selected_guide_id']").val("");
                    $("input[name='selected_guide_supplier_cost']").val(0);
                     $("input[name='selected_guide_agent_cost']").val(0);
                      $("input[name='selected_guide_customer_cost']").val(0);
                      $("input[name='selected_guide_name']").val("");

                       $("input[name='guide']").prop("checked",false);

                      var actual_price=$("input[name='customer_transfer_price']").val();
                      $("#total_price").val(actual_price);
                      $("#total_price_text").text(actual_price);

                }
           });

        $(document).on("submit", "#transfer_booking_form", function (e) {
            if($("#with_guide").prop("checked")==true)
                {
                if (!$("input[name='vehcile_type']:checked").val()) {
                    e.preventDefault();
                    $("#vehcile_type_table").css("border", "1px solid #cf3c63");
                    window.scrollTo(0,document.querySelector("body").scrollHeight);
                    alert("Please Select Vehicle Type first");
                } else

                {
                    $("#vehcile_type_table").css("border", "1px solid #9e9e9e");
                     if ($("input[name='selected_guide_id']").val()==0) {
                    e.preventDefault();
                    $("#guide_div").css("border", "1px solid #cf3c63");
                    window.scrollTo(0,document.querySelector("body").scrollHeight);
                    alert("Please Select Guide first");
                } else

                {
                    $("#guide_div").css("border", "1px solid #9e9e9e");
                }

                }
            }

        });

        </script>
@endif