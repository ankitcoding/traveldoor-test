@include('agent.includes.top-header')
<style>
        div.overlay {
        position: absolute;
        width: 100%;
        bottom: 0;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #5d4ecbbd !important;
        border-top-right-radius: 0 !important;
    }
    .selection-div {
    border: 1px solid gainsboro;
    padding: 0;
    border-radius: 5px;
    margin-bottom: 33px !IMPORTANT;
    background: url(https://sf2.mariefranceasia.com/wp-content/uploads/sites/7/2018/02/bawah-615x410.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}
    div.overlay-2 {
        position: absolute;
        width: 100%;
        top: 0;

        opacity: 0;
        bottom: 0;
        z-index: 99;
        transition: .5s ease;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #000000bd !important;
        border-top-right-radius: 0 !important;
    }

    .img-div:hover div.overlay-2 {
        transition: .5s ease;

        opacity: 1;
    }

    .img-div:hover .ribbon2 {
        display: none;
    }

    .img-div:hover::before,
    .img-div:hover::after {
        border: none
    }

    .left {
        float: left;
        width: 50%;
    }

    p.from {
        color: white;
        text-align: right;
        padding: 5px 10px 0;
        font-size: 18px;
        margin: 0;
    }

    span.price-span {
        display: block;
        color: white;
        text-align: right;
        padding: 0 10px;
    }

    img.h-img {
        width: 100%;
    }

   p.h-head {
    color: white;
    font-size: 15px;
    padding: 5px 10px;
    margin: 0;
    z-index: 9999;
    position: absolute;
    top: 0;
    left: 0;
    color: white;
    background: #E91E63;
    width: 100%;
}

    span.s-span {
        color: white;
        padding: 0 10px;
    }

    .img-div {
        border: 1px solid #756fcc;
        padding: 2px;
        border-radius: 5px;
        position: relative;
        z-index: 9;
        margin-bottom: 42px;
        transition: .5s ease;
    }

    /*  .img-div:before {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c70 transparent transparent;
        top: 56px;
        left: -14px;
        
        z-index: 0;
    }

    .img-div:after {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c7000 #402c70 transparent;
        top: -14px;
        left: 58px;
        
        z-index: -37;
    } */

    .ribbon2:before {
        content: "";
        position: absolute;
        top: 0;
        padding: 10px;
        /* margin: 5px; */
        left: 0;
        width: 100px;
        height: 20px;
        background: #5225c5;
        transform: rotate(135deg) translate(28px, 1px);
    }

    .ribbon2 {
        position: absolute;
        top: -7px;
        left: -7px;
        width: 100px;
        height: 100px;
        /* position: absolute; */
        overflow: hidden;
        /* background: aliceblue; */
    }

    button.hover-btn {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        border: none;
        background: #ffffff;
        color: #5225c5;
        padding: 7px 15px;
        cursor: pointer;
        border-radius: 5px;
    }

    @media screen and (max-width:1170px) {
        p.h-head {
            color: white;
            font-size: 15px;
            padding: 0 10px;
            margin: 0;
        }

        p.from {
            color: white;
            text-align: right;
            padding: 0px 10px 0;
            font-size: 15px;
            margin: 0;
        }

        span.s-span {
            color: white;
            padding: 0 10px;
            font-size: 11px;
        }

        span.price-span {
            display: block;
            color: white;
            text-align: right;
            padding: 0 10px;
            font-size: 12px;
        }
    }

     .table-activity-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
  }

   .carousel-item img {

        height: 100%;

    }



    .carousel-item {

        height: 100%;

    }



    p.start_price {

        margin: 0;

    }



    p.country_name.ng-binding {

        font-size: 20px;

        margin: 0;

    }



    .book_card {

        /* padding: 15px; */

        background: #fefeff;

        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);

        margin-bottom: 50px;



        border-radius: 5px;

    }



    .hotel_detail {

        height: 150px;

    }



    a.moredetail.ng-scope {

        background: gainsboro;

        padding: 7px 10px;

        /* margin-bottom: 10px; */

    }



    .booking_label {

        background: #5d53ce;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;

        color: white;

        padding: 10px 15px;

    }



    .booking_detail {

        padding: 15px;

    }



    a.btn.btn-outline.btn-circle.book_btn1 {

        background: #E91E63;

        border-radius: 5px !IMPORTANT;

        padding: 5px 20px;

        width: auto !important;

        height: auto;

        line-height: 2;

        color: white;

    }



    td p {

        margin: 0;

    }



    td {

        background: gainsboro;

    }



    table {

        border-collapse: separate;

    }



    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: 1px solid #59d25a;

    }



    a.panel-title {

        position: relative !important;

        background: #dfffe3;

        color: green !important;

        padding: 13px 20px 13px 85px !important;

        /* border-bottom: 1px solid #3ca23d; */

    }

.panel-title {

    display: block;

    margin-top: 0;

    margin-bottom: 0;

    padding: 1.25rem;

    font-size: 18px;

    color: #4d4d4d;

    height: 48px;

}

    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: none !important;

    }



    .panel-body {

        background: white;

        /* border: 1px solid #59d25a; */

        padding: 10px !important;

    }



    a.panel-title:before {

        content: attr(title) !important;

        position: absolute !important;

        top: 0px !important;

        opacity: 1 !important;

        left: 0 !important;

        padding: 12px 10px;

           width: auto;

    max-width: 250px;

        text-align: center;

        color: white;

        font-family: inherit !important;

        height: 48px;

        background: #279628;

        z-index: 999;

        transform: none !important;

    }



    .tab-content {

        margin-top: 10px;

    }



    div.panel-heading {

        border: 1px solid #59d25a !important;

    }



    .panel {

        border-top: none !important;

        margin-bottom: 5px !important;

    }

div#carousel-example-generic-captions {

    width: 100%;

}

/*daman css*/

    .hotel-div {

        width:75%;

        float:left;
        border-radius: 5px;
        padding: 8px;

        display: flex;

        background: white;

    }

    button.book-btn {
    background: #d427be;
    border: none;
    border-radius: 50px !important;
    margin-top: 50px !important;
    padding: 10px 20px;
    text-align: center;
    color: white;
    width: 160px;
}

 .hotel-list-div {
    clear: both;
    display: block;
    border: 1px solid #ffc9f8;
    border-radius: 5px;
    position: relative;
    /* clear: both; */
    height: 188px;
}

    .hotel-img-div {

       

        width: 35% !important;

        float:left;

    }



    .hotel-details {

     float:left;

        width: 60%;
        position: relative;
        padding: 0 15px;

    }
    p.hotel-type {
    position: absolute;
    width: auto;
    top: 14px;
    right: -20px;
    padding: 5px 10px;
    background: linear-gradient(45deg, #d5135a, #f05924);
    border: 1px solid white;
    border-radius: 30px;
    color: white;
    min-width: 100px;
    text-align: center;
}
a.img-div i {
    display: block;
    margin: 2px 1px 1px;
    color: #e94831;
}
a.img-div {
    background: #ffcec3 !important;
    display: block;
    padding: 6px 8px 9px;
    width: 30px;
    height: 30px;
    border: none;
    top: 16px;
    position: absolute;
    margin-bottom: 20px;
    right: 82px;
    border-radius: 50%;
}

   .hotel-info-div {
    width: 25%;
    float: left;
    padding: 15px;
    background: #ffe0fb;
    text-align: right;
    height: 100%;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
}



    .checked {

        color: orange;

    }



    span.hotel-s {

        border: 1px solid #F44336;

        padding: 2px 5px;

        display: inline-block;

        color: #f44336;

        font-size: 12px;

        margin: 0 10px 0 0;

    }



    p.hotel-name {

        font-size: 22px;

        margin: 10px 0 0;

        color: black;

        float: left;

    }



    .rate-no {

        float: right;

    }





    .heading-div {

        clear: both;

    }



    .rating {

        display: block;

        float: left;

        list-style: none;

        margin: 0;

        padding: 0;

    }


    .tags-div {
    display: flex;
    justify-content: space-between;
    width: 40%;
}
.tags-div p {
    color: #dc254c;
    background: white;
    border: 1px solid #d91e51;
    padding: 2px 5px;
    margin-right: 10px;
}
    p.r-number {

        float: right;

    }



    p.time-info {

        color: #4CAF50;

    }



    p.info {

        clear: both;

        margin: 0;

    }



    span.tag-item {

        background: #ffcbcd;

        padding: 5px 10px;

        border-radius: 5px;

        color: #ff4e54;

    }



    .inclusions {

        margin: 20px 0;

    }



    span.inclusion-item {

        padding: 10px 10px 0 0;

        color: #644ac9;

    }



    img.icon-i {

        width: auto;

        height: 20px;

    }



    p.include-p {

        color: black;

    }



    .inclusion-p {

        color: green

    }



    p.price-p span {

        background: #ee2128;

        color: white;

        padding: 3px 5px 3px 9px;

        border-radius: 5px;

        position: relative;

        z-index: 9999;

        border-top-right-radius: 5px;

        border-bottom-right-radius: 5px;

        margin-left: 20px;

        border-bottom-left-radius: 4px;

        font-size: 12px;

    }



    p.price-p span:before {

        content: "";

        width: 15px;

        height: 14.5px;

        background: #ee2128;

        position: absolute;

        transform: rotate(45deg);

        top: 3.5px;

        left: -6px;

        border-radius: 0px 0px 0px 3px;

        z-index: -1;

    }



    p.price-p span:after {

        content: "";

        background: white;

        width: 4px;

        height: 4px;

        position: absolute;

        top: 50%;

        left: 0px;

        transform: translateY(-50%);

        border-radius: 50%;

    }



    p.price-p {

        color: #ee2128;

    }



    p.tax {

        font-size: 12px;

        margin: 0;

    }



    p.days {

        font-size: 12px;

        margin: 0;

    }



    p.offer {

        font-size: 25px;

        color: black;

        font-weight: bold;

        margin: 0;

    }



    p.cut-price {

        margin: 0;

        text-decoration: line-through;

        font-size: 19px;

    }



    .login-a {

        color: #0088ff;

        font-size: 15px;

        font-weight: bold;

        margin-top: 10px;

        display: block;

    }

    @media screen and (max-width:1200px){

        p.hotel-name {

    font-size: 17px;

    margin: 10px 0 0;

    color: black;

    float: left;

}

p.r-number {

    float: right;

    font-size: 12px;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: block;

}

p.cut-price {

    margin: 0;

    text-decoration: line-through;

    font-size: 16px;

}

p.offer {

    font-size: 20px;

    color: black;

    font-weight: bold;

    margin: 0;

}

.login-a {

    color: #0088ff;

    font-size: 13px;

    font-weight: bold;

    margin-top: 10px;

    display: block;

}

    }

    @media screen and (max-width:1200px){

        .hotel-div {

    width: 100%;

    float: left;

    padding: 15px;
    border-radius: 5px;

    display: flex;

    background: white;

}

.hotel-info-div {

    width: 100%;

    height: 100%;

    float: left;

    padding: 15px;

    background: #ffe0fb;

    text-align: left;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: inline;

}

    }

    @media screen and (max-width:992px){

        p.hotel-name {

    font-size: 17px;

    margin: 10px 0 0;

    color: black;

    float: none;

}

.rate-no {

    float: none;

    display: none;

    margin-top: 14px;

    background: #2d3134;

    color: white;

    padding: 1px 8px;

    font-size: 12px;

    border-radius: 5px;

}

.rating {

    display: block;

    float: none;

    list-style: none;

    margin: 0;

    padding: 0;

}

p.r-number {

    float: none;

    font-size: 12px;

}

.hotel-details {

    float: none !important;

    width: 100% !important;

    padding: 0 15px;

    margin-top: 20px;

}
.hotel-list-div {
    clear: both;
    display: block;
    border: 1px solid #ffc9f8;
    border-radius: 5px;
    position: relative;
    /* clear: both; */
    height: 188px;
}

.hotel-div {

    width: 100%;

    float: none;

    padding: 15px;

    display: block;
    border-radius: 5px;

    background: white;

}

.hotel-img-div {

    width: 100% !important;

    float: none !important;

    display: block !important;

}

a.flex-prev,a.flex-next {

    display: none;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: block;

}

    }

    .img-slide{

            height: 170px;

    width: 100%;

    }

    .flex-control-thumbs {

    margin: 5px 0 0;

    position: static;

    overflow: hidden;

    width: 100%;

    height: 50px;
}
.form-group label{
    color: #fff;
}
.selection-div >.row.mb-10 {
    position: relative;
    background: #2727e07a;
    top: 0;
    left: 14px;
    width: 100%;
    min-height: 140px;
    height: auto;
    bottom: 0;
    padding-top: 26px;
    padding-bottom: 26px;
    border-radius: 5px;
    margin-bottom: 0 !important;
}

.theme-rosegold .btn-success {
    border-color: #F44336;
    background-color: #F44336 !important;
    color: #fff;
}
.input-group.date input {
    background: #dcdada7d !important;
    border-color: white;
    border: 1px solid #fff;
    color: #fff;
    height: 32px;
    border-right: 0px;
    border-top-right-radius: 0px !important;
    border-bottom-right-radius: 0px !important;
}
.select2-container--default .select2-selection--single {
    border: 1px solid #ddd;
    border-radius: 22px;
    /* padding: 6px 12px; */
    /* height: 34px; */
    color: white !important;
    background: #dcdada7d !important;
}

.form-control + .input-group-addon:not(:first-child) {
    border-radius: 0px 16px 14px 0px !important;
    border-left: 0;
}
button#search_hotel {
    text-align: center;
    margin-top: 22px;
    min-width: 110px;
}
.form-control {
    border-radius: 22px !important;
    box-shadow: none;
    border-color: #9e9e9e;
    height: auto;
    color: #fff;
    background-color: #dcdada7d !important;
}
/*----------- 30-1-2020---------*/
.input-group .input-group-addon {
    
    border-color: #fff;
    background-color: #dcdada7d;
}
select#room_quantity {
    border-color: #fff;
}
.select2-container--default .select2-selection--single .select2-selection__rendered{
    color: #fff !important;
}
.adult-div {
    border-radius: 5px;
    position: absolute;
    top: 65px;
    left: 20px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    width: 315px;
    z-index: 999;
}
span {cursor:pointer; }
.number {
    width: 80%;
    margin: 0 auto 20px;
    background: white;
    border-radius: 50px;
    overflow: hidden;
    border: 1px solid gainsboro;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0 10px 65px -11px rgba(0, 0, 0, 0.25);
}
.minus, .plus {
    height: 34px;
    flex: 0 0 30%;
    background: none;
    border-radius: 4px;
    padding: 2px 5px 5px 5px;
    border: none;
    position: relative;
    display: inline-block;
    vertical-align: middle;
    border-radius: 50px;
    text-align: center;
}
span.minus:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 13px;
    height: 2px;
    background: #979797;
}

.minus:hover, .plus:hover{
    box-shadow: -1px 3px 50px 3px #E3E3E3;
}
input.counter {
    height: 39px;
    text-align: center;
    flex: 0 0 33%;
    width: 33%;
    background: none;
    border: 1px solid gainsboro !important;
    font-size: 18px;
    border-top: 0 !important;
    border-bottom: 0 !important;
    border-radius: 0;
    display: inline-block;
    vertical-align: middle;
}
span.plus:before {
    content: "";
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    width: 15px;
    height: 2px;
    background: #979797;
}
span.plus:after {
    content: "";
    position: absolute;
    top: 69%;
    left: 29.21%;
    transform: rotate(90deg) translate(-50%, -50%);
    width: 14px;
    height: 2px;
    background: #979797;
}
.more_hotel
{
    display: none;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('agent.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative"> 

@include('agent.includes.nav')

    <div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">Hotels</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item active" aria-current="page">Home

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

            <!-- <div class="right-title">

                <div class="dropdown">

                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

                            class="mdi mdi-dots-horizontal"></i></button>

                    <div class="dropdown-menu dropdown-menu-right">

                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

                        <div class="dropdown-divider"></div>

                        <button type="button" class="btn btn-rounded btn-success">Submit</button>

                    </div>

                </div>

            </div> -->

        </div>

    </div>


   <div class="row">
      <div class="box">
          <div class="box-body">
                  
                         <div class="row">

                            <div class="col-12">

                                <div class="box">



                                    <div class="box-body">
                                        <div class="selection-div">
                                      <div class="row mb-10">

                                        <div class="col-sm-12 col-md-12 col-lg-3">
                                            <div class="form-group">
                                                <label for="hotel_country">COUNTRY <span class="asterisk">*</span></label>
                                                <select id="hotel_country" name="hotel_country" class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">SELECT COUNTRY</option>
                                                    @if(!empty($hotel_array))
                                                        @foreach($countries as $country)
                                                        <option value="{{$country->country_id}}" @if($hotel_array['selected_country']==$country->country_id)selected @endif>{{$country->country_name}}</option>

                                                        @endforeach
                                                    @else
                                                        @foreach($countries as $country)
                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                        @endforeach
                                                    @endif
                                                    
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-3">

                                            <div class="form-group">
                                                <label for="hotel_city">CITY <span class="asterisk">*</span></label>
                                                <select id="hotel_city" name="hotel_city" class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">SELECT CITY</option>
                                                     @if(!empty($hotel_array))
                                                        @foreach($fetch_cities as $cities)
                                                        <option value="{{$cities->id}}" @if($hotel_array['selected_city']==$cities->id)selected @endif>{{$cities->name}}</option>

                                                        @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                       
                                        <div class="col-sm-12 col-md-12 col-lg-3">
                                            <label for="hotel_name" style="color:white">Hotel Name</label>
                                                <select id="hotel_name" name="hotel_name" class="form-control select2" style="width: 100%;">
                                                
                                                </select>
                                            
                                        </div>
                                        
                                        <div class="col-sm-12 col-md-12 col-lg-3">

                                            <div class="form-group">
                                                <label for="hotel_date_from">CHECKIN DATE <span class="asterisk">*</span></label>
                                                <div class="input-group date">
                                                    <input type="text" placeholder="CHECKIN DATE"
                                                    class="form-control pull-right datepicker" id="hotel_date_from" name="date_from" readonly="readonly" value="@if(!empty($hotel_array)){{$hotel_array['checkin']}}@endif">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->

                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-3">

                                            <div class="form-group">
                                             <label for="hotel_date_to">CHECKOUT DATE <span class="asterisk">*</span></label>
                                             <div class="input-group date">
                                                <input type="text" placeholder="CHECKOUT DATE"
                                                class="form-control pull-right datepicker" id="hotel_date_to" name="date_to" readonly="readonly" value="@if(!empty($hotel_array)){{$hotel_array['checkout']}}@endif">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-3" >
                                        <div class="form-group">
                                          <label for="room_quantity">Room Quantity<span class="asterisk">*</span></label>
                                            <input type="text" class="form-control" style="width: 100%;"  id="room_quantity" name="room_quantity" required="required" readonly="readonly" value="@if(!empty($hotel_array)){{$hotel_array['adults']}} adults-{{$hotel_array['child']}} children-{{$hotel_array['room']}} room
                                            @else 2 adults-0 children-1 room
                                            @endif">
                                            <div id="rooms_main_div" class="adult-div" style="background-color:white;padding:20px;display:none;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        Adults
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="number">
                                                            <span class="minus"></span>
                                                            <input type="text" name="no_of_adults" id="no_of_adults" class="counter" @if(!empty($hotel_array)) value="{{$hotel_array['adults']}}" @else  value="2" readonly="readonly" @endif/>
                                                            <span class="plus"></span>
                                                        </div>
                                                       {{--  <div class="form-group">
                                                        <select name="no_of_adults" id="no_of_adults" class="no_of_adults form-control" style="color:black;">
                                                            <option value="" hidden="hidden">Adults</option>
                                                            @if(!empty($hotel_array))
                                                                @for($i=1;$i<=20;$i++)
                                                                <option @if($i==$hotel_array['adults']) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            @else
                                                                @for($i=1;$i<=20;$i++)
                                                                <option @if($i==2) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            @endif
                                                           
                                                        </select>
                                                    </div> --}}
                                                    </div>
                                                </div>
                                                  <div class="row">
                                                    <div class="col-md-4">
                                                        Children
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="number">
                                                            <span class="minus"></span>
                                                            <input type="text" name="no_of_child" id="no_of_child" class="counter" @if(!empty($hotel_array)) value="{{$hotel_array['child']}}" @else  value="0" readonly="readonly" @endif/>
                                                            <span class="plus"></span>
                                                        </div>
                                                       {{--  <div class="form-group">
                                                        <select name="no_of_child" id="no_of_child" class="no_of_child form-control" style="color:black;">
                                                            <option value="" hidden="hidden">Children</option>
                                                             @if(!empty($hotel_array))
                                                                @for($i=0;$i<=10;$i++)
                                                                <option @if($i==$hotel_array['child']) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            @else
                                                                @for($i=0;$i<=10;$i++)
                                                                <option @if($i==0) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            @endif

                                                            
                                                        </select>
                                                    </div> --}}
                                                    </div>
                                                </div>
                                                 <div class="row">
                                                    <div class="col-md-4">
                                                        Rooms
                                                    </div>
                                                     <div class="col-md-8">
                                                        <div class="number">
                                                            <span class="minus"></span>
                                                            <input type="text" name="no_of_rooms" id="no_of_rooms" class="counter" @if(!empty($hotel_array)) value="{{$hotel_array['room']}}" @else  value="1" readonly="readonly" @endif/>
                                                            <span class="plus"></span>
                                                        </div>
                                                       {{--  <div class="form-group">
                                                        <select name="no_of_rooms" id="no_of_rooms" class="no_of_rooms form-control" style="color:black;">
                                                            <option value="" hidden="hidden">Rooms</option>
                                                             @if(!empty($hotel_array))
                                                                @for($i=1;$i<=10;$i++)
                                                                <option @if($i==$hotel_array['room']) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            @else
                                                                @for($i=1;$i<=10;$i++)
                                                                <option @if($i==1) selected @endif>{{$i}}</option>
                                                                @endfor
                                                            @endif

                                                            
                                                        </select>
                                                    </div> --}}
                                                    </div>
                                                </div>
                                                <div id="child_age_div" class="row">
                                                  @if(!empty($hotel_array))
                                                  @if($hotel_array['child']>0)

                                                  @for($i=0;$i< count($hotel_array['hotel_child_age']);$i++)
                                                  <div class="col-md-6"> <div class="form-group"><label for="child_age{{($i+1)}}" style="color:black">Child Age {{($i+1)}}<span class="asterisk" >*</span></label><input type="text" id="child_age{{($i+1)}}" name="child_age[]" class="form-control child_age" style="color:black" value="{{$hotel_array['hotel_child_age'][$i]}}" onkeypress="javascript:return validateNumber(event)" required="required"></div></div>
                                                  @endfor

                                                  @endif 
                                                  @endif  
                                                </div>
                                            </div>
                                    
                                        </div>
                                   
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">

                                    <div class="form-group">
                                        <label for="hotel_rating">Hotel Star</label>
                                        <select id="hotel_rating" name="hotel_rating" class="form-control select2" style="width: 100%;">
                                            <option value="0" selected="selected">ANY STAR</option>
                                            @if(!empty($hotel_array))
                                            @for($i=5;$i>=1;$i--)
                                            <option value="{{$i}}" @if($hotel_array['star']==$i)selected @endif>{{$i}} star</option>
                                            @endfor
                                            @else
                                            @for($i=5;$i>=1;$i--)
                                            <option value="{{$i}}">{{$i}} star</option>
                                            @endfor
                                            @endif
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-3">

                                    <div class="form-group">
                                        <label for="hotel_type">Hotel Type</label>
                                        <select id="hotel_type" name="hotel_type" class="form-control select2" style="width: 100%;">
                                             <option value="0">ANY HOTEL TYPE</option>
                                                @foreach($fetch_hotel_type as $hotel_type)
                                                <option value="{{$hotel_type->hotel_type_id}}">{{$hotel_type->hotel_type_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                   <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                              <input type="hidden" id="offset" value="0">
                                           <button id="search_hotel" class="btn btn-rounded  btn-success" style="display: block;margin: 20px auto !important;">Search</button>
                                    </div>
                                </div>
                    
                            </div>
                           
                        </div>
                        <div class="row">
                            <div class="col-6">
                            </div>
                            <div class="col-6" >
                                </div>
                        </div>
                          

                            <div id="hotel_div">

                            </div>
                            <div class="text-center table-activity-loader" style="display: none">
                              <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                              viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                              <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                                <animateTransform 
                                attributeName="transform" 
                                dur="1s" 
                                type="translate" 
                                values="0 15 ; 0 -15; 0 15" 
                                repeatCount="indefinite" 
                                begin="0.1"/>
                            </circle>
                            <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                                <animateTransform 
                                attributeName="transform" 
                                dur="1s" 
                                type="translate" 
                                values="0 10 ; 0 -10; 0 10" 
                                repeatCount="indefinite" 
                                begin="0.2"/>
                            </circle>
                            <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                                <animateTransform 
                                attributeName="transform" 
                                dur="1s" 
                                type="translate" 
                                values="0 5 ; 0 -5; 0 5" 
                                repeatCount="indefinite" 
                                begin="0.3"/>
                            </circle>
                                    </svg>
                                </div>
                                 <button class="btn btn-primary more_hotel" id="show_more_hotel">Show More Results</button>
                                            <button class="btn btn-primary more_hotel " id="no_more_hotel" disabled="disabled">No More Results</button>



                            </div>



                        </div>

                    </div>

                </div>
                      
          </div>
      </div>
   </div>

</div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
@if(!empty($hotel_array))
<script>
   $(document).ready(function()
    {
        $("#search_hotel").trigger("click");



    });  
</script>
@endif
<script>
    var comp=0;
    $(document).ready(function()
    {

    $('.select2').select2();
    var date = new Date();
        date.setDate(date.getDate());
      $('#hotel_date_from').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#hotel_date_from").datepicker("getDate");
        var date_to = $("#hotel_date_to").datepicker("getDate");

        if(!date_to)
        {
            date_from.setDate(date_from.getDate()+1); 
            $('#hotel_date_to').datepicker("setDate",date_from);
        }
        else if(date_to<date_from)
        {
             date_from.setDate(date_from.getDate()+1); 
            $('#hotel_date_to').datepicker("setDate",date_from);
        }
    });

    $('#hotel_date_to').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#hotel_date_from").datepicker("getDate");
        var date_to = $("#hotel_date_to").datepicker("getDate");

        if(!date_from)
        {
            $('#hotel_date_from').datepicker("setDate",date_to);
        }
        else if(date_to<date_from)
        {
            $('#hotel_date_from').datepicker("setDate",date_to);
        }
    });

});

    $(document).on("keyup input change",".select2-search__field",function() {
            var val = $(this).val();
            
            $.ajax({
                url:'{{route("fetchHotelNames")}}',
                data :{"search_value":val},
                type:"GET",
                success:function(response)
                {
                    
                    $("#hotel_name").html(response);
                }
            });
        });



     $(document).on("change","#hotel_country",function()
    {
       if($("#hotel_country").val()!="0")
       {
        var country_id=$(this).val();
        $.ajax({
            url:"{{route('search-country-cities')}}",
            type:"GET",
            data:{"country_id":country_id},
            success:function(response)
            {

                $("#hotel_city").html(response);
                $('#hotel_city').select2();
                
            }

        });
    }

});

        //Hotel search filters

    $(document).on("click","#search_hotel",function()
    {
          $("#show_more_hotel").hide();
                $("#no_more_hotel").hide();
                     $("#offset").val(0);
        var country_id=$("#hotel_country").val();
        var city_id=$("#hotel_city").val();
        var hotel_name=$("#hotel_name").val();
        var date_from=$("#hotel_date_from").val();
        var date_to=$("#hotel_date_to").val();
        var room_qty=$("#room_quantity").val();

        var adults=$("#no_of_adults").val();
        var children=$("#no_of_child").val();
        var child_age=$("input[name='child_age[]']").val();
        var child_age = [];

        // Initializing array with Checkbox checked values
        $("input[name='child_age[]']").each(function(){
            child_age.push(this.value);
        });

        var rooms=$("#no_of_rooms").val();

        var hotel_rating=$("#hotel_rating").val();

        var hotel_type=$("#hotel_type").val();
         var offset=$("#offset").val();

        if(date_from==date_to)
        {
            alert("Checkin and Checkout dates cannot be same");
        }
        else
        {

            if(country_id!="0" && city_id!="0" && room_qty!="0")
            {
                $("#hotel_div").html("");
                $(".table-activity-loader").show();
                $.ajax({
                    url:"{{route('fetchHotels')}}",
                    data:{"country_id":country_id,
                    "city_id":city_id,
                    "hotel_name":hotel_name,
                    "date_from":date_from,
                    "date_to":date_to,
                    "room_qty":room_qty,
                    "hotel_rating":hotel_rating,
                    "hotel_type":hotel_type,
                    "adults":adults,
                    "children":children,
                    "child_age":child_age,
                    "rooms":rooms,
                      "offset":offset
                },
                type:"GET",
                success:function(response)
                {
                    $("#hotel_div").html(response);
                    $(".table-activity-loader").hide();
                      $("#offset").val(parseInt($("#offset").val()+1));
                    if(response.indexOf("slides")!=-1)
                    {
                        $("#show_more_hotel").show();
                     $("#no_more_hotel").hide();

                 }
                 else
                 {
                    $("#show_more_hotel").hide();
                    $("#no_more_hotel").show();

                }
                }

            });
            }
        }

    });

    $(document).on("click","#show_more_hotel",function()
    {
    
        var country_id=$("#hotel_country").val();
        var city_id=$("#hotel_city").val();
        var hotel_name=$("#hotel_name").val();
        var date_from=$("#hotel_date_from").val();
        var date_to=$("#hotel_date_to").val();
        var room_qty=$("#room_quantity").val();

        var adults=$("#no_of_adults").val();
        var children=$("#no_of_child").val();
        var child_age=$("input[name='child_age[]']").val();
        var child_age = [];

        // Initializing array with Checkbox checked values
        $("input[name='child_age[]']").each(function(){
            child_age.push(this.value);
        });

        var rooms=$("#no_of_rooms").val();

        var hotel_rating=$("#hotel_rating").val();

        var hotel_type=$("#hotel_type").val();
         var offset=$("#offset").val();

        if(date_from==date_to)
        {
            alert("Checkin and Checkout dates cannot be same");
        }
        else
        {

            if(country_id!="0" && city_id!="0" && room_qty!="0")
            {
                // $("#hotel_div").html("");
                $(".table-activity-loader").show();
                $.ajax({
                    url:"{{route('fetchHotels')}}",
                    data:{"country_id":country_id,
                    "city_id":city_id,
                    "hotel_name":hotel_name,
                    "date_from":date_from,
                    "date_to":date_to,
                    "room_qty":room_qty,
                    "hotel_rating":hotel_rating,
                    "hotel_type":hotel_type,
                    "adults":adults,
                    "children":children,
                    "child_age":child_age,
                    "rooms":rooms,
                      "offset":offset
                },
                type:"GET",
                success:function(response)
                {
                    $("#hotel_div").append(response);
                    $(".table-activity-loader").hide();
                      $("#offset").val(parseInt($("#offset").val()+1));
                    if(response.indexOf("slides")!=-1)
                    {
                        $("#show_more_hotel").show();
                     $("#no_more_hotel").hide();

                 }
                 else
                 {
                    $("#show_more_hotel").hide();
                    $("#no_more_hotel").show();

                }
                }

            });
            }
        }

    });
    $(document).on("click","#room_quantity",function()
    {
        $("#rooms_main_div").show();
    });
    $(document.body).click(function(e){
        if (!$(e.target).closest('#rooms_main_div').length) {
            $("#rooms_main_div").hide();
        }
    });

    $(document).on("change","#no_of_adults,#no_of_child,#no_of_rooms",function()
    {
        var no_of_adults=$("#no_of_adults").val();
        var no_of_child=$("#no_of_child").val();
        var no_of_rooms=$("#no_of_rooms").val();

        var guest_data="";
        if(no_of_adults>1)
            guest_data+=no_of_adults+ " adults-";
        else
        guest_data+=no_of_adults+ " adult-";

        if(no_of_child>1)
            guest_data+=no_of_child+ " children-";
        else
            guest_data+=no_of_child+ " child-";

        if(no_of_rooms>1)
            guest_data+=no_of_rooms+ " rooms";
        else
            guest_data+=no_of_rooms+ " room";
        
        $("#room_quantity").val(guest_data);
    });

       // $(document).on("change","#room_quantity",function()
       //  {
       //      var room_quantity=$(this).val();
       //      var show_rooms="";
       //      for($i=1;$i<=room_quantity;$i++)
       //      {
       //          show_rooms+='<div class="row room_div" id="room_div'+$i+'"><div class="col-md-2"><div class="form-group"><label for="">Room - '+$i+'</label><input type="hidden" name="rooms['+$i+']" value="'+$i+'" id="rooms_'+$i+'" class="rooms_quantity"><select name="tax-tour-adult-quantity['+$i+'][]" id="adult_quantity_'+$i+'" class="adult_quantity form-control"><option value="">Adults</option><option>1</option><option>2</option><option>3</option><option>4</option></select></div></div><div class="col-md-2" ><div class="form-group"><label for="">&nbsp;</label><select name="tax-tour-child-quantity['+$i+'][]" id="child_quantity_'+$i+'" class="child_quantity form-control"><option value="">Child</option><option>0</option><option>1</option><option>2</option><option>3</option><option>4</option></select></div></div></div>';
       //      }

       //      $('#rooms_main_div').html(show_rooms);
       //  })

           $(document).on("change","#no_of_child",function()
{
    var html_data="";
    var child_count=$(this).val();
    for($i=1;$i<=child_count;$i++)
    {
        html_data+='<div class="col-md-6"> <div class="form-group"><label for="child_age'+$i+'" style="color:black">Child Age '+$i+' <span class="asterisk" >*</span></label><input type="text" id="child_age'+$i+'" name="child_age[]"  onkeypress="javascript:return validateNumber(event)" maxlength=2  class="form-control child_age" style="color:black" required></div></div>'
    }
    $("#child_age_div").html(html_data);
    $("#child_age_div").show();
});
</script>

<script>

// var value;
// var checkin;
// var checkout;
// var no_of_child;
// var childdiv;
// var childArr;
// var object;
// var country;
// $(document).ready(function(){

   

//     var city=$("#hotel_city");
//     if(localStorage.getItem("city")){
//         value=localStorage.getItem("city");
//         $("#hotel_country").trigger("change");
//     }
//     if(localStorage.getItem("checkin")){
//         checkin=localStorage.getItem("checkin");
//         $("#hotel_country").trigger("change");
//     }
//     if(localStorage.getItem("checkout")){
//         checkout=localStorage.getItem("checkout");
//         $("#hotel_country").trigger("change");
//     }
//     if(localStorage.getItem("object")){
//         object=localStorage.getItem("object");
        
//     }
//     if(localStorage.getItem("country")){
//         country=localStorage.getItem("country");
        
//     }
//     city.change(function(){
//     if(value!==null){
//         localStorage.setItem("city",$(this).val())
//     }
   
//     $("#hotel_country").change(function(){

//     localStorage.setItem("country",$(this).val())


// })



// })

// $("#hotel_date_from").change(function(){
//     localStorage.setItem("checkin",$(this).val())
// })
// $("#no_of_child").change(function(){
//     no_of_child=$(this).val()
// })
// $("#search_hotel").click(function(){
//         childdiv= $("#child_age_div").html()
//         childArr={
//             "no_of_child":no_of_child,
//             "childdiv":childdiv
//             }
//             localStorage.setItem("object",JSON.stringify(childArr))
// })

// var country_id=country;
//         $.ajax({
//             url:"{{route('search-country-cities')}}",
//             type:"GET",
//             data:{"country_id":country_id},
//             success:function(response)
//             {

//                 $("#hotel_city").html(response);
//                 $('#hotel_city').select2();
//                 $("#hotel_country").val(country).trigger("click");
//                 $("#hotel_city").val(value).trigger("change");

//                 if(value!==null){
//      $("#hotel_date_from").val(checkin).trigger("change");
//     $("#hotel_date_to").val(checkout).trigger("change");
//     }
   
//     var o=JSON.parse(object)
//     $("#no_of_child").val(o.no_of_child)
    
   
//     $("#child_age_div").append(o.childdiv)
//     $(".child_age").keyup(function() {
//         this.setAttribute("value",$(this).val())
// });
//             }

//         });
// $("#hotel_date_to").change(function(){
//    localStorage.setItem("checkout",$(this).val())
//    })
// })




</script>

<script>
    $(document).ready(function() {
			$('.minus').click(function () {
				var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
               if($input.attr('name')=="no_of_child")
               {
                count = count < 0 ? 0 : count;
               }
               else
               {
                count = count < 1 ? 1 : count;
               }
				
				$input.val(count);
				$input.change();
				return false;
			});
			$('.plus').click(function () {
				var $input = $(this).parent().find('input');
				$input.val(parseInt($input.val()) + 1);
				$input.change();
				return false;
			});
		});
    </script>
</body>
</html>
