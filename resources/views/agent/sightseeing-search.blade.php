
@include('agent.includes.top-header')
<style>
    div.overlay {
        position: absolute;
        width: 100%;
        bottom: 0;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #5d4ecbbd !important;
        border-top-right-radius: 0 !important;
    }
    div.overlay-2 {
        position: absolute;
        width: 100%;
        top: 0;
        opacity: 0;
        bottom: 0;
        z-index: 99;
        transition: .5s ease;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #000000bd !important;
        border-top-right-radius: 0 !important;
    }
    .img-div:hover div.overlay-2 {
        transition: .5s ease;
        opacity: 1;
    }
    .img-div:hover .ribbon2 {
        display: none;
    }
    .img-div:hover::before,
    .img-div:hover::after {
        border: none
    }
    .left {
        float: left;
        width: 50%;
    }
    p.from {
        color: white;
        text-align: right;
        padding: 5px 10px 0;
        font-size: 18px;
        margin: 0;
    }
    span.price-span {
        display: block;
        color: white;
        text-align: right;
        padding: 0 10px;
    }
    img.h-img {
        width: 100%;
    }
    p.h-head {
        color: white;
        font-size: 15px;
        padding: 5px 10px;
        margin: 0;
        z-index: 9999;
        position: absolute;
        top: 0;
        left: 0;
        color: white;
        background: #E91E63;
        width: 100%;
    }
    span.s-span {
        color: white;
        padding: 0 10px;
    }
    .img-div {
        border: 1px solid #756fcc;
        padding: 2px;
        border-radius: 5px;
        position: relative;
        z-index: 9;
        margin-bottom: 42px;
        transition: .5s ease;
    }
/*  .img-div:before {
content: "";
position: absolute;
width: 9px;
height: 10px;
border-width: 7px;
border-style: solid;
border-color: transparent #402c70 transparent transparent;
top: 56px;
left: -14px;

z-index: 0;
}
.img-div:after {
content: "";
position: absolute;
width: 9px;
height: 10px;
border-width: 7px;
border-style: solid;
border-color: transparent #402c7000 #402c70 transparent;
top: -14px;
left: 58px;

z-index: -37;
} */
.ribbon2:before {
    content: "";
    position: absolute;
    top: 0;
    padding: 10px;
    /* margin: 5px; */
    left: 0;
    width: 100px;
    height: 20px;
    background: #5225c5;
    transform: rotate(135deg) translate(28px, 1px);
}
.ribbon2 {
    position: absolute;
    top: -7px;
    left: -7px;
    width: 100px;
    height: 100px;
    /* position: absolute; */
    overflow: hidden;
    /* background: aliceblue; */
}
button.hover-btn {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border: none;
    background: #ffffff;
    color: #5225c5;
    padding: 7px 15px;
    cursor: pointer;
    border-radius: 5px;
}
.selection-div {
    border: 1px solid gainsboro;
    padding: 0;
    border-radius: 5px;
    margin-bottom: 33px !IMPORTANT;
    background: url(https://sf2.mariefranceasia.com/wp-content/uploads/sites/7/2018/02/bawah-615x410.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}
.box-body {
    padding: 1.5rem 1.5rem;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    border-radius: 5px;
    background-color: #f2f3f8;
    padding-left: 0;
    padding-right: 0;
}

.selection-div >.row.mb-10 {
    position: relative;
    background: #2727e07a;
    top: 0;
    left: 14px;
    width: 100%;
    padding-bottom: 26px;
    padding-top: 26px;
    height: 100%;
    border-radius: 5px;
    margin-bottom: 0 !important;
}
.select2-container--default .select2-selection--single {
    border: 1px solid #ddd;
  border-radius: 22px;
   /* padding: 6px 12px;*/
    /*height: 34px;*/
    color: white !important;
    background: #dcdada7d !important;
}

.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #fff ;
    line-height: 28px;
}
.form-group label {
    color: #ffffff !important;
}
.input-group.date input {
    background: transparent !important;
    border-color: white;
    border-width: 2px;
    color: white;
    border-radius: 25px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    padding-left: 10px;
}
.input-group.date .input-group-addon {
    border: 1px solid white;
    border-top-right-radius: 25px;
    border-bottom-right-radius: 25px;
    background-color: #fff;
    border-left: 0px;
}
.selection-div button {
    background: #f44336  !important;
   /* border: 2px solid red !important;*/
    /* color: red !important; */
    min-width: 120px;
    padding: 5px !important;
    margin-top: 2px;
}
@media screen and (max-width:1170px) {
    p.h-head {
        color: white;
        font-size: 15px;
        padding: 0 10px;
        margin: 0;
    }
    p.from {
        color: white;
        text-align: right;
        padding: 0px 10px 0;
        font-size: 15px;
        margin: 0;
    }
    span.s-span {
        color: white;
        padding: 0 10px;
        font-size: 11px;
    }
    span.price-span {
        display: block;
        color: white;
        text-align: right;
        padding: 0 10px;
        font-size: 12px;
    }
}
.table-sightseeing-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
}
.carousel-item img {
    height: 100%;
}
.carousel-item {
    height: 100%;
}
p.start_price {
    margin: 0;
}
p.country_name.ng-binding {
    font-size: 20px;
    margin: 0;
}
.book_card {
    /* padding: 15px; */
    background: #fefeff;
    box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
    margin-bottom: 50px;
    border-radius: 5px;
}
.hotel_detail {
    height: 150px;
}
a.moredetail.ng-scope {
    background: gainsboro;
    padding: 7px 10px;
    /* margin-bottom: 10px; */
}
.booking_label {
    background: #5d53ce;
    border-top-left-radius: 5px;
    border-top-right-radius: 5px;
    color: white;
    padding: 10px 15px;
}
.booking_detail {
    padding: 15px;
}
a.btn.btn-outline.btn-circle.book_btn1 {
    background: #E91E63;
    border-radius: 5px !IMPORTANT;
    padding: 5px 20px;
    width: auto !important;
    height: auto;
    line-height: 2;
    color: white;
}
td p {
    margin: 0;
}
td {
    background: gainsboro;
}
table {
    border-collapse: separate;
}
.panel-group .panel-heading+.panel-collapse>.panel-body {
    border-top: 1px solid #59d25a;
}
a.panel-title {
    position: relative !important;
    background: #dfffe3;
    color: green !important;
    padding: 13px 20px 13px 85px !important;
    /* border-bottom: 1px solid #3ca23d; */
}
.panel-title {
    display: block;
    margin-top: 0;
    margin-bottom: 0;
    padding: 1.25rem;
    font-size: 18px;
    color: #4d4d4d;
    height: 48px;
}
.panel-group .panel-heading+.panel-collapse>.panel-body {
    border-top: none !important;
}
.panel-body {
    background: white;
    /* border: 1px solid #59d25a; */
    padding: 10px !important;
}
a.panel-title:before {
    content: attr(title) !important;
    position: absolute !important;
    top: 0px !important;
    opacity: 1 !important;
    left: 0 !important;
    padding: 12px 10px;
    width: auto;
    max-width: 250px;
    text-align: center;
    color: white;
    font-family: inherit !important;
    height: 48px;
    background: #279628;
    z-index: 999;
    transform: none !important;
}
.tab-content {
    margin-top: 10px;
}
div.panel-heading {
    border: 1px solid #59d25a !important;
}
.panel {
    border-top: none !important;
    margin-bottom: 5px !important;
}
div#carousel-example-generic-captions {
    width: 100%;
}
/*daman css*/
.hotel-div {
    width:75%;
    float:left;
    padding: 15px;
    display: flex;
    background: white;
}
.hotel-list-div {
    clear: both;
    display: block;
    border: 1px solid #c6bee6;
    border-radius: 5px;
    position: relative;
    /* clear: both; */
    height: 190px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.3);

}
.hotel-img-div {

    width: 35% !important;
    float:left;
}
.hotel-details {
    float:left;
    width: 60%;
    position: relative;
    padding: 0 15px;
}
p.sight-type {
    position: absolute;
    top: 8px;
    right: -8%;
    background: #ffffff;
    color: #5cb660;
    border: 1px solid #5cb660;
    padding: 4px 10px;
    border-radius: 8px;
    min-width: 99px;
    width: auto;
    text-align: center;
}
.hotel-info-div {
    width: 25%;
    float: left;
    padding: 15px;
    background: #ccffce;
    text-align: right;
    height: 100%;
    position: relative;
}
.checked {
    color: orange;
}
/*span.hotel-s {
    border: 1px solid #F44336;
    padding: 2px 5px;
    display: inline-block;
    color: #f44336;
    font-size: 12px;
    margin: 0 10px 0 0;
}*/
span.hotel-s {
    border: 1px solid #F44336;
    padding: 4px 7px;
    display: inline-block;
    color: #F44336;
    font-size: 12px;
    margin: 0 10px 0 0;
    /*background-color: #f44336;*/
    /*border-radius: 8px;*/
    margin-top: 10px;
}
p.hotel-name {
    font-size: 20px;
    margin: 10px 0 0;
    color: #4CAF50;
    /* float: left; */
    text-align: left;
    font-weight: bold;
    max-width: 80%;
}
.rate-no {
    float: right;
}
.rate-no {
    float: right;
    display: block;
    margin-top: 14px;
    background: #2d3134;
    color: white;
    padding: 1px 8px;
    font-size: 12px;
    border-radius: 5px;
}
.heading-div {
    clear: both;
    
}
.rating {
    display: block;
    float: left;
    list-style: none;
    margin: 0;
    padding: 0;
}
p.r-number {
    float: right;
}
p.time-info {
    color: #4CAF50;
}
p.info {
    clear: both;
    margin: 0;
}
span.tag-item {
    background: #ffcbcd;
    padding: 5px 10px;
    border-radius: 5px;
    color: #ff4e54;
}
.inclusions {
    margin: 20px 0;
}
span.inclusion-item {
    padding: 10px 10px 0 0;
    color: #644ac9;
}
img.icon-i {
    width: auto;
    height: 20px;
}
p.include-p {
    color: black;
}
.inclusion-p {
    color: green
}
p.price-p span {
    background: #f44336;
    color: white;
    padding: 3px 5px 3px 9px;
    border-radius: 5px;
    position: relative;
    z-index: 9999;
    border-top-right-radius: 5px;
    border-bottom-right-radius: 5px;
    margin-left: 20px;
    border-bottom-left-radius: 4px;
    font-size: 12px;
}
p.price-p span:before {
    content: "";
    width: 15px;
    height: 14.5px;
    background: #F44336;
    position: absolute;
    transform: rotate(45deg);
    top: 3.5px;
    left: -6px;
    border-radius: 0px 0px 0px 3px;
    z-index: -1;
}
.theme-rosegold .btn-success {
    border-color: #F44336;
    color: #ffffff;
}
p.price-p span:after {
    content: "";
    background: white;
    width: 4px;
    height: 4px;
    position: absolute;
    top: 50%;
    left: 0px;
    transform: translateY(-50%);
    border-radius: 50%;
}
p.price-p {
    color: #ee2128;
    text-align: right;
    margin-right: 5px;
}
p.tax {
    font-size: 12px;
    margin: 0;
}
p.days {
    font-size: 12px;
    margin: 0;
}
p.offer {
    font-size: 21px;
    color: black;
    font-weight: bold;
    margin: 7px;
}

p.cut-price {
    margin: 0;
    text-decoration: line-through;
    font-size: 14px;
    padding-right: 5px;
    font-family: Verdana;
}
.login-a {
    color: #0088ff;
    font-size: 15px;
    font-weight: bold;
    margin-top: 10px;
    display: block;
}
@media screen and (max-width:1200px){
    p.hotel-name {
        font-size: 17px;
        margin: 22px 0 0;
        color: black;
        float: left;
    }
    p.r-number {
        float: right;
        font-size: 12px;
    }
    span.inclusion-item {
        padding: 10px 10px 0 0;
        color: #644ac9;
        display: block;
    }
    p.cut-price {
        margin: 0;
        text-decoration: line-through;
        font-size: 15px;
    }
    p.offer {
        font-size: 24px;
        color: black;
        font-weight: bold;
        margin: 0;
    }
    button.book-btn {
    background: #5cb660;
    border: none;
    border-radius: 50px !important;
    margin-top: 70px !important;
    padding: 6px 20px !important;
    text-align: center !important;
    color: white !important;
    width: auto !important;
    cursor: pointer;
}
    .login-a {
        color: #0088ff;
        font-size: 13px;
        font-weight: bold;
        margin-top: 10px;
        display: block;
    }
}
@media screen and (max-width:1200px){
    .hotel-div {
        width: 75%;
        float: left;
        padding: 15px;
        display: flex;
        background: white;
    }
    .hotel-list-div {
    width: 94% !important;
    margin: auto;
    border: 1px solid gainsboro;
}
    span.inclusion-item {
        padding: 10px 10px 0 0;
        color: #644ac9;
        display: inline;
    }
    .hotel-details {
    float: left;
    width: 60%;
    padding: 0;
}
}


@media screen and (max-width:992px){
    p.sight-type {
    position: absolute;
    top: 74%;
    right: 0%;
    background: #ffffff;
    color: #5cb660;
    border: 1px solid #5cb660;
    padding: 4px 10px;
    border-radius: 8px;
    min-width: 99px;
    width: auto;
    text-align: center;
}
    .hotel-info-div {
    width: 100%;
    float: none;
    display: flex;
    padding: 15px;
    background: #c0ffc2;
    flex-wrap: wrap;
    text-align: center;
    height: 101%;
}
.price-div {
    flex: 0 0 50%;
}
p.price-p {
    color: #ee2128;
    text-align: left;
    margin-right: 5px;
}
p.cut-price {
    margin: 0;
    text-decoration: line-through;
    font-size: 15px;
    flex: 0 0 50%;
    text-align: right;
}
    .selection-div >.row.mb-10{
        padding-right: 30px;
        padding-left: 30px;
    }
    p.hotel-name {
        font-size: 17px;
        margin: 22px 0 0;
        color: black;
        float: none;
    }
    .rate-no {
        float: none;
        display: inline;
        margin-top: 14px;
        background: #2d3134;
        color: white;
        padding: 1px 8px;
        font-size: 12px;
        border-radius: 5px;
    }
    .rating {
        display: block;
        float: none;
        list-style: none;
        margin: 0;
        padding: 0;
    }
    p.r-number {
        float: none;
        font-size: 12px;
    }
    .hotel-details {
        float: none !important;
        width: 100% !important;
        padding: 0 15px;
        margin-top: 20px;
    }
    .hotel-list-div {
        display: block;
        border: 1px solid #c6bee6;
        border-radius: 5px;
        position: relative;
        /* clear: both; */
        height: auto;
    }
    .hotel-div {
        width: 100%;
        float: none;
        padding: 15px;
        display: block;
        background: white;
    }
    /*.hotel-img-div {
        width: 100% !important;
        float: none !important;
        display: block !important;
    }*/
    .hotel-img-div {
    width: 100% !important;
    float: left;
}
.img-slide {
    height: 290px !important;
    width: 100% !important;
    clip-path: none !important;
    object-fit: cover;
    margin-bottom: 20px;
}
.hotel-info-div {
    width: 100%;
    float: none;
    padding: 15px;
    background: #ccffce;
    text-align: right;
    height: auto;
    position: relative;
    bottom: -6px;
    border-radius: 5px;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
    a.flex-prev,a.flex-next {
        display: none;
    }
    span.inclusion-item {
        padding: 10px 10px 0 0;
        color: #644ac9;
        display: block;
    }
}
.img-slide{
    height: 157px;
    width: 95%;
    clip-path: polygon(0% 0%, 75% 0%, 100% 50%, 75% 100%, 0% 100%);
}
.flex-control-thumbs {
    margin: 5px 0 0;
    position: static;
    overflow: hidden;
    width: 100%;
    height: 50px;
}
.form-group label{
    color: #673AB7;
}
.theme-rosegold .select2-container--default .select2-results__option--highlighted[aria-selected] {
    background-color: #f44336 !important;
}
.theme-rosegold .select2-container--default .select2-search--dropdown .select2-search__field {
    border-color: #f44336 !important;
}
.input-group.date input {
    background: #dcdada7d !important;
    border-color: white;
    border: 1px solid #ddd;
    color: white;
    border-right: 0px;
    border-radius: 22px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    /*padding-left: 10px;*/
}
.theme-rosegold .btn-success:hover, .theme-rosegold .btn-success:active, .theme-rosegold .btn-success:focus, .theme-rosegold .btn-success.active{
    background-color: #e05247fa !important;
    border-color: #e05247fa !important;
    color: #ffffff;
}
.box {
    position: relative;
    border-top: 0;
    margin-bottom: 30px;
    width: 100%;
    background-color: #ffffff;
    border-radius: 5px;
    padding: 0px;
    -webkit-transition: .5s;
    transition: .5s;
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
    box-shadow: 0 10px 23px -5px rgba(0, 0, 0, 0);
}
button.book-btn {
background: #5cb660 !important;
border: none;
border-radius: 5px !important;
/*margin-top: 70px;*/
/* padding: 10px 20px; */
text-align: center;
color: #fff;
width: 160px;
cursor: pointer;
position: absolute;
bottom: 15px;
right: 22px;
/*left: 0;*/
margin: 0 auto;
}
@media (max-width: 800px) {
    .flexslider2 { width: 100%; }

  
    .hotel-info-div {
    width: 100%;
    float: none;
    display: flex;
    padding: 15px;
    background: #c0ffc2;
    flex-wrap: wrap;
    text-align: center;
    height: 101%;
}

}

.more_sightseeing
{
    margin: 5% 0%;
    display: none;
}

#sightseeing_div
{
  padding: 15px 25px;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('agent.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('agent.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">SightSeeing</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item active" aria-current="page">Home
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="box">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="box">
                                            <div class="box-body">
                                                <div class="selection-div">
                                                    <div class="row mb-10">
                                                        <div class="col-sm-5 col-md-6 col-lg-4">
                                                            <div class="form-group">
                                                                <label for="sightseeing_country">COUNTRY <span class="asterisk">*</span></label>
                                                                <select id="sightseeing_country" name="sightseeing_country" class="form-control select2" style="width: 100%;">
                                                                    <option selected="selected" value="0">SELECT COUNTRY</option>
                                                                    @foreach($countries as $country)
                                                                    <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-4">
                                                            <div class="form-group">
                                                                <label for="sightseeing_city">CITY <span class="asterisk">*</span></label>
                                                                <select id="sightseeing_city" name="sightseeing_city" class="form-control select2" style="width: 100%;">
                                                                    <option selected="selected" value="0">SELECT CITY</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-4">
                                                            <div class="form-group">
                                                                <label for="date_from">SELECT DATE <span class="asterisk">*</span></label>
                                                                <div class="input-group date">
                                                                    <input type="text" placeholder="FROM"
                                                                    class="form-control pull-right datepicker" id="date_from" name="date_from" readonly="readonly">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-5 col-md-6 col-lg-4">
                                                            <div class="form-group">
                                                                <label for="sightseeing_tour_type">Tour Type <span class="asterisk">*</span></label>
                                                                <select id="sightseeing_tour_type" name="sightseeing_tour_type" class="form-control select2" style="width: 100%;">
                                                                   <option value="0">SELECT ALL</option>
                                                                    @foreach($fetch_tour_type as $tour_type)
                                                                    <option value="{{$tour_type->tour_type_id}}">{{$tour_type->tour_type_name}}</option>
                                                                    @endforeach
                                                                   
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-12 col-lg-4" style="display: none">
                                                            <div class="form-group">
                                                                <label for="date_to">TO DATE <span class="asterisk">*</span></label>
                                                                <div class="input-group date">
                                                                    <input type="text" placeholder="TO"
                                                                    class="form-control pull-right datepicker" id="date_to" name="date_to" readonly="readonly">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-sm-12 col-md-12 col-lg-4">
                                                            <div class="form-group">
                                                                <br>
                                                                <input type="hidden" id="offset" value="0">
                                                                <button id="search_sightseeing" class="btn btn-rounded  btn-success">Search</button>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="sightseeing_div">
                                            </div>
                                            <div class="text-center table-sightseeing-loader" style="display: none">
                                                <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                                                <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                                                    <animateTransform
                                                    attributeName="transform"
                                                    dur="1s"
                                                    type="translate"
                                                    values="0 15 ; 0 -15; 0 15"
                                                    repeatCount="indefinite"
                                                    begin="0.1"/>
                                                </circle>
                                                <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                                                    <animateTransform
                                                    attributeName="transform"
                                                    dur="1s"
                                                    type="translate"
                                                    values="0 10 ; 0 -10; 0 10"
                                                    repeatCount="indefinite"
                                                    begin="0.2"/>
                                                </circle>
                                                <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                                                    <animateTransform
                                                    attributeName="transform"
                                                    dur="1s"
                                                    type="translate"
                                                    values="0 5 ; 0 -5; 0 5"
                                                    repeatCount="indefinite"
                                                    begin="0.3"/>
                                                </circle>
                                            </svg>
                                        </div>
                                            <button class="btn btn-primary more_sightseeing " id="show_more_sightseeing">Show More Results</button>
                                            <button class="btn btn-primary more_sightseeing " id="no_more_sightseeing" disabled="disabled">No More Results</button>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('agent.includes.footer')
@include('agent.includes.bottom-footer')
<script>
    $(document).ready(function()
    {
        $('.select2').select2();
        var date = new Date();
        date.setDate(date.getDate());
        $('#date_from').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        }).on('changeDate', function (e) {
            var date_from = $("#date_from").datepicker("getDate");
            var date_to = $("#date_to").datepicker("getDate");
            if(!date_to)
            {
                $('#date_to').datepicker("setDate",date_from);
            }
            else if(date_to<date_from)
            {
                $('#date_to').datepicker("setDate",date_from);
            }
        });
        $('#date_to').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        }).on('changeDate', function (e) {
            var date_from = $("#date_from").datepicker("getDate");
            var date_to = $("#date_to").datepicker("getDate");
            if(!date_from)
            {
                $('#date_from').datepicker("setDate",date_to);
            }
            else if(date_to<date_from)
            {
                $('#date_from').datepicker("setDate",date_to);
            }
        });
        $(document).on("change","#sightseeing_country",function()
        {
            if($("#sightseeing_country").val()!="0")
            {
                var country_id=$(this).val();
                $.ajax({
                    url:"{{route('search-country-cities')}}",
                    type:"GET",
                    data:{"country_id":country_id},
                    success:function(response)
                    {
                        $("#sightseeing_city").html(response);
                        $('#sightseeing_city').select2();
                    }
                });
            }
        });
                //sightseeing search filters
                $(document).on("click","#search_sightseeing",function()
                {
                     $("#show_more_sightseeing").hide();
                $("#no_more_sightseeing").hide();
                     $("#offset").val(0);
                    var country_id=$("#sightseeing_country").val();
                    var city_id=$("#sightseeing_city").val();
                    var date_from=$("#date_from").val();
                     var offset=$("#offset").val();
                     var sightseeing_tour_type=$("#sightseeing_tour_type").val();
                    var error=[];

                 if(country_id == "0" ||  country_id== "SELECT COUNTRY")
                {
                    error.push("Please Select Country");
                }

                if(city_id == "0" || city_id == "SELECT CITY")
                {

                     error.push("Please Select City");

                }

                if(date_from == "")
                {

                     error.push("Please Select From Date");

                }
                if(error.length>0)
                {
                   alert(error.join(" , "));  
                }

          if (country_id != "0" && city_id != "0" && date_from!="") 
          {
                    $("#sightseeing_div").html("");
                    $(".table-sightseeing-loader").show();
                    $.ajax({
                        url:"{{route('fetchSightseeing')}}",
                        data:{"country_id":country_id,
                        "city_id":city_id,
                        "date_from":date_from,
                        "tour_type":sightseeing_tour_type,
                        "offset":offset
            },
            type:"GET",
            success:function(response)
            {
                $("#sightseeing_div").html(response);
                $(".table-sightseeing-loader").hide();
                $("#offset").val(parseInt($("#offset").val()+1));

                if(response.indexOf("slides")!=-1)
                {
                   $("#show_more_sightseeing").show();
                   $("#no_more_sightseeing").hide();

               }
               else
               {
                $("#show_more_sightseeing").hide();
                $("#no_more_sightseeing").show();

            }
               
            }
        });
                }
            });
            });

    $(document).on("click","#show_more_sightseeing",function()
    {
          var country_id=$("#sightseeing_country").val();
                    var city_id=$("#sightseeing_city").val();
                    var date_from=$("#date_from").val();
                     var offset=$("#offset").val();
                     var sightseeing_tour_type=$("#sightseeing_tour_type").val();
                   
                // var date_to=$("#date_to").val();
                if(country_id!="0" && city_id!="0")
                {
                    // $("#sightseeing_div").html("");
                    $(".table-sightseeing-loader").show();
                    $.ajax({
                        url:"{{route('fetchSightseeing')}}",
                        data:{"country_id":country_id,
                        "city_id":city_id,
                        "date_from":date_from,
                        "tour_type":sightseeing_tour_type,
                        "offset":offset
            },
            type:"GET",
            success:function(response)
            {
                $("#sightseeing_div").append(response);
                $(".table-sightseeing-loader").hide();
                $("#offset").val(parseInt($("#offset").val()+1));

                if(response.indexOf("slides")!=-1)
                {
                   $("#show_more_sightseeing").show();
                   $("#no_more_sightseeing").hide();

               }
               else
               {
                $("#show_more_sightseeing").hide();
                $("#no_more_sightseeing").show();

            }
               
            }
        });
                }
    });

        </script>
    </body>
    </html>