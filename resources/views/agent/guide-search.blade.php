@include('agent.includes.top-header')

<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  position:relative;
}
.title {
    color: grey;
}
p.price-p {
    margin-top: 50px;
    margin-bottom: 0;
    font-size: 16px;
    color: #ff8f00;
    background: #ffdeb3;
    padding: 5px 15px;
    display: inline-block;
    border-radius: 4px;
    text-align: left;
}
button.button-guide {
    background: #ffffff00;
    border: none;
    color: white;
    padding:5px 6px 0;
    display: block;
    width: 100%;
    border-radius: 5px;
    font-size: 17px;
    cursor: pointer;
}
button.button-guide:focus {
    outline: 1px dotted;
    outline: none;
}
h2.g-name {
    position: absolute;
    top: 49.5%;
    left: 0;
    transform: translate(0%, -50%);
    background: black;
    color: white;
    padding: 10px 20px;
    width: auto;
    font-size: 17px;
    text-align: center;
    border-top-right-radius: 10px;
    border-bottom-right-radius: 10px;
}
span.g-badge {
    background: #ffc8c7;
    padding: 5px 10px;
    margin-right: 10px;
    color: #ee1044;
    display: inline-block;
    margin-top: 10px;
    border-radius: 3px;
}
button.button-guide:hover {
    background: #ffffff00;
    border: none;
    color: white;
    padding: 5px 6px 0px;
    display: block;
    width: 100%;
    border-radius: 5px;
    font-size: 17px;
    cursor: pointer;
}
a.g-link {
    display: block !important;
    width: 100%;
    margin-bottom:0px;
}
.table-activity-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
}

.form-group label{
    color: #fff;
}
/*------------ start 1-29-2020---------*/
.selection-div {
    border: 1px solid gainsboro;
    padding: 0;
    border-radius: 5px;
    margin-bottom: 33px !IMPORTANT;
    background: url(https://sf2.mariefranceasia.com/wp-content/uploads/sites/7/2018/02/bawah-615x410.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}
.selection-div >.row.mb-10 {
    position: relative;
    background: #2727e07a;
    top: 0;
    left: 14px;
    width: 100%;
    height: 100%;
    bottom: 0;
    padding-top: 26px;
    padding-bottom: 26px;
    border-radius: 5px;
    margin-bottom: 0 !important;
}
.theme-rosegold .btn-success {
    border-color: #F44336;
    background-color: #F44336;
    color: #ffffff;
}
.select2-container--default .select2-selection--single {
    border: 1px solid #ddd;
    border-radius: 22px;
    color: white !important;
    background: #dcdada7d !important;
}

.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #fff ;
    line-height: 28px;
}
.input-group.date input {
    background: #dcdada7d !important;
    border-color: white;
    border-width: 1px;
    color: white;
    border-radius: 25px;
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    padding-left: 10px;
}
.theme-rosegold .btn-success:hover, .theme-rosegold .btn-success:active, .theme-rosegold .btn-success:focus, .theme-rosegold .btn-success.active{
    background-color: #F44336 !important;
    border-color: #F44336 !important;
    color: #fff;
}


.profile {
    background: -webkit-linear-gradient(45deg, #B06AB3, #4568DC);
    background: linear-gradient(45deg, #B06AB3, #4568DC);
    width: 100%;
    height: 240px;
    border-radius: 24px;
    transition: all .5s ease;
    border: none;
    box-shadow: 0 37.125px 70px -12.125px rgba(0,0,0,0.2);
}



select#language_filter ~ span .selection span.select2-selection.select2-selection--single {
    border: none;
    border-radius: 22px;
    padding: 8px;
    height: auto;
    color: #4c68d9 !important;
    background: #e8e8e8 !important;
}
select#language_filter ~ span .selection span.select2-selection.select2-selection--single span{
    color: #a06ab9;
    line-height: 28px;
    font-size: 15px;
    font-weight: 500;
}
.card-header.profileNamer {
    display: -webkit-box;
    display: flex;
    flex-direction: column;
    -webkit-box-pack: justify;
    justify-content: space-between;
    -webkit-box-align: center;
    align-items: center;
    padding: 1.5rem 1.5rem;
    background-color: transparent;
    border-bottom: 1px solid rgba(0, 0, 0, 0.07);
}
.profileName {
  background:transparent;
  border:0;
  text-align:center;
  font-size:16px;
  font-weight:800;
  color:#eee;
}

.isVerified {
  fill: #eee;
  margin:5px;
}

.card-body {
  color:#eee;
  margin-top:0;
}
.card-body.profileBody {
    padding-top: 0;
    padding-bottom: 0;
}
.avatar {
  border-radius:50% !important;
 
  transform: scale(0.95) !important;
  transition:all .5s !important;
  cursor:pointer !important;
}
.profileInfo p {
    font-size: 15px;
    color: white;
}
.avatar:hover {
  transform: scale(1);
  box-shadow: 0 37.125px 70px -12.125px rgba(0,0,0,0.2);
}
.profilePic {
  text-align:center;
}


.isOnline {
    text-align: center;
    color: #FF9800;
    background: #000000;
    padding: 2px 10px;
    border-radius: 20px;
    /* font-weight: unset; */
}
.profileInfo {
  margin-top:1em;
  font-weight:200;
  font-size:12px;
  color:#eee;
  transition: all .5s ease;
  text-align:center;
}

.actions {
    background: #00000052;
    border: 0;
    padding: 0;
    color: #fff;
    display: flex;
    border-bottom-left-radius: 25px !IMPORTANT;
    border-bottom-right-radius: 25px !important;
}
.actionFirst, .actionSecond {
  width:50%;
  text-align:center;
  fill:#ddd;
  transform: scale(0.8);
  transition:all .3s;
  cursor:pointer;
  position: relative;
}

.title {
    color: grey;
    margin: 5px 0;
    position: absolute;
    bottom: 42px;
    transition: all .5s ease;
    height: 0;
    overflow: hidden;
    opacity: 0;
    text-align: left;
}
p.skill-a {
    margin: 0;
    font-size: 17px;
    transition: all .5s ease;
    padding: 7px 4px 4px;
}
p.skill-a:hover ~ p.title {
    height: auto;
    transition: all .5s ease;
    opacity: 1;
}
.avatar {
    position: relative;
    display: block;
    width: 120px !important;
    height: 120px !important;
    margin: auto;
    border-radius: 50% !important;
    background-color: #f0f0f0;
    color: #999999;
    text-transform: uppercase;
}
/*----------- 30-1-2020---------*/
.input-group .input-group-addon {
    
    border-color: #fff;
    background-color: #dcdada7d;
}
.form-control + .input-group-addon:not(:first-child) {
    border-radius: 0px 17px 17px 0px !important;
    border-left: 0;
}
.input-group > .form-control:not(:last-child), .input-group > .custom-select:not(:last-child) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
    border-right: 0px;
}
    </style>

    <body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

        <div class="wrapper">

            @include('agent.includes.top-nav')

            <div class="content-wrapper">

                <div class="container-full clearfix position-relative"> 

                    @include('agent.includes.nav')

                    <div class="content">



                        <div class="content-header">

                            <div class="d-flex align-items-center">

                                <div class="mr-auto">

                                    <h3 class="page-title">Guides</h3>

                                    <div class="d-inline-block align-items-center">

                                        <nav>

                                            <ol class="breadcrumb">

                                                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                                                <li class="breadcrumb-item active" aria-current="page">Home

                                                </li>

                                            </ol>

                                        </nav>

                                    </div>

                                </div>

        </div>

    </div>


    <div class="row">
      <div class="box">
          <div class="box-body">

             <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                        <div class="selection-div">
                         <div class="row mb-10">

                            <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="guide_country">COUNTRY <span class="asterisk">*</span></label>
                                    <select id="guide_country" name="guide_country" class="form-control select2" style="width: 100%;">
                                        <option selected="selected">SELECT COUNTRY</option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-2">

                                <div class="form-group">
                                    <label for="guide_city">CITY <span class="asterisk">*</span></label>
                                    <select id="guide_city" name="guide_city" class="form-control select2" style="width: 100%;">
                                        <option selected="selected">SELECT CITY</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-2">

                                <div class="form-group">
                                    <label for="date_from">FROM DATE <span class="asterisk">*</span></label>
                                    <div class="input-group date">
                                        <input type="text" placeholder="FROM"
                                        class="form-control pull-right datepicker" id="date_from" name="date_from" readonly="readonly">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->

                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-lg-2">

                                <div class="form-group">
                                   <label for="date_to">TO DATE <span class="asterisk">*</span></label>
                                   <div class="input-group date">
                                    <input type="text" placeholder="TO"
                                    class="form-control pull-right datepicker" id="date_to" name="date_to" readonly="readonly">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->

                            </div>
                        </div>
                          <div class="col-sm-12 col-md-12 col-lg-2">

                                <div class="form-group">
                                    <br>
                                    <button id="search_guide" class="btn btn-rounded  btn-success">Search</button>

                                </div>
                                <!-- /.input group -->

                            </div>

                    </div>
                </div>
                    <div class="row" id="language_filter_div" style="display: none">

                        <div class="col-sm-12 col-md-12" style="text-align:center">
                            <div class="form-group pull-right" style="width:200px">
                                 <select id="language_filter" name="language_filter" class="form-control select2" style="width: 100%;">
                                        <option selected="selected" value="all">All Languages</option>

                                        @foreach($languages as $language)
                                        <option value="{{$language->language_name}}">{{$language->language_name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        
                    </div>

                    <div id="guide_div">

                     {{--  <div class="row">
                       @foreach($get_guides as $guides)
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="card">
                                <img src="https://www.w3schools.com/w3images/team1.jpg" alt="Jane" style="width:100%">
                                <div class="container g-body">
                                    <h2 class="g-name">{{ucwords($guides->guide_first_name." ".$guides->guide_last_name)}}</h2>
                                    <div style="text-align:right">
                                        <p class="price-p">{{$guides->guide_price_per_day}}</p>
                                    </div>
                                    <p class="title">

                                        @php
                                        $languages_data=explode(",",$guides->guide_language);
                                        @endphp 

                                        @foreach($languages as $language)
                                        @if(in_array($language->language_id,$languages_data))
                                        <span
                                        class="g-badge">{{$language->language_name}}</span>
                                        @endif
                                        @endforeach
                                    </p>
                                        <p>{{$guides->guide_description}}</p>

                                        <a href="guide-detail.php" class="g-link"><button class="button-guide">Book Now</button></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                           
                            </div>--}}
                        </div>
                        <div id="no_guide_shown" style="display: none" class="text-center">
                            <p class="alert alert-danger">No Guide found matching this language. <br> Please choose another option in language filter</p>
                        </div>
                    <div class="text-center table-activity-loader" style="display: none">
                      <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                      <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 15 ; 0 -15; 0 15" 
                        repeatCount="indefinite" 
                        begin="0.1"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 10 ; 0 -10; 0 10" 
                        repeatCount="indefinite" 
                        begin="0.2"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 5 ; 0 -5; 0 5" 
                        repeatCount="indefinite" 
                        begin="0.3"/>
                    </circle>
                </svg>
            </div>

    </div>
</div>
</div>
</div>





</div>
</div>
</div>

</div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
<script>
    $(document).ready(function()
    {
        $('.select2').select2();
        var date = new Date();
        date.setDate(date.getDate());
        $('#date_from').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        }).on('changeDate', function (e) {
            var date_from = $("#date_from").datepicker("getDate");
            var date_to = $("#date_to").datepicker("getDate");

            if(!date_to)
            {
                $('#date_to').datepicker("setDate",date_from);
            }
            else if(date_to<date_from)
            {
                $('#date_to').datepicker("setDate",date_from);
            }
        });

        $('#date_to').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        }).on('changeDate', function (e) {
            var date_from = $("#date_from").datepicker("getDate");
            var date_to = $("#date_to").datepicker("getDate");

            if(!date_from)
            {
                $('#date_from').datepicker("setDate",date_to);
            }
            else if(date_to<date_from)
            {
                $('#date_from').datepicker("setDate",date_to);
            }
        });

        $(document).on("change","#guide_country",function()
        {
         if($("#guide_country").val()!="0")
         {
            var country_id=$(this).val();
            $.ajax({
                url:"{{route('search-country-cities')}}",
                type:"GET",
                data:{"country_id":country_id},
                success:function(response)
                {

                    $("#guide_city").html(response);
                    $('#guide_city').select2();

                }
            });
        }

    });

    //Activity search filters

    $(document).on("click","#search_guide",function()
    {
        var country_id=$("#guide_country").val();
        var city_id=$("#guide_city").val();
        var date_from=$("#date_from").val();
          var date_to=$("#date_to").val();
           var error=[];

                if(country_id == "0" || country_id== "SELECT COUNTRY")
                {
                    error.push("Please Select Country");
                }

                if(city_id == "0" || city_id == "SELECT CITY")
                {

                     error.push("Please Select City");

                }

                if(date_from == "")
                {

                     error.push("Please Select From Date");

                }
                 if(date_to == "")
                {

                     error.push("Please Select To Date");

                }
                if(error.length>0)
                {
                   alert(error.join(" , "));  
                }

          if (country_id != "0" && city_id != "0" && date_from!="" && date_to!="") 
          {
            $("#guide_div").html("");
            $(".table-activity-loader").show();
            $.ajax({
                url:"{{route('fetchGuides')}}",
                data:{"country_id":country_id,
                "city_id":city_id,
                "date_from":date_from,
                "date_to":date_to
            },
            type:"GET",
            success:function(response)
            {
                $("#guide_div").html(response);
                $(".table-activity-loader").hide();
                $("#language_filter_div").show();
            }

        })
        }


    });
    
    });
</script>

<script>
    $(document).on("change","#language_filter",function()
    {
        var language=$(this).val();
        var total_guide_count= $(".guides_rows").length;
        var total_hide_count=0;
       $(".guides_rows").each(function()
         {   

            if(language=="all")
            {
            $(this).show(1000);
            }
            else
            {
                 if($(this).find('span.'+language).length !== 0)
          {  
              $(this).show(600);
          }
          else
          {
            $(this).hide();
            total_hide_count++;
          }
            }
           

     });

       if(total_guide_count==total_hide_count)
        $("#no_guide_shown").show();
     else
        $("#no_guide_shown").hide();
    });
   
</script>


</body>
</html>
