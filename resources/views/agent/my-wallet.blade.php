<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('agent.includes.top-header')
<style>
    .text-green
    {
        color:green;
    }
    .text-red
    {
        color:red;
    }
    .page-item.active .page-link {
    margin: 8px 0px;
}
  .page-item.disabled .page-link {
    margin: 8px 0px;
}
svg
{
    height:30px;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('agent.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('agent.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Agent's Wallet</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item active" aria-current="page">Agent's Wallet</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row mb-40">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">My Wallet</h4>
                                </div>
                                <div class="box-body">
                                    <div class="row mb-40">
                                        <div class="col-md-3">
                                           <strong>Wallet's Balance</strong>
                                             <h3>
                                            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 0 167.748 167.748" style="enable-background:new 0 0 167.748 167.748;" xml:space="preserve">
                                            <g>
                                                <g>
                                                    <path style="fill:#ec407a;" d="M151.383,49.067h-4.791v61.178c0,5.549-4.499,11.916-10.06,14.201l-105.22,43.302h120.071
                                                        c5.561,0,10.06-4.511,10.06-10.06V59.115C161.443,53.578,156.944,49.067,151.383,49.067z"/>
                                                    <path style="fill:#ec407a;" d="M131.597,0.819L16.365,48.243c-5.561,2.285-10.06,8.652-10.06,14.195v98.567
                                                        c0,5.543,4.499,8.204,10.06,5.919L131.597,119.5c5.561-2.285,10.06-8.646,10.06-14.201V6.738
                                                        C141.657,1.2,137.158-1.467,131.597,0.819z M121.226,69.497c-4.069,1.36-7.351-0.853-7.351-4.911c0-4.069,3.288-8.449,7.351-9.804
                                                        c4.063-1.354,7.351,0.853,7.351,4.923C128.578,63.769,125.278,68.149,121.226,69.497z"/>
                                                </g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            <g>
                                            </g>
                                            </svg>
                                            GEL {{$total_amount}}</h3>
                                        </div>
                                         <div class="col-md-3">
                                           @if($withdaw_yes==1)
                                          
                                            <strong>Withdrawal</strong>
                                            <br>
                                            <button id="withdraw_funds" class="btn btn-primary"  @if($total_amount_withdraw<=0) disabled @endif>Withdraw Funds</button>
                                           
                                            @endif
                                        </div>

                                    </div>
                                    <div class="row mt-10">
                                        <div class="col-md-3">
                                            <input type="hidden" id="total_amount_withdraw" value="{{$total_amount_withdraw}}">
                                           <strong>Total Credited Till Date</strong>
                                             <h5>GEL {{$total_amount_credited_all}} </h5>
                                    </div>
                                     <div class="col-md-3">
                                           <strong>Total Debited Till Date</strong>
                                             <h5>GEL {{$total_amount_withdrawn_all}} </h5>
                                    </div>
                                        <div class="col-md-3">
                                           <strong>Total Credited This Month</strong>
                                             <h5>GEL {{$total_amount_credited_month}} </h5>
                                    </div>
                                     <div class="col-md-3">
                                           <strong>Total Debited This Month</strong>
                                             <h5>GEL {{$total_amount_withdrawn_month}} </h5>
                                    </div>
                                </div>
                                    <div class="row mt-20">
                                        <div class="col-md-12">
                                            <div class="table-responsive" id="commission_data">
                                              <table class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    <th>Sr. no</th>
                                                    <th>Description</th>
                                                    <th>Amount</th>
                                                    
                                                     <th>Transaction Datetime</th>
                                                  
                                                    <th>Approved/Rejected At</th>
                                                    <th>Remarks (if any)</th>
                                                      <th>Status</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                    @if(count($get_wallet)<=0)
                                                    <tr>
                                                        <td colspan=6 class="text-center"> No Transactions Found</td>
                                                    </tr>
                                                    @endif
                                                  @foreach($get_wallet as $wallet)
                                                  <tr>
                                                    <td>@isset($_GET['page'])
                                                        {{((10*($_GET['page']-1))+$loop->iteration)}}
                                                        @else
                                                        {{$loop->iteration}}
                                                    @endisset</td>
                                                    <td>{{$wallet->age_wallet_remarks}}</td>
                                                    <td>
                                                        @if($wallet->age_wallet_credit_amount!=null)
                                                        <span class="text-green">+ GEL {{$wallet->age_wallet_credit_amount}}</span>
                                                        @elseif($wallet->age_wallet_debit_amount!=null)
                                                        <span class="text-red">- GEL {{$wallet->age_wallet_debit_amount}}</span>
                                                        @endif
                                                    </td>
                                                 
                                                      <td>{{date('d/m/Y h:ia',strtotime($wallet->age_wallet_date." ".$wallet->age_wallet_time))}}</td>
                                                       <td>@if($wallet->age_wallet_approve_reject_at!="") {{date('d/m/Y h:ia',strtotime($wallet->age_wallet_approve_reject_at))}} @else - @endif</td>
                                                      <td>@if($wallet->age_wallet_approve_reject_remarks!="") {{$wallet->age_wallet_approve_reject_remarks}} @else - @endif</td>
                                                      <td>
                                                         @if($wallet->age_wallet_status==1)
                                                         <span class="text-green"><i class="fa fa-check"></i> Completed</span>
                                                         @elseif($wallet->age_wallet_status==2)
                                                             <span class="text-danger"><i class="fa fa-times"></i> Rejected</span>
                                                         @elseif($wallet->age_wallet_status==0)
                                                        <span class="text-warning"><i class="fa fa-clock-o"></i> Pending</span>
                                                         @endif
                                                      
                                                      </td>
                                                  </tr>

                                                  @endforeach

                                                </tbody>
                                              </table>
                                            </div>
                                            <p>Total Transactions : {{$get_wallet->total()}}</p>
                                            <div class="pagination">
                                            {!! $get_wallet->appends(request()->query())->render() !!}
                                        </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                
                                </div>
                            </div>
                        </div>
                        @include('agent.includes.footer')
                        @include('agent.includes.bottom-footer')
                         <div class="modal" id="withdrawModal">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Withdrawal</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><strong>Total Withdrawable Amount </strong>
                                            <br> GEL {{$total_amount_withdraw}}</p>
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong>Pending Withdrawable Amount </strong>
                                            <br> GEL {{($total_amount-$total_amount_withdraw)}}</p>
                                    </div>
                                </div>
                               <div class="row mt-20 mb-20">
                                   <div class="col-md-3">
                                       <label>Withdrawal Amount</label>
                                   </div>
                                   <div class="col-md-6">
                                       <div class="form-group">
                                            <input type="text" name="withdraw_amount" id="withdraw_amount" placeholder="Enter Amount" class="form-control"  onkeypress="javascript:return validateNumber(event)"  onpaste="javascript:return validateNumber(event)">
                                            <span style="color:red" id="withdraw_amount_error"></span>

                                             <span style="color:green" id="withdraw_amount_success"></span>
                                       </div>
                                   </div>
                               </div>
                             </div>
                            <div class="col-md-12">
                                <button type="button" id="sendRequestButton" class="btn btn-sm btn-primary pull-right">Send Request</button>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

                    </body>
                    </html>