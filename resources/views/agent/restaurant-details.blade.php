<?php
use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\LoginController;
?>
@include('agent.includes.top-header')
<style>
 .hr-total {
  display: block;
  width: 94%;
  background: #eeeeee;
  margin: 10px auto;
} 
.Tab-para
{
  font-size: 21px;
  display: flex;
  justify-content: flex-start;
  align-items: end;
  color: #5c6975;
  font-style: normal;
  font-weight: 400;
  float:left;
}
.trad
{
 position: absolute;
 top: 0;
 right: 13px;
 background: #ffffff;
 color: #5cb660;
 border: 1px solid #5cb660;
 padding: 4px 10px;
 border-radius: 8px;
 min-width: 99px;
 width: auto;
 text-align: center;
}
p.address {
  margin-bottom: 25px;
}
.slider-img
{
  max-width:100%;
  height:400px;
}
.star-p {
  font-size: 18px;
  color: #d23d64;
}
.star1
{
  color: white;
  background: orange;
  padding: 5px;
  border-radius: 50%;
}
.decription-section ul li
{
 line-height: 24px;
}
.decription-section
{
  margin-top: 19px;
  padding: 10px 20px;
}
.first ul
{
  margin-top:12px;
  color: #5c6975;
  font-style: normal;
  font-weight: 400;
}
.price-block
{
  border: 3px dashed #F44336;
  padding: 10px 15px;
  text-align: center;
  margin-bottom: 20px;
}
.price-gel
{
  color: #f44336;
  font-size: 15px;
  line-height: 1;
  font-weight: 600;
  text-align: right;
  position: relative;
}
.price-1
{
  font-size: 15px;
  color: #3F51B5;
}
.price-2
{
  font-size: 15px;
  color: #3F51B5;
}
.price-total
{
  font-size: 20px;
  color: #000000;
}
.total-gel
{
  color: #f44336;
  font-size: 23px;
  text-align: right;
  font-weight: 600;
  position: relative;
}
.form-group label {
  font-weight: 500;
  font-size: 12px;
}
span.asterisk {
  color: red !important;
}
#select_date
{
  border-radius: 0;
  border-color: gainsboro;
  background: white;
}
.input-group-addon {
  border-color: gainsboro !important;
  border-radius: 0 !important;
  background: gainsboro !important;
  font-weight: 300;
  padding: .425rem .75rem;
  border: 1px solid #9e9e9e;
  line-height: 1.25;
  color: #2f363c;
  text-align: center;
  margin-bottom: 0;
  font-size: 1rem;
}
.book-btn
{
  background-color: #ec407a;
  border-color: #ec407a;
  color: #ffffff;
  border-radius: 60px;
  line-height: inherit;
  padding: 8px 16px;
  border:none;
}
.person
{
  font-size:15px;
}
.form1
{
  padding:5px 5px;
}
.collapse-btn
{
 width: 100%;
 text-align: left;
 border-radius: 0px;
 /* background: gray; */
 border: none;
 color:white;
 background:#ec407a;
 cursor:pointer;
 padding:10px 10px;
 font-weight:600;
}
.sight-seeing
{
  padding: 10px 10px;
  margin-top: 16px;
}
.btn-heading
{
  font-family: 'Roboto Condensed',sans-serif;
  font-size: 16px;
  text-transform: uppercase;
  font-weight:600;
}
.cat-text
{
  color: #000;
  padding-right: 4px;
}
.break-text
{
  color: #e53932;
}
.para-text
{
  font-size: 14px;
  line-height: 20px;
  color: #888;;
}
.rate
{
  color: #e53932;
  font-size:18px;
}
.col-div
{
  padding:20px 10px;
}
.book-card
{
  background: #fefeff;
  box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
  margin-bottom: 50px;
  border-radius: 5px;
  width: 100%;
}
.row-border
{
  border-color: gray;
  border-bottom: 1px dashed;
  padding: 14px 10px;
}
.fur-p
{
  font-family: 'Roboto Condensed',sans-serif;
  font-size: 16px;
  text-transform: uppercase;
  padding-bottom: 9px;
}
.right-class
{
  position: absolute;
  right: 26px;
}
.fixed {
  position: fixed;
  top: 0;
  left: 0;
  width: 100%; 
}
/*.carousel-inner>.carousel-item>img, .carousel-inner>.carousel-item>a>img {
        display: block;
        height: 550px;
        max-width: 100%;
        line-height: 1;
        margin:auto;
        width: 100%; // Add this
        }*/
        .flexslider2 {
          width: 100%;
          height: auto;
        }
        li.flex-active-slide {
          height: 500px;
        }
        .flexslider2 .slides img {
          width: auto;
          height: 100%;
          margin: auto;
          display: block;
        }
        .flex-control-thumbs img{
          opacity:1;
        }
        .flex-viewport {
          height: 500px;
          background: rgb(14, 14, 14);
        }
        .flex-silder-div .flex-viewport {
          height: 300px;
        }

        .flex-viewport {
          max-height: 100%;
          -webkit-transition: all 1s ease;
          -moz-transition: all 1s ease;
          -ms-transition: all 1s ease;
          -o-transition: all 1s ease;
          transition: all 1s ease;
        }

    /*ul.slides li img {
        height: 100% !important;
        width: 100% !important;
        }*/

        .flex-control-thumbs img {
          width: 100%;
          height: 100%;
          display: block;
          opacity: .7;
          cursor: pointer;
          -moz-user-select: none;
          -webkit-transition: all 1s ease;
          -moz-transition: all 1s ease;
          -ms-transition: all 1s ease;
          -o-transition: all 1s ease;
          transition: all 1s ease;
        }
        .flex-control-thumbs li {
          width: 13% !important;
          float: left !important;
          margin: 2.4px !important;
          height: 52px !important;

        }

        ol.flex-control-nav.flex-control-thumbs {
          height: 100px;
        }
        .flexslider2 a.flex-prev {
          color: white;
          z-index: 98999;
          background: black;
          overflow: visible;
          left: 0 !important;
          position: absolute;
          opacity: 1;
          text-shadow: none;
          box-shadow: none;
        }
        .flexslider2 a.flex-next{
          color: white;
          z-index: 98999;
          background: black;
          overflow: visible;
          right: 42px !important;
          position: absolute;
          opacity: 1;
          text-shadow: none;
          box-shadow: none;
        }

        ul.flex-direction-nav {
          position: absolute;
          z-index: 999;
          opacity: 1;
          visibility: visible;
          top: 40%;
          transform: translateY(-50%);
          width: 100%;
          height: 10px;
        }
        .flex-direction-nav a:before {
          font-family: "flexslider-icon";
          font-size: 29px !important;
          display: inline-block !important;
          content: \f001;
          color: rgba(255, 255, 255, 0.8) !important;
        }
          ul.slides {
            /* width: 100% !important; */
            height: 100% !important;
          }

          li.flex-active-slide {
            height: 100%;
          }
        </style>
        <body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">



          <div class="wrapper">



            @include('agent.includes.top-nav')



            <div class="content-wrapper">



              <div class="container-full clearfix position-relative">	



                @include('agent.includes.nav')



                <div class="content">







                  <div class="content-header">



                    <div class="d-flex align-items-center">



                      <div class="mr-auto">



                        <h3 class="page-title">Restaurant Details</h3>



                        <div class="d-inline-block align-items-center">



                          <nav>



                            <ol class="breadcrumb">



                              <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>



                              <li class="breadcrumb-item" aria-current="page">Home</li>



                              <li class="breadcrumb-item active" aria-current="page">Restaurant Details



                              </li>



                            </ol>



                          </nav>



                        </div>



                      </div>


                    </div>



                  </div>


  <form id="restaurant_booking_form" method="post"
            action="{{route('restaurant-booking')}}">
            {{csrf_field()}}
            <input type="hidden" name="restaurant_id" value="{{$get_restaurant->restaurant_id}}">
             <input type="hidden" name="supplier_id" value="{{$get_restaurant->supplier_id}}">
                  <div class="row">
                    <div class="col-12">
                      <div class="box">
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <p class="Tab-para">{{ $get_restaurant->restaurant_name}}</p>
                              <p class="trad">{{$get_restaurant->getRestaurantType->restaurant_type_name}}</p>
                            </div>
                            <div class="col-md-4">
                              <p class="address">
                                <i class="fa fa-map-marker"></i>
                                ({{ $get_restaurant->restaurant_address}} )   
                              </p>
                            </div>

                            <div class="col-md-4">
                              <i class="fa fa-clock-o"></i>
                              {{ $get_restaurant->validity_fromtime}} - {{ $get_restaurant->validity_totime}}
                            </div>
                            <div class="col-md-4">

                            </div>
                            @php
                            $get_restaurant_images=unserialize($get_restaurant->restaurant_images);
                            @endphp
                            <div class="row">
                              <div class="col-md-8">
                                <div class="row">
                                  <div class="col-lg-12 col-md-12">
                                    <div class="box">
                                      <div class="box-body" style="padding:0">
                                    <div class="flexslider2">
                                      <ul class="slides">
                                        @if($get_restaurant->restaurant_images!=null && $get_restaurant->restaurant_images!="")
                                        @php 
                                        $get_restaurant_images=unserialize($get_restaurant->restaurant_images);
                                        @endphp

                                        @if(isset($get_restaurant_images[0]))
                                        @foreach($get_restaurant_images as $restaurant_image)
                                        <li
                                        data-thumb='{{asset("assets/uploads/restaurant_images")}}/{{$restaurant_image}}'>
                                        <img src='{{asset("assets/uploads/restaurant_images")}}/{{$restaurant_image}}'
                                        alt="slide" />
                                      </li>
                                      @endforeach
                                      @else
                                      <li
                                      data-thumb='{{asset("assets/images/no-photo.png")}}'>
                                      <img src='{{asset("assets/images/no-photo.png")}}' alt="slide" />
                                    </li>


                                    @endif

                                    @else
                                    <li data-thumb='{{asset("assets/images/no-photo.png")}}'>
                                      <img src='{{asset("assets/images/no-photo.png")}}' alt="slide" />
                                    </li>
                                    @endif
                                  </ul>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>





                        <div class="row">
                         <div class="col-md-12">
                           <section class="decription-section">
                             <p class="star-p">
                               <i class="fa fa-star star1"></i>&nbsp;Restaurant Description
                             </p>
                           <!-- <ul class="first-ul">
                            <li><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p></li>
                            <li><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p></li>
                            <li><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p></li>

                            <li><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p></li>

                            <li><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p></li>

                            <li><p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p></li>


                          </ul> -->
                          <?= $get_restaurant->restaurant_description ?>
                        </section>
                      </div>
                    </div>
                    <hr style="border-color: #ffc0c0;width: 100%;">

                    <div class="sight-seeing">
                     <p class="star-p">
                       <i class="fa fa-star star1"></i>&nbsp;Restaurant Menu
                     </p>
                     <div class="row">
                      <?php
                      $food_counter=0;
                      ?>
                      @foreach($get_menu_categories as $menu_category_key =>$menu_category_value)
                      <?php
                      $get_menu_foods=App\RestaurantFood::where('food_status',1)->where('food_approval_status',1)->where('restaurant_id_fk',$get_restaurant->restaurant_id)->where('menu_category_id_fk',$menu_category_value->restaurant_menu_category_id)->get();

                      if(count($get_menu_foods)>0)
                      {
                        ?>
                        <div class="col-md-12 mt-1">
                          <button type="button" class="collapse-btn" data-toggle="collapse" data-target="#{{strtolower(str_replace(' ','_',$menu_category_value->restaurant_menu_category_name))}}_{{$menu_category_value->restaurant_menu_category_id}}">{{$menu_category_value->restaurant_menu_category_name}}<span class="right-class"><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                          <div id="{{strtolower(str_replace(' ','_',$menu_category_value->restaurant_menu_category_name))}}_{{$menu_category_value->restaurant_menu_category_id}}" class="collapse col-div">
                            @foreach($get_menu_foods as $menu_food_key=>$menu_foods)
                            <input type="hidden" name="food_id[{{$food_counter}}]"  id="food_id__{{$food_counter}}" value="{{$menu_foods->restaurant_food_id}}">
                            <input type="hidden" name="food_category_id[{{$food_counter}}]"  id="food_category_id__{{$food_counter}}" value="{{$menu_category_value->restaurant_menu_category_id}}">
                             <input type="hidden" name="food_name[{{$food_counter}}]" id="food_name__{{$food_counter}}"  value="{{$menu_foods->food_name}}">
                            <div class="row row-border">
                               @php 
                              $get_food_images=unserialize($menu_foods->food_images);
                              @endphp

                              @if($menu_foods->food_images!=null && $menu_foods->food_images!="" && count($get_food_images)>0)
                              <div class="col-md-3">
                                <img src='{{asset("assets/uploads/food_images")}}/{{$get_food_images[0]}}'>
                             </div>
                             <div class="col-md-7">
                               @else
                               <div class="col-md-10">
                                 @endif
                                 
                                  <p class="btn-heading">{{$menu_foods->food_name}}</p>
                                  <span class="cat-text">Category:</span>&nbsp;<span class="break-text">{{$menu_category_value->restaurant_menu_category_name}}</span>
                                  <p class="para-text"><b>Ingredients : </b>{{$menu_foods->food_ingredients}}</p>
                                </div>

                                <div class="col-md-2">
                                  @php
                                  if($menu_foods->food_discounted_price!=null && $menu_foods->food_discounted_price<=$menu_foods->food_price)
                                  {
                                    $price=$menu_foods->food_discounted_price;
                                  }
                                  else
                                  {
                                    $price=$menu_foods->food_price;
                                  }
                                   $markup_cost=round($price*$markup)/100;
                                    $total_agent_cost=round($price+$markup_cost);
                                    $own_markup_cost=round(($total_agent_cost*$own_markup)/100);
                                    $total_cost=round($total_agent_cost+$own_markup_cost);

                                    @endphp
                                 
                                     <p class="rate">GEL {{$total_cost}}</p>
                                      <input type="hidden" name="food_supplier_price[{{$food_counter}}]" id="food_supplier_price__{{$food_counter}}"  value="{{$price}}">
                                       <input type="hidden" name="food_agent_price[{{$food_counter}}]" id="food_agent_price__{{$food_counter}}"  value="{{$total_agent_cost}}">
                                   <input type="hidden" name="food_price[{{$food_counter}}]" id="food_price__{{$food_counter}}"  value="{{$total_cost}}">
  
                                  <div class="form-group">
                                     <input type="hidden" name="food_unit[{{$food_counter}}]" id="food_unit__{{$food_counter}}"  value="{{$menu_foods->food_unit}}">
                                    <select class="form-control food_qty" name="food_qty[{{$food_counter}}]" id="food_qty__{{$food_counter}}">
                                      <option value="0">Quantity</option>
                                      @for($op_count=1;$op_count<=50;$op_count++)
                                      <option value="{{$op_count}}">{{$op_count}}</option>
                                      @endfor
                                    </select> / {{$menu_foods->food_unit}}
                                  </div>
                                </div>
                              </div>
                              @php
                              $food_counter++;
                              @endphp
                              @endforeach

                            </div>
                          </div>
                          <?php 
                        } 
                        ?>
                        @endforeach
                      <!--  <div class="col-md-12 mt-1">
                         <button type="button" class="collapse-btn" data-toggle="collapse" data-target="#demo">Breakfast<span class="right-class"><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                         <div id="demo" class="collapse col-div">

                          <div class="row row-border">
                            <div class="col-md-3">
                              <img src="http://crm.traveldoor.ge/dev-custom/assets/images/food.jpg">
                            </div>
                            <div class="col-md-8">
                              <p class="btn-heading">ANAOLATION BREAKFAST</p>
                              <span class="cat-text">Category:</span>&nbsp;<span class="break-text">Breakfast</span>
                              <p class="para-text">With choice of: Eggs with sujuk or menemen for your choice, cheese plate (sulguni, feta, kasar), tomato
                                and cucumber salad,tahin-pekmez, turkish adjika (walnut sauce), fried halloumi cheese, olives, butter, 
                              jam, dried fruits, honey, simit, bread busket, turkish tea (unlimited)&nbsp;</p>
                            </div>

                            <div class="col-md-1">
                              <p class="rate">70.00</p>
                              <div class="form-group">
                                <select class="form-control" id="sel1">
                                  <option>1</option>
                                  <option>2</option>
                                  <option>3</option>
                                  <option>4</option>
                                </select>
                              </div>
                            </div>
                          </div>
                          <div class="row row-border">
                            <div class="col-md-3">
                              <img src="http://crm.traveldoor.ge/dev-custom/assets/images/food.jpg">
                            </div>
                            <div class="col-md-8">
                              <h3 class="btn-heading">ANAOLATION BREAKFAST</h3>
                              <span class="cat-text">Category:</span>&nbsp;<span class="break-text">Breakfast</span>
                              <p class="para-text">With choice of: Eggs with sujuk or menemen for your choice, cheese plate (sulguni, feta, kasar), tomato
                                and cucumber salad,tahin-pekmez, turkish adjika (walnut sauce), fried halloumi cheese, olives, butter, 
                              jam, dried fruits, honey, simit, bread busket, turkish tea (unlimited)&nbsp;</p>
                            </div>

                            <div class="col-md-1">
                              <p class="rate">70.00</p>
                            </div>
                          </div>
                          <div class="row row-border">
                            <div class="col-md-4">
                              <p class="fur-p"> 3 egg omlet</p>
                              <div class="span-tag-div">
                                <span class="cat-text">Category:</span><span class="break-text">Breakfast</span>
                              </div>
                              <p class="para-text">With the option of: cheese, ham, mushroom, pepper, tomato</p>
                            </div>
                            <div class="col-md-7"></div>
                            <div class="col-md-1">
                              <p class="rate">70.00</p>
                            </div>
                          </div>

                        </div>
                      </div>


                      <div class="col-md-12 mt-1">
                       <button type="button" class="collapse-btn" data-toggle="collapse" data-target="#demo1">Dinner<span class="right-class"><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                       <div id="demo1" class="collapse col-div">

                        <div class="row row-border">
                          <div class="col-md-3">
                            <img src="http://crm.traveldoor.ge/dev-custom/assets/images/food.jpg">
                          </div>
                          <div class="col-md-8">
                            <p class="btn-heading">ANAOLATION BREAKFAST</p>
                            <span class="cat-text">Category:</span>&nbsp;<span class="break-text">Breakfast</span>
                            <p class="para-text">With choice of: Eggs with sujuk or menemen for your choice, cheese plate (sulguni, feta, kasar), tomato
                              and cucumber salad,tahin-pekmez, turkish adjika (walnut sauce), fried halloumi cheese, olives, butter, 
                            jam, dried fruits, honey, simit, bread busket, turkish tea (unlimited)&nbsp;</p>
                          </div>

                          <div class="col-md-1">
                            <p class="rate">70.00</p>
                          </div>
                        </div>
                        <div class="row row-border">
                          <div class="col-md-3">
                            <img src="http://crm.traveldoor.ge/dev-custom/assets/images/food.jpg">
                          </div>
                          <div class="col-md-8">
                            <p class="btn-heading">ANAOLATION BREAKFAST</p>
                            <span class="cat-text">Category:</span>&nbsp;<span class="break-text">Breakfast</span>
                            <p class="para-text">With choice of: Eggs with sujuk or menemen for your choice, cheese plate (sulguni, feta, kasar), tomato
                              and cucumber salad,tahin-pekmez, turkish adjika (walnut sauce), fried halloumi cheese, olives, butter, 
                            jam, dried fruits, honey, simit, bread busket, turkish tea (unlimited)&nbsp;</p>
                          </div>

                          <div class="col-md-1">
                            <p class="rate">70.00</p>
                          </div>
                        </div>
                        <div class="row row-border">
                          <div class="col-md-4">
                            <p class="fur-p"> 3 egg omlet</p>
                            <div class="span-tag-div">
                              <span class="cat-text">Category:</span><span class="break-text">Breakfast</span>
                            </div>
                            <p class="para-text">With the option of: cheese, ham, mushroom, pepper, tomato</p>
                          </div>
                          <div class="col-md-7"></div>
                          <div class="col-md-1">
                            <p class="rate">70.00</p>
                          </div>
                        </div>


                      </div>





                    </div>
                    <div class="col-md-12 mt-1">
                     <button type="button" class="collapse-btn" data-toggle="collapse" data-target="#demo2">Lunch<span class="right-class"><i class="fa fa-plus" aria-hidden="true"></i></span></button>
                     <div id="demo2" class="collapse col-div">

                      <div class="row row-border">
                        <div class="col-md-3">
                          <img src="http://crm.traveldoor.ge/dev-custom/assets/images/food.jpg">
                        </div>
                        <div class="col-md-8">
                          <p class="btn-heading">ANAOLATION BREAKFAST</p>
                          <span class="cat-text">Category:</span>&nbsp;<span class="break-text">Breakfast</span>
                          <p class="para-text">With choice of: Eggs with sujuk or menemen for your choice, cheese plate (sulguni, feta, kasar), tomato
                            and cucumber salad,tahin-pekmez, turkish adjika (walnut sauce), fried halloumi cheese, olives, butter, 
                          jam, dried fruits, honey, simit, bread busket, turkish tea (unlimited)&nbsp;</p>
                        </div>

                        <div class="col-md-1">
                          <p class="rate">70.00</p>
                        </div>
                      </div>
                      <div class="row row-border">
                        <div class="col-md-3">
                          <img src="http://crm.traveldoor.ge/dev-custom/assets/images/food.jpg">
                        </div>
                        <div class="col-md-8">
                          <p class="btn-heading">ANAOLATION BREAKFAST</p>
                          <span class="cat-text">Category:</span>&nbsp;<span class="break-text">Breakfast</span>
                          <p class="para-text">With choice of: Eggs with sujuk or menemen for your choice, cheese plate (sulguni, feta, kasar), tomato
                            and cucumber salad,tahin-pekmez, turkish adjika (walnut sauce), fried halloumi cheese, olives, butter, 
                          jam, dried fruits, honey, simit, bread busket, turkish tea (unlimited)&nbsp;</p>
                        </div>

                        <div class="col-md-1">
                          <p class="rate">70.00</p>
                        </div>
                      </div>
                      <div class="row row-border">
                        <div class="col-md-4">
                          <p class="fur-p"> 3 egg omlet</p>
                          <div class="span-tag-div">
                            <span class="cat-text">Category:</span><span class="break-text">Breakfast</span>
                          </div>
                          <p class="para-text">With the option of: cheese, ham, mushroom, pepper, tomato</p>
                        </div>
                        <div class="col-md-7"></div>
                        <div class="col-md-1">
                          <p class="rate">70.00</p>
                        </div>
                      </div>


                    </div>





                  </div>
                -->

              </div>
            </div>
          </div>



          <div class="col-lg-4 col-md-4">
            <div class="sticky-top sticky-sidebar">
              <div class="row">

                <div class="col-md-12">
                 
                    <div class="price-block">
                      <div class="row">
                        <div class="col-12">

<!-- 
                           <div class="row">
                            <div class="col-6">
                              <p class="price-2">Noodels(2 X 2 plates)</p>

                            </div>
                            <div class="col-6">
                              <p class="price-gel">
                               GEL 30
                             </p>
                           </div>
                         </div>
                       -->

                <!--          <div class="row">
                          <div class="col-8">
                            <p class="price-2">Butter-Chicken(2 X 2 plates)</p>

                          </div>
                          <div class="col-4">
                            <p class="price-gel">
                              GEL 30
                            </p>
                          </div>
                        </div>
                      -->
                      <div id="food_select_info">
                      </div>
                      <hr class="hr-total">
                      <div class="row">
                        <div class="col-6">
                          <p class="price-total">Total Price</p>
                        </div>
                        <div class="col-6">
                          <p class="total-gel">
                           GEL 0
                         </p>
                         <input type="hidden" name="markup" value="{{$markup}}">
                         <input type="hidden" name="own_markup" value="{{$own_markup}}">
                         <input type="hidden" id="total_supplier_price" name="total_supplier_price" value="0">
                         <input type="hidden" id="total_agent_price" name="total_agent_price" value="0">
                         <input type="hidden" id="total_price" name="total_price" value="0">
                       </div>
                     </div>




                   </div>
                 </div>
               </div>

               <div class="book-card">
                <div class="form-group form1">
                  <label for="select_date">SELECT DATE <span class="asterisk">*</span></label>
                  <div class="input-group date">
                   <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" placeholder="DATE" class="form-control pull-right datepicker" id="select_date" name="select_date" required="required" value="{{$restaurant_date}}">

                </div>
              </div>



              <div class="form-group form1">
                <label for="select_date">SELECT TIME<span class="asterisk">*</span></label>
                <div class="input-group date clockpicker">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" placeholder="TIME" class="form-control pull-right clockpicker" id="select_time" name="select_time"  required="required" value="{{$restaurant_time}}">

                </div>
              </div>

              <div class="form-group form1">
                <label for="person">NO. OF PERSONS<span class="asterisk">*</span></label>
                <div class="input-group ">
                  <div class="input-group-addon">
                    <i class="fa fa-user"></i>
                  </div>
                  <select class="form-control" name="no_of_persons" required="required">
                    <option value="" selected="selected" hidden="">SELECT
                      PERSONS
                    </option>
                    @for($i=1;$i<=50;$i++)
                    <option value="{{$i}}">{{$i}}</option>
                    @endfor
                  </select>
                </div>
              </div>
              <div class="book-btn-div">
                <button type="submit" class="book-btn">BOOK</button>
              </div>
            </div>


         
        </div>
      </div>
    </div>
  </div>
</div>








</div>
</div>
</div>
</div>
</div>

</div>
</form>
</div>
</div>

@include('agent.includes.footer')
@include('agent.includes.bottom-footer')
<script>
 $(document).ready(function()
 {
  $('.select2').select2();
  var date = new Date();
  date.setDate(date.getDate());
  $('#select_date').datepicker({
    autoclose:true,
    todayHighlight: true,
    format: 'yyyy-mm-dd',
    startDate:date
  });
  $('.clockpicker').clockpicker({
    'default': 'now',
    vibrate: true,
    placement: "top",
    align: "left",
    autoclose: false,
    twelvehour: true
  });


});
</script>

<!-- <script>
    $(window).scroll(function(){
    if ($(window).scrollTop() >= 250) {
        $('.stick').addClass('fixed');
       
    }
    else {
        $('.stick').removeClass('fixed');
      
    }
});
</script> -->
<script>
 $(window).scroll(function(e){ 
  var $el = $('.sticky-sidebar'); 
  var isPositionFixed = ($el.css('position') == 'fixed');
  if ($(this).scrollTop() > 200 && !isPositionFixed){ 
    $el.css({'position': 'fixed', 'top': '0px',"width": "24%"}); 

  }
  if ($(this).scrollTop() < 200 && isPositionFixed){
    $el.css({'position': 'static', 'top': '0px',"width": "100%"}); 
  } 
});
</script>
<script> 
 $('.collapse-btn').click(function() { 
  $(this).find('i').toggleClass('fas fa-plus fas fa-minus') 
}); 
</script> 


<script>
  $(document).on("change",".food_qty",function()
  {
    var html="";
    var total_price=0;
    var total_supplier_price=0;
     var total_agent_price=0;
      
    $(".food_qty").each(function()
    {
      if($(this).val()>0)
      {
        var id=$(this).attr('id').split("__")[1];
        var food_price=$("#food_price__"+id).val()
          var food_supplier_price=$("#food_supplier_price__"+id).val()
            var food_agent_price=$("#food_agent_price__"+id).val()

        var food_qty=$(this).val()

        var food_name=$("#food_name__"+id).val()

         var food_unit=$("#food_unit__"+id).val()

        var calculated_price=parseInt(food_price)*food_qty
         var calculated_supplier_price=parseInt(food_supplier_price)*food_qty
          var calculated_agent_price=parseInt(food_agent_price)*food_qty

        total_price+=parseInt(calculated_price)
        total_supplier_price+=parseInt(calculated_supplier_price)
        total_agent_price+=parseInt(calculated_agent_price)

         html+='<div class="row"><div class="col-8"><p class="price-2">'+food_name+' ('+food_qty+' X '+food_unit+')</p></div><div class="col-4"><p class="price-gel">GEL '+calculated_price+'</p></div></div>';
      }
    })


    $("#food_select_info").html(html)
    $(".total-gel").text("GEL "+total_price)
    $("#total_price").val(total_price)
    $("#total_supplier_price").val(total_supplier_price)
    $("#total_agent_price").val(total_agent_price)
  })


  $(document).on("submit","#restaurant_booking_form",function()
  {
    var count=0
    $(".food_qty").each(function()
    {
      if($(this).val()>0)
      {
        count++;
      }
    });
    if(count==0)
    {
      swal("Information","Please select atleast one food item","error");
      return false;
    }
   
  });
</script>





