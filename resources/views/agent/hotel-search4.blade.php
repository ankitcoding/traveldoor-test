@include('agent.includes.top-header')
<style>
        div.overlay {
        position: absolute;
        width: 100%;
        bottom: 0;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #5d4ecbbd !important;
        border-top-right-radius: 0 !important;
    }
    .selection-div {
    border: 1px solid gainsboro;
    padding: 0;
    border-radius: 5px;
    margin-bottom: 33px !IMPORTANT;
    background: url(https://sf2.mariefranceasia.com/wp-content/uploads/sites/7/2018/02/bawah-615x410.jpg);
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
}
    div.overlay-2 {
        position: absolute;
        width: 100%;
        top: 0;

        opacity: 0;
        bottom: 0;
        z-index: 99;
        transition: .5s ease;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #000000bd !important;
        border-top-right-radius: 0 !important;
    }

    .img-div:hover div.overlay-2 {
        transition: .5s ease;

        opacity: 1;
    }

    .img-div:hover .ribbon2 {
        display: none;
    }

    .img-div:hover::before,
    .img-div:hover::after {
        border: none
    }

    .left {
        float: left;
        width: 50%;
    }

    p.from {
        color: white;
        text-align: right;
        padding: 5px 10px 0;
        font-size: 18px;
        margin: 0;
    }

    span.price-span {
        display: block;
        color: white;
        text-align: right;
        padding: 0 10px;
    }

    img.h-img {
        width: 100%;
    }

   p.h-head {
    color: white;
    font-size: 15px;
    padding: 5px 10px;
    margin: 0;
    z-index: 9999;
    position: absolute;
    top: 0;
    left: 0;
    color: white;
    background: #E91E63;
    width: 100%;
}

    span.s-span {
        color: white;
        padding: 0 10px;
    }

    .img-div {
        border: 1px solid #756fcc;
        padding: 2px;
        border-radius: 5px;
        position: relative;
        z-index: 9;
        margin-bottom: 42px;
        transition: .5s ease;
    }

    /*  .img-div:before {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c70 transparent transparent;
        top: 56px;
        left: -14px;
        
        z-index: 0;
    }

    .img-div:after {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c7000 #402c70 transparent;
        top: -14px;
        left: 58px;
        
        z-index: -37;
    } */

    .ribbon2:before {
        content: "";
        position: absolute;
        top: 0;
        padding: 10px;
        /* margin: 5px; */
        left: 0;
        width: 100px;
        height: 20px;
        background: #5225c5;
        transform: rotate(135deg) translate(28px, 1px);
    }

    .ribbon2 {
        position: absolute;
        top: -7px;
        left: -7px;
        width: 100px;
        height: 100px;
        /* position: absolute; */
        overflow: hidden;
        /* background: aliceblue; */
    }

    button.hover-btn {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        border: none;
        background: #ffffff;
        color: #5225c5;
        padding: 7px 15px;
        cursor: pointer;
        border-radius: 5px;
    }

    @media screen and (max-width:1170px) {
        p.h-head {
            color: white;
            font-size: 15px;
            padding: 0 10px;
            margin: 0;
        }

        p.from {
            color: white;
            text-align: right;
            padding: 0px 10px 0;
            font-size: 15px;
            margin: 0;
        }

        span.s-span {
            color: white;
            padding: 0 10px;
            font-size: 11px;
        }

        span.price-span {
            display: block;
            color: white;
            text-align: right;
            padding: 0 10px;
            font-size: 12px;
        }
    }

     .table-activity-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
  }

   .carousel-item img {

        height: 100%;

    }



    .carousel-item {

        height: 100%;

    }



    p.start_price {

        margin: 0;

    }



    p.country_name.ng-binding {

        font-size: 20px;

        margin: 0;

    }



    .book_card {

        /* padding: 15px; */

        background: #fefeff;

        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);

        margin-bottom: 50px;



        border-radius: 5px;

    }



    .hotel_detail {

        height: 150px;

    }



    a.moredetail.ng-scope {

        background: gainsboro;

        padding: 7px 10px;

        /* margin-bottom: 10px; */

    }



    .booking_label {

        background: #5d53ce;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;

        color: white;

        padding: 10px 15px;

    }



    .booking_detail {

        padding: 15px;

    }



    a.btn.btn-outline.btn-circle.book_btn1 {

        background: #E91E63;

        border-radius: 5px !IMPORTANT;

        padding: 5px 20px;

        width: auto !important;

        height: auto;

        line-height: 2;

        color: white;

    }



    td p {

        margin: 0;

    }



    td {

        background: gainsboro;

    }



    table {

        border-collapse: separate;

    }



    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: 1px solid #59d25a;

    }



    a.panel-title {

        position: relative !important;

        background: #dfffe3;

        color: green !important;

        padding: 13px 20px 13px 85px !important;

        /* border-bottom: 1px solid #3ca23d; */

    }

.panel-title {

    display: block;

    margin-top: 0;

    margin-bottom: 0;

    padding: 1.25rem;

    font-size: 18px;

    color: #4d4d4d;

    height: 48px;

}

    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: none !important;

    }



    .panel-body {

        background: white;

        /* border: 1px solid #59d25a; */

        padding: 10px !important;

    }



    a.panel-title:before {

        content: attr(title) !important;

        position: absolute !important;

        top: 0px !important;

        opacity: 1 !important;

        left: 0 !important;

        padding: 12px 10px;

           width: auto;

    max-width: 250px;

        text-align: center;

        color: white;

        font-family: inherit !important;

        height: 48px;

        background: #279628;

        z-index: 999;

        transform: none !important;

    }



    .tab-content {

        margin-top: 10px;

    }



    div.panel-heading {

        border: 1px solid #59d25a !important;

    }



    .panel {

        border-top: none !important;

        margin-bottom: 5px !important;

    }

div#carousel-example-generic-captions {

    width: 100%;

}

/*daman css*/

    .hotel-div {

        width:75%;

        float:left;

        padding: 15px;

        display: flex;

        background: white;

    }



 .hotel-list-div {
    clear: both;
    display: block;
    border: 1px solid #c6bee6;
    border-radius: 5px;
    position: relative;
    /* clear: both; */
    height: 250px;
}

    .hotel-img-div {

       

        width: 40% !important;

        float:left;

    }



    .hotel-details {

     float:left;

        width: 60%;

        padding: 0 15px;

    }



   .hotel-info-div {
    width: 25%;
    float: left;
    padding: 15px;
    background: #e6e0ff;
    text-align: right;
    height: 100%;
}



    .checked {

        color: orange;

    }



    span.hotel-s {

        border: 1px solid #F44336;

        padding: 2px 5px;

        display: inline-block;

        color: #f44336;

        font-size: 12px;

        margin: 0 10px 0 0;

    }



    p.hotel-name {

        font-size: 22px;

        margin: 10px 0 0;

        color: black;

        float: left;

    }



    .rate-no {

        float: right;

    }



    .rate-no {

        float: right;

        display: block;

        margin-top: 14px;

        background: #2d3134;

        color: white;

        padding: 1px 8px;

        font-size: 12px;

        border-radius: 5px;

    }



    .heading-div {

        clear: both;

    }



    .rating {

        display: block;

        float: left;

        list-style: none;

        margin: 0;

        padding: 0;

    }



    p.r-number {

        float: right;

    }



    p.time-info {

        color: #4CAF50;

    }



    p.info {

        clear: both;

        margin: 0;

    }



    span.tag-item {

        background: #ffcbcd;

        padding: 5px 10px;

        border-radius: 5px;

        color: #ff4e54;

    }



    .inclusions {

        margin: 20px 0;

    }



    span.inclusion-item {

        padding: 10px 10px 0 0;

        color: #644ac9;

    }



    img.icon-i {

        width: auto;

        height: 20px;

    }



    p.include-p {

        color: black;

    }



    .inclusion-p {

        color: green

    }



    p.price-p span {

        background: #ee2128;

        color: white;

        padding: 3px 5px 3px 9px;

        border-radius: 5px;

        position: relative;

        z-index: 9999;

        border-top-right-radius: 5px;

        border-bottom-right-radius: 5px;

        margin-left: 20px;

        border-bottom-left-radius: 4px;

        font-size: 12px;

    }



    p.price-p span:before {

        content: "";

        width: 15px;

        height: 14.5px;

        background: #ee2128;

        position: absolute;

        transform: rotate(45deg);

        top: 3.5px;

        left: -6px;

        border-radius: 0px 0px 0px 3px;

        z-index: -1;

    }



    p.price-p span:after {

        content: "";

        background: white;

        width: 4px;

        height: 4px;

        position: absolute;

        top: 50%;

        left: 0px;

        transform: translateY(-50%);

        border-radius: 50%;

    }



    p.price-p {

        color: #ee2128;

    }



    p.tax {

        font-size: 12px;

        margin: 0;

    }



    p.days {

        font-size: 12px;

        margin: 0;

    }



    p.offer {

        font-size: 25px;

        color: black;

        font-weight: bold;

        margin: 0;

    }



    p.cut-price {

        margin: 0;

        text-decoration: line-through;

        font-size: 19px;

    }



    .login-a {

        color: #0088ff;

        font-size: 15px;

        font-weight: bold;

        margin-top: 10px;

        display: block;

    }

    @media screen and (max-width:1200px){

        p.hotel-name {

    font-size: 17px;

    margin: 10px 0 0;

    color: black;

    float: left;

}

p.r-number {

    float: right;

    font-size: 12px;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: block;

}

p.cut-price {

    margin: 0;

    text-decoration: line-through;

    font-size: 16px;

}

p.offer {

    font-size: 20px;

    color: black;

    font-weight: bold;

    margin: 0;

}

.login-a {

    color: #0088ff;

    font-size: 13px;

    font-weight: bold;

    margin-top: 10px;

    display: block;

}

    }

    @media screen and (max-width:1200px){

        .hotel-div {

    width: 100%;

    float: left;

    padding: 15px;

    display: flex;

    background: white;

}

.hotel-info-div {

    width: 100%;

    height: 100%;

    float: left;

    padding: 15px;

    background: #e6e0ff;

    text-align: left;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: inline;

}

    }

    @media screen and (max-width:992px){

        p.hotel-name {

    font-size: 17px;

    margin: 10px 0 0;

    color: black;

    float: none;

}

.rate-no {

    float: none;

    display: inline;

    margin-top: 14px;

    background: #2d3134;

    color: white;

    padding: 1px 8px;

    font-size: 12px;

    border-radius: 5px;

}

.rating {

    display: block;

    float: none;

    list-style: none;

    margin: 0;

    padding: 0;

}

p.r-number {

    float: none;

    font-size: 12px;

}

.hotel-details {

    float: none !important;

    width: 100% !important;

    padding: 0 15px;

    margin-top: 20px;

}

.hotel-list-div {

    display: block;

    border: 1px solid #c6bee6;

    border-radius: 5px;

    position: relative;

    /* clear: both; */

    height: 101%;

}

.hotel-div {

    width: 100%;

    float: none;

    padding: 15px;

    display: block;

    background: white;

}

.hotel-img-div {

    width: 100% !important;

    float: none !important;

    display: block !important;

}

a.flex-prev,a.flex-next {

    display: none;

}

span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

    display: block;

}

    }

    .img-slide{

            height: 200px;

    width: 100%;

    }

    .flex-control-thumbs {

    margin: 5px 0 0;

    position: static;

    overflow: hidden;

    width: 100%;

    height: 50px;
}
.form-group label{
    color: #fff;
}
.selection-div >.row.mb-10 {
    position: relative;
    background: #2727e07a;
    top: 0;
    left: 14px;
    width: 100%;
    height: 140px;
    bottom: 0;
    padding-top: 26px;
    border-radius: 5px;
    margin-bottom: 0 !important;
}

.theme-rosegold .btn-success {
    border-color: #F44336;
    background-color: #F44336 !important;
    color: #fff;
}
.input-group.date input {
    background: #dcdada7d !important;
    border-color: white;
    border: 1px solid #fff;
    color: #fff;
    height: 32px;
    border-right: 0px;
    border-top-right-radius: 0px !important;
    border-bottom-right-radius: 0px !important;
}
.select2-container--default .select2-selection--single {
    border: 1px solid #ddd;
    border-radius: 22px;
    /* padding: 6px 12px; */
    /* height: 34px; */
    color: white !important;
    background: #dcdada7d !important;
}

.form-control + .input-group-addon:not(:first-child) {
    border-radius: 0px 16px 14px 0px !important;
    border-left: 0;
}
button#search_hotel {
    text-align: center;
    margin-top: 22px;
    min-width: 110px;
}
.form-control {
    border-radius: 22px !important;
    box-shadow: none;
    border-color: #9e9e9e;
    height: auto;
    color: #fff;
    background-color: #dcdada7d !important;
}
/*----------- 30-1-2020---------*/
.input-group .input-group-addon {
    
    border-color: #fff;
    background-color: #dcdada7d;
}
select#room_quantity {
    border-color: #fff;
}
.select2-container--default .select2-selection--single .select2-selection__rendered{
    color: #fff !important;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('agent.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative"> 

@include('agent.includes.nav')

    <div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">Hotels</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item active" aria-current="page">Home

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

            <!-- <div class="right-title">

                <div class="dropdown">

                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

                            class="mdi mdi-dots-horizontal"></i></button>

                    <div class="dropdown-menu dropdown-menu-right">

                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

                        <div class="dropdown-divider"></div>

                        <button type="button" class="btn btn-rounded btn-success">Submit</button>

                    </div>

                </div>

            </div> -->

        </div>

    </div>


   <div class="row">
      <div class="box">
          <div class="box-body">
                  
                         <div class="row">

                            <div class="col-12">

                                <div class="box">



                                    <div class="box-body">
                                        <div class="selection-div">
                                      <div class="row mb-10">

                                        <div class="col-sm-12 col-md-12 col-lg-2">
                                            <div class="form-group">
                                                <label for="hotel_country">COUNTRY <span class="asterisk">*</span></label>
                                                <select id="hotel_country" name="hotel_country" class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">SELECT COUNTRY</option>
                                                    @foreach($countries as $country)
                                                    <option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-2">

                                            <div class="form-group">
                                                <label for="hotel_city">CITY <span class="asterisk">*</span></label>
                                                <select id="hotel_city" name="hotel_city" class="form-control select2" style="width: 100%;">
                                                    <option selected="selected">SELECT CITY</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-2">

                                            <div class="form-group">
                                                <label for="hotel_date_from">CHECKIN DATE <span class="asterisk">*</span></label>
                                                <div class="input-group date">
                                                    <input type="text" placeholder="CHECKIN DATE"
                                                    class="form-control pull-right datepicker" id="hotel_date_from" name="date_from" readonly="readonly">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <!-- /.input group -->

                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-2">

                                            <div class="form-group">
                                             <label for="hotel_date_to">CHECKOUT DATE <span class="asterisk">*</span></label>
                                             <div class="input-group date">
                                                <input type="text" placeholder="CHECKOUT DATE"
                                                class="form-control pull-right datepicker" id="hotel_date_to" name="date_to" readonly="readonly">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-2">
                                        <div class="form-group">
                                          <label for="room_quantity">Room Quantity<span class="asterisk">*</span></label>
                                          <select class="form-control" style="width: 100%;"  id="room_quantity" name="room_quantity" required="required">
                                            <option value="0">Select Room Quantity</option>
                                            <option selected="selected" >1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                            <option>10</option>
                                        </select>
                                    </div>
                                </div>
                                   <div class="col-sm-12 col-md-12 col-lg-2">
                                        <div class="form-group">
                                           <button id="search_hotel" class="btn btn-rounded  btn-success">Search</button>
                                    </div>
                                </div>

                            </div>
                        </div>

                            <div id="hotel_div">

                            </div>
                            <div class="text-center table-activity-loader" style="display: none">
                              <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                              viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                              <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                                <animateTransform 
                                attributeName="transform" 
                                dur="1s" 
                                type="translate" 
                                values="0 15 ; 0 -15; 0 15" 
                                repeatCount="indefinite" 
                                begin="0.1"/>
                            </circle>
                            <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                                <animateTransform 
                                attributeName="transform" 
                                dur="1s" 
                                type="translate" 
                                values="0 10 ; 0 -10; 0 10" 
                                repeatCount="indefinite" 
                                begin="0.2"/>
                            </circle>
                            <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                                <animateTransform 
                                attributeName="transform" 
                                dur="1s" 
                                type="translate" 
                                values="0 5 ; 0 -5; 0 5" 
                                repeatCount="indefinite" 
                                begin="0.3"/>
                            </circle>
                                    </svg>
                                </div>



                            </div>



                                            </div>

                                        </div>

                                    </div>
                      
                    
              
          </div>
      </div>
   </div>

</div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
<script>
    $(document).ready(function()
{
    $('.select2').select2();
    var date = new Date();
        date.setDate(date.getDate());
      $('#hotel_date_from').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#hotel_date_from").datepicker("getDate");
        var date_to = $("#hotel_date_to").datepicker("getDate");

        if(!date_to)
        {
            date_from.setDate(date_from.getDate()+1); 
            $('#hotel_date_to').datepicker("setDate",date_from);
        }
        else if(date_to<date_from)
        {
             date_from.setDate(date_from.getDate()+1); 
            $('#hotel_date_to').datepicker("setDate",date_from);
        }
    });

    $('#hotel_date_to').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#hotel_date_from").datepicker("getDate");
        var date_to = $("#hotel_date_to").datepicker("getDate");

        if(!date_from)
        {
            $('#hotel_date_from').datepicker("setDate",date_to);
        }
        else if(date_to<date_from)
        {
            $('#hotel_date_from').datepicker("setDate",date_to);
        }
    });

});



     $(document).on("change","#hotel_country",function()
    {
       if($("#hotel_country").val()!="0")
       {
        var country_id=$(this).val();
        $.ajax({
            url:"{{route('search-country-cities')}}",
            type:"GET",
            data:{"country_id":country_id},
            success:function(response)
            {

                $("#hotel_city").html(response);
                $('#hotel_city').select2();

            }
        });
    }

});

        //Hotel search filters

    $(document).on("click","#search_hotel",function()
    {
        var country_id=$("#hotel_country").val();
        var city_id=$("#hotel_city").val();
         var date_from=$("#hotel_date_from").val();
          var date_to=$("#hotel_date_to").val();
           var room_qty=$("#room_quantity").val();

           if(date_from==date_to)
           {
            alert("Checkin and Checkout dates cannot be same");
           }
           else
           {

        if(country_id!="0" && city_id!="0" && room_qty!="0")
        {
            $("#hotel_div").html("");
            $(".table-activity-loader").show();
            $.ajax({
                url:"{{route('fetchHotels')}}",
                data:{"country_id":country_id,
                "city_id":city_id,
                "date_from":date_from,
                "date_to":date_to,
                "room_qty":room_qty
            },
                type:"GET",
                success:function(response)
                {
                    $("#hotel_div").html(response);
                      $(".table-activity-loader").hide();
                }

            })
        }
    }


    });
    
</script>


</body>
</html>
