<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('agent.includes.top-header')

<style>
    
    .carousel-item img {
        height: 100%;
    }

    .carousel-item {
        height: 100%;
    }

    p.start_price {
        margin: 0;
    }

    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }

    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;

        border-radius: 5px;
    }

    .hotel_detail {
        height: 150px;
    }

    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }

    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }

    .booking_detail {
        padding: 15px;
    }

    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }

    td p {
        margin: 0;
    }

    td {
        background: gainsboro;
    }

    table {
        border-collapse: separate;
    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #59d25a;
    }

    a.panel-title {
        position: relative !important;
        background: #dfffe3;
        color: green !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #3ca23d; */
    }
.panel-title {
    display: block;
    margin-top: 0;
    margin-bottom: 0;
    padding: 1.25rem;
    font-size: 18px;
    color: #4d4d4d;
    height: 48px;
}
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }

    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }

    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
           width: auto;
    max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #279628;
        z-index: 999;
        transform: none !important;
    }

    .tab-content {
        margin-top: 10px;
    }

    div.panel-heading {
        border: 1px solid #59d25a !important;
    }

    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
div#carousel-example-generic-captions {
    width: 100%;
}
/*daman css*/
    .hotel-div {
        width:75%;
        float:left;
        padding: 15px;
        display: flex;
        background: white;
    }

    .hotel-list-div {
    display: block;
    border: 1px solid #c6bee6;
    border-radius: 5px;
    position: relative;
    /* clear: both; */
    height: 101%;
}
    .hotel-img-div {
       
        width: 40% !important;
        float:left;
    }

    .hotel-details {
     float:left;
        width: 60%;
        padding: 0 15px;
    }

    .hotel-info-div {
    width: 25%;
    float: left;
    padding: 15px;
    background: #e6e0ff;
    text-align: right;
    height: 100%;
}

    .checked {
        color: orange;
    }

    span.hotel-s {
        border: 1px solid #F44336;
        padding: 2px 5px;
        display: inline-block;
        color: #f44336;
        font-size: 12px;
        margin: 0 10px 0 0;
    }

    p.hotel-name {
        font-size: 22px;
        margin: 10px 0 0;
        color: black;
        float: left;
    }

    .rate-no {
        float: right;
    }

    .rate-no {
        float: right;
        display: block;
        margin-top: 14px;
        background: #2d3134;
        color: white;
        padding: 1px 8px;
        font-size: 12px;
        border-radius: 5px;
    }

    .heading-div {
        clear: both;
    }

    .rating {
        display: block;
        float: left;
        list-style: none;
        margin: 0;
        padding: 0;
    }

    p.r-number {
        float: right;
    }

    p.time-info {
        color: #4CAF50;
    }

    p.info {
        clear: both;
        margin: 0;
    }

    span.tag-item {
        background: #ffcbcd;
        padding: 5px 10px;
        border-radius: 5px;
        color: #ff4e54;
    }

    .inclusions {
        margin: 20px 0;
    }

    span.inclusion-item {
        padding: 10px 10px 0 0;
        color: #644ac9;
    }

    img.icon-i {
        width: auto;
        height: 20px;
    }

    p.include-p {
        color: black;
    }

    .inclusion-p {
        color: green
    }

    p.price-p span {
        background: #ee2128;
        color: white;
        padding: 3px 5px 3px 9px;
        border-radius: 5px;
        position: relative;
        z-index: 9999;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        margin-left: 20px;
        border-bottom-left-radius: 4px;
        font-size: 12px;
    }

    p.price-p span:before {
        content: "";
        width: 15px;
        height: 14.5px;
        background: #ee2128;
        position: absolute;
        transform: rotate(45deg);
        top: 3.5px;
        left: -6px;
        border-radius: 0px 0px 0px 3px;
        z-index: -1;
    }

    p.price-p span:after {
        content: "";
        background: white;
        width: 4px;
        height: 4px;
        position: absolute;
        top: 50%;
        left: 0px;
        transform: translateY(-50%);
        border-radius: 50%;
    }

    p.price-p {
        color: #ee2128;
    }

    p.tax {
        font-size: 12px;
        margin: 0;
    }

    p.days {
        font-size: 12px;
        margin: 0;
    }

    p.offer {
        font-size: 25px;
        color: black;
        font-weight: bold;
        margin: 0;
    }

    p.cut-price {
        margin: 0;
        text-decoration: line-through;
        font-size: 19px;
    }

    .login-a {
        color: #0088ff;
        font-size: 15px;
        font-weight: bold;
        margin-top: 10px;
        display: block;
    }
    @media screen and (max-width:1200px){
        p.hotel-name {
    font-size: 17px;
    margin: 10px 0 0;
    color: black;
    float: left;
}
p.r-number {
    float: right;
    font-size: 12px;
}
span.inclusion-item {
    padding: 10px 10px 0 0;
    color: #644ac9;
    display: block;
}
p.cut-price {
    margin: 0;
    text-decoration: line-through;
    font-size: 16px;
}
p.offer {
    font-size: 20px;
    color: black;
    font-weight: bold;
    margin: 0;
}
.login-a {
    color: #0088ff;
    font-size: 13px;
    font-weight: bold;
    margin-top: 10px;
    display: block;
}
    }
    @media screen and (max-width:1200px){
        .hotel-div {
    width: 100%;
    float: left;
    padding: 15px;
    display: flex;
    background: white;
}
.hotel-info-div {
    width: 100%;
    height: 100%;
    float: left;
    padding: 15px;
    background: #e6e0ff;
    text-align: left;
}
span.inclusion-item {
    padding: 10px 10px 0 0;
    color: #644ac9;
    display: inline;
}
    }
    @media screen and (max-width:992px){
        p.hotel-name {
    font-size: 17px;
    margin: 10px 0 0;
    color: black;
    float: none;
}
.rate-no {
    float: none;
    display: inline;
    margin-top: 14px;
    background: #2d3134;
    color: white;
    padding: 1px 8px;
    font-size: 12px;
    border-radius: 5px;
}
.rating {
    display: block;
    float: none;
    list-style: none;
    margin: 0;
    padding: 0;
}
p.r-number {
    float: none;
    font-size: 12px;
}
.hotel-details {
    float: none !important;
    width: 100% !important;
    padding: 0 15px;
    margin-top: 20px;
}
.hotel-list-div {
    display: block;
    border: 1px solid #c6bee6;
    border-radius: 5px;
    position: relative;
    /* clear: both; */
    height: 101%;
}
.hotel-div {
    width: 100%;
    float: none;
    padding: 15px;
    display: block;
    background: white;
}
.hotel-img-div {
    width: 100% !important;
    float: none !important;
    display: block !important;
}
a.flex-prev,a.flex-next {
    display: none;
}
span.inclusion-item {
    padding: 10px 10px 0 0;
    color: #644ac9;
    display: block;
}
    }
    .img-slide{
            height: 200px;
    width: 100%;
    }
    .flex-control-thumbs {
    margin: 5px 0 0;
    position: static;
    overflow: hidden;
    width: 100%;
    height: 50px;
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('agent.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('agent.includes.nav')

	<div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">Hotel List</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Home

                            <li class="breadcrumb-item active" aria-current="page">Hotel List

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

            <!-- <div class="right-title">

                <div class="dropdown">

                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

                            class="mdi mdi-dots-horizontal"></i></button>

                    <div class="dropdown-menu dropdown-menu-right">

                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

                        <div class="dropdown-divider"></div>

                        <button type="button" class="btn btn-rounded btn-success">Submit</button>

                    </div>

                </div>

            </div> -->

        </div>

    </div>

    <div class="row">
        <div class="col-12">
            <div class="box">

                <div class="box-body">
                    <div class="row">
                    <?php 
                    foreach ($get_hotels as $get_hotel_data) {
                        # code...
                    $newid=base64_encode($get_hotel_data->hotel_id);


                    ?>
                        <div class="col-md-12" style="margin:0 0 25px">
                            <a href="{{route('hotel-detail',['hotelid'=>$newid])}}" style="display: block;">
                                <div class="hotel-list-div">
    
                                    <div class="hotel-div">
                                        <div class="hotel-img-div">

                                            <div class="flexslider2">
                                                <ul class="slides">
                                                    <?php
                                                     $hotelimage=unserialize($get_hotel_data->hotel_images);
                                                     for($i=0;$i<count($hotelimage);$i++)
                                                     {
                                                        echo '<li
                                                        data-thumb="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[$i].'" >
                                                        <img class="img-slide" src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[$i].'"
                                                            alt="slide"  />';
                                                        }
                                                    ?>
                                                  
                                                </ul>



                                            </div>
                                        </div>
                                        <div class="hotel-details">
                                            <span class="hotel-s">RESORT</span>
                                            <span class="hotel-s">MMT ASSURED</span>
                                            <div class="heading-div">
                                                <p class="hotel-name">{{$get_hotel_data->hotel_name}}</p>
                                                <div class="rate-no">
                                                   <!--  <span>{{$get_hotel_data->hotel_rating}}</span> -->

                                                </div>
                                            </div>
                                            <div class="heading-div">
                                                <div class="rating">
                                                    <?php 
                                                    for($jt=1;$jt<=$get_hotel_data->hotel_rating;$jt++)
                                                    {
                                                        echo '<span class="fa fa-star checked"></span>';
                                                    }
                                                   ?>
                                                    <!-- <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span> -->

                                                </div>
                                                <!-- <p class="r-number">1421 RATINGS</p> -->
                                            </div>
                                            
                                            <p class="info">BOOKING VALIDITY: 
                                                <br>
                                               <?php
                                                $hotel_season_details=unserialize($get_hotel_data->hotel_season_details);
                                                $rate_allocation_details=unserialize($get_hotel_data->rate_allocation_details);
                                                for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
                                            {
                                                if($hotel_season_details[$rate_allocation_count]['season_name']!="" && $hotel_season_details[$rate_allocation_count]['season_name']!=null)
                                                {
                                                     echo ucfirst($hotel_season_details[$rate_allocation_count]['season_name'])."  ";
                                                }
                                                else
                                                {
                                                    echo " No Data Available";
                                                }
                                                                       
                                                 
                                                if($hotel_season_details[$rate_allocation_count]['booking_validity_from']!="" && $hotel_season_details[$rate_allocation_count]['booking_validity_from']!=null)
                                                {
                                                   echo date('d-m-Y',strtotime($hotel_season_details[$rate_allocation_count]['booking_validity_from']));
                                                }
                                                else
                                                {
                                                    echo "No Data Available";
                                                }    
                                                    echo " To "  ;   
                                                 if($hotel_season_details[$rate_allocation_count]['booking_validity_to'] && $hotel_season_details[$rate_allocation_count]['booking_validity_to']!=null)
                                                 {
                                                        echo date('d-m-Y',strtotime($hotel_season_details[$rate_allocation_count]['booking_validity_to']));
                                                 }   
                                                 else
                                                 {
                                                    echo "No Data Available";
                                                 } 
                                                 echo "<br>";
                                                } 
                                                 ?>       
                                             </p>


                                            <p class="time-info">{{$get_hotel_data->hotel_address}}</p>
                                            <div class="tags">
                                               <?php 
                                               for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)
                                            {
                                                for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)
                                                {
                                                    
                                               ?> 
                                                <span class="tag-item" style="margin-right: 5px">
                                                    <?php
                                                    if($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]!="null")
                                                        {
                                                            echo $rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]." ";
                                                        }
                                                        else
                                                        {
                                                            echo "No Data Available";
                                                        }
                                                    ?>

                                                        </span>
                                            <?php }
                                             }
                                             ?>
                                            </div>
                                            <div class="inclusions">
                                                <span class="inclusion-item"><img src="images/swimming-silhouette.png"
                                                        class="icon-i"> Swimming Pool</span>
                                                <span class="inclusion-item"><img src="images/parked-car.png"
                                                        class="icon-i"> Parking</span>
                                                <span class="inclusion-item"><img src="images/check-symbol.png"
                                                        class="icon-i"> Housekeeping</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hotel-info-div">
                                        <div class="include-div">
                                            <p class="include-p">INCLUDED IN THIS PRICE</p>
                                            <p class="inclusion-p"><span class="fa fa-check"> </span>
                                            

                                             Free Breakfast</p>
                                        </div>
                                        <div class="price-div">
                                            <p class="price-p">Save INR 855 <span>44%</span></p>
                                        </div>
                                        <p class="cut-price">INR 3,870</p>
                                        <p class="offer">INR 3,015</p>
                                        <p class="days">Per Night</p>
                                        <p class="tax">+ INR 679 taxes and charges</p>
                                        <a href="#" class="login-a">Login & unlock a secret deal!</a>

                                    </div>

                                </div>

                            </a>

                        </div>
                    <?php } ?>
                     <!--    <div class="col-md-12" style="margin:0 0 25px">
                            <a href="hotel-detail.php" style="display: block;">
                                <div class="hotel-list-div">

                                    <div class="hotel-div">
                                        <div class="hotel-img-div">

                                            <div class="flexslider2">
                                                <ul class="slides">
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                </ul>



                                            </div>
                                        </div>
                                        <div class="hotel-details">
                                            <span class="hotel-s">RESORT</span>
                                            <span class="hotel-s">MMT ASSURED</span>
                                            <div class="heading-div">
                                                <p class="hotel-name">Quality Inn Ocean Palms Goa</p>
                                                <div class="rate-no">
                                                    <span>4.3/5</span>

                                                </div>
                                            </div>
                                            <div class="heading-div">
                                                <div class="rating">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>

                                                </div>
                                                <p class="r-number">1421 RATINGS</p>
                                            </div>

                                            <p class="info">Calangute</p>
                                            <p class="time-info">1 min from Kerkar Art Complex</p>
                                            <div class="tags">
                                                <span class="tag-item">Couple Friendly</span>
                                            </div>
                                            <div class="inclusions">
                                                <span class="inclusion-item"><img src="images/swimming-silhouette.png"
                                                        class="icon-i"> Swimming Pool</span>
                                                <span class="inclusion-item"><img src="images/parked-car.png"
                                                        class="icon-i"> Parking</span>
                                                <span class="inclusion-item"><img src="images/check-symbol.png"
                                                        class="icon-i"> Housekeeping</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hotel-info-div">
                                        <div class="include-div">
                                            <p class="include-p">INCLUDED IN THIS PRICE</p>
                                            <p class="inclusion-p"><span class="fa fa-check"> </span> Free Breakfast</p>
                                        </div>
                                        <div class="price-div">
                                            <p class="price-p">Save INR 855 <span>44%</span></p>
                                        </div>
                                        <p class="cut-price">INR 3,870</p>
                                        <p class="offer">INR 3,015</p>
                                        <p class="days">Per Night</p>
                                        <p class="tax">+ INR 679 taxes and charges</p>
                                        <a href="#" class="login-a">Login & unlock a secret deal!</a>

                                    </div>

                                </div>

                            </a>

                        </div>
                        <div class="col-md-12" style="margin:0 0 25px">
                            <a href="hotel-detail.php" style="display: block;">
                                <div class="hotel-list-div">

                                    <div class="hotel-div">
                                        <div class="hotel-img-div">

                                            <div class="flexslider2">
                                                <ul class="slides">
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                </ul>



                                            </div>
                                        </div>
                                        <div class="hotel-details">
                                            <span class="hotel-s">RESORT</span>
                                            <span class="hotel-s">MMT ASSURED</span>
                                            <div class="heading-div">
                                                <p class="hotel-name">Quality Inn Ocean Palms Goa</p>
                                                <div class="rate-no">
                                                    <span>4.3/5</span>

                                                </div>
                                            </div>
                                            <div class="heading-div">
                                                <div class="rating">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>

                                                </div>
                                                <p class="r-number">1421 RATINGS</p>
                                            </div>

                                            <p class="info">Calangute</p>
                                            <p class="time-info">1 min from Kerkar Art Complex</p>
                                            <div class="tags">
                                                <span class="tag-item">Couple Friendly</span>
                                            </div>
                                            <div class="inclusions">
                                                <span class="inclusion-item"><img src="images/swimming-silhouette.png"
                                                        class="icon-i"> Swimming Pool</span>
                                                <span class="inclusion-item"><img src="images/parked-car.png"
                                                        class="icon-i"> Parking</span>
                                                <span class="inclusion-item"><img src="images/check-symbol.png"
                                                        class="icon-i"> Housekeeping</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hotel-info-div">
                                        <div class="include-div">
                                            <p class="include-p">INCLUDED IN THIS PRICE</p>
                                            <p class="inclusion-p"><span class="fa fa-check"> </span> Free Breakfast</p>
                                        </div>
                                        <div class="price-div">
                                            <p class="price-p">Save INR 855 <span>44%</span></p>
                                        </div>
                                        <p class="cut-price">INR 3,870</p>
                                        <p class="offer">INR 3,015</p>
                                        <p class="days">Per Night</p>
                                        <p class="tax">+ INR 679 taxes and charges</p>
                                        <a href="#" class="login-a">Login & unlock a secret deal!</a>

                                    </div>

                                </div>

                            </a>

                        </div>
                        <div class="col-md-12" style="margin:0 0 25px">
                            <a href="hotel-detail.php" style="display: block;">
                                <div class="hotel-list-div">

                                    <div class="hotel-div">
                                        <div class="hotel-img-div">

                                            <div class="flexslider2">
                                                <ul class="slides">
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                </ul>



                                            </div>
                                        </div>
                                        <div class="hotel-details">
                                            <span class="hotel-s">RESORT</span>
                                            <span class="hotel-s">MMT ASSURED</span>
                                            <div class="heading-div">
                                                <p class="hotel-name">Quality Inn Ocean Palms Goa</p>
                                                <div class="rate-no">
                                                    <span>4.3/5</span>

                                                </div>
                                            </div>
                                            <div class="heading-div">
                                                <div class="rating">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>

                                                </div>
                                                <p class="r-number">1421 RATINGS</p>
                                            </div>

                                            <p class="info">Calangute</p>
                                            <p class="time-info">1 min from Kerkar Art Complex</p>
                                            <div class="tags">
                                                <span class="tag-item">Couple Friendly</span>
                                            </div>
                                            <div class="inclusions">
                                                <span class="inclusion-item"><img src="images/swimming-silhouette.png"
                                                        class="icon-i"> Swimming Pool</span>
                                                <span class="inclusion-item"><img src="images/parked-car.png"
                                                        class="icon-i"> Parking</span>
                                                <span class="inclusion-item"><img src="images/check-symbol.png"
                                                        class="icon-i"> Housekeeping</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hotel-info-div">
                                        <div class="include-div">
                                            <p class="include-p">INCLUDED IN THIS PRICE</p>
                                            <p class="inclusion-p"><span class="fa fa-check"> </span> Free Breakfast</p>
                                        </div>
                                        <div class="price-div">
                                            <p class="price-p">Save INR 855 <span>44%</span></p>
                                        </div>
                                        <p class="cut-price">INR 3,870</p>
                                        <p class="offer">INR 3,015</p>
                                        <p class="days">Per Night</p>
                                        <p class="tax">+ INR 679 taxes and charges</p>
                                        <a href="#" class="login-a">Login & unlock a secret deal!</a>

                                    </div>

                                </div>

                            </a>

                        </div>
                        <div class="col-md-12" style="margin:0 0 25px">
                            <a href="hotel-detail.php" style="display: block;">
                                <div class="hotel-list-div">

                                    <div class="hotel-div">
                                        <div class="hotel-img-div">

                                            <div class="flexslider2">
                                                <ul class="slides">
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                    <li
                                                        data-thumb="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg">
                                                        <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"
                                                            alt="slide" />
                                                    </li>
                                                </ul>



                                            </div>
                                        </div>
                                        <div class="hotel-details">
                                            <span class="hotel-s">RESORT</span>
                                            <span class="hotel-s">MMT ASSURED</span>
                                            <div class="heading-div">
                                                <p class="hotel-name">Quality Inn Ocean Palms Goa</p>
                                                <div class="rate-no">
                                                    <span>4.3/5</span>

                                                </div>
                                            </div>
                                            <div class="heading-div">
                                                <div class="rating">
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star checked"></span>
                                                    <span class="fa fa-star"></span>
                                                    <span class="fa fa-star"></span>

                                                </div>
                                                <p class="r-number">1421 RATINGS</p>
                                            </div>

                                            <p class="info">Calangute</p>
                                            <p class="time-info">1 min from Kerkar Art Complex</p>
                                            <div class="tags">
                                                <span class="tag-item">Couple Friendly</span>
                                            </div>
                                            <div class="inclusions">
                                                <span class="inclusion-item"><img src="images/swimming-silhouette.png"
                                                        class="icon-i"> Swimming Pool</span>
                                                <span class="inclusion-item"><img src="images/parked-car.png"
                                                        class="icon-i"> Parking</span>
                                                <span class="inclusion-item"><img src="images/check-symbol.png"
                                                        class="icon-i"> Housekeeping</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hotel-info-div">
                                        <div class="include-div">
                                            <p class="include-p">INCLUDED IN THIS PRICE</p>
                                            <p class="inclusion-p"><span class="fa fa-check"> </span> Free Breakfast</p>
                                        </div>
                                        <div class="price-div">
                                            <p class="price-p">Save INR 855 <span>44%</span></p>
                                        </div>
                                        <p class="cut-price">INR 3,870</p>
                                        <p class="offer">INR 3,015</p>
                                        <p class="days">Per Night</p>
                                        <p class="tax">+ INR 679 taxes and charges</p>
                                        <a href="#" class="login-a">Login & unlock a secret deal!</a>

                                    </div>

                                </div>

                            </a>

                        </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
 
           
                
              
          </div>
      </div>
   </div>

</div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')

<aside class="control-sidebar">

    <div class="rpanel-title"><span class="pull-right btn btn-circle btn-danger"><i class="ion ion-close text-white"
                data-toggle="control-sidebar"></i></span> </div>
    <!-- Create the tabs -->
    <ul class="nav nav-tabs control-sidebar-tabs">
        <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab" title="Notifications"><i
                    class="ti-comment-alt"></i></a></li>
        <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab" title="Comments"><i
                    class="ti-tag"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
            <div class="lookup lookup-sm lookup-right d-none d-lg-block mb-10">
                <input type="text" name="s" placeholder="Search" class="w-p100">
            </div>
            <div class="media-list media-list-hover">
                <a class="media media-single" href="#">
                    <h4 class="w-50 text-gray font-weight-500">10:10</h4>
                    <div class="media-body pl-15 bl-5 rounded border-primary">
                        <p>Morbi quis ex eu arcu auctor sagittis.</p>
                        <span class="text-fade">by Johne</span>
                    </div>
                </a>

                <a class="media media-single" href="#">
                    <h4 class="w-50 text-gray font-weight-500">08:40</h4>
                    <div class="media-body pl-15 bl-5 rounded border-success">
                        <p>Proin iaculis eros non odio ornare efficitur.</p>
                        <span class="text-fade">by Amla</span>
                    </div>
                </a>

                <a class="media media-single" href="#">
                    <h4 class="w-50 text-gray font-weight-500">07:10</h4>
                    <div class="media-body pl-15 bl-5 rounded border-info">
                        <p>In mattis mi ut posuere consectetur.</p>
                        <span class="text-fade">by Josef</span>
                    </div>
                </a>

                <a class="media media-single" href="#">
                    <h4 class="w-50 text-gray font-weight-500">01:15</h4>
                    <div class="media-body pl-15 bl-5 rounded border-danger">
                        <p>Morbi quis ex eu arcu auctor sagittis.</p>
                        <span class="text-fade">by Rima</span>
                    </div>
                </a>

                <a class="media media-single" href="#">
                    <h4 class="w-50 text-gray font-weight-500">23:12</h4>
                    <div class="media-body pl-15 bl-5 rounded border-warning">
                        <p>Morbi quis ex eu arcu auctor sagittis.</p>
                        <span class="text-fade">by Alaxa</span>
                    </div>
                </a>

            </div>
        </div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
            <div class="media-list media-list-hover media-list-divided">
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
                        <p>Cras tempor diam nec metus...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (22)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
                        <p>Praesent tristique diam...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (23)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
                        <p>Cras tempor diam nec...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (22)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
                        <p>Praesent tristique diam...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (23)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
                        <p>Cras tempor diam nec metus...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (22)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
                        <p>Praesent tristique diam...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (23)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>
                        <p>Cras tempor diam nec...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (22)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="media">
                    <img class="avatar avatar-lg" src="" alt="...">

                    <div class="media-body">
                        <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>
                        <p>Praesent tristique diam...</p>
                        <div class="media-block-actions">
                            <nav class="nav nav-dot-separated no-gutters">
                                <div class="nav-item">
                                    <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>
                                        (17)</a>
                                </div>
                                <div class="nav-item">
                                    <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>
                                        (23)</a>
                                </div>
                            </nav>

                            <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">
                                <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Approve"><i class="ion-checkmark"></i></a>
                                <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""
                                    data-original-title="Delete"><i class="ion-close"></i></a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</aside>

<div class="control-sidebar-bg"></div>
</div>