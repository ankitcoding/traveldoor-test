<?php

use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\LoginController;
?>

@include('agent.includes.top-header')


<style>
    .flexslider2 {
    width: 100%;
    height: auto;
}
li.flex-active-slide {
    height: 580px;
}
.flex-control-thumbs img{
    opacity:1;
}
.flexslider2 .slides img {
    width: auto;
    height: 100%;
    margin: auto;
    display: block;
}
.flex-control-thumbs img{
    opacity:1;
}
    .flex-viewport {
        height: 580px;
        background: rgb(14, 14, 14);

    }
    .flex-silder-div .flex-viewport {
        height: 300px;

    }

    .flex-viewport {
        max-height: 100%;
        -webkit-transition: all 1s ease;
        -moz-transition: all 1s ease;
        -ms-transition: all 1s ease;
        -o-transition: all 1s ease;
        transition: all 1s ease;
    }

     .flexslider1 {
    width: 100%;
    height: auto;
}
.flexslider1 li.flex-active-slide {
    height: 300px;
}
.flexslider1 .flex-control-thumbs img{
    opacity:1;
}
.flexslider1 .slides img {
    width: auto;
    height: 100%;
    margin: auto;
    display: block;
}
.flexslider1 .flex-control-thumbs img{
    opacity:1;
}

   /* ul.slides li img {
        height: 100% !important;
        width: 100% !important;
    }*/

    @media screen and (max-width: 500px) {
  .flexslider2 li.flex-active-slide {
    width: 500px !important;
    height: 450px !important;
}
.flex-viewport {
        height: 450px;
        background: rgb(14, 14, 14);

    }
}
.flexslider2 li.flex-active-slide {
    width: 1145px !important;
    height: 580px !important;
}
    .flex-control-thumbs img {
        width: 100%;
        height: 100%;
        display: block;
        opacity: .7;
        cursor: pointer;
        -moz-user-select: none;
        -webkit-transition: all 1s ease;
        -moz-transition: all 1s ease;
        -ms-transition: all 1s ease;
        -o-transition: all 1s ease;
        transition: all 1s ease;
    }

    .flex-control-thumbs li {
        width: 25%;
        float: left;
        margin: 0;
        height: 100%;
    }

    ol.flex-control-nav.flex-control-thumbs {
        height: auto;
    }

    p.para-h {
        font-size: 21px;
    }

}
span.badge-icon {
    background: #ffffff;
    padding: 5px;
    border-radius: 5px;
    font-size: 13px;
    color: #FF9800;
    margin-left: 10px;
    display: inline-block;
}
p.para-h {
    font-size: 21px;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    /* margin-left: 10px; */
    color: #FF5722;
    border-bottom: 1px solid #e4e4e4;
}
span.badge-icon {
    display: inline-block;
    margin-left: 10px;
    font-size: 14px;
    color: #FF9800;
}
li.flex-active-slide {
    width: 969px !important;
}

.section {
    padding: 15px;
}

p.star-p {
    font-size: 18px;
    color: #d23d64;
}

i.fa.fa-star.star {
    color: white;
    background: orange;
    padding: 5px;
    border-radius: 50%;
}

.sidebar-div {
    background: #ffe5e5;
    border: 1px solid #ff9494;
    border-radius: 5px;
    padding: 15px;
}

p.s-head {
    color: #ff5e5e;
    font-size: 20px;
}

p.s-head-2 {
    color: #5f5f5f;
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 0;
}

.review {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
}

.r-div p {
    margin: 0;
    color: #424242;
    font-size: 14px;
    text-transform: capitalize;
}

.f-head {
    color: #644ac9;
    font-weight: 600;
}

.parking i {
    color: white;
    font-size: 20px;
    font-weight: 700;
    background: black;
    padding: 1px 9px;
    display: inline-block;
    width: 30px;
    border-radius: 50%;
    height: 30px;
}

button.review-btn {
    background: #ff7272;
    border: none;
    padding: 10px;
    width: 100%;
    color: white;
}

p.parking {
    color: black;
}
p.facillity-head {
    font-size: 20px;
}
p.span-f {
    background: #a4ecff;
    margin: 0 15px 0 0;
    color: #00a3cc;
    padding: 5px 10px;
    border-radius: 5px;
}
p.span-f2 {
   background: none;
   margin: 0 15px 0 0;
   color: #4e4e4e;
   padding: 5px 10px;
   border-radius: 5px;
}
.facilities-div {
    display: flex;
}

p.para {
    margin: 10px 0;
    color: #11aad1;
    font-size: 16px;
}
.alert-success {
    color: #155724 !IMPORTANT;
    background-color: #d4edda;
    border-color: #c3e6cb;
}
p.para {
    color: #00a3cc;
    font-size: 16px;
    margin-top: 10px;
}
.check-form {
    background: #313131;
    padding: 15px;
    color: white;
}
a.details i {
    font-size: 19px;
    margin-right: 7px;
    color: orange;
}
p.span-f i {
    margin-right: 5px;
}
a.details {
    color: #3F51B5;
    font-size: 16px;
}
button.show-price {
    border: none;
    background: #ffffff;
    padding: 10px 15px;
    color: #ff5722;
    width: 90%;
    border-radius: 50px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
button.book-btn {
    cursor: pointer;
    border: none;
    background: #ffffff;
    padding: 10px 15px;
    color: #ff5722;
    width: 100%;
    font-size: 14px;
    white-space: pre;
    border-radius: 50px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}
button.book-btn-sold {
    cursor: pointer;
    border: none;
    background: #ff5722;
    padding: 10px 15px;
    color: #ffffff;
    width: 100%;
    font-size: 14px;
    white-space: pre;
    border-radius: 50px;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
}

button.selected-room
{
    color: #fff !important;
    background-color: #ff5722 !important;
}
.table thead {
    background: #FF5722;
    color: white;
}
table#room_type_table {
    margin-bottom: 30px;
}
table#room_type_table tr td {
    background: #ffebe5;
    color: #000000;
    border:1px solid #ffd4c7;
}
.alert-danger {
    color: #721c24 !IMPORTANT;
    background-color: #f8d7da;
    border-color: #f5c6cb;
}
p.h-para {
    font-size: 20px;
}
.h-div {
    background: #e9f0fa;
    padding: 20px;
}
i.tick-s {
    color: #1550a9;
    background: white;
    padding: 5px;
    border: 1px solid #74d6ff;
    border-radius: 50%;
}
.p-div {
    font-size: 14px;
    color: #1f1f1f;
    margin-bottom: 20px;

}
.see-avail {
    display: flex;
    justify-content: space-between;
    align-items: center;
}
.table > tbody > tr > td, .table > tbody > tr > th {
    border-top: none;
    padding: 1rem;
    vertical-align: middle;
}
.table-div div table tr th {
    background: #eaffeb;
    border-color: #b7ddb8;
    border-top: 1px solid #b7ddb8;
}

.table-div div table tr td {
    border-color: #b7ddb8;
    border-top: 1px solid #b7ddb8;
}

.icon-i span {
    background: #cbffcb;
    color: green;
}

.icon-i {
    margin-bottom: 36px;
    margin-top: 10px;
}
span.tag {
    border: 1px solid black;
    padding: 2px 5px;
    margin: 0 5px 5px 0;
    font-size: 12px;
    border-radius: 2px;
}
.tags-div {
    display: flex;
    flex-wrap: wrap;
}

.flexslider1 a.flex-prev {
    color: white;
    z-index: 98999;
    overflow: visible;
    left: 0 !important;
    position: absolute;
    opacity: 1;
    text-shadow: none;
    box-shadow: none;
}
.flexslider2 a.flex-prev {
    color: white;
    z-index: 98999;
    background: black;
    overflow: visible;
    left: 0 !important;
    position: absolute;
    opacity: 1;
    text-shadow: none;
    box-shadow: none;
}
.flexslider1 a.flex-next{
  color: white;
  z-index: 98999;

  overflow: visible;
  right: 33px !important;
  position: absolute;
  opacity: 1;
  text-shadow: none;
  box-shadow: none;
}
.flexslider2 a.flex-next{
  color: white;
  z-index: 98999;
  background: black;
  overflow: visible;
  right: 45px !important;
  position: absolute;
  opacity: 1;
  text-shadow: none;
  box-shadow: none;
}
.flexslider1 li.flex-active-slide {
    width: 420px !important;
    height: 300px;
}

ul.flex-direction-nav {
    position: absolute;
    z-index: 999;
    opacity: 1;
    visibility: visible;
    top: 40%;
    transform: translateY(-50%);
    width: 100%;
    height: 10px;
}
.flex-direction-nav a:before {
    font-family: "flexslider-icon";
    font-size: 29px !important;
    display: inline-block !important;
    content: \f001;
    color: rgba(255, 255, 255, 0.8) !important;
    text-shadow: none !important;
}
ol.flex-control-nav.flex-control-thumbs {
    width: 100%;
    display: flex;
    flex-wrap: wrap;
}
.flex-control-thumbs li {
    width: 13% !important;
    float: left !important;
    margin: 2.4px !important;
    height: 52px !important;
    
}
.most-p-div {
    background: #ffeae8;
    padding: 10px;
}
.most-p-div .row .col-md-4 h3 {
    color: #f44336;
    background: #ffffff;
    font-size: 18px;
    padding: 10px 10px 0px;
    margin-bottom: 0;
}
.list-div {
    border: 1px solid #ffffff;
    background: #ffffff;
    padding-top: 5px;
    margin-bottom: 15px;
}
.change-search {
    background: #3373ce !important;
    border: none;
    border-radius: 50px;
}
div#myfilter .modal-dialog {
    margin-top: 15%;
    max-width: 50% !important;
}
button#check_availability {
    display: block;
}
button#check_availability:disabled {
    background: gray;
    cursor: not-allowed;
}
button#check_availability:disabled:hover {
    background: gray !important;
    cursor: not-allowed;
}
div#myfilter .modal-dialog .modal-content {
    border-radius: 5px;
}

p.hotel-type {
    position: absolute;
    width: auto;
    top: -11px;
    right: 0px;
    padding: 5px 10px;
    background: linear-gradient(45deg, #d5135a, #f05924);
    border: 1px solid white;
    border-radius: 30px;
    color: white;
    min-width: 100px;
    text-align: center;
}
.wrap-div {
    border: 1px solid #ffcbbb;
    padding: 15px;
    background: #ffffff;
    margin: 15px 0;
    border-radius: 5px;
}
h2.wrap-heading {
    color: #dd274b;
    font-size: 21px;
    margin-top: 0;
    border-bottom: 2px solid #ffa99e;
    padding-bottom: 7px;
}
.info-div {
    border-bottom: 1px solid #dfdfdf;
    margin-bottom: 10px;
    display: flex;
    justify-content: space-between;
    font-size: 15px;
    flex: 0 0 47%;
}
.column-div {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
}
a.img-div i {
    display: block;
    margin: 2px 1px 1px;
    color: #e94831;
}
button#check_availability {
    background: black;
    opacity: 1;
    border: none;
}
div#myfilter button.close {
    position: absolute;
    top: 4px;
    right: 4px;
    background: black;
    opacity: 1;
    padding: 2px 5px;
    border-radius: 50%;
    color: white;
}
.form-control:disabled, .form-control[readonly] {
    background-color: #ececec !important;
    opacity: 1;
}
.input-group .input-group-addon {
    border-radius: 5px 0px 0px 5px;
    border-color: #d0d0d0;
    background-color: #ffffff;
}
.form-control {
    border-radius: 5px;
    box-shadow: none;
    border-color: #d0d0d0 !important;
    height: auto;
}
a.img-div {
    background: #ffcec3 !important;
    display: block;
    padding: 6px 8px 9px;
    width: 30px;
    height: 30px;
    border: none;
    top: -9px;
    position: absolute;
    margin-bottom: 20px;
    right: 103px;
    border-radius: 50%;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">



    <div class="wrapper">



        @include('agent.includes.top-nav')



        <div class="content-wrapper">



            <div class="container-full clearfix position-relative"> 



                @include('agent.includes.nav')



                <div class="content">







                    <div class="content-header">



                        <div class="d-flex align-items-center">



                            <div class="mr-auto">



                                <h3 class="page-title">Hotel Details</h3>



                                <div class="d-inline-block align-items-center">



                                    <nav>



                                        <ol class="breadcrumb">



                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>



                                            <li class="breadcrumb-item" aria-current="page">Home



                                                <li class="breadcrumb-item active" aria-current="page">Hotel Details



                                                </li>



                                            </ol>



                                        </nav>



                                    </div>



                                </div>


                            </div>



                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="box">

                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p class="para-h">{{$get_hotels->hotel_name}} <span class="badge-icon">
                                                    @for($i=1;$i<=$get_hotels->hotel_rating;$i++)
                                                    <span class="fa fa-star"></span> 
                                                    @endfor
                                                </span></p>
                                                @if($hotel_type!="")
                                                <a href="#" class="img-div">
                                                    <i class="  fa fa-building-o"></i>                          
                                                </a>
                                                <p class="hotel-type">{{$hotel_type}}</p>
                                                @endif
                                                <p class="address"><i class="fa fa-map-marker"></i> #{{$get_hotels->hotel_address}}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="box">
                                                            <div class="box-body">
                                                                <div class="flexslider2">
                                                                    <ul class="slides">
                                                                     @php
                                                                     $hotelimage=unserialize($get_hotels->hotel_images);
                                                                     @endphp


                                                                     @if(!empty($hotelimage))
                                                                     @php
                                                                     for($images=0;$images< count($hotelimage);$images++)
                                                                     {
                                                                        if(count($hotelimage)==1)
                                                                        {
                                                                            @endphp
                                                                            <li
                                                                            data-thumb="{{asset('assets/uploads/hotel_images')}}/{{$hotelimage[$images]}}">
                                                                            <img src="{{asset('assets/uploads/hotel_images')}}/{{$hotelimage[$images]}}"
                                                                            alt="slide" />
                                                                        </li>

                                                                            @php
                                                                        }
                                                                        @endphp
                                                                        <li
                                                                        data-thumb="{{asset('assets/uploads/hotel_images')}}/{{$hotelimage[$images]}}">
                                                                        <img src="{{asset('assets/uploads/hotel_images')}}/{{$hotelimage[$images]}}"
                                                                        alt="slide" />

                                                                    </li>

                                                                    @php
                                                                }

                                                                @endphp
                                                                @else

                                                                <li
                                                                data-thumb="{{asset('assets/images/no-photo.png')}}">
                                                                <img src="{{asset('assets/images/no-photo.png')}}"
                                                                alt="slide" />
                                                            </li>

                                                            @endif

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="section">
                             <!--    <p class="star-p"> <i class="fa fa-star star"></i> One of our bestsellers in Amritsar!
                             </p> -->
                             <p class="star-p"> <i class="fa fa-star star"></i> DESCRIPTION
                             </p>
                             <p> {{$get_hotels->hotel_description}}
                             </p>
                         </div>
                         <hr style="border-color: #ffc0c0;width: 100%;">
                     </div>
                    <?php 
                        $booking_room_with_qty=array();  
                        $count=0;
                        foreach($past_bookings_array as $past_bookings)
                        {
                           //  $other_info=unserialize($past_bookings['booking_other_info']);

                           // for($i=0;$i< count($other_info);$i++)
                           // {
                           //     $booking_room_with_qty[$count]['room_name']=$other_info[$i]['room_name'];
                           //      $booking_room_with_qty[$count]['room_quantity']=$other_info[$i]['room_qty'];
                           //      $count++;
                                
                           // }
                             $room_count_details=explode(",",$past_bookings['booking_rooms_count_details']);

                          for($index=0;$index< count($room_count_details);$index++)
                          {
                            $get_room_array=explode("---",$room_count_details[$index]);

                            $booking_room_with_qty[$index]['room_id']=$get_room_array[0];
                             $booking_room_with_qty[$index]['room_occupancy_id']=$get_room_array[1];
                            $booking_room_with_qty[$index]['room_qty']=$get_room_array[3];

                          }


                           
                        }
                        //  if(count($booking_room_with_qty)>0)
                        //     {
                        // $booking_room_with_qty = array_reduce($booking_room_with_qty, function ($a, $b) {
                        //         isset($a[$b['room_name']]) ? $a[$b['room_name']]['room_quantity'] += $b['room_quantity'] : $a[$b['room_name']] = $b;  
                        //         return $a;
                        //     });
                           
                        //          $booking_room_with_qty = array_values($booking_room_with_qty);
                        //     }
                       

                     ?>
                      <!--   <div class="col-md-4">
                            <div class="sidebar-div">
                                <p class="s-head">Guests love</p>
                                <div class="review">
                                    <i class="fa fa-star star"></i>
                                    <div style="margin-left: 15px;" class="r-div">
                                        <p class="f-head">“golden temple”</p>
                                        <p>13 related reviews</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <i class="fa fa-star star"></i>
                                    <div style="margin-left: 15px;" class="r-div">
                                        <p class="f-head">“walking distance”</p>
                                        <p>13 related reviews</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <i class="fa fa-star star"></i>
                                    <div style="margin-left: 15px;" class="r-div">
                                        <p class="f-head">“minutes walking”</p>
                                        <p>13 related reviews</p>
                                    </div>
                                </div>
                                <div class="review">
                                    <i class="fa fa-map-marker" style="padding: 8px;"></i>
                                    <div style="margin-left: 15px;" class="r-div">
                                        <p>Top location: Highly rated by recent guests (9.3)</p>

                                    </div>
                                </div>
                                <p class="s-head-2">Breakfast info</p>
                                <p>Vegetarian, Asian</p>
                                <p class="parking"><i class="">P</i> Private parking at the hotel</p>
                                <button class="review-btn">Review</button>
                            </div>
                        </div> -->

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="facilities">
                                <p class="facillity-head">Most popular facilities</p>
                                <div class="facilities-div">
                                    <p class="span-f"><i class="fa fa-wifi"></i> Free WiFi</p>
                                    <p class="span-f"><i class="fa fa-automobile"></i> Parking</p>
                                    <p class="span-f"><i class="fa fa-users"></i>  Family rooms</p>
                                    <p class="span-f"><i class="fa fa-ban"></i> Non-smoking rooms</p>

                                </div>
                            </div> -->
                           <!--  <p class="para">Good for couples - they rate the facilities 9.0 for two-person stays.</p>
                            <div class="alert alert-success">
                                <p style="margin:0"><b>No credit card needed to book.</b> We'll send you an email confirming your reservation.</p>
                            </div>
                            <div class="alert alert-success">
                                <p style="margin:0"><b>Lock in a great price for your upcoming stay</b> </p>
                                <p style="margin:0">Get instant confirmation with FREE cancellation on most rooms!</p>
                            </div> -->
                        </div>
                    </div>
                    <div class="row" @isset($_GET['itinerary']) style="display:none" @endisset>
                       <div class="col-md-12" id="hotel_surroundings"> 
                        <div class="wrap-div">
                         <h2 class="wrap-heading">What's nearby</h2>
                         <div class="column-div">
                             
                         </div>
                     </div>
                 </div>
                         <div class="col-md-12" id="airports">
                            <div class="wrap-div">
                                <h2 class="wrap-heading">Airports *</h2>
                                <div class="column-div">
                                </div>
                            </div>

                        </div>
                    </div>

                    @if($NoOfNights!=0)
                    <h2>BOOK HOTEL</h2>
                    <!-- The Modal -->
                    <div class="modal" id="myfilter" >
                        <div class="modal-dialog">
                          <div class="modal-content">

                            <!-- Modal Header -->
                            <div class="modal-header">
                              <h4 class="modal-title">Change your details</h4>
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                          </div>

                          <!-- Modal body -->
                          <div class="modal-body">
                            <form method="get" action={{route('hotel-search')}}>
                               <input  id="hotel_country" type="hidden" name="country" value="{{$country}}">
                               <input  id="hotel_city" type="hidden" name="city" value="{{$city}}">
                               <input  id="modify" type="hidden" name="modify" value="true">
                               <div class="row mb-10">


                                <div class="col-sm-12 col-md-12 col-lg-4">

                                    <div class="form-group">

                                        <label for="hotel_date_from">CHECKIN DATE <span class="asterisk">*</span></label>
                                        <div class="input-group date">
                                            <input type="text" placeholder="CHECKIN DATE" class="form-control pull-right datepicker" id="hotel_date_from" name="date_from" readonly="readonly" required="required">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->

                                    </div>
                                </div>

                                <div class="col-sm-12 col-md-12 col-lg-4">

                                    <div class="form-group">
                                       <label for="hotel_date_to">CHECKOUT DATE <span class="asterisk">*</span></label>
                                       <div class="input-group date">
                                        <input type="text" placeholder="CHECKOUT DATE" class="form-control pull-right datepicker" id="hotel_date_to" name="date_to" readonly="readonly" required="required">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-4">
                                <div class="form-group">
                                    <label for="hotel_date_to"></label>
                                    <button id="check_availability" type="submit" class="btn btn-rounded  btn-success" disabled="disabled">Check Availability</button>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-10">
                            <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                 <label for="no_of_adults">ADULTS <span class="asterisk">*</span></label>
                                 <select name="no_of_adults" id="no_of_adults" class="no_of_adults form-control" style="color:black;" required="required">
                                    <option value="" hidden="hidden">Adults</option>
                                    @for($i=1;$i<=20;$i++)
                                    <option @if($i==$hotel_adults) selected @endif>{{$i}}</option>
                                    @endfor                                 
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-3">

                            <div class="form-group">
                                <label for="no_of_child">CHILDREN <span class="asterisk">*</span></label>
                                <select name="no_of_child" id="no_of_child" class="no_of_child form-control" style="color:black;" required="required">
                                    <option value="" hidden="hidden">Children</option>
                                    @for($i=0;$i<=10;$i++)
                                    <option @if($i==$hotel_children) selected @endif>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-3">

                            <div class="form-group">
                                <label for="no_of_child">ROOMS <span class="asterisk">*</span></label>
                                <select name="no_of_rooms" id="no_of_rooms" class="no_of_rooms form-control" style="color:black;" required="required">
                                    <option value="" hidden="hidden">Rooms</option>

                                    @for($i=1;$i<=10;$i++)
                                    <option @if($i==$room_quantity) selected @endif>{{$i}}</option>
                                    @endfor

                                </select>
                            </div>
                        </div>


                    </div>
                    <div id="child_age_div" class="row">

                       @if($hotel_children>0)

                       @for($i=0;$i< count($hotel_child_age);$i++)
                       <div class="col-md-2"> <div class="form-group"><label for="child_age{{($i+1)}}" style="color:black">Child Age {{($i+1)}}<span class="asterisk" >*</span></label><input type="text" id="child_age{{($i+1)}}" name="child_age[]" class="form-control child_age" maxlength=2 style="color:black" value="{{$hotel_child_age[$i]}}" onkeypress="javascript:return validateNumber(event)" required="required"></div></div>
                       @endfor

                       @endif    

                       
                   </div>

               </form>
           </div>



       </div>
   </div>
</div>
<div class="row" @isset($_GET['itinerary']) style="display:none" @endisset>
    <div class="col-md-12">
        <p class="h-para"></p>
        <div class="h-div">
            <div class="row" @isset($_GET['itinerary']) style="display:none" @endisset>
                <div class="col-md-3">
                    <div class="p-div" style="text-align:center">
                        <i class="fa fa-check tick-s"></i> <b>CHECKIN DATE </b> : 
                        <p> {{date('d F,Y',strtotime($checkin))}}</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="p-div" style="text-align:center">
                        <i class="fa fa-check  tick-s"></i> <b>CHECKOUT DATE </b> :  
                        <p> {{date('d F,Y',strtotime($checkout))}}</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="p-div" style="text-align:center">
                        <i class="fa fa-check tick-s"></i> <b>NO OF NIGHTS </b> : 
                        <p> {{$NoOfNights}}</p>
                    </div>
                </div>
                <div class="col-md-3">
                    <button  type="button" class="btn btn-primary change-search" data-toggle="modal" data-target="#myfilter">
                        Modify Search
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="p-div" style="text-align:center">
                        <i class="fa fa-check tick-s"></i> <b>TOTAL GUESTS </b> : 
                        <p> @php
                            $guest_data="";

                            if($hotel_adults>1)
                            $guest_data.=$hotel_adults. " adults, ";
                            else
                            $guest_data.=$hotel_adults. " adult, ";


                            if($hotel_children>1)
                            $guest_data.=$hotel_children. " children";
                            else
                            $guest_data.=$hotel_children. " child";


                            if($hotel_children>0)
                            {
                             $child_age_array=array();

                             if(!empty($hotel_child_age))
                             {
                                 foreach($hotel_child_age as $child_age)
                                 {
                                    $child_age_array[]=$child_age;
                                }
                            }

                            $guest_data.=" (".implode(',',$child_age_array).")";

                        }

                        echo $guest_data;
                        @endphp
                    </p>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
<form id="hotel_booking_form" action="{{route('hotel-booking')}}" method="post">



    {{csrf_field()}}
    <input type="hidden" name="markup" value="{{$markup}}">
    <input type="hidden" name="own_markup" value="{{$own_markup}}">
    <input type="hidden" name="currency_price" value="{{serialize($currency_price)}}">
    <input type="hidden" name="hotel_id" value="{{$get_hotels->hotel_id}}">
    <input type="hidden" name="supplier_id" value="{{$get_hotels->supplier_id}}">
    <input id="checkin" type="hidden" name="checkin_date" value="{{$checkin}}">
    <input id="checkout" type="hidden" name="checkout_date" value="{{$checkout}}">
    <input id="hotel_adults" type="hidden" name="hotel_adults" value="{{$hotel_adults}}">
    <input id="hotel_children" type="hidden" name="hotel_children" value="{{$hotel_children}}">
    <input id="hotel_child_age" type="hidden" name="hotel_child_age" value="{{serialize($hotel_child_age)}}">
    <input id="no_of_days" type="hidden" name="no_of_days" value="{{$NoOfNights}}">
    @php
    $hotel_rooms=App\HotelRooms::where('hotel_id',$get_hotels->hotel_id)->get();

     $currency=$get_hotels->hotel_currency;
    @endphp
    <div class="row" style="margin-top:20px" >
        <div class="col-md-12" >
            <table class="table table-bordered" id="room_type_table">
                <thead>
                    <tr>
                        <th>Room Type</th>
                        <th>Sleeps/ Occupancy</th>
                        <th>Meal</th>
                        <th @isset($_GET['itinerary']) style="display:none" @endisset>Today's Price</th>
                        <th @isset($_GET['itinerary']) style="display:none" @endisset>Select Rooms</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hotel_rooms as $room_key=>$room_values)
                   <?php
                    if($currency==null)
                    {
                        $currency=$room_values->hotel_room_currency;
                    }
                    $room_price_currency_rate=$currency_price[$currency];
                    $html="";
                        $get_hotel_seasons=App\HotelRoomSeasons::where('hotel_room_id_fk',$room_values->hotel_room_id)->where('hotel_room_season_validity_from','<=',$checkin)->where('hotel_room_season_validity_to','>=',$checkin)->get();
                    $sleeps_count=0;
                    $occupany_counter=0;
                        foreach($get_hotel_seasons as $hotel_seasons)
                        {
                            $get_occupancy=App\HotelRoomSeasonOccupancy::where('hotel_room_season_id_fk',$hotel_seasons->hotel_room_season_id)->get();
                             $sleeps_count+=count($get_occupancy);
                            foreach($get_occupancy as $occupancy)
                            {
                                if($occupany_counter!=0)
                                {
                                 $html.="<tr>"; 
                                }
                                 $html.='<td><input type="hidden" name="room_occupancy_id['.$room_key.']['.($occupany_counter).']" value="'.$occupancy->hotel_room_occupancy_id.'"><input type="hidden" name="room_occupancy_qty['.$room_key.']['.($occupany_counter).']" value="'.$occupancy->hotel_room_occupancy_qty.'">';
                                 for($sleeps=1;$sleeps<=$occupancy->hotel_room_occupancy_qty;$sleeps++)
                                 {
                                    $html.='<span class="fa fa-user"></span>';
                                 }


                                 $html.='</td>';
                                  foreach($hotel_meal as $meal)
                                  {
                                    if($meal->hotel_meals_id==$room_values->hotel_room_meal)
                                    {
                                      $html.="<td>$meal->hotel_meals_name</td>";  
                                    }
                                  }
                                   $past_booking_qty=0;
                                    foreach($booking_room_with_qty as $room_with_qty_key=>$room_with_qty_value)
                                    {
                                      if($room_with_qty_value['room_id']==$room_values->hotel_room_id)
                                      {
                                        $past_booking_qty+=$room_with_qty_value['room_qty'];
                                      }

                                    }
                                   $markup_cost=round((round($occupancy->hotel_room_occupancy_price*$room_price_currency_rate)*$markup)/100);
                                    $total_agent_cost=round((round($occupancy->hotel_room_occupancy_price*$room_price_currency_rate)+$markup_cost));

                                    $own_markup_cost=round(($total_agent_cost*$own_markup)/100);

                                    $total_cost=round(($total_agent_cost+$own_markup_cost));

                                  $html.='<td> <input type="hidden" name="room_price_convert_rate['.$room_key.']" value="'.$room_price_currency_rate.'">  <input type="hidden" name="room_price['.$room_key.']['.($occupany_counter).']" value="'.($NoOfNights*round($occupancy->hotel_room_occupancy_price*$room_price_currency_rate)).'"><input type="hidden" name="room_agent_price_markup['.$room_key.']['.($occupany_counter).']" value="'.($NoOfNights* $total_agent_cost).'"><input type="hidden" name="room_price_markup['.$room_key.']['.($occupany_counter).']" value="'.($NoOfNights* $total_cost).'"><input type="hidden" name="room_price_currency['.$room_key.']['.($occupany_counter).']" value="GEL"> <button type="button" class="show-price"> GEL '.($total_cost*$NoOfNights).'</button>  </td><td>
                                <select name="room_select['.$room_key.']['.($occupany_counter).']" class="form-control room_select room_select__'.$room_key.'" id="room_select__'.$room_key.'__'.($occupany_counter+1).'">
                                        <option value="0">0</option>';
                                        for($i=1;$i<=($room_values->hotel_room_no_of_rooms-$past_booking_qty);$i++)
                                        {
                                          $html.="<option value='".$i."'>".$i." ( GEL ".($total_cost*$NoOfNights*$i)." )</option>";  
                                        }
                                    
                                  $html.="</select>";
                                    if($room_values->hotel_room_extra_bed_price!="" && $room_values->hotel_room_extra_bed_price!=null)
                                    {

                                    
                                    $markup_cost=round((round($room_values->hotel_room_extra_bed_price*$room_price_currency_rate)*$markup)/100);
                                    $total_agent_bed_cost=round((round($room_values->hotel_room_extra_bed_price*$room_price_currency_rate)+$markup_cost));

                                    $own_markup_cost=round(($total_agent_bed_cost*$own_markup)/100);

                                    $total_bed_cost=round(($total_agent_bed_cost+$own_markup_cost));

                                    
                                    $html.='<input type="checkbox" id="room_extra_bed__'.$room_key.'__'.($occupany_counter+1).'" name="room_extra_bed['.$room_key.']['.($occupany_counter).']" class="form-control extra_bed_select" value="Yes">
                                    <label for="room_extra_bed__'.$room_key.'__'.($occupany_counter+1).'">GEL '.($NoOfNights*$total_bed_cost).' for Extra Bed</label>

                                     <input type="hidden" name="room_extra_bed_price['.$room_key.']['.($occupany_counter).']" value="'.($NoOfNights*round($room_values->hotel_room_extra_bed_price*$room_price_currency_rate)).'"> 

                                       <input type="hidden" name="room_agent_extra_bed_price['.$room_key.']['.($occupany_counter).']" value="'.($NoOfNights* $total_agent_bed_cost).'"> 
                                    <input type="hidden" name="room_extra_bed_price_markup['.$room_key.']['.($occupany_counter).']" value="'.($NoOfNights*$total_bed_cost).'">';
                                    } 





                                  $html.="</td></tr>";
                                   $occupany_counter++;

                            }
                        }
                    ?>
                    <tr>
                        <td rowspan={{$sleeps_count}} class="room_details text-primary" id="room_details__{{$room_values->hotel_room_id}}" style="cursor: pointer;">
                            <input type="hidden" name="room_id[{{$room_key}}]" value="{{$room_values->hotel_room_id}}">
                            <input type="hidden" name="room_name[{{$room_key}}]" value="{{ucwords($room_values->hotel_room_class)}} {{ ucwords($room_values->hotel_room_type)}} Room">
                           {{ ucwords($room_values->hotel_room_class)}} {{ ucwords($room_values->hotel_room_type)}} Room
                                    ({{$room_values->hotel_room_adults}} Adults + @if($room_values->hotel_room_cwb=="") 0
                                    @else {{$room_values->hotel_room_cwb}} @endif Child)
                        </td>
                        <?php echo $html; ?>
                  
                    @endforeach


                </tbody>
            </table>
        </div>
    </div>
    <div class="check-form"  @isset($_GET['itinerary']) style="display:none" @endisset>

        <div class="row">
            <div class="col-sm-6 col-md-5">
                <span><b>PARTICULARS</b></span>
            </div>
            <div class="col-sm-6 col-md-3">
                 <span><b>TOTAL PRICE</b></span>
            </div>
            <div class="col-sm-6 col-md-3">
            </div>
            
        </div>

        <div class="row">

                        <div class="col-sm-6 col-md-5" style="width: 100%;display:none" >
                            <div class="form-group">
                               <label for="room_quantity">Room Quantity<span class="asterisk">*</span></label>
                               <select class="form-control"  name="room_quantity" required="required" >
                                <option value="0">Select Room Quantity</option>
                                @php
                                for($i=1;$i<=10;$i++)
                                {
                                    if($i==$room_quantity)
                                    {
                                       echo '<option selected="selected">'.$i.'</option>';
                                   }
                                   else
                                   {
                                    echo '<option>'.$i.'</option>';
                                }

                            }

                            @endphp
                        </select>
                    </div>
                </div>

                <div class="col-sm-6 col-md-5" id="total_div" style="display: none">
                    <div class="form-group">
                        <br>
                        <span>
                       <!--      <strong>TOTAL :</strong> -->
                        </span> <span id="room_price"></span>

                    </div>
                </div>

                <div class="col-sm-6 col-md-3" id="total_price_div" style="display: none">
                    <div class="form-group">
                        <br>
                       <span id="total_price"></span>

                    </div>
                </div>



                <div class="col-sm-6 col-md-3" id="submit_div" style="display: none">
                    <div class="form-group">
                     <br>
                     <button type="submit" class="btn btn-rounded btn-success">CONTINUE</button>

                 </div>
             </div>
                        <div class="col-sm-6 col-md-2">

                        </div>
                    </div>
                </div>

            </form>
            @endif
            @php
            $get_reasons=unserialize($get_hotels->hotel_reasons);

            @endphp
            @if($get_reasons!="" || $get_reasons!=null)
            @if($get_reasons[0]['reason_name']!="")
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <p class="h-para">@php echo count($get_reasons) @endphp reasons to choose {{$get_hotels->hotel_name}}</p>
                    <div class="h-div">
                        <div class="row">
                            @for($i=0;$i< count($get_reasons);$i++)
                            <div class="col-md-6">
                                <div class="p-div">
                                    <i class="fa fa-check tick-s"></i> {{$get_reasons[$i]['reason_name']}}
                                </div>
                            </div>
                            @endfor

                        </div>
                    </div>

                </div>
            </div>
            @endif
            @endif
            <hr>
            @php
            $get_amenities=unserialize($get_hotels->hotel_amenities);
            @endphp
            @if($get_amenities==null || $get_amenities=="")

            @else

            <div class="row">
                <h3 style="width:100%;display:block;padding: 0px 0px 10px;margin-left: 15px;color: #9C27B0;border-bottom: 2px solid #e5e5e5;
                ">Most popular facilities</h3>
                       <!--  <div class="icon-i">
                        <span class="span-f"><i class="fa fa-wifi"></i> Free WiFi</span>
                                    <span class="span-f"><i class="fa fa-automobile"></i> Parking</span>
                                    <span class="span-f"><i class="fa fa-users"></i>  Family rooms</span>
                                    <span class="span-f"><i class="fa fa-ban"></i> Non-smoking rooms</span>

                                </div> -->


                                <div class="col-md-12">
                                 <div class="most-p-div">
                                   <div class="row">
                                    @foreach($get_amenities as $amenities)
                                    @if(!empty($amenities[1]))
                                    <div class="col-md-4">
                                        <h3>@php
                                            $get_amenities_name=LoginController::fetchAmenitiesName($amenities[0]);

                                            echo $get_amenities_name['amenities_name'];

                                        @endphp</h3>
                                        <div class="list-div">
                                            @foreach($amenities[1] as $sub_amenities)

                                            <p class="span-f2"><i class="fa fa-check"></i>
                                                @php
                                                $get_sub_amenities_name=LoginController::fetchSubAmenitiesName($sub_amenities,$amenities[0]);

                                                echo $get_sub_amenities_name['sub_amenities_name'];

                                                @endphp
                                            </p>

                                            @endforeach
                                        </div>
                                    </div>

                                    @endif

                                    @endforeach
                                </div>
                            </div>



                           <!--   <div class="row">
                                <div class="col-md-4">
                                    <h3>Outdoors</h3>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    


                                </div>
                                <div class="col-md-4">
                                    <h3>Parking</h3>
                                    <p>Private parking is possible on site (reservation is needed) and costs INR 100 per day.</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Secured parking</p>

                                </div>
                                <div class="col-md-4">
                                    <h3>Business facilities</h3>
                                    <p class="span-f2"><i class="fa fa-check"></i>Fax/photocopying</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <h3>Pets</h3>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>


                                </div>
                                <div class="col-md-4">
                                    <h3>Transport</h3>
                                    <p>Private parking is possible on site (reservation is needed) and costs INR 100 per day.</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Secured parking</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                </div>
                                <div class="col-md-4">
                                    <h3>General</h3>
                                    <p class="span-f2"><i class="fa fa-check"></i>Fax/photocopying</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>


                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <h3>Food & Drink</h3>
                                    <p class="span-f2"><i class="fa fa-check"></i>Terrace</p>
                                    


                                </div>
                                <div class="col-md-4">
                                    <h3>Reception services</h3>
                                    <p>Private parking is possible on site (reservation is needed) and costs INR 100 per day.</p>
                                    <p class="span-f2"><i class="fa fa-check"></i>Secured parking</p>

                                </div>
                                <div class="col-md-4">
                                    <h3>Languages spoken</h3>
                                    <p class="span-f2"><i class="fa fa-check"></i>Fax/photocopying</p>


                                </div>
                            </div> -->
                        </div>

                    </div>
                    @endif
                    <div class="row" style="margin-top:30px">
                        <div class="col-md-12">
                            <div class="see-avail">
                                <div class="">
                                    <p class="para-h" style="margin:0;color:black">House rules </p>
                                    <p class="address">{{$get_hotels->hotel_name}} takes special requests - add in the next step!</p>
                                </div>

                                <!-- <button class="show-price">See Availability</button> -->
                            </div>

                            <hr>
                        </div>

                        <div class="col-md-12">
                            <div class="table-div">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr style="display:none;">
                                                <th style="width:30%;">Hotel Inclusions</th>
                                                <td>@php echo $get_hotels->hotel_inclusions @endphp</td>

                                            </tr>
                                            <tr style="display:none;">
                                                <th style="width:30%;">Hotel Exclusions</th>
                                                <td>@php echo $get_hotels->hotel_exclusions @endphp</td>

                                            </tr>
                                            <tr>
                                                <th style="width:30%;">Hotel Cancellation Policy</th>
                                                <td>@php echo $get_hotels->hotel_cancel_policy @endphp</td>

                                            </tr>
                                            <tr>
                                                <th style="width:30%;">Hotel Terms and Conditions  </th>
                                                <td>@php echo $get_hotels->hotel_terms_conditions @endphp</td>

                                            </tr>
                                            @php
                                            $get_policies=unserialize($get_hotels->hotel_other_policies);
                                            @endphp
                                            @if($get_policies!="" || $get_policies!=null)


                                            @for($i=0;$i< count($get_policies);$i++)
                                            <tr>
                                                <th style="width:30%;">{{$get_policies[$i]['policy_name']}}</th>
                                                <td>{{$get_policies[$i]['policy_desc']}}</td>
                                            </tr>
                                            @endfor
                                            @endif

                                        </tbody>
                                    </table>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



</div>



</div>







@include('agent.includes.footer')



@include('agent.includes.bottom-footer')
<style>
    ul.list-ul {
        column-count: 2;
        list-style: none;    
        column-gap: 40px;

    }
    p.list-heading {
        color: #9C27B0;
        font-size: 17px;
    }
    ul.list-ul li{
        position: relative;
    }
    ul.list-ul li:before {
        content: "";
        width: 12px;
        height: 2px;
        padding: 2px;
        position: absolute;
        border-top: none !important;
        border-right: none !IMPORTANT;
        border-width: 1.3px;
        border-color: black;
        border-style: solid;
        left: -23px;
        top: 8px;
        transform: rotate(-39deg);
    }
    p.bed-div {
        margin-top: 10px;
        font-size: 16px;
        color: black;
    }
    i.icon-tag {
        margin-right: 5px;
    }
    button#include-hotel {
        background: white;
        margin-left: auto;
        display: block;
        font-size: 16px;
        font-weight: 700;
        color: #8540ec;
        float: none;
        border-radius: 50px;
        border: 1px solid gainsboro;
        margin-bottom: 5px;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
    }
    button#include-hotel:hover{
        background: white !important;
    }

</style>
<div class="modal" id="myModal">
    <div class="modal-dialog" style="max-width: 800px;">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title" id="room-name-heading">Double or Twin Room</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
         <div id="room_details_view">
         </div>
     </div>

     <!-- Modal footer -->
     <div class="modal-footer" style=" padding: 13px;display: none">
      <button type="button" id="include-hotel" class="btn btn-danger" data-dismiss="modal">Book This Room</button>

  </div>
  
</div>
</div>
</div>
<script>
  var includeDiv=$("#include-hotel");
//   var label=undefined;
//   $(".room_details").click(function(){
//     label=this.parentElement.previousElementSibling.children[0].nextElementSibling;

// })
//   $("#include-hotel").on("click",function(){
//     label.click();
// })


</script>
<script>

    $(document).ready(function()
    {
        $(".flexslider2").flexslider({animation: "slide",controlNav: "thumbnails"});
        var date = new Date();
        date.setDate(date.getDate());
        $('#hotel_date_from').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        }).on('changeDate', function (e) {
            var date_from = $("#hotel_date_from").datepicker("getDate");
            var date_to = $("#hotel_date_to").datepicker("getDate");

            if(!date_to)
            {
               date_from.setDate(date_from.getDate()+1); 
               $('#hotel_date_to').datepicker("setDate",date_from);
           }
           else if(date_to<date_from)
           {
               date_from.setDate(date_from.getDate()+1); 
               $('#hotel_date_to').datepicker("setDate",date_from);
           }
       });

        $('#hotel_date_to').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        }).on('changeDate', function (e) {
            var date_from = $("#hotel_date_from").datepicker("getDate");
            var date_to = $("#hotel_date_to").datepicker("getDate");

            if(!date_from)
            {
                $('#hotel_date_from').datepicker("setDate",date_to);
            }
            else if(date_to<date_from)
            {
                $('#hotel_date_from').datepicker("setDate",date_to);
            }
        });


        $("#hotel_booking_form").on("submit",function(e)
        {
            var check_room=0;

            $(".room_select").each(function()
            {
                if($(this).val()>0)
                {
                   check_room++; 
                }
            });
         if(check_room==0)
         {
           $("#room_type_table").css("border","1px solid #ec407a");
           alert("Please Select Room first");
           e.preventDefault();
       }
       else
       {
           $("#room_type_table").css("border","none");
       }

   });

        // $(document).on("change","input[name='room_type'],select[name='room_quantity']",function()
        // {
        //     var room_index=$("input[name='room_type']:checked").val();
        //     var price=$("input[name='room_price_markup_"+room_index+"']").val();
        //     var currency=$("input[name='room_price_currency_"+room_index+"']").val();
        //     var room_quantity=$("select[name='room_quantity']").val();

        //     var total_price=parseFloat(parseFloat(price)*parseFloat(room_quantity)).toFixed(1);

        //     var price_show=room_quantity+" x "+price+" = "+currency+" "+total_price;
        //     $("#total_div").show();
        //     $("#total_price").text(price_show);



        // });

        $(document).on("change",".room_select,.extra_bed_select",function()
        {
        
             var price_show="";
            var total_full_price=0;
            $('.room_select').each(function()
            {
                if($(this).val()>0)
                {
                      var room_index=$(this).attr("id").split("__")[1];
                      var occupancy_index=$(this).attr("id").split("__")[2];
                      occupancy_index=parseInt(occupancy_index)-1;
                       var room_name=$("input[name='room_name["+room_index+"]").val();
                        var price=$("input[name='room_price_markup["+room_index+"]["+occupancy_index+"]']").val();
                        var currency=$("input[name='room_price_currency["+room_index+"]["+occupancy_index+"]']").val();
                        var room_quantity=$(this).val();
                    var total_price=parseFloat(parseFloat(price)*parseFloat(room_quantity)).toFixed(1);
                    total_full_price+=parseFloat(total_price);
                     price_show+=room_quantity+" "+room_name+" x "+price+" = "+currency+" "+total_price;
                 
                         price_show+="<br>";

                         if($("#room_extra_bed__"+room_index+"__"+occupancy_index).prop('checked') == true)
                         {

                            var price=$("input[name='room_extra_bed_price_markup["+room_index+"]["+occupancy_index+"]']").val();
                            var currency=$("input[name='room_price_currency["+room_index+"]["+occupancy_index+"]']").val();
                            var room_quantity=$(this).val();
                            var total_price=parseFloat(parseFloat(price)*parseFloat(room_quantity)).toFixed(1);
                            total_full_price+=parseFloat(total_price);
                             price_show+=room_quantity+" Extra Bed x "+price+" = "+currency+" "+total_price;
                     
                             price_show+="<br>";
                         }

                }
              


            });

            $("#total_div").show();
            $("#total_price_div").show();
            $("#submit_div").show();
            $("#room_price").html(price_show);
             $("#total_price").text(total_full_price);



        });


        $("#hotel_surroundings").html("<h3><b>Near By Places Loading.....</b></h3>");
        $("#airports").html("<h3><b>Closest Airport Loading.....</b></h3>");
        var hotel_id=$("input[name='hotel_id']").val();
        $.ajax({
            url:"{{route('hotel-surroundings')}}",
            type:"POST",
            data:{"_token":"{{csrf_token()}}",
            "id":hotel_id},
            dataType:"JSON",
            success:function(response)
            {

              $("#hotel_surroundings").html(response.famousHtml);

               $("#airports").html(response.airportHtml);

              var sorthotels=$("div#hotel_surroundings").find(".info-div"); 
              var rating_low_high_div_hotel = sorthotels.sort(function (a, b) {  
               a = parseFloat($(a).find(".distance").text());
               b = parseFloat($(b).find(".distance").text());
                if(a > b)
                     return 1;
                else if(a < b)
                    return -1;
                else
                    return 0;
            });

               var sortairports=$("div#airports").find(".info-div"); 
              var rating_low_high_div_airport = sortairports.sort(function (a, b) {  
               a = parseFloat($(a).find(".distance").text());
               b = parseFloat($(b).find(".distance").text());
                if(a > b)
                     return 1;
                else if(a < b)
                    return -1;
                else
                    return 0;
            });
              $("#hotel_surroundings").find('.column-div').html(rating_low_high_div_hotel);
              $("#airports").find('.column-div').html(rating_low_high_div_airport);

              
          }
      });





    });

    $(document).on("click",".room_details",function()
    {
        var id=this.id;
        var hotel_id=$("input[name='hotel_id']").val();
        var room_id=id.split('__')[1];
        var room_name=$(this).text();
        $("#room-name-heading").text(room_name);
        $("#room_details_view").html("<h4><b>Loading...</h4></b>");
        $("#myModal").modal("show");
        $.ajax({
            url:"{{route('hotel-room-detail')}}",
            type:"GET",
            data:{"hotel_id":hotel_id,
            "room_id":room_id},
            success:function(response)
            {
                $("#room_details_view").html(response);
                $("#room-name-heading").text(room_name);
                $("#myModal").modal("show");
                $(".flexslider1").flexslider({animation: "slide",controlNav: "thumbnails"});



            }
        });
    });


    $(document).on("change","#hotel_date_from,#hotel_date_to",function()
    {
        if($("#hotel_date_from").val()!="" && $("#hotel_date_from").val()!="")
        {
            $("#check_availability").removeAttr('disabled');
        }
    });

//child age for no of child
$(document).on("change","#no_of_child",function()
{
    var html_data="";
    var child_count=$(this).val();
    for($i=1;$i<=child_count;$i++)
    {
        html_data+='<div class="col-md-2"> <div class="form-group"><label for="child_age'+$i+'" style="color:black">Child Age '+$i+' <span class="asterisk" >*</span></label><input type="text" id="child_age'+$i+'" name="child_age[]" class="form-control child_age" style="color:black"  maxlength=2 onkeypress="javascript:return validateNumber(event)" required></div></div>'
    }
    $("#child_age_div").html(html_data);
    $("#child_age_div").show();
});

$(document).on("click",".book-btn",function()
{
    $(this).parent().parent().find("label").trigger("click");

    $(".book-btn").each(function()
    {
        $(this).text("Book this room").removeClass("selected-room");
    });

    $(this).text("Selected").addClass("selected-room");
});


$(document).on("change",".room_select",function()
{
    var room_select_type=$(this).attr("id");

    var actual_id=room_select_type.split("__")[1];

    var room_option_count=parseInt($("#"+room_select_type+" option").length)-1;
    var room_value=$(this).val();
    var room_total=0;
    $(".room_select__"+actual_id).each(function()
    { var id=$(this).attr("id");
        if(id!=room_select_type)
        {
        room_total+=parseInt($(this).val());
    }
    });


   $(".room_select__"+actual_id).each(function()
    {

        var id=$(this).attr("id");
        if(id!=room_select_type)
        {
            // alert((room_option_count-room_value));
            $("#"+id+" option").each(function(i,value)
            {
                if(i>(room_option_count-room_value))
                {
                    $(this).attr("disabled","disabled");
                }
                else
                {
                     $(this).removeAttr("disabled");
                }
            });
        }
        else
        {

          $("#"+id+" option").each(function(i,value)
            {
                if(i>(room_option_count-room_total))
                {
                    $(this).attr("disabled","disabled");
                }
                else
                {
                     $(this).removeAttr("disabled");
                }
            });  
        }
        
    });


});
</script>

