<?php
use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\LoginController;
?>
@include('agent.includes.top-header')


<style>
    .driver-div {
    cursor: pointer;
}
      .card {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
  position:relative;
}
.title {
    color: grey;
    margin-top: 0px;
    min-height: 80px;
}
p.price-p {
    margin-top: 5px !important;
    margin-bottom: 0;
    font-size: 13px;
    text-align: center;
    color: #ff8f00;
    background: #000000;
    border-radius: 50px;
    padding: 5px 10px;
    display: block;
    /* border-radius: 24px; */
    width: 100px;
    margin: auto;
}
.radio-div {
    position: absolute;
    top: 0;
    left: 0;
}
p.title ~ p {
    /* color: red; */
    min-height: 50px;
}
button.button-guide {
    background: #ee1044;
    border: 1px solid #ee1044;
    color: white;
    padding: 8px 10px;
    display: block;
    width: 100%;
    border-radius: 5px;
    font-size: 16px;
    cursor: pointer;
}
/* h2.g-name {
    position: absolute;
    top: 48%;
    left: 0;
    transform: translate(0%, -50%);
    background: black;
    color: white;
    padding: 10px 20px;
    width: auto;
    font-size: 13px;
    margin: 0;
    margin-bottom: 10px;
    text-align: center;
    border-top-right-radius: 25px;
    border-bottom-right-radius: 25px;
} */
h2.g-name {
    color: black;
    padding: 0;
    width: auto;
    font-size: 17px;
    margin: 0;
    margin-bottom: 10px;
    text-align: center;
}
.card.driver-card {
    margin-bottom: 0 !important;
    border-color: #fed6d3;
    min-height: 330px;
    padding-bottom: 5px;
}
p.price-p {
    margin-top: 0px !important;
    margin-bottom: 0;
    font-size: 13px;
    text-align: center;
    color: #ff8f00;
    background: #000000;
    border-radius: 0px;
    padding: 5px 10px;
    display: block;
    width: 100%;
    margin: 0 auto 30px;
    border: 1px solid black;
}
.card.driver-card {
    border: 1px solid gainsboro;
    border-radius: 0;
}

span.g-badge {
    background: #ffc8c7;
    padding: 4px 9px;
    margin-right: 6px;
    color: #ee1044;
    display: inline-block;
    margin-top: 10px;
    font-size: 10px;
    border-radius: 3px;
}
button.button-guide:hover {
    background: none;
    border: 1px solid #ee1044;
    color: #ee1044;
    padding: 8px 10px;
    display: block;
    width: 100%;
    border-radius: 5px;
    font-size: 16px;
    cursor: pointer;
}
a.g-link {
    display: block !important;
    width: 100%;
    margin-bottom: 14px;
}
.flexslider2 {
    width: 100%;
    height: auto;
}
li.flex-active-slide {
    height: 500px;
}
.flexslider2 .slides img {
    width: auto;
    height: 100%;
    margin: auto;
    display: block;
}
.flex-control-thumbs img{
    opacity:1;
}
    .flex-viewport {
        height: 500px;
        background: rgb(14, 14, 14);
    }
    .flex-silder-div .flex-viewport {
        height: 300px;
    }

    .flex-viewport {
        max-height: 100%;
        -webkit-transition: all 1s ease;
        -moz-transition: all 1s ease;
        -ms-transition: all 1s ease;
        -o-transition: all 1s ease;
        transition: all 1s ease;
    }

    /*ul.slides li img {
        height: 100% !important;
        width: 100% !important;
    }*/

    .flex-control-thumbs img {
        width: 100%;
        height: 100%;
        display: block;
        opacity: .7;
        cursor: pointer;
        -moz-user-select: none;
        -webkit-transition: all 1s ease;
        -moz-transition: all 1s ease;
        -ms-transition: all 1s ease;
        -o-transition: all 1s ease;
        transition: all 1s ease;
    }

    .flex-control-thumbs li {
        width: 25%;
        float: left;
        margin: 0;
        height: 100%;
    }

    ol.flex-control-nav.flex-control-thumbs {
        height: 100px;
    }

    p.para-h {
        font-size: 21px;
    }

    span.badge-icon {
        background: #d7caff;
        padding: 5px;
        border-radius: 5px;
        font-size: 13px;
        color: #6649c7;
        margin-left: 10px;
        display: inline-block;
    }

    p.para-h {
        font-size: 21px;
        display: flex;
        justify-content: flex-start;
        align-items: end;
        /* margin-left: 10px; */
    }
ul.slides {
    /* width: 100% !important; */
    height: 100% !important;
}

li.flex-active-slide {
    height: 100%;
}
   
    .section {
        padding: 15px;
    }

    p.star-p {
        font-size: 18px;
        color: #d23d64;
    }

    i.fa.fa-star.star {
        color: white;
        background: orange;
        padding: 5px;
        border-radius: 50%;
    }

    .sidebar-div {
        background: #ffe5e5;
        border: 1px solid #ff9494;
        border-radius: 5px;
        padding: 15px;
    }

    p.s-head {
        color: #ff5e5e;
        font-size: 20px;
    }

    p.s-head-2 {
        color: #5f5f5f;
        font-size: 16px;
        font-weight: 600;
        margin-bottom: 0;
    }

    .review {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .r-div p {
        margin: 0;
        color: #424242;
        font-size: 14px;
        text-transform: capitalize;
    }

    .f-head {
        color: #644ac9;
        font-weight: 600;
    }

    .parking i {
        color: white;
        font-size: 20px;
        font-weight: 700;
        background: black;
        padding: 1px 9px;
        display: inline-block;
        width: 30px;
        border-radius: 50%;
        height: 30px;
    }

    button.review-btn {
        background: #ff7272;
        border: none;
        padding: 10px;
        width: 100%;
        color: white;
    }

    p.parking {
        color: black;
    }

    p.facillity-head {
        font-size: 20px;
    }

    p.span-f {
        background: #a4ecff;
        margin: 0 15px 0 0;
        color: #00a3cc;
        padding: 5px 10px;
        border-radius: 5px;
    }

    p.span-f2 {
        background: none;
        margin: 0 15px 0 0;
        color: #4e4e4e;
        padding: 5px 10px;
        border-radius: 5px;
    }

    .facilities-div {
        display: flex;
    }

    p.para {
        margin: 10px 0;
        color: #11aad1;
        font-size: 16px;
    }

    .alert-success {
        color: #155724 !IMPORTANT;
        background-color: #d4edda;
        border-color: #c3e6cb;
    }

    p.para {
        color: #00a3cc;
        font-size: 16px;
        margin-top: 10px;
    }

    .check-form {
        background: #313131;
        padding: 15px;
        color: white;
    }

    a.details i {
        font-size: 19px;
        margin-right: 7px;
        color: orange;
    }

    p.span-f i {
        margin-right: 5px;
    }

    a.details {
        color: #3F51B5;
        font-size: 16px;
    }

    button.show-price {
        border: none;
        background: #3F51B5;
        padding: 10px 15px;
        color: white;
    }

    .table thead {
        background: #E91E63;
        color: white;
    }

    .alert-danger {
        color: #721c24 !IMPORTANT;
        background-color: #f8d7da;
        border-color: #f5c6cb;
    }

    p.h-para {
        font-size: 20px;
    }

    .h-div {
        background: #e9f0fa;
        padding: 20px;
    }

    i.tick-s {
        color: #1550a9;
        background: white;
        padding: 10px;
        border: 1px solid #74d6ff;
        border-radius: 50%;
    }

    .p-div {
        font-size: 17px;
        color: #1f1f1f;
        font-weight: 600;
        margin-bottom: 20px;
    }

    .see-avail {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th {
        border-top: none;
        padding: 1rem;
        vertical-align: middle;
    }

    .table-div {
        background: #ddebff;
    }

    .icon-i span {
        background: #cbffcb;
        color: green;
    }

    .icon-i {
        margin-bottom: 36px;
        margin-top: 10px;
    }

    p.start_price {
        margin: 0;
    }

    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }

    .book_card {
    /* padding: 15px; */
    background: #fefeff;
    box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
    margin-bottom: 50px;
    border-radius: 5px;
    width: 100%;
}
    .hotel_detail {
        height: 150px;
    }

    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }

    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }

    .booking_detail {
        padding: 15px;
    }

    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }

    td p {
        margin: 0;
    }

    td {
        background: gainsboro;
    }

    table {
        border-collapse: separate;
    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #f44336;
    }

    a.panel-title {
        position: relative !important;
        background: #ffccc8;
        color: #f44336 !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #f44336 ; */
    }

    .panel-title {
        display: block;
        margin-top: 0;
        margin-bottom: 0;
        padding: 1.25rem;
        font-size: 18px;
        color: #4d4d4d;
        height: 48px;
    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }

    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }

    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
        width: auto;
        max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #f44336;
        z-index: 999;
        transform: none !important;
    }

    .tab-content {
        margin-top: 10px;
    }

    div.panel-heading {
        border: none !important;
    }

    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }

  .a-price {
    color: #f44336;
    font-size: 15px;
    line-height: 1;
    font-weight: 600;
    text-align: right;
    position: relative;
}
 .a-totalprice {
    color: #f44336;
    font-size: 23px;
   
    text-align: right;
    font-weight: 600;
    position: relative;
}
hr.hr-total {
    display: block;
    width: 94%;
    background: #eeeeee;
    margin: 10px auto;
}
    span.from {
        color: #f44336;
        font-size: 14px;
        display: block;
        margin-bottom: 10px;
    }

    .price-b {
        border: 3px dashed #F44336;
        padding: 10px 15px;
        text-align: center;
        margin-bottom: 20px;
    }

    .heading-name p {
        color: #FF9800;
        font-size: 25px !IMPORTANT;
    }

    .heading-name {
        border-bottom: 1px solid #adadad;
        margin-bottom: 10px;
    }

    .hotel_detail {
        background: gainsboro;
        padding: 15px;
        margin: 20px 0;
    }

    .panel-body p {
        color: black;
        font-size: 20px;
        text-transform: capitalize;
    }

    .panel-body {
        background: #f2f2f2;
    }

    .input-group-addon {
        border-color: gainsboro !important;
        border-radius: 0 !important;
        background: gainsboro !important;
    }

    input#select_date,
    select#adult_count,
    select#child_count {
        border-radius: 0;
        border-color: gainsboro;
        background: white;
    }

    span.info-s {
        color: white;
        background: #909090;
        font-size: 12px;
        padding: 2px 2px;
        width: 16px;
        font-weight: 800;
        height: 16px;
        border-radius: 50%;
        position: absolute;
        box-sizing: border-box;
        left: 80%;
        display: inline-block;
        cursor: auto;
    }
      .table-sightseeing-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
  }
  p.font-size-26.text-center.color-tiles-text a {
    color: #673AB7 !IMPORTANT;
}
p.font-size-26.text-center.color-tiles-text {
    background: #e1e1e1;
    position: absolute;
    width: 100%;
    top: 100%;
    left: 0;
    padding: 5px 10px;
    height: auto;
    display: flex;
    justify-content: center;
    color: #0088ff !important;
    border-radius: 5px;
    align-items: center;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
}
.bg-dark {
    
    height: 130px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 65px;
    position: relative;
}

label.tile-label {
    position: absolute !important;
    top: 8px !important;
    left: 20px;
    border-color: white;
    z-index: 9999;
    color: red !important;
}
p.address {
    margin-bottom: 25px;
}
ol.flex-control-nav.flex-control-thumbs li {
    flex-grow: 1;
}
ol.flex-control-nav.flex-control-thumbs {
    display: flex;
    justify-content: center;
}
ol.flex-control-nav.flex-control-thumbs li {
    flex-grow: 1;
    margin-right: 8px;
}
ol.flex-control-nav.flex-control-thumbs li:last-child {
    flex-grow: 1;
    margin-right: 0px;
  
}
.box .overlay {
    z-index: 50;
    background: rgba(103, 58, 183, 0.39);
    border-radius: 5px;
}
/* img.guide-img {
    width: 100%;
    height: 150px;
    max-height: 150px !important;
    object-fit: cover;
    object-position: 0px 0px;
} */
h2.g-name ~ p {
    min-height: 35px;
    margin-bottom: 0;
}
img.guide-img,img.driver-img{
    width: 120px;
    height: 120px;
    max-height: 150px !important;
    object-fit: cover;
    object-position: top center;
    display: block;
    margin: 15px auto;
    border-radius: 50%;
}
.container.g-body {
    height:150px;
    max-height: 150px;
}
.price-p2{
    font-size: 15px;
    color: #3F51B5;
}
.price-totalp2{
    font-size: 20px;
    color: #000000;
}
p.sight-type {
    position: absolute;
    top: 0;
    right: 13px;
    background: #ffffff;
    color: #5cb660;
    border: 1px solid #5cb660;
    padding: 4px 10px;
    border-radius: 8px;
    min-width: 99px;
    width: auto;
    text-align: center;
}
/*slider arrrow*/
.flex-direction-nav a:before {
    font-family: "flexslider-icon";
    font-size: 40px;
    display: inline-block;
    content: '\f001';
    color: rgba(255, 255, 255, 0.8);
    text-shadow: none;
}
ul.flex-direction-nav {
    position: absolute;
    top: 37%;
    left: 0;
    opacity: 1;
    display: block;
    width: 100%;
    height: 50px;
    z-index: 99999;
    transform: translateY(-50%);
}
.flex-direction-nav a {
    left: 0 !IMPORTANT;
    color: rgb(255, 255, 255) !important;
    opacity: 1;
    text-shadow: none !important;
}
.flex-direction-nav .flex-next {
    right: 5px !important;
    text-align: right;
    left: unset !important;
}

#child_age_div
{
        overflow-y: auto;
    height: auto;
    min-height: 0px;
    max-height: 300px;
    border: 1px solid #f6f2f2;
    margin: 13px 3px;
    padding: 5px;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">



    <div class="wrapper">



        @include('agent.includes.top-nav')



        <div class="content-wrapper">



            <div class="container-full clearfix position-relative">	



                @include('agent.includes.nav')



                <div class="content">







                    <div class="content-header">



                        <div class="d-flex align-items-center">



                            <div class="mr-auto">



                                <h3 class="page-title">Sightseeing Details</h3>



                                <div class="d-inline-block align-items-center">



                                    <nav>



                                        <ol class="breadcrumb">



                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>



                                            <li class="breadcrumb-item" aria-current="page">Home



                                                <li class="breadcrumb-item active" aria-current="page">Sightseeing Details



                                                </li>



                                            </ol>



                                        </nav>



                                    </div>



                                </div>


        </div>



    </div>



    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-body">
                     <div class="row">
                        <div class="col-md-12">

                            <p class="para-h">{{$get_sightseeing->sightseeing_tour_name}}</p>
                            @if($tour_type!="")
                            <p class="sight-type">{{$tour_type}}</p>
                            @endif
                        </div>
                        <div class="col-md-3">
                             <p class="address"><i class="fa fa-map-marker"></i> ( 
                                <?php
                                 $get_from_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_from,$get_sightseeing->sightseeing_country);
                                               echo $get_from_city['name']."-";

                                                if($get_sightseeing->sightseeing_city_between!=null && $get_sightseeing->sightseeing_city_between!="")
                                                {
                                               $all_between_cities=explode(",",$get_sightseeing->sightseeing_city_between);
                                               for($cities=0;$cities< count($all_between_cities);$cities++)
                                               {
                                                $fetch_city=ServiceManagement::searchCities($all_between_cities[$cities],$get_sightseeing->sightseeing_country);
                                                 echo $fetch_city['name']."-";
                                              }
                                            }
                          
                    
                                               $get_from_city=ServiceManagement::searchCities($get_sightseeing->sightseeing_city_to,$get_sightseeing->sightseeing_country);
                                                echo  $get_from_city['name'];

                                ?> ) 
                            </p>
                        </div>
                         <div class="col-md-3">
                             <p class="address">
                             <i class="fa fa-road"></i> {{$get_sightseeing->sightseeing_distance_covered}} KMS</p>
                         

                        </div>
                        <div class="col-md-3">
                             <p class="address">
                             <i class="fa fa-clock-o"></i> {{$get_sightseeing->sightseeing_duration}} HOURS</p>
                         

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="box">
                                        <div class="box-body" style="padding:0">
                                            <div class="flexslider2">
                                                <ul class="slides">
                                                    @if($get_sightseeing->sightseeing_images!=null && $get_sightseeing->sightseeing_images!="")
                                                        @php 
                                                        $sightseeing_images=unserialize($get_sightseeing->sightseeing_images);
                                                        @endphp

                                                        @if(!empty($sightseeing_images[0]))
                                                        @foreach($sightseeing_images as $sightseeing_image)
                                                        <li
                                                            data-thumb='{{asset("assets/uploads/sightseeing_images")}}/{{$sightseeing_image}}'>
                                                            <img src='{{asset("assets/uploads/sightseeing_images")}}/{{$sightseeing_image}}'
                                                                alt="slide" />
                                                        </li>
                                                        @endforeach
                                                        @else
                                                         <li
                                                            data-thumb='{{asset("assets/images/no-photo.png")}}'>
                                                            <img src='{{asset("assets/images/no-photo.png")}}' alt="slide" />
                                                        </li>


                                                        @endif

                                                    @else
                                                       <li data-thumb='{{asset("assets/images/no-photo.png")}}'>
                                                            <img src='{{asset("assets/images/no-photo.png")}}' alt="slide" />
                                                        </li>
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                              <div class="row">
                                                                  <div class="col-lg-12 col-md-12">

                                    <div class="section">
                                        <p class="star-p"> <i class="fa fa-star star"></i> Sightseeing Description
                                        </p>
                                       <p style="text-align:justify;">
                                          <?php echo $get_sightseeing->sightseeing_tour_desc; ?>
                                       </p>
                                    </div>
                                      <hr style="border-color: #ffc0c0;width: 100%;">
                                      <div class="section">
                                        <p class="star-p"> <i class="fa fa-star star"></i> Sightseeing Attractions
                                        </p>
                                       <?php echo $get_sightseeing->sightseeing_attractions; ?>
                                       
                                    </div>
                                    <hr style="border-color: #ffc0c0;width: 100%;">
                                </div>
                            </div>
                            @if(!empty($_GET['itinerary']))

                            @else
                            <div class="row" style="margin-top:20px" id="vehicle_type_div">

                                @foreach($get_vehicles as $vehicles)
                                       <!--  <i class="fa fa-user"></i> -->
                                       <div class="col-md-4 text-center parent-tile" style="cursor: pointer">
                                        <div class="box box-body bg-dark pull-up" @if($vehicles->vehicle_type_image!="")
                                        style="background: url({{ asset('assets/uploads/vehicle_type_images') }}/{{$vehicles->vehicle_type_image}});background-size: cover;background-repeat: no-repeat;" 
                                        @else 
                                        style="background: url({{asset('assets/images/vehicle-type.jpg')}});background-size: contain;background-repeat: no-repeat;" @endif>
                                            <div class="overlay"></div>
                                            <br>
                                            <p class="font-size-26 text-center color-tiles-text"> <a href="#" class="details text-light" >
                                              {{$vehicles->vehicle_type_name}}
                                              <input type="hidden" id="vehcile_type_name_{{$vehicles->vehicle_type_id}}" name="vehcile_type_name_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_name}}"></a></p>
                                              <br>
                                          </div>
                                          <input type="radio" class="with-gap radio-col-primary vehcile_type_show" name="vehcile_type" id="vehcile_type_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_id}}" data-minvalue="{{$vehicles->vehicle_type_min}}" data-maxvalue="{{$vehicles->vehicle_type_max}}">
                                          <label for="vehcile_type_{{$vehicles->vehicle_type_id}}" class="tile-label"></label>
                                      </div>
                                        
                                   
                                    
                                       
                                    
                                @endforeach                               

                       
                    </div>
                    <hr>
                    <br>
                    <p class="star-p" id="driver_div_head"> <i class="fa fa-star star"></i> Select Driver for Sightseeing
                                </p>
                    <div class="col-md-12" id="driver_div">
                    </div>
                     <br>
                     <p class="star-p" id="guide_div_head"> <i class="fa fa-star star"></i> Select Guide for Sightseeing
                                </p>
                    <div class="col-md-12" id="guide_div">
                    </div>
                    @endif
                     <div class="text-center table-sightseeing-loader" style="display: none">
                      <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                      <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 15 ; 0 -15; 0 15" 
                        repeatCount="indefinite" 
                        begin="0.1"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 10 ; 0 -10; 0 10" 
                        repeatCount="indefinite" 
                        begin="0.2"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 5 ; 0 -5; 0 5" 
                        repeatCount="indefinite" 
                        begin="0.3"/>
                    </circle>
                </svg>
            </div>
                   
                        </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="sticky-top sticky-sidebar" @isset($_GET['itinerary']) style="display:none" @endisset>
                                    <div class="row">

                                        <div class="col-md-12">
                                            

                                                <form id="sightseeing_booking_form" method="post"
                                                action="{{route('sightseeing-booking')}}">
                                            <div class="price-b">
                                                
                                                <span class="from" style="display: none">From 
                                                    @if($get_sightseeing->sightseeing_discount!=null && $get_sightseeing->sightseeing_discount!=0)
                                                    @php
                                                    $price=$get_sightseeing->sightseeing_adult_cost;

                                                    $price+=round($get_sightseeing->sightseeing_food_cost);
                                                    $price+=round($get_sightseeing->sightseeing_hotel_cost);

                                                    if($get_sightseeing->sightseeing_default_guide_price!="" && $get_sightseeing->sightseeing_default_guide_price!=null)
                                                    {
                                                        $price+=round($get_sightseeing->sightseeing_default_guide_price);
                                                    }

                                                    if($get_sightseeing->sightseeing_default_driver_price!="" && $get_sightseeing->sightseeing_default_driver_price!=null)
                                                    {
                                                        $price+=round($get_sightseeing->sightseeing_default_driver_price);
                                                    }

                                                    $markup_cost=round(($price*$markup)/100);
                                                    $total_agent_cost=round(($price+$markup_cost));

                                                    $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                    $total_cost=round(($total_agent_cost+$markup_agent_cost));

                                                    $sightseeing_discount=$get_sightseeing->sightseeing_discount;
    
                                                    $discount=round(($total_cost*$get_sightseeing->sightseeing_discount)/100);
                                                    $price_after_discount=$total_cost-$discount;
                                                     @endphp
                                                   <!--  <span id="inital_cost" class="ng-binding"><strike>
                                                        GEL  {{$total_cost}}</strike>
                                                    </span> -->
                                                    @else
                                                    @php
                                                    $sightseeing_discount=0;

                                                    @endphp

                                                    @endif
                                                </span>
                                                 @if($get_sightseeing->sightseeing_discount!=null && $get_sightseeing->sightseeing_discount!=0)
                                                 <div class="row">
                                                     <div class="col-12" style="padding:2px 15px">
                                                        <div class="row">
                                                            <div class="col-6">  <p class="price-p2">Price</p></div>
                                                            <div class="col-6">
                                                                <div class="a-price">GEL  
                                                    <span id="total_price_text">
                                                        {{$total_cost}}
                                                   </span>
                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                
                                                     </div>
                                           
                                                @endif
                                                 @if($get_sightseeing->sightseeing_discount!=null && $get_sightseeing->sightseeing_discount!=0)
                                                <div class="col-12" style="padding:2px 15px">
                                                    <div class="row">
                                                        <div class="col-6">  <p class="price-p2">{{$sightseeing_discount}} % Discount ( - )</p></div>
                                                        <div class="col-6">
                                                            <div class="a-price">GEL  
                                                                <span id="total_price_discount">
                                                                    {{$discount}}
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr class="hr-total">
                                                @endif
                                                
                                                <div class="col-12" style="padding:2px 15px">
                                                    <div class="row">
                                                        <div class="col-6">  <p class="price-totalp2">Total Price</p></div>
                                                        <div class="col-6">
                                                            <div class="a-totalprice">GEL  
                                                                 <span id="total_price_text_discount">
                                                         @if($get_sightseeing->sightseeing_discount!=null && $get_sightseeing->sightseeing_discount!=0)
                                                         {{$price_after_discount}}
                                                    @else
                                                    @php

                                                       $price=$get_sightseeing->sightseeing_adult_cost;

                                                       $price+=round($get_sightseeing->sightseeing_food_cost);
                                                       $price+=round($get_sightseeing->sightseeing_hotel_cost);

                                                        if($get_sightseeing->sightseeing_default_guide_price!="" && $get_sightseeing->sightseeing_default_guide_price!=null)
                                                    {
                                                        $price+=round($get_sightseeing->sightseeing_default_guide_price);
                                                    }

                                                    if($get_sightseeing->sightseeing_default_driver_price!="" && $get_sightseeing->sightseeing_default_driver_price!=null)
                                                    {
                                                        $price+=round($get_sightseeing->sightseeing_default_driver_price);
                                                    }



                                                       $markup_cost=round(($price*$markup)/100);
                                                    $total_agent_cost=round(($price+$markup_cost));

                                                    $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                    $total_cost=round(($total_agent_cost+$markup_agent_cost));
                                                       echo $total_cost;
                                                    @endphp
                                                    @endif </span><!--  <span class="info-s"
                                                        title="The initial price based on 1 adult">i</span> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                    
                                                </div>
                                            </div>
                                            
                                               {{csrf_field()}}
                                                <div class="book_card">
                                                    <div class="booking_detail" style="padding:0 !important">
                                                      <div class="">

                                                            <div class="form-group">
                                                                <label for="sightseeing_tour_type">TOUR TYPE<span class="asterisk">*</span></label>
                                                                <select id="sightseeing_tour_type" name="sightseeing_tour_type" class="form-control select2" style="width: 100%;">
                                                                    <option selected="selected" value="0" hidden="hidden">SELECT TOUR TYPE</option>
                                                                    <option value="private" selected="selected">PRIVATE TOUR</option>
                                                                    @if($get_sightseeing->sightseeing_group_adult_cost>0)
                                                                    <option value="group">GROUP TOUR</option>
                                                                    @endif

                                                                </select>
                                                            
                                                            </div>

                                                        </div>
                    





                                                        <div class="">

                                                            <input type="hidden"  name="markup" value="{{$markup}}">
                                                             <input type="hidden"  name="own_markup" value="{{$own_markup}}">
                                                             <input type="hidden"  id="discount" name="discount" value="{{$sightseeing_discount}}">
                                                                        <input type="hidden" name="sightseeing_id"
                                                                            value="{{$get_sightseeing->sightseeing_id}}">
                                                                        <!-- <input type="hidden" name="supplier_id"
                                                                            value="0"> -->
                                                                        <input type="hidden" name="total_price"
                                                                            id="total_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_adult_cost;
                                                       $price+=round($get_sightseeing->sightseeing_food_cost);
                                                       $price+=round($get_sightseeing->sightseeing_hotel_cost);
                                                       $markup_cost=round(($price*$markup)/100);
                                                        $total_agent_cost=round(($price+$markup_cost));
                                                         $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                        $total_cost=round(($total_agent_cost+$markup_agent_cost));
                                                       echo $total_cost;
                                                    @endphp">

                                                     <input type="hidden" id="sightseeing_food_cost" name="sightseeing_food_cost" value="@php
                                                       $price=$get_sightseeing->sightseeing_food_cost;
                                                        $markup_cost=round(($price*$markup)/100);
                                                        $total_agent_cost=round(($price+$markup_cost));

                                                         $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                        $total_cost=round(($total_agent_cost+$markup_agent_cost));

                                                       echo $total_cost;
                                                        @endphp">
                                                           <input type="hidden" id="sightseeing_hotel_cost" name="sightseeing_hotel_cost" value="@php
                                                       $price=$get_sightseeing->sightseeing_hotel_cost;
                                                    $markup_cost=round(($price*$markup)/100);
                                                        $total_agent_cost=round(($price+$markup_cost));
                                                        $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                        $total_cost=round(($total_agent_cost+$markup_agent_cost));
                                                    echo $total_cost;
                                                        @endphp">


                                                        </div>
                                                        <div class="">

                                                            <div class="form-group">
                                                                <label for="select_date">SELECT DATE <span
                                                                        class="asterisk">*</span></label>
                                                                <div class="input-group date">
                                                                    <input type="text" placeholder="FROM"
                                                                        class="form-control pull-right datepicker"
                                                                        id="select_date" name="select_date"
                                                                        readonly="readonly" value="{{$checkin}}"
                                                                        required="required">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                                <!-- /.input group -->

                                                            </div>

                                                        </div>



                                                        <div class="">

                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <label for="adult_count">ADULT<span
                                                                                class="asterisk">*</span>
                                                                           
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                         <input type="hidden" name="agent_adult_price"
                                                                            id="agent_adult_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_adult_cost;
                                                          $markup_cost=round(($price*$markup)/100);
                                                        $total_cost=round(($price+$markup_cost));
                                                       echo $total_cost;
                                                    @endphp">
                                                                        <input type="hidden" name="adult_price"
                                                                            id="adult_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_adult_cost;
                                                          $markup_cost=round(($price*$markup)/100);
                                                        $total_agent_cost=round(($price+$markup_cost));
                                                         $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                        $total_cost=round(($total_agent_cost+$markup_agent_cost));
                                                       echo $total_cost;
                                                    @endphp">
                                                     <input type="hidden" name="group_agent_adult_price"
                                                                            id="group_agent_adult_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_group_adult_cost;
                                                          $markup_cost=round(($price*$markup)/100);
                                                        $total_cost=round(($price+$markup_cost));
                                                       echo $total_cost;
                                                    @endphp">
                                                     <input type="hidden" name="group_adult_price"
                                                                            id="group_adult_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_group_adult_cost;
                                                          $markup_cost=round(($price*$markup)/100);
                                                        $total_agent_cost=round(($price+$markup_cost));
                                                        $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                        $total_cost=round(($total_agent_cost+$markup_agent_cost));
                                                       echo $total_cost;
                                                    @endphp">
                                                                        <select name="adult_count" id="adult_count"
                                                                            class="form-control" required="required">
                                                                            <option value="" selected="selected"
                                                                                hidden="">SELECT
                                                                                ADULT
                                                                            </option>
                                                                              @for($i=1;$i<=30;$i++)
                                                                           <option value="{{$i}}">{{$i}}</option>
                                                                           @endfor
                                                                        </select>
                                                                    </div>


                                                                </div>

                                                            </div>

                                                        </div>

                                                        <div class="">

                                                            <div class="form-group">
                                                                <div class="row">
                                                                    <div class="col-md-5">
                                                                        <label for="child_count">CHILD <span
                                                                                class="asterisk">*</span>
                                                                            <br>
                                                                           
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-7">
                                                                         <input type="hidden" name="agent_child_price"
                                                                            id="agent_child_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_child_cost;
                                                        $markup_cost=round(($price*$markup)/100);
                                                        $total_cost=round(($price+$markup_cost));
                                                       echo $total_cost;
                                                        @endphp">
                                                         <input type="hidden" name="group_agent_child_price"
                                                                            id="group_agent_child_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_group_child_cost;
                                                          $markup_cost=round(($price*$markup)/100);
                                                        $total_cost=round(($price+$markup_cost));
                                                       echo $total_cost;
                                                    @endphp">
                                                                        <input type="hidden" name="child_price"
                                                                            id="child_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_child_cost;
                                                        $markup_cost=round(($price*$markup)/100);
                                                        $total_agent_cost=round(($price+$markup_cost));
                                                         $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                        $total_cost=round(($total_agent_cost+$markup_agent_cost));
                                                       echo $total_cost;
                                                        @endphp">
                                                         <input type="hidden" name="group_child_price"
                                                                            id="group_child_price" value="@php
                                                       $price=$get_sightseeing->sightseeing_group_child_cost;
                                                          $markup_cost=round(($price*$markup)/100);
                                                        $total_agent_cost=round(($price+$markup_cost));
                                                         $markup_agent_cost=round(($total_agent_cost*$own_markup)/100);
                                                        $total_cost=round(($total_agent_cost+$markup_agent_cost));
                                                       echo $total_cost;
                                                    @endphp">

                                                                        <select name="child_count" id="child_count"
                                                                            class="form-control" required="required">
                                                                            <option value="" selected="selected"
                                                                                hidden="">SELECT
                                                                                CHILD
                                                                            </option>
                                                                           @for($i=0;$i<=30;$i++)
                                                                           <option value="{{$i}}">{{$i}}</option>
                                                                           @endfor
                                                                        </select>
                                                                    </div>


                                                                </div>

                                                            </div>

                                                        </div>
                                                         <div class="row" id="child_age_div" style="display:none">
                                                         </div>

                                                       <!--  <div class="hotel_detail">
                                                            <p class="hotel_desc">Activity Description</p>
                                                            <p class="para"><b>Country : </b>
                                                                Georgia
                                                            </p>


                                                            <p class="para"><b>City : </b>
                                                                Tbilisi </p>
                                                            <p class="para"><b>Location :
                                                                </b>gudauri</p>
                                                        </div> -->
                                                        <div class="book_btn">

                                                             <input type="hidden" name="selected_vehicle_type_id" value="0">
                                                               <input type="hidden" name="selected_vehicle_type_name" value="">
                                                            <input type="hidden" name="selected_guide_id" value="0">
                                                                <input type="hidden" name="selected_guide_name" value="">
                                                                 <input type="hidden" name="selected_guide_supplier_cost" value="">
                                                                  <input type="hidden" name="selected_guide_agent_cost" value="0">
                                                             <input type="hidden" name="selected_guide_customer_cost" value="0">
                                                             <input type="hidden" name="selected_driver_id" value="0">
                                                                <input type="hidden" name="selected_driver_name" value="">
                                                                <input type="hidden" name="selected_driver_supplier_cost" value="0">
                                                                <input type="hidden" name="selected_driver_agent_cost" value="0">
                                                             <input type="hidden" name="selected_driver_customer_cost" value="0">
                                                            <button type="submit" id="activity_book_btn"
                                                                class="btn btn-rounded btn-primary mr-10">BOOK</button>
                                                        </div>

                                                    </div>
                                                </div>
                                            </form>

                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                    </div>
                  
                   
                   <!--  <div class="row">
                        <div class="col-md-8">
                            <p class="h-para">4 reasons to choose Hotel Kabir Residency</p>
                            <div class="h-div">
                                <div class='row'>
                                    <div class="col-md-4">
                                        <div class="p-div">
                                            <i class="fa fa-check tick-s"></i> Prices you can't beat!
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="p-div">
                                            <i class="fa fa-check tick-s"></i> Prices you can't beat!
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="p-div">
                                            <i class="fa fa-check tick-s"></i> Prices you can't beat!
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div> -->
                   
                </div>
            </div>
        </div>
    </div>
</div>

</div>

@include('agent.includes.footer')
@include('agent.includes.bottom-footer')
 <div class="modal" id="vehicleModal" style="z-index: 99999">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Vehicle Images</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;" id="vehicle_view_images">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div>

        <div class="modal" id="guideModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Guide Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height:600px;overflow-y: auto;overflow-x: hidden;" id="guide_details_div">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div>
        <div class="modal" id="driverModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Driver Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height:600px;overflow-y: auto;overflow-x: hidden;" id="driver_details_div">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div>
 <script>
        //we start with splitting the provided string into an array

        // var enableDays = enabledates.split(', ')

        // function formatDate(d) {
        //     var day = String(d.getDate())
        //     //add leading zero if day is is single digit
        //     if (day.length == 1)
        //         day = '0' + day
        //     var month = String((d.getMonth() + 1))
        //     //add leading zero if month is is single digit
        //     if (month.length == 1)
        //         month = '0' + month
        //     return d.getFullYear() + "-" + month + "-" + day;
        // }

        $(function () {
            $("#select_date").datepicker({
                maxViewMode: 2,
                weekStart: 1,
                startDate: "+1d",
                todayHighlight: true,
                format: "yyyy-mm-dd",
                clearBtn: true,
                autoclose: true
            })
        });
        function roundLikePHP(num, dec){
  var num_sign = num >= 0 ? 1 : -1;
  return parseFloat((Math.round((num * Math.pow(10, dec)) + (num_sign * 0.0001)) / Math.pow(10, dec)).toFixed(dec));
}
    </script>

    

    <script>
        $(document).on("submit", "#sightseeing_booking_form", function (e) {
            var select_date = $("#select_date").val();
             var sightseeing_tour_type=$("#sightseeing_tour_type").val();
           
            if (select_date == "") {
                e.preventDefault();
                $("#select_date").css("border", "1px solid #cf3c63");
            } else

            {
                $("#select_date").css("border", "1px solid #9e9e9e");
            }
            if(sightseeing_tour_type=="private")
            {

                if (!$("input[name='vehcile_type']:checked").val()) {
                    e.preventDefault();
                    $("#vehcile_type_table").css("border", "1px solid #cf3c63");
                    window.scrollTo(0,document.querySelector("body").scrollHeight);
                    alert("Please Select Vehicle Type first");
                } else

                {
                    $("#vehcile_type_table").css("border", "1px solid #9e9e9e");
                     if ($("input[name='selected_guide_id']").val()==0) {
                    e.preventDefault();
                    $("#guide_div").css("border", "1px solid #cf3c63");
                    window.scrollTo(0,document.querySelector("body").scrollHeight);
                    alert("Please Select Guide first");
                } else

                {
                    $("#guide_div").css("border", "1px solid #9e9e9e");
                }

                if ($("input[name='selected_driver_id']").val()==0) {
                    e.preventDefault();
                    $("#driver_div").css("border", "1px solid #cf3c63");
                    window.scrollTo(0,document.querySelector("body").scrollHeight);
                    alert("Please Select Driver first");
                } else

                {
                    $("#driver_div").css("border", "1px solid #9e9e9e");
                }
                }

               
            }

             var no_of_kids=$("#child_count").val();
            var child_age_error=0;
            if(no_of_kids!="0")
            {

                $(".child_age").each(function()
                {
                    if($(this).val().trim()=="")
                    {

                        $(this).css("border","1px solid #cf3c63");
                         $(this).focus();
                        child_age_error++;
                    }
                    else
                    {
                        $(this).css("border","1px solid #9e9e9e");
                    }
                })

            }
            if(child_age_error>0)
            {
                  e.preventDefault();
            }

        });
    </script>

    <script>
          //on vehcile type change , get the respective guides and their cost
        $(document).on("change","input[name='vehcile_type']",function()
        {
            var sightseeing_id=$("input[name='sightseeing_id']").val();
            var vehicle_type_id=$(this).val();
             var vehicle_type_name=$("#vehcile_type_name_"+vehicle_type_id).val();
              var select_date=$("#select_date").val();
              
            $("input[name='selected_vehicle_type_id']").val(vehicle_type_id);
              $("input[name='selected_vehicle_type_name']").val(vehicle_type_name);
              $(".table-sightseeing-loader").show();

                    $("#guide_div").html("");
                    $("#driver_div").html("");
            $.ajax({
                url:"{{route('fetchGuidesSightseeing')}}",
                data:{"sightseeing_id":sightseeing_id,
                        "vehicle_type_id":vehicle_type_id,
                        "sightseeing_date":select_date},
                type:"GET",
                success:function(response)
                {
                    $("#guide_div").html(response);
                    $(".table-sightseeing-loader").hide();

                    //empty guide id and cost because guides data refreshed
                    $("input[name='selected_guide_id']").val("");
                    $("input[name='selected_guide_supplier_cost']").val(0);
                     $("input[name='selected_guide_agent_cost']").val(0);
                      $("input[name='selected_guide_customer_cost']").val(0);
                      $("input[name='selected_guide_name']").val("");


                }
            }).done(function(){
                var guideDiv=$(".guide-div");
                guideDiv.on("click",function(){
                 
                    this.getElementsByClassName("tile-label")[0].click()
                })
            });

              $.ajax({
                url:"{{route('fetchDriversSightseeing')}}",
                data:{"sightseeing_id":sightseeing_id,
                        "vehicle_type_id":vehicle_type_id,
                        "sightseeing_date":select_date},
                type:"GET",
                success:function(response)
                {
                    $("#driver_div").html(response);
                    $(".table-sightseeing-loader").hide();

                    //empty guide id and cost because guides data refreshed
                    $("input[name='selected_driver_id']").val("");
                     $("input[name='selected_driver_supplier_cost']").val(0);
                      $("input[name='selected_driver_agent_cost']").val(0);
                    $("input[name='selected_driver_customer_cost']").val(0);
                      $("input[name='selected_driver_name']").val("");

                      
                },
            }).done(function(){
               
                var driverDiv=$(".driver-div");
                
                driverDiv.on("click",function(){
                   
                    this.getElementsByClassName("tile-label")[0].click()
                })
               
              

         

        
            });
           
        });

        $(document).on("change", "#child_count,#adult_count", function () {
            var child_count = $("#child_count").val();
            var adult_count = $("#adult_count").val();
             var discount = parseFloat($("#discount").val());


             

            var sightseeing_tour_type=$("#sightseeing_tour_type").val();
            if(sightseeing_tour_type=="private")
            {
            var child_price = $("#child_price").val();
            var adult_price = $("#adult_price").val();
            }
            else
            {
            var child_price = $("#group_child_price").val();
            var adult_price = $("#group_adult_price").val();
            }



            if (!child_count) {
                child_count = 0;
            }

            if (!adult_count) {
                adult_count = 0;
            }
            if (!child_price) {
                child_price = 0;
            }

            if (!adult_price) {
                adult_price = 0;
            }
            var total_pax=parseInt(child_count)+parseInt(adult_count);


             $(".vehcile_type_show").each(function()
             {
                var minimum=$(this).attr("data-minvalue");
                var maximum=$(this).attr("data-maxvalue");
                if(total_pax<minimum || total_pax>maximum)
                {
                    if($(this).prop("checked")==true)
                    {
                         $("input[name='vehcile_type']").prop("checked",false);
                    }

                    $(this).parent().hide();
                }
                else
                {
                    $(this).parent().show();
                }
             });


            var sightseeing_hotel_cost = parseFloat($("#sightseeing_hotel_cost").val());
            var sightseeing_food_cost = parseFloat($("#sightseeing_food_cost").val());

            var guide_customer_cost= parseFloat($("input[name='selected_guide_customer_cost']").val());
            var driver_customer_cost= parseFloat($("input[name='selected_driver_customer_cost']").val());




            var child_final_price = parseFloat(child_price*child_count);

            var adult_final_price = parseFloat(adult_price*adult_count);

               if(sightseeing_tour_type=="private")
            {
          var total_hotel_foodcost = parseFloat(sightseeing_hotel_cost + sightseeing_food_cost);
            }
            else
            {
           var total_hotel_foodcost = 0;
            }

            var total_price = parseFloat(child_final_price+adult_final_price+total_hotel_foodcost+guide_customer_cost+driver_customer_cost);
            var total_price_1=roundLikePHP(total_price,0)

            var discounted_price=parseFloat((total_price*discount)/100);
            var discounted_price=roundLikePHP(discounted_price,0)
            var price_after_discount=parseFloat(total_price-discounted_price);

             $("#total_price_text").text(total_price_1);
              $("#total_price_discount").text(discounted_price);
            $("#total_price_text_discount").text(price_after_discount);
            $("#total_price").val(price_after_discount);
        $("#inital_cost").html("");
        });


        //get the cost and guide that is selected
           $(document).on("change","input[name='guide']",function()
        {
            var guide_id=$(this).val();
            var guide_customer_cost=$("#guide_price_"+guide_id).text();
            var guide_agent_cost=$("#guide_agent_cost_"+guide_id).text();
            var guide_supplier_cost=$("#guide_cost_"+guide_id).text();
             var guide_name=$("#guide_name_"+guide_id).text();
            var discount = parseFloat($("#discount").val());

            

            $("input[name='selected_guide_id']").val(guide_id);
             $("input[name='selected_guide_supplier_cost']").val(guide_supplier_cost);
              $("input[name='selected_guide_agent_cost']").val(guide_agent_cost);
              $("input[name='selected_guide_customer_cost']").val(guide_customer_cost);
              $("input[name='selected_guide_name']").val(guide_name);

              var driver_customer_cost=$("input[name='selected_driver_customer_cost']").val();

                var child_count = $("#child_count").val();
            var adult_count = $("#adult_count").val();


            var sightseeing_tour_type=$("#sightseeing_tour_type").val();
            if(sightseeing_tour_type=="private")
            {
            var child_price = $("#child_price").val();
            var adult_price = $("#adult_price").val();
            }
            else
            {
            var child_price = $("#group_child_price").val();
            var adult_price = $("#group_adult_price").val();
            }
            



            if (!child_count) {
                child_count = 0;
            }

            if (!adult_count) {
                adult_count = 0;
            }
            if (!child_price) {
                child_price = 0;
            }

            if (!adult_price) {
                adult_price = 0;
            }


            var sightseeing_hotel_cost = parseFloat($("#sightseeing_hotel_cost").val());
            var sightseeing_food_cost = parseFloat($("#sightseeing_food_cost").val());




            var child_final_price = parseFloat(child_price * child_count);

            var adult_final_price = parseFloat(adult_price * adult_count);
            if(sightseeing_tour_type=="private")
            {
          var total_hotel_foodcost = parseFloat(sightseeing_hotel_cost + sightseeing_food_cost);
            }
            else
            {
           var total_hotel_foodcost = 0;
            }
               

            var total_price = parseFloat(child_final_price + adult_final_price + total_hotel_foodcost);

            var total_price_after_driver = parseFloat(parseFloat(driver_customer_cost) + parseFloat(total_price));

                var total_price_after_guide = parseFloat(parseFloat(guide_customer_cost) + total_price_after_driver);
                var total_price_after_guide_1=roundLikePHP(total_price_after_guide,0);

            var discounted_price=parseFloat((total_price_after_guide*discount)/100);
            var discounted_price=roundLikePHP(discounted_price,0)
            var price_after_discount=parseFloat(total_price_after_guide-discounted_price);
                     $("#total_price_text").text(total_price_after_guide_1);
                      $("#total_price_discount").text(discounted_price);
            $("#total_price_text_discount").text(price_after_discount);
            $("#total_price").val(price_after_discount);
             $("#inital_cost").html("");
          
        });


        //get the cost and driver that is selected
           $(document).on("change","input[name='driver']",function()
        {
            var driver_id=$(this).val();
            var driver_customer_cost=$("#driver_price_"+driver_id).text();
            var driver_agent_cost=$("#driver_agent_cost_"+driver_id).text();
            var driver_name=$("#driver_name_"+driver_id).text();
            var driver_supplier_cost=$("#driver_cost_"+driver_id).text();
            var driver_name=$("#driver_name_"+driver_id).text();

            var discount = parseFloat($("#discount").val());

            $("input[name='selected_driver_id']").val(driver_id);
            $("input[name='selected_driver_supplier_cost']").val(driver_supplier_cost);
            $("input[name='selected_driver_agent_cost']").val(driver_agent_cost);
            $("input[name='selected_driver_customer_cost']").val(driver_customer_cost);
            $("input[name='selected_driver_name']").val(driver_name);

               var guide_customer_cost=$("input[name='selected_guide_customer_cost']").val();

                var child_count = $("#child_count").val();
            var adult_count = $("#adult_count").val();




             var sightseeing_tour_type=$("#sightseeing_tour_type").val();
            if(sightseeing_tour_type=="private")
            {
            var child_price = $("#child_price").val();
            var adult_price = $("#adult_price").val();
            }
            else
            {
            var child_price = $("#group_child_price").val();
            var adult_price = $("#group_adult_price").val();
            }
            



            if (!child_count) {
                child_count = 0;
            }

            if (!adult_count) {
                adult_count = 0;
            }
            if (!child_price) {
                child_price = 0;
            }

            if (!adult_price) {
                adult_price = 0;
            }


            var sightseeing_hotel_cost = parseFloat($("#sightseeing_hotel_cost").val());
            var sightseeing_food_cost = parseFloat($("#sightseeing_food_cost").val());




            var child_final_price = parseFloat(child_price * child_count);

            var adult_final_price = parseFloat(adult_price * adult_count);

            if(sightseeing_tour_type=="private")
            {
          var total_hotel_foodcost = parseFloat(sightseeing_hotel_cost + sightseeing_food_cost);
            }
            else
            {
           var total_hotel_foodcost = 0;
            }

            var total_price = parseFloat(child_final_price + adult_final_price + total_hotel_foodcost);

                 var total_price_after_guide = parseFloat(parseFloat(guide_customer_cost) + parseFloat(total_price));
                var total_price_after_driver = parseFloat(parseFloat(driver_customer_cost) + total_price_after_guide);
                  var total_price_after_driver_1=roundLikePHP(total_price_after_driver,0)

              var discounted_price=parseFloat((total_price_after_driver*discount)/100);
            var discounted_price=roundLikePHP(discounted_price,0)
            var price_after_discount=parseFloat(total_price_after_driver-discounted_price);

            $("#total_price_text").text(total_price_after_driver_1);
             $("#total_price_discount").text(discounted_price);
            $("#total_price_text_discount").text(price_after_discount);
            $("#total_price").val(price_after_discount);
             $("#inital_cost").html("");
          
        });
    $(document).on("change","#child_count",function()
{
    var html_data="";
    var child_count=$(this).val();
    for($i=1;$i<=child_count;$i++)
    {
        html_data+='<div class="col-md-6"> <div class="form-group"><label for="child_age'+$i+'">Child Age '+$i+' <span class="asterisk">*</span></label><input type="text" id="child_age'+$i+'" name="child_age[]" class="form-control child_age" onkeypress="javascript:return validateNumber(event)" maxlength=2 required></div></div>'
    }
    $("#child_age_div").html(html_data);
    $("#child_age_div").show();
});

    </script>
    <script>
        $(document).on("change","#sightseeing_tour_type",function()
        {
            if($(this).val()=="private")
            {
            var adult_price = $("#adult_price").val();
             $("#guide_div").show();
            $("#driver_div").show();
            $("#guide_div_head").show();
            $("#driver_div_head").show();
            $("#vehicle_type_div").show();
            }
            else
            {
            var adult_price = $("#group_adult_price").val();
              $("#guide_div").hide();
            $("#driver_div").hide();
              $("#guide_div_head").hide();
            $("#driver_div_head").hide();
            $("#vehicle_type_div").hide();

            }
            $("#adult_count").val("");
            $("#child_count").val("");
            $("#guide_div").html("");
            $("#driver_div").html("");

            $("#child_age_div").html("");
            $("input[name='selected_guide_id']").val("");
              $("input[name='selected_guide_supplier_cost']").val(0);
                $("input[name='selected_guide_agent_cost']").val(0);
              $("input[name='selected_guide_customer_cost']").val(0);
              $("input[name='selected_guide_name']").val("");
            $("input[name='selected_driver_id']").val("");
             $("input[name='selected_driver_supplier_cost']").val(0);
              $("input[name='selected_driver_agent_cost']").val(0);
              $("input[name='selected_driver_customer_cost']").val(0);
              $("input[name='selected_driver_name']").val("");
              $("input[name='vehcile_type']").prop("checked",false);


            // $("#total_price_text_discount").text(adult_price);
            // $("#total_price").val(adult_price);

             var discount = parseFloat($("#discount").val());
            var sightseeing_hotel_cost = parseFloat($("#sightseeing_hotel_cost").val());
            var sightseeing_food_cost = parseFloat($("#sightseeing_food_cost").val());


            if(sightseeing_tour_type=="private")
            {
          var total_hotel_foodcost = parseFloat(sightseeing_hotel_cost + sightseeing_food_cost);
            }
            else
            {
           var total_hotel_foodcost = parseFloat(0);
            }

            var total_price = parseFloat(parseFloat(adult_price)+parseFloat(total_hotel_foodcost));
            var total_price_1=roundLikePHP(total_price,0)
            var discounted_price=parseFloat((total_price*discount)/100);
            var discounted_price=roundLikePHP(discounted_price,0)
            var price_after_discount=parseFloat(total_price-discounted_price);

            $("#total_price_text").text(total_price_1);
             $("#total_price_discount").text(discounted_price);
            $("#total_price_text_discount").text(price_after_discount);
            $("#total_price").val(price_after_discount);
             $("#inital_cost").html("");
          
        });

        $(document).on("click",".card-activate-click",function()
        {
            $(this).find(".card-activate-radio").trigger("click");
        });
    </script>

    <script>
        var tile=document.getElementsByClassName("parent-tile")
        
        for(var i=0;i<tile.length;i++){
            tile[i].addEventListener("click",function(){
                this.lastElementChild.click()
               
                
            })
          
           
        }
       
       
       
        </script>

        <script>
            $(window).scroll(function(e){ 
  var $el = $('.sticky-sidebar'); 
  var isPositionFixed = ($el.css('position') == 'fixed');
  if ($(this).scrollTop() > 200 && !isPositionFixed){ 
    $el.css({'position': 'fixed', 'top': '0px',"width": "24%"}); 

  }
  if ($(this).scrollTop() < 200 && isPositionFixed){
    $el.css({'position': 'static', 'top': '0px',"width": "100%"}); 
  } 
});
             $(document).on("click",".view_more",function()
           {
            var clone=$(this).parent().clone();
            clone.find(".view_more").remove();
            clone.find(".more-vehicle-image").css("display","block");
            clone.find(".more-vehicle-image").parent().css("display","block");
             clone.find("img").removeAttr("width height").attr({"width":"250","height":"200"}).css("padding","10px");
            $("#vehicle_view_images").html(clone);
            $("#vehicleModal").modal("show");
           });

             $(document).on("click",".view_guide",function()
             {
                var id=$(this).attr("id").split("_")[1];

                $.ajax({
                    url:"{{route('fetchGuidesDetails')}}",
                    type:"GET",
                    data:{"guide_id":id},
                    success:function(response)
                    {
                        $("#guide_details_div").html(response);

                        $("#guideModal").modal("show");
                        $('#guide_details_div').animate({
                            scrollTop: $('#guide_details_div').position().top
                        }, 'slow');
                    }

                });
             });
              $(document).on("click",".view_driver",function()
             {
                var id=$(this).attr("id").split("_")[1];

                $.ajax({
                    url:"{{route('fetchDriversDetails')}}",
                    type:"GET",
                    data:{"driver_id":id},
                    success:function(response)
                    {
                        $("#driver_details_div").html(response);

                        $("#driverModal").modal("show");
                        $('#driver_details_div').animate({
                            scrollTop: $('#driver_details_div').position().top
                        }, 'slow');
                    }

                });
             });
        </script>


