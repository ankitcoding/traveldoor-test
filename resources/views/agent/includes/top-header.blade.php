<!DOCTYPE html>

<html lang="en">

  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="description" content="">

    <meta name="author" content="">



    <title>Travel Agent CRM</title>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendor_components/datatable/datatables.min.css') }}"/>

	<link rel="stylesheet" href="{{ asset('assets/vendor_components/select2/dist/css/select2.min.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css') }}">

	




<link href="{{ asset('assets/vendor_plugins/bootstrap-slider/slider.css') }}">
    <link href="{{ asset('assets/vendor_components/OwlCarousel2/dist/assets/owl.carousel.css' ) }}">
    
<link href="{{ asset('assets/vendor_components/OwlCarousel2/dist/assets/owl.theme.default.css' ) }}">
	
	
	<!-- flexslider-->
	<link rel="stylesheet" href="{{ asset('assets/vendor_components/flexslider/flexslider.css') }}">	




	<!-- Bootstrap 4.0-->

	<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap/dist/css/bootstrap.css') }}">

	<!-- bootstrap datepicker -->

	<link rel="stylesheet"

		href="{{ asset('assets/vendor_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

<!-- bootstrap datetimepicker -->

	<link rel="stylesheet"

		href="{{ asset('assets/vendor_components/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">


		<!-- Bootstrap time Picker -->
	<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/timepicker/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">

	<!-- Bootstrap Clock Picker -->
	<link rel="stylesheet" href="{{ asset('assets/vendor_plugins/clockpicker/dist/bootstrap4-clockpicker.min.css') }}">


	<!-- daterange picker -->

	<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-daterangepicker/daterangepicker.css') }}">

	<link rel="stylesheet" href="{{ asset('assets/vendor_components/bootstrap-select/dist/css/bootstrap-select.css') }}">

	<link href="{{ asset('assets/vendor_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">

	<!-- c3 CSS -->





	<!-- toast CSS -->

	<link href="{{ asset('assets/vendor_components/jquery-toast-plugin-master/src/jquery.toast.css') }}">



	<!-- theme style -->

	<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/css/common.css') }}">


	<!-- VoiceX Admin skins -->

	<link rel="stylesheet" href="{{ asset('assets/css/skin_color.css') }}">
	
	
	<!--test-->
	
<style>
  .main-sidebar
  {
   background: -webkit-linear-gradient(-55deg, #4171ae 35%, #e2dcff 100%);
    border-radius: 10px;
    border: none;
  }
  .sidebar-menu li > a {
    color:white !important;
    font-weight: 600;
    font-size:15px;
  }
  .sidebar-menu > li > a > i {
   color:white !important;
    font-size:15px;
 }
 .sidebar-menu > li.active {
    background-color: #192787;
}
</style>


     

  </head>