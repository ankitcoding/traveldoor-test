<?php

use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\LoginController;
?>

@include('agent.includes.top-header')


<style>
   .detail-card {
        border: 1px solid #ffe0ac;
        background: #ffeccc;
        padding: 15px;
        border-radius: 5px;
    }

    img.g-detail-img {
        border: 1px dotted #f88d04;
        padding: 5px;
        border-radius: 5px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
    }

    h3.guide-name {
        background: #f88d04;
        color: white;
        padding: 10px;
        margin: 0;
        text-align: center;
        border-bottom-left-radius: 5px;
        border-bottom-right-radius: 5px;
        font-size: 20px;
    }

    i.fa.fa-phone.phn {
        background: #3c9d36;
        padding: 14px 10px;
        width: 50px;
        height: 50px;
        position: absolute;
        color: white;
        font-size: 25px;
        left: 0px;
        text-align: center;
        top: 50%;
        transform: translateY(-50%);
        border-radius: 50%;
    }

    p.call-now {
        background: #ffffff;
        padding: 6px 10px 6px 65px;
        margin: 25px 0 10px;
        color: #3c9d36;
        position: relative;
        border-radius: 20px;
        /* border-top-left-radius: 0; */
        /* border-bottom-left-radius: 0; */
    }

    label {
        color: #f9a825;
    }

    button.g-btn {
        position: absolute;
        top: 50px;
        right: 28px;
        width: 140px;
        border-radius: 21px;
        font-size: 18px;
        height: 42px;
        overflow: hidden;
        background: #ee1044;
        white-space: pre;
        border: none;
        color: white;
        padding: 6px;
        cursor: pointer;
        transition: all .5s ease;
    }

    
p.price-p {
    background: #ee1044;
    color: white;
    display: inline-block;
    padding: 5px 10px;
    position: absolute;
    top: 10px;
    border-radius: 5px;
    right: 33px;
}


    i.fa.fa-user.g-btn-icon {
        font-size: 18px;
        padding: 0px 13px 0 3px;
    }

    a.panel-title {
        position: relative;
    }

    a.panel-title:before {
        content: attr(title);
        top: 0;
        color: red;
    }
   

h3.g-title {
    margin: 0;
}
ul.nav.nav-tabs.c-nav ~ .tab-content {
    padding: 20px 10px;
    background: white;
}
p.h-para {
    font-size: 20px;
}
.h-div {
    background: #e9f0fa;
    padding: 20px;
}
li.nav-item.c-item a.nav-link {
    padding: 7px 20px !important;
    height: 39px !important;
    margin-left: 15px;
    border-radius: 50px;
   
    
}
li.nav-item.c-item a.nav-link.active{
  background:linear-gradient(45deg, #d5135a, #f05924);
}
.p-div {
    font-size: 14px;
    color: #1f1f1f;
    margin-bottom: 0px;
}
i.tick-s {
    color: #1550a9;
    background: white;
    padding: 5px;
    border: 1px solid #74d6ff;
    border-radius: 50%;
}
  
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">



    <div class="wrapper">



        @include('agent.includes.top-nav')



        <div class="content-wrapper">



            <div class="container-full clearfix position-relative">	



                @include('agent.includes.nav')



                <div class="content">







                    <div class="content-header">



                        <div class="d-flex align-items-center">



                            <div class="mr-auto">



                                <h3 class="page-title">Guide Details</h3>



                                <div class="d-inline-block align-items-center">



                                    <nav>



                                        <ol class="breadcrumb">



                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>



                                            <li class="breadcrumb-item" aria-current="page">Home



                                                <li class="breadcrumb-item active" aria-current="page">Guide Details



                                                </li>



                                            </ol>



                                        </nav>



                                    </div>



                                </div>


        </div>



    </div>

<style>
@import url("https://fonts.googleapis.com/css?family=Quicksand:400,500,700&subset=latin-ext");

.icon-2 {
  display: inline-block;
  width: 1em;
  height: 1em;
  stroke-width: 0;
  stroke: currentColor;
  fill: currentColor;
}

.wrapper-2 {
  width: 100%;
  width: 100%;
  height: auto;
  min-height: 100vh;
  padding: 0;
  padding-top: 70px;
  display: flex;
  margin-bottom: 50px;
  
}
@media screen and (max-width: 768px) {
  .wrapper-2 {
    height: auto;
    min-height: 100vh;
    padding-top: 100px;
  }
}

.profile-card {
  width: 100%;
  min-height: 460px;
  margin: auto;
  box-shadow: 0px 8px 60px -10px rgba(13, 28, 39, 0.6);
  background: #fff;
  border-radius: 12px;
  
  position: relative;
}
.profile-card.active .profile-card__cnt {
  filter: blur(6px);
}
.profile-card.active .profile-card-message,
.profile-card.active .profile-card__overlay {
  opacity: 1;
  pointer-events: auto;
  transition-delay: .1s;
}
.profile-card.active .profile-card-form {
  transform: none;
  transition-delay: .1s;
}
.profile-card__img {
  width: 150px;
  height: 150px;
  margin-left: auto;
  margin-right: auto;
  transform: translateY(-50%);
  border-radius: 50%;
  overflow: hidden;
  position: relative;
  z-index: 4;
  box-shadow: 0px 5px 50px 0px #6c44fc, 0px 0px 0px 7px rgba(107, 74, 255, 0.5);
}
@media screen and (max-width: 576px) {
  .profile-card__img {
    width: 120px;
    height: 120px;
  }
}
.profile-card__img img {
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
  border-radius: 50%;
}
.profile-card__cnt {
  margin-top: -50px;
  text-align: center;
  padding: 0 20px;
  padding-bottom: 40px;
  transition: all .3s;
}
span.profile-card-loc__txt {
    position: absolute;
    top: 20px;
    right: 37px;
    font-size: 16px;
    color: red;
    background: #ebebeb;
    padding: 8px 15px;
    border-radius: 5px;
}
button.profile-card__button.button--orange {
    position: absolute;
    top: 65px;
    right: 21px;
}
.profile-card__name {
  font-weight: 700;
  font-size: 24px;
  color: #6944ff;
  margin-bottom: 15px;
}
.profile-card__txt {
  font-size: 18px;
  font-weight: 500;
  color: #324e63;
  margin-bottom: 15px;
}
.profile-card__txt strong {
  font-weight: 700;
}
.profile-card-loc {
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  font-weight: 600;
}
.profile-card-loc__icon {
  display: inline-flex;
  font-size: 27px;
  margin-right: 10px;
}
.profile-card-inf {
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  align-items: flex-start;
  margin-top: 0px;
}
.profile-card-inf__item {
  padding: 10px 35px;
  min-width: 150px;
}
p#supplier_id {
    margin: 0;
    text-align: center;
}
@media screen and (max-width: 768px) {
  .profile-card-inf__item {
    padding: 10px 20px;
    min-width: 120px;
  }
}
.profile-card-inf__title {
  font-weight: 700;
  font-size: 17px;
  color: #324e63;
}
.profile-card-inf__txt {
  font-weight: 500;
  margin-top: 7px;
}
.profile-card-social {
  margin-top: 0px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
}
.profile-card-social__item {
    display: inline-flex;
    width: 85px;
    height: 34px;
    margin: 15px;
    border-radius: 50px;
    align-items: center;
    justify-content: center;
    color: #fff;
    background: #405de6;
    box-shadow: 0px 7px 30px rgba(43, 98, 169, 0.5);
    position: relative;
    font-size: 21px;
    flex-shrink: 0;
    transition: all .3s;
}
.profile-card-social .icon-font {
    display: inline-flex;
    font-size: 14px;
}
@media screen and (max-width: 768px) {
  .profile-card-social__item {
    width: 50px;
    height: 50px;
    margin: 10px;
  }
}
.theme-rosegold a:hover, .theme-rosegold a:active, .theme-rosegold a:focus {
    color: #ffffff;
}
.profile-card-social__item.facebook {
  background: linear-gradient(45deg, #3b5998, #0078d7);
  box-shadow: 0px 4px 30px rgba(43, 98, 169, 0.5);
}
.profile-card-social__item.twitter {
  background: linear-gradient(45deg, #1da1f2, #0e71c8);
  box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.7);
}
.profile-card-social__item.instagram {
  background: linear-gradient(45deg, #405de6, #5851db, #833ab4, #c13584, #e1306c, #fd1d1d);
  box-shadow: 0px 4px 30px rgba(120, 64, 190, 0.6);
}
.profile-card-social__item.behance {
  background: linear-gradient(45deg, #1769ff, #213fca);
  box-shadow: 0px 4px 30px rgba(27, 86, 231, 0.7);
}
.profile-card-social__item.github {
  background: linear-gradient(45deg, #333333, #626b73);
  box-shadow: 0px 4px 30px rgba(63, 65, 67, 0.6);
}
.profile-card-social__item.codepen {
  background: linear-gradient(45deg, #324e63, #414447);
  box-shadow: 0px 4px 30px rgba(55, 75, 90, 0.6);
}
.profile-card-social__item.link {
  background: linear-gradient(45deg, #d5135a, #f05924);
  box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.6);
}
.profile-card-social .icon-font {
  display: inline-flex;
}
.profile-card-ctr {
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 40px;
}
@media screen and (max-width: 576px) {
  .profile-card-ctr {
    flex-wrap: wrap;
  }
}
.profile-card__button {
    background: none;
    border: none;
    font-family: 'Quicksand', sans-serif;
    font-weight: 700;
    font-size: 18px;
    margin: 15px 35px;
    padding: 7px 31px;
    min-width: auto;
    border-radius: 50px;
    min-height: 50px;
    color: #fff;
    cursor: pointer;
    backface-visibility: hidden;
    transition: all .3s;
}
@media screen and (max-width: 768px) {
  .profile-card__button {
    min-width: 170px;
    margin: 15px 25px;
  }
}
@media screen and (max-width: 576px) {
  .profile-card__button {
    min-width: inherit;
    margin: 0;
    margin-bottom: 16px;
    width: 100%;
    max-width: 300px;
  }
  .profile-card__button:last-child {
    margin-bottom: 0;
  }
}
.profile-card__button:focus {
  outline: none !important;
}
@media screen and (min-width: 768px) {
  .profile-card__button:hover {
    transform: translateY(-5px);
  }
}
.profile-card__button:first-child {
  margin-left: 0;
}
.profile-card__button:last-child {
  margin-right: 0;
}
.profile-card__button.button--blue {
  background: linear-gradient(45deg, #1da1f2, #0e71c8);
  box-shadow: 0px 4px 30px rgba(19, 127, 212, 0.4);
}
.profile-card__button.button--blue:hover {
  box-shadow: 0px 7px 30px rgba(19, 127, 212, 0.75);
}
.profile-card__button.button--orange {
  background: linear-gradient(45deg, #d5135a, #f05924);
  box-shadow: 0px 4px 30px rgba(223, 45, 70, 0.35);
}
.profile-card__button.button--orange:hover {
  box-shadow: 0px 7px 30px rgba(223, 45, 70, 0.75);
}
.profile-card__button.button--gray {
  box-shadow: none;
  background: #dcdcdc;
  color: #142029;
}
.profile-card-message {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  padding-top: 130px;
  padding-bottom: 100px;
  opacity: 0;
  pointer-events: none;
  transition: all .3s;
}
.profile-card-form {
  box-shadow: 0 4px 30px rgba(15, 22, 56, 0.35);
  max-width: 80%;
  margin-left: auto;
  margin-right: auto;
  height: 100%;
  background: #fff;
  border-radius: 10px;
  padding: 35px;
  transform: scale(0.8);
  position: relative;
  z-index: 3;
  transition: all .3s;
}
@media screen and (max-width: 768px) {
  .profile-card-form {
    max-width: 90%;
    height: auto;
  }
}
@media screen and (max-width: 576px) {
  .profile-card-form {
    padding: 20px;
  }
}
.profile-card-form__bottom {
  justify-content: space-between;
  display: flex;
}
@media screen and (max-width: 576px) {
  .profile-card-form__bottom {
    flex-wrap: wrap;
  }
}
.profile-card textarea {
  width: 100%;
  resize: none;
  height: 210px;
  margin-bottom: 20px;
  border: 2px solid #dcdcdc;
  border-radius: 10px;
  padding: 15px 20px;
  color: #324e63;
  font-weight: 500;
  font-family: 'Quicksand', sans-serif;
  outline: none;
  transition: all .3s;
}
.profile-card textarea:focus {
  outline: none;
  border-color: #8a979e;
}
.profile-card__overlay {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  pointer-events: none;
  opacity: 0;
  background: rgba(22, 33, 72, 0.35);
  border-radius: 12px;
  transition: all .3s;
}
.main-footer {
    background-color: rgb(254, 254, 255);
    padding: 15px 30px;
    border-top: 1px solid #cccccc;
    z-index: 999;
    position: relative;
    width: 100%;
    left: 10px !important;
    text-align: center;
    margin: 0;
}
ul.nav.nav-tabs.c-nav {
    border: none;
    margin-top: 10px;
    margin-bottom: 10px;
}

img.ul-img {
    width: 100%;
    height: 100%;
}
li.c-item {
    display: flex;
}
a.img-div {
     background: #ffcec3 !important;
    display: block;
    padding: 6px 8px 9px;
    width: 35px;
    height: 35px;
    margin-bottom: 20px;
    border-radius: 50%;
}
.c-tab-contents {
    background: #ffddd5;
    margin-top: 0px;
    padding: 20px;
    border-radius: 5px;
}
h3.g-title {
    color: #ed512a;
    text-align: left;
    font-size: 19px;
    border-bottom: 1px solid #ffbcad;
    padding-bottom: 7px;
}
.g-deatil {
    margin-top: 10px;
    font-size: 16px;
    text-transform: capitalize;
}
</style>
<div class="container">
  <div class="row">
    <div class="col-12" style="padding:0">
        <div class="" style="box-shadow:none">

            <div class="box-body" style="padding:0">
               {{--  <div class="row">
                    <div class="col-md-12">
                        <p class="h-para"></p>
                        <div class="h-div">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="p-div">
                                    <i class="fa fa-check tick-s"></i> <b>FROM DATE </b> :  {{date('d F,Y',strtotime($from_date))}}
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="p-div">
                                    <i class="fa fa-check  tick-s"></i> <b>TO DATE </b> :   {{date('d F,Y',strtotime($to_date))}}
                                </div>
                            </div>
                            <div class="col-md-4">
                            <div class="p-div">
                                    <i class="fa fa-check tick-s"></i> <b>NO OF DAYS </b> :  {{$no_of_days}}
                                </div>
                            </div>
                        </div>
                        </div>
                      
                    </div>
                </div> --}}
               <div class="container">
                <div class="row">
                  <div class="col-md-12">
                   <form id="activity_continue_booking" method="post" action="{{route('guide-booking')}}">
                     {{csrf_field()}}
                     <input type="hidden" name="guide_id" value="{{$get_guides->guide_id}}">
                      <input type="hidden" name="supplier_id" value="{{$get_guides->guide_supplier_id}}">
                         <input type="hidden" name="from_date" value="{{$from_date}}">
                         <input type="hidden" name="to_date" value="{{$to_date}}">
                          <input type="hidden" name="no_of_days" value="{{$no_of_days}}">
                          <input type="hidden" name="booking_guide_quantity" value="1">
                          <input type="hidden" name="supplier_guide_price" value="@php 
                         $price=$get_guides->guide_price_per_day;
                          //$price+=$hotel_cost;
                          //$price+=$food_cost;
                          
                          echo $price;
                          
                          @endphp">
                           <input type="hidden" name="agent_guide_price" value="@php 
                         $price=$get_guides->guide_price_per_day;
                          //$price+=$hotel_cost;
                          //$price+=$food_cost;
                        $guide_markup_cost=round(($price*$markup)/100);
                        $total_cost=round(($price+$guide_markup_cost));
                         echo $total_cost;
                         @endphp">

                         @php 
                         $price=$get_guides->guide_price_per_day;
                          //$price+=$hotel_cost;
                          //$price+=$food_cost;
                        $guide_markup_cost=round(($price*$markup)/100);
                        $total_agent_cost=round(($price+$guide_markup_cost));


                        $own_guide_markup_cost=round(($total_agent_cost*$own_markup)/100);
                        $total_cost=round(($total_agent_cost+$own_guide_markup_cost));
                         @endphp

                          <input type="hidden" name="customer_guide_price" value="{{$total_cost}}">
                       <input type="hidden" name="markup" value="{{$markup}}">
                         <input type="hidden" name="own_markup" value="{{$own_markup}}">
                        <input type="hidden" name="guide_currency" value="GEL">
                    <div class="wrapper-2">
                 
                   
                        <div class="profile-card js-profile-card">
                          <div class="profile-card__img">
                            @if($get_guides->guide_image!="" && $get_guides->guide_image!=null)
                            <img src="@php echo asset("assets/uploads/guide_images").'/'.$get_guides->guide_image @endphp" style="width:100%;">
                            @else
                             <img src="@php echo asset("assets/images/no-photo.png") @endphp" style="width:100%">'
                         @endif
                          /*   <img src="https://res.cloudinary.com/muhammederdem/image/upload/v1537638518/Ba%C5%9Fl%C4%B1ks%C4%B1z-1.jpg" alt="profile card"> */
                          </div>
                      
                          <div class="profile-card__cnt js-profile-cnt">
                            <div class="profile-card__name">{{ucwords($get_guides->guide_first_name)}}</div>
                            <div class="profile-card__txt">Guide from <strong id="guide_country">@if($get_guides->guide_country!="" && $get_guides->guide_country!=null)
                                               @foreach($countries as $country)
                                             
                                               @if($country->country_id==$get_guides->guide_country)
                                               {{$country->country_name}}
                                               @endif
                                             
                                               @endforeach
                                                @else No Data Available @endif</strong>,
                                                <span id="guide_city">
                                                @if($get_guides->guide_city!="" && $get_guides->guide_city!=null)
                                               <?php
                                               $fetch_city=ServiceManagement::searchCities($get_guides->guide_city,$get_guides->guide_country);
                 
                                               echo $fetch_city['name'];
                                               ?>
                                             </span>
                                             @else No Data Available @endif</div>
                                             <p class="des" id="supplier_id"> {{$get_guides->guide_description}}
                                         </p>
                            <div class="profile-card-loc">
                              <span class="profile-card-loc__icon">
                             
                              </span>
                      
                              <span class="profile-card-loc__txt">GEL @php 
                            $price=$get_guides->guide_price_per_day;
                            //$price+=$hotel_cost;
                            //$price+=$food_cost;
                           $guide_markup_cost=round(($price*$markup)/100);
                        $total_agent_cost=round(($price+$guide_markup_cost));

                        $own_guide_markup_cost=round(($total_agent_cost*$own_markup)/100);
                        $total_cost=round(($total_agent_cost+$own_guide_markup_cost));
                           $total_cost=round(($total_agent_cost+$own_guide_markup_cost)*$no_of_days); echo $total_cost;
                        @endphp
                              </span>
                            </div>
                      
                            <div class="profile-card-inf">
                              <div class="profile-card-inf__item">
                                <div class="profile-card-inf__title">{{date('d F,Y',strtotime($from_date))}}</div>
                                <div class="profile-card-inf__txt"> <i class="fa fa-check tick-s"></i> <b>FROM DATE </b></div>
                              </div>
                      
                              <div class="profile-card-inf__item">
                                <div class="profile-card-inf__title">{{date('d F,Y',strtotime($to_date))}}</div>
                                <div class="profile-card-inf__txt"> <i class="fa fa-check tick-s"></i> <b>TO DATE </b></div>
                              </div>
                      
                              <div class="profile-card-inf__item">
                                <div class="profile-card-inf__title"> {{$no_of_days}}</div>
                                <div class="profile-card-inf__txt">  <i class="fa fa-check tick-s"></i> <b>NO OF DAYS </b></div>
                              </div>
                      
                             
                            </div>
                      
                            <div class="profile-card-social" id="guide_language">
                             @php  foreach($languages_spoken as $language){
                               
                               @endphp
                               <a href="#" class="profile-card-social__item facebook" target="_blank">
                                <span class="icon-font">
                                 @php 
                                 echo $language;
                                 @endphp
                                </span>
                              </a>
                              @php
                             }
                             
                             
                             
                             @endphp
                             
                            </div>
                            <ul class="nav nav-tabs c-nav">
                                     <li class="nav-item c-item">
                                    
                                        <a class="nav-link active" data-toggle="tab" href="#home">Cancellation</a>
                                     </li>
                                     <li class="nav-item c-item">
                                      
                                         <a class="nav-link" data-toggle="tab" href="#menu1">Terms & Condition</a>
                                     </li>
                                   
                                 </ul>
                            <div class="col-md-12">
                                

                                 <!-- Tab panes -->
                                 <div class="tab-content c-tab-contents">
                                     <div class="tab-pane container active" id="home">
                                         <h3 class="g-title">Cancellation</h3>
                                         <div class="g-deatil">
                                             @php
                                             echo $get_guides->guide_cancel_policy;
                                             @endphp
                                         </div>
                                     </div>
                                     <div class="tab-pane container fade" id="menu1">
                                     <h3 class="g-title">Terms & Condition</h3>
                                         <div class="g-deatil"> 
                                           @php
                                             echo $get_guides->guide_terms_conditions;
                                             @endphp
                                         </div>
                                     </div>
                                     
                                     <div>
                                     </div>
                                    </div>
                             </div>
                            <div class="profile-card-ctr">
                             
                              <button type="submit" class="profile-card__button button--orange"><i class="fa fa-user g-btn-icon"></i>Continue</button>
                            </div>
                          </div>
                      
                          <div class="profile-card-message js-message">
                            <form class="profile-card-form">
                              <div class="profile-card-form__container">
                                <textarea placeholder="Say something..."></textarea>
                              </div>
                      
                              <div class="profile-card-form__bottom">
                                <button class="profile-card__button button--blue js-message-close">
                                  Send
                                </button>
                      
                                <button class="profile-card__button button--gray js-message-close">
                                  Cancel
                                </button>
                              </div>
                            </form>
                      
                            <div class="profile-card__overlay js-message-close"></div>
                          </div>
                      
                        </div>
                      
                      </div>
                   </form>
                      
                 </div> 
               </div>
           
        
               </div>
                  </div>
        </div>


    </div>
</div>
</div>
    



</div>



</div>







@include('agent.includes.footer')



@include('agent.includes.bottom-footer')


