@include('agent.includes.top-header')

<style>
        
.hovereffect {
  width: 100%;
  height: 190px;
  border-radius: 5px;
  float: left;
  overflow: hidden;
  position: relative;
  text-align: center;
  cursor: default;
  background-image: linear-gradient(to right bottom, #3782f2, #4d77e0, #596dcf, #6163bd, #6459ac);
}

.hovereffect .overlay {
    width: 100%;
    height: 100%;
    position: absolute;
    overflow: hidden;
    top: 0;
    left: 0;
    padding: 0px;
}
.hovereffect img {
  display: block;
  position: relative;
  max-width: none;
  height:100%;
  width: calc(100% + 20px);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(-10px,0,0);
  transform: translate3d(-10px,0,0);
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
}

.hovereffect:hover img {
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
}

.hovereffect h2 {
    text-transform: uppercase;
    color: #fff;
    text-align: center;
    background-image: linear-gradient(to right bottom, #3782f2, #4d77e085, #596dcf85, #6163bd85, #6459ac85);
    position: relative;
    font-size: 24px;
    margin: 0;
    overflow: hidden;
    height: 100%;
    padding-top:15%;
    background-color: transparent;
}
.hovereffect h2:after {
  position: absolute;
  bottom: 17%;
  left: 0;
  width: 100%;

  height: 2px;
  background: #fff;
  content: '';
  -webkit-transition: -webkit-transform 0.35s;
  transition: transform 0.35s;
  -webkit-transform: translate3d(-100%,0,0);
  transform: translate3d(-100%,0,0);
}
.box .overlay {
    z-index: 50;
    background: rgba(0, 0, 0, 0);
    border-radius: 5px;
}
.hovereffect:hover h2:after {
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
  transform: translate(0%, -50%) !important;
}

.hovereffect a, .hovereffect p {
  color: #FFF;
  opacity: 0;
  filter: alpha(opacity=0);
  -webkit-transition: opacity 0.35s, -webkit-transform 0.35s;
  transition: opacity 0.35s, transform 0.35s;
  -webkit-transform: translate3d(100%,0,0);
  transform: translate3d(100%,0,0);
}

.hovereffect:hover a, .hovereffect:hover p {
  opacity: 1;
  filter: alpha(opacity=100);
  -webkit-transform: translate3d(0,-38px,0);
  transform: translate3d(0,0,0);
}
.hovereffect:hover h2 {
    text-transform: uppercase;
    color: #fff;
    text-align: center;
    position: relative;
    font-size: 17px;
    overflow: hidden;
    padding: 0.5em 0;
    background: none;
    height: auto;
    padding-bottom: 30px;
}
.hovereffect:hover .overlay {
    width: 100%;
    height: 100%;
    position: absolute;
    overflow: hidden;
    top: 0;
    left: 0;
    padding: 11% 10px 20%;
}
.hovereffect:hover a {
    background: #ffffff;
    padding: 10px 20px;
    margin-top: 0;
    color: aliceblue;
    display: inline-block;
    cursor: pointer;
    color: #6162bb;
    font-weight: 700;
    border-radius: 50px;
    font-size: 13px;
    text-transform: capitalize;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('agent.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('agent.includes.nav')

	<div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">Home</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item active" aria-current="page">Home

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

           
        </div>

    </div>


    <div class="row">
      <div class="box">
          <div class="box-body">


            <div class="col-12">

                <div class="box">



                    <div class="box-body">
                <?php
                $service_type=session()->get('travel_agent_type');

                $services=explode(',',$service_type);
                ?>

                       <div class="row">
                        @if(in_array("hotel",$services))
                        <div class="col-md-4 col-sm-6 col-xs-12 mb-30" >
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/sunset-pool_1203-3192.jpg') }}" alt="hotels">
                                <div class="overlay">
                                    <h2>Hotel</h2>
                                    <p>
                                        <a href="{{route('hotel-search')}}">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endif
                         @if(in_array("activity",$services))
                        <div class="col-md-4 col-sm-6 col-xs-12 mb-30">
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/skydiving-wallpapers-13.jpg') }}" alt="activity">
                                <div class="overlay">
                                    <h2>Activity</h2>
                                    <p>
                                        <a href="{{route('activity-search')}}">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                         @endif

                       @if(in_array("guide",$services))
                          <div class="col-md-4 col-sm-6 col-xs-12 mb-30">
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/2019-11-28.png') }}" alt="guide">
                                <div class="overlay">
                                    <h2>Guide</h2>
                                    <p>
                                        <a href="{{route('guide-search')}}">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                         @endif

                     @if(in_array("sightseeing",$services))
                        <div class="col-md-4 col-sm-6 col-xs-12 mb-30">
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/climate-landscape-paradise-hotel-sunset_1203-5734.jpg') }}" alt="sightseeing">
                                <div class="overlay">
                                    <h2>SightSeeing</h2>
                                    <p>
                                        <a href="{{route('sightseeing-search')}}">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                         @endif
                        <!-- daman -->
                         @if(in_array("itinerary",$services))
                        <div class="col-md-4 col-sm-6 col-xs-12 mb-30">
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/iternary.jpg') }}" alt="Packages">
                                <div class="overlay">
                                    <h2>Packages</h2>
                                    <p>
                                        <a href="{{route('itinerary-search')}}">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                         @endif
                        
                      
                         @if(in_array("transfer",$services))
                        <div class="col-md-4 col-sm-6 col-xs-12 mb-30">
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/bitcoin payment gateway.jpg') }}" alt="Tranfer">
                                <div class="overlay">
                                    <h2>Transfer</h2>
                                    <p>
                                        <a href="{{route('transfer-search')}}">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                         @endif

                          @if(in_array("restaurant",$services))
                        <div class="col-md-4 col-sm-6 col-xs-12 mb-30">
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/restaurant-image.jpg') }}" alt="Tranfer">
                                <div class="overlay">
                                    <h2>Restaurant</h2>
                                    <p>
                                        <a href="{{route('restaurant-search')}}">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                         @endif
                        
                      
                      <!--   <div class="col-sm-6 col-xs-12 mb-30">
                            <div class="hovereffect">
                                <img class="img-responsive" src="{{ asset('assets/images/agent/travelling-background-design_1262-2532.jpg') }}" alt="itenerary">
                                <div class="overlay">
                                    <h2>Itenerary</h2>
                                    <p>
                                        <a href="#">CLICK HERE</a>
                                    </p>
                                </div>
                            </div>
                        </div> -->


                    </div>


                </div>
            </div>
        </div>

    </div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')


</body>
</html>
