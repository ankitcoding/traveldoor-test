<?php
use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\LoginController;
?>
@include('agent.includes.top-header')

<style>
    
    .carousel-item img {
        height: 100%;
    }

    .carousel-item {
        height: 100%;
    }

    p.start_price {
        margin: 0;
    }

    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }

    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;

        border-radius: 5px;
    }
    .book_card {
    border: 1px solid gainsboro;
    -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=31, Direction=117, Color=#E3E3E3)";
    -moz-box-shadow: 3px 6px 31px 5px #E3E3E3;
    -webkit-box-shadow: 3px 6px 31px 5px #E3E3E3;
    box-shadow: 3px 6px 31px 5px #E3E3E3;
    filter: progid:DXImageTransform.Microsoft.Shadow(Strength=31, Direction=135, Color=#E3E3E3);
}
    .hotel_detail {
        height: auto;
    }

    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }

    .booking_label {
    background: #4CAF50;
    /* border-top-left-radius: 5px; */
    /* border-top-right-radius: 5px; */
    color: white;
    padding: 10px 15px;
    border: 1px solid #4caf50;
}

    .booking_detail {
        padding: 15px;
    }

    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }

    td p {
        margin: 0;
    }

    td {
        background: gainsboro;
    }

    table {
        border-collapse: separate;
    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #59d25a;
    }

    a.panel-title {
        position: relative !important;
        background: #dfffe3;
        color: green !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #3ca23d; */
    }
.panel-title {
    display: block;
    margin-top: 0;
    margin-bottom: 0;
    padding: 1.25rem;
    font-size: 18px;
    color: #4d4d4d;
    height: 48px;
}
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }

    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }

    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
           width: auto;
    max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #279628;
        z-index: 999;
        transform: none !important;
    }

    .tab-content {
        margin-top: 10px;
    }

    div.panel-heading {
        border: 1px solid #59d25a !important;
    }

    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
div#carousel-example-generic-captions {
    width: 100%;
}
.form-div{
    border: 1px solid gainsboro;
    padding: 15px;
}
.form-div input, .form-div select {
    border-radius: 0;
    border-color: #c8c8c8;
    /* background: #f3f3f3; */
}
.transfer_detail {
        height: auto;
    }
    .guide_detail {
        height: 150px;
    }
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('agent.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('agent.includes.nav')

	<div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">Book {{ucwords($booking_type)}}</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Home

                            <li class="breadcrumb-item active" aria-current="page">Book {{ucwords($booking_type)}}

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

            <!-- <div class="right-title">

                <div class="dropdown">

                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

                            class="mdi mdi-dots-horizontal"></i></button>

                    <div class="dropdown-menu dropdown-menu-right">

                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

                        <div class="dropdown-divider"></div>

                        <button type="button" class="btn btn-rounded btn-success">Submit</button>

                    </div>

                </div>

            </div> -->

        </div>

    </div>

   <div class="row">
      <div class="box">
          <div class="box-body">
            @php
            $action="";
            if($booking_type=="activity")
            {
                $action=route('confirm-activity-booking');
            }
            else if($booking_type=="hotel")
            {
                $action=route('confirm-hotel-booking');
            }
            else if($booking_type=="guide")
            {
                $action=route('confirm-guide-booking');
            }
            else if($booking_type=="package")
            {
                $action=route('confirm-itinerary-booking');
            }
            else if($booking_type=="sightseeing")
            {
                $action=route('confirm-sightseeing-booking');
            }
            else if($booking_type=="transfer")
            {
                $action=route('confirm-transfer-booking');
            }
            else if($booking_type=="restaurant")
            {
                $action=route('confirm-restaurant-booking');
            }

            @endphp
            



           <form id="booking_form" method="post" action="{{$action}}" enctype="multipart/form-data">
                 {{csrf_field()}}
             <div class="row mb-20" style="margin-bottom:60px !important">
                <div class="col-md-7">
                    <h4>Enter customer information in order to complete the booking</h4>
                    <div class="form-div">
                            <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
            
                                        <div class="form-group">
                                            <label for="customer_name">CUSTOMER NAME <span class="asterisk">*</span></label>
                                            <input type="text" placeholder="CUSTOMER NAME"class="form-control" id="customer_name" name="customer_name" required="required">
            
            
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
            
                                        <div class="form-group">
                                            <label for="customer_name">EMAIL<span class="asterisk">*</span></label>
                                            <input type="email" placeholder="CUSTOMER EMAIL"class="form-control" id="customer_email" name="customer_email" required="required">
            
            
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
            
                                        <div class="form-group">
                                            <label for="customer_name">PHONE<span class="asterisk">*</span></label>
                                            <input type="text" placeholder="CUSTOMER PHONE" class="form-control" id="customer_phone" name="customer_phone" required="required">
            
            
                                        </div>
                                    </div>
            
                                     <div class="col-sm-12 col-md-12 col-lg-12">
            
                                        <div class="form-group">
                                            <label for="customer_name">COUNTRY<span class="asterisk">*</span></label>
                                            <select class="form-control" id="customer_country" name="customer_country" required="required">
                                                <option value="">SELECT COUNTRY</option>
                                                @foreach($countries as $country)
                                                <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                @endforeach
                                            </select>
            
            
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-12 col-lg-12">
            
                                     <div class="form-group">
                                            <label for="customer_address">ADDRESS<span class="asterisk">*</span></label>
                                            <textarea placeholder="ADDRESS" class="form-control" id="customer_address" name="customer_address" required="required"></textarea>
            
            
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-12">
            
                                     <div class="form-group">
                                            <label for="customer_remarks">REMARKS</label>
                                            <textarea placeholder="REMARKS" class="form-control" id="customer_remarks" name="customer_remarks"></textarea>
            
            
                                        </div>
                                    </div>
                                      <div class="col-sm-12 col-md-12 col-lg-12">
                                     <div class="form-group">
                                            <label for="customer_file">ATTACHMENTS</label>
                                            <input type="file" name="customer_file[]" id="customer_file" class="form-control" multiple accept="image/*,application/pdf">
            
                                        </div>
                                    </div>
            
                                </div>
                                @php
                                $booking_details=serialize($booking_array);
                                echo "<input type='hidden' name='booking_details' value='".$booking_details."'>";
                                @endphp
                                <div class="book_btn">
                                    <button  type="submit" id="activity_book_btn" style="margin-left: auto;display: block;margin-right: 0 !important;border-radius: 5px;" class="btn btn-rounded btn-primary mr-10">CONFIRM BOOKING</button>
                                </div>
            
                    </div>
                    

                  
                </div>
                <div class="col-md-1">
                </div>
                 <div class="col-md-4">
            <div class="book_card">
                <div class="booking_label">
                    <p class="country_name ng-binding">PROCESS ORDER</p>
                </div>
            @if($booking_type=="activity")
                <div class="booking_detail">
                         <div class="hotel_detail">
                            <p class="hotel_desc">{{$get_activities->activity_name}}</p>
                            <p class="para"><b>Country  : </b>
                                @foreach($countries as $country)
                                @if($country->country_id==$get_activities->activity_country)
                                {{$country->country_name}}
                                @endif
                                @endforeach
                            </p>
                            

                            <p class="para"><b>City  : </b>
                                <?php
                                $fetch_city=ServiceManagement::searchCities($get_activities->activity_city,$get_activities->activity_country);
                                echo $fetch_city['name'];
                                ?>
                            </p>
                            <p class="para"><b>Location  : </b>{{$get_activities->activity_location}}</p>
                             <p class="para"><b>Date  : </b>{{ date('d-M-Y',strtotime($booking_array['booking_date']))}}</p>
                             <p class="para"><b>Time Slot  : </b>{{$booking_array['booking_time']}}</p>
                                    </div>
                
                                    <div class="" style="margin-top: 15px">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label for="total_price"><b>TOTAL : </b></label>
                                             </div>
                                             <div class="col-md-7">
                                                <span id="total_price_text">{{$get_activities->activity_currency}} {{$booking_array['booking_amount']}}</span>
                                                

                                            </div>


                                        </div>

                                </div>
                </div>
                @elseif($booking_type=="hotel")
                 <div class="booking_detail">
                     <div class="hotel_detail">
                        <p class="hotel_desc">{{$get_hotel->hotel_name}}  @php for($jt=1;$jt<=$get_hotel->hotel_rating;$jt++)
                                                    {
                                                        @endphp
                                                        <span class="fa fa-star checked" style="color:#d4af37"></span>
                                                        @php

                                                    } @endphp</p>
                        <p class="para"><b>Room Type  : </b>{{$booking_array['room_name']}}</p>
                         <p class="para"><b>Quantity  : </b>{{$booking_array['booking_room_quantity']}} Rooms</p>
                          <p class="para"><b>Checkin Date : </b>{{date('d F,Y',strtotime($booking_array['checkin_date']))}}</p>
                           <p class="para"><b>Checkout Date : </b>{{date('d F,Y',strtotime($booking_array['checkout_date']))}}</p>
                           <p class="para"><b>No of Nights : </b>{{$booking_array['no_of_days']}}</p>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                            
                                    <div class="">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label for="total_price">TOTAL : </label>
                                             </div>
                                             <div class="col-md-7">
                                                <span id="total_price_text">{{$booking_array['room_currency']}} {{$booking_array['booking_amount']}}</span>
                                                

                                            </div>


                                        </div>

                                </div>
               
                </div>

                @elseif($booking_type=="guide")
                  <div class="booking_detail">
                                 <div class="guide_detail">
                                    <p class="guide_desc">{{ucwords($get_guides['guide_first_name'])}} (GUIDE)</p>
                                      <p class="para"><b>From Date : </b>{{date('d F,Y',strtotime($booking_array['from_date']))}}</p>
                                       <p class="para"><b>To Date : </b>{{date('d F,Y',strtotime($booking_array['to_date']))}}</p>
                                       <p class="para"><b>No of Days : </b>{{$booking_array['no_of_days']}}</p>
                                </div>
                                <div class="">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <label for="total_price"><strong>TOTAL : </strong></label>
                                         </div>
                                         <div class="col-md-7">
                                            <span id="total_price_text">{{$booking_array['guide_currency']}} {{$booking_array['booking_amount']}}</span>
                                            

                                        </div>


                                    </div>

                            </div>
                </div>

                @elseif($booking_type=="package")
                 <div class="booking_detail">
                     <div class="hotel_detail">
                        <p class="hotel_desc">{{$get_itinerary->itinerary_tour_name}} ({{$get_itinerary->itinerary_tour_days}} Nights)</p>
                        <p class="para"><b> Passenger Count : </b>{{($booking_array['booking_adult_count']+$booking_array['booking_child_count'])}} Pax</p>
                          <p class="para"><b>From Date : </b>{{date('d F,Y',strtotime($booking_array['booking_from_date']))}}</p>
                           <p class="para"><b>To Date : </b>{{date('d F,Y',strtotime($booking_array['booking_to_date']))}}</p>

                    </div>
                    <br>
                    <br>
                    <br>
            
                    <div class="">
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="total_price"><strong>TOTAL : </strong></label>
                             </div>
                             <div class="col-md-7">
                             <span id="total_price_text">{{$booking_array['itinerary_currency']}} {{$booking_array['booking_amount']}}</span>
                                

                            </div>


                        </div>

                </div>
               

                  

                </div>

                @elseif($booking_type=="sightseeing")
                 <div class="booking_detail">
                     <div class="hotel_detail">
                        <p class="hotel_desc">{{$get_sightseeing->sightseeing_tour_name}} ({{$get_sightseeing->sightseeing_distance_covered}} Kms)</p>
                          <p class="para"><b>Booking Date : </b>{{date('d F,Y',strtotime($booking_array['booking_date']))}}</p>
                         <p class="para"><b>Passenger Count  : </b> {{$booking_array['booking_adult_count']}} Adults, {{$booking_array['booking_child_count']}} Children</p>
                         @if(session()->get('sightseeing_tour_type')=="private")
                          <p class="para"><b>Tour Type : </b>Private Tour</p>
                         <p class="para"><b>Vehicle Type : </b>{{$booking_array['vehicle_type_name']}}</p>
                         <p class="para"><b>Guide Name : </b>{{$booking_array['guide_name']}}</p>
                          <p class="para"><b>Driver Name : </b>{{$booking_array['driver_name']}}</p>
                          @else
                           <p class="para"><b>Tour Type : </b>Group Tour</p>
                          @endif

                         {{--
                           <p class="para"><b>Checkout Date : </b>{{date('d F,Y',strtotime($booking_array['checkout_date']))}}</p>
                           <p class="para"><b>No of Days : </b>{{$booking_array['no_of_days']}}</p>--}}
                    </div>
                    <br>
                    <br>
                    <br>
            
                    <div class="">
                            <div class="row">
                                <div class="col-md-5">
                                    <label for="total_price"><strong>TOTAL : </strong></label>
                             </div>
                             <div class="col-md-7">
                             <span id="total_price_text">{{$booking_array['sightseeing_currency']}} {{$booking_array['booking_amount']}}</span>
                                

                            </div>


                        </div>

                </div>
               

                  

                </div>

                @elseif($booking_type=="transfer")
                 <div class="booking_detail">
                                                        <div class="transfer_detail">
                                                              @php
                                                            $vehicle_type=$get_transfer->vehicleType->vehicle_type_name;
                                                            $vehicle_name=$get_transfer->vehicle->vehicle_name;
                                                            @endphp
                                                            <p class="guide_desc">{{ucwords($vehicle_name)}} ({{ucwords($vehicle_type)}}) <br> <small>{{$get_transfer->transfer_vehicle_info}}</small></p>
                                                            <p class="para"><b>Transfer Date : </b>{{date('d F,Y',strtotime($booking_array['transfer_date']))}}</p>
                                                            <p class="para"><b>Transfer Time : </b>{{$booking_array['transfer_time']}}</p>

                                                            @if($get_transfer->transfer_type=="city")

                                                            <p class="para"><b>FROM CITY :  </b>
                                                                @php
                                                                $from_city=ServiceManagement::searchCities(session()->get('transfer_from_city'),$get_transfer->transfer_country);
                                                                echo $from_city['name'];
                                                                @endphp
                                                            </p>

                                                            <p class="para"><b>TO CITY :  </b>
                                                                @php
                                                                $to_city=ServiceManagement::searchCities(session()->get('transfer_to_city'),$get_transfer->transfer_country);
                                                                echo $to_city['name'];
                                                                @endphp
                                                            </p>
                                                            <p class="para"><b>PICK UP LOCATION :  </b>{{session()->get('transfer_pickup_location')}}
                                                            </p>
                                                            <p class="para"><b>DROP OFF LOCATION :  </b>{{session()->get('transfer_dropoff_location')}}
                                                            </p>

                                                            @elseif($get_transfer->transfer_type=="from-airport")
                                                            <p class="para"><b>FROM AIRPORT :  </b>
                                                                @php
                                                                $to_city=LoginController::searchAirports(session()->get('transfer_from_city_airport'));
                                                                echo $to_city['airport_master_name'];
                                                                @endphp
                                                            </p>
                                                            <p class="para"><b>TO CITY :  </b>
                                                                @php
                                                                $to_city=ServiceManagement::searchCities(session()->get('transfer_to_city_airport'),$get_transfer->transfer_country);
                                                                echo $to_city['name'];
                                                                @endphp
                                                            </p>
                                                            <p class="para"><b>DROP OFF LOCATION :  </b>{{session()->get('transfer_location')}}
                                                            </p>

                                                            @else
                                                            
                                                            <p class="para"><b>FROM CITY :  </b>
                                                                @php
                                                                $from_city=ServiceManagement::searchCities(session()->get('transfer_from_city_airport'),$get_transfer->transfer_country);
                                                                echo $from_city['name'];
                                                                @endphp
                                                            </p>
                                                            <p class="para"><b>TO AIRPORT :  </b>
                                                                @php
                                                                $to_city=LoginController::searchAirports(session()->get('transfer_to_city_airport'));
                                                                echo $to_city['airport_master_name'];
                                                                @endphp
                                                            </p>
                                                            <p class="para"><b>PICK UP LOCATION :  </b>{{session()->get('transfer_location')}}
                                                            </p>

                                                            @endif

                                                            @if($booking_array['guide_id']!="" && $booking_array['guide_id']!=0)

                                                             <p class="para"><b>GUIDE NAME : </b>{{$booking_array['guide_name']}}</p>
                                                             @endif
                                                        </div>
                                                        <br>
                                                        <div class="">
                                                            <div class="row">
                                                                <div class="col-md-5">
                                                                    <label for="total_price"><strong>TOTAL : </strong></label>
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <span id="total_price_text">{{$booking_array['transfer_currency']}} {{$booking_array['booking_amount']}}</span>

                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                     @elseif($booking_type=="restaurant")
                 <div class="booking_detail">
                     <div class="hotel_detail">
                        <p class="hotel_desc">{{$get_restaurant->restaurant_name}}</p>
                         <p class="para"><b>Selected Food  : </b><br>
                         @php
                         $selected_food_text=explode("---",$booking_array['selected_food']);
                     @endphp
                     @foreach($selected_food_text as $text)
                     {{$text}}<br>
                     @endforeach
                 </p>
                          <p class="para"><b>Visit Date : </b>{{date('d F,Y',strtotime($booking_array['selected_date']))}}</p>
                           <p class="para"><b>Visit Time : </b>{{$booking_array['selected_time']}}</p>
                           <p class="para"><b>No of Persons : </b>{{$booking_array['no_of_persons']}}</p>
                                    </div>
                                    <br>
                                    <br>
                                    <br>
                            
                                    <div class="">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label for="total_price">TOTAL : </label>
                                             </div>
                                             <div class="col-md-7">
                                                <span id="total_price_text">{{$booking_array['restaurant_currency']}} {{$booking_array['booking_amount']}}</span>
                                                

                                            </div>


                                        </div>

                                </div>
               
                </div>

                @endif
            </div>
       
               
                </div>
                 
    </div>
    </form>
           
                
              
          </div>
      </div>
   </div>

</div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')