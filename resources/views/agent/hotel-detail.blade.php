<?php

use App\Http\Controllers\ServiceManagement;

?>

@include('agent.includes.top-header')



<style>



    .carousel-item img {

        height: 100%;

    }



    .carousel-item {

        height: 100%;

    }



    p.start_price {

        margin: 0;

    }



    p.country_name.ng-binding {

        font-size: 20px;

        margin: 0;

    }



    .book_card {

        /* padding: 15px; */

        background: #fefeff;

        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);

        margin-bottom: 50px;



        border-radius: 5px;

    }



    .hotel_detail {

        height: 150px;

    }



    a.moredetail.ng-scope {

        background: gainsboro;

        padding: 7px 10px;

        /* margin-bottom: 10px; */

    }



    .booking_label {

        background: #5d53ce;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;

        color: white;

        padding: 10px 15px;

    }



    .booking_detail {

        padding: 15px;

    }



    a.btn.btn-outline.btn-circle.book_btn1 {

        background: #E91E63;

        border-radius: 5px !IMPORTANT;

        padding: 5px 20px;

        width: auto !important;

        height: auto;

        line-height: 2;

        color: white;

    }



    td p {

        margin: 0;

    }



    td {

        background: gainsboro;

    }



    table {

        border-collapse: separate;

    }



    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: 1px solid #59d25a;

    }



    a.panel-title {

        position: relative !important;

        background: #dfffe3;

        color: green !important;

        padding: 13px 20px 13px 85px !important;

        /* border-bottom: 1px solid #3ca23d; */

    }

    .panel-title {

        display: block;

        margin-top: 0;

        margin-bottom: 0;

        padding: 1.25rem;

        font-size: 18px;

        color: #4d4d4d;

        height: 48px;

    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: none !important;

    }



    .panel-body {

        background: white;

        /* border: 1px solid #59d25a; */

        padding: 10px !important;

    }



    a.panel-title:before {

        content: attr(title) !important;

        position: absolute !important;

        top: 0px !important;

        opacity: 1 !important;

        left: 0 !important;

        padding: 12px 10px;

        width: auto;

        max-width: 250px;

        text-align: center;

        color: white;

        font-family: inherit !important;

        height: 48px;

        background: #279628;

        z-index: 999;

        transform: none !important;

    }



    .tab-content {

        margin-top: 10px;

    }



    div.panel-heading {

        border: 1px solid #59d25a !important;

    }



    .panel {

        border-top: none !important;

        margin-bottom: 5px !important;

    }

    div#carousel-example-generic-captions {

        width: 100%;

    }

    /*daman css*/

    .hotel-div {

        width:75%;

        float:left;

        padding: 15px;

        display: flex;

        background: white;

    }



    .hotel-list-div {

        display: block;

        border: 1px solid #c6bee6;

        border-radius: 5px;

        position: relative;

        /* clear: both; */

        height: 101%;

    }

    .hotel-img-div {



        width: 40% !important;

        float:left;

    }



    .hotel-details {

       float:left;

       width: 60%;

       padding: 0 15px;

   }



   .hotel-info-div {

    width: 25%;

    float: left;

    padding: 15px;

    background: #e6e0ff;

    text-align: right;

    height: 100%;

}



.checked {

    color: orange;

}



span.hotel-s {

    border: 1px solid #F44336;

    padding: 2px 5px;

    display: inline-block;

    color: #f44336;

    font-size: 12px;

    margin: 0 10px 0 0;

}



p.hotel-name {

    font-size: 22px;

    margin: 10px 0 0;

    color: black;

    float: left;

}



.rate-no {

    float: right;

}



.rate-no {

    float: right;

    display: block;

    margin-top: 14px;

    background: #2d3134;

    color: white;

    padding: 1px 8px;

    font-size: 12px;

    border-radius: 5px;

}



.heading-div {

    clear: both;

}



.rating {

    display: block;

    float: left;

    list-style: none;

    margin: 0;

    padding: 0;

}



p.r-number {

    float: right;

}



p.time-info {

    color: #4CAF50;

}



p.info {

    clear: both;

    margin: 0;

}



span.tag-item {

    background: #ffcbcd;

    padding: 5px 10px;

    border-radius: 5px;

    color: #ff4e54;

}



.inclusions {

    margin: 20px 0;

}



span.inclusion-item {

    padding: 10px 10px 0 0;

    color: #644ac9;

}



img.icon-i {

    width: auto;

    height: 20px;

}



p.include-p {

    color: black;

}



.inclusion-p {

    color: green

}



p.price-p span {

    background: #ee2128;

    color: white;

    padding: 3px 5px 3px 9px;

    border-radius: 5px;

    position: relative;

    z-index: 9999;

    border-top-right-radius: 5px;

    border-bottom-right-radius: 5px;

    margin-left: 20px;

    border-bottom-left-radius: 4px;

    font-size: 12px;

}



p.price-p span:before {

    content: "";

    width: 15px;

    height: 14.5px;

    background: #ee2128;

    position: absolute;

    transform: rotate(45deg);

    top: 3.5px;

    left: -6px;

    border-radius: 0px 0px 0px 3px;

    z-index: -1;

}



p.price-p span:after {

    content: "";

    background: white;

    width: 4px;

    height: 4px;

    position: absolute;

    top: 50%;

    left: 0px;

    transform: translateY(-50%);

    border-radius: 50%;

}



p.price-p {

    color: #ee2128;

}



p.tax {

    font-size: 12px;

    margin: 0;

}



p.days {

    font-size: 12px;

    margin: 0;

}



p.offer {

    font-size: 25px;

    color: black;

    font-weight: bold;

    margin: 0;

}



p.cut-price {

    margin: 0;

    text-decoration: line-through;

    font-size: 19px;

}



.login-a {

    color: #0088ff;

    font-size: 15px;

    font-weight: bold;

    margin-top: 10px;

    display: block;

}

@media screen and (max-width:1200px){

    p.hotel-name {

        font-size: 17px;

        margin: 10px 0 0;

        color: black;

        float: left;

    }

    p.r-number {

        float: right;

        font-size: 12px;

    }

    span.inclusion-item {

        padding: 10px 10px 0 0;

        color: #644ac9;

        display: block;

    }

    p.cut-price {

        margin: 0;

        text-decoration: line-through;

        font-size: 16px;

    }

    p.offer {

        font-size: 20px;

        color: black;

        font-weight: bold;

        margin: 0;

    }

    .login-a {

        color: #0088ff;

        font-size: 13px;

        font-weight: bold;

        margin-top: 10px;

        display: block;

    }

}

@media screen and (max-width:1200px){

    .hotel-div {

        width: 100%;

        float: left;

        padding: 15px;

        display: flex;

        background: white;

    }

    .hotel-info-div {

        width: 100%;

        height: 100%;

        float: left;

        padding: 15px;

        background: #e6e0ff;

        text-align: left;

    }

    span.inclusion-item {

        padding: 10px 10px 0 0;

        color: #644ac9;

        display: inline;

    }

}

@media screen and (max-width:992px){

    p.hotel-name {

        font-size: 17px;

        margin: 10px 0 0;

        color: black;

        float: none;

    }

    .rate-no {

        float: none;

        display: inline;

        margin-top: 14px;

        background: #2d3134;

        color: white;

        padding: 1px 8px;

        font-size: 12px;

        border-radius: 5px;

    }

    .rating {

        display: block;

        float: none;

        list-style: none;

        margin: 0;

        padding: 0;

    }

    p.r-number {

        float: none;

        font-size: 12px;

    }

    .hotel-details {

        float: none !important;

        width: 100% !important;

        padding: 0 15px;

        margin-top: 20px;

    }

    .hotel-list-div {

        display: block;

        border: 1px solid #c6bee6;

        border-radius: 5px;

        position: relative;

        /* clear: both; */

        height: 101%;

    }

    .hotel-div {

        width: 100%;

        float: none;

        padding: 15px;

        display: block;

        background: white;

    }

    .hotel-img-div {

        width: 100% !important;

        float: none !important;

        display: block !important;

    }

    a.flex-prev,a.flex-next {

        display: none;

    }

    span.inclusion-item {

        padding: 10px 10px 0 0;

        color: #644ac9;

        display: block;

    }

}



/*daman*/



img.slide {

    width: 100%;

    height: 100%;

}



.carousel {

    position: relative;

    height: 100%;

}



.carousel-inner {

    position: relative;

    width: 100%;

    overflow: hidden;

    height: 100%;

}



.carousel-item.active,

.carousel-item {

    height: 100%;

}



.carousel-item img {

    height: 100%;

}



.grid {

    padding: 10px 0;

    height: 100%;

    position: relative;

}



.c-div {

    border: 1px solid #8cd08e;

    padding: 5px;

    clear: both;

    background: #e8ffe9;

    display: block;

    width: 100%;

    border-radius: 5px;

    height: 130%;

}



.i-info {

    float: left;

    width: 80%;

}



.i-icon {

    float: left;

    width: 20%;

}



.i-info p {

    color: black;

    font-size: 13px;

    margin: 0;

}



.i-info a {

    color: #2196F3;

    font-size: 13px;

    margin-top: 6px;

    display: block;

}



.overlay-text {

    position: absolute;

    bottom: 20px;

    left: 10px;

    background: #000000a3;

    padding: 5px 10px;

    border-radius: 5px;

}



.overlay-text p {

    color: white;

    font-size: 11px;

    font-weight: bold;

    margin: 0;

}



.div-info {

    clear: both;

    height: 104%;

    border: 1px solid #e89a94;

    padding: 10px;

    border-radius: 5px;

    background: #ffe8e8;

}



.left {

    float: left;

    width: 50%;

}



.left p {

    color: #515cd4;

    font-size: 17px;

    font-weight: bold;

    margin: 0;

}



.left li {

    color: #d63226;

}



.right {

    float: left;

    width: 50%;

    text-align: right;

}



p.days {

    font-size: 12px;

    margin: 0;

}



p.cut-price {

    text-decoration: line-through;

    margin: 0;

}



p.offer {

    margin: 0;

    font-size: 18px;

    color: black;

}



p.use-code {

    /* margin: 0; */

    color: #d63226;

}



a.b-room {

    color: #515ad2;

    font-weight: bold;

}



button.book-btn {

    border: none;

    background: linear-gradient(45deg, #4b61d8, #6b41bd);

    padding: 10px;

    color: white;

    border-radius: 21px;

}



.div-hotel {

    border: 1px solid gainsboro;

    height: 103%;

    padding: 10px;

    border-radius: 5px;

}



p.p-para {

    color: #00bcd4;

}



span.rate {

    background: #00BCD4;

    color: white;

    padding: 10px;

    border-radius: 5px;

    /* margin-top: 10px; */

    display: inline-block;

    margin-right: 10px;

}



a.review {

    color: #2196F3;

}



.t-item:first-child {

    border-top-left-radius: 5px;

    /* width: calc(100% / 2.5); */

}



.t-item:last-child {

    border-right: 1px solid #a8a8a8;

    border-top-right-radius: 5px;

    width: calc(100% / 2.5);

}



.t-item {

    float: left;

    width: calc(945px / 5);

    background: #dedede;

    border: 1px solid #a8a8a8;

    /* border-radius: 5px; */

    padding: 10px;

    border-right: none;

}



.t-item-i:last-child {

    border-right: 1px solid #a8a8a8;

    width: 390px;

}



.t-item-i.cstm-t:first-child {

    border-left: 1px solid #a8a8a8 !important;

}





.t-item-i {

    float: left;

    width: calc(945px / 5);

    background: #fff;

    border: 1px solid #a8a8a8;

    /* border-radius: 5px; */

    border-left: 1px solid transparent !IMPORTANT;

    padding: 10px;

    /* border-right: 1px solid transparent !IMPORTANT; */

    border-top: none;

}



.table-row {

    width: 100%;



    clear: both;

    border-radius: 5px;

}



.h-table {

    /* border: 1px solid black; */

    width: 945px;

    height: 100%;

    border-radius: 5px;

}



.hotel-main {

    height: 100%;

}



.ul-li li:before {

    font-family: "FontAwesome";

    content: "\f00c";

    display: inline-block;

    padding-right: 3px;

    vertical-align: middle;

    font-weight: 100;

}



.ul-li li {

    list-style: none;

}



ul.ul-li {

    padding: 0px 10px;

}

.over {

    overflow-x: auto;

    padding: 20px;

    

}





.img-slide{

   height: 400px !important;

   width: 100% !important;

}

</style>



<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">



    <div class="wrapper">



        @include('agent.includes.top-nav')



        <div class="content-wrapper">



            <div class="container-full clearfix position-relative">	



                @include('agent.includes.nav')



                <div class="content">







                    <div class="content-header">



                        <div class="d-flex align-items-center">



                            <div class="mr-auto">



                                <h3 class="page-title">Hotel Details</h3>



                                <div class="d-inline-block align-items-center">



                                    <nav>



                                        <ol class="breadcrumb">



                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>



                                            <li class="breadcrumb-item" aria-current="page">Home



                                                <li class="breadcrumb-item active" aria-current="page">Hotel Details



                                                </li>



                                            </ol>



                                        </nav>



                                    </div>



                                </div>



            <!-- <div class="right-title">



                <div class="dropdown">



                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i



                            class="mdi mdi-dots-horizontal"></i></button>



                    <div class="dropdown-menu dropdown-menu-right">



                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>



                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>



                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>



                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>



                        <div class="dropdown-divider"></div>



                        <button type="button" class="btn btn-rounded btn-success">Submit</button>



                    </div>



                </div>



            </div> -->



        </div>



    </div>



    <div class="row">

        <div class="col-12">

            <div class="box">



                <div class="box-body">

                    <div class="row">

                        <div class="col-md-12">

                            <div class="row">

                                <div class="col-lg-7 col-md-12">

                                    <div class="grid">

                                        <div id="carousel-example-generic-captions" class="carousel slide"

                                        data-ride="carousel">

                                            <!-- Indicators -->

                                            <ol class="carousel-indicators">

                                                <?php $hotelimage=unserialize($get_hotels->hotel_images);

                                                if(count($hotelimage)==1)

                                                {

                                                    $hotelimagecount= count($hotelimage);

                                                }

                                                else

                                                {

                                                    $hotelimagecount= count($hotelimage) -1;

                                                }



                                                for($i=0;$i<count($hotelimage);$i++)

                                                {

                                                    if($i==0)

                                                    {

                                                        $newclss='active';

                                                    }

                                                    else

                                                    {

                                                        $newclss='';

                                                    }

                                                    echo '<li data-target="#carousel-example-generic-captions" data-slide-to="'.$i.'"

                                                    class="'.$newclss.'"></li>';

                                                } ?>



                                            </ol>

                                                <!-- Wrapper for slides -->

                                            <div class="carousel-inner" role="listbox">

                                                <?php



                                                for($i=0;$i<count($hotelimage);$i++)

                                                {

                                                   if($i==0)

                                                   {

                                                    $newclss_nxt='carousel-item-next carousel-item-left';

                                                }

                                                else

                                                {

                                                    $newclss_nxt='';

                                                }

                                                if($i==$hotelimagecount)

                                                {

                                                    $newclss_new='active carousel-item-left';

                                                }

                                                else

                                                {

                                                    $newclss_new='';

                                                }

                                                echo '<div class="carousel-item '.$newclss_nxt.' '.$newclss_new.'   ">

                                                <img src="'.asset("assets/uploads/hotel_images"). '/'.$hotelimage[$i].'"

                                                class="img-fluid img-slide" alt="slide-1">



                                                </div>';

                                                ?>



                                                 <?php } ?>





                                                       <!--  <div class="carousel-item active carousel-item-left">

                                                            <img src="https://adminvoice-admin-template.multipurposethemes.com/images/img3.jpg"

                                                                class="img-fluid" alt="slide-3">

                                                            <div class="carousel-caption">

                                                                <h3>Third here</h3>

                                                                <p>this is the subcontent you can use this</p>

                                                            </div>

                                                        </div> -->

                                            </div>

                                                    <!-- Controls -->

                                                    <a class="carousel-control-prev" href="#carousel-example-generic-captions"

                                                    role="button" data-slide="prev">

                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>

                                                    <span class="sr-only">Previous</span>

                                                </a>

                                                <a class="carousel-control-next" href="#carousel-example-generic-captions"

                                                role="button" data-slide="next">

                                                <span class="carousel-control-next-icon" aria-hidden="true"></span>

                                                <span class="sr-only">Next</span>

                                            </a>

                                        </div>

                                    </div>



                                </div>

                                <div class="col-lg-5 col-md-12">

                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="grid">

                                                <img src="https://r1imghtlak.mmtcdn.com/2e8cf9dc6dd611e794fb025f77df004f.jpg?&output-quality=75&downsize=*:350&crop=520:350;2,0&output-format=jpg"

                                                class="slide">

                                                <div class="overlay-text">

                                                    <p>ROOM IMAGES</p>

                                                </div>

                                            </div>





                                        </div>

                                        <div class="col-md-12">

                                            <div class="row">

                                                <div class="col-md-6">

                                                    <div class="grid">

                                                        <img src="https://r1imghtlak.mmtcdn.com/egj07viq952t1407vmmf7j05002i.jpg?&output-quality=75&downsize=*:350&crop=520:350;2,0&output-format=jpg"

                                                        class="slide">

                                                        <div class="overlay-text">

                                                            <p>RESTAURANT</p>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="col-md-6">

                                                    <div class="row">

                                                        <div class="col-md-12">

                                                            <div class="grid">

                                                                <div class="c-div">

                                                                    <div class="i-info">

                                                                        <p>Special secret deal for you</p>

                                                                        <a href="#">UNLOCK NOW</a>

                                                                    </div>

                                                                    <div class="i-icon">

                                                                        <i class="fa fa-gift"></i>

                                                                    </div>



                                                                </div>

                                                            </div>

                                                        </div>





                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>



                    </div>

                    <div class="row" style="margin-top:30px">

                        <div class="col-lg-7 col-md-12" style="margin-bottom:25px">

                            <div class="div-info">

                                <div class="left">

                                    <p>Palm Room</p>

                                    <ul class="ul-li">

                                        <li>Non Refundable</li>

                                        <li>Breakfast</li>

                                    </ul>

                                </div>

                                <div class="right">

                                    <p class="days">Per night</p>

                                    <p class="cut-price">INR 5,480</p>

                                    <p class="offer">INR 3,969</p>

                                    <p class="use-code">Code Used ADVANCEOFFD2</p>

                                </div>

                                <div class="left">

                                    <a href="#" class="b-room">OTHER ROOMS</a>

                                </div>

                                <div class="right">

                                    <button class="book-btn">Book This Now</button>

                                </div>

                            </div>

                        </div>

                        <div class="col-lg-5 col-md-12" style="margin-bottom:25px">

                            <div class="div-hotel">

                                <p class="p-para"><span class="rate">4.3</span>1421 Ratings & 1116 Reviews</p>

                                <p class="n-para">The rooms were big and clean. We got the 4 complimentary beers only

                                in...</p>

                                <div class="left">

                                    <a href="#" class="b-room">Nutan Bhambhani</a>

                                </div>

                                <div class="right">

                                    <a href="#" class="review">MORE REVIEW</a>

                                </div>

                            </div>



                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12">

                             <ul class="nav nav-tabs customtab" role="tablist">

                              <!--   <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Itenary"

                                        role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span

                                            class="hidden-xs-down">Itenary</span></a> </li>

                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Blackout" role="tab"><span

                                            class="hidden-sm-up"><i class="ion-person"></i></span> <span

                                            class="hidden-xs-down">Blackout</span></a> </li> -->

                                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#policy" role="tab"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span

                                class="hidden-xs-down">Hotel Policy</span></a> </li>

                                <li class="nav-item"> <a class="nav-link " data-toggle="tab" href="#Inclusion" role="tab"><span class="hidden-sm-up"><i class="ion-email"></i></span> <span

                                            class="hidden-xs-down">Inclusions</span></a> </li>

                                <li class="nav-item"> <a class="nav-link " data-toggle="tab" href="#Exclusion" role="tab"><span

                                            class="hidden-sm-up"><i class="ion-home"></i></span> <span

                                            class="hidden-xs-down">Exclusions</span></a> </li>

                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Cancellation"

                                        role="tab"><span class="hidden-sm-up"><i class="ion-person"></i></span> <span

                                            class="hidden-xs-down">Cancellation</span></a> </li>

                                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tc" role="tab"><span

                                            class="hidden-sm-up"><i class="ion-email"></i></span> <span class="hidden-xs-down">T

                                            & C</span></a> </li>

                            </ul>

                            <!-- Tab panes -->

                            <div class="tab-content">

                                <div class="tab-pane active" id="policy" role="tabpanel">

                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="tab-content">

                                                <div id="navpills-1" class="tab-pane active">

                                                    <!-- Categroy 1 -->

                                                    <div class=" tab-pane animation-fade active" id="category-1"

                                                        role="tabpanel">

                                                        <div class="panel-group panel-group-simple panel-group-continuous"

                                                            id="accordion2" aria-multiselectable="true" role="tablist">

                                                            <!-- Question 1 -->

                                                            @if($get_hotels->hotel_other_policies!="" && $get_hotels->hotel_other_policies!=null)

                                                        @php

                                                        $hotel_other_policies=unserialize($get_hotels->hotel_other_policies);



                                                        for($adoon_count=0;$adoon_count< count($hotel_other_policies);$adoon_count++)

                                                        {

                                                        @endphp

                                                            <div class="panel">

                                                                <div class="panel-heading" id="question-0{{$adoon_count}}" role="tab">

                                                                    <a class="panel-title" title="{{$hotel_other_policies[$adoon_count]['policy_name']}}"

                                                                        aria-controls="answer-0{{$adoon_count}}" aria-expanded="true"

                                                                        data-toggle="collapse" href="#answer-0{{$adoon_count}}"

                                                                        data-parent="#accordion2">

                                                                    </a>

                                                                </div>

                                                                <div class="panel-collapse collapse" id="answer-0{{$adoon_count}}"

                                                                    aria-labelledby="question-0{{$adoon_count}}" role="tabpanel">

                                                                    <div class="panel-body">

                                                                {{$hotel_other_policies[$adoon_count]['policy_desc']}}

                                                                </div>

                                                            </div>



                                                        </div>

                                                        @php

                                                        }

                                                        @endphp                 

                                                        @else

                                                        No Data Available 

                                                        @endif

                                                    </div>



                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                                <div class="tab-pane " id="Inclusion" role="tabpanel">

                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="tab-content">

                                                <div id="navpills-1" class="tab-pane active">

                                                    <!-- Categroy 1 -->

                                                    <div class=" tab-pane animation-fade active" id="category-1"

                                                        role="tabpanel">

                                                        <div class="panel-group panel-group-simple panel-group-continuous"

                                                            id="accordion2" aria-multiselectable="true" role="tablist">

                                                            <!-- Question 1 -->

                                                            <div class="panel">

                                                                <div class="panel-heading" id="question-1" role="tab">

                                                                    <a class="panel-title" title="Inclusions"

                                                                        aria-controls="answer-1" aria-expanded="true"

                                                                        data-toggle="collapse" href="#answer-1"

                                                                        data-parent="#accordion2">

                                                                    </a>

                                                                </div>

                                                                <div class="panel-collapse collapse show" id="answer-1"

                                                                    aria-labelledby="question-1" role="tabpanel">

                                                                    <div class="panel-body">

                                                                <?php

                                                                     if($get_hotels->hotel_inclusions!="" && $get_hotels->hotel_inclusions!=null)

                                                                    {

                                                                     echo  $get_hotels->hotel_inclusions;

                                                                    }

                                                                     else

                                                                     {

                                                                         echo "No Data Available";

                                                                         

                                                                     }

                                                                 ?>

                                                                </div>

                                                            </div>



                                                        </div>

                                                    </div>



                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                                <div class="tab-pane" id="Exclusion" role="tabpanel">

                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="tab-content">

                                                <div id="navpills-1" class="tab-pane active">

                                                    <div class=" tab-pane animation-fade active" id="category-1"

                                                        role="tabpanel">

                                                        <div class="panel-group panel-group-simple panel-group-continuous"

                                                            id="accordion2" aria-multiselectable="true" role="tablist">

                                                            <div class="panel">

                                                                <div class="panel-heading" id="question-1" role="tab">

                                                                    <a class="panel-title" title="Exclusions"

                                                                        aria-controls="answer-1" aria-expanded="true"

                                                                        data-toggle="collapse" href="#answer-1"

                                                                        data-parent="#accordion2">

                                                                    </a>

                                                                </div>

                                                                <div class="panel-collapse collapse show" id="answer-1"

                                                                    aria-labelledby="question-1" role="tabpanel">

                                                                    <div class="panel-body">

                                                                       <?php

                                                                     if($get_hotels->hotel_exclusions!="" && $get_hotels->hotel_exclusions!=null)

                                                                    {

                                                                     echo  $get_hotels->hotel_exclusions;

                                                                    }

                                                                     else

                                                                     {

                                                                         echo "No Data Available";

                                                                         

                                                                     }

                                                                 ?>

                                                                    </div>

                                                                </div>

                                                            </div>



                                                        </div>

                                                    </div>

                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div class="tab-pane" id="Cancellation" role="tabpanel">

                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="tab-content">

                                                <div id="navpills-1" class="tab-pane active">

                                                    <div class=" tab-pane animation-fade active" id="category-1"

                                                        role="tabpanel">

                                                        <div class="panel-group panel-group-simple panel-group-continuous"

                                                            id="accordion2" aria-multiselectable="true" role="tablist">

                                                            <div class="panel">

                                                                <div class="panel-heading" id="question-1" role="tab">

                                                                    <a class="panel-title" title="Cancellation"

                                                                        aria-controls="answer-1" aria-expanded="true"

                                                                        data-toggle="collapse" href="#answer-1"

                                                                        data-parent="#accordion2">

                                                                    </a>

                                                                </div>

                                                                <div class="panel-collapse collapse show" id="answer-1"

                                                                    aria-labelledby="question-1" role="tabpanel">

                                                                    <div class="panel-body">

                                                                       <?php

                                                                             if($get_hotels->hotel_cancel_policy!="" && $get_hotels->hotel_cancel_policy!=null)

                                                                            {

                                                                             echo  $get_hotels->hotel_cancel_policy;

                                                                            }

                                                                             else

                                                                             {

                                                                                 echo "No Data Available";

                                                                                 

                                                                             }

                                                                         ?>

                                                                       

                                                                    </div>

                                                                </div>

                                                            </div>



                                                        </div>

                                                    </div>

                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                               <div class="tab-pane" id="tc" role="tabpanel">

                                    <div class="row">

                                        <div class="col-md-12">

                                            <div class="tab-content">

                                                <div id="navpills-1" class="tab-pane active">

                                                    <div class=" tab-pane animation-fade active" id="category-1"

                                                        role="tabpanel">

                                                        <div class="panel-group panel-group-simple panel-group-continuous"

                                                            id="accordion2" aria-multiselectable="true" role="tablist">

                                                            <div class="panel">

                                                                <div class="panel-heading" id="question-1" role="tab">

                                                                    <a class="panel-title" title="TC"

                                                                        aria-controls="answer-1" aria-expanded="true"

                                                                        data-toggle="collapse" href="#answer-1"

                                                                        data-parent="#accordion2">

                                                                    </a>

                                                                </div>

                                                                <div class="panel-collapse collapse show" id="answer-1"

                                                                    aria-labelledby="question-1" role="tabpanel">

                                                                    <div class="panel-body">

                                                                       

                                                                       <?php

                                                                             if($get_hotels->hotel_terms_conditions!="" && $get_hotels->hotel_terms_conditions!=null)

                                                                            {

                                                                             echo  $get_hotels->hotel_terms_conditions;

                                                                            }

                                                                             else

                                                                             {

                                                                                 echo "No Data Available";

                                                                                 

                                                                             }

                                                                         ?>

                                                                    </div>

                                                                </div>

                                                            </div>



                                                        </div>

                                                    </div>

                                                </div>



                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

            

               

                    @php

                        $hotel_season_details=unserialize($get_hotels->hotel_season_details);

                        $hotel_markup_details=unserialize($get_hotels->hotel_markup_details);

                        $hotel_blackout_dates=unserialize($get_hotels->hotel_blackout_dates);

                        $hotel_surcharge_details=unserialize($get_hotels->hotel_surcharge_details);

                        $rate_allocation_details=unserialize($get_hotels->rate_allocation_details);

                        for($rate_allocation_count=0;$rate_allocation_count< count($hotel_season_details);$rate_allocation_count++)

                        {

                    @endphp

                    <div class="container">

            <div class="row" style="margin-top:40px">

               <div class="col-md-12">

                    <div class="over">

                        <div class="h-table" style="width: 985px">



                            <div class="row">

                                <div class="col-md-3 t-item">

                                    <p>ROOM TYPE</p>

                                </div>

                                <div class="col-md-3 t-item">

                                    <p>OPTIONS</p>

                                </div>

                                <div class="col-md-3 t-item">

                                    <p>INCLUDED IN THIS PRICE</p>

                                </div>

                                <div class="col-md-3 t-item">

                                    <p>PRICE</p>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-3" style="width: 189px;">

                                    <div class="row hotel-main">



                                        <div class="col-md-12 t-item-i cstm-t">

                                            <p>SEASON {{($rate_allocation_count+1)}} DETAILS</p>

                                            <img src="https://r1imghtlak.mmtcdn.com/q829bt8hvd1s7347f2fuogk1002n.jpg?&output-quality=75&downsize=520:350&output-format=jpg"

                                            class="t-img">

                                            <ul class="ul-li">

                                                <li>SEASON NAME:

                                                 @if($hotel_season_details[$rate_allocation_count]['season_name']!="" && $hotel_season_details[$rate_allocation_count]['season_name']!=null)

                                                {{$hotel_season_details[$rate_allocation_count]['season_name']}}

                                                 @else No Data Available @endif</li>

                                                <li>BOOKING VALIDITY : <br>

                                                 @if($hotel_season_details[$rate_allocation_count]['booking_validity_from']!="" && $hotel_season_details[$rate_allocation_count]['booking_validity_from']!=null)

                                                                        @php

                                                                        echo date('d-m-Y',strtotime($hotel_season_details[$rate_allocation_count]['booking_validity_from']));

                                                                        @endphp

                                                                        @else No Data Available

                                                                        @endif 

                                                                        To



                                                                        @if($hotel_season_details[$rate_allocation_count]['booking_validity_to'] && $hotel_season_details[$rate_allocation_count]['booking_validity_to']!=null)

                                                                        @php

                                                                        echo date('d-m-Y',strtotime($hotel_season_details[$rate_allocation_count]['booking_validity_to']));

                                                                        @endphp

                                                                        @else No Data Available

                                                                        @endif </li>

                                                <li>Stop Sale : <br> 

                                                 @if($hotel_season_details[$rate_allocation_count]['stop_sale']!="" && $hotel_season_details[$rate_allocation_count]['stop_sale']!=null)

                                                                        @php

                                                                        echo date('d-m-Y',strtotime($hotel_season_details[$rate_allocation_count]['stop_sale']));

                                                                        @endphp

                                                                        @else No Data Available

                                                                        @endif </li>

                                                <!-- <li>Electric Kettle</li>

                                                <li>Iron/Ironing Board</li>

                                                <li>Safe</li> -->

                                            </ul>

                                            <a href="#" class="more-info">MORE ABOUT THIS ROOM</a>



                                        </div>

                                    </div>

                                </div>

                                <div class="col-md-9" style="width: 786px;">

                                    <div class="row">

                                      

                                            

                                            <? $markup= unserialize($get_hotels->hotel_markup_details); 

                                           

                                            for($nation_count=0;$nation_count< count($hotel_markup_details[$rate_allocation_count]['activity_nationality']);$nation_count++)

                                                {

                                                    if($countrycode==$hotel_markup_details[$rate_allocation_count]['activity_nationality'][$nation_count])

                                                    {



                                                        $markupprice=$hotel_markup_details[$rate_allocation_count]['activity_amount'][$nation_count];

                                                        $marktype=$hotel_markup_details[$rate_allocation_count]['activity_markup'][$nation_count];

                                                    }

                                                   

                                                }

                                            ?>

                                            

                                            @php

                                                for($room_count=0;$room_count< count($rate_allocation_details[$rate_allocation_count]['room_type']);$room_count++)

                                                {

                                            @endphp

                                        <div class="col-md-12">

                                            <div class="row">



                                                        <div class=" col-md-4  t-item-i">

                                                            <p class="option">Rooms : 

                                                                @if($rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]!="null")

                                                            {{$rate_allocation_details[$rate_allocation_count]['room_type'][$room_count]}}

                                                            @else

                                                            No Data Available

                                                            @endif 

                                                            <br><span

                                                                class="c-badge">Recommended</span></p>

                                                                <ul class="ul-li">

                                                                    <li>Min : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_min'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_min'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_min'][$room_count]}}

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                    </li>

                                                                    <li> Max : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_max'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_max'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_max'][$room_count]}}

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                     </li>

                                                                      <li> Room Class : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_class'][$room_count]}}

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                     </li>

                                                                      <li> Currency : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count]!="null")

                                                                        @foreach($currency as $curr)

                                                                        @if($curr->code==$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count])

                                                                        {{$curr->code}} ({{$curr->name}})

                                                                        @endif

                                                                        @endforeach

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                     </li>

                                                                     <li> Adults : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count]}}



                                                                        @else

                                                                        No Data Available

                                                                        @endif



                                                                     </li>

                                                                      <li> CWB : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_cwb'][$room_count]}}

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                     </li>

                                                                     <li> CNB : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_cnb'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_cnb'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_cnb'][$room_count]}}

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                     </li>

                                                                     <li> Check in-out : 

                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_checkin'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_checkin'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_checkin'][$room_count]}}

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                        To 



                                                                        @if($rate_allocation_details[$rate_allocation_count]['room_checkout'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_checkout'][$room_count]!="null")

                                                                        {{$rate_allocation_details[$rate_allocation_count]['room_checkout'][$room_count]}}

                                                                        @else

                                                                        No Data Available

                                                                        @endif

                                                                     </li>



                                                                </ul>

                                                     </div>

                                                    <div class="col-md-4 t-item-i">

                                                                <ul class="ul-li">

                                                                        <li>Meal : 

                                                                            @if($rate_allocation_details[$rate_allocation_count]['room_meal'][$room_count]!="" && $rate_allocation_details[$rate_allocation_count]['room_meal'][$room_count]!="null")

                                                                            <!-- {{$rate_allocation_details[$rate_allocation_count]['room_meal'][$room_count]}} -->

                                                                            @foreach($hotel_meal as $meals)

                                                                            @if($rate_allocation_details[$rate_allocation_count]['room_meal'][$room_count]==$meals->hotel_meals_id)

                                                                            {{$meals->hotel_meals_name}}

                                                                            @endif

                                                                            @endforeach

                                                                            @else

                                                                            No Data Available

                                                                            @endif

                                                                        </li>

                                                                               



                                                                </ul>

                                                    </div>

                                                    <div class="col-md-4 t-item-i">

                                                       <!--  <p class="tooltip-c">Deal Applied: <b>ADVANCEOFFD2</b>. You

                                                            Get

                                                        Flat INR 2890.0 OFF!</p>



                                                        <p class="cut-price">INR 5,480</p> -->

                                                        <p class="offer">{{$rate_allocation_details[$rate_allocation_count]['room_currency'][$room_count]}} 

                                                            <?php

                                                            if($marktype=='Markup Percentage')

                                                            {

                                                                $p_price=($rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count] *  $markupprice) / 100;

                                                                $totalprice=$rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count] +  $p_price;

                                                            }

                                                            else

                                                            {

                                                                $totalprice = $markupprice + $rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count];

                                                            }

                                                            ?>



                                                            {{$totalprice}}

                                                        <!-- {{$markup[$rate_allocation_count]['activity_amount'][$room_count]}} -- {{$markup[$rate_allocation_count]['activity_nationality'][$room_count]}}-- {{$rate_allocation_details[$rate_allocation_count]['room_adult'][$room_count]}} --{{$markup[$rate_allocation_count]['activity_markup'][$room_count]}}  -->

                                                    </p>

                                                        <p class="days">Per night</p>

                                                        <p class="use-code">Code Used ADVANCEOFFD2</p>

                                                        <button class="book-btn">Book This Now</button>

                                                    </div>

                                                </div>

                                        </div>

                                        @php } @endphp

                                           

                                                    </div>

                            </div>



                                            </div>

                                        </div>

                     </div>





                                </div>

                            </div>

                        </div>

                    </div>

                

           

                    @php

                     }

                   @endphp



               

        </div>









    </div>

</div>

</div>



</div>



</div>



</div>







@include('agent.includes.footer')



@include('agent.includes.bottom-footer')



<aside class="control-sidebar">



    <div class="rpanel-title"><span class="pull-right btn btn-circle btn-danger"><i class="ion ion-close text-white"

        data-toggle="control-sidebar"></i></span> </div>

        <!-- Create the tabs -->

        <ul class="nav nav-tabs control-sidebar-tabs">

            <li class="nav-item"><a href="#control-sidebar-home-tab" data-toggle="tab" title="Notifications"><i

                class="ti-comment-alt"></i></a></li>

                <li class="nav-item"><a href="#control-sidebar-settings-tab" data-toggle="tab" title="Comments"><i

                    class="ti-tag"></i></a></li>

                </ul>

                <!-- Tab panes -->

                <div class="tab-content">

                    <!-- Home tab content -->

                    <div class="tab-pane" id="control-sidebar-home-tab">

                        <div class="lookup lookup-sm lookup-right d-none d-lg-block mb-10">

                            <input type="text" name="s" placeholder="Search" class="w-p100">

                        </div>

                        <div class="media-list media-list-hover">

                            <a class="media media-single" href="#">

                                <h4 class="w-50 text-gray font-weight-500">10:10</h4>

                                <div class="media-body pl-15 bl-5 rounded border-primary">

                                    <p>Morbi quis ex eu arcu auctor sagittis.</p>

                                    <span class="text-fade">by Johne</span>

                                </div>

                            </a>



                            <a class="media media-single" href="#">

                                <h4 class="w-50 text-gray font-weight-500">08:40</h4>

                                <div class="media-body pl-15 bl-5 rounded border-success">

                                    <p>Proin iaculis eros non odio ornare efficitur.</p>

                                    <span class="text-fade">by Amla</span>

                                </div>

                            </a>



                            <a class="media media-single" href="#">

                                <h4 class="w-50 text-gray font-weight-500">07:10</h4>

                                <div class="media-body pl-15 bl-5 rounded border-info">

                                    <p>In mattis mi ut posuere consectetur.</p>

                                    <span class="text-fade">by Josef</span>

                                </div>

                            </a>



                            <a class="media media-single" href="#">

                                <h4 class="w-50 text-gray font-weight-500">01:15</h4>

                                <div class="media-body pl-15 bl-5 rounded border-danger">

                                    <p>Morbi quis ex eu arcu auctor sagittis.</p>

                                    <span class="text-fade">by Rima</span>

                                </div>

                            </a>



                            <a class="media media-single" href="#">

                                <h4 class="w-50 text-gray font-weight-500">23:12</h4>

                                <div class="media-body pl-15 bl-5 rounded border-warning">

                                    <p>Morbi quis ex eu arcu auctor sagittis.</p>

                                    <span class="text-fade">by Alaxa</span>

                                </div>

                            </a>



                        </div>

                    </div>

                    <!-- /.tab-pane -->

                    <!-- Settings tab content -->

                    <div class="tab-pane" id="control-sidebar-settings-tab">

                        <div class="media-list media-list-hover media-list-divided">

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>

                                    <p>Cras tempor diam nec metus...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (22)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>

                                    <p>Praesent tristique diam...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (23)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>

                                    <p>Cras tempor diam nec...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (22)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>

                                    <p>Praesent tristique diam...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (23)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>

                                    <p>Cras tempor diam nec metus...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (22)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>

                                    <p>Praesent tristique diam...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (23)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Carter</strong> <span class="float-right">33 min ago</span></p>

                                    <p>Cras tempor diam nec...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (22)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                            <div class="media">

                                <img class="avatar avatar-lg" src="" alt="...">



                                <div class="media-body">

                                    <p><strong>Nicholas</strong> <span class="float-right">11 hour ago</span></p>

                                    <p>Praesent tristique diam...</p>

                                    <div class="media-block-actions">

                                        <nav class="nav nav-dot-separated no-gutters">

                                            <div class="nav-item">

                                                <a class="nav-link text-success" href="#"><i class="fa fa-thumbs-up"></i>

                                                (17)</a>

                                            </div>

                                            <div class="nav-item">

                                                <a class="nav-link text-danger" href="#"><i class="fa fa-thumbs-down"></i>

                                                (23)</a>

                                            </div>

                                        </nav>



                                        <nav class="nav no-gutters gap-2 font-size-16 media-hover-show">

                                            <a class="nav-link text-success" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Approve"><i class="ion-checkmark"></i></a>

                                            <a class="nav-link text-danger" href="#" data-toggle="tooltip" title=""

                                            data-original-title="Delete"><i class="ion-close"></i></a>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                </div>

            </aside>



            <div class="control-sidebar-bg"></div>

        </div>