@include('agent.includes.top-header')
<style>
    div.overlay {
        position: absolute;
        width: 100%;
        bottom: 0;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #5d4ecbbd !important;
        border-top-right-radius: 0 !important;
    }

    div.overlay-2 {
        position: absolute;
        width: 100%;
        top: 0;

        opacity: 0;
        bottom: 0;
        z-index: 99;
        transition: .5s ease;
        border-radius: 5px;
        border-top-left-radius: 0 !important;
        left: 0;
        background: #000000bd !important;
        border-top-right-radius: 0 !important;
    }

    .img-div:hover div.overlay-2 {
        transition: .5s ease;

        opacity: 1;
    }

    .img-div:hover .ribbon2 {
        display: none;
    }

    .img-div:hover::before,
    .img-div:hover::after {
        border: none
    }

    .left {
        float: left;
        width: 50%;
    }

    p.from {
        color: white;
        text-align: right;
        padding: 5px 10px 0;
        font-size: 18px;
        margin: 0;
    }

    span.price-span {
        display: block;
        color: white;
        text-align: right;
        padding: 0 10px;
    }

    img.h-img {
        width: 100%;
    }

    p.h-head {
        color: white;
        font-size: 15px;
        padding: 5px 10px;
        margin: 0;
        z-index: 9999;
        position: absolute;
        top: 0;
        left: 0;
        color: white;
        background: #E91E63;
        width: 100%;
    }

    span.s-span {
        color: white;
        padding: 0 10px;
    }

    .img-div {
        border: 1px solid #756fcc;
        padding: 2px;
        border-radius: 5px;
        position: relative;
        z-index: 9;
        margin-bottom: 42px;
        transition: .5s ease;
    }

    /*  .img-div:before {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c70 transparent transparent;
        top: 56px;
        left: -14px;
        
        z-index: 0;
    }

    .img-div:after {
        content: "";
        position: absolute;
        width: 9px;
        height: 10px;
        border-width: 7px;
        border-style: solid;
        border-color: transparent #402c7000 #402c70 transparent;
        top: -14px;
        left: 58px;
        
        z-index: -37;
        } */

    .ribbon2:before {
        content: "";
        position: absolute;
        top: 0;
        padding: 10px;
        /* margin: 5px; */
        left: 0;
        width: 100px;
        height: 20px;
        background: #5225c5;
        transform: rotate(135deg) translate(28px, 1px);
    }

    .ribbon2 {
        position: absolute;
        top: -7px;
        left: -7px;
        width: 100px;
        height: 100px;
        /* position: absolute; */
        overflow: hidden;
        /* background: aliceblue; */
    }

    button.hover-btn {
    position: relative;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border: none;
    background: #ffffff;
    color: #FF5722;
    border: 0.5px solid #ff5b4f;
    padding: 7px 15px;
    cursor: pointer;
    border-radius: 18px;
    z-index: 1;
}

    @media screen and (max-width:1170px) {
        p.h-head {
            color: white;
            font-size: 15px;
            padding: 0 10px;
            margin: 0;
        }

        p.from {
            color: white;
            text-align: right;
            padding: 0px 10px 0;
            font-size: 15px;
            margin: 0;
        }

        span.s-span {
            color: white;
            padding: 0 10px;
            font-size: 11px;
        }

        span.price-span {
            display: block;
            color: white;
            text-align: right;
            padding: 0 10px;
            font-size: 12px;
        }
    }

    .table-restaurant-loader svg {
        width: 100px;
        height: 100px;
        display: inline-block;
    }

    .carousel-item img {

        height: 100%;

    }



    .carousel-item {

        height: 100%;

    }



    p.start_price {

        margin: 0;

    }



    p.country_name.ng-binding {

        font-size: 20px;

        margin: 0;

    }



    .book_card {

        /* padding: 15px; */

        background: #fefeff;

        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);

        margin-bottom: 50px;



        border-radius: 5px;

    }



    .hotel_detail {

        height: 150px;

    }



    a.moredetail.ng-scope {

        background: gainsboro;

        padding: 7px 10px;

        /* margin-bottom: 10px; */

    }



    .booking_label {

        background: #5d53ce;

        border-top-left-radius: 5px;

        border-top-right-radius: 5px;

        color: white;

        padding: 10px 15px;

    }



    .booking_detail {

        padding: 15px;

    }



    a.btn.btn-outline.btn-circle.book_btn1 {

        background: #E91E63;

        border-radius: 5px !IMPORTANT;

        padding: 5px 20px;

        width: auto !important;

        height: auto;

        line-height: 2;

        color: white;

    }



    td p {

        margin: 0;

    }



    td {

        background: gainsboro;

    }



    table {

        border-collapse: separate;

    }



    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: 1px solid #59d25a;

    }



    a.panel-title {

        position: relative !important;

        background: #dfffe3;

        color: green !important;

        padding: 13px 20px 13px 85px !important;

        /* border-bottom: 1px solid #3ca23d; */

    }

    .panel-title {

        display: block;

        margin-top: 0;

        margin-bottom: 0;

        padding: 1.25rem;

        font-size: 18px;

        color: #4d4d4d;

        height: 48px;

    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {

        border-top: none !important;

    }



    .panel-body {

        background: white;

        /* border: 1px solid #59d25a; */

        padding: 10px !important;

    }



    a.panel-title:before {

        content: attr(title) !important;

        position: absolute !important;

        top: 0px !important;

        opacity: 1 !important;

        left: 0 !important;

        padding: 12px 10px;

        width: auto;

        max-width: 250px;

        text-align: center;

        color: white;

        font-family: inherit !important;

        height: 48px;

        background: #279628;

        z-index: 999;

        transform: none !important;

    }



    .tab-content {

        margin-top: 10px;

    }



    div.panel-heading {

        border: 1px solid #59d25a !important;

    }



    .panel {

        border-top: none !important;

        margin-bottom: 5px !important;

    }

    div#carousel-example-generic-captions {

        width: 100%;

    }

    /*daman css*/

    .hotel-div {

        width: 75%;

        float: left;

        padding: 15px;

        display: flex;

        background: white;

    }



    .hotel-list-div {
        clear: both;
        display: block;
        border: 1px solid #c6bee6;
        border-radius: 5px;
        position: relative;
        /* clear: both; */
        height: 250px;
    }

    .hotel-img-div {



        width: 40% !important;

        float: left;

    }



    .hotel-details {

        float: left;

        width: 60%;

        padding: 0 15px;

    }



    .hotel-info-div {
        width: 25%;
        float: left;
        padding: 15px;
        background: #e6e0ff;
        text-align: right;
        height: 100%;
    }



    .checked {

        color: orange;

    }



    span.hotel-s {

        border: 1px solid #F44336;

        padding: 2px 5px;

        display: inline-block;

        color: #f44336;

        font-size: 12px;

        margin: 0 10px 0 0;

    }



    p.hotel-name {

        font-size: 22px;

        margin: 10px 0 0;

        color: black;

        float: left;

    }



    .rate-no {

        float: right;

    }



    .rate-no {

        float: right;

        display: block;

        margin-top: 14px;

        background: #2d3134;

        color: white;

        padding: 1px 8px;

        font-size: 12px;

        border-radius: 5px;

    }



    .heading-div {

        clear: both;

    }



    .rating {

        display: block;

        float: left;

        list-style: none;

        margin: 0;

        padding: 0;

    }



    p.r-number {

        float: right;

    }



    p.time-info {

        color: #4CAF50;

    }



    p.info {

        clear: both;

        margin: 0;

    }



    span.tag-item {

        background: #ffcbcd;

        padding: 5px 10px;

        border-radius: 5px;

        color: #ff4e54;

    }



    .inclusions {

        margin: 20px 0;

    }



    span.inclusion-item {

        padding: 10px 10px 0 0;

        color: #644ac9;

    }



    img.icon-i {

        width: auto;

        height: 20px;

    }



    p.include-p {

        color: black;

    }



    .inclusion-p {

        color: green
    }



    p.price-p span {

        background: #ee2128;

        color: white;

        padding: 3px 5px 3px 9px;

        border-radius: 5px;

        position: relative;

        z-index: 9999;

        border-top-right-radius: 5px;

        border-bottom-right-radius: 5px;

        margin-left: 20px;

        border-bottom-left-radius: 4px;

        font-size: 12px;

    }



    p.price-p span:before {

        content: "";

        width: 15px;

        height: 14.5px;

        background: #ee2128;

        position: absolute;

        transform: rotate(45deg);

        top: 3.5px;

        left: -6px;

        border-radius: 0px 0px 0px 3px;

        z-index: -1;

    }



    p.price-p span:after {

        content: "";

        background: white;

        width: 4px;

        height: 4px;

        position: absolute;

        top: 50%;

        left: 0px;

        transform: translateY(-50%);

        border-radius: 50%;

    }



    p.price-p {

        color: #ee2128;

    }



    p.tax {

        font-size: 12px;

        margin: 0;

    }



    p.days {

        font-size: 12px;

        margin: 0;

    }



    p.offer {

        font-size: 25px;

        color: black;

        font-weight: bold;

        margin: 0;

    }



    p.cut-price {

        margin: 0;

        text-decoration: line-through;

        font-size: 19px;

    }



    .login-a {

        color: #0088ff;

        font-size: 15px;

        font-weight: bold;

        margin-top: 10px;

        display: block;

    }

    @media screen and (max-width:1200px) {

        p.hotel-name {

            font-size: 17px;

            margin: 10px 0 0;

            color: black;

            float: left;

        }

        p.r-number {

            float: right;

            font-size: 12px;

        }

        span.inclusion-item {

            padding: 10px 10px 0 0;

            color: #644ac9;

            display: block;

        }

        p.cut-price {

            margin: 0;

            text-decoration: line-through;

            font-size: 16px;

        }

        p.offer {

            font-size: 20px;

            color: black;

            font-weight: bold;

            margin: 0;

        }

        .login-a {

            color: #0088ff;

            font-size: 13px;

            font-weight: bold;

            margin-top: 10px;

            display: block;

        }

    }

    @media screen and (max-width:1200px) {

        .hotel-div {

            width: 100%;

            float: left;

            padding: 15px;

            display: flex;

            background: white;

        }

        .hotel-info-div {

            width: 100%;

            height: 100%;

            float: left;

            padding: 15px;

            background: #e6e0ff;

            text-align: left;

        }

        span.inclusion-item {

            padding: 10px 10px 0 0;

            color: #644ac9;

            display: inline;

        }

    }

    @media screen and (max-width:992px) {

        p.hotel-name {

            font-size: 17px;

            margin: 10px 0 0;

            color: black;

            float: none;

        }

        .rate-no {

            float: none;

            display: inline;

            margin-top: 14px;

            background: #2d3134;

            color: white;

            padding: 1px 8px;

            font-size: 12px;

            border-radius: 5px;

        }

        .rating {

            display: block;

            float: none;

            list-style: none;

            margin: 0;

            padding: 0;

        }

        p.r-number {

            float: none;

            font-size: 12px;

        }

        .hotel-details {

            float: none !important;

            width: 100% !important;

            padding: 0 15px;

            margin-top: 20px;

        }

        .hotel-list-div {

            display: block;

            border: 1px solid #c6bee6;

            border-radius: 5px;

            position: relative;

            /* clear: both; */

            height: 101%;

        }

        .hotel-div {

            width: 100%;

            float: none;

            padding: 15px;

            display: block;

            background: white;

        }

        .hotel-img-div {

            width: 100% !important;

            float: none !important;

            display: block !important;

        }

        a.flex-prev,
        a.flex-next {

            display: none;

        }

        span.inclusion-item {

            padding: 10px 10px 0 0;

            color: #644ac9;

            display: block;

        }

    }

    .img-slide {

        height: 200px;

        width: 100%;

    }

    .flex-control-thumbs {

        margin: 5px 0 0;

        position: static;

        overflow: hidden;

        width: 100%;

        height: 50px;
    }

    .form-group label {
        color: #673AB7;
    }
</style>
<style>
    .selection-div {
        border: 1px solid gainsboro;
        padding: 0;
        border-radius: 5px;
        margin-bottom: 33px !IMPORTANT;
        background: url(https://sf2.mariefranceasia.com/wp-content/uploads/sites/7/2018/02/bawah-615x410.jpg);
        background-repeat: no-repeat;
        background-size: cover;
        background-position: center;
    }

    .box-body {
        padding: 1.5rem 1.5rem;
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        border-radius: 5px;
        background-color: #fff;
       
    }

   .selection-div >.row.mb-10 {
    position: relative;
    background: #2727e07a;
    top: 0;
    left: 14px;
    width: 100%;
    padding-bottom: 26px;
    padding-top: 26px;
    height: 100%;
    border-radius: 5px;
    margin-bottom: 0 !important;
}

    .select2-container--default .select2-selection--single {
        border: 1px solid #ddd;
        border-radius: 22px;
        /* padding: 6px 12px;*/
        /*height: 34px;*/
        color: white !important;
        background: #dcdada7d !important;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        color: #fff;
        line-height: 28px;
    }

    .form-group label {
        color: #ffffff !important;
    }

    .input-group.date input {
        background: #dcdada7d !important;
        border-color: white;
        border-width: 1px;
        color: white;
        border-radius: 25px;
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
        padding-left: 10px;
    }

    .input-group.date .input-group-addon {
        border: 2px solid white;
        border-top-right-radius: 25px;
        border-bottom-right-radius: 25px;
    }

    .selection-div button,.selection-div button:hover,.selection-div button:focus {
        background: #f44336 !important;
        /* border: 2px solid red !important;*/
        /* color: red !important; */
        min-width: 120px;
        padding: 5px !important;
        margin-top: 2px;
    }

    button#search_activity {
    border: none;
    padding: 10px !important;
}
div#activity_div {
    width: 100%;
    padding: 20px;
}


.note {
  margin-top: 30px;
  color: #fff;
  font-size: 1rem;
  font-family: "Merriweather", sans-serif;
  line-height: 1.5;
  text-align: center;
}
article.card {
    position: relative;
    transition: all .5s ease;
    left: 50%;
    top: 0;
    left: 0;
    width: 100%;
    height: 280px;
    transform: none;
    border-radius: 3px;
    box-shadow: 4px 5px 20px 0px rgba(0, 0, 0, 0.3);
    overflow: hidden;
}
article.card .thumb {
  width: auto;
  height: 190px;
  background-size: cover;
  border-radius: 3px;
}
article.card .infos {
  width: auto;
  height: 130px;
  position: relative;
  padding: 14px 24px;
  background: #fff;
  -webkit-transition: 0.4s 0.15s cubic-bezier(0.17, 0.67, 0.5, 1.03);
  transition: 0.4s 0.15s cubic-bezier(0.17, 0.67, 0.5, 1.03);
}
article.card .infos .title {
    position: relative;
    margin: 5px 0;
    letter-spacing: 3px;
    color: #673AB7;
    /* font-family: "Grotesque Black", sans-serif; */
    font-size: 13px;
    text-transform: uppercase;
    text-shadow: 0 0 0px #000000;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 200px;
    letter-spacing: 1px;
}
article.card .infos .flag {
  position: absolute;
  top: 50%;
  right: 0;
  -webkit-transform: translateY(-50%);
          transform: translateY(-50%);
  width: 35px;
  height: 23px;
  background: url("https://s3-us-west-2.amazonaws.com/s.cdpn.io/397014/flag.png") no-repeat top right;
  background-size: 100% auto;
  display: inline-block;
}
article.card .infos .date, article.card .infos .seats {
  margin-bottom: 10px;
  text-transform: uppercase;
  font-size: .85rem;
  color: rgba(21, 37, 54, 0.7);
  font-family: "Grotesque", sans-serif;
}
.main-info {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
}
article.card .infos .seats {
    display: inline-block;
    margin-bottom: 0;
    padding: 8px 6px;
    margin-top: 0;
    color: #FF9800;
    font-weight: bolder;
    font-size: 13px;
    background: black;
    /* border: 1px dashed #FF5722 !important; */
    border-bottom: 1px dashed rgba(0, 0, 0, 0.2);
    opacity: 1;
    -webkit-transition: 0.5s 0.25s cubic-bezier(0.17, 0.67, 0.5, 1.03);
    transition: 0.5s 0.25s cubic-bezier(0.17, 0.67, 0.5, 1.03);
}
*,
*:after,
*:before {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
            
.timeline-2 {
  width: 100%;
     height: 45px;
  max-width: 800px;
  margin: 0 auto;
  display: flex;      
  justify-content: center;    
}

.timeline-2 .events {
    position: relative;
    background-color: #dddddd;
    height: 2px;
    width: 100%;
    border-radius: 3px;
    margin: 30px 0;
}

.timeline-2 .events ol {
  margin: 0;
  padding: 0;
  text-align: center;
}

.timeline-2 .events ul li {
    /* display: inline-block; */
    width: 30px;
    margin: 0;
    height: 30px;
    border-radius: 50%;
    background: white;
    position: relative;
    padding: 0;
    top: 50%;
    transform: translateY(-50%);
}
.timeline-2 .events ul li a:first-child {
    font-family: 'Arapey', sans-serif;
    font-style: italic;
    font-size: 17px;
    text-align: center;
    background: #FF9800;
    width: 30px;
    display: block;
    padding: 2px;
    height: 30px;
    border: 2px solid #ffd290;
    border-radius: 50px;
    background-clip: content-box;
    color: #ffffff;
    text-decoration: none;
    vertical-align: middle;
}
a.tooltip-time {
    position: absolute;
    min-width: 90px;
    width: auto;
    padding: 3px 6px;
    text-align: center;
    opacity: 0;
    display: none;
    transition: all .5s ease;
    background: #eeeeee;
    color: black;
    text-decoration: none;
    top: -42px;
    left: 149%;
    font-size: 12px;
    transform: translateX(-50%);
}
article.card:hover .timeline-2 .events ul li a.tooltip-time{
    opacity: 1;
    display: block;
}
a.tooltip-time.bottom-time {
    top: 40px;
    left: -70%;
}
a.tooltip-time:before {
    content: "";
    position: absolute;
    top: 100%;
    left: 15%;
    width: 2px;
    transform: translateX(-50%);
    height: 12px;
    background: #ffd290;
}
a.tooltip-time.bottom-time:before {
    content: "";
    position: absolute;
    top: -10px;
    left: 88%;
    width: 2px;
    transform: translateX(-50%);
    height: 11px;
    background: #ffd290;
}
.timeline-2 .events ul {
    list-style: none;
    display: flex;
    justify-content: space-around;
}

.timeline-2 .events ul li a:hover::after {
  background-color: #194693;
  border-color: #194693;
}

.timeline-2 .events ul li a.selected:after {
  background-color: #194693;
  border-color: #194693;
}
            
.events-content {
  width: 100%;
  height: 100px;
  max-width: 800px;
  margin: 0 auto;
  display: flex;      
  justify-content: left;
}

.events-content li {
  display: none;
  list-style: none;
}

.events-content li.selected {
  display: initial;
}

.events-content li h2 {
  font-family: 'Frank Ruhl Libre', serif;
  font-weight: 500;
  color: #919191;
  font-size: 2.5em;
}
article.card.sold {
    
    cursor: not-allowed;
    pointer-events: none;
    -webkit-appearance: none;
}
.sold-para {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    width: 100%;
    height: 100%;
    text-align: center;
    color: #FF5722;
    font-size: 30px;
    padding: 110px 0;
    z-index: 9999;
    opacity: 1;
    pointer-events: none;
    background: #ffffff94;
}
article.card .infos .date {
    color: #795548;
    font-size: 13px;
    text-transform: capitalize;
    margin-bottom: 33px;
    margin-top: 0;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    width: 220px;
}
p.hotel-type {
    color: #ffffff;
    position: absolute;
    background: black;
    top: -31px;
    left: 0;
    padding: 5px 10px;
}
article.card .infos .txt {
  font-family: "Merriweather", sans-serif;
  line-height: 2;
  font-size: .95rem;
  color: rgba(21, 37, 54, 0.7);
  opacity: 0;
  -webkit-transition: 0.5s 0.25s cubic-bezier(0.17, 0.67, 0.5, 1.03);
  transition: 0.5s 0.25s cubic-bezier(0.17, 0.67, 0.5, 1.03);
}
article.card .infos .details {
  position: absolute;
  left: 0;
  left: 0;
  bottom: 0;
  margin: 10px 0;
  padding: 20px 24px;
  letter-spacing: 1px;
  color: #4e958b;
  font-family: "Grotesque Black", sans-serif;
  font-size: .9rem;
  text-transform: uppercase;
  cursor: pointer;
  opacity: 0;
  -webkit-transition: 0.5s 0.25s cubic-bezier(0.17, 0.67, 0.5, 1.03);
  transition: 0.5s 0.25s cubic-bezier(0.17, 0.67, 0.5, 1.03);
}
.no-of-booking {
    position: absolute;
    top: 10px;
    right: 10px;
    background: #ffefd6;
    color: #ff9800;
    min-width: 95px;
    padding: 5px 10px;
    border-radius: 50px;
    display: flex;
    justify-content: space-between;
    align-items: center;
}

article.card:hover .infos {
    -webkit-transform: translateY(-145px);
    transform: translateY(-145px);
    height: 100%;
}
article.card:hover .infos .seats, article.card:hover .infos .txt, article.card:hover .infos .details {
  opacity: 1;
}
@media screen and (max-width:768px){
    .selection-div>.row.mb-10 {
    position: relative;
    background: #2727e07a;
    top: 0;
    left: 14px;
    width: 100%;
    height: 100%;
    bottom: 0;
    padding-top: 26px;
    padding-bottom: 26px;
    padding: 26px 50px;
    border-radius: 5px;
    margin-bottom: 0 !important;
}
button#search_activity {
    display: block;
    margin: auto;
}
.theme-rosegold .btn-success:hover, .theme-rosegold .btn-success:active, .theme-rosegold .btn-success:focus, .theme-rosegold .btn-success.active{
    background-color: #F44336 !important;
    border-color: #F44336 !important;
    color: #fff;
}
</style>

    <body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

        <div class="wrapper">

            @include('agent.includes.top-nav')

            <div class="content-wrapper">

                <div class="container-full clearfix position-relative">	

                    @include('agent.includes.nav')

                    <div class="content">



                        <div class="content-header">

                            <div class="d-flex align-items-center">

                                <div class="mr-auto">

                                    <h3 class="page-title">Restaurants</h3>

                                    <div class="d-inline-block align-items-center">

                                        <nav>

                                            <ol class="breadcrumb">

                                                <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                                                <li class="breadcrumb-item active" aria-current="page">Home

                                                </li>

                                            </ol>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                        </div>


    <div class="row">
      <div class="box">
          <div class="box-body">

             <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                            <div class="selection-div">

                         <div class="row mb-10">
                               <div class="col-sm-12 col-md-12 col-lg-6">

                                    <div class="form-group">
                                        <label for="restaurant_type">Restaurant Type</label>
                                        <select id="restaurant_type" name="restaurant_type" class="form-control select2" style="width: 100%;">
                                             <option value="0">ANY RESTAURANT TYPE</option>
                                                @foreach($fetch_restaurant_type as $restaurant_type)
                                                <option value="{{$restaurant_type->restaurant_type_id}}">{{$restaurant_type->restaurant_type_name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>

                            <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="restaurant_country">COUNTRY <span class="asterisk">*</span></label>
                                    <select id="restaurant_country" name="restaurant_country" class="form-control select2" style="width: 100%;">
                                        <option selected="selected">SELECT COUNTRY</option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <label for="restaurant_city">CITY <span class="asterisk">*</span></label>
                                    <select id="restaurant_city" name="restaurant_city" class="form-control select2" style="width: 100%;">
                                        <option selected="selected">SELECT CITY</option>
                                    </select>
                                </div>
                            </div>
                         
                            <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <label for="select_date">DATE<span class="asterisk">*</span></label>
                                    <div class="input-group date">
                                        <input type="text" placeholder="DATE"
                                        class="form-control pull-right datepicker" id="select_date" name="select_date" readonly="readonly">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->

                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <label for="select_time">Time<span class="asterisk">*</span></label>
                                    <div class="input-group date clockpicker">
                                       <input type="text" placeholder="TIME"
                                       class="form-control pull-right clockpicker" id="select_time" name="select_time" readonly="readonly">
                                       <div class="input-group-addon">
                                        <i class="fa fa-clock-o"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->

                            </div>
                        </div>

                            
                         <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <br>
                                     <input type="hidden" id="offset" value="0">
                                    <button id="search_restaurant" class="btn btn-rounded  btn-success">Search</button>

                                </div>
                                <!-- /.input group -->

                            </div>
                        </div>
                    </div>

                    </div>

                    <div id="restaurant_div">

                    </div>
                    <div class="text-center table-restaurant-loader" style="display: none">
                      <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                      viewBox="0 0 100 100" enable-background="new 0 0 0 0" xml:space="preserve">
                      <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 15 ; 0 -15; 0 15" 
                        repeatCount="indefinite" 
                        begin="0.1"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 10 ; 0 -10; 0 10" 
                        repeatCount="indefinite" 
                        begin="0.2"/>
                    </circle>
                    <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6">
                        <animateTransform 
                        attributeName="transform" 
                        dur="1s" 
                        type="translate" 
                        values="0 5 ; 0 -5; 0 5" 
                        repeatCount="indefinite" 
                        begin="0.3"/>
                    </circle>
                </svg>
            </div>
            <button type="button" class="btn btn-primary more_restaurant" id="show_more_restaurant" style=" margin: 5% 0%;display: none !important;">Show More Results</button>
            <button type="button" class="btn btn-primary more_restaurant" id="no_more_restaurant"  style=" margin: 5% 0%; display: none !important;" disabled="disabled">No More Results</button>
    </div>
</div>
</div>
</div>





</div>
</div>
</div>

</div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
<script>
    $(document).ready(function()
    {
        $('.select2').select2();
        var date = new Date();
        date.setDate(date.getDate());
        $('#select_date').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        });
        $('.clockpicker').clockpicker({
    'default': 'now',
    vibrate: true,
    placement: "top",
    align: "left",
    autoclose: false,
    twelvehour: true
  });

        $(document).on("change","#restaurant_country",function()
        {
         if($("#restaurant_country").val()!="0")
         {
            var country_id=$(this).val();
            $.ajax({
                url:"{{route('search-country-cities')}}",
                type:"GET",
                data:{"country_id":country_id},
                success:function(response)
                {

                    $("#restaurant_city").html(response);
                    $('#restaurant_city').select2();

                }
            });
        }

    });

    //Activity search filters

    $(document).on("click","#search_restaurant",function()
    {

        $("#show_more_restaurant").hide();
                $("#no_more_restaurant").hide();
                $("#offset").val(0);
        var country_id=$("#restaurant_country").val();
        var city_id=$("#restaurant_city").val();
        var select_date=$("#select_date").val();
        var select_time=$("#select_time").val();
        var offset=$("#offset").val();
          var restaurant_type=$("#restaurant_type").val();
          // var date_to=$("#date_to").val();

           var error=[];
          if(country_id== "SELECT COUNTRY")
                {
                    error.push("Please Select Country");
                }

                if(city_id == "0" || city_id == "SELECT CITY")
                {

                     error.push("Please Select City");

                }

                if(select_date == "")
                {

                     error.push("Please Select Date");
                }
                if(select_time == "")
                {

                     error.push("Please Select Time");
                }
                if(error.length>0)
                {
                   alert(error.join(" , "));  
                   return false;
                }

         if(country_id!="0" && city_id!="0")
          {
            $("#restaurant_div").html("");
            $(".table-restaurant-loader").show();
            $.ajax({
                url:"{{route('fetchRestaurant')}}",
                data:{"_token":"{{csrf_token()}}",
                    "country_id":country_id,
                "city_id":city_id,
                "select_date":select_date,
                 "select_time":select_time,
                  "restaurant_type":restaurant_type,
                        "offset":offset
            },
            type:"POST",
            success:function(response)
            {
                $("#restaurant_div").html(response);
                $(".table-restaurant-loader").hide();
                  $("#offset").val(parseInt(parseInt($("#offset").val())+1));

                if(response.indexOf("img")!=-1)
                {
                   $("#show_more_restaurant").show();
                   $("#no_more_restaurant").hide();

               }
               else
               {
                $("#show_more_restaurant").hide();
                $("#no_more_restaurant").show();

            }
            }

        });
        }
    });


    $(document).on("click","#show_more_restaurant",function()
    {
        var country_id=$("#restaurant_country").val();
        var city_id=$("#restaurant_city").val();
        var select_date=$("#select_date").val();
        var select_time=$("#select_time").val();
        var offset=$("#offset").val();
          var restaurant_type=$("#restaurant_type").val();
          // var date_to=$("#date_to").val();

           var error=[];
          if(country_id== "SELECT COUNTRY")
                {
                    error.push("Please Select Country");
                }

                if(city_id == "0" || city_id == "SELECT CITY")
                {

                     error.push("Please Select City");

                }

                if(select_date == "")
                {

                     error.push("Please Select Date");
                }
                if(select_time == "")
                {

                     error.push("Please Select Time");
                }
                if(error.length>0)
                {
                   alert(error.join(" , "));  
                   return false;
                }

         if(country_id!="0" && city_id!="0")
          {
            $(".table-restaurant-loader").show();
            $.ajax({
                url:"{{route('fetchRestaurant')}}",
                data:{"_token":"{{csrf_token()}}",
                    "country_id":country_id,
                "city_id":city_id,
                "select_date":select_date,
                 "select_time":select_time,
                  "restaurant_type":restaurant_type,
                        "offset":offset
            },
            type:"POST",
            success:function(response)
            {
                $("#restaurant_div").append(response);
                $(".table-restaurant-loader").hide();
                    $("#offset").val(parseInt(parseInt($("#offset").val())+1));

                if(response.indexOf("img")!=-1)
                {
                   $("#show_more_restaurant").show();
                   $("#no_more_restaurant").hide();

               }
               else
               {
                $("#show_more_restaurant").hide();
                $("#no_more_restaurant").show();

            }
            }

        });
        }
    });
});
    
</script>
</body>
</html>