<?php
use App\Http\Controllers\LoginController;
?>
@include('agent.includes.top-header')
<style>
.iti-flag {
width: 20px;
height: 15px;
box-shadow: 0px 0px 1px 0px #888;
background-image: url("{{asset('assets/images/flags.png')}}") !important;
background-repeat: no-repeat;
background-color: #DBDBDB;
background-position: 20px 0
}
div#cke_1_contents {
height: 250px !important;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('agent.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('agent.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Exchange Rates</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Accounting</li>
                                            <li class="breadcrumb-item active" aria-current="page">Exchange Rates
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            <!--  <div class="right-title">
                                <div class="dropdown">
                                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
                                    class="mdi mdi-dots-horizontal"></i></button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
                                        <div class="dropdown-divider"></div>
                                        <button type="button" class="btn btn-rounded btn-success">Submit</button>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">View Exchange Rates</h4>
                                </div>
                                <div class="box-body">
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12">
                                                <table class="table table-bordered">
                                                    <tr>
                                                  <th>
                                                      Sr. No
                                                  </th> 
                                                  <th>
                                                      Currency Name
                                                  </th> 
                                                  <th>
                                                      Exchange Price ( for GEL)
                                                  </th>
                                              </tr>
                                                @foreach($currency_price_array as $key=>$item)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$key}}</td>
                                                    <td>{{$item}}</td>
                                                </tr>

                                                @endforeach

                                                </table>

                                            </div>
                                            <div class="col-sm-12 col-md-12">
                                                <p>Try it :</p>
                                                    <div class="row">
                                                        <div class="col-md-2">

                                                            <div class="form-group">
                                                                <input type="text" id="currency_amount" name="currency_amount" class="form-control" placeholder="Amount" value="" style="border:1px solid #aaa">
                                                            </div>
                                                        </div>
                                                
                                                         <div class="col-md-2">
                                                            <div class="form-group">
                                                                <select name="from_currency" id="from_currency" class="form-control select2">
                                                                   @foreach($currency as $curr)
                                                                   <option value="{{$curr->code}}" @if($curr->code=="USD") selected @endif>{{$curr->code}}</option>
                                                                   @endforeach
                                                                </select>
                                                               
                                                            </div>
                                                        </div>
                                                         <div class="col-md-1">
                                                            <div class="text-center">
                                                                <label>to</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <select name="from_currency" id="to_currency" class="form-control select2">
                                                                    @foreach($currency as $curr)
                                                                   <option value="{{$curr->code}}" @if($curr->code=="GEL") selected @endif>{{$curr->code}}</option>
                                                                   @endforeach
                                                                </select>
                                                               
                                                            </div>
                                                        </div>
                                                        <div class="col-md-1">
                                                            <button type="button" id="convert_btn" class="btn btn-primary" style="line-height: 15px">Convert</button>
                                                            
                                                        </div>
                                                        <div class="col-md-2">
                                                            <input type="text" id="converted_price" class="form-control" readonly="" placeholder="Press Convert button" style="background-color: #eee;font-weight: bold;">
                                                        </div>


                                                    </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    @include('agent.includes.footer')
    @include('agent.includes.bottom-footer')
    <script>
    $(document).ready(function()
    {
        $(".datatable").DataTable();

        $(".select2").select2();
    });

    $(document).on("click","#convert_btn",function()
    {
        var amount=$("#currency_amount").val();
        var from_currency=$("#from_currency").val();
        var to_currency=$("#to_currency").val();
        if(amount.trim()=="")
        {
            alert("Please enter an amount to convert");
        }
        else
        { 

                 setTimeout(function()
               {
                    $("#converted_price").val("Converting..");
                },0);
               setTimeout(function()
               {
                   $("#converted_price").val("Converting....");
                },300);
               setTimeout(function()
               {
                   $("#converted_price").val("Converting.....");
                },600);
                 setTimeout(function()
               {
                    $("#converted_price").val("Converting.......");
                },900);
                   setTimeout(function()
               {
                 $("#converted_price").val("Converting..........");
                        $.ajax({
            url:"{{route('get-exchange-rates')}}",
            data: {'from_currency':from_currency,'to_currency':to_currency,'amount':amount},
            type:"GET",
            success:function(response)
            {
                $("#converted_price").val("Converting..........");
               $("#converted_price").val(response);
        
        
            }
        });    
        },1000);    

                      
              
              

        //        setTimeout(function()
        //        {
        //             $("#cities_div").html("<h4><b>LOADING..</b></h4>");
        //         },0);
        //        setTimeout(function()
        //        {
        //            $("#cities_div").html("<h4><b>LOADING....</b></h4>");
        //         },300);
        //        setTimeout(function()
        //        {
        //            $("#cities_div").html("<h4><b>LOADING.....</b></h4>");
        //         },600);
        //          setTimeout(function()
        //        {
        //             $("#cities_div").html("<h4><b>LOADING.......</b></h4>");
        //         },900);
        //            setTimeout(function()
        //        {
        //          $("#cities_div").html("<h4><b>LOADING..........</b></h4>");
        //                $.ajax({
        //     url:"{{route('get-all-cities-tab')}}",
        //     data: {'country_id':country_id},
        //     type:"GET",
        //     success:function(response)
        //     {
        //         $("#cities_div").html("<h4><b>LOADING..........</b></h4>");
        //         $("#cities_div").html(response);
        //          $(".datatable").DataTable();
        
        //     }
        // });
        //            },1000);
        

        }
      
    });
    </script>


</body>
</html>