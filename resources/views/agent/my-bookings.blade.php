<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('agent.includes.top-header')

<style>
    
    .carousel-item img {
        height: 100%;
    }

    .carousel-item {
        height: 100%;
    }

    p.start_price {
        margin: 0;
    }

    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }

    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;

        border-radius: 5px;
    }

    .hotel_detail {
        height: 150px;
    }

    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }

    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }

    .booking_detail {
        padding: 15px;
    }

    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }

    td p {
        margin: 0;
    }

    td {
        background: gainsboro;
    }

    table {
        border-collapse: separate;
    }

    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #59d25a;
    }

    a.panel-title {
        position: relative !important;
        background: #dfffe3;
        color: green !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #3ca23d; */
    }
.panel-title {
    display: block;
    margin-top: 0;
    margin-bottom: 0;
    padding: 1.25rem;
    font-size: 18px;
    color: #4d4d4d;
    height: 48px;
}
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }

    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }

    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
           width: auto;
    max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #279628;
        z-index: 999;
        transform: none !important;
    }

    .tab-content {
        margin-top: 10px;
    }

    div.panel-heading {
        border: 1px solid #59d25a !important;
    }

    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
div#carousel-example-generic-captions {
    width: 100%;
}
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
 .table tr td, .table tr th {
        white-space: nowrap !important;
        padding: 10px 20px !important;
        background: white !important;
    }
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('agent.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('agent.includes.nav')

	<div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">My Bookings</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Home</li>

                            <li class="breadcrumb-item active" aria-current="page">My Bookings

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

        </div>

    </div>

   <div class="row">
      <div class="box">
          <div class="box-body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-pills mb-20">
                        <li class=" nav-item"> <a href="#navpills-all" id="navpills-all-link" class="nav-link show active" data-toggle="tab"
                                aria-expanded="false">All</a> </li>
                        <li class="nav-item"> <a href="#navpills-confirmed" id="navpills-confirmed-link" class="nav-link show" data-toggle="tab"
                                aria-expanded="false">Confirmed</a> </li>
                                  <li class="nav-item"> <a href="#navpills-pending" id="navpills-pending-link" class="nav-link show" data-toggle="tab"
                                aria-expanded="false">Pending</a> </li>
                                <li class="nav-item"> <a href="#navpills-rejected" id="navpills-rejected-link" class="nav-link show" data-toggle="tab"
                                aria-expanded="false">Rejected</a> </li>
                                  <li class="nav-item"> <a href="#navpills-cancelled" id="navpills-cancelled-link" class="nav-link show" data-toggle="tab"
                                aria-expanded="false">Cancelled</a> </li>
                    </ul>
                    <!-- Tab panes -->

                <div class="tab-content">
                    <div id="navpills-all" class="tab-pane show active">
                        <div class="row">
                         <div class="col-md-12">
                             <div class="table-responsive">
                            <table id="example_all" class="table table-condensed table-striped">
                                <thead>
                                    <tr>
                                        <td>Serial No</td>
                                        <td>Booking ID</td>
                                        <td>Booking Type</td>
                                        <td>Type Name</td>
                                        <td>Name</td>
                                        <td>Email</td>
                                        <td>Mobile</td>
                                        <td>Booking Date</td>
                                        <td>Amount</td>
                                        <td>Status</td>
                                        <td>Remarks</td>
                                        <td>Attachments</td>
                                         <td>Full Payment</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($fetch_bookings as $bookings)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                     <td>{{$bookings->booking_sep_id}}</td>
                                     <td>{{ucwords($bookings->booking_type)}}</td>
                                     <td>@php

                                        $booking_type_id=$bookings->booking_type_id;
                                        if($bookings->booking_type=="activity")
                                        {
                                            $fetch_activity=ServiceManagement::searchActivity($booking_type_id);
                                            echo $fetch_activity['activity_name'];
                                        }
                                        else if($bookings->booking_type=="hotel")
                                        {
                                            $fetch_hotel=ServiceManagement::searchHotel($booking_type_id);
                                            echo $fetch_hotel['hotel_name'];
                                        }
                                        else if($bookings->booking_type=="guide")
                                        {
                                            $fetch_guide=ServiceManagement::searchGuide($booking_type_id);
                                            echo $fetch_guide['guide_first_name']." ".$fetch_guide['guide_last_name'];
                                        }
                                        else if($bookings->booking_type=="driver")
                                        {
                                            $fetch_driver=ServiceManagement::searchDriver($booking_type_id);
                                            echo $fetch_driver['driver_first_name']." ".$fetch_driver['driver_last_name'];
                                        }
                                        else if($bookings->booking_type=="sightseeing")
                                        {
                                            $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($booking_type_id);
                                            echo $fetch_sightseeing['sightseeing_tour_name'];
                                        }
                                        else if($bookings->booking_type=="itinerary")
                                        {
                                            $fetch_itinerary=ServiceManagement::searchItinerary($booking_type_id);
                                            echo $fetch_itinerary['itinerary_tour_name'];
                                        }
                                         else if($bookings->booking_type=="restaurant")
                                                        {
                                                            $fetch_restaurant=ServiceManagement::searchRestaurant($booking_type_id);
                                                            echo $fetch_restaurant['restaurant_name'];
                                                        }
                                        else if($bookings->booking_type=="transfer")
                                        {
                                            
                                            echo $bookings->booking_subject_name;
                                        }
                                    @endphp</td>
                                    <td>{{$bookings->customer_name}}</td>
                                    <td>{{$bookings->customer_email}}</td>
                                    <td>{{$bookings->customer_contact}}</td>
                                    <td>{{$bookings->booking_date}}</td>
                                    <td>{{$bookings->booking_currency}} {{$bookings->booking_amount}}</td>
                                    <td id="status_{{$bookings->booking_id}}">
                                    @if($bookings->booking_status==2)
                                    <button class="btn btn-sm btn-info">Cancelled</button>
                                    @elseif($bookings->booking_admin_status==0)
                                    <button class="btn btn-sm btn-warning">Pending</button>
                                    @elseif($bookings->booking_admin_status==1)
                                    <button class="btn btn-sm btn-success">Confirmed</button>
                                    @elseif($bookings->booking_admin_status==2)
                                    <button class="btn btn-sm btn-danger">Rejected</button>
                                    @endif
                                    </td>
                                    <td>{{$bookings->booking_admin_message}}</td>
                                    <td><button id="attachments_{{$bookings->booking_sep_id}}" class="btn btn-rounded btn-primary btn-sm attachments">View Attachments</button></td>
                                   <td>
                                        @if($bookings->itinerary_status==0)
                                        @if($bookings->booking_final_amount_status==0)
                                        <button type="button" class="btn btn-sm btn-rounded btn-info">Pending</button>
                                        @elseif($bookings->booking_final_amount_status==1)
                                        <button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button>
                                        @endif
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('agent-invoice',['booking_id'=>$bookings->booking_sep_id])}}" class="btn btn-rounded btn-default btn-sm">Agent Invoice</a> &nbsp;

                                        <a href="{{route('customer-invoice',['booking_id'=>$bookings->booking_sep_id])}}" class="btn btn-rounded btn-default btn-sm">Customer Invoice</a> &nbsp;

                                        @if($bookings->booking_status==2)

                                     @elseif($bookings->booking_admin_status==0 && $bookings->booking_supplier_status==0)
                                     <button class="btn btn-sm btn-info cancel" id="cancel_{{$bookings->booking_id}}">Cancel</button>
                                     @elseif($bookings->booking_admin_status==0 && $bookings->booking_supplier_status==1 && $bookings->booking_type=="sightseeing")
                                      <button class="btn btn-sm btn-info cancel" id="cancel_{{$bookings->booking_id}}">Cancel</button>
                                    @endif

                                    @if($bookings->itinerary_status==0)
                                    <button id="installment_{{$bookings->booking_sep_id}}" class="btn btn-rounded btn-primary btn-sm create_installment" @if($bookings->booking_admin_status!=1) style="display:none" @endif>Pay Amount</button>
                                    @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
                    </div>
                    <div id="navpills-confirmed" class="tab-pane show">
                    </div>
                    <div id="navpills-pending" class="tab-pane show">
                    </div>
                    <div id="navpills-rejected" class="tab-pane show">
                    </div>
                    <div id="navpills-cancelled" class="tab-pane show">
                    </div>

                 </div>
   </div>

</div>

</div>

</div>
</div>
</div>


@include('agent.includes.footer')

@include('agent.includes.bottom-footer')
   <div class="modal" id="attachmentModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">View Attachments</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form id="attachmentForm" action="{{route('booking-attachments-agent')}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="attachment_id" id="attachment_id" required="required">
                            <div class="row">
                                <div class="col-md-12" id="attachment_div"></div>
                                <div class="col-md-12">
                                    <button type="submit" id="updateAttachments" class="btn btn-primary pull-right">SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
<div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>

   <div class="modal" id="installmentModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Installments</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-md-12 mb-25">
                                <h5>Existing Installments </h5>
                                <div id="pre_install_table">
                                </div>
                             </div>
                            <div class="col-md-12 mb-25">
                                 <p>Payment Details</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><strong>Total Amount:</strong> <br> GEL <span id="total_amount_pay">0</span></p> 
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong>Remaining Amount <small>(yet to be generated)</small>:</strong> <br> GEL <span id="rem_amount_pay">0</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><strong>Paid Payment:</strong> <br> GEL <span id="rec_amount_pay">0</span></p> 
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong>Pending Payment:</strong> <br> GEL <span id="pen_amount_pay">0</span></p>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        @if(session()->has('success-message'))
        <script>
            $(document).ready(function()
            {
                swal("Success","{{session()->get('success-message')}}","success");
            });
        </script>
        @elseif(session()->has('error-message'))
        <script>
          $(document).ready(function()
          {
            swal("Error","{{session()->get('error-message')}}","error");
        });
    </script>
    @endif
<script>
    $(document).ready(function()
    {
        $('#example_all').DataTable();
  
    $(document).on("click","#navpills-all-link",function()
    {
         $("#loaderModal").modal("show");
         $("#navpills-all").html("");
        $.ajax({
            url:"{{route('agent-bookings-tab')}}",
            type:"GET",
            data:{"booking_type":"all"},
            success:function(response)
            {
                $("#navpills-all").html(response);
                $('#example_all').DataTable();
                 $("#loaderModal").modal("hide");
            }
        });
    });
     $(document).on("click","#navpills-confirmed-link",function()
    {
        $("#loaderModal").modal("show");
         $.ajax({
            url:"{{route('agent-bookings-tab')}}",
            type:"GET",
            data:{"booking_type":1},
            success:function(response)
            {
                $("#navpills-confirmed").html(response);
                   $('#example_1').DataTable();
                     $("#loaderModal").modal("hide");
            }
        }); 
    });
      $(document).on("click","#navpills-pending-link",function()
    {
        $("#loaderModal").modal("show");
         $.ajax({
            url:"{{route('agent-bookings-tab')}}",
            type:"GET",
            data:{"booking_type":0},
            success:function(response)
            {
                $("#navpills-pending").html(response);
                 $('#example_0').DataTable();
                   $("#loaderModal").modal("hide");
            }
        }); 
    });
        $(document).on("click","#navpills-rejected-link",function()
    {
        $("#loaderModal").modal("show");
        $.ajax({
            url:"{{route('agent-bookings-tab')}}",
            type:"GET",
            data:{"booking_type":2},
            success:function(response)
            {
                $("#navpills-rejected").html(response);
                 $('#example_2').DataTable();
                   $("#loaderModal").modal("hide");
            }
        }); 
    });

         $(document).on("click","#navpills-cancelled-link",function()
    {
        $("#loaderModal").modal("show");
        $.ajax({
            url:"{{route('agent-bookings-tab')}}",
            type:"GET",
            data:{"booking_type":"cancelled"},
            success:function(response)
            {
                $("#navpills-cancelled").html(response);
                 $('#example_cancelled').DataTable();
                   $("#loaderModal").modal("hide");
            }
        }); 
    });

         $(document).on("click",'.cancel',function()
                {
                    var id=this.id;
                    var actual_id=id.split("_");
                    var action_id=actual_id[1];
                    var remarks=$("#remarks").val();
                    swal({
                        title: "Are you sure?",
                        text: "You want to cancel this booking !",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes, Go Ahead",
                        cancelButtonText: "No, cancel!",
                        closeOnConfirm: false,
                        closeOnCancel: true
                    }, function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url:"{{route('agent-bookings-cancel')}}",
                                type:"POST",
                                data:{"_token":"{{csrf_token()}}",
                                "booking_id":action_id},
                                success:function(response)
                                {
                                        if(response.indexOf("reload")!=-1)
                                        {
                                            location.reload();
                                        }
                                        else if(response.indexOf("success")!=-1)
                                        {
                                              swal("Success!", "Selected Booking has been cancelled.", "success");
                                            $("#cancel_"+action_id).remove();
                                            $("#navpills-cancelled-link").trigger("click");
                                          
                                        }
                                         else if(response.indexOf("process")!=-1)
                                        {
                                        
                                           swal("Error", "Your booking is in process , so it cannot be cancelled now", "error");
                                        }
                                        else
                                        {
                                            swal("Error", "Unable to perform this operation", "error");
                                        }
                            
                                }
                            });
                        } else {

                        }
                    });
                });




         $(document).on("click",".create_installment",function()
    {
        $("#pay_amount_error").hide();
         $("#pay_amount_success").hide();
         $("#pay_amount").val("");
        var id=this.id;

        var actual_id=id.split("_")[1];

       
    $("#loaderModal").modal("show");
         $.ajax({
                url: "{{route('get-booking-installment-detail')}}",
                data: {
                    'booking_id':actual_id,
                    'request_type':'agent'
                },
                type: 'GET',
                dataType:"JSON",
                success: function (response)
                {

                    $("#total_amount_pay").text(response.total_amount);
                    $("#rem_amount_pay").text(response.remaining_amount);
                     $("#rec_amount_pay").text(response.received_amount);
                    $("#pen_amount_pay").text(response.pending_amount);
                     $("#pre_install_table").html(response.previous_installments_html);

                     // $('#installments_table').DataTable({ "scrollX": true});
                    $("#installmentModal").modal("show");


                    setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                    },500);


                }
            });
       

    });

});

  $(document).on("click",".attachments",function()
    {
        var id=this.id;
        var actual_id=id.split("_")[1];

        $("#loaderModal").modal("show");
        $("#attachment_id").val(actual_id);
         $.ajax({
                url: "{{route('booking-attachments-agent')}}",
                data: {
                    'booking_id':actual_id,
                },
                type: 'GET',
                success: function (response)
                {
                    if(response.indexOf("previewImg_new")!=-1)
                    {
                         $("#attachment_div").html(response);
                    $("#attachmentModal").modal("show");
                     setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                    },500); 
                    
                    }
                    else if(response.indexOf("invalid")!=-1)
                    {
                         setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                            alert("Invalid Booking ID");
                    },500);  
                       
                    }
                    else if(response.indexOf("user_session")!=-1)
                    {
                       setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                            alert("Invalid User Session");
                            location.reload();
                    },500); 
                    }
                   


                }
            });


    });

      $(document).on("click",".remove_already_attachments",function()
    {
        var image=this.id;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this attachment !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
              $("#"+image).parent().remove();
              swal("Deleted!", "Selected attachment has been deleted.", "success");
          } else {
            swal("Cancelled", "Your attachment is safe :)", "error");
        }
    });
    });

       $(document).on("click",".add_more_attachment",function(){ 
        var id=$(this).parent().parent().attr("id");
        var actual_id=id.split("increment");

        if(actual_id[1]=="")
        {
             var html = $("#clone").html();
          $("div#increment").after(html);
        }
        else
        {
              var html = $("#clone"+actual_id[1]).html();
          $("#"+id).after(html);

        }
         
      });
      $("body").on("click",".remove_more_attachment",function(){ 
          $(this).parents(".control-group").remove();
      });
</script>
</body>
</html>
