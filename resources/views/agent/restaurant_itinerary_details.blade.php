<?php
use App\Http\Controllers\ServiceManagement;
use App\Http\Controllers\AgentController;
?>
@include('agent.includes.top-header')

<style>
    .hotel-div {
        width: 90%;
        float: left;
        display: flex;
        background: white;
    }

    .hotel-list-div {
        display: block;

        border-radius: 5px;
        position: relative;
        /* clear: both; */
        height: 101%;
    }

    .hotel-img-div {

        width: 40% !important;
        float: left;
    }

    .hotel-details {
        float: left;
        width: 60%;
        padding: 0 15px;
    }

    .hotel-info-div {
        width: 10%;
        float: left;


        text-align: right;
        height: 100%;
    }

    .checked {
        color: orange;
    }

    span.hotel-s {
        border: 1px solid #F44336;
        padding: 2px 5px;
        display: inline-block;
        color: #f44336;
        font-size: 12px;
        margin: 0 10px 0 0;
    }

    p.hotel-name {
        font-size: 16px;
        margin: 10px 0 0;
        color: black;
        float: left;
    }


    .rate-no {
        float: right;
    }

    .rate-no {
        float: right;
        display: block;
        margin-top: 14px;
        background: #2d3134;
        color: white;
        padding: 1px 8px;
        font-size: 12px;
        border-radius: 5px;
    }

    .heading-div {
        clear: both;
    }

    .rating {
        display: flex;
        list-style: none;
        margin: 0;
        justify-content: space-between;
        padding: 0;
        width: 100%;
    }

    p.r-number {
        float: right;
    }

    p.time-info {
        color: #4CAF50;
    }

    p.info {
        clear: both;
        margin: 0;
    }

    span.tag-item {
        background: #ffcbcd;
        padding: 5px 10px;
        border-radius: 5px;
        color: #ff4e54;
    }

    .inclusions {
        margin: 20px 0;
    }

    span.inclusion-item {
        padding: 10px 10px 0 0;
        color: #644ac9;
    }

    img.icon-i {
        width: auto;
        height: 20px;
    }

    p.include-p {
        color: black;
    }

    .inclusion-p {
        color: green
    }

    p.price-p span {
        background: #ee2128;
        color: white;
        padding: 3px 5px 3px 9px;
        border-radius: 5px;
        position: relative;
        z-index: 9999;
        border-top-right-radius: 5px;
        border-bottom-right-radius: 5px;
        margin-left: 20px;
        border-bottom-left-radius: 4px;
        font-size: 12px;
    }

    p.price-p span:before {
        content: "";
        width: 15px;
        height: 14.5px;
        background: #ee2128;
        position: absolute;
        transform: rotate(45deg);
        top: 3.5px;
        left: -6px;
        border-radius: 0px 0px 0px 3px;
        z-index: -1;
    }

    p.price-p span:after {
        content: "";
        background: white;
        width: 4px;
        height: 4px;
        position: absolute;
        top: 50%;
        left: 0px;
        transform: translateY(-50%);
        border-radius: 50%;
    }

    p.price-p {
        color: #ee2128;
    }

    p.tax {
        font-size: 12px;
        margin: 0;
    }

    p.days {
        font-size: 12px;
        margin: 0;
    }

    p.offer {
        font-size: 25px;
        color: black;
        font-weight: bold;
        margin: 0;
    }

    p.cut-price {
        margin: 0;
        text-decoration: line-through;
        font-size: 19px;
    }

    .login-a {
        color: #0088ff;
        font-size: 14px;
        font-weight: 600;
        margin-top: 10px;
        display: block;
    }

    @media screen and (max-width:1200px) {
        p.hotel-name {
            font-size: 15px;
            margin: 10px 0 0;
            color: black;
            float: left;
        }

        p.r-number {
            float: right;
            font-size: 12px;
        }

        span.inclusion-item {
            padding: 10px 10px 0 0;
            color: #644ac9;
            display: block;
        }

        p.cut-price {
            margin: 0;
            text-decoration: line-through;
            font-size: 16px;
        }

        p.offer {
            font-size: 20px;
            color: black;
            font-weight: bold;
            margin: 0;
        }

        .login-a {
            color: #0088ff;
            font-size: 13px;
            font-weight: 600;
            margin-top: 10px;
            display: block;
        }
    }

    @media screen and (max-width:1200px) {
        .hotel-div {
            width: 100%;
            float: left;

            display: flex;
            background: white;
        }

        .hotel-info-div {
            width: 100%;
            height: 100%;
            float: left;



            text-align: left;
        }

        span.inclusion-item {
            padding: 10px 10px 0 0;
            color: #644ac9;
            display: inline;
        }
    }

    @media screen and (max-width:992px) {
        p.hotel-name {
            font-size: 17px;
            margin: 10px 0 0;
            color: black;
            float: none;
        }

        .rate-no {
            float: none;
            display: inline;
            margin-top: 14px;
            background: #2d3134;
            color: white;
            padding: 1px 8px;
            font-size: 12px;
            border-radius: 5px;
        }
        p.r-number {
            float: none;
            font-size: 12px;
        }

        .hotel-details {
            float: none !important;
            width: 100% !important;
            padding: 0 15px;
            margin-top: 20px;
        }

        .hotel-list-div {
            display: block;

            border-radius: 5px;
            position: relative;
            /* clear: both; */
            height: 101%;
        }

        .hotel-div {
            width: 100%;
            float: none;

            display: block;
            background: white;
        }

        .hotel-img-div {
            width: 100% !important;
            float: none !important;
            display: block !important;
        }

        a.flex-prev,
        a.flex-next {
            display: none;
        }

        span.inclusion-item {
            padding: 10px 10px 0 0;
            color: #644ac9;
            display: block;
        }
    }

    .packagePriceContainer {
        border: 1px solid gainsboro;
        border-radius: 5px;
    }

    .flexOne {
        background: #c7fffa;
        padding: 10px;
        border-radius: 5px;
        color: #009688;
    }

    .flexOne p {
        margin-bottom: 5px;
    }

    p.oldPrice {
        text-decoration: line-through;
        font-size: 17px;
        color: #6b6b6b;
        margin-bottom: 0;
    }

    p.c-price {
        color: #852d94;
        font-size: 24px;
        margin-bottom: 0;
    }

    span.font12 {
        font-size: 13px;
        margin-left: 5px;
        color: #616161;
    }

    .priceContainer {
        padding: 10px;
    }

    .wdth70 {
        position: absolute;
        top: 20px;
        right: 36px;
        background: #77d6cd;
        color: white;
        font-size: 12px;
        padding: 5px;
    }

    .viewOffers {
        /* margin-top: 10px; */
        padding: 0 15px;
        border-bottom: 1px solid gainsboro;
    }

    button#bookMyPackage {
        background: #009688;
        color: white;
        padding: 8px 15px;
        display: block;
        width: 120px;
        margin: 13px auto;
        text-align: center;
        border-radius: 21px;
        cursor: pointer;
    }

    img.h-img {
        width: 100%;
        height: 100%;
        border: 1px solid gainsboro;
        border-radius: 5px;
    }

    .viewOffers a {
        color: #59c789;
        font-size: 16px;
    }

    h4.head {
        margin-top: 16px;
        text-transform: capitalize;
        font-size: 22px;
        color: #009688;
    }

    p.itenerary {
        border-bottom: 2px solid #359ff4;
        font-size: 17px;
        color: #2196F3;
        padding-bottom: 5px;
    }

    .nav-pills .nav-link.active,
    .nav-pills .show>.nav-link {
        color: #009688;
        background-color: #ffffff;
        border-radius: 50px;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);

    }

    a.nav-link {
        border-radius: 50px !IMPORTANT;
        margin-right: 10px;
        padding: 6px 21px;
        text-align: center;
        font-size: 16px;
    }

    .tab-content {
        margin-top: 20px;
    }

    .t-tab {
        border: 1px solid #d0d0d0;
        padding: 5px;
        border-radius: 5px;
        margin-bottom: 30px;
        margin-left: 25px;
        margin-top: 55px;
        position: relative;
        border-top-left-radius: 0;
    }
    .t-tab a {
        color: #009688;
    }

    p.days {
        color: black;
        font-size: 18px;
        font-weight: 700;
    }

    p.title-1 {
        margin-bottom: 0;
        color: black;
        font-size: 16px;
    }

    p.title-2 {
        color: black;
        margin-bottom: 5px;
    }

    p.p-i {
        color: black;
    }

    span.span-i {
        color: gray;
        font-size: 12px;
    }

    a.remove {
        color: #359ff4;
        font-weight: 600;


    }

    a.chng {
        color: #359ff4;
        font-weight: 600;
    }

    .change {
        display: flex;
        justify-content: space-between;
    }

    .hr {
        border-right: 2px solid gainsboro;
    }

    .heading-div.c-h-div {
        display: flex;
        justify-content: space-between;
    }
    .t-div {
        display: inline-block;
        background: #ffe2ca;
        padding: 7px 20px;
        width: auto !important;
        border-radius: 5px;
        margin: 15px 15px 0;

    }
    div#activity_select__2__1 {
        margin: 0 !important;
    }
    img.cstm-activity-image {
        width: 100%;
        height: 150px;
    }
    button#save_itinerary {
        font-size: 13px;
        background: #ffe2ca;
        color: #ff5722;
        border-color: #fdd5c9;
        border-radius: 50px;
    }
    .theme-rosegold .nav-pills > li > a.active {
        border-top-color: #ffe2ca;
        background-color: #ffe2ca !important;
        color: #FF5722;
    }
    .theme-rosegold .nav-pills > li > a:hover {
        border-top-color: #ffe2ca !important;
        background-color: #ffe2ca !important;
        color: #FF5722 !important;
    }
    button#save_itinerary:hover {
        font-size: 13px;
        background: #ffe2ca !important;
        color: #ff5722 !important;
        border-color: #fdd5c9 !important;
        border-radius: 50px;
    }
    p.country {
        min-width: 150px;
        margin-bottom: 0;
        font-size: 20px;
        color: #e3aa00;
    }
    p.city {
        margin-bottom: 0;
        font-size: 13px;
        color: #e86e08;
    }
    .d-label
    {
        background: #303046;
        color: white;
        padding: 5px 10px;
        margin-bottom: 15px;
        border-radius: 3px;
    }
    .relative.latoBold.font16.lineHeight18.appendBottom20 {
        color: #ffffff;
        font-size: 14px;
        background: #199a8d;
        text-transform: capitalize;
        letter-spacing: 1px;
        padding-bottom: 5px;
        margin-bottom: 15px;
        display: inline-block;
        text-align: center;
        padding: 10px 20px;
        border-radius: 43px;
    }
    .accordContent {
        background: #c7fffa;
        padding: 15px;
        margin-bottom: 30px;
        border-radius: 5px;
        margin-left: 30px;
        position: relative;
    }
    .accordContent:before {
        position: absolute;
        top: 50%;
        content: "";
        left: -15px;
        transform: translateY(-50%);
        width: 15px;
        height: 1px;
        background: #c1c1c1;
    }
    .parent-div-acc div.accordContent:first-child:after {
        position: absolute;
        top: 5px;
        content: "";
        left: -16px;
        transform: translateY(-50%);
        width: 1px;
        height: 82%;
        background: #c1c1c1;
    }
    .accordContent:after {
        position: absolute;
        top: -16px;
        content: "";
        left: -16px;
        transform: translateY(-50%);
        width: 1px;
        height: 82px;
        background: #c1c1c1;
    }
    hr.h-hr {
        border-bottom: 1px solid gainsboro !important;
        display: block;
        width: 100%;
        margin-bottom: 30px;
    }
    label#add_more_rooms
    {
        font-size: 12px;
        cursor:pointer;
        margin-top:10px;
    }
    label.remove_more_rooms {
        color: white;
        font-size: 14px;
        cursor: pointer;
        float: right;
    }
    div.modal {
        z-index: 9999;
    }
    div#demo {
        margin-top: 25px;
        height: 300px;
    }
    .carousel-inner {
        height: 100%;
    }
    .carousel-item {
        height: 100%;
    }
    button#go_back_hotel_btn {
        background: #ffcbda;
        border: 1px solid #ffbed0;
        color: #c51e4e;
        border-radius: 0;
    }
    button#go_back_sightseeing_btn {
        background: #ffcbda;
        border: 1px solid #ffbed0;
        color: #c51e4e;
        border-radius: 0;
    }

    h2.title {
        margin: 0;
    }
    p.paragraph {
        margin: 0;
    }
    h3.amenty-title {
        font-size: 18px;
    }
    p.blackfont {
        margin: 0;
    }
    .go-icon {
        font-size: 20px;
        margin-right: 10px;
    }
    span.text-lt {
        /*max-width: 35%;*/
        width: 40%;
    }
    .select_activity,.select_hotel,.select_sightseeing
    {
        cursor:pointer;
    }
    img.cstm-hotel-image {
        width: 100%;
        height: 100%;
        object-fit: cover;
    }
    .day-count {
        background: white;
        margin-bottom: 20px;
        padding: 10px;
        border: 1px solid gainsboro;
        border-radius: 5px;
    }
    .select_hotel,
    .select_sightseeing,
    .select_activity {
        cursor: pointer !important;
    }

    .color-tiles-text {
        background: #573a9e85;
    }

    .grid-images img {
        width: 30%;
        margin: 10px;
        border: 1px solid #bdbdbd;
        padding: 4px;
        border-radius: 5px;
    }
    .grid-images {
        display: flex;
        flex-wrap: wrap;
        justify-content: flex-start;
    }
    p.para-h {
        color: #9C27B0;
        font-size: 18px;
        border-bottom: 1px dotted #9C27B0;
    }
    p.address {
        background: #c7fffa;
        padding: 5px 10px;
        border-radius: 5px;
        color: #009688;
        font-size: 13px;
        display: inline-block;
        margin: 5px 0;
    }
    .t-tab label {
        color: #545454;
        font-size: 13px;
        padding: 5px 15px;
        /* border-radius: 22px; */
        position: absolute;
        background: #d0d0d0;
        top: -30px;
        left: -1px;
    }
    p.star-p i {
        background: #ffccbc;
        color: #ff5722;
        padding: 7px;
        border-radius: 14px;
    }
    p.star-p {
        color: #FF5722;
        font-size: 15px;
        font-weight: 600;
        border-bottom: 1px solid lightgrey;
        padding-bottom: 8px;
    }
    .cstm-alert-notice{
        background: #e4e4e4 !important;
        color: #2f2f2f;
        border: none;
    }
    .box .overlay {
        z-index: 50;
        background: rgba(103, 58, 183, 0.39);
        border-radius: 5px;
    }
    p.color-tiles-text {
        background: #e1e1e1;
        position: absolute;
        width: 100%;
        top: 100%;
        left: 0;
        padding: 5px 10px;
        height: auto;
        display: flex;
        justify-content: center;
        color: #0088ff !important;
        border-radius: 5px;
        align-items: center;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
        font-size: 15px;
    }
    .bg-div {
        background: url(https://crm.traveldoor.ge/dev-custom/assets/images/vehicle-type.jpg)!important;
        height: 130px;
        border-bottom-left-radius: 0;
        border-bottom-right-radius: 0;
        margin-bottom: 65px;
        position: relative;
    }
    .theme-rosegold .alert-success {
        border-color: #ffffff;
        background-color: #e0e0e0 !important;
        color: #353535;
    }
    div.hotels_select .box.box-body.bg-dark.pull-up,.activity_select .box.box-body.bg-dark.pull-up,.sightseeing_select .box.box-body.bg-dark.pull-up,.transfer_select .box.box-body.bg-dark.pull-up{
        height: 115px;
        margin-bottom: 42px;
        margin-top: 20px;
    }
    .bg-dark {
      /*  background: url("{{asset('assets/images/vehicle-type.jpg')}}")!important;*/
      height: 130px;
      border-bottom-left-radius: 0;
      border-bottom-right-radius: 0;
      margin-bottom: 65px;
      position: relative;
  }
  label.tile-label {
    position: absolute !important;
    top: 8px !important;
    left: 20px;
    border-color: white;
    z-index: 999999;
    color: red !important;
}
a.details.text-light {
    color: #673AB7 !important;
    font-weight: 600;
}
div#selectSightseeingModal .modal-content ,div#selectRestaurantModal .modal-content{
    height: 700px;
}
div#selectSightseeingModal .modal-dialog,div#selectRestaurantModal .modal-dialog {
    margin-top: 1%;
}
div#selectSightseeingModal .modal-content .modal-body,div#selectRestaurantModal .modal-content .modal-body {
    padding: 10px;
}
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-track ,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #fbe1ff;
}
div#selectSightseeingModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus ,div#selectRestaurantModal .modal-footer button,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus{
    background: white;
    color: #9c27b0;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-thumb ,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar-thumb{
    background: #9C27B0;
    border-radius: 10px;
}

/* Handle on hover */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover,div#selectRestaurantModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #555;
}
div#selectSightseeingModal .modal-lg ,div#selectRestaurantModal .modal-lg{
    max-width: 787px;
}
div#selectSightseeingModal .modal-xl ,div#selectRestaurantModal .modal-xl{
    max-width: 1000px;
}
label.days_no {
    color: white;
    font-size: 15px;
    background: #FF5722;
    padding: 7px 20px;
    border-radius: 5px;
    width: 100px;
}
.days_div .select2-container--default .select2-selection--single {
    border: 1px solid #ffc5b2;
    border-radius: 20px;
    padding: 6px 12px;
    height: 34px;
}
.days_div p.color-tiles-text {
    background: #ff5722;
    position: absolute;
    width: 100%;
    top: 100%;
    left: 0;
    padding: 5px 10px;
    height: auto;
    display: flex;
    justify-content: center;
    color: #ffffff !important;
    border-radius: 5px;
    align-items: center;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    font-size: 15px;
}
button.sightseeing_details,button.sightseeing_details:hover,button.sightseeing_details:focus {
    background: #9C27B0 !important;
    border: none;
    padding: 5px 9px;
    color:white;
    font-size: 11px;
    border-radius: 16px;
}
.select_sightseeing .card .card-body {
    background: #fbe4ff;
}
.select_sightseeing .card {
    border: 1px solid #f7caff;
    padding: 2px;
    min-height: 245px;
}

.select_activity .card .card-body {
    background: #ffe7df;
}

.select_activity .card {
    border: 1px solid #fdc4b1;
    padding: 2px;
    min-height: 240px;
}
.select_activity .card .card-body a{
    background: #FF5722 !important;
    border: none;
    color:white;
    padding: 5px 9px;
    font-size: 11px;
    border-radius: 16px;
}
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}


div#selectHotelModal .modal-content {
    height: 700px;
}
div#selectHotelModal .modal-dialog {
    margin-top: 1%;
}
div#selectHotelModal .modal-content .modal-body {
    padding: 10px;
}
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #edffd9;
}
div#selectHotelModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #8BC34A;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #8BC34A;
    border-radius: 10px;
}

/* Handle on hover */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #8BC34A;
}
div#selectHotelModal .modal-lg {
    max-width: 787px;
}
div#selectHotelModal .modal-xl {
    max-width: 1000px;
}




div#selectActivityModal .modal-content {
    height: 700px;
}
div#selectActivityModal .modal-dialog {
    margin-top: 1%;
}
div#selectActivityModal .modal-content .modal-body {
    padding: 10px;
}
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #fdc4b1;
}
div#selectActivityModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #FF5722;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #FF5722;
    border-radius: 10px;
}

/* Handle on hover */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #e83a00;
}
div#selectActivityModal .modal-lg{
    max-width: 787px;
}
div#selectActivityModal .modal-xl {
    max-width: 1000px;
}
div#selectHotelModal .card-title {
    color: #8BC34A;
    text-transform: capitalize;
    font-size: 16px !important;
    border-bottom: 1px dotted #8BC34A;
}
table#room_type_table tr td, table#room_type_table tr th {
    padding: 5px;
    font-size: 13px;
}
table#room_type_table tr th:last-child {
    width: 100px !important;
}
table#room_type_table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #dee2e6;
}
.select_hotel .card .card-body {
    background: #edffd9 !IMPORTANT;
}
.select_hotel .card {
    border: 1px solid #d0e8b5;
    padding: 0px;
    max-height: 100%;
}
.select_hotel .card a {
    background: #8BC34A;
    border-color: #8BC34A;
    padding: 4px 10px;
    font-size: 13px;
}
table#room_type_table a {
    background: none;
    padding: 0;
}
.select_hotel {
    margin-bottom: 35px;
}


div#selectHotelModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #8BC34A;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #8BC34A;
    border-radius: 10px;
}

/* Handle on hover */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #8BC34A;
}

.modal-filter {
    background: #dbfdfa;
    padding: 10px;
    border-radius: 5px;
}
.modal-filter {
    background: #dbfdfa;
    padding: 10px;
    border: 1px solid #bef2ed;
    border-radius: 5px;
}
.modal-filter select,.modal-filter input,.modal-filter select:focus,.modal-filter input:focus {
    border: 1px solid #9ed9e2;
    border-radius: 50px;
}
button#search_transfer {
    background: #00BCD4;
    border: none;
    font-size: 14px;
    padding: 5px 15px;
    display: block;
    margin-top: 17px;
}
p.price {
    color: #9C27B0;
    font-size: 20px;
    padding: 0;
    text-align: right;
    border: 1px dashed;
    padding: 5px 10px;
    width: 130px;
    display: block;
    margin-left: auto;
}
select.type-select ,select.type-select:focus{
    border: 1px solid #f9d0ff;
    border-radius: 4px;
    width: 50%;
    margin-bottom: 20px;
    background: #fbe1ff;
    color: #9C27B0;
}
select.type-select:focu option{

    background: #fff;
    color: #9C27B0;
}
.modal-filter label {
    color: #00bcd4;
    margin-top: 10px;
}
div#transfer_div p:first-child {
    background: #ffe1e1;
    padding: 10px;
    color: #f44336;
    margin: 20px 0;
    border-radius: 5px;
}
.select_transfer .card .card-body {
    background: #dbfdfa;
}
.select_transfer .card {
    border: 1px solid #a6e3ea;
}

div#selectTransferModal .modal-content {
    height: 700px;
}
div#selectTransferModal .modal-dialog {
    margin-top: 1%;
}
div#selectTransferModal .modal-content .modal-body {
    padding: 10px;
}
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #a6e3ea;
}
div#selectTransferModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus,div#selectRestaurantModal .modal-footer button:hover,div#selectRestaurantModal .modal-footer button:focus {
    background: white;
    color: #00bcd4;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #00bcd4;
    border-radius: 10px;
}

/* Handle on hover */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #00b0ca;
}
div#selectTransferModal .modal-lg{
    max-width: 787px;
}
div#selectTransferModal .modal-xl {
    max-width: 1000px;
}
.select_transfer .card .card-body h4.card-title {
    color: #00bcd4;
    font-weight: 600;
}

button.btn.btn-success.btn-rounded.change_date.pull-right {
    background: #FF5722;
    border: none;
    margin-top: 21px;
}
.days_div .form-group {
    margin-bottom: 0;
}
div#loaderModal {
    background: #0000005c;
}
.table-sightseeing-loader svg{
    width: 100px;
    height: 100px;
    display:inline-block;
}
.add_activity,.add_restaurant
{
    margin-top:10px;
}
.sticky-sidebar
{
    max-height: 660px;overflow-y: auto;overflow-x: hidden;
}
.transfer_guide_remove
{
    background-color: red !important; 
}

#restaurant_food_select_info_head
{
    text-align: right;
    display: none;
}

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('agent.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('agent.includes.nav')

                <div class="content">



                    <div class="content-header">

                        <div class="d-flex align-items-center">

                            <div class="mr-auto">

                                <h3 class="page-title">Restaurant Itinerary</h3>

                                <div class="d-inline-block align-items-center">

                                    <nav>

                                        <ol class="breadcrumb">

                                            <li class="breadcrumb-item"><a href="#"><i
                                                class="mdi mdi-home-outline"></i></a></li>

                                                <li class="breadcrumb-item" aria-current="page">Home </li>

                                                <li class="breadcrumb-item active" aria-current="page">Restaurant Itinerary Details

                                                </li>

                                            </ol>

                                        </nav>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <form id="itinerary_form" action="{{route('itinerary-booking')}}" method="post">
                                    {{ csrf_field() }}
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <div class="row">

                                                        <div class="col-md-12">

                                                           <div class="row">

                                                              <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                                                                 <label class="d-label"> <span><i class="fa fa-car"></i>  Itinerary </span> - {{$no_of_days}} @if($no_of_days>1) Day @else Days @endif</label>

                                                             </div>
                                                              <input type="hidden" name="no_of_days" value="{{$no_of_days}}">
                                         <input type="hidden" name="from_date" value="{{date('Y-m-d',strtotime($itinerary_date_from))}}">
                                            <input type="hidden" name="to_date" value="<?php echo date('Y-m-d',strtotime("+".($no_of_days)." days",strtotime($itinerary_date_from))) ?>">

                                                         </div>
                                                         <p class="itenerary">RESTAURANT ITINERARY</p>
                                                         <ul class="nav nav-pills">
                                                            <li class="nav-item">
                                                                <a class="nav-link active" data-toggle="pill" href="#all">Day Plan</a>
                                                            </li>
                                                        </ul>

                                                        <!-- Tab panes -->
                                                        <div class="tab-content">
                                                            <div class="tab-pane container active" id="all">
                                                                @for($days_count=0;$days_count<$no_of_days;$days_count++)
                                                                <button type="button" class="btn collapsed" data-toggle="collapse" data-target="#days_count{{($days_count+1)}}" style="width: 100%;margin-bottom: 10px;text-align: left;border-top-color: #ffe2ca;background-color: #ffe2ca !important;color: #FF5722;" id="days_header{{($days_count+1)}}"> Day {{($days_count+1)}}</button>
                                                                <div class="day-count collapse" id="days_count{{($days_count+1)}}">
                                                                    <p class="days" style="display:none">Day {{($days_count+1)}} - </p>
                                                                    <div class="t-div">
                                                                        <!-- <p class="country"></p> -->
                                                                        <p class="city"><?php 
                                                                        $fetch_city_name=ServiceManagement::searchCities($selected_city,$selected_country);
                                                                        echo $fetch_city_name['name'];

                                                                        ?>,
                                                                        @foreach($countries as $country)
                                                                        @if($country->country_id==$selected_country)
                                                                        {{$country->country_name}}
                                                                        @endif
                                                                    @endforeach </p>
                                                                    <input type="hidden" id="days_country__{{($days_count+1)}}" name="days_country[{{$days_count}}]" value="{{$selected_country}}">

                                                                    <input type="hidden" id="days_city__{{($days_count+1)}}"  name="days_city[{{$days_count}}]" value="{{$selected_city}}">
                                                                    <input type="hidden" id="days_city_name__{{($days_count+1)}}"  name="days_city_name[{{$days_count}}]" value="{{$fetch_city_name['name']}}">
                                                                    <?php $days_date=date('Y-m-d',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?>
                                                                    <input type="hidden" id="days_dates__{{($days_count+1)}}" name="days_dates[]" value="{{$days_date}}">
                                                                </div>

                                                                <div class="t-tab" style="display:none">
                                                                    <label>Restaurants</label>
                                                                    <div class="row">
                                                                        <div class="col-md-12 restaurant_select__{{$days_count+1}}" id="restaurant_select__{{$days_count+1}}__1" style="margin:0 0 25px;display:none">
                                                                            <div class="hotel-list-div">

                                                                                <div class="hotel-div">
                                                                                    <div class="hotel-img-div">
                                                                                      <img class="cstm-restaurant-image" src="{{ asset('assets/images/no-photo.png')}}"
                                                                                      style="width:100%">

                                                                                  </div>
                                                                                  <div class="hotel-details">
                                                                                    <div class="heading-div">
                                                                                        <p class="hotel-name cstm-restaurant-name"></p>
                                                                                    </div>


                                                                                    <p class="info"></p>
                                                                                    <p class="address cstm-restaurant-address"><i class="fa fa-map-marker"></i></p>
                                                                                    <div class="heading-div c-h-div">
                                                                                        <div class="rating">
                                                                                            <br>  
                                                                                            <div class="row">
                                                                                               <div class="col-md-12">
                                                                                                <span class="span-i">SELECTED FOOD / DRINK :</span>
                                                                                                <p class="title-2 cstm-restaurant-food-type"></p>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <span class="span-i">DATE</span>
                                                                                                <p class="title-2"><?php echo date('D, d M Y',strtotime("+".($days_count)." days",strtotime($itinerary_date_from))); ?></p>
                                                                                            </div>
                                                                                            <div class="col-md-6">
                                                                                                <span class="span-i">FOOD / DRINK FOR :</span>
                                                                                                <p class="title-2 cstm-restaurant-food-purpose"></p>
                                                                                            </div>
                                                                                        </div>
                                                                                        <input type="hidden" class="restaurant_id" id="restaurant_id__{{($days_count+1)}}__1" name="restaurant_id[{{$days_count}}][]" value="">
                                                                                        <input type="hidden" class="restaurant_name" name="restaurant_name[{{$days_count}}][]"  id="restaurant_name__{{($days_count+1)}}__1" value="">
                                                                                        <input type="hidden" class="calc_cost restaurant_cost" id="restaurant_cost__{{($days_count+1)}}__1"  
                                                                                        name="restaurant_cost[{{$days_count}}][]"
                                                                                        value="0">
                                                                                        <input type="hidden" class="restaurant_food_for" name="restaurant_food_for[{{$days_count}}][]"  id="restaurant_food_for__{{($days_count+1)}}__1" value="">

                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="restaurant_food_detail" id="restaurant_food_detail__{{($days_count+1)}}__1"></div>
                                                                        </div>
                                                                        <div class="hotel-info-div">

                                                                           <a href="#" class="login-a change_restaurant" id="change_restaurant__{{$days_count+1}}_1">Change</a>

                                                                           <a href="javascript:void(0)" class="login-a remove_restaurant" id="remove_restaurant__{{$days_count+1}}__1">Remove</a>

                                                                           <a href="" class="login-a show_restaurant" id="show_restaurant__{{$days_count+1}}__1" target="_blanks">Details</a>

                                                                       </div>

                                                                   </div>


                                                               </div>
                                                           </div>
                                                       </div>
                                                       <div class="text-center"><button type="button" class="btn btn-md btn-primary add_restaurant" id="add_restaurant__{{($days_count+1)}}">+ Add Restaurant</button></div>
                                                   </div>
                                                   @endfor

                                               </div>
                                           </div>
                                       </div>
                                   </div>


                               </div>
                               <div class="col-md-4">
                                <div class="sticky-top sticky-sidebar">
                                    <div class="packagePriceContainer">
                                        <div class="priceContainer">
                                            <div class="flexOne">
                                                <p class="c-price">GEL <span id="total_cost_text">0</span><span class="font12"></span></p>
                                                <input type="hidden" name="total_cost" id="total_cost" value="0">
                                                <input type="hidden" name="markup_per" id="markup_per"  value="{{$markup}}">
                                                <input type="hidden" name="own_markup_per" id="own_markup_per"  value="{{$own_markup}}">
                                                <input type="hidden" name="total_cost_w_agent_markup" id="total_cost_w_agent_markup" value="0">
                                                <input type="hidden" name="total_cost_w_markup" id="total_cost_w_markup" value="0">
                                                <p class="font10 appendTop5">(Taxes are included in this price)</p>
                                            </div>
                                            <!--  <div class="wdth70"><span class="orangeGrad">27% OFF</span></div> -->
                                            @if(!isset($saved))
                               <!--  <div class="row rooms_div" id="rooms_div__1" style="margin-top: 10px;">
                                    <div class="col-md-3">
                                        <span class="rooms_text">Room 1</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="1">
                                    </div>
                                     <div class="col-md-4">
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3" >3</option>
                                            <option value="4" >4</option>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select name="select_child[]" class="form-control select_child" required="required">
                                            <option value="">Child</option>
                                            <option value="0">0</option>
                                           <option value="1">1</option>
                                           <option value="2">2</option>
                                           <option value="3">3</option>
                                        </select>
                                    </div>
                                    <div class="add_more_div">
                                       
                                    </div>
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row child_age_div" style="display: none">

                                            
                                        </div>
                                        </div>
                                        

                                 </div>
                             -->
                             <div class="row rooms_div" id="rooms_div__1" style="margin-top: 10px;">
                                   <!--  <div class="col-md-3">
                                        <span class="rooms_text">Room 1</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="1">
                                    </div> -->
                                    <div class="col-md-4"><label for="select_adults">Adults</label>
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                            @for($adults=1;$adults<=50;$adults++)
                                            <option value="{{$adults}}" @if($adults==1) selected="selected" @endif>{{$adults}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="select_child">Child</label>
                                        <select name="select_child[]" class="form-control select_child" required="required">
                                            <option value="">Child</option>
                                            @for($child=0;$child<=50;$child++)
                                            <option value="{{$child}}">{{$child}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="add_more_div">

                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row child_age_div" style="display: none">


                                        </div>
                                    </div>


                                </div>
                                @else
                                @php
                                $select_adults=unserialize($fetch_saved_itinerary->select_adults);
                                $select_child=unserialize($fetch_saved_itinerary->select_child);
                                $select_child_age=unserialize($fetch_saved_itinerary->select_child_age);
                                @endphp

                                @if(empty($select_adults))
                                <div class="row rooms_div" id="rooms_div__1" style="margin-top: 10px;">
                                    <!-- <div class="col-md-3">
                                        <span class="rooms_text">Room 1</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="1">
                                    </div> -->
                                    <div class="col-md-4"><label for="select_adults">Adults</label>
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                            @for($adults=1;$adults<=50;$adults++)
                                            <option value="{{$adults}}" @if($adults==1) selected="selected" @endif>{{$adults}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="select_child">Child</label>
                                        <select name="select_child[]" class="form-control select_child" required="required">
                                            <option value="">Child</option>
                                            @for($child=0;$child<=50;$child++)
                                            <option value="{{$child}}">{{$child}}</option>
                                            @endfor
                                        </select>                                    </div>
                                        <div class="add_more_div">

                                        </div>
                                        <div class="col-md-3">
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row child_age_div" style="display: none">


                                            </div>
                                        </div>
                                        

                                    </div>
                                    @else
                                    @php
                                    $child_count_age=0;
                                    @endphp
                                    @for($i=0;$i<1;$i++)
                                    <div class="row rooms_div" id="rooms_div__{{($i+1)}}" style="margin-top: 10px;">
                                   <!--  <div class="col-md-3">
                                        <span class="rooms_text">Room {{($i+1)}}</span>
                                        <input type="hidden" name="rooms_count[]" class="rooms_count" id="rooms_count" value="{{($i+1)}}">
                                    </div> -->
                                    <div class="col-md-4"><label for="select_adults">Adults</label>
                                        <select name="select_adults[]" class="form-control select_adults" required="required">
                                            <option value="">Adults</option>
                                            @for($j=1;$j<=50;$j++)
                                            <option value="{{$j}}" @if($select_adults[$i]==$j) selected="selected" @endif>{{$j}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                      <label for="select_child">Child</label>
                                      <select name="select_child[]" class="form-control select_child" required="required">
                                        <option value="">Child</option>
                                        @for($c=0;$c<=50;$c++)
                                        <option value="{{$c}}"  @if($select_child[$i]==$c) selected="selected" @endif>{{$c}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="add_more_div">
                                    @if($i>0)

                                    <label class="remove_more_rooms" id="remove_more_rooms__{{($i+1)}}">x</label>
                                    @endif

                                </div>
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-9">
                                    <div class="row child_age_div" @if($select_child[$i]<=0) style="display: none" @endif>
                                        @for($age=1;$age<=$select_child[$i];$age++)
                                        <div class="col-md-6"> <div class="form-group"><label for="child_age{{$age}}" style="color:black">Child Age {{$age}}</label><input type="number" min="0" id="child_age{{$age}}" name="child_age[]" class="form-control child_age" style="color:black" value="{{$select_child_age[($child_count_age+$age)-1]}}" required=""></div></div>

                                        @php
                                        $child_count_age++;
                                        @endphp
                                        @endfor

                                    </div>
                                </div>
                            </div>



                            @endfor
                            @endif
                            @endif
                               <!--   <div class="row">
                                    <div class="col-md-6"></div>
                                    <div class="col-md-6 text-right">
                                         <label id="add_more_rooms"><i class="fa fa-plus plus-icon"></i> Add room</label>
                                    </div>
                                </div> -->
                            </div>


                            <!-- <div class="viewOffers"><a href="javascript:void(0);">View All offers applied</a></div> -->
                            <div class="btnContainer"><button type="button" class="primaryBtn"
                                id="bookMyPackage">Book</button></div>
                                <button style="display:none" type="button" id="calculate_cost"></button>

                                <div class="hide"><a href="javascript:void(0);" id="create_quote_id">Create Quote</a>
                                </div>
                                <div class="hide"><a href="//holidayz.makemytrip.com/holidays/generateCrm"
                                    target="_blank">Crm Pax Association</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </form>

    </div>


</div>


</div>

</div>

</div>

</div>
@include('agent.includes.footer')
@include('agent.includes.bottom-footer')  

<div class="modal" id="selectRestaurantModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Restaurant</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
    </div>

    <!-- Modal body -->
    <div class="modal-body" style="height: 550px;
    overflow-y: auto;
    overflow-x: hidden;">
    <input type="hidden" id="restaurant_day_index">
    <input type="hidden" id="restaurant_day_index_self">
    <input type="hidden" id="restaurant_offset">
    <div class="row" id="restaurant_filter">
        <div class="col-md-5">
            <label for="restaurant_name_filter"><b>Restaurant Name</b></label>
            <input name="restaurant_name_filter" id="restaurant_name_filter" class="form-control" placeholder="Restaurant Name">
        </div>
    </div>
    <br>
    <div id="restaurant_div">
    </div>
    <div id="restaurant_div_buttons">
     <button type="button" class="btn btn-primary more_restaurant" id="show_more_restaurant" style=" margin: 5% 0%;width:100%;display: none !important;">Show More Restaurants</button>
     <button type="button" class="btn btn-primary more_restaurant" id="no_more_restaurant"  style=" margin: 5% 0%; width:100%;display: none !important;" disabled="disabled">No More Restaurants</button>
 </div>

 <div id="restaurant_details_div">
   <button id="go_back_restaurant_btn" class="btn btn-default"><i class="fa fa-angle-left go-icon"></i> Go back to Restaurant List</button>
   <div class="row">
    <div class="col-12">
        <div class="box">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <input type="hidden" name="restaurant_details_id" id="restaurant_details_id">
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <p class="para-h" id="restaurant_details_name">Restaurant Name</p>
                                <small id="restaurant_details_type"></small>
                                <div class="row">
                                    <div class="col-md-7">
                                        <p class="address" id="restaurant_details_address" ><i class="fa fa-map-marker"></i></p>
                                    </div>
                                    <div class="col-md-5">
                                        <p class="address" id="restaurant_details_timing">
                                            <i class="fa fa-clock-o" ></i></p>
                                        </div>      
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                   <input type="hidden"  id="selected_restaurant_address" value="">
                                   <input type="hidden"  id="selected_restaurant_detail_link" value="">
                                   <input type="hidden"  id="selected_restaurant_total_price" value="0">
                                   <input type="hidden"  id="selected_restaurant_food_items" value="">
                                   <p class="price">Total Price
                                    <span id="restaurant_total_price_text">GEL 0</span></p>
                                </div>
                                <div class="col-md-7 col-sm-12">
                                </div>
                                <div class="col-md-5 col-sm-12" id="restaurant_food_select_info_head">
                                    <h4><strong>Selected Food /  Drink Items :</strong></h4>
                                    <div id="restaurant_food_select_info">

                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="box">
                                        <div class="box-body" style="padding:0">
                                            <div class="grid-images" id="restaurant_detail_images">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12">
                                    <div class="section" id="">
                                        <p class="star-p"> <i class="fa fa-star star"></i>
                                            Restaurant Description
                                        </p>
                                        <div id="restaurant_detail_desc">

                                        </div>


                                    </div>
                                    <hr style="border-color: #ffc0c0;width: 100%;">
                                </div>
                            </div>

                            <hr>
                            <br>
                            <p class="star-p" id="restaurant_menu_div_head"> <i
                                class="fa fa-star star"></i> Restaurant Menu
                            </p>
                            <div class="col-md-12" id="restaurant_menu_div">
                            </div>
                            <br>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Modal footer -->
<div class="modal-footer">
    <div class="row">
      <div class="col-md-7 text-right">
       <label for="food_for" class="control-label"><b>Food / Drink For :</b></label>
   </div>
   <div class="col-md-3">
    <select name="food_for" id="food_for" class="form-control">
      <option value="">Select Food For</option>
      <option value="lunch">Lunch</option>
      <option value="dinner">Dinner</option>
  </select>
</div>
<div class="col-md-2">
   <button type="button" class="btn btn-danger" id="include_restaurant">Include</button>
</div>
</div>

<!--    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
</div>

</div>
</div>
</div>

<div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>

<script>
     $(document).on("click",".change_restaurant,.add_restaurant",function(e)
    {
        $("#restaurant_details_div,#restaurant_div_buttons").hide()
        var id=this.id;
        if(e.target.id.indexOf("add_restaurant")!=-1)
        {
         var new_id=id.split("__")[1];
        var restaurant_prev_id=$(".restaurant_select__"+new_id).last().attr("id");
           var restaurant_indiv_prev_id=$(".restaurant_indiv_select__"+new_id).last().attr("id");

        var check_restaurant_id=$("#"+restaurant_prev_id).find(".restaurant_id").val();

        if(check_restaurant_id=="")
        {
        restaurant_id=restaurant_prev_id.split("__")[1];
        var restaurant_index=restaurant_prev_id.split("__")[2]; 

        }
        else
        {
             restaurant_id=restaurant_prev_id.split("__")[1];
             var restaurant_index=restaurant_prev_id.split("__")[2]; 
            var clone_restaurant=$("#"+restaurant_prev_id).clone();
            var old_index=restaurant_index;
            var new_index=parseInt(old_index)+1;

        clone_restaurant.attr("id","restaurant_select__"+restaurant_id+"__"+new_index).css("display","none");
        clone_restaurant.find(".restaurant_id").attr("id","restaurant_id__"+restaurant_id+"__"+new_index).val("");
        clone_restaurant.find(".restaurant_name").attr("id","restaurant_name__"+restaurant_id+"__"+new_index).val("");
          clone_restaurant.find(".restaurant_cost").attr("id","restaurant_cost__"+restaurant_id+"__"+new_index).val(0);
            clone_restaurant.find(".restaurant_food_detail").attr("id","restaurant_food_detail__"+restaurant_id+"__"+new_index);
           clone_restaurant.find(".change_restaurant").attr("id","change_restaurant__"+restaurant_id+"_"+new_index);
            clone_restaurant.find(".remove_restaurant").attr("id","remove_restaurant__"+restaurant_id+"__"+new_index);
            clone_restaurant.find(".show_restaurant").attr({"id":"show_restaurant__"+restaurant_id+"__"+new_index,"href":""});
               // clone_restaurant.find(".restaurant_pax_div").attr("id","restaurant_pax_div__"+restaurant_id+"__"+new_index);
               //  clone_restaurant.find(".select_restaurant_adults").attr("id","restaurant_select_adults__"+restaurant_id+"__"+new_index);
               //  clone_restaurant.find(".select_restaurant_child").attr("id","restaurant_select_child__"+restaurant_id+"__"+new_index);
               //  clone_restaurant.find(".restaurant_child_age_div").attr("id","restaurant_child_age_div__"+restaurant_id+"__"+new_index);

            $("#"+restaurant_prev_id).after(clone_restaurant);

              var clone_indiv_restaurant=$("#"+restaurant_indiv_prev_id).clone();
            var old_indiv_index=restaurant_index;
            var new_indiv_index=parseInt(old_indiv_index)+1;

        clone_indiv_restaurant.attr("id","restaurant_indiv_select__"+restaurant_id+"__"+new_indiv_index).css("display","none");
           clone_indiv_restaurant.find(".change_restaurant").attr("id","change_indiv_restaurant__"+restaurant_id+"_"+new_indiv_index);
            clone_indiv_restaurant.find(".remove_restaurant").attr("id","remove_indiv_restaurant__"+restaurant_id+"__"+new_indiv_index);
            clone_indiv_restaurant.find(".show_restaurant").attr({"id":"show_indiv_restaurant__"+restaurant_id+"__"+new_indiv_index,"href":""});
            $("#"+restaurant_indiv_prev_id).after(clone_indiv_restaurant);

            restaurant_index=new_index;

        }

        }
        else
        {
        var restaurant_id=id.split("__")[1].split("_")[0];
        var restaurant_index=id.split("__")[1].split("_")[1]; 
        }
       

        var restaurant_date=$("#days_dates__"+restaurant_id).val();
        var restaurant_current_id=$("#restaurant_id__"+restaurant_id+"__"+restaurant_index).val();
        var country_id=$("#days_country__"+restaurant_id).val();
        var city_id=$("#days_city__"+restaurant_id).val();
        city_id+=","+$("#sightseeing_cities__"+restaurant_id).val();
            if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show()
        $.ajax({
            url:"{{route('agent-itinerary-get-restaurant')}}",
            data:{"country_id":country_id,
                    "city_id":city_id,
                    "restaurant_date":restaurant_date,
                    "restaurant_current_id":restaurant_current_id},
            type:"get",
            success:function(response)
            {
                $("#loaderModal").hide()
                $("#restaurant_div").html(response).show();
                $("#restaurant_day_index").val(restaurant_id);
                 $("#restaurant_day_index_self").val(restaurant_index);
                 $("#restaurant_filter").show()
                 $("#restaurant_sort_filter").val("");
                 $("#restaurant_name_filter").val("");
                $("#selectRestaurantModal").modal("show");
            }
        });
        }
        
    });
   $(document).on("click", ".restaurant_details", function () {
    var id = $(this).attr("id").split("_")[2];
    $("#loaderModal").modal("show");
    $.ajax({
        url: "{{route('agent-itinerary-get-restaurant-details')}}",
        data: {
            "restaurant_id": id
        },
        type: "get",
        dataType:"JSON",
        success: function (response) {

            if(response.status=="200")
            {
                $("#restaurant_div").hide();
                $("#restaurant_div_buttons").hide();
                $("#restaurant_total_price_text").text("GEL 0");
                $("#restaurant_food_select_info_head").hide();
                $("#restaurant_details_id").val(response.body.restaurant_id);
                $("#selected_restaurant_address").val(response.body.restaurant_address);
                $("#restaurant_details_name").text(response.body.restaurant_name);
                $("#restaurant_details_type").text(response.body.restaurant_type);
                $("#selected_restaurant_detail_link").val(response.body.restaurant_details_link);
                $("#restaurant_details_address").html('<i class="fa fa-map-marker"></i> '+response.body.restaurant_address);
                $("#restaurant_details_timing").html('<i class="fa fa-clock-o" ></i> '+response.body.restaurant_timing); 
                $("#restaurant_detail_images").html("");
                var images=response.body.restaurant_images; 

                $.each( images, function( key, value ) {
                   $("#restaurant_detail_images").append("<img src='"+value+"'>");
               });
                $("#restaurant_detail_desc").html(response.body.restaurant_description);
                $("#restaurant_menu_div").html(response.body.restaurant_menu_html);

                $("#loaderModal").modal("hide");

                $("#restaurant_filter").hide();
                $("#restaurant_details_div").show();
                document.querySelector("div#selectRestaurantModal .modal-content .modal-body").scrollTop=0 
            }
            else
            {
                swal("Error!","No Restaurant Details Found.","error");
            }


        }
    });
});


   $(document).on("click","#include_restaurant",function()
   {
    var check_food_selected_flag=0
    $(".food_qty").each(function()
    {
        if($(this).val()>0)
        {
         check_food_selected_flag++ 
     }

 })
    if(check_food_selected_flag<=0)
    {
        swal(" Error! ","Please select atleast one food / drink item from this restaurant","error");
        return false
    }

    var food_for=$("#food_for").val()
    if(food_for=="")
    {
      swal(" Error! ","Please select 'Food / Drink For' selection.","error");
      return false
  }
  var restaurant_id="";
  var restaurant_name="";
  var restaurant_cost="";
  var restaurant_address="";
  var restaurant_selected_food="";
  var restaurant_image="";

  var restaurant_day_index=$("#restaurant_day_index").val();
  var restaurant_day_index_self=$("#restaurant_day_index_self").val();

  restaurant_id=$("#restaurant_details_id").val();
  restaurant_name=$("#restaurant_details_name").text();
  restaurant_cost=$("#selected_restaurant_total_price").val();
  restaurant_address=$("#selected_restaurant_address").val();
  restaurant_selected_food=$("#selected_restaurant_food_items").val();
  restaurant_image=$("#restaurant__"+restaurant_id).find('.search_restaurant_image').val();

  var search_restaurant_detail_link=$("#selected_restaurant_detail_link").val();    


  var restaurant_food_detail_html="";

  $(".food_qty").each(function()
  {
    if($(this).val()>0)
    {
        var food_qty=$(this).val();
        var food_index=$(this).attr('id').split("__")[1];

        var food_id=$('input[name="food_id['+food_index+']').val()
        var food_name=$('input[name="food_name['+food_index+']').val()
        var food_category_id=$('input[name="food_category_id['+food_index+']').val()
        var food_price=$('input[name="food_price['+food_index+']').val()
        var food_unit=$('input[name="food_unit['+food_index+']').val()
        var food_qty=$('select[name="food_qty['+food_index+']').val()


        restaurant_food_detail_html+='<input type="hidden" class="restaurant_food_name" name="restaurant_food_name['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_name+'"><input type="hidden" class="restaurant_food_qty" name="restaurant_food_qty['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_qty+'"><input type="hidden" class="restaurant_food_price" name="restaurant_food_price['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_price+'"><input type="hidden" class="restaurant_food_id" name="restaurant_food_id['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_id+'"><input type="hidden" class="restaurant_food_category_id" name="restaurant_food_category_id['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_category_id+'"><input type="hidden" class="restaurant_food_unit" name="restaurant_food_unit['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_unit+'">';
    }

});

  $("#restaurant_food_detail__"+restaurant_day_index+"__"+restaurant_day_index_self).html(restaurant_food_detail_html);
  $("#restaurant_id__"+restaurant_day_index+"__"+restaurant_day_index_self).val(restaurant_id);
  $("#selected_restaurant_name__"+restaurant_day_index+"__"+restaurant_day_index_self).text(restaurant_name);
  $("#restaurant_name__"+restaurant_day_index+"__"+restaurant_day_index_self).val(restaurant_name);
  $("#restaurant_food_for__"+restaurant_day_index+"__"+restaurant_day_index_self).val(food_for);
  $("#restaurant_cost__"+restaurant_day_index+"__"+restaurant_day_index_self).val(restaurant_cost);
  $("#restaurant_warning__"+restaurant_day_index+"__"+restaurant_day_index_self).hide();

  $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-name').text(restaurant_name);
  $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-name').text(restaurant_name);
  $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-address').html('<i class="fa fa-map-marker"></i> '+restaurant_address);
  $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-address').html('<i class="fa fa-map-marker"></i>'+restaurant_address);
  $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-image').attr("src",restaurant_image);
  $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-image').attr("src",restaurant_image);
  $("#show_restaurant__"+restaurant_day_index+"__"+restaurant_day_index_self+",#show_indiv_restaurant__"+restaurant_day_index+"__"+restaurant_day_index_self).attr("href",search_restaurant_detail_link);
  $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-food-type').text(restaurant_selected_food.replace(/---/g," , "));
  $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-food-type').text(restaurant_selected_food.replace(/---/g," , "));
  $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-food-purpose').text("For "+food_for);
  $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-food-purpose').text("For "+food_for);



              // alert(restaurant_day_index);
              //  alert(restaurant_day_index_self);
              $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).parent().parent().show();
              $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).show();

              $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).parent().parent().show();
              $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).show();


              if ($("#restaurant_warning__"+restaurant_day_index+"__"+restaurant_day_index_self).length)
              {
               $("#warning_count").val(parseInt($("#warning_count").val())-1);
               $("#restaurant_warning__"+restaurant_day_index+"__"+restaurant_day_index_self).hide()
           }


           $("#calculate_cost").trigger("click");

           $("#selectRestaurantModal").modal("hide");

       });

$(document).on("change",".food_qty",function()
{
    var html="";
    var html_as_value=[];
    var total_price=0;

    $(".food_qty").each(function()
    {
      if($(this).val()>0)
      {
        var id=$(this).attr('id').split("__")[1];
        var food_price=$("#food_price__"+id).val()

        var food_qty=$(this).val()

        var food_name=$("#food_name__"+id).val()

        var food_unit=$("#food_unit__"+id).val()

        var calculated_price=parseInt(food_price)*food_qty
        total_price+=parseInt(calculated_price)
        html+='<div class="row"><div class="col-12"><p class="price-2">'+food_name+' ('+food_qty+' X '+food_unit+')</p></div></div>';
        html_as_value.push(food_name+' ('+food_qty+' X '+food_unit+')');
    }
})


    $("#restaurant_food_select_info").html(html)
    $("#selected_restaurant_food_items").val(html_as_value.join("---"))
    $("#restaurant_food_select_info_head").show()
    $("#restaurant_total_price_text").text("GEL "+total_price)
    $("#selected_restaurant_total_price").val(total_price)
})
 $(document).on("click","#calculate_cost",function()
              {
                 $("#loaderModal").show();
                var formdata=$("#itinerary_form").serialize();
                 $.ajax({
                    url:"{{route('itinerary-booking-whole-cost')}}",
                    type:"POST",
                    data:formdata,
                    success:function(response)
                    {
                        var response1=response.split("--");
                         $("#total_cost_w_markup").val(response1[0]);
                       $("#total_cost_w_agent_markup").val(response1[1]);
                       $("#total_cost").val(response1[2]);
                       $("#total_cost_text").text(response1[2]);
                       console.log(response);

                        $("#loaderModal").hide();
                    }

                });


              });
</script>

</body>
</html>