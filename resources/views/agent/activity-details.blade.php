<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('agent.includes.top-header')
<style>
    .carousel-item img {
        height: 100%;
    }
    .carousel-item {
        height: 100%;
    }
    p.start_price {
        margin: 0;
    }
    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }
    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;
        border-radius: 5px;
    }
    .hotel_detail {
        height: 150px;
    }
    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }
    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }
    .booking_detail {
        padding: 15px;
    }
    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }
    td p {
        margin: 0;
    }
    td {
        background: gainsboro;
    }
    table {
        border-collapse: separate;
    }
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #f44336;
    }
    a.panel-title {
        position: relative !important;
        background: #ffe4dc;
        color: #f44336 !important;
        padding: 13px 20px 13px 85px !important;
        border-radius: 0px;
    }
    .panel-title {
        display: block;
        margin-top: 0;
        margin-bottom: 0;
        padding: 1.25rem;
        font-size: 18px;
        color: #4d4d4d;
        height: 40px;
    }
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }
    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }
    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 9px 10px;
        width: auto;
        max-width: 165px;
        border-radius: 0;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 40px;
        background: #f44336;
        z-index: 999;
        transform: none !important;
    }
    .tab-content {
        margin-top: 10px;
    }
    div.panel-heading {
        border: none !important;
    }
    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
    div#carousel-example-generic-captions {
        width: 100%;
    }
    p.hotel-type {
        display: inline-block;
        background: linear-gradient(45deg, #d5135a, #f05924);
        color: white;
        padding: 10px;
        margin-left: 5px;
        margin-bottom: 0;
        border: 1px solid white;
        color: white;
    }
    .flexslider2 a.flex-prev {
        color: white;
        z-index: 98999;
        background: black;
        overflow: visible;
        left: 0 !important;
        position: absolute;
        opacity: 1;
        text-shadow: none;
        box-shadow: none;
    }
    .flexslider2 a.flex-next{
        color: white;
        z-index: 98999;
        background: black;
        overflow: visible;
        right: 42px !important;
        position: absolute;
        opacity: 1;
        text-shadow: none;
        box-shadow: none;
    }

    ul.flex-direction-nav {
        position: absolute;
        z-index: 999;
        opacity: 1;
        visibility: visible;
        top: 40%;
        transform: translateY(-50%);
        width: 100%;
        height: 10px;
    }
    .flex-direction-nav a:before {
        font-family: "flexslider-icon";
        font-size: 29px !important;
        display: inline-block !important;
        content: \f001;
        color: rgba(255, 255, 255, 0.8) !important;
        text-shadow: none !important;
    }
    ol.flex-control-nav.flex-control-thumbs {
        width: 100%;
        display: flex;
        flex-wrap: wrap;
    }
    .flex-control-thumbs li {
        width: 13% !important;
        float: left !important;
        margin: 2.4px !important;
        height: 52px !important;

    }
    .flexslider1 a.flex-prev {
        color: white;
        z-index: 98999;
        overflow: visible;
        left: 0 !important;
        position: absolute;
        opacity: 1;
        text-shadow: none;
        box-shadow: none;
    }
    .flexslider2 a.flex-prev {
        color: white;
        z-index: 98999;
        background: black;
        overflow: visible;
        left: 0 !important;
        position: absolute;
        opacity: 1;
        text-shadow: none;
        box-shadow: none;
    }
    .flexslider1 a.flex-next{
        color: white;
        z-index: 98999;
        overflow: visible;
        right: 33px !important;
        position: absolute;
        opacity: 1;
        text-shadow: none;
        box-shadow: none;
    }
    .flexslider2 a.flex-next{
        color: white;
        z-index: 98999;
        background: black;
        overflow: visible;
        right: 42px !important;
        position: absolute;
        opacity: 1;
        text-shadow: none;
        box-shadow: none;
    }
    .flexslider1 li.flex-active-slide {
        width: 420px !important;
        height: 300px;
    }
  
.flexslider2 {
    width: 100%;
    height: auto;
}
li.flex-active-slide {
    height: 500px;
}

/*.flexslider2 .slides img {
    width:100%;
    height:100%;
}*/
.flexslider2 .slides img {
    width: auto;
    height: 100%;
    margin: auto;
    display: block;
}
.flex-control-thumbs img{
    opacity:1;
}
    .flex-viewport {
        height: 500px;
        background: rgb(14, 14, 14);
    }
    .flex-silder-div .flex-viewport {
        height: 300px;
    }
    .flex-viewport {
        max-height: 100%;
        -webkit-transition: all 1s ease;
        -moz-transition: all 1s ease;
        -ms-transition: all 1s ease;
        -o-transition: all 1s ease;
        transition: all 1s ease;
    }
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
  
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('agent.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('agent.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Activity Details</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                                <li class="breadcrumb-item" aria-current="page">Home
                                                    <li class="breadcrumb-item active" aria-current="page">Activity Details
                                                    </li>
                                                </ol>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="box">
                                    <div class="box-body">
                                        <div class="row mb-20" style="margin-bottom:60px !important">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-12" style="padding:0;overflow:hidden">
                                                        <div class="slider-box">
                                                            <div class="heading-name">
                                                                <p class="country_name ng-binding">
                                                                    {{strtoupper($get_activities->activity_name)}}
                                                                </p>
                                                            </div>

                                                            <div class="col-lg-12 col-md-12">
                                                                <div class="box">
                                                                    <div class="box-body">
                                                                        <div class="flexslider2">
                                                                            <ul class="slides">
                                                                                @php
                                                                                $get_activity_images=unserialize($get_activities->activity_images);
                                                                                @endphp
                                                                                @if(!empty($get_activity_images))
                                                                                @php
                                                                                for($images=0;$images< count($get_activity_images);$images++)
                                                                                {
                                                                                    if(count($get_activity_images)==1)
                                                                                    {
                                                                                        @endphp
                                                                                        <li
                                                                                        data-thumb="{{asset('assets/uploads/activities_images')}}/{{$get_activity_images[$images]}}">
                                                                                        <img src="{{asset('assets/uploads/activities_images')}}/{{$get_activity_images[$images]}}"
                                                                                        alt="slide" />
                                                                                        @php
                                                                                    }
                                                                                    @endphp
                                                                                    <li
                                                                                    data-thumb="{{asset('assets/uploads/activities_images')}}/{{$get_activity_images[$images]}}">
                                                                                    <img src="{{asset('assets/uploads/activities_images')}}/{{$get_activity_images[$images]}}"
                                                                                    alt="slide" />
                                                                                </li>
                                                                                @php
                                                                            }
                                                                            @endphp
                                                                            @else
                                                                            <li
                                                                            data-thumb="{{asset('assets/images/no-photo.png')}}">
                                                                            <img src="{{asset('assets/images/no-photo.png')}}"
                                                                            alt="slide" />
                                                                        </li>
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if($activity_type!="")
                                                <p class="hotel-type">{{$activity_type}}</p>
                                                @endif
                                                @if($get_activities->activity_duration!="" && $get_activities->activity_duration!=null)
                                                <p class="hotel-type"><b>Duration : </b>{{$get_activities->activity_duration}}</p>
                                                @endif
                                            </div>
                                            <div class="col-md-12" style="margin-top:10px">
                                                @php echo $get_activities->activity_description; @endphp
                                            </div>
                                            <div class="col-md-12" style="margin-top:20px">
                                                <div class="box">
                                                    <div class="box-body accor-div">
                                                        <!-- Nav tabs -->
                                                        <ul class="nav nav-tabs customtab" role="tablist">
                                                                    <!--   <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#Itenary"
                                                                        role="tab"><span class="hidden-sm-up"><i class="ion-home"></i></span> <span
                                                                    class="hidden-xs-down">Itenary</span></a> </li>
                                                                    <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#Blackout" role="tab"><span
                                                                    class="hidden-sm-up"><i class="ion-person"></i></span> <span
                                                                    class="hidden-xs-down">Blackout</span></a> </li> -->
                                                                    <li class="nav-item"> <a class="nav-link active"
                                                                        data-toggle="tab" href="#Inclusion" role="tab"><span
                                                                        class="hidden-sm-up"><i
                                                                        class="ion-email"></i></span> <span
                                                                        class="hidden-xs-down">Inclusions</span></a> </li>
                                                                        <li class="nav-item"> <a class="nav-link " data-toggle="tab"
                                                                            href="#Exclusion" role="tab"><span
                                                                            class="hidden-sm-up"><i class="ion-home"></i></span>
                                                                            <span class="hidden-xs-down">Exclusions</span></a> </li>
                                                                            <li class="nav-item"> <a class="nav-link" data-toggle="tab"
                                                                                href="#Cancellation" role="tab"><span
                                                                                class="hidden-sm-up"><i
                                                                                class="ion-person"></i></span>
                                                                                <span class="hidden-xs-down">Cancellation</span></a>
                                                                            </li>
                                                                            <li class="nav-item"> <a class="nav-link" data-toggle="tab"
                                                                                href="#tc" role="tab"><span class="hidden-sm-up"><i
                                                                                    class="ion-email"></i></span>
                                                                                    <span class="hidden-xs-down">T
                                                                                    & C</span></a> </li>
                                                                                </ul>
                                                                                <!-- Tab panes -->
                                                                                <div class="tab-content accord-content">
                                                                                    <div class="tab-pane" id="Blackout" role="tabpanel">
                                                                                        <div class="row">
                                                                                            <div class="col-md-12">
                                                                                                <div class="tab-content" style="margin-top:0">
                                                                                                    <div id="navpills-1" class="tab-pane active">
                                                                                                        <!-- Categroy 1 -->
                                                                                                        <div class=" tab-pane animation-fade active"
                                                                                                        id="category-1" role="tabpanel">
                                                                                                        <div class="panel-group panel-group-simple panel-group-continuous"
                                                                                                        id="accordion8"
                                                                                                        aria-multiselectable="true"
                                                                                                        role="tablist">
                                                                                                        <!-- Question 1 -->
                                                                                                        <div class="panel">
                                                                                                            <div class="panel-heading"
                                                                                                            id="question-1" role="tab">
                                                                                                            <a class="panel-title"
                                                                                                            title="Day 1"
                                                                                                            aria-controls="answer-1"
                                                                                                            aria-expanded="true"
                                                                                                            data-toggle="collapse"
                                                                                                            href="#answer-1"
                                                                                                            data-parent="#accordion8">
                                                                                                            Blackout
                                                                                                        </a>
                                                                                                    </div>
                                                                                                    <div class="panel-collapse collapse show"
                                                                                                    id="answer-1"
                                                                                                    aria-labelledby="question-1"
                                                                                                    role="tabpanel">
                                                                                                    <div class="panel-body">
                                                                                                        Nothing To Show
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <!-- End Categroy 1 -->
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="tab-pane active" id="Inclusion" role="tabpanel">
                                                                    <div class="row">
                                                                        <div class="col-md-12">
                                                                            <div class="tab-content" style="margin-top:0">
                                                                                <div id="navpills-1" class="tab-pane active">
                                                                                    <!-- Categroy 1 -->
                                                                                    <div class=" tab-pane animation-fade active"
                                                                                    id="category-1" role="tabpanel">
                                                                                    <div class="panel-group panel-group-simple panel-group-continuous"
                                                                                    id="accordion2"
                                                                                    aria-multiselectable="true"
                                                                                    role="tablist">
                                                                                    <!-- Question 1 -->
                                                                                    <div class="panel">
                                                                                        <div class="panel-heading"
                                                                                        id="question-1" role="tab">
                                                                                        <a class="panel-title"
                                                                                        title="Inclusions"
                                                                                        aria-controls="answer-1"
                                                                                        aria-expanded="true"
                                                                                        data-toggle="collapse"
                                                                                        href="#answer-1"
                                                                                        data-parent="#accordion2">
                                                                                    </a>
                                                                                </div>
                                                                                <div class="panel-collapse collapse show"
                                                                                id="answer-1"
                                                                                aria-labelledby="question-1"
                                                                                role="tabpanel">
                                                                                <div class="panel-body">
                                                                                    <?php echo $get_activities->activity_inclusions; ?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="Exclusion" role="tabpanel">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="tab-content" style="margin-top:0">
                                                            <div id="navpills-1" class="tab-pane active">
                                                                <div class=" tab-pane animation-fade active"
                                                                id="category-1" role="tabpanel">
                                                                <div class="panel-group panel-group-simple panel-group-continuous"
                                                                id="accordion2"
                                                                aria-multiselectable="true"
                                                                role="tablist">
                                                                <div class="panel">
                                                                    <div class="panel-heading"
                                                                    id="question-1" role="tab">
                                                                    <a class="panel-title"
                                                                    title="Exclusions"
                                                                    aria-controls="answer-1"
                                                                    aria-expanded="true"
                                                                    data-toggle="collapse"
                                                                    href="#answer-1"
                                                                    data-parent="#accordion2">
                                                                </a>
                                                            </div>
                                                            <div class="panel-collapse collapse show"
                                                            id="answer-1"
                                                            aria-labelledby="question-1"
                                                            role="tabpanel">
                                                            <div class="panel-body">
                                                                <?php echo $get_activities->activity_exclusions; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="Cancellation" role="tabpanel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="tab-content" style="margin-top:0">
                                        <div id="navpills-1" class="tab-pane active">
                                            <div class=" tab-pane animation-fade active"
                                            id="category-1" role="tabpanel">
                                            <div class="panel-group panel-group-simple panel-group-continuous"
                                            id="accordion2"
                                            aria-multiselectable="true"
                                            role="tablist">
                                            <div class="panel">
                                                <div class="panel-heading"
                                                id="question-1" role="tab">
                                                <a class="panel-title"
                                                title="Cancellation"
                                                aria-controls="answer-1"
                                                aria-expanded="true"
                                                data-toggle="collapse"
                                                href="#answer-1"
                                                data-parent="#accordion2">
                                            </a>
                                        </div>
                                        <div class="panel-collapse collapse show"
                                        id="answer-1"
                                        aria-labelledby="question-1"
                                        role="tabpanel">
                                        <div class="panel-body">
                                            <?php echo $get_activities->activity_cancel_policy; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tc" role="tabpanel">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content" style="margin-top:0">
                    <div id="navpills-1" class="tab-pane active">
                        <!-- Categroy 1 -->
                        <div class=" tab-pane animation-fade active"
                        id="category-1" role="tabpanel">
                        <div class="panel-group panel-group-simple panel-group-continuous"
                        id="accordion2"
                        aria-multiselectable="true"
                        role="tablist">
                        <!-- Question 1 -->
                        <div class="panel">
                            <div class="panel-heading"
                            id="question-1" role="tab">
                            <a class="panel-title"
                            title="Terms And Conditions"
                            aria-controls="answer-1"
                            aria-expanded="true"
                            data-toggle="collapse"
                            href="#answer-1"
                            data-parent="#accordion2">
                        </a>
                    </div>
                    <div class="panel-collapse collapse show"
                    id="answer-1"
                    aria-labelledby="question-1"
                    role="tabpanel">
                    <div class="panel-body">
                        <?php echo $get_activities->activity_terms_conditions; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<style>
    .a-price {
        color: #f44336;
        font-size: 40px;
        line-height: 1;
        font-weight: 600;
        position: relative;
    }
    span.from {
        color: #f44336;
        font-size: 14px;
        display: block;
        margin-bottom: 10px;
    }
    .price-b {
        border: 3px dashed #F44336;
        padding: 10px 15px;
        text-align: center;
        margin-bottom: 20px;
    }
    .heading-name p {
        color: #ffffff;
        background: linear-gradient(45deg, #d5135a, #f05924);
        font-size: 16px !IMPORTANT;
        border: none;
        padding: 5px 10px;
        border-radius: 0 10px 10px 0;
        text-align: center;
        position: absolute;
        z-index: 99;
        min-width: 167px;
        transform: translate(-6px, 7px);
    }
    .heading-name p:before {
        content: "";
        position: absolute;
        border-width: 10px;
        border-style: solid;
        border-color: #2f2a2a00 #da204f transparent #00000000;
        top: 0;
        transform: translate(-83px, 24px);
    }
    .slider-box {
        padding: 6px;
        overflow: hidden !important;
    }
    .accor-div {
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
    }
    .heading-name {
        border-bottom: none;
        margin-bottom: 0px;
        margin-top: 0;
    }
    .hotel_detail {
        background: #ffe4dc;
        padding: 15px;
        margin: 20px 0;
    }
    .panel-body p {
        color: #f44336;
        font-size: 16px;
        text-transform: capitalize;
    }
    .panel-body {
        background: #f2f2f2;
    }
    .input-group-addon {
        border-color: gainsboro !important;
        border-radius: 0 !important;
        background: gainsboro !important;
    }
    input#select_date,
    select#adult_count,
    select#child_count {
        border-radius: 0;
        border-color: gainsboro;
        background: white;
    }
    span.info-s {
        color: white;
        background: #909090;
        font-size: 12px;
        padding: 2px 2px;
        width: 16px;
        font-weight: 800;
        height: 16px;
        border-radius: 50%;
        position: absolute;
        box-sizing: border-box;
        left: 80%;
        display: inline-block;
        cursor: auto;
    }
    ul.nav.nav-tabs.customtab li a.active {
        background: linear-gradient(45deg, #d5135a, #f05924);
        border-radius: 5px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
        border: none;
    }
    ul.nav.nav-tabs.customtab li a {
        border: 1px solid gainsboro;
        border-radius: 5px;
        padding: 8px 15px;
        border-top-right-radius: 20px;
        border-bottom-right-radius: 20px;
    }
    .tab-content.accord-content {
        flex: 0 0 75%;
        margin: 0;
    }
    ul.nav.nav-tabs.customtab li {
        margin: 0 0 10px;
        position: relative;
    }
    .panel-body {
        background: #ffe4dc;
        padding: 20px !important;
        color: #f44336;
        height: auto;
        min-height: 155px;
        border-radius: 0;
    }
    ul.nav.nav-tabs.customtab {
        border: none;
        flex-direction: column;
        flex: 0 0 20%;
        text-align: left;
    }
    #select_date
    {
        pointer-events: none;
    }
</style>
<div class="col-md-4" @isset($_GET['itinerary']) style="display:none" @endisset>
    <div class="row">
        <div class="col-md-12">
            <div class="price-b">
                <span class="from">From <span class=" ng-binding">
                    @php
                     $adult_price=0;
                      $adult_price_details=unserialize($get_activities->adult_price_details);

                      if(!empty($adult_price_details))
                      {
                         $adult_price=$adult_price_details[0]['adult_pax_price'];
                      }
                      else
                      {
                        $adult_price=0;
                      }

                    $markup_cost=round($adult_price*$markup)/100;
                    $total_agent_adult_cost=round($adult_price+$markup_cost);

                    $own_markup_cost=round($total_agent_adult_cost*$own_markup)/100;
                    $total_adult_cost=round($total_agent_adult_cost+$own_markup_cost);
                    
                      $child_price=0;
                      $child_price_details=unserialize($get_activities->child_price_details);

                      if(!empty($child_price_details))
                      {
                         $child_price=$child_price_details[0]['child_pax_price'];
                      }
                      else
                      {
                        $child_price=0;
                      }

                    $markupchild_cost=round($child_price*$markup)/100;
                    $total_agent_child_cost=round($child_price+$markupchild_cost);

                    $own_markupchild_cost=round($total_agent_child_cost*$own_markup)/100;
                    $total_child_cost=round($total_agent_child_cost+$own_markupchild_cost);



                    $infant_price=0;
                      $infant_price_details=unserialize($get_activities->infant_price_details);

                      if(!empty($infant_price_details))
                      {
                         $infant_price=$infant_price_details[0]['infant_pax_price'];
                      }
                      else
                      {
                        $infant_price=0;
                      }

                    $markupinfant_cost=round($infant_price*$markup)/100;
                    $total_agent_infant_cost=round($infant_price+$markupinfant_cost);

                    $own_markupinfant_cost=round($total_agent_infant_cost*$own_markup)/100;
                    $total_infant_cost=round($total_agent_infant_cost+$own_markupinfant_cost);



                    @endphp
                    {{$get_activities->activity_currency}}
                </span></span>
                <div class="a-price">
                    @if($total_adult_cost)
                    <span id="total_price_text">{{$total_adult_cost}} </span><span
                    class="info-s"
                    title="The initial price based on 1 adult">i</span>
                    @else

                    <span id="total_price_text">{{$total_child_cost}} </span><span
                    class="info-s"
                    title="The initial price based on 1 child">i</span>
                    @endif
                </div>
            </div>
            <form id="activity_booking_form" method="post"
            action="{{route('activity-booking')}}">
            {{csrf_field()}}
            <div class="book_card">
                <div class="booking_detail" style="padding:0 !important">
                    <div class="">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-7">
                              {{--    @php
                                    $no_of_bookings=0;
                                        $activity_date=session()->get('activity_date');
                                        $availability_qty_details=unserialize($get_activities->availability_qty_details);
                                        
                                        for($avail=0;$avail< count($availability_qty_details);$avail++)
                                        {
                                            if($activity_date>=$availability_qty_details[$avail]['availability_from'] && $activity_date<=$availability_qty_details[$avail]['availability_to'])
                                            {
                                                $no_of_bookings=$availability_qty_details[$avail]['no_of_bookings'];
                                            }
                                        }
                                    $no_of_bookings=$no_of_bookings-$past_bookings;

                                    @endphp --}}
                                     <input type="hidden" name="no_of_bookings" value="0">
                                    <input type="hidden" name="markup" value="{{$markup}}">
                                    <input type="hidden" name="own_markup" value="{{$own_markup}}">
                                    <input type="hidden" name="activity_id"
                                    value="{{$get_activities->activity_id}}">
                                    <input type="hidden" name="supplier_id"
                                    value="{{$get_activities->supplier_id}}">
                                    <input type="hidden" name="total_price" id="total_price"
                                    value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <div class="form-group">
                            <label for="select_date">SELECT DATE <span
                                class="asterisk">*</span></label>
                                <div class="input-group date">
                                    <input type="text" placeholder="FROM"
                                    class="form-control pull-right datepicker"
                                    id="select_date" name="select_date" readonly="readonly"
                                    value="{{session()->get('activity_date')}}" required="required">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>

                        @php
                        $time_slots=array();
                         $activity_date=session()->get('activity_date');
                                        $availability_qty_details=unserialize($get_activities->availability_qty_details);
                                        
                                        for($avail=0;$avail< count($availability_qty_details);$avail++)
                                        {
                                            if($activity_date>=$availability_qty_details[$avail]['availability_from'] && $activity_date<=$availability_qty_details[$avail]['availability_to'])
                                            {
                                                if(!empty($availability_qty_details[$avail]["availability_time_from"]))
                                                {
                                                $availability_time_from=$availability_qty_details[$avail]["availability_time_from"];   
                                                $availability_time_to=$availability_qty_details[$avail]["availability_time_to"];   
                                                }
                                                else
                                                {
                                                     $availability_time_from=array();  
                                                $availability_time_to=array();

                                                }
                                                
                                                
                                                for($time_count=0;$time_count< count($availability_time_from);$time_count++)
                                                {
                                                  $time_slots[$time_count]["from_time"]= $availability_time_from[$time_count];
                                                  $time_slots[$time_count]["to_time"]= $availability_time_to[$time_count];
                                                }
                                            }
                                        }
                        @endphp
                         <div class="">
                        <div class="form-group">
                            <label for="select_date">SELECT TIME SLOT <span
                                class="asterisk">*</span></label>
                                <div class="input-group">

                               <select name="select_time" id="select_time" class="form-control" required="required">
                                <option value="">SELECT TIME SLOT</option>
                                @foreach($time_slots as $slots)
                                <option value="{{$slots['from_time']}} - {{$slots['to_time']}}">{{$slots['from_time']}} - {{$slots['to_time']}}</option>
                                @endforeach
                               </select>

                           </div>
                            </div>
                        </div>
                        @if(count($time_slots)<=0)
                        <p style="background-color: #f3f3f3;padding: 10px">
                            No time slots available
                            </p>
                        @endif
                        <p id="no_of_bookings_section" style="display: none;padding: 10px;">  <i class="ti-ticket"></i> <span id="no_of_bookings_left" ></span> left</p>
                                
                         @php
                         $adult_age="";
                           $child_age="";
                           $infant_age="";
                           $age_group_details=unserialize($get_activities->age_group_details);
                            $adult_price_details=unserialize($get_activities->adult_price_details);
                            $child_price_details=unserialize($get_activities->child_price_details);
                            $infant_price_details=unserialize($get_activities->infant_price_details);
                           if(!empty($age_group_details))
                           {
                            $adult_age=$age_group_details['adults'];
                            $child_age=$age_group_details['child'];
                            $infant_age=$age_group_details['infant'];
                            }
                         @endphp 

                        @if(!empty($adult_age) && $adult_age['allowed']=="yes") 
                         <div class="">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="adult_count"><b>ADULT <span
                                            class="asterisk">*</span></b><br>
                                    @if($adult_age['min_age']!="" ||  $adult_age['max_age']!="")
                                           Age @if($adult_age['min_age']!=""){{$adult_age['min_age']}}@endif - @if($adult_age['max_age']!=""){{$adult_age['max_age']}}@endif
                                           @endif
                                          </label>
                                    </div>
                                    <div class="col-md-7">
                                        <input type="hidden" name="agent_adult_price" id="agent_adult_price"
                                        value="">
                                        <input type="hidden" name="adult_price" id="adult_price"
                                        value="">
                                        @php
                                        if(!empty($adult_price_details))
                                        {
                                            $min_adult=$adult_price_details[0]['adult_min_pax'];
                                        $max_adult=$adult_price_details[0]['adult_max_pax'];
                                         foreach($adult_price_details as $adult_price_i)
                                         {
                                            if($adult_price_i['adult_min_pax']<$min_adult)
                                            {
                                                $min_adult=$adult_price_i['adult_min_pax'];
                                            }
                                            if($adult_price_i['adult_max_pax']>$max_adult)
                                            {
                                                $max_adult=$adult_price_i['adult_max_pax'];
                                            }

                                         }

                                        }
                                        else
                                        {
                                         $min_adult=0;
                                         $max_adult=0; 
                                        }
                                        
                                        @endphp
                                        <select name="adult_count" id="adult_count" class="form-control" required="required">
                                        <option value="" selected="selected" hidden>SELECT ADULT</option>
                                        <option value="0">0</option>
                                        @php
                                        for($i=$min_adult;$i<=$max_adult;$i++) 
                                        { 
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                        @endphp
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                      <div class="">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            *<b>Adults</b> are not allowed
                        </div>
                    </div>
                </div>
            </div>

                    @endif


                    @if(!empty($child_age) && $child_age['allowed']=="yes") 
                         <div class="">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="child_count"><b>CHILD <span
                                            class="asterisk">*</span></b><br>
                                             @if($child_age['min_age']!="" ||  $child_age['max_age']!="")
                                           Age @if($child_age['min_age']!=""){{$child_age['min_age']}}@endif - @if($child_age['max_age']!=""){{$child_age['max_age']}}@endif
                                           @endif
                                       </label>
                                    </div>
                                    <div class="col-md-7">
                                         <input type="hidden" name="agent_child_price" id="agent_child_price" value="">
                                        <input type="hidden" name="child_price" id="child_price"
                                        value="">
                                        @php
                                         if(!empty($child_price_details))
                                        {
                                        $min_child=$child_price_details[0]['child_min_pax'];
                                        $max_child=$child_price_details[0]['child_max_pax'];
                                         foreach($child_price_details as $child_price_i)
                                         {  
                                            if($child_price_i['child_min_pax']<$min_child)
                                            {
                                                $min_child=$child_price_i['child_min_pax'];
                                            }
                                            if($child_price_i['child_max_pax']>$max_child)
                                            {
                                                $max_child=$child_price_i['child_max_pax'];
                                            }

                                         }
                                     }
                                     else
                                     {
                                         $min_child=0;
                                        $max_child=0;

                                     }
                                        @endphp
                                        <select name="child_count" id="child_count" class="form-control" required="required">
                                        <option value="" selected="selected" hidden>SELECT CHILD</option>
                                          <option value="0">0</option>
                                        @php
                                        for($i=$min_child;$i<=$max_child;$i++) 
                                        { 
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                        @endphp
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                      <div class="">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            *<b>Children</b> are not allowed
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if(!empty($infant_age) && $infant_age['allowed']=="yes") 
                         <div class="">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="child_count"><b>INFANT <span
                                            class="asterisk">*</span></b><br>
                                             @if($infant_age['min_age']!="" ||  $infant_age['max_age']!="")
                                           Age @if($infant_age['min_age']!=""){{$infant_age['min_age']}}@endif - @if($infant_age['max_age']!=""){{$infant_age['max_age']}}@endif
                                           @endif
                                       </label>
                                    </div>
                                    <div class="col-md-7">
                                         <input type="hidden" name="agent_infant_price" id="agent_infant_price" value="">
                                        <input type="hidden" name="infant_price" id="infant_price"
                                        value="">
                                          @php
                                           if(!empty($infant_price_details))
                                        {
                                        $min_infant=$infant_price_details[0]['infant_min_pax'];
                                        $max_infant=$infant_price_details[0]['infant_max_pax'];
                                         foreach($infant_price_details as $infant_price_i)
                                         {  
                                            if($infant_price_i['infant_min_pax']<$min_infant)
                                            {
                                                $min_infant=$infant_price_i['child_min_pax'];
                                            }
                                            if($infant_price_i['infant_max_pax']>$max_infant)
                                            {
                                                $max_infant=$infant_price_i['infant_max_pax'];
                                            }

                                         }
                                     }
                                     else
                                     {
                                        $min_infant=0;
                                        $max_infant=0;

                                     }
                                        @endphp
                                        <select name="infant_count" id="infant_count"
                                        class="form-control" required="required">
                                        <option value="" selected="selected" hidden>SELECT INFANT</option>
                                          <option value="0">0</option>
                                        @php
                                        for($i=$min_infant;$i<=$max_infant;$i++) 
                                        { 
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        }
                                        @endphp
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                      <div class="">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            *<b>Infants</b> are not allowed
                        </div>
                    </div>
                </div>
            </div>
            @endif



                        
        
<div class="hotel_detail">
    <p class="hotel_desc">Activity Description</p>
    <p class="para"><b>Country : </b>
        @foreach($countries as $country)
        @if($country->country_id==$get_activities->activity_country)
        {{$country->country_name}}
        @endif
        @endforeach
    </p>
    <p class="para"><b>City : </b>
        <?php
        $fetch_city=ServiceManagement::searchCities($get_activities->activity_city,$get_activities->activity_country);
        echo $fetch_city['name'];
        ?>
    </p>
    <p class="para"><b>Location :
    </b>{{$get_activities->activity_location}}</p>
</div>

<div class="book_btn">
    <button type="submit" id="activity_book_btn"
    class="btn btn-rounded btn-primary mr-10">BOOK</button>
</div>

</div>
</div>
</form>
</div>
</div>
</div>
{{-- start --}}
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@include('agent.includes.footer')
@include('agent.includes.bottom-footer')

<div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>

<script>
    $(document).ready(function()
    {
        $(".flexslider2").flexslider({animation: "slide",controlNav: "thumbnails"});
    // var date = new Date();
    // date.setDate(date.getDate());
    // $('#select_date').datepicker({
    //     autoclose:true,
    //     todayHighlight: true,
    //     format: 'yyyy-mm-dd',
    //     startDate:date
    // });
});
</script>
<script>
    //we start with splitting the provided string into an array
    var enabledates = "{{$dates}}";
    var enableDays = enabledates.split(', ')
    function formatDate(d) {
        var day = String(d.getDate())
    //add leading zero if day is is single digit
    if (day.length == 1)
        day = '0' + day
    var month = String((d.getMonth() + 1))
    //add leading zero if month is is single digit
    if (month.length == 1)
        month = '0' + month
    return d.getFullYear() + "-" + month + "-" + day;
}
$(function () {
    $("#select_date").datepicker({
        maxViewMode: 2,
        weekStart: 1,
        startDate: "+1d",
        beforeShowDay: function (date) {
            if (enableDays.indexOf(formatDate(date)) < 0)
                return {
                    enabled: false
                }
                else
                    return {
                        enabled: true
                    }
                },
                todayHighlight: true,
                format: "yyyy-mm-dd",
                clearBtn: true,
                autoclose: true
            })
});
</script>
<script>
     function calculatePrice()
    {
          var child_count = $("#child_count").val();
        var adult_count = $("#adult_count").val();
        var infant_count = $("#infant_count").val();
        var child_price = $("#child_price").val();
        var adult_price = $("#adult_price").val();
        var infant_price = $("#infant_price").val();

        if (!child_count) {
            child_count = 0;
        }
        if (!adult_count) {
            adult_count = 0;
        }
        if (!infant_count) {
            infant_count = 0;
        }

        if (!child_price) {
            child_price = 0;
        }
        if (!adult_price) {
            adult_price = 0;
        }
        if (!infant_price) {
            infant_price = 0;
        }
        var child_final_price = parseInt(child_price * child_count);
        var adult_final_price = parseInt(adult_price * adult_count);
        var infant_final_price = parseInt(infant_price * infant_count);
        var total_price = parseInt(child_final_price + adult_final_price + infant_final_price);
        $("#total_price_text").text(total_price);
        $("#total_price").val(total_price);
    }


    $(document).on("change", "#child_count,#adult_count,#infant_count", function (event) {

        var activity_id=$("input[name='activity_id']").val();
        var check=0;
        $("#loaderModal").modal("show");
            if(event.target.id=="adult_count")
            {
                var pax_qty=$("#adult_count").val();
                

                 $.ajax({
                        url: "{{route('fetchActivitiesPricing')}}",
                        data: {
                            "pax_type": "adult",
                            "pax_qty": pax_qty,
                            "activity_id":activity_id
                        },
                        type: "GET",
                        dataType:"JSON",
                        success: function (response) {
                             $("#loaderModal").modal("hide");
                             $("#agent_adult_price").val(response.cost_a);
                            $("#adult_price").val(response.cost_t);

                            calculatePrice();

                           
                        }

                    });
            
            }
            else if(event.target.id=="child_count")
            {
                var pax_qty=$("#child_count").val();
                 $.ajax({
                        url: "{{route('fetchActivitiesPricing')}}",
                        data: {
                            "pax_type": "child",
                            "pax_qty": pax_qty,
                            "activity_id":activity_id
                        },
                        type: "GET",
                        dataType:"JSON",
                        success: function (response) {
                             $("#loaderModal").modal("hide");
                             $("#agent_child_price").val(response.cost_a);
                            $("#child_price").val(response.cost_t);
                             calculatePrice();
                        }

                    });

            }
            else
            {
                var pax_qty=$("#child_count").val();
                 $.ajax({
                        url: "{{route('fetchActivitiesPricing')}}",
                        data: {
                            "pax_type": "infant",
                            "pax_qty": pax_qty,
                            "activity_id":activity_id
                        },
                        type: "GET",
                        dataType:"JSON",
                        success: function (response) {
                             $("#loaderModal").modal("hide");
                             $("#agent_infant_price").val(response.cost_a);
                            $("#infant_price").val(response.cost_t);
                             calculatePrice();
                        }

                    });
            }
           
      
    });


</script>
<script>
    $(document).on("submit", "#activity_booking_form", function (e) {
        var select_date = $("#select_date").val();

        var no_of_bookings=$("input[name='no_of_bookings']").val();

        var adult_count=parseInt($("select[name='adult_count']").val()) || 0;

         var child_count=parseInt($("select[name='child_count']").val()) || 0;

         var infant_count=parseInt($("select[name='infant_count']").val()) || 0;


         var total_count=parseInt(adult_count+child_count+infant_count);

         if(no_of_bookings<total_count)
         {
            e.preventDefault();
            alert("You cannot book more than "+no_of_bookings+" tickets");
         }

        if (select_date == "") {
            e.preventDefault();
            $("#select_date").css("border", "1px solid #cf3c63");
        } else
        {
            $("#select_date").css("border", "1px solid #9e9e9e");
        }

        if(total_count==0)
        {
            alert("Please select pax");
            e.preventDefault();
        }
    });


    $(document).on("change","#select_time",function()
    {
         var activity_id=$("input[name='activity_id']").val();
         var select_time=$(this).val();
         var select_date=$("#select_date").val();

         if(select_time!="")
         {
           $("#loaderModal").modal("show");
          $.ajax({
                        url: "{{route('fetchActivitiesAvailability')}}",
                        data: {
                            "activity_time_slot": select_time,
                            "activity_date": select_date,
                            "activity_id":activity_id
                        },
                        type: "GET",
                        success: function (response) {
                             $("#loaderModal").modal("hide");
                             $("#no_of_bookings").val(response);
                             if(response>0)
                             {
                                $("#no_of_bookings_section").show();
                                $("#no_of_bookings_left").text(response);
                                $("input[name='no_of_bookings']").val(response);
                             }
                             else
                             {
                                $("#no_of_bookings_section").hide();
                             }
                           
                        }

                    }); 
         }
         else{
            $("#no_of_bookings_section").hide();
         }

              
    });
</script>