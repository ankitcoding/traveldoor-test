@include('agent.includes.top-header')
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('agent.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('agent.includes.nav')

                <div class="content">



                    <div class="content-header">

                        <div class="d-flex align-items-center">

                            <div class="mr-auto">

                                <h3 class="page-title">Markup</h3>

                                <div class="d-inline-block align-items-center">

                                    <nav>

                                        <ol class="breadcrumb">

                                            <li class="breadcrumb-item"><a href="#"><i
                                                class="mdi mdi-home-outline"></i></a></li>

                                                <li class="breadcrumb-item active" aria-current="page">Home</li>

                                            </ol>

                                        </nav>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="container">
                            <div class="row">
                                <div class="box">
                                    <div class="box-body" style="padding-left:0;padding-right:0">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="box" style="box-shadow: none">
                                                    <div class="box-body">
                                                        <p><strong>Enter your services markup for your customer here : </strong></p>
                                                        <form action="" id="agent_markup_form">
                                                            {{csrf_field()}}

                                                            <div class="row">
                                                                @php
                                                                $hotel_markup="";
                                                                $activity_markup="";
                                                                $transportation_markup="";
                                                                $guide_markup="";
                                                                 $driver_markup="";
                                                                $sightseeing_markup="";
                                                                $itinerary_markup="";
                                                                $transfer_markup="";
                                                                $restaurant_markup="";


                                                                //if($fetch_agent['agent_created_by']!="")
                                                                //{
                                                                    $get_services=explode("///",$fetch_agent['agent_own_service_markup']);
                                                                    
                                                                    for($services=0;$services< count($get_services);$services++)
                                                                    {
                                                                        if($get_services[$services]!="")
                                                                        {
                                                                           $get_services_individual=explode("---",$get_services[$services]);
                                                                           $service_name=$get_services_individual[0]."_markup";
                                                                           $$service_name=$get_services_individual[1];


                                                                       }
                                                                   }
                                                               //}
                                                               

                                                               @endphp


                                                               <div class="col-sm-12 col-md-6 mt-20">
                                                                <div class="form-group">
                                                                    <input type='hidden' name='service_name[]' value='hotel'>
                                                                    <label for="">{{ strtoupper('Hotel')}}</label>
                                                                    <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$hotel_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-12 col-md-6 mt-20">
                                                                <div class="form-group">
                                                                 <input type='hidden' name='service_name[]' value='activity'>
                                                                 <label for="">{{ strtoupper('Activity')}}</label>
                                                                 <input type='text' class='form-control' name='service_cost[]' value="{{$activity_markup}}" placeholder="Markup in %" onkeypress='javascript:return validateNumber(event)'>
                                                             </div>
                                                         </div>
                                                         <div class="col-sm-12 col-md-6 mt-20">
                                                            <div class="form-group">
                                                                <input type='hidden' name='service_name[]' value='guide'>
                                                                <label for="">{{ strtoupper('Guide')}}</label>
                                                                <input type='text' class='form-control' name='service_cost[]' value="{{$guide_markup}}"placeholder="Markup in %" onkeypress='javascript:return validateNumber(event)'>
                                                            </div>
                                                        </div>
                                                         <div class="col-sm-12 col-md-6 mt-20">
                                                            <div class="form-group">
                                                                <input type='hidden' name='service_name[]' value='driver'>
                                                                <label for="">{{ strtoupper('Driver')}}</label>
                                                                <input type='text' class='form-control' name='service_cost[]' value="{{$driver_markup}}"placeholder="Markup in %" onkeypress='javascript:return validateNumber(event)'>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-12 col-md-6 mt-20">
                                                            <div class="form-group">
                                                               <input type='hidden' name='service_name[]' value='sightseeing'>
                                                               <label for="">{{ strtoupper('Sightseeing')}}</label>
                                                               <input type='text' class='form-control' name='service_cost[]' value="{{$sightseeing_markup}}"placeholder="Markup in %" onkeypress='javascript:return validateNumber(event)'>
                                                           </div>
                                                       </div>
                                                       <div class="col-sm-12 col-md-6 mt-20">
                                                        <div class="form-group">
                                                           <input type='hidden' name='service_name[]' value='itinerary'>
                                                           <label for="">{{ strtoupper('Itinerary')}}</label>
                                                           <input type='text' class='form-control' name='service_cost[]' value="{{$itinerary_markup}}" placeholder="Markup in %" onkeypress='javascript:return validateNumber(event)'>
                                                       </div>
                                                   </div>
                                                   <div class="col-sm-12 col-md-6 mt-20">
                                                    <div class="form-group">
                                                       <input type='hidden' name='service_name[]' value='transfer'>
                                                       <label for="">{{ strtoupper('Transfer')}}</label>
                                                       <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$transfer_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                   </div>
                                               </div>
                                               <div class="col-sm-12 col-md-6 mt-20">
                                                    <div class="form-group">
                                                       <input type='hidden' name='service_name[]' value='restaurant'>
                                                       <label for="">{{ strtoupper('Restaurant')}}</label>
                                                       <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$restaurant_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                   </div>
                                               </div>
                                           </div>
                                           
                                           

                                           <div class="col-sm-12 col-md-6 col-lg-3">

                                            <div class="form-group">
                                                <br>
                                                <button type="button" id="submit_markup"
                                                class="btn btn-rounded  btn-primary">Update</button>

                                            </div>


                                        </div>
                                    </form>
                                    
                                    


                                </div>

                            </div>
                        </div>
                    </div>
                    
                </div>


            </div>
        </div>
    </div>
</div>

</div>

</div>

</div>



@include('agent.includes.footer')

@include('agent.includes.bottom-footer')

<script>

    $(document).on("click","#submit_markup",function()
    {
       $("#submit_markup").prop("disabled",true);
       var formData=$("#agent_markup_form").serialize();

       $.ajax({
        url:"{{route('agent-markup-update')}}",
        type:"POST",
        data:formData,
        success:function(response)
        {
            if(response.indexOf("success")!=-1)
            {
                swal({title:"Success",text:"Agent Markup Updated Successfully !",type: "success"});
                location.reload();
            }
            else if(response.indexOf("fail")!=-1)
            {
                swal("ERROR", "Agent cannot be updated right now! ");
            }

            $("#submit_markup").prop("disabled",false);


        }
    });


   });

    
</script>

</body>

</html>
