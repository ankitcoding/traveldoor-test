@include('agent.includes.top-header')

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('agent.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('agent.includes.nav')

                <div class="content">



                    <div class="content-header">

                        <div class="d-flex align-items-center">

                            <div class="mr-auto">

                                <h3 class="page-title">Restaurant Itinerary</h3>

                                <div class="d-inline-block align-items-center">

                                    <nav>

                                        <ol class="breadcrumb">

                                            <li class="breadcrumb-item"><a href="#"><i
                                                        class="mdi mdi-home-outline"></i></a></li>

                                            <li class="breadcrumb-item" aria-current="page">Home </li>

                                            <li class="breadcrumb-item active" aria-current="page">Restaurant Itinerary

                                            </li>

                                        </ol>

                                    </nav>

                                </div>

                            </div>

                                </div>
                            </div>
                             <div class="row">
        <div class="col-12">
            <form id="itinerary_form" action="{{route('restaurant-itinerary-details')}}" method="post">
                {{ csrf_field() }}
            <div class="box">
                <div class="box-body">
                      <div class="row mb-10">
                            <div class="col-sm-12 col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="restaurant_country">COUNTRY <span class="asterisk">*</span></label>
                                    <select id="restaurant_country" name="restaurant_country" class="form-control select2" style="width: 100%;">
                                        <option selected="selected">SELECT COUNTRY</option>
                                        @foreach($countries as $country)
                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <label for="restaurant_city">CITY <span class="asterisk">*</span></label>
                                    <select id="restaurant_city" name="restaurant_city" class="form-control select2" style="width: 100%;">
                                        <option selected="selected">SELECT CITY</option>
                                    </select>
                                </div>
                            </div>
                         
                            <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <label for="select_date">START DATE<span class="asterisk">*</span></label>
                                    <div class="input-group date">
                                        <input type="text" placeholder="DATE"
                                        class="form-control pull-right datepicker" id="select_date" name="select_date" readonly="readonly">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <!-- /.input group -->

                                </div>
                            </div>
                         <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <label for="restaurant_days">NO. OF DAYS<span class="asterisk">*</span></label>
                                    <select id="no_of_days" name="no_of_days" class="form-control" style="width: 100%;" required="required">
                                        <option value="">SELECT NO. OF DAYS</option>
                                        @for($i=1;$i<=20;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                         

                            
                         <div class="col-sm-12 col-md-12 col-lg-3">

                                <div class="form-group">
                                    <br>
                                    <button type="submit" class="btn btn-rounded  btn-success">Create Itinerary</button>

                                </div>
                                <!-- /.input group -->

                            </div>
                        </div>
                </div>
            </div>
        </form>
    </div>
</div>
 </div>

    </div>

    </div>

</div>
    @include('agent.includes.footer')
    @include('agent.includes.bottom-footer') 
    <script>
          $(document).ready(function()
    {
        $('.select2').select2();
        var date = new Date();
        date.setDate(date.getDate());
        $('#select_date').datepicker({
            autoclose:true,
            todayHighlight: true,
            format: 'yyyy-mm-dd',
            startDate:date
        });
       $(document).on("change","#restaurant_country",function()
        {
         if($("#restaurant_country").val()!="0")
         {
            var country_id=$(this).val();
            $.ajax({
                url:"{{route('search-country-cities')}}",
                type:"GET",
                data:{"country_id":country_id},
                success:function(response)
                {

                    $("#restaurant_city").html(response);
                    $('#restaurant_city').select2();

                }
            });
        }

    });
    });
    </script>   

</body>
    </html>
