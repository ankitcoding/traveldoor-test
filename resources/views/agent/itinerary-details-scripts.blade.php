 <div class="modal" id="vehicleModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Vehicle Images</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;" id="vehicle_view_images">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div>
  <div class="modal" id="guideModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Guide Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height:600px;overflow-y: auto;overflow-x: hidden;" id="guide_details_div">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div>
        <div class="modal" id="driverModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Driver Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height:600px;overflow-y: auto;overflow-x: hidden;" id="driver_details_div">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div>
<div class="modal" id="selectHotelModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Hotel</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="height: 480px;
      overflow-y: auto;
      overflow-x: hidden;">

      <input type="hidden" id="hotel_day_index">
      <input type="hidden" id="hotel_offset">

      <div id="first_step_hotel">
      <div class="row">
        <div class="col-md-5">
            <label for="hotel_rating_filter"><b>Star Rating</b></label>
            <select name="hotel_rating_filter" id="hotel_rating_filter" class="form-control">
                <option value="">Any</option>
                <?php
                for($i=5;$i>=1;$i--)
                {
                    echo '<option value="'.$i.'">'.$i.' Star</option>';
                }
                ?>
            </select>
        </div>
        <div class="col-md-5">
            <label for="hotel_sort_filter"><b>Sort By</b></label>
            <select name="hotel_sort_filter" id="hotel_sort_filter" class="form-control">
                <option value="">Sort By</option>
                 <option value="rating_low_high">Hotel Rating: Low to High</option>
                <option value="rating_high_low">Hotel Rating: High to Low</option>
                <option value="price_low_high">Price: Low to High</option>
                <option value="price_high_low">Price: High to Low</option>
            </select>
        </div>
    </div>
    <!--  <div class="row">
         <div class="col-md-5">
            <label for="hotel_name_filter"><b>Hotel Name</b></label>
           <input name="hotel_name_filter" id="hotel_name_filter" class="form-control" placeholder="Hotel Name">
        </div>
         <div class="col-md-5">
            <label for="hotel_location_filter"><b>Hotel Location</b></label>
           <input name="hotel_location_filter" id="hotel_location_filter" class="form-control" placeholder="Hotel Location">
        </div>
      </div> -->
      <br>
         <div id="hotels_div">
         </div>
             <button type="button" class="btn btn-primary more_hotel" id="show_more_hotel" style=" margin: 5% 0%;width:100%;display: none !important;">Show More Hotels</button>
            <button type="button" class="btn btn-primary more_hotel" id="no_more_hotel"  style=" margin: 5% 0%; width:100%;display: none !important;" disabled="disabled">No More Hotels</button>
     </div>
       <div id="second_step_hotel">
        <button id="go_back_hotel_btn" class="btn btn-default"><i class="fa fa-angle-left go-icon"></i> Go back to Hotel List</button>
        <div id="hotels_details_div">
        </div>
       </div>
     </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" id="include_hotel" class="btn btn-danger">Include</button>
      </div>

    </div>
  </div>
</div>

    <div class="modal" id="selectTransferModal">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Transfer</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="height: 550px;
        overflow-y: auto;
    overflow-x: hidden;">
     <input type="hidden" id="transfer_day_index">
      <div class="row">
        <div class="col-md-5">
            <label for="transfer_name_filter"><b>Vehicle Name</b></label>
           <input name="transfer_name_filter" id="transfer_name_filter" class="form-control" placeholder="Type Vehicle Name">
        </div>
        <div class="col-md-5">
            <label for="transfer_sort_filter"><b>Sort By</b></label>
            <select name="transfer_sort_filter" id="transfer_sort_filter" class="form-control">
                <option value="">Sort By</option>
                <option value="price_low_high">Price: Low to High</option>
                <option value="price_high_low">Price: High to Low</option>
            </select>
        </div>
    </div>
      <br>
         <div id="transfer_div">
         </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
 <div class="modal" id="selectTransferGuideModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Select Transfer Guide</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;">

                        <input type="hidden" id="transfer_guide_day_index">
                       <div id="transfer_guide_div">
                           
                       </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <!--   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                        <button type="button" class="btn btn-primary" id="include_transfer_guide">Include</button>
                    </div>

                </div>
            </div>
        </div>

<div class="modal" id="selectActivityModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Activity</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="height: 550px;
        overflow-y: auto;
    overflow-x: hidden;">
     <input type="hidden" id="activities_day_index">
       <input type="hidden" id="activities_day_index_self">
      <div class="row">
        <div class="col-md-5">
            <label for="activity_name_filter"><b>Activity Name</b></label>
           <input name="activity_name_filter" id="activity_name_filter" class="form-control" placeholder="Type Activity Name">
        </div>
        <div class="col-md-5">
            <label for="activities_sort_filter"><b>Sort By</b></label>
            <select name="activities_sort_filter" id="activities_sort_filter" class="form-control">
                <option value="">Sort By</option>
                <option value="price_low_high">Price: Low to High</option>
                <option value="price_high_low">Price: High to Low</option>
            </select>
        </div>
    </div>
      <br>
         <div id="activities_div">
         </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
<div class="modal" id="selectSightseeingModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Sightseeing</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="height: 550px;
        overflow-y: auto;
    overflow-x: hidden;">
     <input type="hidden" id="sightseeing_day_index">
     <input type="hidden" id="sightseeing_offset">
      <div class="row" id="sightseeing_filter">
        <div class="col-md-5">
            <label for="sightseeing_name_filter"><b>Sightseeing Name</b></label>
           <input name="sightseeing_name_filter" id="sightseeing_name_filter" class="form-control" placeholder="Sightseeing Name">
        </div>
        <div class="col-md-5">
            <label for="sightseeing_sort_filter"><b>Sort By</b></label>
            <select name="sightseeing_sort_filter" id="sightseeing_sort_filter" class="form-control">
                <option value="">Sort By</option>
                <option value="price_low_high">Price: Low to High</option>
                <option value="price_high_low">Price: High to Low</option>
            </select>
        </div>
    </div>
      <br>
       <div id="sightseeing_div">
       </div>
       <div id="sightseeing_div_buttons">
           <button type="button" class="btn btn-primary more_sightseeing" id="show_more_sightseeing" style=" margin: 5% 0%;width:100%;display: none !important;">Show More Sightseeing</button>
            <button type="button" class="btn btn-primary more_sightseeing" id="no_more_sightseeing"  style=" margin: 5% 0%; width:100%;display: none !important;" disabled="disabled">No More Sightseeing</button>
       </div>
       
        <div id="sightseeing_details_div">
             <button id="go_back_sightseeing_btn" class="btn btn-default"><i class="fa fa-angle-left go-icon"></i> Go back to Sightseeing List</button>
                            <div class="row">
                                <div class="col-12">
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="sightseeing_details_id" id="sightseeing_details_id">
                                                        <select class="form-control type-select" id="sightseeing_details_tour_type">
                                                            <option value="private" selected="selected">PRIVATE TOUR</option>
                                                            <option value="group">GROUP TOUR</option>                                                   
                                                        </select>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12">

                                                            <p class="para-h" id="sightseeing_details_name">Tbilisi - Mtskheta -Tbilisi</p>
                                                
                                                        </div>
                                                        <div class="col-md-6 col-sm-12">
                                                            <input type="hidden" name="sightseeing_adult_cost" id="sightseeing_adult_cost" value="">
                                                            <input type="hidden" name="sightseeing_group_cost" id="sightseeing_group_cost" value="">
                                                             <input type="hidden" name="sightseeing_total_cost" id="sightseeing_total_cost" value="">
                                                              <input type="hidden" name="selected_vehicle_type_id" id="selected_vehicle_type_id" value="0">
                                                               <input type="hidden" name="selected_vehicle_type_name"  id="selected_vehicle_type_name" value="">
                                                            <input type="hidden" name="selected_guide_id"  id="selected_guide_id" value="0">
                                                                <input type="hidden" name="selected_guide_name"  id="selected_guide_name" value="">
                                                             <input type="hidden" name="selected_guide_cost" id="selected_guide_cost" value="0">
                                                             <input type="hidden" name="selected_driver_id" id="selected_driver_id" value="0">
                                                                <input type="hidden" name="selected_driver_name"  id="selected_driver_name" value="">
                                                             <input type="hidden" name="selected_driver_cost" id="selected_driver_cost" value="0">
                                                              <input type="hidden" name="sightseeing_additional_cost" id="sightseeing_additional_cost" value="0">
                                                              <input type="hidden" name="sightseeing_cities" id="sightseeing_cities" value="">
                                                                <input type="hidden"  id="selected_sightseeing_detail_link" value="">
                                                            <p class="price">Total Price
                                                        
                                                                <span id="sightseeing_total_price_text">GEL 94</span></p>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                                <div class="col-md-3">
                                                    <p class="address" id="sightseeing_details_address" ><i class="fa fa-map-marker"></i> (
                                                        Tbilisi-Mtskheta )
                                                    </p>
                                                </div>
                                                <div class="col-md-3">
                                                    <p class="address" id="sightseeing_details_distance">
                                                        <i class="fa fa-road" ></i> 51 KMS</p>


                                                </div>
                                                <div class="col-md-3">
                                                    <p class="address" id="sightseeing_details_duration">
                                                        <i class="fa fa-clock-o" ></i> 8 HOURS</p>

                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="box">
                                                                <div class="box-body" style="padding:0">
                                                                    <div class="grid-images" id="sightseeing_detail_images">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="section" id="">
                                                                <p class="star-p"> <i class="fa fa-star star"></i>
                                                                    Sightseeing Description
                                                                </p>
                                                                <div id="sightseeing_detail_desc">
                                                            
                                                            </div>


                                                            </div>
                                                            <div class="section">
                                                                <p class="star-p"> <i class="fa fa-star star"></i>
                                                                    Sightseeing Attractions
                                                                </p>
                                                                  <div id="sightseeing_detail_attract">

                                                            </div>
                                                            </div>
                                                            <hr style="border-color: #ffc0c0;width: 100%;">
                                                        </div>
                                                    </div>
                                                    <div class="row" style="margin-top:20px" id="vehicle_type_div">

                                                     @foreach($get_vehicles as $vehicles)
                                                     <!--  <i class="fa fa-user"></i> -->
                                                     <div class="col-md-4 text-center vehicle-type parent-tile" style="cursor: pointer">
                                                        <div class="box box-body bg-dark pull-up" @if($vehicles->vehicle_type_image!="")
                                        style="background: url({{ asset('assets/uploads/vehicle_type_images') }}/{{$vehicles->vehicle_type_image}});background-size: contain;background-repeat: no-repeat;" 
                                        @else 
                                        style="background: url({{asset('assets/images/vehicle-type.jpg')}});background-size: contain;background-repeat: no-repeat;" @endif>
                                                            <div class="overlay"></div>
                                                            <br>
                                                            <p class="font-size-26 text-center color-tiles-text"> <a href="#" class="details text-light" >
                                                              {{$vehicles->vehicle_type_name}}
                                                              <input type="hidden" id="vehcile_type_name_{{$vehicles->vehicle_type_id}}" name="vehcile_type_name_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_name}}"></a></p>
                                                              <br>
                                                          </div>
                                                          <input type="radio" class="with-gap radio-col-primary vehcile_type_show" name="vehcile_type" id="vehcile_type_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_id}}" data-minvalue="{{$vehicles->vehicle_type_min}}" data-maxvalue="{{$vehicles->vehicle_type_max}}">
                                                          <label for="vehcile_type_{{$vehicles->vehicle_type_id}}" class="tile-label"></label>
                                                      </div>

                                                      @endforeach           

                                                    </div>
                                                    <hr>
                                                    <br>
                                                    <p class="star-p" id="driver_div_head"> <i
                                                            class="fa fa-star star"></i> Select Driver for Sightseeing
                                                    </p>
                                                    <div class="col-md-12" id="driver_div">
                                                    </div>
                                                    <br>
                                                    <p class="star-p" id="guide_div_head"> <i
                                                            class="fa fa-star star"></i> Select Guide for Sightseeing
                                                    </p>
                                                    <div class="col-md-12" id="guide_div">
                                                    </div>
                                                    <div class="text-center table-sightseeing-loader"
                                                        style="display: none">
                                                        <svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg"
                                                            xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                            viewBox="0 0 100 100" enable-background="new 0 0 0 0"
                                                            xml:space="preserve">
                                                            <circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6"
                                                                transform="translate(0 -3.75126)">
                                                                <animateTransform attributeName="transform" dur="1s"
                                                                    type="translate" values="0 15 ; 0 -15; 0 15"
                                                                    repeatCount="indefinite" begin="0.1">
                                                                </animateTransform>
                                                            </circle>
                                                            <circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6"
                                                                transform="translate(0 1.49916)">
                                                                <animateTransform attributeName="transform" dur="1s"
                                                                    type="translate" values="0 10 ; 0 -10; 0 10"
                                                                    repeatCount="indefinite" begin="0.2">
                                                                </animateTransform>
                                                            </circle>
                                                            <circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6"
                                                                transform="translate(0 2.74958)">
                                                                <animateTransform attributeName="transform" dur="1s"
                                                                    type="translate" values="0 5 ; 0 -5; 0 5"
                                                                    repeatCount="indefinite" begin="0.3">
                                                                </animateTransform>
                                                            </circle>
                                                        </svg>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" id="include_sightseeing">Include</button>
     <!--    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
      </div>

    </div>
  </div>
</div>


<div class="modal" id="selectRestaurantModal" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-xl">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Select Restaurant</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" style="height: 550px;
        overflow-y: auto;
    overflow-x: hidden;">
     <input type="hidden" id="restaurant_day_index">
      <input type="hidden" id="restaurant_day_index_self">
     <input type="hidden" id="restaurant_offset">
      <div class="row" id="restaurant_filter">
        <div class="col-md-5">
            <label for="restaurant_name_filter"><b>Restaurant Name</b></label>
           <input name="restaurant_name_filter" id="restaurant_name_filter" class="form-control" placeholder="Restaurant Name">
        </div>
    </div>
      <br>
       <div id="restaurant_div">
       </div>
       <div id="restaurant_div_buttons">
           <button type="button" class="btn btn-primary more_restaurant" id="show_more_restaurant" style=" margin: 5% 0%;width:100%;display: none !important;">Show More Restaurants</button>
            <button type="button" class="btn btn-primary more_restaurant" id="no_more_restaurant"  style=" margin: 5% 0%; width:100%;display: none !important;" disabled="disabled">No More Restaurants</button>
       </div>
       
        <div id="restaurant_details_div">
             <button id="go_back_restaurant_btn" class="btn btn-default"><i class="fa fa-angle-left go-icon"></i> Go back to Restaurant List</button>
                            <div class="row">
                                <div class="col-12">
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <input type="hidden" name="restaurant_details_id" id="restaurant_details_id">
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12">
                                                            <p class="para-h" id="restaurant_details_name">Restaurant Name</p>
                                                            <small id="restaurant_details_type"></small>
                                                            <div class="row">
                                                                <div class="col-md-7">
                                                                <p class="address" id="restaurant_details_address" ><i class="fa fa-map-marker"></i></p>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <p class="address" id="restaurant_details_timing">
                                                                        <i class="fa fa-clock-o" ></i></p>
                                                                    </div>      
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-sm-12">
                                                                 <input type="hidden"  id="selected_restaurant_address" value="">
                                                                <input type="hidden"  id="selected_restaurant_detail_link" value="">
                                                                <input type="hidden"  id="selected_restaurant_total_price" value="0">
                                                                 <input type="hidden"  id="selected_restaurant_food_items" value="">
                                                                <p class="price">Total Price
                                                                    <span id="restaurant_total_price_text">GEL 0</span></p>
                                                                </div>
                                                                <div class="col-md-7 col-sm-12">
                                                                </div>
                                                                <div class="col-md-5 col-sm-12" id="restaurant_food_select_info_head">
                                                                    <h4><strong>Selected Food /  Drink Items :</strong></h4>
                                                                    <div id="restaurant_food_select_info">

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="box">
                                                                <div class="box-body" style="padding:0">
                                                                    <div class="grid-images" id="restaurant_detail_images">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12">
                                                            <div class="section" id="">
                                                                <p class="star-p"> <i class="fa fa-star star"></i>
                                                                    Restaurant Description
                                                                </p>
                                                                <div id="restaurant_detail_desc">
                                                            
                                                            </div>


                                                            </div>
                                                            <hr style="border-color: #ffc0c0;width: 100%;">
                                                        </div>
                                                    </div>

                                                    <hr>
                                                    <br>
                                                    <p class="star-p" id="restaurant_menu_div_head"> <i
                                                            class="fa fa-star star"></i> Restaurant Menu
                                                    </p>
                                                    <div class="col-md-12" id="restaurant_menu_div">
                                                    </div>
                                                    <br>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-7 text-right">
             <label for="food_for" class="control-label"><b>Food / Drink For :</b></label>
          </div>
          <div class="col-md-3">
        <select name="food_for" id="food_for" class="form-control">
          <option value="">Select Food For</option>
          <option value="lunch">Lunch</option>
          <option value="dinner">Dinner</option>
        </select>
      </div>
      <div class="col-md-2">
         <button type="button" class="btn btn-danger" id="include_restaurant">Include</button>
      </div>
      </div>
       
     <!--    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
      </div>

    </div>
  </div>
</div>

<div class="modal" id="saveItineraryModal">
    <div class="modal-dialog">
      <div class="modal-content">
          <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Enter Customer Details</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

        <!-- Modal body -->
        <div class="modal-body">

            <div class="row"  style="padding: 10px;">
              <p><strong>Enter your Customer Details</strong></p>
              <form id="customer_details_form">
              <div class="row">
               <div class="col-md-4">
                <label for="save_name">Customer Name</label>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                 <input type="text" id="customer_name" name="customer_name" class="form-control">
             </div>
         </div>


         <div class="col-md-4">
            <label for="save_name">Customer Contact</label>
        </div>
        <div class="col-md-8">
            <div class="form-group">
             <input type="text" id="customer_contact" name="customer_contact" class="form-control">
         </div>
     </div>


     <div class="col-md-4">
         <label for="save_name">Customer Email</label> 
     </div>
     <div class="col-md-8">
       <div class="form-group">
         <input type="text" id="customer_email" name="customer_email" class="form-control">
     </div>
 </div>


 <div class="col-md-4">
  <button type="button" id="save_itinerary_btn" class="btn btn-primary btn-sm">Save Iitneray</button>
</div>

</div>
</form>

</div>
</div>
  
       
      </div>
    </div>
  </div>

<div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>


    <script>
    $(document).on("click",".change_hotel",function()
    {
         $("#show_more_hotel").hide();
                $("#no_more_hotel").hide();
                $("#hotel_offset").val(0);
        var id=this.id;
        var hotel_id=id.split("__")[1];
        var change_room=false;
        var hotel_actual_id=0; 
        var offset=$("#hotel_offset").val();
        hotel_id=hotel_id.split("_");
       if(hotel_id[1] != undefined)
       {
         hotel_actual_id=hotel_id[0];
        hotel_id=hotel_id[1];
        change_room=true;
       
       }
       else
       {
        hotel_id=hotel_id[0];
       }
        var no_of_days=$("#no_of_days").val();
        var country_id=$("#days_country__"+hotel_id).val();
        var city_id=$("#days_city__"+hotel_id).val();
        var hotel_checkin=$("#hotel_checkin__"+hotel_id).val()
        var hotel_current_id=$("#hotel_id__"+hotel_id).val();
        if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show();
            $.ajax({
            url:"{{route('agent-itinerary-get-hotels')}}",
            data:{"country_id":country_id,
                    "city_id":city_id,
                    "hotel_checkin":hotel_checkin,
                    "hotel_current_id":hotel_current_id,
                    "offset":offset},
            type:"GET",
           
            success:function(response)
            {
               $("#loaderModal").hide();
                $("#hotels_div").html(response);
                $("#hotel_day_index").val(hotel_id);
                 $("#first_step_hotel").show();
                 $("#hotel_sort_filter").val("");
                 $("#hotel_rating_filter").val("");
              $("#second_step_hotel").hide();
            $("#selectHotelModal").modal("show");
             $("#hotel_offset").val(parseInt(parseInt($("#hotel_offset").val())+1));
             if(response.indexOf("select_hotel")!=-1)
                {
                   $("#show_more_hotel").show();
                   $("#no_more_hotel").hide();

               }
               else
               {
                $("#show_more_hotel").hide();
                $("#no_more_hotel").show();

            }
            if(change_room==true)
            {
                $("#hotel__"+hotel_actual_id).trigger("click");
            }
    
            }
        });
        
        }


        
    });

    $(document).on("click","#show_more_hotel",function()
    {
        var hotel_id=$("#hotel_day_index").val();
        var offset=$("#hotel_offset").val();
        var no_of_days=$("#no_of_days").val();
        var country_id=$("#days_country__"+hotel_id).val();
        var city_id=$("#days_city__"+hotel_id).val();
        var hotel_checkin=$("#hotel_checkin__"+hotel_id).val()
        var hotel_current_id=$("#hotel_id__"+hotel_id).val();
        if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show();
            $.ajax({
            url:"{{route('agent-itinerary-get-hotels')}}",
            data:{"country_id":country_id,
                    "city_id":city_id,
                    "hotel_checkin":hotel_checkin,
                    "hotel_current_id":hotel_current_id,
                    "offset":offset},
            type:"GET",
           
            success:function(response)
            {
               $("#loaderModal").hide();
                $("#hotel_sort_div").append(response);
                 $("#first_step_hotel").show();
                 $("#hotel_sort_filter").val("");
                 $("#hotel_rating_filter").val("");
              $("#second_step_hotel").hide();
            $("#selectHotelModal").modal("show");
             $("#hotel_offset").val(parseInt(parseInt($("#hotel_offset").val())+1));
        
                if(response.indexOf("select_hotel")!=-1)
                {
                   $("#show_more_hotel").show();
                   $("#no_more_hotel").hide();

               }
               else
               {
                $("#show_more_hotel").hide();
                $("#no_more_hotel").show();

            }
    
            }
        });
        
        }


        
    });


    $(document).on("click",".change_sightseeing,.add_sightseeing",function()
    {
          $("#sightseeing_filter").show();
                        $("#sightseeing_details_div").hide();
                         $("#sightseeing_div_buttons").hide();
                $("#sightseeing_offset").val(0);
        var id=this.id;
        var sightseeing_id=id.split("__")[1];
        var sightseeing_current_id=$("#sightseeing_id__"+sightseeing_id).val();

        var country_id=$("#days_country__"+sightseeing_id).val();
        var city_id=$("#days_city__"+sightseeing_id).val();
        var offset=$("#sightseeing_offset").val();
         if(parseInt(sightseeing_id)-1==0)
                {
                   var prev_city_id = "";  
                }
                else
                {
                     var prev_city_id = $("#days_city__" + parseInt(sightseeing_id-1)).val();  
                }
    if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show()
        $.ajax({
            url:"{{route('agent-itinerary-get-sightseeing')}}",
            data:{"sightseeing_current_id":sightseeing_current_id,
                "country_id":country_id,
                    "city_id":city_id,
                    "prev_city_id":prev_city_id,
                    "offset":offset},
            type:"get",
            success:function(response)
            {
                $("#loaderModal").hide()
                 $("#sightseeing_div").show();
                $("#sightseeing_div").html(response);
                $("#sightseeing_day_index").val(sightseeing_id);
                $("#sightseeing_sort_filter").val("");
                 $("#sightseeing_name_filter").val("");
            $("#selectSightseeingModal").modal("show");
               $("#sightseeing_offset").val(parseInt(parseInt($("#sightseeing_offset").val())+1));
             if(response.indexOf("select_sightseeing")!=-1)
                {
                    $("#sightseeing_div_buttons").show();
                   $("#show_more_sightseeing").show();
                   $("#no_more_sightseeing").hide();

               }
               else
               {
                $("#sightseeing_div_buttons").show();
                $("#show_more_sightseeing").hide();
                $("#no_more_sightseeing").show();

            }

            document.querySelector("div#selectSightseeingModal .modal-content .modal-body").scrollTop=0
            }
        });
        }
        
    });

     $(document).on("click","#show_more_sightseeing",function()
    {
          $("#sightseeing_filter").show();
                        $("#sightseeing_details_div").hide();
        
        var sightseeing_id=$("#sightseeing_day_index").val();
        var sightseeing_current_id=$("#sightseeing_id__"+sightseeing_id).val();

        var country_id=$("#days_country__"+sightseeing_id).val();
        var city_id=$("#days_city__"+sightseeing_id).val();
        var offset=$("#sightseeing_offset").val();
         if(parseInt(sightseeing_id)-1==0)
                {
                   var prev_city_id = "";  
                }
                else
                {
                     var prev_city_id = $("#days_city__" + parseInt(sightseeing_id-1)).val();  
                }
    if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show()
        $.ajax({
            url:"{{route('agent-itinerary-get-sightseeing')}}",
            data:{"sightseeing_current_id":sightseeing_current_id,
                "country_id":country_id,
                    "city_id":city_id,
                    "prev_city_id":prev_city_id,
                    "offset":offset},
            type:"get",
            success:function(response)
            {
                $("#loaderModal").hide()
                 $("#sightseeing_div").show();
                 document.querySelector("div#selectSightseeingModal .modal-content .modal-body").scrollTop=500
                $("#sightseeing_sort_div").append(response);
                $("#sightseeing_day_index").val(sightseeing_id);
                $("#sightseeing_sort_filter").val("");
                 $("#sightseeing_name_filter").val("");
            $("#selectSightseeingModal").modal("show");
               $("#sightseeing_offset").val(parseInt(parseInt($("#sightseeing_offset").val())+1));
             if(response.indexOf("select_sightseeing")!=-1)
                {
                    $("#sightseeing_div_buttons").show();
                   $("#show_more_sightseeing").show();
                   $("#no_more_sightseeing").hide();

               }
               else
               {
                $("#sightseeing_div_buttons").show();
                $("#show_more_sightseeing").hide();
                $("#no_more_sightseeing").show();

            }

            // document.querySelector("div#selectSightseeingModal .modal-content .modal-body").scrollTop=0
            }
        });
        }
        
    });

    $(document).on("click",".change_activity,.add_activity",function(e)
    {
        var id=this.id;
        if(e.target.id.indexOf("add_activity")!=-1)
        {
         var new_id=id.split("__")[1];
        var activity_prev_id=$(".activity_select__"+new_id).last().attr("id");
           var activity_indiv_prev_id=$(".activity_indiv_select__"+new_id).last().attr("id");

        var check_activity_id=$("#"+activity_prev_id).find(".activity_id").val();

        if(check_activity_id=="")
        {
        activity_id=activity_prev_id.split("__")[1];
        var activity_index=activity_prev_id.split("__")[2]; 

        }
        else
        {
             activity_id=activity_prev_id.split("__")[1];
             var activity_index=activity_prev_id.split("__")[2]; 
            var clone_activity=$("#"+activity_prev_id).clone();
            var old_index=activity_index;
            var new_index=parseInt(old_index)+1;

        clone_activity.attr("id","activity_select__"+activity_id+"__"+new_index).css("display","none");
        clone_activity.find(".activity_id").attr("id","activity_id__"+activity_id+"__"+new_index).val("");
        clone_activity.find(".activity_name").attr("id","activity_name__"+activity_id+"__"+new_index).val("");
          clone_activity.find(".activity_cost").attr("id","activity_cost__"+activity_id+"__"+new_index).val(0);
           clone_activity.find(".change_activity").attr("id","change_activity__"+activity_id+"_"+new_index);
            clone_activity.find(".remove_activity").attr("id","remove_activity__"+activity_id+"__"+new_index);
            clone_activity.find(".show_activity").attr({"id":"show_activity__"+activity_id+"__"+new_index,"href":""});
               clone_activity.find(".activities_pax_div").attr("id","activity_pax_div__"+activity_id+"__"+new_index);
                clone_activity.find(".select_activities_adults").attr("id","activities_select_adults__"+activity_id+"__"+new_index);
                clone_activity.find(".select_activities_child").attr("id","activities_select_child__"+activity_id+"__"+new_index);
                clone_activity.find(".activities_child_age_div").attr("id","activities_child_age_div__"+activity_id+"__"+new_index);

            $("#"+activity_prev_id).after(clone_activity);

              var clone_indiv_activity=$("#"+activity_indiv_prev_id).clone();
            var old_indiv_index=activity_index;
            var new_indiv_index=parseInt(old_indiv_index)+1;

        clone_indiv_activity.attr("id","activity_indiv_select__"+activity_id+"__"+new_indiv_index).css("display","none");
           clone_indiv_activity.find(".change_activity").attr("id","change_indiv_activity__"+activity_id+"_"+new_indiv_index);
            clone_indiv_activity.find(".remove_activity").attr("id","remove_indiv_activity__"+activity_id+"__"+new_indiv_index);
            clone_indiv_activity.find(".show_activity").attr({"id":"show_indiv_activity__"+activity_id+"__"+new_indiv_index,"href":""});
            $("#"+activity_indiv_prev_id).after(clone_indiv_activity);

            activity_index=new_index;

        }

        }
        else
        {
        var activity_id=id.split("__")[1].split("_")[0];
        var activity_index=id.split("__")[1].split("_")[1]; 
        }
       

        var activity_date=$("#days_dates__"+activity_id).val();
        var activity_current_id=$("#activity_id__"+activity_id+"__"+activity_index).val();
        var country_id=$("#days_country__"+activity_id).val();
        var city_id=$("#sightseeing_cities__"+activity_id).val();
            if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show()
        $.ajax({
            url:"{{route('agent-itinerary-get-activities')}}",
            data:{"country_id":country_id,
                    "city_id":city_id,
                    "activity_date":activity_date,
                    "activity_current_id":activity_current_id},
            type:"get",
            success:function(response)
            {
                $("#loaderModal").hide()
                $("#activities_div").html(response);
                $("#activities_day_index").val(activity_id);
                 $("#activities_day_index_self").val(activity_index);
                 $("#activities_sort_filter").val("");
                 $("#activity_name_filter").val("");
                $("#selectActivityModal").modal("show");
            }
        });
        }
        
    });

$(document).on("click",".change_restaurant,.add_restaurant",function(e)
    {
        $("#restaurant_details_div,#restaurant_div_buttons").hide()
        var id=this.id;
        if(e.target.id.indexOf("add_restaurant")!=-1)
        {
         var new_id=id.split("__")[1];
        var restaurant_prev_id=$(".restaurant_select__"+new_id).last().attr("id");
           var restaurant_indiv_prev_id=$(".restaurant_indiv_select__"+new_id).last().attr("id");

        var check_restaurant_id=$("#"+restaurant_prev_id).find(".restaurant_id").val();

        if(check_restaurant_id=="")
        {
        restaurant_id=restaurant_prev_id.split("__")[1];
        var restaurant_index=restaurant_prev_id.split("__")[2]; 

        }
        else
        {
             restaurant_id=restaurant_prev_id.split("__")[1];
             var restaurant_index=restaurant_prev_id.split("__")[2]; 
            var clone_restaurant=$("#"+restaurant_prev_id).clone();
            var old_index=restaurant_index;
            var new_index=parseInt(old_index)+1;

        clone_restaurant.attr("id","restaurant_select__"+restaurant_id+"__"+new_index).css("display","none");
        clone_restaurant.find(".restaurant_id").attr("id","restaurant_id__"+restaurant_id+"__"+new_index).val("");
        clone_restaurant.find(".restaurant_name").attr("id","restaurant_name__"+restaurant_id+"__"+new_index).val("");
          clone_restaurant.find(".restaurant_cost").attr("id","restaurant_cost__"+restaurant_id+"__"+new_index).val(0);
            clone_restaurant.find(".restaurant_food_detail").attr("id","restaurant_food_detail__"+restaurant_id+"__"+new_index);
           clone_restaurant.find(".change_restaurant").attr("id","change_restaurant__"+restaurant_id+"_"+new_index);
            clone_restaurant.find(".remove_restaurant").attr("id","remove_restaurant__"+restaurant_id+"__"+new_index);
            clone_restaurant.find(".show_restaurant").attr({"id":"show_restaurant__"+restaurant_id+"__"+new_index,"href":""});
               // clone_restaurant.find(".restaurant_pax_div").attr("id","restaurant_pax_div__"+restaurant_id+"__"+new_index);
               //  clone_restaurant.find(".select_restaurant_adults").attr("id","restaurant_select_adults__"+restaurant_id+"__"+new_index);
               //  clone_restaurant.find(".select_restaurant_child").attr("id","restaurant_select_child__"+restaurant_id+"__"+new_index);
               //  clone_restaurant.find(".restaurant_child_age_div").attr("id","restaurant_child_age_div__"+restaurant_id+"__"+new_index);

            $("#"+restaurant_prev_id).after(clone_restaurant);

              var clone_indiv_restaurant=$("#"+restaurant_indiv_prev_id).clone();
            var old_indiv_index=restaurant_index;
            var new_indiv_index=parseInt(old_indiv_index)+1;

        clone_indiv_restaurant.attr("id","restaurant_indiv_select__"+restaurant_id+"__"+new_indiv_index).css("display","none");
           clone_indiv_restaurant.find(".change_restaurant").attr("id","change_indiv_restaurant__"+restaurant_id+"_"+new_indiv_index);
            clone_indiv_restaurant.find(".remove_restaurant").attr("id","remove_indiv_restaurant__"+restaurant_id+"__"+new_indiv_index);
            clone_indiv_restaurant.find(".show_restaurant").attr({"id":"show_indiv_restaurant__"+restaurant_id+"__"+new_indiv_index,"href":""});
            $("#"+restaurant_indiv_prev_id).after(clone_indiv_restaurant);

            restaurant_index=new_index;

        }

        }
        else
        {
        var restaurant_id=id.split("__")[1].split("_")[0];
        var restaurant_index=id.split("__")[1].split("_")[1]; 
        }
       

        var restaurant_date=$("#days_dates__"+restaurant_id).val();
        var restaurant_current_id=$("#restaurant_id__"+restaurant_id+"__"+restaurant_index).val();
        var country_id=$("#days_country__"+restaurant_id).val();
        var city_id=$("#days_city__"+restaurant_id).val();
        city_id+=","+$("#sightseeing_cities__"+restaurant_id).val();
            if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show()
        $.ajax({
            url:"{{route('agent-itinerary-get-restaurant')}}",
            data:{"country_id":country_id,
                    "city_id":city_id,
                    "restaurant_date":restaurant_date,
                    "restaurant_current_id":restaurant_current_id},
            type:"get",
            success:function(response)
            {
                $("#loaderModal").hide()
                $("#restaurant_div").html(response).show();
                $("#restaurant_day_index").val(restaurant_id);
                 $("#restaurant_day_index_self").val(restaurant_index);
                 $("#restaurant_filter").show()
                 $("#restaurant_sort_filter").val("");
                 $("#restaurant_name_filter").val("");
                $("#selectRestaurantModal").modal("show");
            }
        });
        }
        
    });

     $(document).on("click",".change_transfer",function()
    {
        var id=this.id;
        var transfer_id=id.split("__")[1].split("_")[0];
        var transfer_index=id.split("__")[1].split("_")[1];

        var transfer_type=$("#transfer_type__"+transfer_id).val();
        var transfer_date=$("#days_dates__"+transfer_id).val();
        var country_id=$("#days_country__"+transfer_id).val();
        var city_id=$("#days_city__"+transfer_id).val();
        if(country_id==null)
        {
            alert("Please select country first");
        }
        else if(city_id==0)
        {
            alert("Please select city first");
        }
        else
        {
            $("#loaderModal").show();
            if (transfer_type == "from-airport") {
                $("#transfer_div").html(
                                "<div class='jumbotron'><h3 class='text-center' style='margin: 60px;'><b>Searching....</b></h3></div>"
                                );
                            var from_airport = $("#transfer_from_airport__"+transfer_id).val();
                            var to_city = $("#transfer_to_city__"+transfer_id).val();
                            if (from_airport == 0) {
                                alert("Please select airport");
                            } else if (to_city == 0) {
                                alert("Please select city");
                            } else {
                                $.ajax({
                                    url: "{{route('agent-itinerary-get-transfer')}}",
                                    data: {
                                        "country_id": country_id,
                                        "city_id": city_id,
                                        "transfer_type": transfer_type,
                                        "from_airport": from_airport,
                                        "to_city": to_city,
                                    },
                                    type: "get",
                                    success: function (response) {
                                        $("#transfer_div").html(response);
                                    }
                                });
                            }
                        } else if (transfer_type == "to-airport") {
                            $("#transfer_div").html(
                                "<div class='jumbotron'><h3 class='text-center' style='margin: 60px;'><b>Searching....</b></h3></div>"
                                );

                            var from_city = $("#transfer_from_city__"+transfer_id).val();
                            var to_airport = $("#transfer_to_airport__"+transfer_id).val();
                            if (from_city == 0) {
                                alert("Please select city");
                            } else if (to_airport == 0) {
                                alert("Please select airport");
                            } else {
                                $.ajax({
                                    url: "{{route('agent-itinerary-get-transfer')}}",
                                    data: {
                                        "country_id": country_id,
                                        "city_id": city_id,
                                        "transfer_type": transfer_type,
                                        "from_city": from_city,
                                        "to_airport": to_airport
                                    },
                                    type: "get",
                                    success: function (response) {
                                        $("#transfer_div").html(response);
                                    }
                                });
                            }

                        } else {
                            $("#transfer_div").html(
                                "<div class='jumbotron'><h3 class='text-center' style='margin: 60px;'><b>Searching....</b></h3></div>"
                                );

                            var from_city = $("#transfer_from_city__"+transfer_id).val();
                            var to_city = $("#transfer_to_city__"+transfer_id).val();
                            if (from_city == 0) {
                                alert("Please select from city");
                            } else if (to_city == 0) {
                                alert("Please select to city");
                            } else {
                                $.ajax({
                                    url: "{{route('agent-itinerary-get-transfer')}}",
                                    data: {
                                        "country_id": country_id,
                                        "city_id": city_id,
                                        "transfer_type": transfer_type,
                                        "from_city": from_city,
                                        "to_city": to_city
                                    },
                                    type: "get",
                                    success: function (response) {
                                        $("#transfer_div").html(response);
                                    }
                                });
                            }

                        }
        // $.ajax({
        //     url:"{{route('agent-itinerary-get-transfer')}}",
        //     data:{"country_id":country_id,
        //             "city_id":city_id,
        //             "transfer_date":transfer_date},
        //     type:"get",
        //     success:function(response)
        //     {
                $("#loaderModal").hide();
                // $("#transfer_div").html(response);
                $("#transfer_day_index").val(transfer_id);
                 $("#transfer_sort_filter").val("");
                 $("#transfer_name_filter").val("");
                $("#selectTransferModal").modal("show");
        //     }
        // });

        }
        
    });
</script>
<script>
    // $(document).on("click",".select_hotel",function(e)
    // {
    //     $(".select_hotel").each(function()
    //     {
    //         $(this).find('.tick').css("display","none");
    //         $(this).removeClass('selected_hotel');

    //     });
    //     var hotel_day_index=$("#hotel_day_index").val();

    //         var hotel_id=$(this).attr('id').split("__")[1];
    //         var hotel_name=$(this).find('.card-title').text();
    //         $("#hotel_id__"+hotel_day_index).val("");
    //         $("#hotel_name__"+hotel_day_index).val("");
    //         $("#room_name__"+hotel_day_index).val("");
    //         $("#hotel_cost__"+hotel_day_index).val("");
    //         if($("input[name='room_type_"+hotel_id+"']:checked").val())
    //         {
                
    //             $(".select_hotel").each(function()
    //             {
    //                 var id=this.id;
    //                 var hotel_select_id=$(this).attr('id').split("__")[1];

    //                 if(id!="hotel__"+hotel_id)
    //                 {
    //                     $("input[name='room_type_"+hotel_select_id+"']").prop("checked",false);
    //                 }

    //             });

    //         $(this).find('.tick').css("display","block");

    //         $(this).addClass('selected_hotel');
    //         var room_index=$("input[name='room_type_"+hotel_id+"']:checked").val();
    
    //         var hotel_address=$(this).find('.search_hotel_address').val();
    //         var hotel_image=$(this).find('.search_hotel_image').val();
    //         var hotel_rating=$(this).find('.search_hotel_rating').val();

    //         var room_name=$('input[name="room_name_'+room_index+'_'+hotel_id+'"]').val();
    //         var hotel_cost=$('input[name="room_price_'+room_index+'_'+hotel_id+'"]').val();

    //         var days_country=$("#days_country__"+hotel_day_index).val();
    //         var hotel_no_of_days=$("#hotel_no_of_days__"+hotel_day_index).val();
    //         hotel_cost=Math.round(hotel_cost*hotel_no_of_days);
    //         $("#hotel_id__"+hotel_day_index).val(hotel_id);
    //         $("#hotel_name__"+hotel_day_index).val(hotel_name);
    //         $("#room_name__"+hotel_day_index).val(room_name);
    //         $("#hotel_cost__"+hotel_day_index).val(hotel_cost);

    //         var hotel_rating_html="";
    //         for($i=1;$i<hotel_rating;$i++)
    //         {
    //             hotel_rating_html+="<span class='fa fa-star checked'></span>";
    //         }


    //         $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-name').text(hotel_name);
    //          $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-name').text(hotel_name);
    //           $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-address').html('<i class="fa fa-map-marker"></i> '+hotel_address);
    //          $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-address').html('<i class="fa fa-map-marker"></i>'+hotel_address);
    //            $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-image').attr("src",hotel_image);
    //          $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-image').attr("src",hotel_image);
    //           $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-room-type').text(room_name);
    //          $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-room-type').text(room_name);
    //            $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-rating').html(hotel_rating_html);
    //          $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-rating').html(hotel_rating_html);

    //         var total_cost=0;
    //         $(".calc_cost").each(function()
    //         {
               
    //             if($(this).val()!="")
    //             {
    //                 total_cost+=Math.round($(this).val());
    //             }

    //         });

    //         var markup=$("#markup_per").val();
    //         var markup_cost=Math.round((total_cost*markup)/100);
    //          $("#total_cost_w_markup").val(total_cost);
    //         total_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

    //         $("#total_cost").val(total_cost);
    //          $("#total_cost_text").text(total_cost);
    //     }
    //     else
    //     {
    //         alert(" Please select room type");
    //     }

    // });

    $(document).on("click","#go_back_hotel_btn",function()
    {
        $("#second_step_hotel").hide(500);
        $("#first_step_hotel").show(200);
         
    });
      $(document).on("click","#go_back_sightseeing_btn",function()
    {
        $("#sightseeing_details_div").hide(500);
        $("#sightseeing_div").show(200);
        $("#sightseeing_div_buttons").show(200);
         $("#sightseeing_filter").show();
         document.querySelector("div#selectSightseeingModal .modal-content .modal-body").scrollTop=0
         
    });
        $(document).on("click","#go_back_restaurant_btn",function()
    {
        $("#restaurant_details_div").hide(500);
        $("#restaurant_div").show(200);
        $("#restaurant_div_buttons").show(200);
         $("#restaurant_filter").show();
         document.querySelector("div#selectRestauarantModal .modal-content .modal-body").scrollTop=0
         
    });
    $(document).on("click",".select_hotel",function(e)
    {
        $("#loaderModal").modal("show");
        var hotel_index=$("#hotel_day_index").val();
        var hotel_id=$(this).attr("id").split("__")[1];
        var hotel_checkin=$("#hotel_checkin__"+hotel_index).val();
        var hotel_checkout=$("#hotel_checkout__"+hotel_index).val();
         var room_occupancy_ids=[];
         var room_quantity_array=[];
         $("#hotel_room_detail__"+hotel_index).find('.hotel_occupancy_id').each(function()
         {
            room_occupancy_ids.push($(this).val())
         });
          $("#hotel_room_detail__"+hotel_index).find('.hotel_room_qty').each(function()
         {
            room_quantity_array.push($(this).val())
         });
        var room_count=$(".rooms_count").length;
          $.ajax({
            url:"{{route('agent-itinerary-get-hotels-details')}}",
            data:{"hotel_id":hotel_id,
                "no_of_rooms":room_count,
                "hotel_checkin":hotel_checkin,
                "hotel_checkout":hotel_checkout,
                "room_occupancy_ids":room_occupancy_ids,
                 "room_quantity_array":room_quantity_array,
            },
            type:"get",
            success:function(response)
            {
              $("#first_step_hotel").hide(100);
              $("#second_step_hotel").show(400);

              $("#hotels_details_div").html(response);
                $("#loaderModal").modal("hide");
        
            }
        });
    
    });

    $(document).on("click",".room_type_change1",function()
    {
            var hotel_day_index=$("#hotel_day_index").val();
            var hotel_id=$("#hotels_details_div").find('.search_hotel_id').val();
            var hotel_name=$("#hotels_details_div").find('.search_hotel_name').val();
            $("#hotel_id__"+hotel_day_index).val("");
            $("#hotel_name__"+hotel_day_index).val("");
            $("#room_name__"+hotel_day_index).val("");
            $("#hotel_cost__"+hotel_day_index).val("");
            var room_index=$(this).attr('id').split("_")[1];
            var hotel_address=$("#hotels_details_div").find('.search_hotel_address').val();
            var hotel_image=$("#hotels_details_div").find('.search_hotel_image').val();
            var hotel_rating=$("#hotels_details_div").find('.search_hotel_rating').val();

            var room_name=$('input[name="room_name_'+room_index+'_'+hotel_id+'"]').val();
            var hotel_cost=$('input[name="room_price_'+room_index+'_'+hotel_id+'"]').val();
            var days_country=$("#days_country__"+hotel_day_index).val();
            var hotel_no_of_days=$("#hotel_no_of_days__"+hotel_day_index).val();
            hotel_cost=Math.round(hotel_cost*hotel_no_of_days);
            $("#hotel_id__"+hotel_day_index).val(hotel_id);
            $("#hotel_name__"+hotel_day_index).val(hotel_name);
            $("#room_name__"+hotel_day_index).val(room_name);
            $("#hotel_cost__"+hotel_day_index).val(hotel_cost);

            var hotel_rating_html="";
            for($i=1;$i<=hotel_rating;$i++)
            {
                hotel_rating_html+="<span class='fa fa-star checked'></span>";
            }


            $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-name').text(hotel_name);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-name').text(hotel_name);
              $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-address').html('<i class="fa fa-map-marker"></i> '+hotel_address);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-address').html('<i class="fa fa-map-marker"></i>'+hotel_address);
               $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-image').attr("src",hotel_image);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-image').attr("src",hotel_image);
              $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-room-type').text(room_name);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-room-type').text(room_name);
               $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-rating').html(hotel_rating_html);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-rating').html(hotel_rating_html);

            // var total_cost=0;
            // $(".calc_cost").each(function()
            // {
               
            //     if($(this).val()!="")
            //     {
            //         total_cost+=Math.round($(this).val());
            //     }

            // });

            // var markup=$("#markup_per").val();
            // var own_markup=$("#own_markup_per").val();
            // var markup_cost=Math.round((total_cost*markup)/100);
            //  $("#total_cost_w_markup").val(total_cost);
            // var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

            // $("#total_cost_w_agent_markup").val(total_agent_cost);

            // var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
            // total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

            // $("#total_cost").val(total_cost);


            //  $("#total_cost_text").text(total_cost);
             $("#calculate_cost").trigger("click");
                 $("#selectHotelModal").modal("hide");
    });

     $(document).on("click","#include_hotel",function()
    {
            var room_count=$(".rooms_count").length;
            var hotel_day_index=$("#hotel_day_index").val();
            var hotel_id=$("#hotels_details_div").find('.search_hotel_id').val();
            var hotel_name=$("#hotels_details_div").find('.search_hotel_name').val();
         

            var room_actual_count=0;
            $(".room_selected_qty").each(function()
            {
                if(!$(this).find(':selected').prop('disabled'))
                {
                  if($(this).val()>0)
                {
                    room_actual_count+=parseInt($(this).val());
                }   
                }
                else
                {
                    swal("Please re-check your room selection. As one or more room selection is not allowed to proceed.");
                    return false;
                }
               
            });

            if(room_actual_count==0)
            {
                alert("Please select atleast 1 room/rooms");
                return false;
            }
            // else if(room_actual_count>room_count)
            // {
            //     alert("Total room quantity cannot be greater than "+room_count+" rooms");
            // }
            // else if(room_actual_count<room_count)
            // {
            //     alert("Total room quantity cannot be lesser than "+room_count+" rooms");
            // }
            // else
            // {

             $("#hotel_id__"+hotel_day_index).val("");
             $("#hotel_name__"+hotel_day_index).val("");
             $("#room_name__"+hotel_day_index).val("");
             $("#hotel_cost__"+hotel_day_index).val("");

            var hotel_address=$("#hotels_details_div").find('.search_hotel_address').val();
            var hotel_image=$("#hotels_details_div").find('.search_hotel_image').val();
            var hotel_rating=$("#hotels_details_div").find('.search_hotel_rating').val();
            var hotel_detail_link=$("#hotels_details_div").find('.search_hotel_detail_link').val();
             var hotel_rating_html="";
            for($i=1;$i<=hotel_rating;$i++)
            {
                hotel_rating_html+="<span class='fa fa-star checked'></span>";
            }

            var room_all_names=[];
            var hotel_room_detail_html="";
            var hotel_total_cost=0;
            var total_rooms=0;
            $(".room_selected_qty").each(function()
            {
                if($(this).val()>0)
                {
                    var rooms_qty=$(this).val();
                    var room_index=$(this).attr('id').split("__")[1];
                    var occupancy_index=$(this).attr('id').split("__")[2];
                     var room_id=$('input[name="room_id__'+room_index+'__'+hotel_id+'"]').val();
                      var occupancy_id=$('input[name="room_occupancy_id__'+room_index+'__'+occupancy_index+'__'+hotel_id+'"]').val();
                       var occupancy_qty    =$('input[name="room_occupancy_qty__'+room_index+'__'+occupancy_index+'__'+hotel_id+'"]').val();
                    var room_name=$('input[name="room_name__'+room_index+'__'+hotel_id+'"]').val();
                    var hotel_cost=$('input[name="room_price__'+room_index+'__'+occupancy_index+'__'+hotel_id+'"]').val();
                    var hotel_no_of_days=$("#hotel_no_of_days__"+hotel_day_index).val();
                    hotel_cost=Math.round(hotel_cost*hotel_no_of_days);

                    total_rooms+=parseInt(rooms_qty);


                    room_all_names.push(rooms_qty+"-"+room_name+"-("+occupancy_qty+" Occupancy)");
                    $("#hotel_id__"+hotel_day_index).val(hotel_id);
                    $("#hotel_name__"+hotel_day_index).val(hotel_name);

                    hotel_total_cost+=parseInt(hotel_cost);
                    hotel_room_detail_html+='<input type="hidden" class="hotel_room_name" name="hotel_room_name['+(hotel_day_index-1)+'][]" value="'+room_name+'"><input type="hidden" class="hotel_room_qty" name="hotel_room_qty['+(hotel_day_index-1)+'][]" value="'+rooms_qty+'"><input type="hidden" class="hotel_room_cost" name="hotel_room_cost['+(hotel_day_index-1)+'][]" value="'+hotel_cost+'"><input type="hidden" class="hotel_room_id" name="hotel_room_id['+(hotel_day_index-1)+'][]" value="'+room_id+'"><input type="hidden" class="hotel_occupancy_id" name="hotel_occupancy_id['+(hotel_day_index-1)+'][]" value="'+occupancy_id+'"><input type="hidden" class="hotel_occupancy_qty" name="hotel_occupancy_qty['+(hotel_day_index-1)+'][]" value="'+occupancy_qty+'">';
                }

            });

                   $("#room_name__"+hotel_day_index).val(room_all_names.join(", "));
                   $("#room_qty__"+hotel_day_index).val(total_rooms);
                    $("#hotel_cost__"+hotel_day_index).val(hotel_total_cost);
                     if ($("#hotel_warning__"+hotel_day_index).length)
                    {
                       $("#warning_count").val(parseInt($("#warning_count").val())-1);
                        $("#hotel_warning__"+hotel_day_index).html("");
                    }
          
                   

                   $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-name').text(hotel_name);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-name').text(hotel_name);
              $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-address').html('<i class="fa fa-map-marker"></i> '+hotel_address);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-address').html('<i class="fa fa-map-marker"></i>'+hotel_address);
               $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-image').attr("src",hotel_image);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-image').attr("src",hotel_image);
             var room_all_names_text=room_all_names.join(", ");
              $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-room-type').text(room_all_names_text.replace(/-/g, " "));
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-room-type').text(room_all_names_text.replace(/-/g, " "));
               $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-rating').html(hotel_rating_html);
             $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-hotel-indiv-rating').html(hotel_rating_html);
            $("#hotel_room_detail__"+hotel_day_index).html(hotel_room_detail_html);
            $("#hotels_select__"+hotel_day_index).find('.cstm-hotel-name').text(hotel_name);
            $("#hotels_select__"+hotel_day_index).find('.cstm-change-hotel').attr("id","change_hotel__"+hotel_id+"_"+hotel_day_index);
            $("#hotels_indiv_select__"+hotel_day_index).find('.cstm-change-hotel-indiv').attr("id","change_hotel__"+hotel_id+"_"+hotel_day_index);

            $("#show_hotel__"+hotel_day_index+",#show_indiv_hotel__"+hotel_day_index).attr("href",hotel_detail_link);




            // var total_cost=0;
            // $(".calc_cost").each(function()
            // {
               
            //     if($(this).val()!="")
            //     {
            //         total_cost+=Math.round($(this).val());
            //     }

            // });

            //  var markup=$("#markup_per").val();
            // var own_markup=$("#own_markup_per").val();
            // var markup_cost=Math.round((total_cost*markup)/100);
            //  $("#total_cost_w_markup").val(total_cost);
            // var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

            // $("#total_cost_w_agent_markup").val(total_agent_cost);

            // var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
            // total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

            // $("#total_cost").val(total_cost);


            //  $("#total_cost_text").text(total_cost);

             $("#calculate_cost").trigger("click");
            $("#selectHotelModal").modal("hide");
            // }   
    });

    
    


    $(document).on("click",".select_sightseeing1",function()
    {
        $(".select_sightseeing").each(function()
        {
            $(this).find('.tick').css("display","none");
            $(this).removeClass('selected_sightseeing');

        });

        
            $(this).find('.tick').css("display","block");
            $(this).addClass('selected_sightseeing');

            var sightseeing_day_index=$("#sightseeing_day_index").val();

            var sightseeing_id=$(this).attr('id').split("__")[1];

            var sightseeing_name=$(this).find('.card-title').text();
            var sightseeing_cost=$(this).find('.search_sightseeing_cost').val();
            var sightseeing_address=$(this).find('.search_sightseeing_address').val();
            var sightseeing_image=$(this).find('.search_sightseeing_image').val();
            var sightseeing_distance=$(this).find('.search_sightseeing_distance').val();
            var sightseeing_attractions=$(this).find('.search_sightseeing_attractions').val();


            $("#sightseeing_id__"+sightseeing_day_index).val(sightseeing_id);
            $("#selected_sightseeing_name__"+sightseeing_day_index).text(sightseeing_name);
            $("#sightseeing_name__"+sightseeing_day_index).val(sightseeing_name);
            $("#sightseeing_cost__"+sightseeing_day_index).val(sightseeing_cost);


              $("#sightseeing_select__"+sightseeing_day_index).find('.cstm-sightseeing-name').text(sightseeing_name+" ("+sightseeing_distance+"KMS)");
              $("#days_count"+sightseeing_day_index).find('.days').text("DAY "+sightseeing_day_index+" - "+sightseeing_name);
              $("#days_header"+sightseeing_day_index).html("Day "+sightseeing_day_index+" - "+sightseeing_name+" <span class='fa fa-angle-down text-right'></span>");
               $("#days_count"+sightseeing_day_index).find('.des').html(sightseeing_attractions);
             $("#sightseeing_indiv_select__"+sightseeing_day_index).find('.cstm-sightseeing-indiv-name').text(sightseeing_name+" ("+sightseeing_distance+"KMS)");
              $("#sightseeing_select__"+sightseeing_day_index).find('.cstm-sightseeing-address').html('<i class="fa fa-map-marker"></i> '+sightseeing_address);
             $("#sightseeing_indiv_select__"+sightseeing_day_index).find('.cstm-sightseeing-indiv-address').html('<i class="fa fa-map-marker"></i>'+sightseeing_address);
            $("#sightseeing_select__"+sightseeing_day_index).find('.cstm-sightseeing-image').attr("src",sightseeing_image);
             $("#sightseeing_indiv_select__"+sightseeing_day_index).find('.cstm-sightseeing-indiv-image').attr("src",sightseeing_image);

            //   var total_cost=0;
            // $(".calc_cost").each(function()
            // {
            //     if($(this).val()!="")
            //     {
            //         total_cost+=parseInt($(this).val());
            //     }

            // });

            //    var markup=$("#markup_per").val();
            // var own_markup=$("#own_markup_per").val();
            // var markup_cost=Math.round((total_cost*markup)/100);
            //  $("#total_cost_w_markup").val(total_cost);
            // var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

            // $("#total_cost_w_agent_markup").val(total_agent_cost);

            // var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
            // total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

            // $("#total_cost").val(total_cost);


            //  $("#total_cost_text").text(total_cost);
             $("#calculate_cost").trigger("click");
             $("#selectSightseeingModal").modal("hide");

    });

     $(document).on("click", "#include_sightseeing", function (e) {
 var error=0;
         var sightseeing_tour_type = $("#sightseeing_details_tour_type").val();
         if(sightseeing_tour_type=="private")
                {
                   
                if (!$("input[name='vehcile_type']:checked").val()) {
                    e.preventDefault();
                    alert("Please Select Vehicle Type first");
                    error++;
                } else

                {
                    $("#vehcile_type_table").css("border", "1px solid #9e9e9e");
                     if ($("input[name='selected_guide_id']").val()==0) {
                    e.preventDefault();
                    alert("Please Select Guide first");
                      error++;
                } else

                {
                    $("#guide_div").css("border", "1px solid #9e9e9e");
                }

                if ($("input[name='selected_driver_id']").val()==0) {
                    e.preventDefault();
                    $("#driver_div").css("border", "1px solid #cf3c63");
                    alert("Please Select Driver first");
                      error++;
                } else

                {
                    $("#driver_div").css("border", "1px solid #9e9e9e");
                }
                }
            }

                if(error==0)
                {
                      var sightseeing_day_index = $("#sightseeing_day_index").val();

                var sightseeing_id = $("#sightseeing_details_id").val();

                var sightseeing_name = $("#sightseeing_details_name").text();
               
                if(sightseeing_tour_type=="private")
                {
                 var sightseeing_adult_cost = $("#sightseeing_adult_cost").val();
                }
                else
                {
                       var sightseeing_adult_cost = $("#sightseeing_group_cost").val();
                }


                var selected_vehicle_type_id = $("#selected_vehicle_type_id").val();
                var selected_vehicle_type_name = $("#selected_vehicle_type_name").val();
                var selected_guide_id = $("#selected_guide_id").val();
                var selected_guide_name = $("#selected_guide_name").val();
                var selected_guide_cost = $("#selected_guide_cost").val();
                var selected_driver_id = $("#selected_driver_id").val();
                var selected_driver_name = $("#selected_driver_name").val();
                var selected_driver_cost = $("#selected_driver_cost").val();
                var sightseeing_total_cost = $("#sightseeing_total_cost").val();
                var selected_sightseeing_detail_link=$("#selected_sightseeing_detail_link").val();
                var sightseeing_additional_cost = $("#sightseeing_additional_cost").val();
                var sightseeing_cities = $("#sightseeing_cities").val();



                $("#sightseeing_id__" + sightseeing_day_index).val(sightseeing_id);
                $("#selected_sightseeing_name__" + sightseeing_day_index).text(sightseeing_name);
                $("#sightseeing_name__" + sightseeing_day_index).val(sightseeing_name);
                 $("#sightseeing_tour_type__" + sightseeing_day_index).val(sightseeing_tour_type);
                $("#sightseeing_vehicle_type__" + sightseeing_day_index).val(selected_vehicle_type_id);
                $("#sightseeing_guide_id__" + sightseeing_day_index).val(selected_guide_id);
                $("#sightseeing_guide_name__" + sightseeing_day_index).val(selected_guide_name);  
                $("#sightseeing_guide_cost__" + sightseeing_day_index).val(selected_guide_cost); 
                $("#sightseeing_driver_id__" + sightseeing_day_index).val(selected_driver_id);   
                $("#sightseeing_driver_name__" + sightseeing_day_index).val(selected_driver_name);
                $("#sightseeing_driver_cost__" + sightseeing_day_index).val(selected_driver_cost);
                $("#sightseeing_adult_cost__" + sightseeing_day_index).val(sightseeing_adult_cost);
                 $("#sightseeing_additional_cost__" + sightseeing_day_index).val(sightseeing_additional_cost);
                  $("#sightseeing_cities__" + sightseeing_day_index).val(sightseeing_cities);
                $("#sightseeing_cost__" + sightseeing_day_index).val(sightseeing_total_cost);
                $("#shows_sightseeing__" + sightseeing_day_index+",#shows_indiv_sightseeing__" + sightseeing_day_index).attr("href",selected_sightseeing_detail_link);

                $("#sightseeing_select__"+sightseeing_day_index).find('.view_guide_btn').attr("id","view_"+selected_guide_id);
                $("#sightseeing_select__"+sightseeing_day_index).find('.view_driver_btn').attr("id","view_"+selected_driver_id);




            var sightseeing_name=$("#sightseeing__"+sightseeing_id).find('.card-title').text();
            var sightseeing_cost=$("#sightseeing__"+sightseeing_id).find('.search_sightseeing_cost').val();
            var sightseeing_address=$("#sightseeing__"+sightseeing_id).find('.search_sightseeing_address').val();
            var sightseeing_image=$("#sightseeing__"+sightseeing_id).find('.search_sightseeing_image').val();
            var sightseeing_distance=$("#sightseeing__"+sightseeing_id).find('.search_sightseeing_distance').val();
            var sightseeing_attractions=$("#sightseeing__"+sightseeing_id).find('.search_sightseeing_attractions').val();
             $("#sightseeing_select__"+sightseeing_day_index).find('.cstm-sightseeing-name').text(sightseeing_name+" ("+sightseeing_distance+"KMS)");
              $("#days_count"+sightseeing_day_index).find('.days').text("Day "+sightseeing_day_index+" - "+sightseeing_name);
               $("#days_header"+sightseeing_day_index).html("Day "+sightseeing_day_index+" - "+sightseeing_name+" <span class='fa fa-angle-down text-right'></span>");
               $("#days_count"+sightseeing_day_index).find('.des').html(sightseeing_attractions);
             $("#sightseeing_indiv_select__"+sightseeing_day_index).find('.cstm-sightseeing-indiv-name').text(sightseeing_name+" ("+sightseeing_distance+"KMS)");
              $("#sightseeing_select__"+sightseeing_day_index).find('.cstm-sightseeing-address').html('<i class="fa fa-map-marker"></i> '+sightseeing_address);
             $("#sightseeing_indiv_select__"+sightseeing_day_index).find('.cstm-sightseeing-indiv-address').html('<i class="fa fa-map-marker"></i>'+sightseeing_address);
            $("#sightseeing_select__"+sightseeing_day_index).find('.cstm-sightseeing-image').attr("src",sightseeing_image);
             $("#sightseeing_indiv_select__"+sightseeing_day_index).find('.cstm-sightseeing-indiv-image').attr("src",sightseeing_image);



              $("#sightseeing_select__"+sightseeing_day_index).parent().parent().css("display","block");
          $("#sightseeing_indiv_select__"+sightseeing_day_index).parent().parent().css("display","block");
          $("#add_sightseeing__"+sightseeing_day_index).hide();

            if ($("#sightseeing_warning__"+sightseeing_day_index).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                      $("#sightseeing_warning__"+sightseeing_day_index).hide();
                    }
         



             if($('[id^="activity_select__'+sightseeing_day_index+'__"]').length==1)
            {
                 $(".activity_select__"+sightseeing_day_index).each(function()
                 {
                     var activity_id=$(this).attr('id').split("__");
                       if ($("#activities_warning__"+activity_id[1]+"__"+activity_id[2]).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                    }
          
                      $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().css('display','none');
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_id").val("");
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_cost").val(0);
                $("#activity_pax_div__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
                  $("#activities_select_adults__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");
                    $("#activities_select_child__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");

                $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().parent().css('display','none');

                 });

                 $("#add_activity__"+sightseeing_day_index).show();
               

            }
            else
            {
                 $(".activity_select__"+sightseeing_day_index).each(function()
                 {
                     var activity_id=$(this).attr('id').split("__");
                       if ($("#activities_warning__"+activity_id[1]+"__"+activity_id[2]).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                    }
          
                      $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().css('display','none');
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
                 $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_id").val("");
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_cost").val(0);

          $("#activity_pax_div__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
           $("#activities_select_adults__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");
                    $("#activities_select_child__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");


           $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
              $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().parent().css('display','none');
            });
                  $("#add_activity__"+sightseeing_day_index).show();
               
            }



          $("#calculate_cost").trigger("click");


            //       var total_cost=0;
            // $(".calc_cost").each(function()
            // {
            //     if($(this).val()!="")
            //     {
            //         total_cost+=parseInt($(this).val());
            //     }

            // });

            //   var markup=$("#markup_per").val();
            // var own_markup=$("#own_markup_per").val();
            // var markup_cost=Math.round((total_cost*markup)/100);
            //  $("#total_cost_w_markup").val(total_cost);
            // var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

            // $("#total_cost_w_agent_markup").val(total_agent_cost);

            // var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
            // total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

            // $("#total_cost").val(total_cost);


            //  $("#total_cost_text").text(total_cost);
             $("#calculate_cost").trigger("click");
             $("#selectSightseeingModal").modal("hide");
                }
              

            });


    $(document).on("click",".select_activity",function()
    {
         $(".select_activity").each(function()
        {
              $(this).find('.tick').css("display","none");
            $(this).removeClass('selected_activity');

        });

            $(this).find('.tick').css("display","block");
            $(this).addClass('selected_activity');
            
            var activity_id="";
            var activity_name="";
            var activity_cost="";
            var activity_address="";
            var activity_image="";
            var search_activity_adult_min_max="";
            var search_activity_child_min_max="";

            var activity_day_index=$("#activities_day_index").val();
             var activity_day_index_self=$("#activities_day_index_self").val();

            activity_id=$(this).attr('id').split("__")[1];
            activity_name=$(this).find('.card-title').text();
            activity_cost=$(this).find('.search_activity_cost').val();
             activity_address=$(this).find('.search_activity_address').val();
            activity_image=$(this).find('.search_activity_image').val();
            search_activity_adult_min_max=$(this).find('.search_activity_adult_min_max').val();
            search_activity_child_min_max=$(this).find('.search_activity_child_min_max').val();
            var search_activity_detail_link=$(this).find('.search_activity_detail_link').val();     

            $("#activity_id__"+activity_day_index+"__"+activity_day_index_self).val(activity_id);
            $("#selected_activity_name__"+activity_day_index+"__"+activity_day_index_self).text(activity_name);
            $("#activity_name__"+activity_day_index+"__"+activity_day_index_self).val(activity_name);
            $("#activity_cost__"+activity_day_index+"__"+activity_day_index_self).val(activity_cost);
            $("#activities_warning__"+activity_day_index+"__"+activity_day_index_self).hide();

             $("#activity_select__"+activity_day_index+"__"+activity_day_index_self).find('.cstm-activity-name').text(activity_name);
             $("#activity_indiv_select__"+activity_day_index+"__"+activity_day_index_self).find('.cstm-activity-indiv-name').text(activity_name);
              $("#activity_select__"+activity_day_index+"__"+activity_day_index_self).find('.cstm-activity-address').html('<i class="fa fa-map-marker"></i> '+activity_address);
             $("#activity_indiv_select__"+activity_day_index+"__"+activity_day_index_self).find('.cstm-activity-indiv-address').html('<i class="fa fa-map-marker"></i>'+activity_address);
               $("#activity_select__"+activity_day_index+"__"+activity_day_index_self).find('.cstm-activity-image').attr("src",activity_image);
             $("#activity_indiv_select__"+activity_day_index+"__"+activity_day_index_self).find('.cstm-activity-indiv-image').attr("src",activity_image);
            $("#show_activity__"+activity_day_index+"__"+activity_day_index_self+",#show_indiv_activity__"+activity_day_index+"__"+activity_day_index_self).attr("href",search_activity_detail_link);
             var activity_adult_min=search_activity_adult_min_max.split("-")[0];
             var activity_adult_max=search_activity_adult_min_max.split("-")[1];
             var activity_child_min=search_activity_child_min_max.split("-")[0];
             var activity_child_max=search_activity_child_min_max.split("-")[1];

             var adult_data="<option value=''>Adults</option>";
             if(activity_adult_min>0)
             {
                 adult_data+="<option value='0'>0</option>";
             }
             for(i=activity_adult_min;i<=activity_adult_max;i++)
             {
                adult_data+="<option value='"+i+"'>"+i+"</option>";
             }
             var child_data="<option value=''>Child</option>";
              if(activity_child_min>0)
             {
                 child_data+="<option value='0'>0</option>";
             }
             for(j=activity_child_min;j<=activity_child_max;j++)
             {
                child_data+="<option value='"+j+"'>"+j+"</option>";
             }

             if( $("#activity_pax_div__"+activity_day_index+"__"+activity_day_index_self).length==0)
             {
                var pax_div_id=$(".activities_pax_div__"+activity_day_index).last().attr("id");

                var clone_pax_div=$("#"+pax_div_id).clone();
                clone_pax_div.attr("id","activity_pax_div__"+activity_day_index+"__"+activity_day_index_self);
                clone_pax_div.find('.rooms_text').attr("id","rooms_text__"+activity_day_index+"__"+activity_day_index_self).html("&nbsp; &nbsp;Activity "+activity_day_index_self);
                clone_pax_div.find(".select_activities_adults").attr("id","activities_select_adults__"+activity_day_index+"__"+activity_day_index_self);
                clone_pax_div.find(".select_activities_child").attr("id","activities_select_child__"+activity_day_index+"__"+activity_day_index_self);
                clone_pax_div.find(".activities_child_age_div").attr("id","activities_child_age_div__"+activity_day_index+"__"+activity_day_index_self);

                $("#"+pax_div_id).after(clone_pax_div);
                $("#activity_pax_div__"+activity_day_index+"__"+activity_day_index_self).show();
             $("#activities_select_adults__"+activity_day_index+"__"+activity_day_index_self).attr("required","required").html(adult_data).show();
             $("#activities_select_child__"+activity_day_index+"__"+activity_day_index_self).attr("required","required").html(child_data).show();
              $("#activities_child_age_div__"+activity_day_index+"__"+activity_day_index_self).html("");

             }
             else
             {
                 $("#activity_pax_div__"+activity_day_index+"__"+activity_day_index_self).show();
             $("#activities_select_adults__"+activity_day_index+"__"+activity_day_index_self).attr("required","required").html(adult_data).show();
             $("#activities_select_child__"+activity_day_index+"__"+activity_day_index_self).attr("required","required").html(child_data).show();
              $("#activities_child_age_div__"+activity_day_index+"__"+activity_day_index_self).html("");
             }
            
              // alert(activity_day_index);
              //  alert(activity_day_index_self);
               $("#activity_select__"+activity_day_index+"__"+activity_day_index_self).parent().parent().show();
               $("#activity_select__"+activity_day_index+"__"+activity_day_index_self).show();

                $("#activity_indiv_select__"+activity_day_index+"__"+activity_day_index_self).parent().parent().parent().show();
               $("#activity_indiv_select__"+activity_day_index+"__"+activity_day_index_self).show();


               if ($("#activities_warning__"+activity_day_index+"__"+activity_day_index_self).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                     $("#activities_warning__"+activity_day_index+"__"+activity_day_index_self).hide()
                    }



            //  var total_cost=0;
            // $(".calc_cost").each(function()
            // {
            //     if($(this).val()!="")
            //     {
            //         total_cost+=parseInt($(this).val());
            //     }

            // });
            //    var markup=$("#markup_per").val();
            // var own_markup=$("#own_markup_per").val();
            // var markup_cost=Math.round((total_cost*markup)/100);
            //  $("#total_cost_w_markup").val(total_cost);
            // var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

            // $("#total_cost_w_agent_markup").val(total_agent_cost);

            // var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
            // total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

            // $("#total_cost").val(total_cost);


            //  $("#total_cost_text").text(total_cost);

             $("#calculate_cost").trigger("click");

  $("#selectActivityModal").modal("hide");

    });

            $(document).on("click", ".select_transfer", function () {
                $(".select_transfer").each(function () {
                    $(this).find('.tick').css("display", "none");
                    $(this).removeClass('selected_transfer');

                });


                $(this).find('.tick').css("display", "block");
                $(this).addClass('select_transfer');

                var transfer_day_index = $("#transfer_day_index").val();

                var transfer_id = $(this).attr('id').split("__")[1];

                var transfer_name = $(this).find('.card-title').text();
                var transfer_cost = $(this).find('.search_transfer_cost').val();
                var from_airport = $(this).find('.search_transfer_from_airport').val();
                var to_airport = $(this).find('.search_transfer_to_airport').val();
                var from_city = $(this).find('.search_transfer_from_city').val();
                var to_city = $(this).find('.search_transfer_to_city').val();
                var vehicle_image = $(this).find('.search_vehicle_image').val();
                 var vehicle_type = $(this).find('.search_vehicle_type').val();


                var transfer_type = $("#transfer_type__"+transfer_day_index).val();

                $("#transfer_id__" + transfer_day_index).val(transfer_id);
                $("#transfer_car_name__" + transfer_day_index).val(transfer_name);
                  $("#transfer_vehicle_type__" + transfer_day_index).val(vehicle_type);
                $("#transfer_select__"+transfer_day_index).find(".cstm-transfer-car").text(transfer_name);
                $("#transfer_select__"+transfer_day_index).find(".cstm-transfer-image").attr("src",vehicle_image);

                $("#transfer_indiv_select__"+transfer_day_index).find(".cstm-indiv-transfer-car").text(transfer_name);
                $("#transfer_indiv_select__"+transfer_day_index).find(".cstm-indiv-transfer-image").attr("src",vehicle_image);
                if (transfer_type == "from-airport") {
                    $("#transfer_from_airport__" + transfer_day_index).val(from_airport);
                    $("#transfer_to_city__" + transfer_day_index).val(to_city);

                     $("#transfer_guide_id__"+transfer_day_index).val("");
                     $("#transfer_guide_name__"+transfer_day_index).val("");
                      $("#transfer_guide_cost__"+transfer_day_index).val("");


                    $("#select_transfer_guide_btn__"+transfer_day_index).show();

                    $('#selected_transfer_guide_label__'+transfer_day_index).hide();
                    $('#transfer_guide_remove__'+transfer_day_index).hide();
                    $('#selected_transfer_guide_name__'+transfer_day_index).text("").hide();
                } else if (transfer_type == "to-airport") {
                    $("#transfer_from_city__" + transfer_day_index).val(from_city);
                    $("#transfer_to_airport__" + transfer_day_index).val(to_airport);
                      $("#transfer_guide_id__"+transfer_day_index).val("");
                     $("#transfer_guide_name__"+transfer_day_index).val("");
                      $("#transfer_guide_cost__"+transfer_day_index).val("");


                      $("#select_transfer_guide_btn__"+transfer_day_index).show();

                    $('#selected_transfer_guide_label__'+transfer_day_index).hide();
                    $('#transfer_guide_remove__'+transfer_day_index).hide();
                    $('#selected_transfer_guide_name__'+transfer_day_index).text("").hide();

                } else if (transfer_type == "city") {
                    $("#transfer_from_city__" + transfer_day_index).val(from_city);
                     $("#transfer_to_city__" + transfer_day_index).val(to_city);
                      $("#transfer_guide_id__"+transfer_day_index).val("");
                     $("#transfer_guide_name__"+transfer_day_index).val("");
                      $("#transfer_guide_cost__"+transfer_day_index).val("");

                      $("#select_transfer_guide_btn__"+transfer_day_index).hide();
                       $('#selected_transfer_guide_label__'+transfer_day_index).hide();
                    $('#transfer_guide_remove__'+transfer_day_index).hide();
                    $('#selected_transfer_guide_name__'+transfer_day_index).text("").hide();
                }


               $("#transfer_cost__" + transfer_day_index).val(transfer_cost);
                $("#transfer_total_cost__" + transfer_day_index).val(transfer_cost);

               
            //  var total_cost=0;
            // $(".calc_cost").each(function()
            // {
            //     if($(this).val()!="")
            //     {
            //         total_cost+=parseInt($(this).val());
            //     }

            // });
            //    var markup=$("#markup_per").val();
            // var own_markup=$("#own_markup_per").val();
            // var markup_cost=Math.round((total_cost*markup)/100);
            //  $("#total_cost_w_markup").val(total_cost);
            // var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

            // $("#total_cost_w_agent_markup").val(total_agent_cost);

            // var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
            // total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

            // $("#total_cost").val(total_cost);


            //  $("#total_cost_text").text(total_cost);

             $("#calculate_cost").trigger("click");

                setTimeout(function () {
                    $("#selectTransferModal").modal("hide");
                }, 400);

            });



                   $(document).on("click", ".select_transfer_guide_btn", function (event) {

                var id = this.id;
                var transfer_id = id.split("__")[1];


                var transfer_own_id = $("#transfer_id__" + transfer_id).val();
               var country_id = $("#days_country__" + transfer_id).val();
                var city_id = $("#days_city__" + transfer_id).val();
                  var transfer_date=$("#days_dates__"+transfer_id).val();
                 var vehicle_type_id=$("#transfer_vehicle_type__"+transfer_id).val();

                  var transfer_type = $("#transfer_type__" + transfer_id).val();
                  if(transfer_type=="from-airport")
                  {
                    var airport_id=$("#transfer_from_airport__"+transfer_id).val();
                     var city_transfer=$("#transfer_to_city__"+transfer_id).val();
                  }
                  else if(transfer_type=="to-airport")
                  {
                    var airport_id=$("#transfer_to_airport__"+transfer_id).val();
                     var city_transfer=$("#transfer_from_city__"+transfer_id).val();
                  }
                var error = 0;
                    if (transfer_own_id == "") {
                        alert("Please select transfer first");
                        error++;
                    }



                if (error == 0) {

                    $.ajax({

                        url: "{{route('fetchGuidesTransfer')}}",

                        type: "GET",

                        data: {
                            "country_id": country_id,
                            "airport_id":airport_id,
                        "vehicle_type_id":vehicle_type_id,
                        "transfer_date":transfer_date,
                         "city_id":city_transfer,
                        "request_type":"agent"
                        },

                        success: function (response) {
                            $("#transfer_guide_day_index").val(transfer_id);
                            $("#transfer_guide_div").html(response);
                            $("#selectTransferGuideModal").modal("show");

                        }

                    }).done(function(){
                var guideDiv=$(".guide-div");
                guideDiv.on("click",function(){
                 
                    this.getElementsByClassName("tile-label")[0].click()
                })
            });

                }

            });



               $(document).on("click", "#include_transfer_guide", function (e) {

                    var error=0;
                     if ($("input[name='guide']:checked").val()=="" || $("input[name='guide']:checked").val() == undefined) {
                    e.preventDefault();
                    alert("Please Select Guide first");
                      error++;
                      $("#transfer_guide_div").css("border", "1px solid #cf3c63");
                      
                } else

                {
                     $("#transfer_guide_div").css("border","");
                }

                if(error==0)
                {
                      var transfer_guide_day_index = $("#transfer_guide_day_index").val();
                var guide_id=$("input[name='guide']:checked").val();
            var guide_cost=$("#guide_price_"+guide_id).text();
             var guide_name=$("#guide_name_"+guide_id).text();

             var transfer_cost=$("#transfer_cost__"+transfer_guide_day_index).val();

             $("#transfer_guide_id__"+transfer_guide_day_index).val(guide_id);
             $("#transfer_guide_name__"+transfer_guide_day_index).val(guide_name);
             $("#transfer_guide_cost__"+transfer_guide_day_index).val(guide_cost);
             var total_cost=parseInt(parseInt(transfer_cost)+parseInt(guide_cost));
             $("#transfer_total_cost__"+transfer_guide_day_index).val(total_cost);

              $('#selected_transfer_guide_label__'+transfer_guide_day_index).show();
               $('#transfer_guide_remove__'+transfer_guide_day_index).show();
             $('#selected_transfer_guide_name__'+transfer_guide_day_index).text(guide_name).show();
             $('#select_transfer_guide_btn__'+transfer_guide_day_index).hide();



               $("#calculate_cost").trigger("click");

                setTimeout(function () {
                    $("#selectTransferGuideModal").modal("hide");
                }, 500);
                }

               
              

            });

               $(document).on("click",".transfer_guide_remove",function()
            {
                if(confirm("Are you sure that you want to remove guide from this transfer?")==true)
                {
                    var id=this.id;
                    var transfer_day_index=id.split("__")[1];
                    $("#transfer_guide_id__" + transfer_day_index).val("");
                     $("#transfer_guide_name__" + transfer_day_index).val("");
                      $("#transfer_guide_cost__" + transfer_day_index).val("");

                    var transfer_cost=$("#transfer_cost__"+transfer_day_index).val();
                    var total_cost=parseInt(transfer_cost);
                     $("#transfer_total_cost__"+transfer_day_index).val(total_cost);

                    $("#"+id).css("display","none");

                     $("#select_transfer_guide_btn__"+transfer_day_index).show();

                    $("#transfer_guide_remove__"+transfer_day_index).hide();
                     $("#selected_transfer_guide_name__"+transfer_day_index).hide();
                      $("#selected_transfer_guide_label__"+transfer_day_index).hide();

                  $("#calculate_cost").trigger("click");

                }
                
            });
</script>
<script>
    $(document).on("click","#add_more_rooms",function()
    {
        var room_id=$(".rooms_div:last").attr("id").split("__")[1];
        var clone_rooms=$(".rooms_div:last").clone();
        old_id=room_id;
        new_id=parseInt(room_id)+1;
        if(new_id>7)
        {
        $("#add_more_rooms").hide();
        }
        else
        {

            clone_rooms.find('.rooms_text').parent().parent().attr("id","rooms_div__"+new_id);
        clone_rooms.find('.rooms_text').text("Room "+new_id);
        clone_rooms.find('.rooms_count').val(new_id);
        clone_rooms.find('.select_adults').val("").trigger("change");
          clone_rooms.find('.select_child').val("").trigger("change");
           clone_rooms.find('.child_age_div').html("");
        clone_rooms.find('.add_more_div').html('<label class="remove_more_rooms" id="remove_more_rooms__'+new_id+'">x</label>');
        $(".rooms_div:last").after(clone_rooms);

        $(".change_hotel").css({"background-color":'red',"padding":"5px","color":"white","font-size": "10px"});
        if(new_id==7)
        {
            $("#add_more_rooms").hide();
        }
        }
        
    });
         $(document).on("change",".select_child",function()
        {
            var html_data="";
            var child_count=$(this).val();
            for($i=1;$i<=child_count;$i++)
            {
                html_data+='<div class="col-md-6"> <div class="form-group"><label for="child_age'+$i+'" style="color:black">Child Age '+$i+'</label><input type="number" min=0 id="child_age'+$i+'" name="child_age[]" class="form-control child_age" style="color:black" required></div></div>'
            }
            $(this).parent().parent().find('.child_age_div').html(html_data);
            $(this).parent().parent().find('.child_age_div').show();
        });


          $(document).on("change",".select_activities_child",function()
        {
            var id=this.id;
            var actual_id=id.split("__");
            var html_data="";
            var child_count=$(this).val();
            for($i=1;$i<=child_count;$i++)
            {
                html_data+='<div class="col-md-6"> <div class="form-group"><input type="number" min=0 id="activities_select_child__'+actual_id[1]+'__'+actual_id[2]+'__'+$i+'" name="activities_select_child_age['+parseInt(actual_id[1]-1)+']['+parseInt(actual_id[2]-1)+'][]" class="form-control activities_select_child_age" placeholder="Child Age '+$i+'" required></div></div>'
            }
            $('#activities_child_age_div__'+actual_id[1]+'__'+actual_id[2]).html(html_data).show();
        });

    $(document).on("click",".remove_more_rooms",function()
    {
        $(this).parent().parent().remove();
        var count=1;

        $(".rooms_div").each(function()
        {
            $(this).attr("id","rooms_div__"+count);
             $(this).find('.rooms_text').text("Room "+count);
              $(this).find('.rooms_text').text("Room "+count);
            count++;
        });
        if(count<=4)
        {
            $("#add_more_rooms").show();
        }
    });
</script>
<script>
    $(document).on("change","#hotel_sort_filter",function()
    {
        var sort_factor=$(this).val();
        var sorthotels=$("div.select_hotel");
        if(sort_factor=="rating_low_high")
        {
           
            var rating_low_high_div = sorthotels.sort(function (a, b) {  
               a = parseInt($(a).find(".card-rating-text").text(), 10);
               b = parseInt($(b).find(".card-rating-text").text(), 10);
               if(a > b) {
                return 1;
            } else if(a < b) {
                return -1;
            } else {
                return 0;
            }
        });
            $("#hotel_sort_div").html(rating_low_high_div);
        }
        if(sort_factor=="rating_high_low")
        {
            
            var rating_high_low_div = sorthotels.sort(function (a, b) {
               a = parseInt($(a).find(".card-rating-text").text(), 10);
               b = parseInt($(b).find(".card-rating-text").text(), 10);
               if(a > b) {
                return -1;
            } else if(a < b) {
                return 1;
            } else {
                return 0;
            }
        });
            $("#hotel_sort_div").html(rating_high_low_div);
        }

        if(sort_factor=="price_low_high")
        {
           
            var price_low_high_div = sorthotels.sort(function (a, b) {  
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return 1;
            } else if(a < b) {
                return -1;
            } else {
                return 0;
            }
        });
            $("#hotel_sort_div").html(price_low_high_div);
        }
        if(sort_factor=="price_high_low")
        {
            
            var price_high_low_div = sorthotels.sort(function (a, b) {
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return -1;
            } else if(a < b) {
                return 1;
            } else {
                return 0;
            }
        });
            $("#hotel_sort_div").html(price_high_low_div);
        }
    });

    $(document).on("change","#hotel_rating_filter",function()
    {
      var optionValue = $(this).val();
      if(optionValue!="")
      {
       $(".select_hotel").each(function()
       {
        if($(this).find(".card-rating-text").text()==optionValue)
        {
            $(this).show(500);
        }
        else
        {
            $(this).hide();
        }
    })
   }
   else
   {
    $(".select_hotel").show(500);
}


});

     $(document).on("change","#activities_sort_filter",function()
    {
        var sort_factor=$(this).val();
        var sortActivities=$("div.select_activity");
    
        if(sort_factor=="price_low_high")
        {
           
            var price_low_high_div = sortActivities.sort(function (a, b) {  
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return 1;
            } else if(a < b) {
                return -1;
            } else {
                return 0;
            }
        });
            $("#activities_sort_div").html(price_low_high_div);
        }
        if(sort_factor=="price_high_low")
        {
            
            var price_high_low_div = sortActivities.sort(function (a, b) {
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return -1;
            } else if(a < b) {
                return 1;
            } else {
                return 0;
            }
        });
            $("#activities_sort_div").html(price_high_low_div);
        }
    });

     $(document).on("keyup keypress change","#activity_name_filter",function()
     {
        var activity_search_value=$(this).val().toLowerCase();

        $(".select_activity").each(function()
        {
            var id=$(this).attr("id");
            var activity_name=$("#"+id).find(".card-title").text().toLowerCase();
            if(activity_name.indexOf(activity_search_value+"")!=-1)
            {
                $("#"+id).show();
            }
            else
            {
                $("#"+id).hide();
            }
        });
     });

      $(document).on("change","#sightseeing_sort_filter",function()
    {
        var sort_factor=$(this).val();
        var sortSightseeing=$("div.select_sightseeing");
    
        if(sort_factor=="price_low_high")
        {
           
            var price_low_high_div = sortSightseeing.sort(function (a, b) {  
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return 1;
            } else if(a < b) {
                return -1;
            } else {
                return 0;
            }
        });
            $("#sightseeing_sort_div").html(price_low_high_div);
        }
        if(sort_factor=="price_high_low")
        {
            
            var price_high_low_div = sortSightseeing.sort(function (a, b) {
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return -1;
            } else if(a < b) {
                return 1;
            } else {
                return 0;
            }
        });
            $("#sightseeing_sort_div").html(price_high_low_div);
        }
    });

        $(document).on("change","#transfer_sort_filter",function()
    {
        var sort_factor=$(this).val();
        var sortTransfer=$("div.select_transfer");
    
        if(sort_factor=="price_low_high")
        {
           
            var price_low_high_div = sortTransfer.sort(function (a, b) {  
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return 1;
            } else if(a < b) {
                return -1;
            } else {
                return 0;
            }
        });
            $("#transfer_sort_div").html(price_low_high_div);
        }
        if(sort_factor=="price_high_low")
        {
            
            var price_high_low_div = sortTransfer.sort(function (a, b) {
               a = parseInt($(a).find(".card-price-text").text(), 10);
               b = parseInt($(b).find(".card-price-text").text(), 10);
               if(a > b) {
                return -1;
            } else if(a < b) {
                return 1;
            } else {
                return 0;
            }
        });
            $("#transfer_sort_div").html(price_high_low_div);
        }
    });

     $(document).on("keyup keypress change","#transfer_name_filter",function()
     {
        var transfer_search_value=$(this).val().toLowerCase();

        $(".select_transfer").each(function()
        {
            var id=$(this).attr("id");
            var transfer_name=$("#"+id).find(".card-title").text().toLowerCase();
            if(transfer_name.indexOf(transfer_search_value+"")!=-1)
            {
                $("#"+id).show();
            }
            else
            {
                $("#"+id).hide();
            }
        });
     });    

     $(document).on("keyup keypress change","#sightseeing_name_filter",function()
     {
        var sightseeing_search_value=$(this).val().toLowerCase();

        $(".select_sightseeing").each(function()
        {
            var id=$(this).attr("id");
            var sightseeing_name=$("#"+id).find(".card-title").text().toLowerCase();
            if(sightseeing_name.indexOf(sightseeing_search_value+"")!=-1)
            {
                $("#"+id).show();
            }
            else
            {
                $("#"+id).hide();
            }
        });
     });

     
     $(document).on("keyup keypress change","#restaurant_name_filter",function()
     {
        var restaurant_search_value=$(this).val().toLowerCase();

        $(".select_restaurant").each(function()
        {
            var id=$(this).attr("id");
            var restaurant_name=$("#"+id).find(".card-title").text().toLowerCase();
            if(restaurant_name.indexOf(restaurant_search_value+"")!=-1)
            {
                $("#"+id).show();
            }
            else
            {
                $("#"+id).hide();
            }
        });
     });

     $(document).on("click",".remove_transfer",function()
     {
        var transfer_id=$(this).attr('id').split("__")[1];
        if(confirm("Are you sure that you want to remove this transfer ?")==true)
        {
          $("#transfer_select__"+transfer_id).parent().parent().remove();

           $("#transfer_indiv_select__"+transfer_id).parent().parent().remove();

         // var total_cost=0;
            // $(".calc_cost").each(function()
            // {
            //     if($(this).val()!="")
            //     {
            //         total_cost+=parseInt($(this).val());
            //     }

            // });
            //    var markup=$("#markup_per").val();
            // var own_markup=$("#own_markup_per").val();
            // var markup_cost=Math.round((total_cost*markup)/100);
            //  $("#total_cost_w_markup").val(total_cost);
            // var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

            // $("#total_cost_w_agent_markup").val(total_agent_cost);

            // var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
            // total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

            // $("#total_cost").val(total_cost);


            //  $("#total_cost_text").text(total_cost);

             $("#calculate_cost").trigger("click");

              if($("#sightseeing_select__"+transfer_id).find(".cstm-sightseeing-name").text()!="")
             {
    
             }
             else
             {
                 $("#days_header"+transfer_id).html("Day "+transfer_id+" - Nothing Planned <span class='fa fa-angle-down text-right'></span>");
                  $("#days_count"+transfer_id).find('.des').html("Spend time at leisure");

             }
                
        }
       
     });


     $(document).on("click",".remove_sightseeing",function()
     {
        var sightseeing_id=$(this).attr('id').split("__")[1];
        if(confirm("Are you sure that you want to remove this sightseeing ?")==true)
        {
          $("#sightseeing_select__"+sightseeing_id).parent().parent().css("display","none");
          $("#sightseeing_id__"+sightseeing_id).val("");
           $("#sightseeing_cost__" + sightseeing_id).val(0);
          $("#sightseeing_indiv_select__"+sightseeing_id).parent().parent().css("display","none");

          $("#add_sightseeing__"+sightseeing_id).show();


           if($('[id^="activity_select__'+sightseeing_id+'__"]').length==1)
            {
                 $(".activity_select__"+sightseeing_id).each(function()
                 {
                   

                     var activity_id=$(this).attr('id').split("__");
                        if ($("#activities_warning__"+activity_id[1]+"__"+activity_id[2]).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                    }
          
                      $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().css('display','none');
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_id").val("");
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_cost").val(0);
                $("#activity_pax_div__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
                  $("#activities_select_adults__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");
                    $("#activities_select_child__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");

                $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().parent().css('display','none');

                 });

                 $("#add_activity__"+sightseeing_id).hide();
               

            }
            else
            {
                 $(".activity_select__"+sightseeing_id).each(function()
                 {
                      
          

                     var activity_id=$(this).attr('id').split("__");
                     if ($("#activities_warning__"+activity_id[1]+"__"+activity_id[2]).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                    }
                      $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().css('display','none');
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
                 $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_id").val("");
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_cost").val(0);

          $("#activity_pax_div__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
           $("#activities_select_adults__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");
                    $("#activities_select_child__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");


           $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
              $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().parent().css('display','none');
            });
                  $("#add_activity__"+sightseeing_id).hide();


               
            }

            if ($("#sightseeing_warning__"+sightseeing_id).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                    }


         // var total_cost=0;
         //    $(".calc_cost").each(function()
         //    {
         //        if($(this).val()!="")
         //        {
         //            total_cost+=parseInt($(this).val());
         //        }

         //    });
         //       var markup=$("#markup_per").val();
         //    var own_markup=$("#own_markup_per").val();
         //    var markup_cost=Math.round((total_cost*markup)/100);
         //     $("#total_cost_w_markup").val(total_cost);
         //    var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

         //    $("#total_cost_w_agent_markup").val(total_agent_cost);

         //    var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
         //    total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

         //    $("#total_cost").val(total_cost);


         //     $("#total_cost_text").text(total_cost);

          $("#calculate_cost").trigger("click");

             if($("#transfer_select__"+sightseeing_id).find(".cstm-transfer-name").text()!="")
             {
                 $("#days_header"+sightseeing_id).html("Day "+sightseeing_id+" - Transfer : "+$("#transfer_select__"+sightseeing_id).find(".cstm-transfer-name").text()+" <span class='fa fa-angle-down text-right'></span>");
                   $("#days_count"+sightseeing_id).find('.des').html("");
             }
             else
             {
                 $("#days_header"+sightseeing_id).html("Day "+sightseeing_id+" - Nothing Planned <span class='fa fa-angle-down text-right'></span>");
                  $("#days_count"+sightseeing_id).find('.des').html("Spend time at leisure");

             }
                
        }
       
     });

      $(document).on("click",".remove_activity",function()
     {
        var activity_id=$(this).attr('id').split("__");
        if(confirm("Are you sure that you want to remove this activity ?")==true)
        {
            if($('[id^="activity_select__'+activity_id[1]+'__"]').length==1)
            {
             $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().css('display','none');
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_id").val("");
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_cost").val(0);
                $("#activity_pax_div__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
                  $("#activities_select_adults__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");
                    $("#activities_select_child__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");

                $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).parent().parent().parent().css('display','none');

            }
            else
            {
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
                 $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_id").val("");
                $("#activity_select__"+activity_id[1]+"__"+activity_id[2]).find(".activity_cost").val(0);

          $("#activity_pax_div__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
           $("#activities_select_adults__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");
                    $("#activities_select_child__"+activity_id[1]+"__"+activity_id[2]).removeAttr('required').val("");


           $("#activity_indiv_select__"+activity_id[1]+"__"+activity_id[2]).css('display','none');
            }

                if ($("#activities_warning__"+activity_id[1]+"__"+activity_id[2]).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                    }
          

         // var total_cost=0;
         //    $(".calc_cost").each(function()
         //    {
         //        if($(this).val()!="")
         //        {
         //            total_cost+=parseInt($(this).val());
         //        }

         //    });
         //     var markup=$("#markup_per").val();
         //    var own_markup=$("#own_markup_per").val();
         //    var markup_cost=Math.round((total_cost*markup)/100);
         //     $("#total_cost_w_markup").val(total_cost);
         //    var total_agent_cost=parseInt(parseInt(total_cost)+parseInt(markup_cost));

         //    $("#total_cost_w_agent_markup").val(total_agent_cost);

         //    var own_markup_cost=Math.round((total_agent_cost*own_markup)/100);
         //    total_cost=parseInt(parseInt(total_agent_cost)+parseInt(own_markup_cost));

         //    $("#total_cost").val(total_cost);


         //     $("#total_cost_text").text(total_cost);
          $("#calculate_cost").trigger("click");
                
        }
       
     });


         $(document).on("click",".remove_restaurant",function()
     {
        var restaurant_id=$(this).attr('id').split("__");
        if(confirm("Are you sure that you want to remove this restaurant ?")==true)
        {
            if($('[id^="restaurant_select__'+restaurant_id[1]+'__"]').length==1)
            {
             $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).parent().parent().css('display','none');
                $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).find(".restaurant_id").val("");
                $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).find(".restaurant_cost").val(0);
                 $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).find(".restaurant_food_detail").html("");

                $("#restaurant_indiv_select__"+restaurant_id[1]+"__"+restaurant_id[2]).parent().parent().parent().css('display','none');

            }
            else
            {
                $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).css('display','none');
                 $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).find(".restaurant_id").val("");
                $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).find(".restaurant_cost").val(0);
                 $("#restaurant_select__"+restaurant_id[1]+"__"+restaurant_id[2]).find(".restaurant_food_detail").html("");


           $("#restaurant_indiv_select__"+restaurant_id[1]+"__"+restaurant_id[2]).css('display','none');
            }

             if ($("#restaurant_warning__"+restaurant_id[1]+"__"+restaurant_id[2]).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                    }


          
          $("#calculate_cost").trigger("click");
                
        }
       
     });


     $(document).on("click","#save_itinerary",function()
     {
        $("#saveItineraryModal").modal("show");

     });

     $(document).on("click","#save_itinerary_btn",function()
     {
        var customer_name=$("#customer_name").val();

        if(customer_name.trim()=="")
        {
            alert("Please enter customer name");

        }
        else
        {
          var formdata=$("#itinerary_form").serialize()+" ";

          var customFormData=$("#customer_details_form").serialize();

          formdata+="&"+customFormData;

          $.ajax({
            url:"{{route('itinerary-save')}}",
            data:formdata,
            type:"POST",
            success:function(response)
            {
                if(response.indexOf("success")!=-1)
                {
                    $("#customer_name").val("");
                    $("#customer_contact").val("");
                    $("#customer_email").val("");

                    swal("Congratulations!","Package Saved Successfully","success");

                    $("#saveItineraryModal").modal("hide");
                }
                else
                {
                    swal("Error!","There is something went wrong , please try again","error");
                }

            }
        });

      }

       

     });
</script>
    <script>
            $(document).on("click", ".sightseeing_details", function () {
                var id = $(this).attr("id").split("_")[2];
                 $("#loaderModal").modal("show");
                $.ajax({
                    url: "{{route('itinerary-get-sightseeing-details')}}",
                    data: {
                        "sightseeing_id": id
                    },
                    type: "get",
                    dataType:"JSON",
                    success: function (response) {
                        $("#sightseeing_div").hide();
                         $("#sightseeing_div_buttons").hide();
                        $("#sightseeing_details_id").val(response.sightseeing_id);
                        $("#sightseeing_details_name").text(response.sightseeing_name);
                        $("#selected_sightseeing_detail_link").val(response.sightseeing_details_link);
                          $("#sightseeing_details_address").html('<i class="fa fa-map-marker"></i> '+response.sightseeing_route);
                           $("#sightseeing_details_distance").html('<i class="fa fa-road" ></i> '+response.sightseeing_distance_covered);
                           $("#sightseeing_details_duration").html('<i class="fa fa-clock-o" ></i> '+response.sightseeing_duration); 
                            $("#sightseeing_detail_images").html("");
                           var images=response.sightseeing_images; 

                           $.each( images, function( key, value ) {
                             $("#sightseeing_detail_images").append("<img src='"+value+"'>");
                            });
                           $("#sightseeing_detail_desc").html(response.sightseeing_tour_desc);
                            $("#sightseeing_detail_attract").html(response.sightseeing_attractions);

                            if(response.sightseeing_group_tour_status==0)
                            {
                                $("#sightseeing_details_tour_type").html('<option value="private" selected="selected">PRIVATE TOUR</option>');
                                $("#sightseeing_adult_cost").val(response.total_adult_cost);
                                $("#sightseeing_additional_cost").val(response.sightseeing_additional_cost);
                                 $("#sightseeing_cities").val(response.sightseeing_cities);
                                var total_cost=parseInt(response.total_adult_cost);
                                 total_cost+=parseInt(response.sightseeing_additional_cost);
                                if(response.sightseeing_default_guide_price!="" && response.sightseeing_default_guide_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_guide_price);
                                }

                                 if(response.sightseeing_default_driver_price!="" && response.sightseeing_default_driver_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_driver_price);
                                }
                               
                                $("#sightseeing_total_price_text").text("GEL "+total_cost); 
                                 
                                 $("#sightseeing_group_cost").val(response.group_adult_cost);

                            }
                            else
                            {
                                $("#sightseeing_details_tour_type").html('<option value="private" selected="selected">PRIVATE TOUR</option><option value="group">GROUP TOUR</option>');
                                $("#sightseeing_adult_cost").val(response.total_adult_cost);
                                  $("#sightseeing_additional_cost").val(response.sightseeing_additional_cost);
                                  $("#sightseeing_cities").val(response.sightseeing_cities);
                                var total_cost=parseInt(response.total_adult_cost);
                                  total_cost+=parseInt(response.sightseeing_additional_cost);
                                if(response.sightseeing_default_guide_price!="" && response.sightseeing_default_guide_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_guide_price);
                                }

                                 if(response.sightseeing_default_driver_price!="" && response.sightseeing_default_driver_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_driver_price);
                                }
                               
                                $("#sightseeing_total_price_text").text("GEL "+total_cost); 
                                 

                                 $("#sightseeing_group_cost").val(response.group_adult_cost);

                            }

                             $("#guide_div").hide();
                            $("#driver_div").hide();
                              $("#guide_div_head").hide();
                            $("#driver_div_head").hide();
                            $("#vehicle_type_div").show();
                            $("input[name='selected_guide_id']").val("");
                              $("input[name='selected_guide_cost']").val(0);
                              $("input[name='selected_guide_name']").val("");
                            $("input[name='selected_driver_id']").val("");
                              $("input[name='selected_driver_cost']").val(0);
                              $("input[name='selected_driver_name']").val("");
                              $("input[name='vehcile_type']").prop("checked",false);

                               var sightseeing_day_index=$("#sightseeing_day_index").val();
                              setTimeout(function()
                              {
                                 // if($("#sightseeing_vehicle_type__"+sightseeing_day_index).val().trim()!="")
                                 // {
                                    var vehicle_type_id="";
                                    for(var i=sightseeing_day_index;i>1;i--)
                                    {
                                     if($("#sightseeing_vehicle_type__"+i).val() != undefined && $("#sightseeing_vehicle_type__"+i).val() !="")
                                      {
                                        vehicle_type_id=$("#sightseeing_vehicle_type__"+i).val().trim(); 
                                        break;  
                                      } 
                                    }

                                    if(vehicle_type_id=="")
                                    {
                                      vehicle_type_id=1;  
                                    }

                                    $(".vehicle-type").each(function()
                                    {
                                        console.log("Vehicle :"+$(this).find(":radio").val());
                                        if($(this).find(":radio").val()==vehicle_type_id)
                                        {
                                            $(this).find(":radio").attr("checked","checked");
                                             $(this).find(":radio").prop("checked",true).trigger("change");
                                            // $("input[name='vehcile_type']")
                                        }
                                    });
                                // }

                            },1000);
                               $("#loaderModal").modal("hide");

                              $("#sightseeing_filter").hide();
                        $("#sightseeing_details_div").show();
                        document.querySelector("div#selectSightseeingModal .modal-content .modal-body").scrollTop=0

                    }
                });
            });
    
     $(document).on("click", ".restaurant_details", function () {
                var id = $(this).attr("id").split("_")[2];
                 $("#loaderModal").modal("show");
                $.ajax({
                    url: "{{route('agent-itinerary-get-restaurant-details')}}",
                    data: {
                        "restaurant_id": id
                    },
                    type: "get",
                    dataType:"JSON",
                    success: function (response) {

                        if(response.status=="200")
                        {
                            $("#restaurant_div").hide();
                         $("#restaurant_div_buttons").hide();
                         $("#restaurant_total_price_text").text("GEL 0");
                          $("#restaurant_food_select_info_head").hide();
                        $("#restaurant_details_id").val(response.body.restaurant_id);
                           $("#selected_restaurant_address").val(response.body.restaurant_address);
                        $("#restaurant_details_name").text(response.body.restaurant_name);
                         $("#restaurant_details_type").text(response.body.restaurant_type);
                        $("#selected_restaurant_detail_link").val(response.body.restaurant_details_link);
                          $("#restaurant_details_address").html('<i class="fa fa-map-marker"></i> '+response.body.restaurant_address);
                           $("#restaurant_details_timing").html('<i class="fa fa-clock-o" ></i> '+response.body.restaurant_timing); 
                            $("#restaurant_detail_images").html("");
                           var images=response.body.restaurant_images; 

                           $.each( images, function( key, value ) {
                             $("#restaurant_detail_images").append("<img src='"+value+"'>");
                            });
                           $("#restaurant_detail_desc").html(response.body.restaurant_description);
                            $("#restaurant_menu_div").html(response.body.restaurant_menu_html);

                               $("#loaderModal").modal("hide");

                              $("#restaurant_filter").hide();
                        $("#restaurant_details_div").show();
                        document.querySelector("div#selectRestaurantModal .modal-content .modal-body").scrollTop=0 
                        }
                        else
                        {
                            swal("Error!","No Restaurant Details Found.","error");
                        }
                       

                    }
                });
            });


    $(document).on("click","#include_restaurant",function()
    {
        var check_food_selected_flag=0
        $(".food_qty").each(function()
        {
            if($(this).val()>0)
            {
               check_food_selected_flag++ 
            }

        })
            if(check_food_selected_flag<=0)
            {
                swal(" Error! ","Please select atleast one food / drink item from this restaurant","error");
                return false
            }

            var food_for=$("#food_for").val()
            if(food_for=="")
            {
              swal(" Error! ","Please select 'Food / Drink For' selection.","error");
                return false
            }
            var restaurant_id="";
            var restaurant_name="";
            var restaurant_cost="";
            var restaurant_address="";
            var restaurant_selected_food="";
            var restaurant_image="";

            var restaurant_day_index=$("#restaurant_day_index").val();
             var restaurant_day_index_self=$("#restaurant_day_index_self").val();

            restaurant_id=$("#restaurant_details_id").val();
            restaurant_name=$("#restaurant_details_name").text();
            restaurant_cost=$("#selected_restaurant_total_price").val();
            restaurant_address=$("#selected_restaurant_address").val();
            restaurant_selected_food=$("#selected_restaurant_food_items").val();
            restaurant_image=$("#restaurant__"+restaurant_id).find('.search_restaurant_image').val();
        
            var search_restaurant_detail_link=$("#selected_restaurant_detail_link").val();    


            var restaurant_food_detail_html="";
        
            $(".food_qty").each(function()
            {
                if($(this).val()>0)
                {
                    var food_qty=$(this).val();
                    var food_index=$(this).attr('id').split("__")[1];
  
                     var food_id=$('input[name="food_id['+food_index+']').val()
                     var food_name=$('input[name="food_name['+food_index+']').val()
                     var food_category_id=$('input[name="food_category_id['+food_index+']').val()
                     var food_price=$('input[name="food_price['+food_index+']').val()
                     var food_unit=$('input[name="food_unit['+food_index+']').val()
                     var food_qty=$('select[name="food_qty['+food_index+']').val()


                    restaurant_food_detail_html+='<input type="hidden" class="restaurant_food_name" name="restaurant_food_name['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_name+'"><input type="hidden" class="restaurant_food_qty" name="restaurant_food_qty['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_qty+'"><input type="hidden" class="restaurant_food_price" name="restaurant_food_price['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_price+'"><input type="hidden" class="restaurant_food_id" name="restaurant_food_id['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_id+'"><input type="hidden" class="restaurant_food_category_id" name="restaurant_food_category_id['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_category_id+'"><input type="hidden" class="restaurant_food_unit" name="restaurant_food_unit['+(restaurant_day_index-1)+']['+(restaurant_day_index_self-1)+'][]" value="'+food_unit+'">';
                }

            });

       $("#restaurant_food_detail__"+restaurant_day_index+"__"+restaurant_day_index_self).html(restaurant_food_detail_html);
            $("#restaurant_id__"+restaurant_day_index+"__"+restaurant_day_index_self).val(restaurant_id);
            $("#selected_restaurant_name__"+restaurant_day_index+"__"+restaurant_day_index_self).text(restaurant_name);
            $("#restaurant_name__"+restaurant_day_index+"__"+restaurant_day_index_self).val(restaurant_name);
              $("#restaurant_food_for__"+restaurant_day_index+"__"+restaurant_day_index_self).val(food_for);
            $("#restaurant_cost__"+restaurant_day_index+"__"+restaurant_day_index_self).val(restaurant_cost);
            $("#restaurant_warning__"+restaurant_day_index+"__"+restaurant_day_index_self).hide();

             $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-name').text(restaurant_name);
             $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-name').text(restaurant_name);
              $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-address').html('<i class="fa fa-map-marker"></i> '+restaurant_address);
             $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-address').html('<i class="fa fa-map-marker"></i>'+restaurant_address);
               $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-image').attr("src",restaurant_image);
             $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-image').attr("src",restaurant_image);
            $("#show_restaurant__"+restaurant_day_index+"__"+restaurant_day_index_self+",#show_indiv_restaurant__"+restaurant_day_index+"__"+restaurant_day_index_self).attr("href",search_restaurant_detail_link);
               $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-food-type').text(restaurant_selected_food.replace(/---/g," , "));
                $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-food-type').text(restaurant_selected_food.replace(/---/g," , "));
                 $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-food-purpose').text("For "+food_for);
                $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).find('.cstm-restaurant-indiv-food-purpose').text("For "+food_for);
            

    
              // alert(restaurant_day_index);
              //  alert(restaurant_day_index_self);
               $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).parent().parent().show();
               $("#restaurant_select__"+restaurant_day_index+"__"+restaurant_day_index_self).show();

                $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).parent().parent().show();
               $("#restaurant_indiv_select__"+restaurant_day_index+"__"+restaurant_day_index_self).show();


               if ($("#restaurant_warning__"+restaurant_day_index+"__"+restaurant_day_index_self).length)
                    {
                     $("#warning_count").val(parseInt($("#warning_count").val())-1);
                     $("#restaurant_warning__"+restaurant_day_index+"__"+restaurant_day_index_self).hide()
                    }
                    

             $("#calculate_cost").trigger("click");

  $("#selectRestaurantModal").modal("hide");

    });

     $(document).on("change",".food_qty",function()
  {
    var html="";
    var html_as_value=[];
    var total_price=0;
      
    $(".food_qty").each(function()
    {
      if($(this).val()>0)
      {
        var id=$(this).attr('id').split("__")[1];
        var food_price=$("#food_price__"+id).val()

        var food_qty=$(this).val()

        var food_name=$("#food_name__"+id).val()

         var food_unit=$("#food_unit__"+id).val()

        var calculated_price=parseInt(food_price)*food_qty
        total_price+=parseInt(calculated_price)
         html+='<div class="row"><div class="col-12"><p class="price-2">'+food_name+' ('+food_qty+' X '+food_unit+')</p></div></div>';
         html_as_value.push(food_name+' ('+food_qty+' X '+food_unit+')');
      }
    })


    $("#restaurant_food_select_info").html(html)
    $("#selected_restaurant_food_items").val(html_as_value.join("---"))
     $("#restaurant_food_select_info_head").show()
    $("#restaurant_total_price_text").text("GEL "+total_price)
    $("#selected_restaurant_total_price").val(total_price)
  })

        </script>
        <script>
    var tile=document.getElementsByClassName("parent-tile")
    
    for(var i=0;i<tile.length;i++){
        tile[i].addEventListener("click",function(){
            this.lastElementChild.click()
           
            
        })
      
       
    }
     $(document).on("change","input[name='vehcile_type']",function()
        {   


            var sightseeing_day_index=$("#sightseeing_day_index").val();
            var country_id = $("#days_country__" + sightseeing_day_index).val();
              var city_id = $("#days_city__1").val();
              var current_city_id = $("#days_city__" + sightseeing_day_index).val();
              var city_name = $("#days_city_name__1").val();
            var sightseeing_date = $("#days_dates__" + sightseeing_day_index).val();
            var sightseeing_id=$("input[name='sightseeing_details_id']").val();
            var vehicle_type_id=$(this).val();
             var vehicle_type_name=$("#vehcile_type_name_"+vehicle_type_id).val();

            $("input[name='selected_vehicle_type_id']").val(vehicle_type_id);
              $("input[name='selected_vehicle_type_name']").val(vehicle_type_name);
              $(".table-sightseeing-loader").show();
                var day_sequence=0;
                    $("#guide_div").html("");
                    $("#driver_div").html("");
                    if(city_name=="Kutaisi" || city_name=="Batumi")
                    {
                        day_sequence=1;
                    }
                       $("#loaderModal").modal("show");
                    $("#guide_div").html("");
                    $("#driver_div").html("");
                    var data_show=0;
            $.ajax({
                url:"{{route('fetchGuidesSightseeing')}}",
                data:{"sightseeing_id":sightseeing_id,
                        "vehicle_type_id":vehicle_type_id,
                        "country_id":country_id,
                         "city_id":city_id,
                        "sightseeing_date":sightseeing_date,
                        "request_type":"agent",
                     "day_sequence":day_sequence,
                        "current_city_id":current_city_id},
                type:"GET",
                success:function(response)
                {

                    $("#guide_div").html(response);
                    $(".table-sightseeing-loader").hide();

                    //empty guide id and cost because guides data refreshed
                    $("input[name='selected_guide_id']").val("");
                    $("input[name='selected_guide_cost']").val(0);
                      $("input[name='selected_guide_name']").val("");
                       $("#guide_div").show();
                       $("#guide_div_head").show();
               var guide_id="";
               for(var i=sightseeing_day_index;i>=1;i--)
               {
                   if($("#sightseeing_guide_id__"+i).val() != undefined && $("#sightseeing_guide_id__"+i).val() !="")
                   {
                    guide_id=$("#sightseeing_guide_id__"+i).val().trim(); 
                    break;  
                } 
            }
            if(guide_id=="")
            {
              guide_id=1;  
          }

                // var guide_id=$("#sightseeing_guide_id__"+sightseeing_day_index).val().trim();
                $(".guide-div").each(function()
                {
                    console.log("Guide :"+$(this).find(":radio").val());
                    if($(this).find(":radio").val()==guide_id)
                    {
                        $(this).find(":radio").attr("checked","checked");
                        $(this).find(":radio").prop("checked",true).trigger("change");
                    }
                });

            $.ajax({
                url:"{{route('fetchDriversSightseeing')}}",
                data:{"sightseeing_id":sightseeing_id,
                        "vehicle_type_id":vehicle_type_id,
                        "country_id":country_id,
                        "city_id":city_id,
                        "sightseeing_date":sightseeing_date,
                        "request_type":"agent",
                        "day_sequence":day_sequence,
                        "current_city_id":current_city_id},
                type:"GET",
                success:function(response)
                {
                    $("#driver_div").html(response);
                    $(".table-sightseeing-loader").hide();

                    //empty guide id and cost because guides data refreshed
                    $("input[name='selected_driver_id']").val("");
                    $("input[name='selected_driver_cost']").val(0);
                      $("input[name='selected_driver_name']").val("");

                        $("#driver_div").show();
                         $("#driver_div_head").show();

    
               var driver_id="";
               for(var i=sightseeing_day_index;i>=1;i--)
               {
                   if($("#sightseeing_driver_id__"+i).val() != undefined && $("#sightseeing_driver_id__"+i).val() !="")
                   {
                    driver_id=$("#sightseeing_driver_id__"+i).val().trim(); 
                    break;  
                } 
            }

            if(driver_id=="")
            {
              driver_id=1;  
          }

                // var driver_id=$("#sightseeing_driver_id__"+sightseeing_day_index).val().trim();
                $(".driver-div").each(function()
                {
                    console.log("Driver :"+$(this).find(":radio").val());
                    if($(this).find(":radio").val()==driver_id)
                    {
                        $(this).find(":radio").attr("checked","checked");
                        $(this).find(":radio").prop("checked",true).trigger("change");
                    }
                });

                      
                },
            }).done(function(){
               
                var driverDiv=$(".driver-div");
                
                driverDiv.on("click",function(){
                   
                    this.getElementsByClassName("tile-label")[0].click()
                })

                setTimeout(function()
                 {
                       $("#loaderModal").modal("hide");
                 },2000); 
            });  
 
                }
            }).done(function(){
                var guideDiv=$(".guide-div");
                guideDiv.on("click",function(){
                 
                    this.getElementsByClassName("tile-label")[0].click()
                })


            });

            

            //  setTimeout(function()
            // {
            //        if($("#sightseeing_guide_id__"+sightseeing_day_index).val().trim()!="")
            // {
            //     var guide_id=$("#sightseeing_guide_id__"+sightseeing_day_index).val().trim();
            //     $(".guide-div").each(function()
            //     {
            //         console.log("Guide :"+$(this).find(":radio").val());
            //         if($(this).find(":radio").val()==guide_id)
            //         {
            //             $(this).find(":radio").attr("checked","checked");
            //                $(this).find(":radio").prop("checked",true).trigger("change");
            //         }
            //     });
            // }

            // if($("#sightseeing_driver_id__"+sightseeing_day_index).val().trim()!="")
            // {
            //     var driver_id=$("#sightseeing_driver_id__"+sightseeing_day_index).val().trim();
            //     $(".driver-div").each(function()
            //     {
            //         console.log("Driver :"+$(this).find(":radio").val());
            //         if($(this).find(":radio").val()==driver_id)
            //         {
            //             $(this).find(":radio").attr("checked","checked");
            //                $(this).find(":radio").prop("checked",true).trigger("change");
            //         }
            //     });
            // }


            // },1000);
          

          

            // }

 


        });

   function roundLikePHP(num, dec){
  var num_sign = num >= 0 ? 1 : -1;
  return parseFloat((Math.round((num * Math.pow(10, dec)) + (num_sign * 0.0001)) / Math.pow(10, dec)).toFixed(dec));
}

     $(document).on("change","#sightseeing_details_tour_type",function()
     {
        var details_tour_type=$(this).val();

        if(details_tour_type=="group")
        {
            var adult_cost=$("#sightseeing_group_cost").val();
            $("#guide_div").hide();
            $("#driver_div").hide();
              $("#guide_div_head").hide();
            $("#driver_div_head").hide();
            $("#vehicle_type_div").hide();
        }
        else
        {
             var adult_cost=$("#sightseeing_adult_cost").val();
              $("#guide_div").show();
            $("#driver_div").show();
            $("#guide_div_head").show();
            $("#driver_div_head").show();
            $("#vehicle_type_div").show();

        }
        $("#guide_div").html("");
            $("#driver_div").html("");
               adult_cost+=parseInt($("#sightseeing_additional_cost").val());
        $("#sightseeing_total_cost").val(adult_cost);
          $("#sightseeing_total_price_text").text("GEL "+adult_cost);

           $("input[name='selected_guide_id']").val("");
              $("input[name='selected_guide_cost']").val(0);
              $("input[name='selected_guide_name']").val("");
            $("input[name='selected_driver_id']").val("");
              $("input[name='selected_driver_cost']").val(0);
              $("input[name='selected_driver_name']").val("");
              $("input[name='vehcile_type']").prop("checked",false);


     });
       $(document).on("change","input[name='guide']",function()
        {
            var guide_id=$(this).val();
            var guide_cost=$("#guide_price_"+guide_id).text();
             var guide_name=$("#guide_name_"+guide_id).text();

            

            $("input[name='selected_guide_id']").val(guide_id);
              $("input[name='selected_guide_cost']").val(guide_cost);
              $("input[name='selected_guide_name']").val(guide_name);

              var driver_cost=$("input[name='selected_driver_cost']").val();


            var adult_count = 1;


            var sightseeing_tour_type=$("#sightseeing_details_tour_type").val();
            var sightseeing_additional_cost=$("#sightseeing_additional_cost").val();
            if(sightseeing_tour_type=="private")
            {
            var adult_price = $("#sightseeing_adult_cost").val();
            }
            else
            {
            var adult_price = $("#sightseeing_group_cost").val();
            }
            

            if (!adult_count) {
                adult_count = 0;
            }

            if (!adult_price) {
                adult_price = 0;
            }

            if(!sightseeing_additional_cost)
            {
                sightseeing_additional_cost=0;
            }
            else if(sightseeing_additional_cost=="")
            {
              sightseeing_additional_cost=0;  
            }


            var adult_final_price = parseFloat(adult_price * adult_count);
            
           var total_hotel_foodcost = 0;

            var total_price = parseFloat( adult_final_price + total_hotel_foodcost);

            var total_price_after_driver = parseFloat(parseFloat(driver_cost) + parseFloat(total_price));

                var total_price_after_guide = parseFloat(parseFloat(guide_cost) + total_price_after_driver);
                    total_price_after_guide+=parseFloat(sightseeing_additional_cost);
                var total_price_after_guide_1=roundLikePHP(total_price_after_guide,0);
                


            $("#sightseeing_total_price_text").text("GEL "+total_price_after_guide_1);
            $("#sightseeing_total_cost").val(total_price_after_guide_1);
          
        });


        //get the cost and driver that is selected
           $(document).on("change","input[name='driver']",function()
        {
            var driver_id=$(this).val();
            var driver_cost=$("#driver_price_"+driver_id).text();
             var driver_name=$("#driver_name_"+driver_id).text();


            $("input[name='selected_driver_id']").val(driver_id);
              $("input[name='selected_driver_cost']").val(driver_cost);
              $("input[name='selected_driver_name']").val(driver_name);

               var guide_cost=$("input[name='selected_guide_cost']").val();

                var adult_count = 1;


            var sightseeing_tour_type=$("#sightseeing_details_tour_type").val();
               var sightseeing_additional_cost=$("#sightseeing_additional_cost").val();
            if(sightseeing_tour_type=="private")
            {
            var adult_price = $("#sightseeing_adult_cost").val();
            }
            else
            {
            var adult_price = $("#sightseeing_group_cost").val();
            }
            

            if (!adult_count) {
                adult_count = 0;
            }

            if (!adult_price) {
                adult_price = 0;
            }

             if(!sightseeing_additional_cost)
            {
                sightseeing_additional_cost=0;
            }
            else if(sightseeing_additional_cost=="")
            {
              sightseeing_additional_cost=0;  
            }


            var adult_final_price = parseFloat(adult_price * adult_count);
            
           var total_hotel_foodcost = 0;

            var total_price = parseFloat( adult_final_price + total_hotel_foodcost);

           var total_price_after_guide = parseFloat(parseFloat(guide_cost) + parseFloat(total_price));
                var total_price_after_driver = parseFloat(parseFloat(driver_cost) + total_price_after_guide);
                total_price_after_driver+=parseFloat(sightseeing_additional_cost);
                var total_price_after_driver_1=roundLikePHP(total_price_after_driver,0);


            $("#sightseeing_total_price_text").text("GEL "+total_price_after_driver_1);
            $("#sightseeing_total_cost").val(total_price_after_driver_1);

        });
            $(document).on("click",".view_more",function()
           {
            var clone=$(this).parent().clone();
            clone.find(".view_more").remove();
            clone.find(".more-vehicle-image").css("display","block");
            clone.find(".more-vehicle-image").parent().css("display","block");
             clone.find("img").removeAttr("width height").attr({"width":"250","height":"200"}).css("padding","10px");
            $("#vehicle_view_images").html(clone);
            $("#vehicleModal").modal("show");
           });
               $(document).on("click",".view_guide,.view_guide_btn",function()
             {
                var id=$(this).attr("id").split("_")[1];

                $.ajax({
                    url:"{{route('fetchGuidesDetails')}}",
                    type:"GET",
                    data:{"guide_id":id},
                    success:function(response)
                    {
                        $("#guide_details_div").html(response);

                        $("#guideModal").modal("show");
                        $('#guide_details_div').animate({
                            scrollTop: $('#guide_details_div').position().top
                        }, 'slow');
                    }

                });
             });
              $(document).on("click",".view_driver,.view_driver_btn",function()
             {
                var id=$(this).attr("id").split("_")[1];

                $.ajax({
                    url:"{{route('fetchDriversDetails')}}",
                    type:"GET",
                    data:{"driver_id":id},
                    success:function(response)
                    {
                        $("#driver_details_div").html(response);

                        $("#driverModal").modal("show");
                        $('#driver_details_div').animate({
                            scrollTop: $('#driver_details_div').position().top
                        }, 'slow');
                    }

                });
             });

              $(document).on("change",".select_adults,.select_child,.select_activities_adults,.select_activities_child",function(event)
              {
                var classnames=event.target.className;
                if(classnames.indexOf("select_adults")!=-1 || classnames.indexOf("select_child")!=-1)
                    {
                        setTimeout(function()
                        {
                         swal("Information","Please change rooms of selected hotel based on number of guests in this package. Please ignore if already done.");
                          $(".day-count").each(function(){ $(this).addClass("show"); });
                        $(".hotels_select").attr("tabindex",-1).focus()   
                    },1500);
                        
                    }
                $("#calculate_cost").trigger("click");

              });


              //fetch itinerary cost
              $(document).on("click","#calculate_cost",function()
              {
                 $("#loaderModal").show();
                var formdata=$("#itinerary_form").serialize();
                 $.ajax({
                    url:"{{route('itinerary-booking-whole-cost')}}",
                    type:"POST",
                    data:formdata,
                    success:function(response)
                    {
                        var response1=response.split("--");
                         $("#total_cost_w_markup").val(response1[0]);
                       $("#total_cost_w_agent_markup").val(response1[1]);
                       $("#total_cost").val(response1[2]);
                       $("#total_cost_text").text(response1[2]);
                       console.log(response);

                        $("#loaderModal").hide();
                    }

                });


              });
              //end of fetch itinerary cost
    </script>
    @if(isset($saved))
    <script>    
    $(document).ready(function(){
         $("#calculate_cost").trigger("click");
    });</script>
    @endif

    <script> 
    $(document).on("click","#bookMyPackage",function(){ 
        $(".day-count").each(function(){ $(this).addClass("show"); 
    }); });


    $('#itinerary_form').submit(function(e)
    {
        var warning=$("#warning_count").val();
        if(warning<=0)
        {

        }
        else
        {
            e.preventDefault();
             $(".day-count").each(function(){ $(this).addClass("show"); });
            swal("Error!","Please Change those services that are not available on selected dates","error");

        }
    });

    //for sticky price section
     $(window).scroll(function(e){ 
  var $el = $('.sticky-sidebar'); 
  var isPositionFixed = ($el.css('position') == 'fixed');
  if ($(this).scrollTop() > 200 && !isPositionFixed){ 
    $el.css({'position': 'fixed', 'top': '0px',"width": "24%"}); 

  }
  if ($(this).scrollTop() < 200 && isPositionFixed){
    $el.css({'position': 'static', 'top': '0px',"width": "100%"}); 
  } 
});
</script>    
<script>
    $(document).on("change",".room_select",function()
{
    var room_select_type=$(this).attr("id");

    var actual_id=room_select_type.split("__")[1];

    var room_option_count=parseInt($("#"+room_select_type+" option").length)-1;
    var room_value=$(this).val();
    var room_total=0;
    $(".room_select__"+actual_id).each(function()
    { var id=$(this).attr("id");
        if(id!=room_select_type)
        {
        room_total+=parseInt($(this).val());
    }
    });


   $(".room_select__"+actual_id).each(function()
    {

        var id=$(this).attr("id");
        if(id!=room_select_type)
        {
            // alert((room_option_count-room_value));
            $("#"+id+" option").each(function(i,value)
            {
                if(i>(room_option_count-room_value))
                {
                    $(this).attr("disabled","disabled");
                }
                else
                {
                     $(this).removeAttr("disabled");
                }
            });
        }
        else
        {

          $("#"+id+" option").each(function(i,value)
            {
                if(i>(room_option_count-room_total))
                {
                    $(this).attr("disabled","disabled");
                }
                else
                {
                     $(this).removeAttr("disabled");
                }
            });  
        }
        
    });


});
</script>