<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    div#cke_1_contents {

        height: 250px !important;

    }
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('mains.includes.nav')

	<div class="content">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title">Food / Drinks Management</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item" aria-current="page">Dashboard</li>
							<li class="breadcrumb-item active" aria-current="page">Food / Drinks Management
							</li>
						</ol>
					</nav>
				</div>
			</div>

		</div>
	</div>


	<div class="row">


@if($rights['add']==1 || $rights['view']==1)
		<div class="col-12">
			<div class="box">

				<div class="box-body">
					<!-- Nav tabs -->
					<!-- <ul class="nav nav-pills mb-20">
						<li class=" nav-item"> <a href="#navpills-restaurant" id="navpills-restaurant-link" class="nav-link show active" data-toggle="tab"
								aria-expanded="false">Restaurants</a> </li>

					</ul> -->
					<!-- Tab panes -->
					<!-- <div class="tab-content">
						<div id="navpills-restaurant" class="tab-pane show active"> -->
							<div class="row">
									@if($rights['add']==1)
								<div class="col-sm-6 col-md-3">
									<div class="form-group">

										<a href="{{route('create-food')}}"><button type="button"
												class="btn btn-rounded btn-success">Create New Food / Drinks</button></a>
									</div>
								</div>
								@endif

								@if($rights['view']==1)		
								<div class="col-12">
									<div class="box">

										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table  id="restaurant_table" class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>S. No.</th>
															<th style="display:none">FOOD / DRINKS ID</th>
															<th>Name</th>
															<th>Price</th>
															<th>Discounted Price</th>
															<th>Delivery</th>
															<th>Featured</th>
															<th>Restaurant</th>
															<th>Menu Category</th>
															@if($rights['edit_delete']==1)
															<th>Status</th>
															<th>Approval</th>
															@endif
															@if($rights['edit_delete']==1 || $rights['view']==1)
															<th>Action</th>
															@endif
														</tr>
													</thead>
													<tbody>
														@php
														$srno=1;
														@endphp
															@foreach($get_food as $food)

												<tr id="tr_{{$food->restaurant_food_id}}">
													<td style="cursor: all-scroll;">{{$srno}}</td>
													<td style="display:none">{{$food->restaurant_food_id}}</td>
													<td>{{$food->food_name}}</td>
													<td>GEL {{$food->food_price}}</td>
													<td>@if($food->food_discounted_price!=null) GEL {{$food->food_discounted_price}} @else - @endif</td>
													<td>@if($food->food_available_for_delivery=="yes") <button type="button" class="btn btn-sm btn-primary">YES</button> @else <button type="button" class="btn btn-sm btn-danger">NO</button> @endif</td>
													<td>@if($food->food_featured=="yes") <button type="button" class="btn btn-sm btn-primary">YES</button> @else <button type="button" class="btn btn-sm btn-danger">NO</button> @endif</td>
													<td>{{$food->getRestaurant->restaurant_name}}</td>
													<td>{{$food->getMenuCategory->restaurant_menu_category_name}}</td>
														@if($rights['edit_delete']==1)
														@if(strpos($rights['admin_which'],'edit_delete')!==false)
														<td>
															@if($food->food_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_food_{{$food->restaurant_food_id}}">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_food_{{$food->restaurant_food_id}}">InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($food->food_approval_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($food->food_approval_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_food_{{$food->restaurant_food_id}}">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_food_{{$food->restaurant_food_id}}">Reject</button>
															@elseif($food->food_approval_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
															

														@elseif($food->food_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
														<td>
															@if($food->food_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_food_{{$food->restaurant_food_id}}">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_food_{{$food->restaurant_food_id}}">InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($food->food_approval_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($food->food_approval_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_food_{{$food->restaurant_food_id}}">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_food_{{$food->restaurant_food_id}}">Reject</button>
															@elseif($food->food_approval_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
															
														@elseif($food->food_created_by==Session::get('travel_users_id') && $food->food_role!="Supplier" && $rights['edit_delete']==1)
														<td>
															@if($food->food_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_food_{{$food->restaurant_food_id}}">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_food_{{$food->restaurant_food_id}}">InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($food->food_approval_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($food->food_approval_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_food_{{$food->restaurant_food_id}}">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_food_{{$food->restaurant_food_id}}">Reject</button>
															@elseif($food->food_approval_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
															@else
															<td>
															@if($food->food_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_food_{{$food->restaurant_food_id}}" disabled="disabled">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_food_{{$food->restaurant_food_id}}" disabled="disabled">InActive</button>
															@endif
														</td>
														<td style="white-space: nowrap;">
														@if($food->food_approval_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($food->food_approval_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_food_{{$food->restaurant_food_id}}" disabled="disabled">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_food_{{$food->restaurant_food_id}}" disabled="disabled">Reject</button>
															@elseif($food->food_approval_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
														@endif


														
															@endif

															@if($rights['edit_delete']==1 || $rights['view']==1)

													<td>
														@if(strpos($rights['admin_which'],'view')!==false)
														<a href="{{route('food-details',['food_id'=>$food->restaurant_food_id])}}" target="_blank"><i class="fa fa-eye"></i></a>
														@elseif($foods->food_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'view')!==false)
														<a href="{{route('food-details',['food_id'=>$food->restaurant_food_id])}}"><i class="fa fa-eye"></i></a>
														@elseif($foods->food_created_by==Session::get('travel_users_id') && $foods->food_role!="Supplier" && $rights['view']==1)
														<a href="{{route('food-details',['food_id'=>$food->restaurant_food_id])}}" target="_blank"><i class="fa fa-eye"></i></a>
														@endif

														@if(strpos($rights['admin_which'],'edit_delete')!==false)
														<a href="{{route('edit-food',['food_id'=>$food->restaurant_food_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>
														@elseif($foods->food_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
														<a href="{{route('edit-food',['food_id'=>$food->restaurant_food_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>
														@elseif($foods->food_created_by==Session::get('travel_users_id') && $foods->food_role!="Supplier" && $rights['edit_delete']==1)
														<a href="{{route('edit-food',['food_id'=>$food->restaurant_food_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>
														@endif

														

													</td>
													@endif

												</tr>
												@php
														$srno++;
														@endphp

												@endforeach

													</tbody>

												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
								@endif


							</div>
						<!-- </div>
					</div> -->
				</div>


				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
		@else
	<h4 class="text-danger">No rights to access this page</h4>
			
			@endif

		<!-- /.box -->

	</div>

</div>
</div>

</div>

</div>
@include('mains.includes.footer')

@include('mains.includes.bottom-footer')
<div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
<script>
	$(".datatable").DataTable();


    var table_restaurant = $('#restaurant_table').DataTable({
        // rowReorder: true,
         "lengthMenu": [[25, 50, 100,150,200,250,500,-1], [25, 50, 100,150,200,250,500,"All"]]
    });
 
  //   table_restaurant.on( 'row-reorder', function ( e, diff1, edit1 ) {
  //       var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 	// 	var new_data=[];
  //       for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
  //           var rowData = table_restaurant.row( diff1[i].node ).data();
 	// 		new_data.push({restaurant_food_id:rowData[1],new_order:diff1[i].newData});
  //       }

  // // console.log(new_data);
  //       if(new_data.length>0)
  //       {
  //         $.ajax({
  //       	url:"{{route('sort-restaurant')}}",
  //       	type:"POST",
  //       	data:{"_token":"{{csrf_token()}}",
  //       	"new_data":new_data},
  //       	success:function(response)
  //       	{
  //       		console.log(response);
  //       	}
  //       });	
  //       }
      
        
   
  //   });
	$(document).on("click",'.approve',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="food")
		{

			swal({
				title: "Are you sure?",
				text: "You want to approve this Food / Drinks!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-food-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_food_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');

								swal("Approved!", "Selected Food / Drinks has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.reject',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="food")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject this Food / Drinks!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-food-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_food_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');

								swal("Rejected!", "Selected Food / Drinks has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
	});

	$(document).on("click",'.active',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="food")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate this Food / Drinks!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-food-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_food_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_food_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Food / Drinks has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
	});
	$(document).on("click",'.inactive',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="food")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this Food / Drinks!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-food-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"restaurant_food_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_food_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Food / Drinks has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
	});

</script>
</body>
</html>
