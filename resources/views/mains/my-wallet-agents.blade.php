<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    .text-green
    {
        color:green;
    }
    .text-red
    {
        color:red;
    }
       div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Agent's Wallet</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item active" aria-current="page">Agent's Wallet</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                   @if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">All Agents</h4>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive" id="commission_data">
                                              <table id="agent_wallet_table" class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    <th>Sr. no</th>
                                                    <th>Company Name</th>
                                                    <th>Agent Name</th>
                                                    <th>Wallet Balance</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($agents_wallet_data as $wallet_data)
                                                  <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$wallet_data['agent_company']}}</td>
                                                    <td>{{$wallet_data['agent_fullname']}}</td>
                                                     <td id="wallet_amount_{{$wallet_data['agent_id']}}">{{$wallet_data['agent_total_wallet_amount']}}</td>
                                                     <td>@if($wallet_data['agent_status']==1) <button class="btn btn-sm btn-secondary">Active</button> @else <button class="btn btn-sm btn-default">Inactive</button> @endif</td>
                                                      <td>
                                                        <button class="btn btn-sm btn-primary add_money" id="add_money_{{$wallet_data['agent_id']}}">Add / Deduct Money</button>
                                                        <a href="{{route('my-wallet-agent',['agent_id'=>$wallet_data['agent_id']])}}" class="btn btn-sm btn-primary">Show Wallet</a>

                                                      </td>
                                                  </tr>

                                                  @endforeach

                                                </tbody>
                                              </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                                   @else
                                    <h4 class="text-danger">No rights to access this page</h4>
                                   @endif
                                </div>
                            </div>
                        </div>
                        @include('mains.includes.footer')
                        @include('mains.includes.bottom-footer')
                          <div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
   <div class="modal" id="actionModal" style="z-index: 99999;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Peform Operation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                              <form id="walletForm">
                                {{csrf_field()}}
                                <input type="hidden" name="action_id" id="action_id">
                               <div class="form-group">
                                    <label for="operation" id="operation_label">Select Operation</label>
                                    <select name="operation" id="operation" class="form-control">
                                      <option value="credit">Add Money</option>
                                      <option value="debit">Deduct Money</option>
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label for="operation_amount" id="operation_amount_label">Amount</label>
                                    <input type="text" class="form-control" id="operation_amount" name="operation_amount" placeholder="Enter Amount" onkeypress="javascript:return validateNumber(event)"  onpaste="javascript:return validateNumber(event)">
                                </div>
                                <div class="form-group">
                                    <label for="remarks" id="remarks_label">Remarks (if any)</label>
                                    <textarea name="remarks" id="remarks" class="form-control" rows="5"></textarea>
                                </div>
                                
                                
                                <button type="button" id="actionButton" class="btn btn-primary pull-right">Submit</button>
                              </form>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


                  <script>
                          $(document).ready(function()
                          {
                            $("#agent_wallet_table").DataTable({
                               "deferRender": true
                            });
                          });

                          $(document).on("click",'.add_money',function()
                          {
                            var id=this.id
                            var actual_id=id.split('_');

                            $("#action_id").val(actual_id[2]);
                            $("#actionModal").modal("show");
                          });

                          $(document).on("click","#actionButton",function()
                          {
                            var operation_amount=$("input[name='operation_amount']").val();
                            var agent_id=$("#action_id").val();

                            if(operation_amount.trim()=="")
                            {
                              swal("Amount cannot be empty")
                            }
                            else
                            {
                              var formdata=$("#walletForm").serialize()
                              $.ajax({
                                url:"{{route('agents-operation')}}",
                                type:"POST",
                                data:formdata,
                                success:function(response)
                                {
                                  if(response.indexOf("success")!=-1)
                                  {
                                    swal("Operation Performed Successfully");
                                    $("#wallet_amount_"+agent_id).text(response.split("_")[1]);

                                    setTimeout(function()
                                    {
                                      $("#operation_amount").val("");
                                      $("#actionModal").modal("hide");
                                    },500);
                                  }
                                  else if(response.indexOf("fail")!=-1)
                                  {
                                    swal("Operation cannot be performed now.Try Again.");
                                  }

                                }
                              });

                            }

                          });

                        </script>
                    </body>
                    </html>