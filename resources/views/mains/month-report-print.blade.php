<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    .text-green
    {
        color:green;
    }
    .text-red
    {
        color:red;
    }
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
 <div class="container-fluid">
     @if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
     <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Monthly Report for {{$month_name}}, {{$yearname}}</h4>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                          <h3>Bookings</h3>
                          <div class="table-responsive" id="booking_table">
                            {!!$bookings_data!!}
                        </div>

                    </div>
                    <div class="col-md-12">
                        <h3>Office Incomes</h3>
                        <div class="table-responsive" id="office_incomes_table">
                            {!!$incomes_data!!}
                        </div>

                    </div>

                    <div class="col-md-12">
                      <h3>Office Expenses</h3>
                      <div class="table-responsive" id="office_expenses_table">
                        {!!$expenses_data!!}
                    </div>
                </div>
                 <div class="col-md-12">
                    <h3>Final Calculation</h3>
                      <div class="table-responsive" id="office_expenses_table">
                        <table class="table table-bordered">
                          <tr>
                          <th>Particulars</th>
                          <th>Cr.</th>
                          <th>Dr.</th>
                        </tr>
                        <tr>
                          <td>Total Profit from Bookings</td>
                          <td> <span id="total_bookings_profit" class="text-green">GEL {{$total_bookings_profit}}</span></td>
                           <td></td>
                        </tr>
                        <tr>
                          <td>Total Office Incomes</td>
                          <td> <span id="total_office_incomes" class="text-green">GEL {{$total_office_incomes}}</span></td>
                           <td></td>
                        </tr>
                        <tr>
                          <td>Total Office Expenses</td>
                           <td></td>
                          <td> <span id="total_office_expenses" class="text-red">GEL {{$total_office_expenses}}</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <th>Profit</th>
                             <th>Loss</th>
                        </tr>
                          <tr>
                          <td><b>Total Profit/Loss for this month</b></td>
                          <td><span id="total_profit_loss_more" class="text-green">@if($total_profit_loss>=0) GEL {{abs($total_profit_loss)}} @endif</span></td>
                           <td><span id="total_profit_loss_loss" class="text-red">@if($total_profit_loss<0) GEL {{abs($total_profit_loss)}} @endif</span></td>
                        </tr>
                      </table>
                    </div>

                  </div>
            </div>

        </div>
    </div>
</div>
</div>
@else
<h4 class="text-danger">No rights to access this page</h4>
@endif
</div>

@include('mains.includes.bottom-footer')
@if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
<script>
    $(document).ready(function()
    {
      window.print();  
  });
</script>
@endif
</body>
</html>