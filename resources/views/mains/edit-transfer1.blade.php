@include('mains.includes.top-header')
<style>
    header.main-header {
        background: url("{{ asset('assets/images/color-plate/theme-purple.jpg') }}");
    }
/*  div#cke_1_contents {
height: 250px !important;
}*/
img.plus-icon {
    margin: 0 2px;
    display: inline !important;
}

</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Service Management</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                                <li class="breadcrumb-item" aria-current="page">Home</li>
                                                <li class="breadcrumb-item active" aria-current="page">Edit
                                                    Transfer
                                                </li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>

                            </div>
                        </div>
                        @if($rights['add']==1)
                        <div class="row">
                            <div class="col-12">
                                <div class="box">
                                    <div class="box-header with-border">
                                        <h4 class="box-title">EDIT TRANSFER</h4>
                                    </div>
                                    <div class="box-body">
                                        <form id="transfer_form" enctype="multipart/form-data" method="POST">
                                            {{csrf_field()}}
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <label>TRANSFER NAME <span class="asterisk">*</span></label>
                                                        <input type="text" class="form-control" placeholder="FIRST NAME "
                                                        name="transfer_name" id="transfer_name" value="{{$get_transfer->transfer_name}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                </div>
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <label>DESCRIPTION<span class="asterisk">*</span></label>
                                                        <textarea rows="5" cols="5" class="form-control"
                                                        placeholder="DESCRIPTION" name="transfer_description" id="transfer_description">{{$get_transfer->transfer_description}}</textarea>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                </div>
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <label for="transfer_supplier_name">SUPPLIER <span class="asterisk">*</span></label>
                                                        <select id="transfer_supplier_name" name="transfer_supplier_name" class="form-control select2" style="width: 100%;">
                                                            <option value="0" hidden>SELECT SUPPLIER</option>
                                                            @foreach($suppliers as $supplier)
                                                           @if($get_transfer->supplier_id==$supplier->supplier_id)
                                           <option value="{{$supplier->supplier_id}}" selected="selected">{{$supplier->supplier_name}}</option>  
                                           @else
                                           <option value="{{$supplier->supplier_id}}">{{$supplier->supplier_name}}</option>  
                                           @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                </div>
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-sm-12 col-md-12 col-lg-6">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>COUNTRY <span class="asterisk">*</span></label>
                                                                <select class="form-control select2" name="transfer_country"
                                                                id="transfer_country" style="width: 100%;">
                                                                <option selected="selected" hidden value="0">SELECT
                                                                COUNTRY</option>
                                                                   @foreach($countries as $country)
                                                                     @if(in_array($country->country_id,$countries_data))
                                                                     @if($country->country_id==$get_transfer->transfer_country)
                                                                     <option value="{{$country->country_id}}" selected="selected">{{$country->country_name}}</option>
                                                                     @else
                                                                     <option value="{{$country->country_id}}" >{{$country->country_name}}</option>
                                                                     @endif  
                                                                     @endif
                                                                   @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="transfer_type">Transfer Type <span class="asterisk">*</span></label>
                                                    <select id="transfer_type" name="transfer_type" class="form-control " style="width: 100%;">
                                                        <option value="0" hidden>SELECT TRANSFER TYPE</option>
                                                        <option value="airport" @if($get_transfer->transfer_type=="airport") selected @endif>Airport Transfer</option>
                                                        <option value="city" @if($get_transfer->transfer_type=="city") selected @endif>City Transfer</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div id="airport_city_transfer_div_loader" style="display:none;">
                                            <h4 class='text-center'><strong>Loading...</strong></h4>
                                        </div>


                                        <div class="row mb-10" id="airport_city_transfer_div">
                                            @if($get_transfer->transfer_type=="airport")
                                            <div class="col-md-12">
                                              <div class='row'>
                                                  <div class='col-md-3'>
                                                      <p>Select Airport</p>
                                                  </div>
                                                  <div class='col-md-3'>
                                                      <p>Select City</p>
                                                  </div>
                                                  <div class='col-md-5'>
                                                      <div class='row'>
                                                          <div class='col-md-8'>
                                                              <p>Vehicle Type</p>
                                                          </div>
                                                          <div class='col-md-4'>
                                                              <p>Vehicle Cost</p>
                                                          </div>
                                                      </div>


                                                  </div>
                                              </div>
                                              @php
                                              $transfer_count=1;
                                              @endphp
                                            @foreach($get_transfer_details as $transfer_details)
                                            <div class='transfer_div' id='transfer_div{{$transfer_count}}'>
                                              <div class='row mt-15 mb-10'>
                                              <div class='col-md-3'>
                                              <select class='form-control select2' name='transfer_from[]' id='transfer_from__{{$transfer_count}}' style='width:100%'>
                                              <option value='0'>--Select Airport--</option>";
                                              @foreach($fetch_airports as $airports)
                                              @if($airports->airport_master_id==$transfer_details->from_city_airport)
                                                <option value='{{$airports->airport_master_id}}' selected="selected">{{$airports->airport_master_name}}</option>
                                                @else
                                                 <option value='{{$airports->airport_master_id}}'>{{$airports->airport_master_name}}</option>

                                                @endif
                                              @endforeach
                                              </select>
                                              </div>

                                              <div class='col-md-3'>
                                              <select class='form-control select2' name='transfer_to[]' id='transfer_to__{{$transfer_count}}' style='width:100%'>
                                              <option value='0'>--Select City--</option>
                                              @foreach($fetch_cities as $cities)
                                              @if($cities->id==$transfer_details->to_city_airport)
                                              <option value='{{$cities->id}}' selected="selected">{{$cities->name}}</option>
                                              @else
                                              <option value='{{$cities->id}}'>{{$cities->name}}</option>
                                              @endif
                                              @endforeach
                                              </select>
                                              </div>
                                              <div class='col-md-5'>
                                                @php
                                            $transfer_tariff_cost=unserialize($transfer_details->transfer_vehicle_tariff);
                                            @endphp
                                            @for($transfer_vehicle_count=0;$transfer_vehicle_count <=count($transfer_tariff_cost[0])-1;$transfer_vehicle_count++)
                                                            @foreach($fetch_vehicle_type as $vehicle_type)
                                                                @if($vehicle_type->vehicle_type_id==$transfer_tariff_cost[0][$transfer_vehicle_count])
                                                            <div class='row'>
                                                        <div class='col-md-8'>
                                                            <input type='hidden'  class='vehicle_type_name' name='transfer_vehiclename[{{($transfer_count-1)}}][]' value='{{$transfer_tariff_cost[0][$transfer_vehicle_count]}}'>
                                                            <input type='text' class='form-control' value='{{$vehicle_type->vehicle_type_name}}' readonly='readonly'>  
                                                         </div>  
                                                        <div class='col-md-4'>
                                                            <input type='text' class='form-control vehicle_type_cost' name='transfer_vehiclecost[{{($transfer_count-1)}}][]' value='{{$transfer_tariff_cost[1][$transfer_vehicle_count]}}' onkeypress='javascript:return validateNumber(event)'> </div>
                                                        </div>
                                                    
                                                                 @endif
                                                           @endforeach  
                                            @endfor
                                            @foreach($fetch_vehicle_type as $vehicle_type)

                                            @if(!in_array($vehicle_type->vehicle_type_id,$transfer_tariff_cost[0]))
                                                <div class='row'>
                                                    <div class='col-md-8'><input type='hidden' name='tour_vehiclename[{{($transfer_count-1)}}][]' value='{{$vehicle_type->vehicle_type_id}}'>
                                                     <input type='text' class='form-control' value='{{$vehicle_type->vehicle_type_name}}' readonly='readonly'>  
                                                 </div>
                                                 <div class='col-md-4'>
                                                     <input type='text' class='form-control' name='tour_guide_cost[{{($transfer_count-1)}}][]' value='0' onkeypress='javascript:return validateNumber(event)'> </div>
                                                 </div>
                                                 @endif
                                                 @endforeach  
    
                                           
                                              <br></div>
                                              </div>
                                              <div class='col-sm-12 col-md-12 add_more_transfer_div'>
                                                 @if(count($get_transfer_details)==1)

                                               <img id='add_more_transfer{{($transfer_count)}}' class='add_more_transfer plus-icon'   style='margin-left: auto;' src='{{asset("assets/images/add_icon.png")}}'>

                                               @endif



                                               @if(($transfer_count)>1)

                                               @if($transfer_count==count($get_transfer_details))

                                               <img id='remove_transfer{{($transfer_count)}}' class='remove_transfer minus-icon'   style='margin-left: auto;' src='{{asset("assets/images/minus_icon.png")}}'>

                                               <img id='add_more_transfer{{($transfer_count)}}' class='add_more_transfer plus-icon'   style='margin-left: auto;' src='{{asset("assets/images/add_icon.png")}}'>

                                               @else

                                               <img id='remove_transfer{{($transfer_count)}}' class='remove_transfer minus-icon'   style='margin-left: auto;' src='{{asset("assets/images/minus_icon.png")}}'>

                                               @endif

                                               @endif

                                
                                              </div>
                                        
                                                 </div>
                                                @php
                                                $transfer_count++;
                                                @endphp


                                            @endforeach
                                        </div>

                                            @else

                                              <div class="col-md-12">
                                              <div class='row'>
                                                  <div class='col-md-3'>
                                                      <p>From City</p>
                                                  </div>
                                                  <div class='col-md-3'>
                                                      <p>To City</p>
                                                  </div>
                                                  <div class='col-md-5'>
                                                      <div class='row'>
                                                          <div class='col-md-8'>
                                                              <p>Vehicle Type</p>
                                                          </div>
                                                          <div class='col-md-4'>
                                                              <p>Vehicle Cost</p>
                                                          </div>
                                                      </div>


                                                  </div>
                                              </div>
                                              @php
                                              $transfer_count=1;
                                              @endphp
                                            @foreach($get_transfer_details as $transfer_details)
                                            <div class='transfer_div' id='transfer_div{{$transfer_count}}'>
                                              <div class='row mt-15 mb-10'>
                                              <div class='col-md-3'>
                                              <select class='form-control select2' name='transfer_from[]' id='transfer_from__{{$transfer_count}}' style='width:100%;'>
                                              <option value='0'>--Select City--</option>";
                                              @foreach($fetch_cities as $cities)
                                              @if($cities->id==$transfer_details->from_city_airport)
                                              <option value='{{$cities->id}}' selected="selected" >{{$cities->name}}</option>
                                              @else
                                              <option value='{{$cities->id}}' disabled>{{$cities->name}}</option>
                                              @endif
                                              @endforeach
                                              </select>
                                              </div>

                                              <div class='col-md-3'>
                                              <select class='form-control select2' name='transfer_to[]' id='transfer_to__{{$transfer_count}}' style='width:100%;'>
                                              <option value='0'>--Select City--</option>
                                              @foreach($fetch_cities as $cities)
                                              @if($cities->id==$transfer_details->to_city_airport)
                                              <option value='{{$cities->id}}' selected="selected">{{$cities->name}}</option>
                                              @else
                                              <option value='{{$cities->id}}' disabled>{{$cities->name}}</option>
                                              @endif
                                              @endforeach
                                              </select>
                                              </div>
                                              <div class='col-md-5'>
                                                @php
                                            $transfer_tariff_cost=unserialize($transfer_details->transfer_vehicle_tariff);
                                            @endphp
                                            @for($transfer_vehicle_count=0;$transfer_vehicle_count <=count($transfer_tariff_cost[0])-1;$transfer_vehicle_count++)
                                                            @foreach($fetch_vehicle_type as $vehicle_type)
                                                                @if($vehicle_type->vehicle_type_id==$transfer_tariff_cost[0][$transfer_vehicle_count])
                                                            <div class='row'>
                                                        <div class='col-md-8'>
                                                            <input type='hidden'  class='vehicle_type_name' name='transfer_vehiclename[{{($transfer_count-1)}}][]' value='{{$transfer_tariff_cost[0][$transfer_vehicle_count]}}'>
                                                            <input type='text' class='form-control' value='{{$vehicle_type->vehicle_type_name}}' readonly='readonly' >  
                                                         </div>  
                                                        <div class='col-md-4'>
                                                            <input type='text' class='form-control vehicle_type_cost' name='transfer_vehiclecost[{{($transfer_count-1)}}][]' value='{{$transfer_tariff_cost[1][$transfer_vehicle_count]}}' onkeypress='javascript:return validateNumber(event)'> </div>
                                                        </div>
                                                    
                                                                 @endif
                                                           @endforeach  
                                            @endfor
                                            @foreach($fetch_vehicle_type as $vehicle_type)

                                            @if(!in_array($vehicle_type->vehicle_type_id,$transfer_tariff_cost[0]))
                                                <div class='row'>
                                                    <div class='col-md-8'><input type='hidden' name='tour_vehiclename[{{($transfer_count-1)}}][]' value='{{$vehicle_type->vehicle_type_id}}'>
                                                     <input type='text' class='form-control' value='{{$vehicle_type->vehicle_type_name}}' readonly='readonly'>  
                                                 </div>
                                                 <div class='col-md-4'>
                                                     <input type='text' class='form-control' name='tour_guide_cost[{{($transfer_count-1)}}][]' value='0' onkeypress='javascript:return validateNumber(event)'> </div>
                                                 </div>
                                                 @endif
                                                 @endforeach  
    
                                           
                                              <br></div>
                                              </div>
                                              <div class='col-sm-12 col-md-12 add_more_transfer_div'>
                                               @if(count($get_transfer_details)==1)

                                               <!-- <img id='add_more_transfer{{($transfer_count)}}' class='add_more_transfer plus-icon'   style='margin-left: auto;' src='{{asset("assets/images/add_icon.png")}}'> -->

                                               @endif



                                               @if(($transfer_count)>1)

                                               @if($transfer_count==count($get_transfer_details))

                                              <!--  <img id='remove_transfer{{($transfer_count)}}' class='remove_transfer minus-icon'   style='margin-left: auto;' src='{{asset("assets/images/minus_icon.png")}}'>

                                               <img id='add_more_transfer{{($transfer_count)}}' class='add_more_transfer plus-icon'   style='margin-left: auto;' src='{{asset("assets/images/add_icon.png")}}'> -->

                                               @else

                                              <!--  <img id='remove_transfer{{($transfer_count)}}' class='remove_transfer minus-icon'   style='margin-left: auto;' src='{{asset("assets/images/minus_icon.png")}}'> -->

                                               @endif

                                               @endif



                                              </div> 
                                        
                                                 </div>
                                                @php
                                                $transfer_count++;
                                                @endphp


                                            @endforeach
                                        </div>

                                            @endif
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row mb-10">
                                                    <div class="col-sm-12">
                                                        <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                        </div>
                                                        <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                            <i class="fa fa-plus-circle"></i> INCLUSIONS <span class="asterisk">*</span></h4>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="box">
                                                                <!-- /.box-header -->
                                                                <div class="box-body">
                                                                    <textarea class="form-control" id="transfer_inclusions" name="transfer_inclusions">{{$get_transfer->transfer_inclusions}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row mb-10">
                                                        <div class="col-sm-12">
                                                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                            </div>
                                                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                                <i class="fa fa-plus-circle"></i> EXCLUSIONS <span class="asterisk">*</span></h4>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="box">
                                                                    <!-- /.box-header -->
                                                                    <div class="box-body">
                                                                        <textarea class="form-control" id="transfer_exclusions" name="transfer_exclusions">{{$get_transfer->transfer_exclusions}}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row mb-10">
                                                            <div class="col-sm-12">
                                                                <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                                </div>
                                                                <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                                    <i class="fa fa-plus-circle"></i> CANCELLATION POLICY <span class="asterisk">*</span></h4>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="box">
                                                                        <!-- /.box-header -->
                                                                        <div class="box-body">
                                                                            <textarea class="form-control" id="transfer_cancellation" name="transfer_cancellation">{{$get_transfer->transfer_cancellation}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="row mb-10">
                                                                <div class="col-sm-12">
                                                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                                    </div>
                                                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                                        <i class="fa fa-plus-circle"></i> TERMS AND CONDITIONS <span class="asterisk">*</span> </h4>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <div class="box">
                                                                            <!-- /.box-header -->
                                                                            <div class="box-body">
                                                                                <textarea class="form-control" id="transfer_terms_conditions" name="transfer_terms_conditions">{{$get_transfer->transfer_terms_and_conditions}}</textarea>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row mb-10">
                                                            <div class="col-md-12">
                                                                <div class="box-header with-border"
                                                                style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
                                                                <input type="hidden" name="transfer_id" value="{{$get_transfer->transfer_id}}">
                                                                <button type="button" id="update_transfer"
                                                                class="btn btn-rounded btn-primary mr-10">Update</button>
                                                                <button type="button" id="discard_transfer"
                                                                class="btn btn-rounded btn-primary">Discard</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <h4 class="text-danger">No rights to access this page</h4>
                                @endif
                            </div>
                        </div>
                    </div>
        @include('mains.includes.footer')
        @include('mains.includes.bottom-footer')
    <script>
        function previewFile(data) {
            if (data == "logo") {
                var preview = document.getElementById('logo_preview');
                var file = document.querySelector('input[name="transfer_logo_file"]').files[0];
            } else {
                var preview = document.getElementById('certificate_preview');
                var file = document.querySelector('input[name="transfer_certificate_file"]').files[0];
            }
            var reader = new FileReader();
            reader.onloadend = function () {
                preview.src = reader.result;
                preview.style.display = "block";
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        }
    </script>
    <script>
        function dateshow()
        {
            var date = new Date();
            date.setDate(date.getDate());
            $('.transfer_validity_from').datepicker({
                autoclose:true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                startDate:date
            });
            $('.transfer_validity_from').on('change', function () {
                var date_from_id=this.id;
                var date_id=date_from_id.split("transfer_validity_from");
                var date_from = $("#transfer_validity_from"+date_id[1]).datepicker("getDate");
                var date_to = $("#transfer_validity_to"+date_id[1]).datepicker("getDate");
                if(!date_to)
                {
                    $("#transfer_validity_to"+date_id[1]).datepicker("setDate",date_from);
                }
                else if(date_to.getTime()<date_from.getTime())
                {
                    $("#transfer_validity_to"+date_id[1]).datepicker("setDate",date_from);
                }
            });
            $('.transfer_validity_to').datepicker({
                autoclose:true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                startDate:date
            });
            $('.transfer_validity_to').on('change', function () {
                var date_to_id=this.id;
                var date_id=date_to_id.split("transfer_validity_to");
                var date_from = $("#transfer_validity_from"+date_id[1]).datepicker("getDate");
                var date_to = $("#transfer_validity_to"+date_id[1]).datepicker("getDate");
                if(!date_from)
                {
                    $("#transfer_validity_from"+date_id[1]).datepicker("setDate",date_to);
                }
                else if(date_to.getTime()<date_from.getTime())
                {
                    $("#transfer_validity_from"+date_id[1]).datepicker("setDate",date_to);
                }
            });
        }
        $(document).ready(function()
        {
            CKEDITOR.replace('transfer_exclusions');
            CKEDITOR.replace('transfer_inclusions');
            CKEDITOR.replace('transfer_cancellation');
            CKEDITOR.replace('transfer_terms_conditions');
            $('.select2').select2();
            var date = new Date();
            date.setDate(date.getDate());
            $('#blackout_days').datepicker({
                multidate: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                startDate:date
            });
            dateshow();
        });
    </script>
    <script>
        $(document).on("click","#discard_transfer",function()
        {
          window.history.back();
        });
      
        $(document).on("change","#transfer_supplier_name",function()
        {
            if($("#transfer_supplier_name").val()!="0")
            {
                var supplier_id=$(this).val();
                $.ajax({
                    url:"{{route('search-supplier-country')}}",
                    type:"GET",
                    data:{"supplier_id":supplier_id},
                    success:function(response)
                    {
                        $("#transfer_country").html(response);
                        $('#transfer_country').select2();
                        $("#transfer_country").prop("disabled",false);
                    }
                });
            }
        });
        $(document).on("change","#transfer_country",function()
        {
            if($("#transfer_country").val()!="0")
            {
                var country_id=$(this).val();
                $.ajax({
                    url:"{{route('search-country-cities')}}",
                    type:"GET",
                    data:{"country_id":country_id},
                    success:function(response)
                    {
                        $("#transfer_city").html(response);
                        $('#transfer_city').select2();
                        $("#city_div").show();
                    }
                });
            }
        });
        $(document).on("change","#transfer_type",function()
        {
            var transfer_type=$(this).val();
            $("#airport_city_transfer_div_loader").show();
            $("#airport_city_transfer_div").html("");

            if(transfer_type=="airport")
            {
                var country_id=$("#transfer_country").val();
                $.ajax({
                    url:"{{route('fetchAirportTransferData')}}",
                    type:"GET",
                    data:{"country_id":country_id},
                    success:function(response)
                    {
                        $("#airport_city_transfer_div").html(response);
                        $("#transfer_from__1").select2();
                        $("#transfer_to__1").select2();
                        $("#airport_city_transfer_div").show();
                         $("#airport_city_transfer_div_loader").hide();
                    },
                    complete: function() {
                     $("#airport_city_transfer_div_loader").hide();
                 }
                });
            }
            else if(transfer_type=="city")
            {
                var country_id=$("#transfer_country").val();
                $.ajax({
                    url:"{{route('fetchCityTransferData')}}",
                    type:"GET",
                    data:{"country_id":country_id},
                    success:function(response)
                    {
                        $("#airport_city_transfer_div").html(response);
                        $("#transfer_from__1").select2();
                        $("#transfer_to__1").select2();
                        $("#airport_city_transfer_div").show();
                         
                    },
                    complete: function() {
                     $("#airport_city_transfer_div_loader").hide();
                 }
                });
            }
            else
            {

            }

        });
        $(document).on("click",".add_more_transfer",function()
        {
        var clone_policies = $(".transfer_div:last").clone();
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var newer_id = $(".transfer_div:last").attr("id");
        new_id = newer_id.split('transfer_div');
        old_id=parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;
        clone_policies.find("select[name='transfer_from[]']").attr("id", "transfer_from__" + new_id).val("0").select2();
          clone_policies.find(".select2-container").slice(1).remove();
        clone_policies.find("select[name='transfer_from[]']").parent().parent().parent().attr("id","transfer_div" + new_id);
        clone_policies.find("select[name='transfer_to[]']").attr("id", "transfer_to__" + new_id).val("0").select2();
      
        $("#transfer_div"+old_id).find(".add_more_transfer_div").html("");
        clone_policies.find(".vehicle_type_name").each(function()
        {
            $(this).attr("name","transfer_vehiclename["+old_id+"][]");
        });
        clone_policies.find(".vehicle_type_cost").each(function()
        {
            $(this).attr("name","transfer_vehiclecost["+old_id+"][]").val("0");

        });
        if(old_id>1)
        {
        $("#transfer_div"+old_id).find(".add_more_transfer_div").append('<img id="remove_transfer'+old_id+'" class="remove_transfer minus-icon" src="'+minus_url+'" style="margin-left: auto;">');
        }
        clone_policies.find(".add_more_transfer_div").html('');
        clone_policies.find(".add_more_transfer_div").append(' <img id="remove_transfer'+new_id+'" class="remove_transfer minus-icon"  src="'+minus_url+'"   style="margin-left: auto;"> <img id="add_more_transfer'+new_id+'" class="add_more_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;"> ');
        $(".transfer_div:last").after(clone_policies);
        });

        $(document).on("click", ".remove_transfer", function () {
            var id = this.id;
            var split_id = id.split('remove_transfer');
            $("#transfer_div" + split_id[1]).remove();
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var last_id = $(".transfer_div:last").attr("id");
            old_id = last_id.split('transfer_div');
            old_id=parseInt(old_id[1]);
            if(old_id>1)
            {
                $("#transfer_div"+old_id).find(".add_more_transfer_div").html("");
                $("#transfer_div"+old_id).find(".add_more_transfer_div").append('<img id="remove_transfer'+old_id+'" class="remove_transfer minus-icon"  src="'+minus_url+'"   style="margin-left: auto;"> <img id="add_more_transfer'+old_id+'" class="add_more_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;">');
            }
            else
            {
                $("#transfer_div"+old_id).find(".add_more_transfer_div").html("");
                $("#transfer_div"+old_id).find(".add_more_transfer_div").append(' <img id="add_more_transfer'+old_id+'" class="add_more_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;">');
            }
        });
    </script>
    <script>
        $("#transfer_country").on("change", function () {
            if ($(this).val() != "0") {
                $("#city_div").show();
            }
        });
        $("#discard_guide").on("click", function ()
        {
            window.history.back();
        });
        $(document).on("click", "#update_transfer", function ()
        {
            var transfer_name = $("#transfer_name").val();
            var transfer_description = $("#transfer_description").val();
            var transfer_supplier_name = $("#transfer_supplier_name").val();
            var transfer_country = $("#transfer_country").val();
            var transfer_type = $("#transfer_type").val();
            var transfer_inclusions= CKEDITOR.instances.transfer_inclusions.getData();
            var transfer_exclusions=CKEDITOR.instances.transfer_exclusions.getData();
            var transfer_cancellation=CKEDITOR.instances.transfer_cancellation.getData();
            var transfer_terms_conditions=CKEDITOR.instances.transfer_terms_conditions.getData();
            if (transfer_name.trim() == "")
            {
                $("#transfer_name").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_name").css("border", "1px solid #9e9e9e");
            }
            if (transfer_description.trim() == "")
            {
                $("#transfer_description").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_description").css("border", "1px solid #9e9e9e");
            }
           
            if (transfer_supplier_name.trim() == "0")
            {
                $("#transfer_supplier_name").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_supplier_name").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }
            if (transfer_country.trim() == "0")
            {
                $("#transfer_country").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_country").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }
            if (transfer_type.trim() == "0")
            {
                $("#transfer_type").css("border", "1px solid #cf3c63");
            } else
            {
                $("#transfer_type").css("border", "1px solid #9e9e9e");
            }

            if(transfer_type.trim() != "0")
            {
                var transfer_from_error=0;
                var transfer_to_error=0;
                var vehicle_type_cost_error=0;
                $("select[name='transfer_from[]']").each(function()
                {
                    if($(this).val()=="0")
                    {
                        $(this).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
                        $(this).parent().find(".select2-selection").focus();
                        transfer_from_error++;
                    }
                    else
                    {
                      $(this).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
                    }
                });
                $("select[name='transfer_to[]']").each(function()
                {
                    if($(this).val()=="0")
                    {
                        $(this).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
                        $(this).parent().find(".select2-selection").focus();
                        transfer_to_error++;
                    }
                    else
                    {
                      $(this).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
                    }
                });
                $(".vehicle_type_cost").each(function()
                {
                    if($(this).val()=="")
                    {
                        $(this).css("border", "1px solid #cf3c63");
                        $(this).focus();
                        vehicle_type_cost_error++;
                    }
                    else
                    {
                        $(this).css("border", "1px solid #9e9e9e");
                    }
                });
            }


            if (transfer_inclusions.trim() == "")
            {
                $("#cke_transfer_inclusions").css("border", "1px solid #cf3c63");
            } else
            {
                $("#cke_transfer_inclusions").css("border", "1px solid #9e9e9e");
            }
            if (transfer_exclusions.trim() == "")
            {
                $("#cke_transfer_exclusions").css("border", "1px solid #cf3c63");
            } else
            {
                $("#cke_transfer_exclusions").css("border", "1px solid #9e9e9e");
            }

            if (transfer_cancellation.trim() == "")
            {
                $("#cke_transfer_cancellation").css("border", "1px solid #cf3c63");
            } else
            {
                $("#cke_transfer_cancellation").css("border", "1px solid #9e9e9e");
            }
            if (transfer_terms_conditions.trim() == "")
            {
                $("#cke_transfer_terms_conditions").css("border", "1px solid #cf3c63");
            } else
            {
                $("#cke_transfer_terms_conditions").css("border", "1px solid #9e9e9e");
            }
        
        if (transfer_name.trim() == "") {
            $("#transfer_name").focus();
        } else if (transfer_description.trim() == "") {
            $("#transfer_description").focus();
        }  else if (transfer_supplier_name.trim() == "0") {
            $("#transfer_supplier_name").parent().find(".select2-selection").focus();
        } else if (transfer_country.trim() == "0") {
            $("#transfer_country").parent().find(".select2-selection").focus();
        } 
         else if (transfer_type.trim() == "0") {
            $("#transfer_type").focus();
        } 
        else if (transfer_from_error > 0) {
        }
        else if (transfer_to_error > 0) {
        }
         else if (vehicle_type_cost_error > 0) {
        }
        else if(transfer_inclusions.trim()=="")
        {
            $("#cke_transfer_inclusions").focus();
        }
        else if(transfer_exclusions.trim()=="")
        {
            $("#cke_transfer_exclusions").focus();
        }
        else if(transfer_cancellation.trim()=="")
        {
            $("#cke_transfer_cancellation").focus();
        }
        else if(transfer_terms_conditions.trim()=="")
        {
            $("#cke_transfer_terms_conditions").focus();
        }
        else
        {
            $("#update_transfer").prop("disabled",true);
            var formdata = new FormData($("#transfer_form")[0]);
            formdata.append("transfer_inclusions",transfer_inclusions);
            formdata.append("transfer_exclusions",transfer_exclusions);
            formdata.append("transfer_cancellation",transfer_cancellation);
            formdata.append("transfer_terms_conditions",transfer_terms_conditions);
            $.ajax({
                url: "{{route('update-transfer')}}",
                enctype: 'multipart/form-data',
                data: formdata,
                type: "POST",
                processData: false,
                contentType: false,
                success: function (response)
                {
                    if (response.indexOf("success") != -1)
                    {
                        swal({
                            title: "Success",
                            text: "Transfer Updated Successfully !",
                            type: "success"
                        },
                        function () {
                            location.reload();
                        });
                    } else if (response.indexOf("fail") != -1)
                    {
                        swal("ERROR", "Transfer cannot be updated right now! ");
                    }
                    $("#update_transfer").prop("disabled", false);
                }
            });
        }
    });
    </script>
</body>
</html>
</body>
</html>