<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    .text-green
    {
        color:green;
    }
    .text-red
    {
        color:red;
    }
    #actionModal
{
    z-index: 99999;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Bookings</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item" aria-current="page">Bookings</li>
                                            <li class="breadcrumb-item active" aria-current="page">Booking Details
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($rights['view']==1)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Booking Details</h4>
                                </div>
                                <div class="box-body">
                                    @php
                                    $total_paid_amount=0;
                                    $total_expenses=0;
                                    $total_incomes=0;
                                    @endphp
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4>View Booking Details</h4>
                                            <div class="table-responsive">
                                                <table class="table">
                                                    <tr>
                                                        <td><strong>BOOKING ID</strong> :<br> #{{$booking_details->booking_sep_id}}</td>
                                                        <td><strong>BOOKING TYPE</strong> :<br>{{strtoupper($booking_details->booking_type)}}</td>
                                                        @if($booking_details->booking_role=="AGENT")
                                                        <td> <strong>AGENT NAME</strong> :<br> @php
                                                            $fetch_agent=ServiceManagement::searchAgent($booking_details->booking_agent_id);
                                                            echo $fetch_agent['agent_name']." (".$fetch_agent['company_contact'].")";  
                                                        @endphp
                                                        <input type="hidden" id="agent_id" name="agent_id" value="{{$booking_details->booking_agent_id}}">
                                                        <input type="hidden" id="booking_role" name="booking_role" value="agent"></td>
                                                        @else
                                                         <td> <strong>OPERATOR NAME</strong> :<br> @php
                                                           $fetch_user=ServiceManagement::searchUser($booking_details->booking_agent_id);
                                                        echo $fetch_user['users_fname']." ".$fetch_user['users_lname'];  
                                                        @endphp
                                                     <input type="hidden" id="user_id" name="user_id" value="{{$booking_details->booking_agent_id}}">
                                                 <input type="hidden" id="booking_role" name="booking_role" value="operator"></td>

                                                        @endif
                                                        <td><strong>SUPPLIER NAME</strong> :<br> @php
                                                            $fetch_supplier=ServiceManagement::searchSupplier($booking_details->booking_supplier_id);
                                                            if($fetch_supplier['supplier_name']!="")
                                                            {
                                                            echo $fetch_supplier['supplier_name']." (".$fetch_supplier['company_contact'].")";  
                                                            }
                                                            else
                                                            {
                                                              echo "No Supplier mentioned";
                                                            }
                                                        @endphp</td>
                                                    </tr>
                                                    <tr>
                                                        <td><strong>CUSTOMER NAME</strong> :<br> {{$booking_details->customer_name}}</td>
                                                        <td><strong>CUSTOMER EMAIL</strong> :<br> {{$booking_details->customer_email}}</td>
                                                        <td><strong>CUSTOMER CONTACT</strong> :<br> {{$booking_details->customer_contact}}</td>
                                                        <td><strong>CUSTOMER ADDRESS</strong> :<br> {{$booking_details->customer_address}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td> <strong>BOOKING DATE</strong> :<br> {{date('d/m/Y',strtotime($booking_details->booking_date))}}</td>
                                                        <td> <strong>BOOKING TIME</strong> :<br> {{date('h:i a',strtotime($booking_details->booking_time))}}</td>
                                                        <td><strong>SERVICE FROM DATE</strong> :<br> {{date('d/m/Y',strtotime($booking_details->booking_selected_date))}}</td>
                                                        <td><strong>SERVICE TO DATE</strong> :<br>
                                                            @if($booking_details->booking_selected_to_date==null)
                                                            {{date('d/m/Y',strtotime($booking_details->booking_selected_date))}}
                                                            @else
                                                            {{date('d/m/Y',strtotime($booking_details->booking_selected_to_date))}}
                                                        @endif</td>
                                                    </tr>
                                                    <tr>
                                                        <td> <strong>BOOKING STATUS</strong> :<br>
                                                              @if($booking_details->booking_status==2)
                                                            <span class="text-info">Cancelled</span>
                                                            @elseif($booking_details->booking_status==0)
                                                            <span class="text-warning">Pending</span>
                                                            @elseif($booking_details->booking_admin_status==1)
                                                            <span class="text-green">Confirmed</span>
                                                            @elseif($booking_details->booking_admin_status==2)
                                                            <span class="text-danger">Rejected</span>
                                                             @elseif($booking_details->booking_status==1)
                                                            <span class="text-warning">Pending</span>
                                                            @endif
                                                        </td>
                                                        <td> <strong>ADMIN STATUS</strong> :<br>
                                                            @if($booking_details->booking_status==2)
                                                            <span class="text-info">Cancelled</span>
                                                            @elseif($booking_details->booking_admin_status==1)
                                                            <span class="text-green">Approved</span>
                                                            @elseif($booking_details->booking_admin_status==0)
                                                            <span class="text-warning">Pending</span>
                                                            @elseif($booking_details->booking_admin_status==2)
                                                            <span class="text-danger">Rejected</span>
                                                            @endif
                                                        </td>
                                                        <td> <strong>SUPPLIER STATUS</strong> :<br>
                                                            @if($booking_details->booking_status==2)
                                                            <span class="text-info">Cancelled</span>
                                                            @elseif($booking_details->booking_supplier_status==0)
                                                            <span class="text-warning">Pending</span>
                                                            @elseif($booking_details->booking_supplier_status==1)
                                                            <span class="text-green">Approved</span>
                                                            @elseif($booking_details->booking_supplier_status==2)
                                                           <span class="text-dangers">Rejected</span>
                                                            @endif
                                                        </td>
                                                        <td> <strong>FULL PAYMENT</strong> :<br>
                                                            @if($booking_details->itinerary_status==null)
                                                            @if($booking_details->booking_final_amount_status==0)
                                                            <span class="text-warning">Pending</span>
                                                            @elseif($booking_details->booking_final_amount_status==1)
                                                            <span class="text-green">Confirmed</span>
                                                            @endif
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td> <strong>BOOKING REMARKS</strong> :<br>
                                                            {{$booking_details->booking_remarks}}
                                                        </td>
                                                        <td> <strong>INSTALLMENTS</strong>: <br>
                                                         @if(($booking_details->itinerary_status==0 || $booking_details->itinerary_status==null))
                                                        <button id="installment_{{$booking_details->booking_sep_id}}" class="btn btn-rounded btn-primary btn-sm create_installment" @if($booking_details->booking_admin_status!=1) disabled @endif>Installments</button>
                                                        @elseif($booking_details->booking_final_amount_status==1)
                                                            Completed 
                                                        @else
                                                        Not Available
                                                        @endif</td>
                                                        <td>
                                                            <strong>CUSTOMER'S ATTACHMENTS</strong>: <br>
                                                           <button id="attachments_{{$booking_details->booking_sep_id}}" class="btn btn-rounded btn-primary btn-sm attachments">View Attachments</button>
                                                        </td>
                                                        <td>
                                                            @if($booking_details->booking_status!=2)
                                                            @if($booking_details->booking_complete_status==0 && $booking_details->booking_admin_status==1)
                                                            <strong>CANCELLATION</strong>: <br>
                                                           <button id="cancel_{{$booking_details->booking_sep_id}}" class="btn btn-rounded btn-danger btn-sm cancellation">Cancel Booking</button>
                                                            @endif
                                                            @endif
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <button class="btn btn-secondary collapse-btn" data-toggle="collapse" data-target="#payment_div" style="width:100%">Payments &nbsp; &nbsp; &nbsp;<i class="fa fa-caret-down"></i></button>
                                    <div class="row mt-30 collapse show" id="payment_div">
                                        <div class="col-md-12">
                                            <h4>Payment Details</h4>
                                            <div class="table-responsive">
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>S. No.</th>
                                                            <th>Installment Date</th>
                                                            <th>Amount to be paid</th>
                                                            <th>Actual Amount Received</th>
                                                            <th>Paid On</th>
                                                            <th>Status</th>
                                                            <th>Payment Mode</th>
                                                            <th>Payment Info(Transaction ID)</th>
                                                            <th>Payment Attachments</th>
                                                            <th>Admin Approval</th>
                                                            <th>Admin Remarks</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($fetch_booking_installments as $installments)
                                                        <tr>
                                                            <td>{{$loop->iteration}}</td>
                                                            <td>{{date('d/m/Y h:i a',strtotime($installments->created_at))}}</td>
                                                            <td>GEL {{$installments->booking_install_before_amount}}</td>
                                                            <td id='approve_payment_{{$installments->booking_installment_id}}_amount'>
                                                                @if($installments->booking_paid_status==1) GEL {{$installments->booking_install_amount}} @endif</td>
                                                            <td>{{date('d/m/Y h:i a',strtotime($installments->booking_paid_date." ".$installments->booking_paid_time))}}</td>
                                                            @if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                                                            <td><button type="button" class="btn btn-sm btn-rounded btn-primary">Paid</button>
                                                                @if($installments->booking_paid_status==1 && $installments->booking_confirm_status==1)
                                                                @php $total_paid_amount+=$installments->booking_install_amount; @endphp
                                                                @endif
                                                            </td>
                                                                @else
                                                                <td><button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button></td>
                                                                @endif
                                                                <td>
                                                                  {{$installments->booking_payment_mode}}  
                                                                </td>
                                                                 <td>
                                                                  {{$installments->booking_paid_info}}  
                                                                </td>
                                                                <td>
                                                                    @php
                                                                    if($installments->booking_payment_attachments!="")
                                                                    {
                                                                         $attachments=unserialize($installments->booking_payment_attachments);
                                                                    if(count($attachments)>0)
                                                                    {
                                                                        $count=1;
                                                                        foreach($attachments as $attachment)
                                                                        {
                                                                          echo '<a href="'.asset('assets/uploads/agent_payments').'/'.$attachment.'" target="_blank" class="text-primary">Attachment '.$count.'</a><br>';
                                                                          $count++;
                                                                      }
                                                                      
                                                                  }
                                                                  else
                                                                  {
                                                                   echo "Not Available";
                                                               }

                                                                    }
                                                                    else
                                                                    {
                                                                         echo "Not Available";
                                                                    }
                                                                   

                                                                    @endphp
                                                                </td>
                                                                 <td style="white-space: nowrap;">
                                                                     @if($installments->booking_paid_status==1)
                                                                  @if($installments->booking_confirm_status==0)
                                                                  <button class="btn btn-sm btn-primary approve_payment" id="approve_payment_{{$installments->booking_installment_id}}">Approve</button> &nbsp; <button class="btn btn-sm btn-danger reject_payment" id="reject_payment_{{$installments->booking_installment_id}}">Reject</button>
                                                                  @elseif($installments->booking_confirm_status==1)
                                                                  <button class="btn btn-sm btn-primary" disabled>Approved</button>
                                                                  @else
                                                                  <button class="btn btn-sm btn-danger" disabled>Rejected</button>
                                                                  @endif
                                                                  @endif
                                                                </td>
                                                                <td id="remarks_booking_{{$installments->booking_installment_id}}">
                                                                    {{$installments->booking_confirm_remarks}}
                                                                </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>


                                            <button class="btn btn-secondary collapse-btn" data-toggle="collapse" data-target="#expense_div" style="width:100%">Incomes &nbsp; &nbsp; &nbsp;<i class="fa fa-caret-down"></i></button>
                                            <div class="row mt-30 collapse show" id="expense_div">
                                                <div class="col-md-12">
                                                    <h4>View Incomes Details</h4>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sr. no</th>
                                                                    <th>Booking ID</th>
                                                                    <th>Income Category</th>
                                                                    <th>Occured on</th>
                                                                    <th>Amount</th>
                                                                    <th>Remarks</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($fetch_booking_incomes as $incomes)
                                                                <tr>
                                                                    <td>{{$loop->iteration}}</td>
                                                                    <td>{{$incomes->incomes_booking_id}}</td>
                                                                    <td>
                                                                        {{$incomes->get_income_category['expense_category_name']}}
                                                                    </td>
                                                                    <td>
                                                                        {{date('d/m/Y h:i a',strtotime($incomes->incomes_occured_on))}}
                                                                    </td>
                                                                    <td>
                                                                        GEL {{$incomes->incomes_amount}}
                                                                        @php $total_incomes+=$incomes->incomes_amount; @endphp
                                                                    </td>
                                                                    <td>
                                                                        {{$incomes->incomes_remarks}}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>

                                            <button class="btn btn-secondary collapse-btn" data-toggle="collapse" data-target="#expense_div" style="width:100%">Expenses &nbsp; &nbsp; &nbsp;<i class="fa fa-caret-down"></i></button>
                                            <div class="row mt-30 collapse show" id="expense_div">
                                                <div class="col-md-12">
                                                    <h4>View Expenses Details</h4>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sr. no</th>
                                                                    <th>Booking ID</th>
                                                                    <th>Expense Category</th>
                                                                    <th>Occured on</th>
                                                                    <th>Amount</th>
                                                                    <th>Remarks</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($fetch_booking_expenses as $expenses)
                                                                <tr>
                                                                    <td>{{$loop->iteration}}</td>
                                                                    <td>{{$expenses->expense_booking_id}}</td>
                                                                    <td>
                                                                        {{$expenses->get_expense_category['expense_category_name']}}
                                                                    </td>
                                                                    <td>
                                                                        {{date('d/m/Y h:i a',strtotime($expenses->expense_occured_on))}}
                                                                    </td>
                                                                    <td>
                                                                        GEL {{$expenses->expense_amount}}
                                                                        @php $total_expenses+=$expenses->expense_amount; @endphp
                                                                    </td>
                                                                    <td>
                                                                        {{$expenses->expense_remarks}}
                                                                    </td>
                                                                </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <button class="btn btn-secondary collapse-btn" data-toggle="collapse" data-target="#profit_div" style="width:100%">Profit/Loss Calculation &nbsp; &nbsp; &nbsp;<i class="fa fa-caret-down"></i></button>
                                            <div class="row mt-30 collapse show" id="profit_div">
                                                <div class="col-md-12">
                                                    <h4>VIEW PROFIT/LOSS CALCULATION</h4>
                                                    @php

                                                    if($booking_details->booking_type=="sightseeing")
                                                    {
                                                       $booking_subject_name=unserialize($booking_details->booking_subject_name);

                                                       if($booking_subject_name['tour_type']=="private")
                                                       {
                                                         $guide_cost=$booking_subject_name['guide_supplier_cost'];
                                                         $driver_cost=$booking_subject_name['driver_supplier_cost'];

                                                         $booking_supplier_amount=($guide_cost+$driver_cost);
                                                     }
                                                     else if($booking_subject_name['tour_type']=="group")
                                                     {
                                                        $booking_supplier_amount=0;
                                                    }

                                                }
                                                else
                                                {
                                                    $booking_supplier_amount=$booking_details->booking_supplier_amount;
                                                }
                                                $bought_amount=$booking_supplier_amount;
                                                    @endphp
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <p><strong>Service Bought For :</strong> <br> GEL {{$booking_details->booking_supplier_amount}}</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <p><strong>Service Sold for :</strong> <br> GEL {{$booking_details->booking_agent_amount}}</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <p><strong>Booking Markup :</strong> <br> {{$booking_details->booking_markup_per}}%</p>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <p><strong>Estimated Profit :</strong> <br> GEL {{($booking_details->booking_agent_amount-$bought_amount)}}</p>
                                                        </div>
                                                       
                                                    </div>
                                                    @if($total_paid_amount>0)
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>Particulars</th>
                                                                    <th> Cr.</th>
                                                                    <th> Dr.</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                        <td>Service Bought For</td>
                                                                    <td></td>
                                                                    <td><span class="text-red">GEL {{$bought_amount}}</span></td>
                                                                    </tr>
                                                                <tr>
                                                                    <td>Total Amount Received</td>
                                                                    <td><span class="text-green">GEL {{$total_paid_amount}}</span></td>
                                                                        <td></td>
                                                                    </tr>
                                                                      <tr>
                                                                    <td>Total Incomes</td>
                                                                    <td><span class="text-green">GEL {{$total_incomes}}</span></td>
                                                                    <td></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>Total Expenses Incurred</td>
                                                                        <td></td>
                                                                        <td><span class="text-red">GEL {{$total_expenses}}</span></td>
                                                                        </tr>

                                                                    @php
                                                                    $profit_loss=(($total_paid_amount+$total_incomes)-$bought_amount)-$total_expenses;
                                                                    @endphp
                                                                        <tr>
                                                                            <td>Actual Profit/Loss</td>
                                                                           
                                                                               
                                                                                @if($profit_loss>=0)
                                                                                <td> <span class="text-green">GEL {{$profit_loss}}</span></td>
                                                                                <td></td>
                                                                                @else
                                                                                <td></td>
                                                                                <span class="text-red">GEL {{abs($profit_loss)}}</span>
                                                                                @endif
                                                                            </td>
                                                                    
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            @else
                                                            <p class="text-center alert ">Sorry, We cannot calculate actual profit/loss as none of the  payment received yet.</p>
                                                            @endif
                                                        </div>

                                                    </div>
                                                    <hr>
                                <button class="btn btn-secondary collapse-btn" data-toggle="collapse" data-target="#complete_div" style="width:100%">Complete Booking & Supplier Payment Section &nbsp; &nbsp; &nbsp;<i class="fa fa-caret-down"></i></button>
                                            <div class="row mt-30 collapse show" id="complete_div">
                                                <div class="col-md-6">
                                                     <p><strong>Complete Trip:</strong><br>
                                                        @if($booking_details->booking_status!=2)
                                                   @if($booking_details->booking_status==1 && $total_paid_amount>=$booking_details->booking_agent_amount) 
                                                    @if($booking_details->booking_final_amount_status==1 && $booking_details->booking_complete_status==1)
                                                     <button class="btn btn-primary" disabled="disabled">Finished Trip at {{date('d/m/Y h:i a',strtotime($booking_details->booking_complete_timestamp))}}</button>
                                                    @else
                                                    <button class="btn btn-primary complete_booking" id="complete__{{$booking_details->booking_sep_id}}">Mark as Complete/Finished</button>

                                                    <input type="hidden" name="booking_id" id="booking_id"  value="{{$booking_details->booking_sep_id}}">
                                                    <br>
                                                      <span id="complete_error" style="color:red"></span>
                                                      @endif
                                                     @else
                                                     <p class="text-center alert">Sorry, We cannot mark this booking as complete until full payment is received.</p>
                                                    @endif
                                                    @else
                                                     <p class="text-center alert">Sorry, We cannot mark this booking as complete as booking is cancelled.</p>
                                                    @endif
                                                </div>
                                                 <div class="col-md-6">
                                                      <p><strong>Supplier Payments:</strong><br>
                                                            @if($booking_details->booking_status==1 && $booking_details->booking_admin_status==1) 
                                                                @if($booking_details->booking_supp_pay_status==0)
                                                            <button class="btn btn-primary pay_supplier" id="pay_supplier_{{$booking_details->booking_sep_id}}">Pay Supplier's Amount</button></p>
                                                             <br>
                                                              <span id="pay_error" style="color:red"></span>
                                                              @else
                                                               <button class="btn btn-info" disabled="disabled">Paid at {{date('d/m/Y h:i a',strtotime($booking_details->booking_supp_pay_timestamp))}}</button></p>
                                                              @endif
                                                    @else
                                                    @if($booking_details->booking_supp_pay_status==1)
                                                               <button class="btn btn-info" disabled="disabled">Paid at {{date('d/m/Y h:i a',strtotime($booking_details->booking_supp_pay_timestamp))}}</button></p>
                                                              @endif


                                                    @endif

                                                
                                                           
                                                        </div>
                                            </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @else
                                    <h4 class="text-danger">No rights to access this page</h4>
                                    @endif
                                </div>

                            </div>
                        </div>
                        @include('mains.includes.footer')
                        @include('mains.includes.bottom-footer')

                         <div class="modal" id="actionModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Peform Operation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="action_id" id="action_id">
                                 <input type="hidden" name="action_purpose" id="action_purpose">
                                  <div class="form-group" id="action_amount_div">
                                    <label for="action_amount" id="action_amount_label">Installment Amount</label>
                                    <input type="text" name="action_amount" id="action_amount" class="form-control"   onkeypress="javascript:return validateNumber(event)"  onpaste="javascript:return validateNumber(event)">
                                </div>
                                <div class="form-group">
                                    <label for="remarks" id="remarks_label">Remarks for Approval</label>
                                    <textarea name="remarks" id="remarks" class="form-control" rows="5"></textarea>
                                </div>
                                
                                
                                <button type="button" id="actionButton" class="btn btn-primary pull-right">Submit</button>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <div class="modal" id="installmentModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Installments</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-md-12 mb-25">
                                <button data-toggle="collapse" data-target="#pre_install_table" class="btn btn-sm btn-primary">Existing Installments <i class="fa fa-caret-down"></i></button>
                                <div id="pre_install_table" class="collapse">
                                </div>
                             </div>
                            <div class="col-md-12 mb-25">
                                 <p>Payment Details</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><strong>Total Amount:</strong> <br> GEL <span id="total_amount_pay">0</span></p> 
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong>Remaining Amount:</strong> <br> GEL <span id="rem_amount_pay">0</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><strong>Recieved Payment:</strong> <br> GEL <span id="rec_amount_pay">0</span></p> 
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong>Pending Payment:</strong> <br> GEL <span id="pen_amount_pay">0</span></p>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h3>Create New Installment</h3>
                                    </div>
                                </div>
                               <div class="row">
                                   <div class="col-md-3">
                                       <label>Installment Amount</label>
                                   </div>
                                   <div class="col-md-6">
                                       <div class="form-group">
                                            <input type="text" name="pay_amount" id="pay_amount" placeholder="Enter Amount" class="form-control"  onkeypress="javascript:return validateNumber(event)"  onpaste="javascript:return validateNumber(event)">
                                            <span style="color:red" id="pay_amount_error"></span>

                                             <span style="color:green" id="pay_amount_success"></span>
                                       </div>
                                   </div>
                               </div>
                             </div>
                            <div class="col-md-12">
                                <input type="hidden" name="main_booking_id" id="main_booking_id">
                                <button type="button" id="createInstallmentButton" class="btn btn-sm btn-primary pull-right">Generate Installment</button>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>



        <div class="modal" id="attachmentModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">View Attachments</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <form id="attachmentForm" action="{{route('booking-attachments')}}" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <input type="hidden" name="attachment_id" id="attachment_id" required="required">
                            <div class="row">
                                <div class="col-md-12" id="attachment_div"></div>
                                <div class="col-md-12">
                                    <button type="submit" id="updateAttachments" class="btn btn-primary pull-right">SAVE</button>
                                </div>

                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>

        @if(session()->has('success-message'))
        <script>
            $(document).ready(function()
            {
                swal("Success","{{session()->get('success-message')}}","success");
            });
        </script>
        @elseif(session()->has('error-message'))
        <script>
          $(document).ready(function()
          {
            swal("Error","{{session()->get('error-message')}}","error");
        });
    </script>
    @endif

                        <script>

                            $(document).ready(function()
                            {
                                $(".select2").select2();
                                $("#datetimepicker1").datetimepicker({
                                    autoclose:true,
                                    format: 'yyyy-mm-dd hh:ii:00'
                                });
                            });




                            $(document).on("click",".collapse-btn",function()
                            {
                                if($(this).find("i").hasClass("fa-caret-down"))
                                {
                                    $(this).find("i").addClass("fa-caret-up").removeClass("fa-caret-down");

                                }
                                else
                                {
                                    $(this).find("i").addClass("fa-caret-down").removeClass("fa-caret-up");
                                }
                            });

                            $(document).on("click",".complete_booking",function()
                            {
                                $("#complete_error").hide();
                                var booking_id=$("#booking_id").val();
                                $.ajax({
                                    url:"{{route('booking-complete')}}",
                                    type:"POST",
                                    data:{"_token":"{{csrf_token()}}",
                                        "booking_id":booking_id},
                                    success:function(response)
                                    {
                                        if(response.indexOf("success")!=-1 || response.indexOf("already")!=-1)
                                        {
                                            $(".complete_booking").text("Finished Trip").removeClass("complete_booking").attr("disabled",true);
                                        }
                                        else if(response.indexOf("payment")!=-1)
                                        {
                                            $("#complete_error").text("Full Payment haven't paid yet").show();
                                        }
                                        else if(response.indexOf("fail")!=-1)
                                        {
                                             $("#complete_error").text("Sorry! Some issue encountered. Try Again Later.").show();
                                        }
                                         else if(response.indexOf("no_book")!=-1)
                                        {
                                             $("#complete_error").text("Invalid Booking ID").show();
                                        }
                                        
                                    }

                                })

                            });

                            $(document).on("click",".pay_supplier",function()
                            {
                                var id=this.id;
                                var actual_id=id.split("_")[2];


                              swal({
                title: "Are you sure?",
                text: "You want to pay the supplier amount !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Go Ahead",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {

                                $.ajax({
                                    url:"{{route('pay-supplier-booking')}}",
                                    type:"POST",
                                    data:{"_token":"{{csrf_token()}}",
                                        "booking_id":actual_id},
                                    success:function(response)
                                    {
                                        if(response.indexOf("success")!=-1 || response.indexOf("already")!=-1)
                                        {
                                            $(".pay_supplier").text("Paid").removeClass("pay_supplier").attr("disabled",true);
                                            $("#pay_error").hide();
                                             swal("Success", "Paid Successfully", "success");
                                        }
                                        else if(response.indexOf("payment")!=-1)
                                        {
                                            $("#pay_error").text("Full Payment haven't paid yet").show();
                                            swal("Error", "Full Payment haven't paid yet", "error");
                                        }
                                        else if(response.indexOf("fail")!=-1)
                                        {
                                             $("#pay_error").text("Sorry! Some issue encountered. Try Again Later.").show();
                                              swal("Error", "Sorry! Some issue encountered. Try Again Later.", "error");
                                        }
                                         else if(response.indexOf("no_book")!=-1)
                                        {
                                             $("#pay_error").text("Invalid Booking ID").show();
                                              swal("Error", "Invalid Booking ID", "error");
                                        }

                                    }
                                })
                                 } else {
                    swal("Cancelled", "Operation Cancelled", "error");
                }
            });


                            });



                             $(document).on("click",".approve_payment",function()
        {
            var id=this.id;
            var amount=$("#"+id+"_amount").text();
            var actual_amount=amount.split(" ");
            $("#action_id").attr("value",id);
            $("#action_purpose").val("payment");
            $("#action_amount_div").show();
            $("#action_amount").val(actual_amount[1]);
            $("#actionModal").find(".modal-title").text("Perform Approval Operation");
            $("#remarks_label").text("Remarks for Approval");
            $("#remarks").val("");
            $("#actionModal").modal("show");
        });
        $(document).on("click",".reject_payment",function()
        {
            var id=this.id;
             var amount=$("#"+id+"_amount").text();
            var actual_amount=amount.split(" ");
            $("#action_id").attr("value",id);
             $("#action_purpose").val("payment");
             $("#action_amount_div").hide();
              $("#action_amount").val("");
            $("#actionModal").find(".modal-title").text("Perform Rejection Operation");
            $("#remarks_label").text("Remarks for Rejection");
            $("#remarks").val("");
            $("#actionModal").modal("show");
        });


         $(document).on("click",'#actionButton',function()
        {
            var action_purpose=$("#action_purpose").val();

                  var id=$("#action_id").val();
                   var amount=$("#action_amount").val();
            var actual_id=id.split("_");
            var action_perform=actual_id[0];
            var action_id=actual_id[2];
            var remarks=$("#remarks").val();
            swal({
                title: "Are you sure?",
                text: "You want to "+action_perform+" this payment !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Go Ahead",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:"{{route('approve-payment')}}",
                        type:"POST",
                        data:{"_token":"{{csrf_token()}}",
                        "action_perform":action_perform,
                        "amount":amount,
                        "installment_id":action_id,
                        "remarks":remarks},
                        success:function(response)
                        {
                            if(action_perform=="approve")
                            {
                                if(response.indexOf("reload")!=-1)
                                {
                                    location.reload();
                                }
                                else if(response.indexOf("success")!=-1)
                                {
                                     swal("Approved!", "Selected Payment has been approved.", "success");
                                    $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');
                                    $("td#remarks_booking_"+action_id).text(remarks);

                                    setTimeout(function()
                                    {
                                        $("#installmentModal").modal("hide");
                                    },500);
                                   
                                }
                                else
                                {
                                    swal("Error", "Unable to perform this operation", "error");
                                }
                            }
                            else
                            {
                                if(response.indexOf("reload")!=-1)
                                {
                                    location.reload();
                                }
                                else if(response.indexOf("success")!=-1)
                                {
                                   $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');
                                    $("td#remarks_booking_"+action_id).text(remarks);
                                    swal("Rejected!", "Selected Payment has been rejected.", "success");
                                    setTimeout(function()
                                    {
                                        $("#installmentModal").modal("hide");
                                    },500);
                                }
                                else
                                {
                                    swal("Error", "Unable to perform this operation", "error");
                                }
                            }
                            setTimeout(function()
                                    {
                                        $("#actionModal").modal("hide");
                                    },500);

                        }
                    });
                } else {
                    swal("Cancelled", "Operation Cancelled", "error");
                }
            });
     

               });


    $(document).on("click",".create_installment",function()
    {
        $("#pay_amount_error").hide();
         $("#pay_amount_success").hide();
         $("#pay_amount").val("");
        var id=this.id;

        var actual_id=id.split("_")[1];

       
    $("#loaderModal").modal("show");
         $.ajax({
                url: "{{route('get-booking-installment-detail')}}",
                data: {
                    'booking_id':actual_id,
                },
                type: 'GET',
                dataType:"JSON",
                success: function (response)
                {

                    $("#main_booking_id").val(actual_id);
                    $("#total_amount_pay").text(response.total_amount);
                    $("#rem_amount_pay").text(response.remaining_amount);
                     $("#rec_amount_pay").text(response.received_amount);
                    $("#pen_amount_pay").text(response.pending_amount);
                     $("#pre_install_table").html(response.previous_installments_html);

                     // $('#installments_table').DataTable({ "scrollX": true});
                    $("#installmentModal").modal("show");


                    setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                    },500);


                }
            });
       

    });


    $(document).on("click","#createInstallmentButton",function()
    {
        var main_booking_id=$("#main_booking_id").val();
        var total_amount=$("#total_amount_pay").text();
        var remaning_amount=$("#rem_amount_pay").text();
        var pay_amount=$("#pay_amount").val();


        if(pay_amount.trim()=="")
        {
            $("#pay_amount_error").text("Amount cannot be empty").show();
        }
        else if(pay_amount.trim()<0)
        {
            $("#pay_amount_error").text("Amount cannot be less than or equal to 0").show();
        }
        // else if(parseInt(pay_amount.trim())>parseInt(total_amount.trim()))
        // {
        //     $("#pay_amount_error").text("Amount cannot be more than total amount i.e GEL "+total_amount).show();
        // }
        // else if(parseInt(pay_amount.trim())>parseInt(remaning_amount.trim()))
        // {
        //     $("#pay_amount_error").text("Amount cannot be more than remaining amount i.e GEL "+remaning_amount).show();
        // }
        else
        {
            $("#pay_amount_error").hide();
            $("#createInstallmentButton").prop("disabled",true);
             $.ajax({
                url: "{{route('insert-installment')}}",
                data: {
                    'booking_id':main_booking_id,
                    'pay_amount':pay_amount,
                    '_token':"{{csrf_token()}}",

                },
                type: 'POST',
                success: function (response)
                {

                    if(response.indexOf("success")!=-1)
                    {
                        $("#pay_amount_success").text("Installment created successfully").show();
                        $("#pay_amount_error").hide();

                        setTimeout(function()
                        {
                            $("#pay_amount").val("");
                            $("#installmentModal").modal("hide");
                            location.reload();
                        },1000);
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        $("#pay_amount_error").text("Try Again Later").show();
                        $("#pay_amount_success").hide();
                    }
                    else if(response.indexOf("no_more")!=-1)
                    {
                        $("#pay_amount_error").text("No more installments can be created.").show();
                        $("#pay_amount_success").hide();
                    }
                     else if(response.indexOf("more_amount")!=-1)
                    {
                        $("#pay_amount_error").text("Amount cannot exceed remaining amount.").show();
                        $("#pay_amount_success").hide();
                    }

    $("#createInstallmentButton").prop("disabled",false);


                }
            });
        }


    });
    $(document).on("click",".attachments",function()
    {
        var id=this.id;
        var actual_id=id.split("_")[1];

        $("#loaderModal").modal("show");
        $("#attachment_id").val(actual_id);
         $.ajax({
                url: "{{route('booking-attachments')}}",
                data: {
                    'booking_id':actual_id,
                },
                type: 'GET',
                success: function (response)
                {
                    if(response.indexOf("previewImg_new")!=-1)
                    {
                         $("#attachment_div").html(response);
                    $("#attachmentModal").modal("show");
                     setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                    },500); 
                    
                    }
                    else if(response.indexOf("invalid")!=-1)
                    {
                         setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                            alert("Invalid Booking ID");
                    },500);  
                       
                    }
                    else if(response.indexOf("user_session")!=-1)
                    {
                       setTimeout(function()
                    {
                            $("#loaderModal").modal("hide");
                            alert("Invalid User Session");
                            location.reload();
                    },500); 
                    }
                   


                }
            });


    });

      $(document).on("click",".remove_already_attachments",function()
    {
        var image=this.id;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this attachment !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
              $("#"+image).parent().remove();
              swal("Deleted!", "Selected attachment has been deleted.", "success");
          } else {
            swal("Cancelled", "Your attachment is safe :)", "error");
        }
    });
    });

       $(document).on("click",".add_more_attachment",function(){ 
        var id=$(this).parent().parent().attr("id");
        var actual_id=id.split("increment");

        if(actual_id[1]=="")
        {
             var html = $("#clone").html();
          $("div#increment").after(html);
        }
        else
        {
              var html = $("#clone"+actual_id[1]).html();
          $("#"+id).after(html);

        }
         
      });
      $("body").on("click",".remove_more_attachment",function(){ 
          $(this).parents(".control-group").remove();
      });


      $(document).on("click",".cancellation",function()
      {
       var id=this.id;
       var booking_id=id.split("_")[1];
       var booking_role=$("#booking_role").val();
       if(booking_role=="agent")
       {
        var action_id=$("#agent_id").val();
       }
       else
       {
        var action_id=$("#user_id").val();
       }
       swal({
        title: "Are you sure?",
        text: "You want to cancel this booking !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, cancel it!",
        cancelButtonText: "No, stop!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function(isConfirm) {
        if (isConfirm) {
            $.ajax({
                url:"{{route('bookings-cancel')}}",
                type:"POST",
                data:{'_token':'{{csrf_token()}}',
                'booking_id':booking_id,
                'action_id':action_id,
                'booking_role':booking_role},
                success:function(response)
                {
                    if(response.indexOf("process")!=-1)
                    {
                         swal("Mark as Completed!", "This booking has been marked as complete , so it cannot be cancelled", "error");
                    }
                    else if(response.indexOf("success")!=-1)
                    {
                         swal("Cancelled", "This booking is cancelled successfully", "success"); 
                         location.reload();
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        swal("Error", "This booking cannot be cancelled right now . Try Again Later.", "error");
                    }

                }
            });
         
      } else {
        swal("Cancelled", "This booking is safe :)", "error");
    }
});

   });


                        </script>


                    </body>
                    </html>