<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')

<style>

	.iti-flag {

		width: 20px;

		height: 15px;

		box-shadow: 0px 0px 1px 0px #888;

		background-image: url("{{asset('assets/images/flags.png')}}") !important;

		background-repeat: no-repeat;

		background-color: #DBDBDB;

		background-position: 20px 0

	}



	div#cke_1_contents {

		height: 250px !important;

	}

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

	<div class="wrapper">

		@include('mains.includes.top-nav')

		<div class="content-wrapper">

			<div class="container-full clearfix position-relative">	

				@include('mains.includes.nav')

				<div class="content">

					<!-- Content Header (Page header) -->

					<div class="content-header">

						<div class="d-flex align-items-center">

							<div class="mr-auto">

								<h3 class="page-title">Service Management</h3>

								<div class="d-inline-block align-items-center">

									<nav>

										<ol class="breadcrumb">

											<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

											<li class="breadcrumb-item" aria-current="page">Home</li>

											<li class="breadcrumb-item active" aria-current="page">View Driver Details

											</li>

										</ol>

									</nav>

								</div>

							</div>

							<!-- <div class="right-title">

								<div class="dropdown">

									<button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

										class="mdi mdi-dots-horizontal"></i></button>

										<div class="dropdown-menu dropdown-menu-right">

											<a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

											<a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

											<a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

											<a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

											<div class="dropdown-divider"></div>

											<button type="button" class="btn btn-rounded btn-success">Submit</button>

										</div>

									</div>

								</div> -->

							</div>

						</div>




						@if($rights['view']==1)		
						<div class="row">
							<div class="col-12">

								<div class="box">

									<div class="box-body">
										<div class="row">
											<div class="col-md-8">


												<div class="row">

													<div class="col-md-3">

														<label for="driver_name"><strong>DRIVER NAME :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="driver_name"> @if($get_drivers->driver_first_name!="" && $get_drivers->driver_first_name!="0" && $get_drivers->driver_first_name!=null){{$get_drivers->driver_first_name}} @endif  @if($get_drivers->driver_last_name!="" && $get_drivers->driver_last_name!="0" && $get_drivers->driver_last_name!=null){{$get_drivers->driver_last_name}} @endif </p>

													</div>

												</div>

												<div class="row">

													<div class="col-md-3">

														<label for="driver_contact"><strong>CONTACT NUMBER :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="driver_contact"> @if($get_drivers->driver_contact!="" && $get_drivers->driver_contact!=null)
															{{$get_drivers->driver_contact}}
														@else No Data Available @endif </p>

													</div>

												</div>

												<div class="row">

													<div class="col-md-3">

														<label for="driver_address"><strong>ADDRESS :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="driver_address"> @if($get_drivers->driver_address!="" && $get_drivers->driver_address!=null)
															{{$get_drivers->driver_address}}
														@else No Data Available @endif </p>

													</div>

												</div>

												<div class="row">

													<div class="col-md-3">

														<label for="supplier_id"><strong>SUPPLIER NAME :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="supplier_id"> @if($get_drivers->driver_supplier_id!="" && $get_drivers->driver_supplier_id!=null)
															@foreach($suppliers as $supplier)
															@if($get_drivers->driver_supplier_id==$supplier->supplier_id)
															{{$supplier->supplier_name}}
															@endif 
															@endforeach 
														@else No Data Available @endif </p>

													</div>

												</div>




												<div class="row">

													<div class="col-md-3">

														<label for="driver_country"><strong>COUNTRY :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="driver_country"> @if($get_drivers->driver_country!="" && $get_drivers->driver_country!=null)
															@foreach($countries as $country)
															@if(in_array($country->country_id,$countries_data))
															@if($country->country_id==$get_drivers->driver_country)
															{{$country->country_name}}
															@endif
															@endif
															@endforeach
														@else No Data Available @endif </p>

													</div>

												</div>

												<div class="row">

													<div class="col-md-3">

														<label for="driver_city"><strong>CITY :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="driver_city"> @if($get_drivers->driver_city!="" && $get_drivers->driver_city!=null)
															<?php
															$fetch_city=ServiceManagement::searchCities($get_drivers->driver_city,$get_drivers->driver_country);

															echo $fetch_city['name'];
															?>
														@else No Data Available @endif </p>

													</div>

												</div>
												<div class="row">

													<div class="col-md-3">

														<label for="driver_language"><strong>LANGUAGE :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="driver_language"> @if($get_drivers->driver_language!="" && $get_drivers->driver_language!=null)
															@php
															$languages_data=explode(",",$get_drivers->driver_language);

															foreach($languages as $language)
															{
																if(in_array($language->language_id,$languages_data))
																{
																	echo $language->language_name." ,";
																}
															}

															@endphp
														@else No Data Available @endif </p>

													</div>

												</div>
												<div class="row">

													<div class="col-md-3">

														<label for="supplier_id"><strong>DRIVER PRICE <small>(per day)</small> :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="supplier_id"> @if($get_drivers->driver_price_per_day!="" && $get_drivers->driver_price_per_day!=null)
															{{$get_drivers->driver_price_per_day}}
														@else No Data Available @endif </p>

													</div>

												</div>

												<div class="row">

													<div class="col-md-3">

														<label for="supplier_id"><strong>DESCRIPTION :</strong></label>

													</div>

													<div class="col-md-9">

														<p class="" id="supplier_id"> @if($get_drivers->driver_description!="" && $get_drivers->driver_description!=null)
															{{$get_drivers->driver_description}}
														@else No Data Available @endif </p>

													</div>

												</div>

													<div class="row">

											<div class="col-md-3">

												<label for="operating_weekdays"><strong>WORKING DAYS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="operating_weekdays"> @if($get_drivers->operating_weekdays!="" && $get_drivers->operating_weekdays!=null)
													@php
													$weekdays=unserialize($get_drivers->operating_weekdays);
													$show_days=array();
													foreach($weekdays as $key=>$value)
													{
														if($value=="Yes")
														{
															array_push($show_days,ucfirst($key));
														}
													}

													echo implode(" ,",$show_days);
													@endphp
													 @else No Data Available @endif 
													 </p>

											</div>

										</div>
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="driver_vehicles"></i> DRIVER VEHICLES

												</h4>

											</div>



										</div>
										<div id="driver_vehicles_details">

											<div class="col-md-12">
												<table class="table table-bordered">
													<thead>
													<tr>
														 <th>VEHICLE TYPE</th>
														  <th>VEHICLE</th>
														   <th>VEHICLE INFO</th>
														   <th>IMAGES</th>
													</tr>
													</thead>
													<tbody>
												
                                                                    @php
                                                                    $driver_vehicle_type=$get_drivers->driver_vehicle_type;
                                                                    $driver_vehicle=$get_drivers->driver_vehicle;
                                                                    $driver_vehicle_info=$get_drivers->driver_vehicle_info;
                                                                    $driver_vehicle_images=unserialize($get_drivers->driver_vehicle_images);

                                                                    @endphp
                                                                    @if($driver_vehicle_type!=0 && $driver_vehicle_type!=null)

                                                                    @php
                                                                    $vehicle_type_array=explode(",",$driver_vehicle_type);
                                                                    $vehicle_array=explode(",",$driver_vehicle);
                                                                    $vehicle_info_array=explode("---",$driver_vehicle_info);
                                                                    @endphp
                                                                    @for($i=0;$i< count($vehicle_type_array);$i++)
                                                                    
                                                                    <tr>
                                                                                 <td>  @foreach($fetch_vehicle_type as $vehicle_type)
                                                                                    @if($vehicle_type->vehicle_type_id==$vehicle_type_array[$i]) 
                                                                                    <p>{{$vehicle_type->vehicle_type_name}} </p>
                                                                                    @endif
                                                                                    @endforeach

                                                                               
                                                                            </td> 

                                                                            <td>
                                                                                @foreach($fetch_vehicle_type as $vehicle_type)
                                                                                @if($vehicle_type->vehicle_type_id==$vehicle_type_array[$i])
                                                                                @php
                                                                                $vehicles=$vehicle_type->getVehicles;
                                                                                @endphp
                                                                                @endif
                                                                                @endforeach

                                                                            
                                                                                    @foreach($vehicles as $vehicle)
                                                                                    @if($vehicle->vehicle_id==$vehicle_array[$i]) <p>{{$vehicle->vehicle_name}}</p>
                                                                                     @endif
                                                                                    @endforeach
                                                                            </td>
                                                                            <td>
                                                                               <p>{{$vehicle_info_array[$i]}}</p>
                                                                            </td>
                                                                            <td>
                                                                            	@if(!empty($driver_vehicle_images[$i]))
                                                                            	@foreach($driver_vehicle_images[$i] as $vehicle_images)
                                                                            	<img src="{{asset('assets/uploads/driver_vehicle_images/')}}/{{$vehicle_images}}" width="100" height="100" class="img img-responsive">
                                                                            	@endforeach
                                                                            	@endif
                                                                            </td>
                                                                    </tr>
                                                                    @endfor
                                                                    @endif
                                                                </tbody>
                                                                    </table>
                                                                </div>

										</div>

										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="blackout_days"></i> BLACKOUT DAYS

												</h4>

											</div>



										</div>
										<div id="blackout_days_details">
											@if($get_drivers->driver_blackout_dates!="" && $get_drivers->driver_blackout_dates!=null)
											<div class="row">
											@php
											$blackout_dates=explode(',',$get_drivers->driver_blackout_dates);	
											
											for($black=0;$black< count($blackout_dates);$black++)
											{
												@endphp
												
													<div class="col-md-2">

												<label for="blackout_dates{{$black}}"><strong>DAY {{($black+1)}} :</strong></label>

											</div>

											<div class="col-md-2">

												<p class="" id="blackout_dates{{$black}}">
												 @php
												echo date('d-m-Y',strtotime($blackout_dates[$black]));
												 @endphp </p>

											</div>

												@php
											}
											@endphp
										</div>
											@else
											No Data Available 
											@endif
										</div>

												<div class="row mb-10" style="display:none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="nationality_markup_details"></i> NATIONALITY & TRANSFER MARKUP DETAILS

												</h4>

											</div>



										</div>
										<div id="nationality_markup_showdetails" class="row" style="display:none">
											@if($get_drivers->nationality_markup_details!="" && $get_drivers->nationality_markup_details!=null)
											@php
											$nationality_markup_details=unserialize($get_drivers->nationality_markup_details);

											for($nation_count=0;$nation_count< count($nationality_markup_details);$nation_count++)
											{
											@endphp
											<div class="col-md-6">
												<div class="row">
												<div class="col-md-6">
													<label for="driver_nationality{{$nation_count}}"><strong>NATIONALITY:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_nationality{{$nation_count}}">
													@foreach($countries as $country)

											 		@if($country->country_id==$nationality_markup_details[$nation_count]['driver_nationality'])
											 		{{$country->country_name}}
											 		@endif
											 		@endforeach
												</p>
											</div>
												<div class="col-md-6">
													<label for="driver_markup{{$nation_count}}"><strong>MARKUP TYPE:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_markup{{$nation_count}}">{{$nationality_markup_details[$nation_count]['driver_markup']}}
												</p>
											</div>
											<div class="col-md-6">
													<label for="driver_amount{{$nation_count}}"><strong>MARKUP PERCENTAGE/AMOUNT:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_amount{{$nation_count}}">{{$nationality_markup_details[$nation_count]['driver_amount']}}
												</p>
											</div>
											</div>
											</div>
											
											@php
											}
											@endphp					
											@else
											No Data Available 
											@endif
											</div>
											<div class="row mb-10" style="display:none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="transport_pricing"></i> TRANSPORT TARIFF DETAILS

												</h4>

											</div>
										</div>
										<div id="transport_pricing_details" class="row" style="display:none">
											@if($get_drivers->driver_tariff!="" && $get_drivers->driver_tariff!=null)
											@php
											$driver_tariff=unserialize($get_drivers->driver_tariff);

											for($transport_count=0;$transport_count< count($driver_tariff);$transport_count++)
											{
											@endphp
											<div class="col-md-6" style="border-bottom: 1px solid #8c8c8c;padding: 1% 4% 1% 4%">
												<div class="row">
													<div class="col-md-6">
													<label for="driver_validity_from{{$transport_count}}"><strong>VALIDITY:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_validity_from{{$transport_count}}">
													@if($driver_tariff[$transport_count]['driver_validity_from']!="" && $driver_tariff[$transport_count]['driver_validity_from']!="0" && $driver_tariff[$transport_count]['driver_validity_from']!=null)
													@php echo date('d-m-Y',strtotime($driver_tariff[$transport_count]['driver_validity_from'])) @endphp
													@else 
													No Data Available 
													@endif
													To
													@if($driver_tariff[$transport_count]['driver_validity_to']!="" && $driver_tariff[$transport_count]['driver_validity_to']!="0" && $driver_tariff[$transport_count]['driver_validity_to']!=null) 
													@php 
													echo date('d-m-Y',strtotime($driver_tariff[$transport_count]['driver_validity_to'])) @endphp
													@else 
													No Data Available
													@endif
												</p>
											</div>
											<div class="col-md-6">
													<label for="driver_tourname{{$transport_count}}"><strong>TOUR NAME:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_tourname{{$transport_count}}">{{$driver_tariff[$transport_count]['driver_tourname']}}
												</p>
											</div>
											
											
												<div class="col-md-6">
													<label for="driver_cost_four{{$transport_count}}"><strong>PRICE UPTO 4:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_cost_four{{$transport_count}}">{{$driver_tariff[$transport_count]['driver_cost_four']}}
												</p>
											</div>
											<div class="col-md-6">
													<label for="driver_cost_seven{{$transport_count}}"><strong>PRICE UPTO 7:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_cost_seven{{$transport_count}}">{{$driver_tariff[$transport_count]['driver_cost_seven']}}
												</p>
											</div>
											<div class="col-md-6">
													<label for="driver_cost_twenty{{$transport_count}}"><strong>PRICE UPTO 20:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_cost_twenty{{$transport_count}}">{{$driver_tariff[$transport_count]['driver_cost_twenty']}}
												</p>
											</div>
											<div class="col-md-6">
													<label for="driver_duration{{$transport_count}}"><strong>DURATION:</strong></label>
												</div>
												<div class="col-md-6">
												<p class="" id="driver_duration{{$transport_count}}">
													@if(!empty($driver_tariff[$transport_count]['driver_duration']))
													{{$driver_tariff[$transport_count]['driver_duration']}}
													@else 
													No Data Available
													@endif
												</p>
											</div>
											
											
										</div>
											</div>
											
											
											@php
											}
											@endphp					
											@else
											No Data Available 
											@endif
											</div>
												<div class="row mb-10" style="display:none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="driver_inclusions"></i> INCLUSIONS

												</h4>

											</div>



										</div>
										<div class="row" id="driver_inclusions_details" style="display:none">

											<div class="col-md-12">
												<textarea id="driver_inclusions_data">
										 @if($get_drivers->driver_inclusions!="" && $get_drivers->driver_inclusions!=null) {{$get_drivers->driver_inclusions}} @else No Data Available @endif
										 </textarea>

											</div>

										</div>

											
										<div class="row mb-10" style="display:none">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="driver_exclusions"></i> EXCLUSIONS

												</h4>

											</div>



										</div>
										<div class="row" id="driver_exclusions_details" style="display:none">

											
											<div class="col-md-12">
												<textarea id="driver_exclusions_data">
										 @if($get_drivers->driver_exclusions!="" && $get_drivers->driver_exclusions!=null) {{$get_drivers->driver_exclusions}} @else No Data Available @endif
										</textarea>

											</div>

										</div>
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="driver_cancel_policy"></i> CANCELLATION POLICY

												</h4>

											</div>



										</div>
										<div class="row" id="driver_cancel_policy_details">

											<div class="col-md-12">
												<textarea id="driver_cancel_policy_data">
										 @if($get_drivers->driver_cancel_policy!="" && $get_drivers->driver_cancel_policy!=null) {{$get_drivers->driver_cancel_policy}} @else No Data Available @endif
										</textarea>

											</div>

										</div>
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="driver_terms_conditions"></i> TERMS AND CONDITIONS

												</h4>

											</div>



										</div>
										<div class="row" id="driver_terms_conditions_details">

											<div class="col-md-12">
												<textarea id="driver_terms_conditions_data">
										 @if($get_drivers->driver_terms_conditions!="" && $get_drivers->driver_terms_conditions!=null) {{$get_drivers->driver_terms_conditions}} @else No Data Available @endif
										</textarea>

											</div>

										</div>
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="sightseeing_driver_cost"></i> SIGHTSEEING DRIVER COST

												</h4>

											</div>



										</div>
										<div class="row" id="sightseeing_driver_cost_details">

											{{-- <div class="col-md-12">
												
												

												@if($get_drivers->driver_tours_cost!="" && $get_drivers->driver_tours_cost!=null)
												@php
												$driver_cost_details=explode('///',$get_drivers->driver_tours_cost);
												@endphp

												<table class="table table-condensed">
													<tr>
														<th>Tour Name</th>
														<th>Cost</th>
													</tr>
													@php

													for($services=0;$services< count($driver_cost_details);$services++)
													{
														if($driver_cost_details[$services]!="")
														{
															$get_services_individual=explode("---",$driver_cost_details[$services]);
															 $get_name=ServiceManagement::searchSightseeingTourName($get_services_individual[0]);

                                                        $tour_name=$get_name['sightseeing_tour_name'];
															@endphp
															<tr>
																<td>{{$tour_name}}</td>
																<td>
																	@if($get_services_individual[1]!="")
																	{{ $get_services_individual[1]}}
																	@else
																	No Data Available 

																@endif</td>
															</tr>
															@php
														}
													}
													@endphp
												</table>
												@else
												No Data Available 
												@endif

											</div> --}}
											<div class="col-md-12">
                                           @php
                                            $driver_tours_cost=unserialize($get_drivers->driver_tours_cost);
                                           @endphp

                                                @if($driver_tours_cost=="null" || $driver_tours_cost=="" || count($driver_tours_cost)<=0)
                                                <div class='row'><p class='text-center'><b>No Tour Cost Available</b></p></div>
                                                @else
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <th>SIGHTSEEING TOURS</th>
                                                        <th>VEHICLE TYPE</th>
                                                        <th>DRIVER COST</th>
                                                    </tr>
                                                @php
                                                for($tour_count=0;$tour_count < count($driver_tours_cost);$tour_count++)
                                                {
                                                    $fetch_tour_name=ServiceManagement::searchSightseeingTourName($driver_tours_cost[$tour_count]['tour_name']);

                                                    $tour_name=$fetch_tour_name['sightseeing_tour_name'];
                                                    echo "<tr>
                                                        <td rowspan='".(count($driver_tours_cost[$tour_count]['tour_vehicle_name'])+1)."'>".ucwords($tour_name)."</td>
                                                        ";

                                                         for($tour_vehicle_count=0;$tour_vehicle_count < count($driver_tours_cost[$tour_count]['tour_vehicle_name']);$tour_vehicle_count++)
                                                         {
                                                            $vehicle_name="";
                                                            foreach($fetch_vehicle_type as $vehicle_type)
                                                            {
                                                                if($vehicle_type->vehicle_type_id==$driver_tours_cost[$tour_count]['tour_vehicle_name'][$tour_vehicle_count])
                                                                {
                                                                   $vehicle_name=$vehicle_type->vehicle_type_name;

                                                                   echo "<tr><td>$vehicle_name</td>
                                                                   	<td>".$driver_tours_cost[$tour_count]['tour_driver_cost'][$tour_vehicle_count]."</td></tr>";
                                                            
                                                             }
                                                           }     
                                                         } 
                                                            echo "</tr>";
                                                    }
                                                    @endphp
                                                </table>
                                                @endif

                                            </div>

												
									


										</div>










												<br>
												<div class="row mb-10">
													<div class="col-md-12">
														<div class="box-header with-border"
														style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
														<button type="button" id="discard_driver" class="btn btn-rounded btn-primary">BACK</button>
														<a href="{{route('edit-driver',['driver_id'=>$get_drivers->driver_id])}}" id="update_transfer" class="btn btn-rounded btn-primary mr-10">EDIT</a>
													</div>
												</div>
											</div>





											<!-- /.row -->

										</div>
										<div class="col-md-4">
											<a href="{{ asset('assets/uploads/driver_images/')}}/{{$get_drivers->driver_image}}" target="_blank"><img height="200" alt="driver Picture Preview..."
												src="{{ asset('assets/uploads/driver_images/')}}/{{$get_drivers->driver_image}}"></a>
											</div>
										</div>

										<!-- /.box-body -->

									</div>


								</div>
								<!-- /.box -->
							</div>
						</div>
							@else
	<h4 class="text-danger">No rights to access this page</h4>
			
			@endif
					</div>
			</div>
		</div>

		@include('mains.includes.footer')

		@include('mains.includes.bottom-footer')



		<script>
			$(document).on("click","#discard_driver",function()
			{
				window.history.back();

			});
			$(document).ready(function()
			{
				CKEDITOR.replace('driver_exclusions_data', {readOnly:true});
				CKEDITOR.replace('driver_inclusions_data',{readOnly:true});
				CKEDITOR.replace('driver_cancel_policy_data',{readOnly:true});
				CKEDITOR.replace('driver_terms_conditions_data',{readOnly:true});

			});
			$(document).on("click","#nationality_markup_details",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#nationality_markup_showdetails").toggle();



			});



			$(document).on("click","#transport_pricing",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#transport_pricing_details").toggle();



			});
			$(document).on("click","#driver_inclusions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#driver_inclusions_details").toggle();



			});
			$(document).on("click","#driver_exclusions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#driver_exclusions_details").toggle();



			});
			$(document).on("click","#driver_cancel_policy",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#driver_cancel_policy_details").toggle();



			});

	$(document).on("click","#driver_terms_conditions",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#driver_terms_conditions_details").toggle();



			});

			$(document).on("click","#blackout_days",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#blackout_days_details").toggle();



			});

			$(document).on("click","#sightseeing_driver_cost",function()

			{

				if($(this).hasClass('fa-minus-circle'))

				{

					$(this).removeClass('fa-minus-circle').addClass('fa-plus-circle');



				}

				else

				{

					$(this).removeClass('fa-plus-circle').addClass('fa-minus-circle');

				}

				$("#sightseeing_driver_cost_details").toggle();



			});




		</script>



	</body>





	</html>

