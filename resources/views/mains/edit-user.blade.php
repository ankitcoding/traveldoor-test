@include('mains.includes.top-header')

<style>

    .iti-flag {

        width: 20px;

        height: 15px;

        box-shadow: 0px 0px 1px 0px #888;

        background-image: url("{{asset('assets/images/flags.png')}}") !important;

        background-repeat: no-repeat;

        background-color: #DBDBDB;

        background-position: 20px 0

    }



    div#cke_1_contents {

        height: 250px !important;

    }

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative"> 

@include('mains.includes.nav')

    <div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">User Management</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Dashboard</li>

                            <li class="breadcrumb-item" aria-current="page">User Management</li>

                            <li class="breadcrumb-item active" aria-current="page">Update User

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

           <!--  <div class="right-title">

                <div class="dropdown">

                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

                            class="mdi mdi-dots-horizontal"></i></button>

                    <div class="dropdown-menu dropdown-menu-right">

                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

                        <div class="dropdown-divider"></div>

                        <button type="button" class="btn btn-rounded btn-success">Submit</button>

                    </div>

                </div>

            </div> -->

        </div>

    </div>




@if($rights['edit_delete']==1)
    <div class="row">

        <div class="col-12">

            <div class="box">

                <div class="box-header with-border">

                    <h4 class="box-title">Update User</h4>

                </div>

                <div class="box-body">

                      <form id="user_form" method="post" accept-charset="utf-8">

                         {{csrf_field()}}

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>EMPLOYEE CODE <span class="asterisk">*</span></label>

                                <input type="text" class="form-control" placeholder="EMPLOYEE CODE" id="employee_code" name="employee_code" value="{{$get_users->users_empcode}}">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>









                    </div>

                    <div class="row mb-10">



                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <div class="form-group">



                                    <div class="form-group">

                                        <label>SELECT ROLES <span class="asterisk">*</span></label>

                                        <select id="employee_role" name="select_role" class="form-control select2" style="width: 100%;">

                                            <option selected="selected" value="0" @if($get_users->users_assigned_role=="0") selected @endif>Select Role</option>

                                            <option @if($get_users->users_assigned_role=="Sub-User") selected @endif>Sub-User</option>
                                              <option @if($get_users->users_assigned_role=="Partner") selected @endif>Partner</option>

                                        </select>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>

                      

                    </div>

                    <div class="row mb-10" id="partner_div"  @if($get_users->users_partner_country=="" || $get_users->users_partner_country==null) style="display:none" @endif>
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">

                                        <label>Partner's Country <span class="asterisk">*</span></label>

                                        <select id="partner_country" name="partner_country" class="form-control select2" style="width: 100%;">

                                            <option selected="selected" value="0" hidden="hidden">--Select Country--</option>
                                            @foreach($countries as $country)
                                            @if($get_users->users_partner_country==$country->country_id)
                                            <option value="{{$country->country_id}}" selected="selected">{{$country->country_name}}</option>
                                            @else
                                            <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                            @endif
                                            @endforeach

                                        </select>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                        </div>
                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>USERNAME <span class="asterisk">*</span></label>

                                <input type="text" class="form-control" placeholder="USERNAME" id="username" name="username" value="{{$get_users->users_username}}">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>



                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>FIRST NAME <span class="asterisk">*</span></label>

                                <input type="text" class="form-control" placeholder="FIRST NAME" id="first_name" name="first_name" value="{{$get_users->users_fname}}">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>









                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>LAST NAME <span class="asterisk">*</span></label>

                                <input type="text" class="form-control" placeholder="LAST NAME" id="last_name" name="last_name" value="{{$get_users->users_lname}}">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>









                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>CONTACT NUMBER <span class="asterisk">*</span></label>

    

                                    <input type="text" id="contact_number" name="contact_number" class="form-control" autocomplete="off" placeholder="CONTACT NUMBER" value="{{$get_users->users_contact}}" maxlength="15">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>









                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>E-MAIL <span class="asterisk">*</span></label>

                                <input type="text" class="form-control" placeholder="E-MAIL"  id="employee_email" name="employee_email" value="{{$get_users->users_email}}">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>

                    </div>



                    <div class="row mb-10">



                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <div class="form-group">



                                    <div class="form-group">

                                        <label>SELECT STATUS <span class="asterisk">*</span></label>

                                        <select id="employee_status" name="select_status" class="form-control select2" style="width: 100%;">

                                            <option value="1" @if($get_users->users_status=="1") selected @endif>Active</option>

                                            <option value="0" @if($get_users->users_status=="0") selected @endif>InActive</option>

                                        </select>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>

                      

                    </div>
                    {{-- service type --}}
                    <?php
                        if($get_users['users_assigned_role']=='Sub-User')
                        {
                            $showdata='';
                        }
                        else
                        {
                            $showdata="display:none";   
                        }
                    ?>
                    <div class="row mb-10" style="<?php echo $showdata;?>">
                        
                            <div class="col-sm-12 col-md-12 col-lg-6">

                            </div>

                            <div class="col-sm-12">
                                <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                </div>
                                <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                    <i class="fa fa-plus-circle"></i> SERVICE TYPE DETAIL </h4>

                            </div>
                        </div>




                        <div class="row mb-10" style="<?php echo $showdata;?>">

                            <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">
                                            @php
                                        $service_type=explode(',',$get_users['user_service_type']);
                                            @endphp
                                        <label>SERVICE TYPE <span class="asterisk">*</span></label>
                                        <select class="form-control" style="width: 100%;"
                                            tabindex="-1" aria-hidden="true" multiple="multiple" name="service_type[]" id="service_type">
                                            <option value="hotel"   @if(in_array("hotel",$service_type)) selected @endif>Hotel</option>
                                            <option value="activity"  @if(in_array("activity",$service_type)) selected @endif>Activity</option>
                                            <!-- <option value="transportation"  @if(in_array("transportation",$service_type)) selected @endif>Transportation</option> -->
                                            <option value="guide" @if(in_array("guide",$service_type)) selected @endif>Guide</option>
                                            <option value="sightseeing" @if(in_array("sightseeing",$service_type)) selected @endif>Sightseeing</option>
                                            <option value="itinerary" @if(in_array("itinerary",$service_type)) selected @endif>Package</option>
                                             <option value="transfer" @if(in_array("transfer",$service_type)) selected @endif>Transfer</option>
                                              <option value="restaurant" @if(in_array("restaurant",$service_type)) selected @endif>Restaurant</option>
                                        </select>
                                    </div>
                            </div>
                            <div class="col-sm-4 col-md-4 col-lg-3">
                                                <br>
                                                <input name="select_all" type="checkbox"
                                                                    id="select_all">
                                                                <label for="select_all">Select All</label>
                                            </div>
                            @php
                            $hotel_markup="";
                            $activity_markup="";
                            $transportation_markup="";
                            $guide_markup="";
                            $sightseeing_markup="";
                            $itinerary_markup="";
                            $transfer_markup="";
                              $restaurant_markup="";

                            if($get_users['user_service_markup']!="")
                            {
                                $get_services=explode("///",$get_users['user_service_markup']);
                
                                for($services=0;$services< count($get_services);$services++)
                                {
                                    if($get_services[$services]!="")
                                    {
                                         $get_services_individual=explode("---",$get_services[$services]);
                                         $service_name=$get_services_individual[0]."_markup";
                                         $$service_name=$get_services_individual[1];


                                    }
                                }
                            }
                           

                            @endphp
                              <div class="col-sm-12">
                                                <div class='row'>
                                                    <div class="col-md-6"
                                                    style="padding:0">
                                                    <div class="col-sm-12 col-md-12 mt-20">
                                                        <div class="form-group">
                                                            <input type='hidden' name='service_name[]' value='hotel'>
                                                            <label for="">{{ strtoupper('Hotel')}}</label>
                                                            <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$hotel_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-12 mt-20">
                                                        <div class="form-group">
                                                         <input type='hidden' name='service_name[]' value='activity'>
                                                         <label for="">{{ strtoupper('Activity')}}</label>
                                                         <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %"  value="{{$activity_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                     </div>
                                                 </div>
                                                <!--  <div class="col-sm-12 col-md-12 mt-20">
                                                    <div class="form-group">
                                                        <input type='hidden' name='service_name[]' value='transportation'>
                                                        <label for="">{{ strtoupper('Transportation')}}</label>
                                                        <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$transportation_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                    </div>
                                                </div> -->
                                                <div class="col-sm-12 col-md-12 mt-20">
                                                    <div class="form-group">
                                                        <input type='hidden' name='service_name[]' value='guide'>
                                                        <label for="">{{ strtoupper('Guide')}}</label>
                                                        <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$guide_markup}}"  onkeypress='javascript:return validateNumber(event)'>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-12 mt-20">
                                                    <div class="form-group">
                                                       <input type='hidden' name='service_name[]' value='sightseeing'>
                                                       <label for="">{{ strtoupper('Sightseeing')}}</label>
                                                       <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$sightseeing_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                   </div>
                                               </div>
                                                 <div class="col-sm-12 col-md-12 mt-20">
                                                    <div class="form-group">
                                                       <input type='hidden' name='service_name[]' value='itinerary'>
                                                       <label for="">{{ strtoupper('Package')}}</label>
                                                       <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$itinerary_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                   </div>
                                               </div>
                                                <div class="col-sm-12 col-md-12 mt-20">
                                                    <div class="form-group">
                                                       <input type='hidden' name='service_name[]' value='transfer'>
                                                       <label for="">{{ strtoupper('Transfer')}}</label>
                                                       <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$transfer_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                   </div>
                                               </div>
                                                <div class="col-sm-12 col-md-12 mt-20">
                                                    <div class="form-group">
                                                       <input type='hidden' name='service_name[]' value='restaurant'>
                                                       <label for="">{{ strtoupper('Restaurant')}}</label>
                                                       <input type='text' class='form-control' name='service_cost[]' placeholder="Markup in %" value="{{$restaurant_markup}}" onkeypress='javascript:return validateNumber(event)'>
                                                   </div>
                                               </div>



                                           </div>

                                       </div>


                                   </div>

                           

                        </div>
                    {{-- end service type --}}

                    <div class="row mb-10">

                        <div class="col-md-12">

                            <div class="box-header with-border"

                                style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">

                                <input type="hidden"  name="users_id" value="{{$users_id}}">

                                <button type="button"  id="update_user" class="btn btn-rounded btn-primary mr-10">Update</button>

                                <button type="button" id="discard_user" class="btn btn-rounded btn-primary">Discard</button>

                            </div>

                        </div>

                    </div>

                </form>

                </div>

            </div>

        </div>
    </div>
      @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif

</div>

</div>

</div>





@include('mains.includes.footer')

@include('mains.includes.bottom-footer')

<script>  
$(document).ready(function()
    {
        $(".select2").select2();
         $('#service_type').select2({
            "placeholder":"SELECT SERVICE TYPE",
         });

        $("#employee_role").change(function()
        {
            if($(this).val()=="Partner")
            {
                $("#partner_country").val(0).trigger("change");
                $("#partner_div").show(500);

            }
            else
            {
                 $("#partner_country").val(0).trigger("change");
                $("#partner_div").hide();
            }
        });
    });
        $("#discard_user").on("click",function()

        {

            window.history.back();

        });



        $(document).on("click","#update_user",function()

        {

            var employee_code=$("#employee_code").val();

            var employee_role=$("#employee_role").val(); 

             var partner_country=$("#partner_country").val(); 

            var username=$("#username").val();

            var first_name=$("#first_name").val();  

            var last_name=$("#last_name").val(); 

            var contact_number=$("#contact_number").val();

            var employee_email=$("#employee_email").val(); 
            var service_type=$("#service_type").val();
             var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; 



            if(employee_code.trim()=="")

            {

                $("#employee_code").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#employee_code").css("border","1px solid #9e9e9e"); 

            }



           if(employee_role.trim()=="0")

            {

                $("#employee_role").parent().find('.select2-selection').css("border","1px solid #cf3c63");

            }

            else

            {

             $("#employee_role").parent().find('.select2-selection').css("border","1px solid #9e9e9e"); 

            }

             if(employee_role.trim()=="Partner" && partner_country.trim()=="0")

            {

                $("#partner_country").parent().find('.select2-selection').css("border","1px solid #cf3c63");

            }

            else

            {

             $("#partner_country").parent().find('.select2-selection').css("border","1px solid #9e9e9e"); 

            }



             if(username.trim()=="")

            {

                $("#username").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#username").css("border","1px solid #9e9e9e"); 

            }

             if(first_name.trim()=="")

            {

                $("#first_name").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#first_name").css("border","1px solid #9e9e9e"); 

            }

              if(last_name.trim()=="")

            {

                $("#last_name").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#last_name").css("border","1px solid #9e9e9e"); 

            }

             if(contact_number.trim()=="")

            {

                $("#contact_number").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#contact_number").css("border","1px solid #9e9e9e"); 

            }

             if(employee_email.trim()=="")

            {

                $("#employee_email").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#employee_email").css("border","1px solid #9e9e9e"); 

            }

            if(!regex.test(employee_email.trim()) && employee_email.trim()!="")

            {

                $("#employee_email").css("border","1px solid #cf3c63");

            }
             if(service_type=="")
                {
                    $("#service_type").parent().find("label").css("border","1px solid #cf3c63");
                }
                else
                {
                 $("#service_type").parent().find("label").css("border","1px solid white"); 
                }



            if(employee_code.trim()=="")

            {

                $("#employee_code").focus();

            }

            else if(employee_role.trim()=="0")

            {

                $("#employee_role").parent().find('.select2-selection').focus();

            }
            else if(employee_role.trim()=="Partner" && partner_country.trim()=="0")

            {

                $("#partner_country").parent().find('.select2-selection').focus();

            }

              else if(username.trim()=="")

            {

                $("#username").focus();

            }

             else if(first_name.trim()=="")

            {

                $("#first_name").focus();

            }

            else if(last_name.trim()=="")

            {

                $("#last_name").focus();

            }

            else if(contact_number.trim()=="")

            {

                $("#contact_number").focus();

            }

            else if(employee_email.trim()=="")

            {

                $("#employee_email").focus();

            }

            else if(!regex.test(employee_email.trim()))

            {

                $("#employee_email").focus();

            }
          else if(service_type=="")
            {
                $("#service_type").focus();
            }

            else

            {

                $("#update_user").prop("disabled",true);

                var formdata=new FormData($("#user_form")[0]);

                $.ajax({

                    url:"{{route('update-user')}}",

                    data: formdata,

                    type:"POST",

                    processData: false,

                    contentType: false,

                    success:function(response)

                    {

                       if(response.indexOf("exist")!=-1)

                       {

                          swal("Already Exist!", "User with this email or username already exists");

                       }

                      else if(response.indexOf("success")!=-1)

                      {

                        swal({title:"Updated",text:"User Updated Successfully !",type: "success"},

                         function(){ 

                             location.reload();

                         });

                      }

                      else if(response.indexOf("fail")!=-1)

                      {

                       swal("ERROR", "User cannot be update right now! ");

                      }

                        $("#update_user").prop("disabled",false);

                    }

                });  

            }







        });

</script>
<script>
      $("#select_all").click(function(){
    if($("#select_all").is(':checked') ){
        $('#service_type').select2('destroy').find('option').prop('selected', 'selected').end().select2();
    }else{
        $('#service_type').select2('destroy').find('option').prop('selected', false).end().select2();
     }
});
</script>

</body>

</html>

