@include('mains.includes.top-header')

<style>

    .iti-flag {

        width: 20px;

        height: 15px;

        box-shadow: 0px 0px 1px 0px #888;

        background-image: url("{{asset('assets/images/flags.png')}}") !important;

        background-repeat: no-repeat;

        background-color: #DBDBDB;

        background-position: 20px 0

    }



    div#cke_1_contents {

        height: 250px !important;

    }
    .display_hide{
        display:none !important;
    }
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('mains.includes.nav')

	<div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">Enquiry Management</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Dashboard</li>

                            <li class="breadcrumb-item" aria-current="page">Enquiry Management</li>

                            <li class="breadcrumb-item active" aria-current="page">Create Enquiry

                            </li>

                        </ol>

                    </nav>

                </div>

            </div>

           <!--  <div class="right-title">

                <div class="dropdown">

                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

                            class="mdi mdi-dots-horizontal"></i></button>

                    <div class="dropdown-menu dropdown-menu-right">

                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

                        <div class="dropdown-divider"></div>

                        <button type="button" class="btn btn-rounded btn-success">Submit</button>

                    </div>

                </div>

            </div> -->

        </div>

    </div>




@if($rights['add']==1)
    <div class="row">

        <div class="col-12">

            <div class="box">

                <div class="box-header with-border">

                    <h4 class="box-title">Create Enquiry</h4>

                </div>

                <div class="box-body">

                    <form id="enquiry_form" method="post" accept-charset="utf-8">

                        {{csrf_field()}}

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-3">

                            <div class="form-group">

                                <label for="organization_name">ORGANIZATION NAME</label>

                                <input id="organization_name" type="text" class="form-control" placeholder="ORGANIZATION NAME" name="organization_name">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-3">

                            <div class="form-group">

                                <label for="customer_name">CUSTOMER NAME <span class="asterisk">*</span></label>

                                <input id="customer_name" type="text" class="form-control" placeholder="CUSTOMER NAME " name="customer_name">

                            </div>

                        </div>







                        <div class="col-sm-6 col-md-3">

                            <div class="form-group">

                                <label for="customer_contact">CUSTOMER CONTACT<span class="asterisk">*</span></label>

                                    <input id="customer_contact" type="text"

                                        class="form-control input-lg"

                                        id="customer_contact"  name="customer_contact" autocomplete="off"

                                        placeholder="Mobile Number" maxlength="15" value="+995" title="Customer contact must contain country code">
                                        <input type="hidden" name="customer_contact_code" title="Customer contact must contain country code">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-3" style="display:none">

                            <div class="form-group">

                                <label for="customer_alt_contact">ALTERNATE CONTACT ID</label>

        
                                    <input id="customer_alt_contact" type="text" 

                                

                                        class="form-control input-lg"

                                        id="alternate_contact_number" name="alternate_contact_number" autocomplete="off"

                                        placeholder="Alt Mobile Number" maxlength="15" onkeypress='validateNumber(event)'>

                                </div>


                        </div>

                          <div class="col-sm-6 col-md-1">
                            <div class="form-group">
                                <br>
                                <h5>- OR -</h5>
                            </div>
                          </div>           
                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="customer_email">EMAIL ID <span class="asterisk">*</span> </label>

                                <input id="customer_email" type="text" class="form-control" placeholder="EMAIL ID " name="customer_email">

                            </div>

                        </div>

                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12" id="showenqdata">
                            
                        </div>
                    </div>

                    <div class="row mb-10">




                        <div class="col-sm-6 col-md-3" style="display:none">

                            <div class="form-group">

                                <label for="customer_alt_email" >ALTERNATE EMAIL ID</label>

                                <input id="customer_alt_email" type="text" class="form-control" placeholder="ALTERNATE EMAIL ID" name="customer_alt_email">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2 display_hide">

                            <div class="form-group">

                                <label for="passport_no" >PASSPORT NUMBER</label>

                                <input  id="passport_no" type="text" class="form-control" placeholder="PASSPORT NUMBER"  name="passport_no">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2 display_hide">

                            <div class="form-group">





                                <label for="passport_validity">PASSPORT VALIDITY</label>



                                <div class="input-group date">

                                    <input  id="passport_validity" type="text" placeholder="PASSPORT VALIDITY"

                                        class="form-control pull-right datepicker" name="passport_validity">

                                    <div class="input-group-addon">

                                        <i class="fa fa-calendar"></i>

                                    </div>

                                </div>

                                <!-- /.input group -->



                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">



                                <div class="form-group">

                                    <label class="enquiry_type">ENQUIRY TYPE <span class="asterisk">*</span></label>

                                    <select id="enquiry_type" class="form-control" style="width: 100%;" name="enquiry_type">

                                    	<option value="0" selected="selected">Select type</option>
                                        @foreach($enquiry_type as $enquirytype)
                                        <option value="{{$enquirytype->enquiry_type_id}}">{{$enquirytype->enquiry_type_name}}</option>
                                        @endforeach
                                    </select>

                                </div>

                            </div>

                        </div>
                           <div class="col-sm-4 col-md-6 display_hide">

                            <div class="form-group">

                                <label for="address">ADDRESS</label>

                                <textarea id="address" rows="2" cols="5" class="form-control" placeholder="ADDRESS" name="address"></textarea>

                            </div>

                        </div>

                       <div class="col-sm-4 col-md-2">

                            <div class="form-group">

                                <label for="area">NATIONALITY </label>

                                <input id="area" type="text" class="form-control" placeholder="NATIONALITY" name="area">

                            </div>

                        </div>

 






                        <div class="col-sm-4 col-md-2">

                            <div class="form-group">

                                <label for="customer_country">RESIDENCE COUNTRY <span class="asterisk">*</span></label>

                                <select id="customer_country" class="form-control select2" style="width: 100%;" name="customer_country">

                                    <option selected="selected" value="0">COUNTRY</option>

                                    @foreach($countries as $country)

										<option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-sm-4 col-md-2">

                            <div class="form-group">

                                <label for="customer_city">CITY</label>

                                <input id="customer_city" type="text" class="form-control" placeholder="CITY" name="customer_city">

                            </div>

                        </div>
                         <div class="col-sm-6 col-md-3">

                            <div class="form-group">

                                <label for="booking_range">TRAVEL DATES <span class="asterisk">*</span> </label>

                                <div class="input-group">



                                    <input id="booking_range" type="text" class="form-control pull-right"

                                        placeholder="SELECT DATE" name="booking_range" value="">

                                    <div class="input-group-addon">

                                        <i class="fa fa-calendar"></i>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-1">

                            <div class="form-group">

                                <p id="no_of_days_p">Number Of Nights: 0</p>

                                <input id="no_of_days" type="hidden" name="no_of_days">

                            </div>

                        </div>

                    </div>



                    <!--<div class="row mb-10">-->


                     

                    <!--</div>-->



                    <div class="row mb-10">





                        <div class="col-sm-6 col-md-3 display_hide">

                            <div class="form-group">

                                <label for="customer_state">STATE</label>

                                <input id="customer_state" type="text" class="form-control" placeholder="STATE" name="customer_state">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-3 display_hide">

                            <div class="form-group">

                                <label for="customer_zipcode">ZIP CODE</label>

                                <input id="customer_zipcode" type="text" class="form-control" placeholder="ZIP CODE" name="customer_zipcode">

                            </div>

                        </div>







                       

                    </div>



                    <div class="row mb-10">



                        <div class="col-sm-6 col-md-1"  style="padding-right: 0;">

                            <div class="form-group">

                                <label for="no_of_adults">ADULTS <span class="asterisk">*</span></label>

                                <input id="no_of_adults" type="text" class="form-control" value="" name="no_of_adults" onkeypress='validateNumber(event)' maxlength="3">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-1">

                            <div class="form-group">

                                <label for="no_of_kids">KIDS <span class="asterisk">*</span></label>

                               <select id="no_of_kids" class="form-control" style="width: 100%;"  name="no_of_kids">
                                    <option value="0" selected="selected">0</option>
                                   @for($i=1;$i<=10;$i++)
                                   <option value="{{$i}}">{{$i}}</option>
                                   @endfor
                                </select>


                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="hotel_type">HOTEL TYPE</label>

                                <select id="hotel_type" class="form-control" style="width: 100%;"  name="hotel_type">

                                    <option value="0" selected="selected">Hotel type</option>

                                    <option value="Apartment">Apartment</option>

                                    <option value="Hotel">Hotel</option>
                                </select>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="budget_type">BUDGET TYPE</label>

                                <input id="budget_type" type="text" class="form-control" placeholder="BUDGET" name="budget_type">

                            </div>

                        </div>



                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="enquiry_source">ENQUIRY SOURCE</label>

                                <select id="enquiry_source" class="form-control" style="width: 100%;" name="enquiry_source">

                                    <option selected="selected" value="0">SELECT SOURCE</option>

                                    <option value="Facebook">Facebook</option>

                                    <option value="Call">Call</option>

                                    <option value="Email">Email</option>

                                    <option value="Offline">Offline</option>
                                    
                                    <option value="Whatsapp">Whatsapp</option>
                                    
                                    <option value="Instagram">Instagram</option>
                                    
                                     <option value="Website">Website</option>
                                </select>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="enquiry_prospect">PROSPECT <span class="asterisk">*</span></label>

                                <select id="enquiry_prospect" class="form-control" style="width: 100%;" name="enquiry_prospect">

                                    <option selected="selected" value="0">PROSPECT</option>

                                    <option value="Hot">Hot</option>

                                    <option value="Warm">Warm</option>

                                    <option value="Cold">Cold</option>

                                </select>

                            </div>

                        </div>



                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="enquiry_status">STATUS <span class="asterisk">*</span> </label>

                                <select id="enquiry_status" class="form-control" style="width: 100%;" name="enquiry_status">

                                    <option selected="selected" value="0">STATUS</option>
                                     <option value="Open">Open</option>
                                    <option value="Win">Win</option>
                                    <option value="Refused">Refused</option>
                                     <option value="Cancelled">Cancelled</option>
                                    <option value="ITSent">IT Sent</option>

                                </select>

                            </div>

                        </div>



                    </div>
                    <div class="row mb-10" id="child_age_div" style="display:none">
                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-3">

                            <div class="form-group">

                                <label for="assigned_to">ASSIGNED TO</label>

                                <select id="assigned_to" class="form-control select2" style="width: 100%;" name="assigned_to">

                                    <option selected="selected" value="0">SELECT ASSIGNED USER</option>

                                    @foreach($users as $user)

                                    <option value="{{$user->users_id}}">{{$user->users_fname}} {{$user->users_lname}}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-3">

                            <div class="form-group">

                                <label for="nxt_followup_date">NEXT FOLLOWUP <span class="asterisk">*</span> </label>

                                <div class="input-group date">

                                    <input id="nxt_followup_date" type="text" class="form-control pull-right datepicker" placeholder="DATE" name="nxt_followup_date" autocomplete="off" readonly="readonly">

                                    <div class="input-group-addon">

                                        <i class="fa fa-calendar"></i>

                                    </div>

                                </div>

                            </div>

                        </div>







                        <div class="col-sm-6 col-md-3 display_hide">

                            <div class="form-group">

                                <label for="customer_dob">CUSTOMER DOB</label>

                                <div class="input-group date">

                                    <input id="customer_dob" type="text" class="form-control pull-right datepicker" placeholder="DATE" name="customer_dob" autocomplete="off" readonly="readonly">

                                    <div class="input-group-addon">

                                        <i class="fa fa-calendar"></i>

                                    </div>

                                </div>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-3 display_hide">

                            <div class="form-group">

                                <label for="wedding_date">WEDDING ANNIVERSARY DATE </label>

                                <div class="input-group date">

                                    <input id="wedding_date" type="text" class="form-control pull-right datepicker" placeholder="DATE" name="wedding_date" autocomplete="off" readonly="readonly">

                                    <div class="input-group-addon">

                                        <i class="fa fa-calendar"></i>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>



                    <div class="row mb-10 display_hide">





                        <div class="col-sm-7">

                            <div class="row">

                                <div class="col-md-6">

                                    <label>CURRENCY EXCHANGE RATE <span class="asterisk">*</span></label>

                                </div>

                                <div class="col-md-6">



                                    <input name="currency_exchange_rate_status" type="radio" id="radio_30" class="with-gap radio-col-primary" value="Yes"

                                        />

                                    <label for="radio_30">Yes</label>

                                    <input name="currency_exchange_rate_status" type="radio" id="radio_31"

                                        class="with-gap radio-col-primary"  value="No" checked />

                                    <label for="radio_31">No</label>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    <label>VISA <span class="asterisk">*</span></label>

                                </div>

                                <div class="col-md-6">

                                    <input name="have_visa" type="radio" id="radio_32"

                                        class="with-gap radio-col-primary" value="Yes" />

                                    <label for="radio_32">Yes</label>

                                    <input name="have_visa" type="radio" id="radio_33"

                                        class="with-gap radio-col-primary" value="No" checked/>

                                    <label for="radio_33">No</label>

                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-6">

                                    <label>INSURANCE <span class="asterisk">*</span></label>

                                </div>

                                <div class="col-md-6">

                                    <input name="have_insurance_status" type="radio" id="radio_34"

                                        class="with-gap radio-col-primary" value="Yes" />

                                    <label for="radio_34">Yes</label>

                                    <input name="have_insurance_status" type="radio" id="radio_35"

                                        class="with-gap radio-col-primary" value="No" checked/>

                                    <label for="radio_35">No</label>

                                </div>

                            </div>









                        </div>

                     







                        <div class="col-sm-6">

                        	<div id="currency_div" class="row" style="display:none;">

                        		<div class="col-md-6 col-md-3">

                        			<div class="form-group">

                        				<label for="currency" >Currency <span class="asterisk">*</span></label>

                        				<select  id="currency" class="form-control" style="width: 100%;" name="currency">

                        					<option selected="selected" value="0">CURRENCY</option>

                        					  @foreach($currency as $curr)

                                        <option value="{{$curr->code}}">{{$curr->code}} ({{$curr->name}})</option>

                                                 @endforeach



                        				</select>

                        			</div>

                        		</div>

                        		<div class="col-md-6">



                        			<div class="form-group">

                        				<label for="currency_exchange_rate" >CURRENCY EXCHANGE RATE <span class="asterisk">*</span></label>

                        					<input id="currency_exchange_rate"  type="text" class="form-control pull-right" placeholder="CURRENCY EXCHANGE RATE" name="currency_exchange_rate">

                        			</div>

                        		</div>

                        	</div>

                            <div id="insurance_div" class="row" style="display:none;">

                                <div class="col-md-6 col-md-3">

                                     <div class="form-group">

                                        <label for="insurance_days" >INSURANCE DAYS <span class="asterisk">*</span></label>

                                            <input id="insurance_days" type="text" class="form-control pull-right" placeholder="INSURANCE DAYS" name="insurance_days">

                                    </div>

                                    

                                </div>

                                <div class="col-md-6 col-md-3">



                                  <div class="form-group">

                                        <label for="insurance_type">INSURANCE TYPE <span class="asterisk">*</span></label>

                                        <select id="insurance_type" class="form-control" style="width: 100%;" name="insurance_type">

                                            <option selected="selected" value="0">INSURANCE TYPE</option>

                                            <option value="Land">Land</option>

                                            <option value="Land">Air</option>

                                        </select>

                                    </div>

                                </div>

                            </div>



                        </div>

                    </div>







                    <div class="row mb-10">

                        <div class="col-sm-6">

                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">



                            </div>

                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">

                                <i class="fa fa-plus-circle"></i> DEPARTURE CITY</h4>



                        </div>

                        <div class="col-sm-6">

                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">



                            </div>

                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">

                                <i class="fa fa-plus-circle"></i> DESTINATIONS</h4>



                        </div>

                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-3">

                            <div class="form-group" id="departure_country_div">

                                <label for="departure_country">DEPARTURE COUNTRY   <span class="asterisk">*</span></label>

                                <select id="departure_country" class="form-control select2" name="departure_country" style="width: 100%;">

                                    <option selected="selected" value="0">SELECT COUNTRY</option>

                                    @foreach($countries as $country)

										<option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-3" >

                            <div class="form-group" id="departure_city_div">

                                <label for="departure_city" >DEPARTURE CITY</label>

                                <input  id="departure_city"type="text" class="form-control" name="departure_city" placeholder="CITY">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-3 enquiry_country" id="enquiry_country1">

                            <div class="form-group">

                                <label> COUNTRY <span class="asterisk">*</span></label>

                                <select id="enquiry_location_country" class="form-control select2 enquiry_location_country" name="enquiry_location_country[]" style="width: 100%;">

                                    <option selected="selected" value="0">COUNTRY</option>

                                    @foreach($countries as $country)

                                    <option value="{{$country->country_id}}">{{$country->country_name}}</option>

                                    @endforeach

                                </select>

                            </div>



                        </div>

                        <div class="col-sm-4 col-md-2 enquiry_city" id="enquiry_city1">

                           <div class="form-group">

                            <label> CITY <span class="asterisk">*</span></label>

                            <input type="text" class="form-control enquiry_location_city" name="enquiry_location_cities[]" placeholder="CITY">

                        </div>

                     </div>

                      <div class="col-sm-2 col-md-1 add_minus_more" id="add_minus_more1">

                             <img class="plus-icon add_more_locations" src="{{ asset('assets/images/add_icon.png') }}">

                        </div>



                    </div>



                    <div class="row mb-10" style="display:none">

                        <div class="col-sm-12">

                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">



                            </div>

                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">

                                <i class="fa fa-plus-circle"></i> ENQUIRY PASSENGERS DETAILS</h4>



                        </div>



                    </div>

                    <div class="row mb-10" style="display:none">





                        <div class="col-sm-6 col-md-4">

                            <div class="form-group">

                                <label for="passenger_name" >NAME </label>

                                <input id="passenger_name" type="text" class="form-control" placeholder="ORGANIZATION NAME" name="passenger_name">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="passenger_dob">BIRTHDATE</label>

                                <div class="input-group date">

                                    <input id="passenger_dob" type="text" class="form-control pull-right datepicker"

                                        placeholder="BIRTHDATE" name="passenger_dob" autocomplete="off" readonly="readonly">

                                    <div class="input-group-addon">

                                        <i class="fa fa-calendar"></i>

                                    </div>

                                </div>

                            </div>

                        </div>



                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="passenger_pan_number"> PAN NUMBER <span style="color: #cf3c63" title="AAAPL1234C">&#63;</span></label>

                                <input  id="passenger_pan_number" type="text" class="form-control" placeholder="PAN NUMBER" name="passenger_pan_number">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="passenger_passport_no"> PASSPORT NUMBER </label>

                                <input id="passenger_passport_no" type="text" class="form-control" placeholder=" PASSPORT NUMBER " name="passenger_passport_no">

                            </div>

                        </div>

                        <div class="col-sm-6 col-md-2">

                            <div class="form-group">

                                <label for="gstin_no"> GSTIN NUMBER



                                </label>

                                <input id="gstin_no" type="text" class="form-control" placeholder="GSTIN NUMBER" name="gstin_no" maxlength="15">

                            </div>

                        </div>

                       <!--  <div class="col-sm-6 col-md-12">

                            <img class="plus-icon" style="display: block;" src="{{ asset('assets/images/add_icon.png') }}">

                        </div> -->







                    </div>



                    <div class="row mb-10">

                        <div class="col-sm-12">

                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">



                            </div>

                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">

                                <i class="fa fa-plus-circle"></i> ENQUIRY COMMENTS <span class="asterisk">*</span></h4>



                        </div>



                    </div>

                    <div class="row mb-10">



                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label for="enquiry_comments">Comment <span class="asterisk">*</span></label>

                                <textarea  id="enquiry_comments" rows="5" cols="5" class="form-control" placeholder="Comment" name="enquiry_comments"></textarea>

                            </div>



                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>

                    </div>

                    <div class="row mb-10">



                        <div class="col-sm-6 col-md-6">

                            <div class="img_group">

                                <div class="box1">

                                    <input class="hide" type="file" id="upload_image"

                                        accept="image/png,image/jpg,image/jpeg" multiple="" 

                                       name="enquiry_comments_file" onchange="previewFile()">

                                    <button type="button" onclick="document.getElementById('upload_image').click()"

                                        id="upload_0"

                                        class="btn red btn-outline btn-circle">+

                                    </button>

                                </div>

                                <br>

                        <img id="image_preview" src="" height="200" alt="Image preview..." style="display:none">

                                <!-- ngRepeat: (itemindex,item) in temp_loop.enquiry_comment_attachment track by $index -->

                            </div>

                            



                        </div>

                        <div class="col-sm-6 col-md-6">



                        </div>



                    </div>

                    <div class="row mb-10" style="display: none;">

                        <div class="col-md-6">

                            <div class="form-group">

                                <div class="checkbox">

                                    <input type="checkbox" id="Checkbox_1" name="response_email_status"> 

                                    <label for="Checkbox_1">Response Email</label>

                                </div>



                                <div class="checkbox">

                                    <input type="checkbox" id="Checkbox_2" name="response_sms_status">

                                    <label for="Checkbox_2">Response SMS</label>

                                </div>





                            </div>

                        </div>

                        <div class="col-md-6">





                        </div>

                    </div>



                    <div class="row mb-10" id="subject_div" style="display: none;">

                        <div class="col-md-3">

                            <p>Subject:</p>

                        </div>

                        <div class="col-md-3">

                            <p>Enquiry Acknowledgement from ADMIN VOICEX</p>



                        </div>

                    </div>

                    <div class="row mb-10" id="email_div" style="display: none;">

                        <div class="col-md-12">

                            <div class="box">



                                <!-- /.box-header -->

                                <div class="box-body">

                                    <textarea id="editor" name="response_email_text"><h1>Hello world!</h1></textarea> 

                                        

                                </div>

                            </div>

                        </div>



                    </div>



                    <div class="row mb-10">

                        <div class="col-md-12">

                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                <button type="button" id="save_enquiry" class="btn btn-rounded btn-primary mr-10">Save</button>

                                <button type="button" id="discard_enquiry" class="btn btn-rounded btn-primary">Discard</button>

                            </div>

                        </div>

                    </div>

                </form>

                </div>

            </div>

        </div>
    </div>
       @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif

</div>

</div>

</div>



@include('mains.includes.footer')

@include('mains.includes.bottom-footer')

<script>

    function previewFile() {

      var preview = document.getElementById('image_preview');

      var file    = document.querySelector('input[type=file]').files[0];

      var reader  = new FileReader();



      reader.onloadend = function () {

        preview.src = reader.result;

        preview.style.display="block";

    }



    if (file) {

        reader.readAsDataURL(file);

    } else {

        preview.src = "";

    }

}

    $(document).ready(function()

    {

    //Initialize Select2 Elements

    $('.select2').select2();


 $('#customer_contact').intlTelInput({
    utilsScript: "{{ asset('assets/intl-tel-input-master/build/js/utils.js') }}"
 });




    var date = new Date();

    date.setDate(date.getDate());

      //Passport validity Date picker

      $('#passport_validity').datepicker({

          autoclose: true,

          todayHighlight: true,

          format: 'yyyy-mm-dd',

          startDate: date,

      });



      //  //Followup Datetime picker

        $("#nxt_followup_date").datetimepicker({

             autoclose: true,

        format: "yyyy-mm-dd - hh:ii:ss",

        startDate: new Date(),

        });



        //DOB picker

      $('#customer_dob').datepicker({

          autoclose: true,

          todayHighlight: true,

          format: 'yyyy-mm-dd',

          endDate: date,

      });

        //Wedding Date picker

      $('#wedding_date').datepicker({

          autoclose: true,

          todayHighlight: true,

          format: 'yyyy-mm-dd',

      });

       //Passenger Dob Date picker

      $('#passenger_dob').datepicker({

          autoclose: true,

          todayHighlight: true,

          format: 'yyyy-mm-dd',

          endDate: date,

      });



       $('#booking_range').daterangepicker({

         locale: {

            format: 'YYYY-MM-DD',

              cancelLabel: 'Clear'

        }

    }).on('apply.daterangepicker', function(ev, picker) {

                var start = moment(picker.startDate.format('YYYY-MM-DD'));

                var end   = moment(picker.endDate.format('YYYY-MM-DD'));

                var diff = end.diff(start, 'days'); // returns correct number

               $("#no_of_days").val(diff);

               $("#no_of_days_p").text("Number Of Nights: "+diff);

            });





    });

     $(document).on("click","#discard_enquiry",function()

    {

        window.history.back();

    });

$(document).on("change",'input[name="currency_exchange_rate_status"]',function()

{

    if($(this).val()=="Yes")

        $("#currency_div").show();

    else

        $("#currency_div").hide();

});

$(document).on("change",'input[name="have_insurance_status"]',function()

{

    if($(this).val()=="Yes")

        $("#insurance_div").show();

    else

        $("#insurance_div").hide();

});

$(document).on("change",'input[name="response_email_status"]',function()

{

    if($(this).is(":checked"))

    {

        $("#email_div").show();

        $("#subject_div").show();

    }

    else

    {

 $("#email_div").hide();

        $("#subject_div").hide();

    }

});

$(document).on("click",".add_more_locations",function()

{

    var source = "{!! asset('assets/images/minus_icon.png') !!}";

    var lastid=$(".enquiry_country:last").attr('id').split('enquiry_country')[1];

    lastid++;

    var insertdata=$(this).parent().parent().clone();

    insertdata.find("#departure_city_div").remove();

    insertdata.find("#departure_country_div").remove();

    insertdata.find(".add_more_locations").attr("src",source);

    insertdata.find(".enquiry_country").attr("id","enquiry_country"+lastid);

    insertdata.find(".enquiry_city").attr("id","enquiry_city"+lastid);

    insertdata.find(".add_minus_more").attr("id","add_minus_more"+lastid);

    insertdata.find(".add_more_locations").removeClass('plus-icon add_more_locations').addClass('minus-icon remove_more_locations pull-right');

    insertdata.find(".enquiry_location_country").attr("id","enquiry_location_country"+lastid);

      insertdata.find(".enquiry_location_city").attr("id","enquiry_location_city"+lastid);

    insertdata.find("#enquiry_location_country"+lastid).select2();

    insertdata.find(".select2-container").eq(1).remove();

    insertdata.find("#enquiry_location_city"+lastid).val('');





      $(".add_minus_more:last").parent().after(insertdata);





}); 

$(document).on("click",".remove_more_locations",function()

{

    $(this).parent().parent().remove();



});   

$(document).on("change","#no_of_kids",function()
{
    var html_data="";
    var kids_count=$(this).val();
    for($i=1;$i<=kids_count;$i++)
    {
        html_data+='<div class="col-md-2"> <div class="form-group"><label for="child_age'+$i+'">Child Age '+$i+' </label><input type="text" id="child_age'+$i+'" name="child_age[]" class="form-control child_age"></div></div>'
    }
    $("#child_age_div").html(html_data);
    $("#child_age_div").show();
});

$(document).on("click","#save_enquiry",function()

{

    var customer_name=$("#customer_name").val();

    var customer_contact=$("#customer_contact").val();

    var customer_email=$("#customer_email").val();

    var customer_alt_email=$("#customer_alt_email").val();

    var enquiry_type=$("#enquiry_type").val();

     var customer_country=$("#customer_country").val();

    var booking_range=$("#booking_range").val();

    var no_of_adults=$("#no_of_adults").val();

    var no_of_kids=$("#no_of_kids").val();

    var enquiry_prospect=$("#enquiry_prospect").val();

    var enquiry_status=$("#enquiry_status").val();

    var nxt_followup_date=$("#nxt_followup_date").val();

    var currency_exchange_rate_status=$("input[name='currency_exchange_rate_status']:checked").val();

    var have_insurance_status=$("input[name='have_insurance_status']:checked").val();

    var departure_country=$("#departure_country").val();

    var departure_city=$("#departure_city").val();

    var passenger_name=$("#passenger_name").val();

    var passenger_dob=$("#passenger_dob").val();

    var passenger_pan_number=$("#passenger_pan_number").val();

    var passenger_passport_no=$("#passenger_passport_no").val();

    var gstin_no=$("#gstin_no").val();

    var enquiry_comments=$("#enquiry_comments").val();

    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    var regex_gst = /^([0-9]){2}([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}([0-9]){1}([a-zA-Z]){1}([0-9]){1}?$/;



    if(customer_name.trim()=="")

    {

        $("#customer_name").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#customer_name").css("border","1px solid #9e9e9e"); 

    }



     if(customer_contact.trim()=="" && customer_email.trim()=="")

    {

        $("#customer_contact").css("border","1px solid #cf3c63");
        $("#customer_email").css("border","1px solid #cf3c63");

    }

    else

    {
          $("#customer_email").css("border","1px solid #9e9e9e"); 
       $("#customer_contact").css("border","1px solid #9e9e9e"); 

    }

    

    // if(customer_email.trim()=="")

    // {

        

    // }

    // else

    // {

     

    // }

    if(!regex.test(customer_email.trim()) && customer_email.trim()!="")

    {

        $("#customer_email").css("border","1px solid #cf3c63");

    }



    if(!regex.test(customer_alt_email.trim()) && customer_alt_email.trim()!="")

    {

        $("#customer_alt_email").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#customer_alt_email").css("border","1px solid #9e9e9e"); 

    }



    if(enquiry_type.trim()=="0")

    {

        $("#enquiry_type").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#enquiry_type").css("border","1px solid #9e9e9e"); 

    }

    if(customer_country.trim()=="0")

    {

        $("#customer_country").parent().find('.select2-container').css("border","1px solid #cf3c63");

    }

    else

    {

       $("#customer_country").parent().find('.select2-container').css("border","1px solid #9e9e9e"); 

    }

    if(booking_range.trim()=="")

    {

        $("#booking_range").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#booking_range").css("border","1px solid #9e9e9e"); 

    }

    if(no_of_adults.trim()=="")

    {

        $("#no_of_adults").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#no_of_adults").css("border","1px solid #9e9e9e"); 

    }

    if(no_of_kids.trim()=="")

    {

        $("#no_of_kids").css("border","1px solid #cf3c63");

    }

    else

    {

     $("#no_of_kids").css("border","1px solid #9e9e9e"); 

    }
var child_age_error=0;
    // if(no_of_kids!="0")
    // {

    //     $(".child_age").each(function()
    //     {
    //         if($(this).val().trim()=="")
    //         {
    //             $(this).css("border","1px solid #cf3c63");
    //              $(this).focus();
    //             child_age_error++;
    //         }
    //         else
    //         {
    //             $(this).css("border","1px solid #9e9e9e");
    //         }
    //     })

    // }

     if(enquiry_prospect.trim()=="0")

    {

        $("#enquiry_prospect").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#enquiry_prospect").css("border","1px solid #9e9e9e"); 

    }

    if(enquiry_status.trim()=="0")

    {

        $("#enquiry_status").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#enquiry_status").css("border","1px solid #9e9e9e"); 

    }

     if(nxt_followup_date.trim()=="")

    {

        $("#nxt_followup_date").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#nxt_followup_date").css("border","1px solid #9e9e9e"); 

    }

    if(currency_exchange_rate_status=="Yes")

    {

        if($("#currency").val()=="0")

        {

            $("#currency").css("border","1px solid #cf3c63");

        }

        else

        {

         $("#currency").css("border","1px solid #9e9e9e"); 

        }



        if($("#currency_exchange_rate").val()=="")

        {

            $("#currency_exchange_rate").css("border","1px solid #cf3c63");

        }

        else

        {

         $("#currency_exchange_rate").css("border","1px solid #9e9e9e"); 

        }

    }

    if(have_insurance_status=="Yes")

    {

        if($("#insurance_days").val()=="")

        {

            $("#insurance_days").css("border","1px solid #cf3c63");

        }

        else

        {

         $("#insurance_days").css("border","1px solid #9e9e9e"); 

        }



        if($("#insurance_type").val()=="0")

        {

            $("#insurance_type").css("border","1px solid #cf3c63");

        }

        else

        {

         $("#insurance_type").css("border","1px solid #9e9e9e"); 

        }

    }

      if(departure_country=="0")

    {

        $("#departure_country").parent().find('.select2-container').css("border","1px solid #cf3c63");

    }

    else

    {

       $("#departure_country").parent().find('.select2-container').css("border","none"); 

    }

    // if(departure_city.trim()=="")

    // {

    //     $("#departure_city").css("border","1px solid #cf3c63");

    // }

    // else

    // {

    //    $("#departure_city").css("border","1px solid #9e9e9e"); 

    // }

    $location_country=0;

     $(".enquiry_location_country").each(function() {

     if($(this).val().trim()=="0")

     {

        $(this).parent().find('.select2-container').css("border","1px solid #cf3c63");

        $location_country++;

    }

    else

    {

         $(this).parent().find('.select2-container').css("border","none");

    }

    });

    $location_city=0;

    // $("input[name='enquiry_location_cities[]']").each(function() {

    //  if($(this).val().trim()=="")

    //  {

    //     $location_city++;

    //     $(this).parent().find('.select2-container').css("border","1px solid #cf3c63");

    // }

    // else

    // {

    //  $(this).css("border","1px solid #9e9e9e"); 

    //  }

    // });

    //  if(passenger_name.trim()=="")

    // {

    //     $("#passenger_name").css("border","1px solid #cf3c63");

    // }

    // else

    // {

    //    $("#passenger_name").css("border","1px solid #9e9e9e"); 

    // }

     if(enquiry_comments.trim()=="")

    {

        $("#enquiry_comments").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#enquiry_comments").css("border","1px solid #9e9e9e"); 

    }

    if(!regex_gst.test(gstin_no.trim()) && gstin_no.trim()!="")

    {

        $("#gstin_no").css("border","1px solid #cf3c63");

    }

    else

    {

       $("#gstin_no").css("border","1px solid #9e9e9e"); 

    }

    if(customer_name.trim()=="")

    {

        $("#customer_name").focus();

    }

    else if(customer_contact.trim()=="" && customer_email.trim()=="")

    {

        $("#customer_contact").focus();
        $("#customer_email").focus();

    }
    else if(!regex.test(customer_email.trim()) && customer_email.trim()!="")

    {

        $("#customer_email").focus();

    }

     else if(!regex.test(customer_alt_email.trim()) && customer_alt_email!="")

    {

        $("#customer_alt_email").focus();

    }

    else if(enquiry_type.trim()=="0")

    {

        $("#enquiry_type").focus();

    }
    else  if(customer_country.trim()=="0")
    {
            $("#customer_country").parent().find('.select2-container').focus();

    }

    else if(booking_range.trim()=="")

    {

        $("#booking_range").focus();

    }

    else if(no_of_adults.trim()=="")

    {

        $("#no_of_adults").focus();

    }

    else if(no_of_kids.trim()=="")

    {

        $("#no_of_kids").focus();

    }
    else if(child_age_error>0)
    {

    }

     else if(enquiry_prospect.trim()=="0")

    {

        $("#enquiry_prospect").focus();

    }

     else if(enquiry_status.trim()=="0")

    {

        $("#enquiry_status").focus();

    }

     else if(nxt_followup_date.trim()=="")

    {

        $("#nxt_followup_date").focus();

    }

     else if(currency_exchange_rate_status=="Yes" && $("#currency").val()=="0")

     {

        $("#currency").focus();

    }

    else if(currency_exchange_rate_status=="Yes" && $("#currency_exchange_rate").val()=="")

    {

     $("#currency_exchange_rate").focus();

     }

     else if(have_insurance_status=="Yes" && $("#insurance_days").val()=="")

     {

        $("#insurance_days").focus();

     }

     else if(have_insurance_status=="Yes" && $("#insurance_type").val()=="0")

     {

        $("#insurance_type").focus();

     }

     else if(departure_country=="0")

     {

        $("#departure_country").parent().find('.select2-container').focus();

    }

    // else if(departure_city=="")

    // {

    //      $("#departure_city").focus();

    // }

    else if($location_country>0)

    {

          $(".enquiry_location_country").each(function() {

         if($(this).val().trim()=="0")

         {

            $(this).focus();

        }

        });

   }

   else if($location_city>0)

   {

        // $("input[name='enquiry_location_cities[]']").each(function() {

        //   if($(this).val().trim()=="")

        //   {

        //      $(this).focus();

        //  }

        //  });

    }

    //   else if(passenger_name=="")

    // {

    //      $("#passenger_name").focus();

    // }

     else if(!regex_gst.test(gstin_no.trim()) && gstin_no!="")

    {

        $("#gstin_no").focus();

    }

       else if(enquiry_comments=="")

    {

         $("#enquiry_comments").focus();

    }

    else

    {

       var formdata=new FormData($("#enquiry_form")[0]);

       $("#save_enquiry").prop("disabled",true);

    $.ajax({

        url:"{{route('insert-enquiry')}}",

        enctype: 'multipart/form-data',

        data: formdata,

        type:"POST",

        processData: false,

        contentType: false,

        success:function(response)

        {

             if(response.indexOf("exist")!=-1)

            {

                swal("Already Exist!", "Customer with this email already exists");

            }

            else if(response.indexOf("success")!=-1)

            {

                swal({title:"Success",text:"Enquiry Created Successfully !",type: "success"},

                 function(){ 

                     location.reload();

                 });

            }

            else if(response.indexOf("fail")!=-1)

            {

                swal("ERROR", "Enquiry cannot be inserted right now! ");

            }

            $("#save_enquiry").prop("disabled",false);

        }

    });  

    }

   



});

</script>

<script>
    $(document).on('change','#customer_contact',function(){
        var customer_contact=$(this).val();
        $.ajax({
                url : "{{route('search_mobile_enquiry')}}",
                data:{'customer_contact':customer_contact,},
                type:"GET",
                success:function(data)
                {

                    if(data=="")
                    {
                        
                        // $('#customer_name').val('');
                        $('#customer_email').val('');
                        $('#no_of_adults').val('');
                         $('#passport_no').val("");
                     $('#passport_validity').val("");
                        $('#no_of_kids').val(0);
                        $('#showenqdata').hide();
                         $("#child_age_div").hide();
                        $('#showenqdata').html("");
                    }
                    else
                    {
                       
                        $('#showenqdata').show();
                        $('#showenqdata').html(data);
                    }
                }
        })
        
    })
    $(document).on('click','.enq_chk',function(){
        var id= this.id;
        $.ajax({
                url:"{{route('enq_data_search')}}",
                data:{'id':id},
                type:'GET',
                dataType:'Json',
                success:function(data)
                {
                    
                    $('#customer_name').val(data.customer_name);
                    $('#customer_email').val(data.customer_email);
                    $('#passport_no').val(data.passport_no);
                     $('#passport_validity').val(data.passport_validity);
                    $('#no_of_adults').val(data.no_of_adults);
                    $('#no_of_kids').val(data.no_of_kids);
                    var kids_age=data.kids_age;
                    var html_data="";
                    var kids_count=kids_age.length;
                    for($i=1;$i<=kids_count;$i++)
                    {
                        var kids_age=kids_age[($i-1)];
                        if(kids_age==null)
                        {
                          kids_age="";  
                        }
                        html_data+='<div class="col-md-2"> <div class="form-group"><label for="child_age'+$i+'">Child Age '+$i+' </label><input type="text" id="child_age'+$i+'" name="child_age[]" class="form-control child_age" value="'+kids_age+'"></div></div>'
                    }
                    $("#child_age_div").html(html_data);
                    $("#child_age_div").show();

                    $('#showenqdata').hide();
                    $('#showenqdata').html("");
                }
        })
    })
</script>

</body>



</html>



