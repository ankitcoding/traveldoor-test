@include('mains.includes.top-header')
<style>
    header.main-header {
          background: url("{{ asset('assets/images/color-plate/theme-purple.jpg') }}");
    }

    .iti-flag {
        width: 20px;
        height: 15px;
        box-shadow: 0px 0px 1px 0px #888;
        background-image: url("flags.png") !important;
        background-repeat: no-repeat;
        background-color: #DBDBDB;
        background-position: 20px 0
    }

    div#cke_1_contents {
        height: 250px !important;
    }

    table#calendar-demo {
        width: 100%;
        height: 275px !important;
        min-height: 275px !important;
        overflow: hidden;
    }

    .calendar-wrapper.load {
        width: 100%;
        height: 276px;
    }

    .calendar-date-holder .calendar-dates .date.month a {
        display: block;
        padding: 17px 0 !important;
    }

    .calendar-date-holder {
        width: 100% !important;
    }

    section.calendar-head-card {
        display: none;
    }

    .calendar-container {
        border: 1px solid #cccccc;
        height: 276px !important;
    }

    img.plus-icon {
        margin: 0 2px;
        display: inline !important;
    }

    @media screen and (max-width:400px) {
        .calendar-date-holder .calendar-dates .date a {
            text-decoration: none;
            display: block;
            color: inherit;
            padding: 3px !important;
            margin: 1px;
            outline: none;
            border: 2px solid transparent;
            transition: all .3s;
            -o-transition: all .3s;
            -moz-transition: all .3s;
            -webkit-transition: all .3s;
        }
    }
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('mains.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('mains.includes.nav')

                <div class="content">

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Service Management</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                            <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                            <li class="breadcrumb-item" aria-current="page">Service Management</li>
                            <li class="breadcrumb-item active" aria-current="page">Edit Activity
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
           <!--  <div class="right-title">
                <div class="dropdown">
                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
                            class="mdi mdi-dots-horizontal"></i></button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
                        <div class="dropdown-divider"></div>
                        <button type="button" class="btn btn-rounded btn-success">Submit</button>
                    </div>
                </div>
            </div> -->
        </div>
    </div>

 @if($rights['edit_delete']==1)
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Edit Activity</h4>
                </div>
                <div class="box-body">
                    <form id="activity_form" encytpe="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row mb-10">
                        <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                        <label for="activity_type">ACTIVITY TYPE <span class="asterisk">*</span></label>
                         <select id="activity_type" name="activity_type" class="form-control">
                            <option value="0">--SELECT ACTIVITY TYPE--</option>
                            @foreach($fetch_activity_type as $activity_type)
                            @if($get_activity->activity_type==$activity_type->activity_type_id)
                            <option value="{{$activity_type->activity_type_id}}" selected="selected">{{$activity_type->activity_type_name}}</option>
                            @else
                            <option value="{{$activity_type->activity_type_id}}">{{$activity_type->activity_type_name}}</option>

                            @endif

                            @endforeach
                        </select>
                        </div>
                        </div>
                        </div> 
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="activity_name">ACTIVITY NAME <span class="asterisk">*</span></label>
                                <input type="text" id="activity_name" name="activity_name" class="form-control" placeholder="ACTIVITY NAME " value="{{$get_activity->activity_name}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>
                    </div>
                    <div class="row mb-10">

                        <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="supplier_name">SUPPLIER <span class="asterisk">*</span></label>
                                        <select id="supplier_name" name="supplier_name" class="form-control select2" style="width: 100%;">
                                            <option value="0" hidden>Select Supplier</option>
                                           @foreach($suppliers as $supplier)
                                           @if($get_activity->supplier_id==$supplier->supplier_id)
                                           <option value="{{$supplier->supplier_id}}" selected="selected">{{$supplier->supplier_name}}</option>
                                           @else
                                           <option value="{{$supplier->supplier_id}}">{{$supplier->supplier_name}}</option>
                                           @endif  
                                           @endforeach
                                        </select>
                                    </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>

                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="activity_location">ACTIVITY LOCATION <span class="asterisk">*</span></label>
                                <input type="text" class="form-control" placeholder="ACTIVITY LOCATION" id="activity_location" name="activity_location" value="{{$get_activity->activity_location}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>
                    </div>
                    <div class="row mb-10">

                        <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="activity_country">COUNTRY <span class="asterisk">*</span></label>
                                        <select id="activity_country" name="activity_country" class="form-control select2" style="width: 100%;">
                                            <option value="0" hidden>SELECT COUNTRY</option>
                                             @foreach($countries as $country)
                                             @if(in_array($country->country_id,$countries_data))
                                             @if($country->country_id==$get_activity->activity_country)
                                             <option value="{{$country->country_id}}" selected="selected">{{$country->country_name}}</option>
                                             @else
                                             <option value="{{$country->country_id}}" >{{$country->country_name}}</option>
                                             @endif  
                                             @endif
                                           @endforeach
                                        </select>
                                    </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>

                    </div>
                     <div class="row mb-10" id="acitvity_city_div">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                        <label for="activity_city">CITY <span class="asterisk">*</span></label>
                                        <select id="activity_city" name="activity_city" class="form-control select2" style="width: 100%;">
                                             <option value="0" hidden>SELECT CITY</option>
                                            @foreach($cities as $city)
                                             @if($city->id==$get_activity->activity_city)
                                             <option value="{{$city->id}}" selected="selected">{{$city->name}}</option>
                                             @else
                                             <option value="{{$city->id}}" >{{$city->name}}</option>
                                             @endif  
                                           @endforeach
                                        </select>
                            </div>
                        </div>
                    </div>

                     <div class="row mb-10">

                        <div class="col-sm-12 col-md-12 col-lg-6">
                                  <div class="form-group">
                                    <label for="activity_duration">ACTIVITY DURATION <span class="asterisk">*</span></label>
                                    <input type="text" class="form-control" placeholder="ACTIVITY DURATION (in hours)" id="activity_duration" name="activity_duration" maxlength="200" value="{{$get_activity->activity_duration}}">
                                </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>

                    </div>

                    <div class="row mb-10" style="display: none;">

                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label for="period_operation_from">PERIOD OF OPERATION <span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="text" placeholder="FROM"
                                                    class="form-control pull-right datepicker" id="period_operation_from" name="period_operation_from" value="{{$get_activity->operation_period_fromdate}}" readonly="readonly">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <div class="input-group date">
                                                <input type="text" placeholder="TO"
                                                    class="form-control pull-right datepicker" id="period_operation_to" name="period_operation_to" value="{{$get_activity->operation_period_todate}}" readonly="readonly">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">

                        </div>




                    </div>
                    <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>VALIDITY DATE <span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="text" placeholder="FROM"
                                                    class="form-control pull-right datepicker" id="validity_operation_from" name="validity_operation_from" value="{{$get_activity->validity_fromdate}}" readonly="readonly">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <div class="input-group date">
                                                <input type="text" placeholder="TO"
                                                    class="form-control pull-right datepicker" id="validity_operation_to" name="validity_operation_to" value="{{$get_activity->validity_todate}}" readonly="readonly">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-6">

                        </div>




                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                                <label for="activity_time_from">FROM TIME <span class="asterisk">*</span></label>
                                
                                    <div class="form-group">
                                        <div class="bootstrap-timepicker">

                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker1" id="activity_time_from" name="activity_time_from" value="{{$get_activity->validity_fromtime}}" readonly="readonly">

                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                                <label for="activity_time_to">TO TIME  <span class="asterisk">*</span></label>
                                <div class="form-group">
                                <div class="bootstrap-timepicker">
                                    
                                        <div class="input-group">
                                            <input type="text" class="form-control timepicker1" id="activity_time_to" name="activity_time_to" value="{{$get_activity->validity_totime}}" readonly="readonly">

                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                        </div>
                                        <!-- /.input group -->
                                    </div>

                                </div>
                        </div>
                    </div>
                      @php
                        $weekdays=unserialize($get_activity->operating_weekdays);
    
                        @endphp
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="row mb-10">


                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>IS ALL DAYS <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">

                                        <input type="radio" id="radio_10" class="with-gap radio-col-primary week_all_days"  name="is_all_days" value="Yes" @if($weekdays["monday"]=="Yes" && $weekdays["tuesday"]=="Yes" && $weekdays["wednesday"]=="Yes" && $weekdays["thursday"]=="Yes" && $weekdays["friday"]=="Yes" && $weekdays["saturday"]=="Yes" && $weekdays["sunday"]=="Yes")checked @endif>
                                            <label for="radio_10">Yes </label>
                                        <input type="radio" id="radio_11" class="with-gap radio-col-primary week_all_days" name="is_all_days" value="No" @if($weekdays["monday"]=="No" || $weekdays["tuesday"]=="No" || $weekdays["wednesday"]=="No" || $weekdays["thursday"]=="No" || $weekdays["friday"]=="No" ||$weekdays["saturday"]=="No" || $weekdays["sunday"]=="No") checked @endif>
                                            <label for="radio_11">No</label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>MONDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                <input  type="radio" id="radio_20"
                             class="with-gap radio-col-primary weekdays_yes " name="week_monday" value="Yes" @if($weekdays["monday"]=="Yes")checked @endif>
                                            <label for="radio_20">Yes </label>
                                    <input type="radio" id="radio_21"
                                      class="with-gap radio-col-primary weekdays_no " name="week_monday" value="No" @if($weekdays["monday"]=="No")checked @endif>
                                            <label for="radio_21">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>TUESDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_30"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_tuesday" value="Yes" @if($weekdays["tuesday"]=="Yes")checked @endif>
                                            <label for="radio_30">Yes </label>
                                            <input type="radio" id="radio_31"
                                                class="with-gap radio-col-primary weekdays_no" name="week_tuesday" value="No" @if($weekdays["tuesday"]=="No")checked @endif>
                                            <label for="radio_31">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>WEDNESDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_40"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_wednesday" value="Yes" @if($weekdays["wednesday"]=="Yes")checked @endif>
                                            <label for="radio_40">Yes </label>
                                            <input type="radio" id="radio_41"
                                                class="with-gap radio-col-primary weekdays_no" name="week_wednesday" value="No" @if($weekdays["wednesday"]=="No")checked @endif>
                                            <label for="radio_41">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>THURSDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_50"
                                                class="with-gap radio-col-primary weekdays_yes"  name="week_thursday" value="Yes" @if($weekdays["thursday"]=="Yes")checked @endif>
                                            <label for="radio_50">Yes </label>
                                            <input type="radio" id="radio_51"
                                                class="with-gap radio-col-primary weekdays_no"  name="week_thursday" value="No" @if($weekdays["thursday"]=="No")checked @endif>
                                            <label for="radio_51">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>FRIDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_60"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_friday" value="Yes" @if($weekdays["friday"]=="Yes")checked @endif>
                                            <label for="radio_60">Yes </label>
                                            <input type="radio" id="radio_61"
                                                class="with-gap radio-col-primary weekdays_no" name="week_friday" value="No" @if($weekdays["friday"]=="No")checked @endif>
                                            <label for="radio_61">No</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>SATURDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="radio" id="radio_70"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_saturday" value="Yes" @if($weekdays["saturday"]=="Yes")checked @endif>
                                            <label for="radio_70">Yes </label>
                                            <input type="radio" id="radio_71"
                                                class="with-gap radio-col-primary weekdays_no" name="week_saturday" value="No" @if($weekdays["saturday"]=="No")checked @endif>
                                            <label for="radio_71">No</label>
                                        </div>
                                    </div>
                                <div class="row">
                                        <div class="col-md-6">
                                            <label>SUNDAY <span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">

                                            <input type="radio" id="radio_80"
                                                class="with-gap radio-col-primary weekdays_yes" name="week_sunday" value="Yes" @if($weekdays["sunday"]=="Yes")checked @endif>
                                            <label for="radio_80">Yes </label>
                                            <input type="radio" id="radio_81"
                                                class="with-gap radio-col-primary weekdays_no" name="week_sunday" value="No" @if($weekdays["sunday"]=="No")checked @endif>
                                            <label for="radio_81">No</label>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>




                    </div>

                    <div class="row mb-10">

                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                        <label for="activity_currency">CURRENCY <span class="asterisk">*</span></label>
                                        <select class="form-control select2" style="width: 100%;" id="activity_currency" name="activity_currency">
                                             <option value="0" hidden>SELECT CURRENCY</option>
                                             @foreach($currency as $curr)
                                             @if($get_activity->activity_currency==$curr->code)
                                             <option value="{{$curr->code}}" selected="selected">{{$curr->code}} ({{$curr->name}})</option>
                                             @else
                                             <option value="{{$curr->code}}">{{$curr->code}} ({{$curr->name}})</option>

                                             @endif
                                             @endforeach
                                        </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>

                    </div>
                    <div class="row mb-10" style="display: none">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="activity_adult_cost">ADULT PRICE <span class="asterisk">*</span></label>
                                <input type="text" class="form-control" placeholder="ADULT PRICE " id="activity_adult_cost" name="activity_adult_cost"  onkeypress="javascript:return validateNumber(event)" value="{{$get_activity->adult_price}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>
                    </div>


                    <div class="row mb-10" style="display: none">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label value="activity_child_cost">CHILD PRICE <span class="asterisk">*</span></label>
                                <input type="text" class="form-control" placeholder="CHILD PRICE" id="activity_child_cost" name="activity_child_cost" onkeypress="javascript:return validateNumber(event)" value="{{$get_activity->child_price}}">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>
                    </div>
                     <div class="row mb-10" style="display: none">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>FOR ALL AGES<span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">

                                            <input type="radio" id="radio_for_all_ages1"
                                            class="with-gap radio-col-primary for_all_ages radio_allowed"  name="for_all_ages" value="Yes" @if($get_activity->for_all_ages=="Yes") checked @endif>
                                            <label for="radio_for_all_ages1">Yes </label>
                                            <input type="radio" id="radio_for_all_ages2" 
                                            class="with-gap radio-col-primary for_all_ages radio_allowed" name="for_all_ages" value="No" @if($get_activity->for_all_ages=="No") checked @endif>
                                            <label for="radio_for_all_ages2">No</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @php
                        $child_adult_details=unserialize($get_activity->child_adult_age_details);
                    @endphp
                    <div class="row mb-10" id="for_all_ages_div" style="display:none">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>CHILD ALLOWED<span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">

                                            <input type="radio" id="radio_child_allowed1"
                                            class="with-gap radio-col-primary child_allowed radio_allowed"  name="child_allowed" value="Yes" @if($get_activity->for_all_ages=="No" && $child_adult_details[0][1]=="Yes") checked @endif>
                                            <label for="radio_child_allowed1">Yes </label>
                                            <input type="radio" id="radio_child_allowed2"
                                            class="with-gap radio-col-primary child_allowed radio_allowed" name="child_allowed" value="No" @if($get_activity->for_all_ages=="No" && $child_adult_details[0][1]=="No") checked @endif>
                                            <label for="radio_child_allowed2">No</label>

                                            <div class="row mb-10" id="child_allowed_div" @if($get_activity->for_all_ages=="No" && $child_adult_details[0][1]=="No") style="display:none" @endif >
                                                <div class="col-md-12">
                                                 <label for="child_age">CHILD AGE <span class="asterisk">*</span></label>
                                                 <input type="text" class="form-control" placeholder="CHILD AGE " id="child_age" name="child_age" @if($get_activity->for_all_ages=="No" && $child_adult_details[0][2]!="") value="{{$child_adult_details[0][2]}}" @endif>
                                             </div>
                                         </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-10" >
                                <div class="col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label>ADULT ALLOWED<span class="asterisk">*</span></label>
                                        </div>
                                        <div class="col-md-6">

                                            <input type="radio" id="radio_adult_allowed1"
                                            class="with-gap radio-col-primary adult_allowed radio_allowed"  name="adult_allowed" value="Yes" @if($get_activity->for_all_ages=="No" && $child_adult_details[1][1]=="Yes") checked @endif>
                                            <label for="radio_adult_allowed1">Yes </label>
                                            <input type="radio" id="radio_adult_allowed2"
                                            class="with-gap radio-col-primary adult_allowed radio_allowed" name="adult_allowed" value="No" @if($get_activity->for_all_ages=="No" && $child_adult_details[1][1]=="No") checked @endif>
                                            <label for="radio_adult_allowed2">No</label>

                                            <div class="row mb-10" id="adult_allowed_div" @if($get_activity->for_all_ages=="No" && $child_adult_details[1][1]=="No") style="display:none" @endif>
                                                <div class="col-md-12">
                                                    <label for="adult_age">ADULT AGE <span class="asterisk">*</span></label>
                                                 <input type="text" class="form-control" placeholder="ADULT AGE " id="adult_age" name="adult_age"  @if($get_activity->for_all_ages=="No" && $child_adult_details[1][2]!="") value="{{$child_adult_details[1][2]}}" @endif>
                                             </div>
                                         </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label>BLACKOUT DAYS</label>
                                <div class="col-sm-12 col-md-12" style="padding:0">
                                    <button type="button" class="btn btn-rounded btn-primary mr-10"
                                        data-toggle="collapse" data-target="#demo2">Add
                                       Blackout Days</button>

                                    <div id="demo2" class="collapse">
                                        <div class="row mt-15 mb-10">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">

                                                    <div class="input-group date">
                                                        <input type="text" placeholder="BLACKOUT DATES" class="form-control pull-right datepicker" id="blackout_days" name="blackout_days" value="{{$get_activity->activity_blackout_dates}}">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-calendar"></i>
                                                        </div>
                                                    </div>
                                                    <!-- /.input group -->

                                                </div>
                                            </div>
                                            
                                            
                                        </div>

                                        <!-- <div class="col-sm-12 col-md-12">
                                            <img class="plus-icon" style="display: block;" src="{{ asset('assets/images/add_icon.png') }}">
                                        </div> -->
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">

                        </div>
                    </div>
                    <div class="row mb-10" style="display:none">
                        @php
                        $nationality_markup=unserialize($get_activity->nationality_markup_details);

                        for($markup=0;$markup< count($nationality_markup);$markup++)
                        {
                            @endphp
                            <div class="col-sm-12 col-md-12 col-lg-6 markup_div" id="markup_div{{($markup+1)}}">
                                <label>NATIONALITY & ACTIVITY MARKUP <span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="form-group">

                                            <select class="form-control select2" style="width: 100%;" id="activity_nationality{{($markup+1)}}" name="activity_nationality[]">
                                                <option selected="selected" value="0" hidden>SELECT NATIONALITY</option>
                                                @foreach($countries as $country)
                                                 @if($nationality_markup[$markup]['activity_nationality']==$country->country_id)
                                           <option value="{{$country->country_id}}" selected="selected">{{$country->country_name}}</option>
                                             @else
                                            <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                             @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" id="activity_markup{{($markup+1)}}" name="activity_markup[]">
                                                <option value="0">SELECT MARKUP TYPE</option>
                                                <option  @if($nationality_markup[$markup]['activity_markup']=="Markup Percentage") selected @endif>Markup Percentage</option>
                                                <option @if($nationality_markup[$markup]['activity_markup']=="Markup Amount") selected @endif>Markup Amount</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Markup Amount" id="activity_amount{{($markup+1)}}" name="activity_amount[]" onkeypress="javascript:return validateNumber(event)" value="{{$nationality_markup[$markup]['activity_amount']}}">
                                        </div>
                                    </div>

                                </div>
                                <div class="col-sm-12 col-md-12 add_more_markup_div">
                                   @if(count($nationality_markup)==1)
                                   <img id="add_more_markup{{($markup+1)}}" class="plus-icon add_more_markup"  src="{{ asset('assets/images/add_icon.png') }}" style="margin-left: auto;">
                                   @endif

                                   @if($markup>0)
                                   @if($markup==count($nationality_markup)-1)
                                   <img id="remove_more_markup{{($markup+1)}}" class="minus-icon remove_more_markup"  src="{{ asset('assets/images/minus_icon.png') }}" style="margin-left: auto;">
                                   <img id="add_more_markup{{($markup+1)}}" class="plus-icon add_more_markup"  src="{{ asset('assets/images/add_icon.png') }}" style="margin-left: auto;">
                                   @else
                                   <img id="remove_more_markup{{($markup+1)}}" class="minus-icon remove_more_markup"  src="{{ asset('assets/images/minus_icon.png') }}" style="margin-left: auto;">
                                   @endif
                                   @endif
                                </div>
                            </div>
                            @php
                        }
                        @endphp
                    </div>


                    <div class="row mb-10" style="display:none">
                         @php
                        $activity_transport=unserialize($get_activity->activity_transport_pricing);

                        for($transport=0;$transport< count($activity_transport);$transport++)
                        {
                            @endphp
                        <div class="col-sm-12 col-md-12 col-lg-6 transport_div" id="transport_div{{($transport+1)}}">
                            <label for="activity_transport_currency1">
                                ACTIVITY TRANSPORT PRICING <span class="asterisk">*</span></label>
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="form-group">

                                        <select class="form-control select2" style="width: 100%;" id="activity_transport_currency{{($transport+1)}}" name="activity_transport_currency[]">
                                            <option selected="selected" value="0" hidden>SELECT CURRENCY</option>
                                            @foreach($currency as $curr)
                                             @if($activity_transport[$transport]['transport_currency']==$curr->code)
                                             <option value="{{$curr->code}}" selected="selected">{{$curr->code}} ({{$curr->name}})</option>
                                             @else
                                             <option value="{{$curr->code}}">{{$curr->code}} ({{$curr->name}})</option>

                                             @endif
                                            @endforeach
                                        </select>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                            <textarea rows="5" cols="5" class="form-control"
                                                placeholder="DESCRIPTION" id="activity_transport_desc{{($transport+1)}}" name="activity_transport_desc[]">{{$activity_transport[$transport]['transport_desc']}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <input type="text" class="form-control" placeholder="COST " id="activity_transport_cost{{($transport+1)}}" name="activity_transport_cost[]" onkeypress="javascript:return validateNumber(event)" value="{{$activity_transport[$transport]['transport_cost']}}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-12 add_more_transport_div">

                               @if(count($activity_transport)==1)
                               <img id="add_more_transport{{($transport+1)}}" class="plus-icon add_more_transport"  src="{{ asset('assets/images/add_icon.png') }}" style="margin-left: auto;">
                               @endif

                               @if($transport>0)
                               @if($transport==count($activity_transport)-1)
                               <img id="remove_more_transport{{($transport+1)}}" class="minus-icon remove_more_transport"  src="{{ asset('assets/images/minus_icon.png') }}" style="margin-left: auto;">
                               <img id="add_more_transport{{($transport+1)}}" class="plus-icon add_more_transport" src="{{ asset('assets/images/add_icon.png') }}" style="margin-left: auto;">
                               @else
                               <img id="remove_more_transport{{($transport+1)}}" class="minus-icon remove_more_transport" src="{{ asset('assets/images/minus_icon.png') }}" style="margin-left: auto;">
                               @endif
                               @endif
                            </div>
                        </div>
                         @php
                        }
                        @endphp
                    </div>

                    @php
                    $adult_allowed="";
                    $adult_min_age="";
                    $adult_max_age=""; 
                     
                    $child_allowed="";       
                    $child_min_age="";
                    $child_max_age="";

                    $infant_allowed="";  
                    $infant_min_age="";
                    $infant_max_age="";     

                    if($get_activity->age_group_details!="" && $get_activity->age_group_details!=null)
                    {
                        $age_group_details=unserialize($get_activity->age_group_details);
                                
                        if($age_group_details['adults']['allowed']=="yes")
                        {
                            $adult_allowed="yes";
                        }

                        $adult_min_age=$age_group_details['adults']['min_age'];
                        $adult_max_age=$age_group_details['adults']['max_age'];

                        if($age_group_details['child']['allowed']=="yes")
                        {
                            $child_allowed="yes";
                        }

                        $child_min_age=$age_group_details['child']['min_age'];
                        $child_max_age=$age_group_details['child']['max_age'];

                        if($age_group_details['infant']['allowed']=="yes")
                        {
                            $infant_allowed="yes";
                        }

                        $infant_min_age=$age_group_details['infant']['min_age'];
                        $infant_max_age=$age_group_details['infant']['max_age'];

                    }
                    @endphp

                     <div class="row mb-10">

                        <div class="col-lg-6">
                            <h4 class="box-title" style="border-color: #c1c1c1;">
                              AGE GROUPS<span class="asterisk">*</span></h4>

                              <div class="row">   
                                <div class="col-md-3">
                                </div>
                                <div class="col-md-2">
                                    <label>Min Age</label>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-2">
                                    <label>Max Age</label>
                                </div>

                            </div>
                            <div class="row" id="allowed_pax_div">
                                <div class="col-md-3">
                                    <input type="checkbox" id="adults_age" name="adults_age" value="Yes"@if($adult_allowed=='yes')
                                    checked @endif>
                                    <label for="adults_age">Adults</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" id="adults_min_age" name="adults_min_age" class="form-control" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" maxlength="3" value="{{$adult_min_age}}">
                                </div>
                                <div class="col-md-1">-</div>
                                <div class="col-md-2">
                                    <input type="text" id="adults_max_age" name="adults_max_age" class="form-control" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" maxlength="3" value="{{$adult_max_age}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="checkbox" id="children_age" name="children_age" value="Yes" @if($child_allowed=='yes')
                                    checked @endif>
                                    <label for="children_age">Child</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" id="child_min_age" name="child_min_age" class="form-control" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" maxlength="3" value="{{$child_min_age}}">
                                </div>
                                <div class="col-md-1">-</div>
                                <div class="col-md-2">
                                    <input type="text" id="child_max_age" name="child_max_age" class="form-control" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" maxlength="3" value="{{$child_max_age}}">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <input type="checkbox" id="infant_age" name="infant_age" value="Yes" @if($infant_allowed=='yes')
                                    checked @endif>
                                    <label for="infant_age">Infant</label>
                                </div>
                                <div class="col-md-2">
                                    <input type="text" id="infant_min_age" name="infant_min_age" class="form-control" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" maxlength="3" value="{{$infant_min_age}}">
                                </div>
                                <div class="col-md-1">-</div>
                                <div class="col-md-2">
                                    <input type="text" id="infant_max_age" name="infant_max_age" class="form-control" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" maxlength="3" value="{{$infant_max_age}}">
                                </div>
                            </div>

                        </div>
                    </div>

                      <div class="row mb-10">

                        <div class="col-lg-12">
                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                              ACTIVITY PRICING <span class="asterisk">*</span></h4>

                              <div class="row">
                                  <div class="col-md-1"></div>
                                   <div class="col-md-3 text-center">
                                     <label for="">Min - Max per booking</label>
                                 </div>
                                 <div class="col-md-2">
                                     <label for="">Price</label>
                                 </div>
                              </div>
                              @if($get_activity->adult_price_details!="" && $get_activity->adult_price_details!=null)
                              <div class="row">
                                  <div class="col-md-1">
                                    <label for="">Adults</label>
                                 </div>
                                <div class="col-md-6">
                                    @php
                                    $adult_price_details=unserialize($get_activity->adult_price_details);
                                    @endphp
                                    @for($adult_price=0;$adult_price< count($adult_price_details);$adult_price++)
                                    <div class="adultpricing_div" id="adultpricing_div__{{($adult_price+1)}}">
                                    <div class="row">
                                        <div class="col-md-3">
                                                 <input type="text" id="adult_min_pax__{{($adult_price+1)}}" name="adult_min_pax[]" class="form-control adult_min_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Min" value="{{$adult_price_details[$adult_price]['adult_min_pax']}}">
                                        </div>
                                         <div class="col-md-3">
                                                 <input type="text" id="adult_max_pax__{{($adult_price+1)}}" name="adult_max_pax[]" class="form-control adult_max_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Max" value="{{$adult_price_details[$adult_price]['adult_max_pax']}}">

                                        </div>
                                        <div class="col-md-3">
                                           <input type="text" id="adult_pax_price__{{($adult_price+1)}}" name="adult_pax_price[]" class="form-control adult_pax_price" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Price" value="{{$adult_price_details[$adult_price]['adult_pax_price']}}">
                                        </div>
                                        <div class="col-md-2 add_more_adultpricing_div">
                                            @if(count($adult_price_details)==1)
                                                <img id="add_more_adultpricing{{($adult_price+1)}}" class="plus-icon add_more_adultpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                                @else

                                                @if(count($adult_price_details)-1==$adult_price)

                                                <img id="remove_more_adultpricing{{($adult_price+1)}}" class="plus-icon remove_more_adultpricing" style="margin-left: auto;" src="{{ asset('assets/images/minus_icon.png') }}">

                                                <img id="add_more_adultpricing{{($adult_price+1)}}" class="plus-icon add_more_adultpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                                @endif
                                            @endif


                                        </div>
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>
                        @else
                         <div class="row">
                                  <div class="col-md-1">
                                    <label for="">Adults</label>
                                 </div>
                                <div class="col-md-6">
                                    <div class="adultpricing_div" id="adultpricing_div__1">
                                    <div class="row">
                                        <div class="col-md-3">
                                                 <input type="text" id="adult_min_pax__1" name="adult_min_pax[]" class="form-control adult_min_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Min">
                                        </div>
                                         <div class="col-md-3">
                                                 <input type="text" id="adult_max_pax__1" name="adult_max_pax[]" class="form-control adult_max_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Max">

                                        </div>
                                        <div class="col-md-3">
                                           <input type="text" id="adult_pax_price__1" name="adult_pax_price[]" class="form-control adult_pax_price" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Price">
                                        </div>
                                        <div class="col-md-2 add_more_adultpricing_div">

                                            <img id="add_more_adultpricing1" class="plus-icon add_more_adultpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endif


                        <div class="row" style="margin-top:20px">
                                  <div class="col-md-1"></div>
                                   <div class="col-md-3 text-center">
                                     <label for="">Min - Max per booking</label>
                                 </div>
                                 <div class="col-md-2">
                                     <label for="">Price</label>
                                 </div>
                              </div>

                                @if($get_activity->infant_price_details!="" && $get_activity->infant_price_details!=null)
                              <div class="row">
                                  <div class="col-md-1">
                                    <label for="">Infant</label>
                                 </div>
                                <div class="col-md-6">
                                     @php
                                    $infant_price_details=unserialize($get_activity->infant_price_details);
                                    @endphp
                                    @for($infant_price=0;$infant_price< count($infant_price_details);$infant_price++)
                                    <div class="infantpricing_div" id="infantpricing_div__{{($infant_price+1)}}">
                                    <div class="row">
                                        <div class="col-md-3">
                                                 <input type="text" id="infant_min_pax__{{($infant_price+1)}}" name="infant_min_pax[]" class="form-control infant_min_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Min" value="{{$infant_price_details[$infant_price]['infant_min_pax']}}">
                                        </div>
                                         <div class="col-md-3">
                                                 <input type="text" id="infant_max_pax__{{($infant_price+1)}}" name="infant_max_pax[]" class="form-control infant_max_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Max" value="{{$infant_price_details[$infant_price]['infant_max_pax']}}">

                                        </div>
                                        <div class="col-md-3">
                                           <input type="text" id="infant_pax_price__{{($infant_price+1)}}" name="infant_pax_price[]" class="form-control infant_pax_price" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Price" value="{{$infant_price_details[$infant_price]['infant_pax_price']}}">
                                        </div>
                                        <div class="col-md-2 add_more_infantpricing_div">

                                            @if(count($infant_price_details)==1)
                                           <img id="add_more_infantpricing{{($infant_price+1)}}" class="plus-icon add_more_infantpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">
                                            @else

                                             @if(count($infant_price_details)-1==$infant_price)

                                            <img id="remove_more_infantpricing{{($infant_price+1)}}" class="plus-icon remove_more_infantpricing" style="margin-left: auto;" src="{{ asset('assets/images/minus_icon.png') }}">

                                             <img id="add_more_infantpricing{{($infant_price+1)}}" class="plus-icon add_more_infantpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                             @endif

                                              @endif

                                            
                                        </div>
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>
                        @else
                         <div class="row">
                                  <div class="col-md-1">
                                    <label for="">Infant</label>
                                 </div>
                                <div class="col-md-6">
                                    <div class="infantpricing_div" id="infantpricing_div__1">
                                    <div class="row">
                                        <div class="col-md-3">
                                                 <input type="text" id="infant_min_pax__1" name="infant_min_pax[]" class="form-control infant_min_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Min">
                                        </div>
                                         <div class="col-md-3">
                                                 <input type="text" id="infant_max_pax__1" name="infant_max_pax[]" class="form-control infant_max_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Max">

                                        </div>
                                        <div class="col-md-3">
                                           <input type="text" id="infant_pax_price__1" name="infant_pax_price[]" class="form-control infant_pax_price" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Price">
                                        </div>
                                        <div class="col-md-2 add_more_infantpricing_div">
                                            <img id="add_more_infantpricing1" class="plus-icon add_more_infantpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif



                         <div class="row" style="margin-top:20px">
                                  <div class="col-md-1"></div>
                                   <div class="col-md-3 text-center">
                                     <label for="">Min - Max per booking</label>
                                 </div>
                                 <div class="col-md-2">
                                     <label for="">Price</label>
                                 </div>
                              </div>
                               @if($get_activity->child_price_details!="" && $get_activity->child_price_details!=null)
                              <div class="row">
                                  <div class="col-md-1">
                                    <label for="">Child</label>
                                 </div>
                                <div class="col-md-6">
                                      @php
                                    $child_price_details=unserialize($get_activity->child_price_details);
                                    @endphp
                                    @for($child_price=0;$child_price< count($child_price_details);$child_price++)
                                    <div class="childpricing_div" id="childpricing_div__{{($child_price+1)}}">
                                    <div class="row">
                                        <div class="col-md-3">
                                                 <input type="text" id="child_min_pax__{{($child_price+1)}}" name="child_min_pax[]" class="form-control child_min_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Min" value="{{$child_price_details[$child_price]['child_min_pax']}}">
                                        </div>
                                         <div class="col-md-3">
                                                 <input type="text" id="child_max_pax__{{($child_price+1)}}" name="child_max_pax[]" class="form-control child_max_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Max" value="{{$child_price_details[$child_price]['child_max_pax']}}">

                                        </div>
                                        <div class="col-md-3">
                                           <input type="text" id="child_pax_price__{{($child_price+1)}}" name="child_pax_price[]" class="form-control child_pax_price" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Price" value="{{$child_price_details[$child_price]['child_pax_price']}}">
                                        </div>
                                        <div class="col-md-2 add_more_childpricing_div">


                                            @if(count($child_price_details)==1)
                                            <img id="add_more_childpricing{{($child_price+1)}}" class="plus-icon add_more_childpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                            @else
                                                @if(count($child_price_details)-1==$child_price)

                                                <img id="remove_more_childpricing{{($child_price+1)}}" class="plus-icon remove_more_childpricing" style="margin-left: auto;" src="{{ asset('assets/images/minus_icon.png') }}">

                                                <img id="add_more_childpricing{{($child_price+1)}}" class="plus-icon add_more_childpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                                @endif
                                            @endif


                                          
                                        </div>
                                    </div>
                                </div>
                                @endfor
                            </div>
                        </div>
                        @else
                        <div class="row">
                                  <div class="col-md-1">
                                    <label for="">Child</label>
                                 </div>
                                <div class="col-md-6">
                                    <div class="childpricing_div" id="childpricing_div__1">
                                    <div class="row">
                                        <div class="col-md-3">
                                                 <input type="text" id="child_min_pax__1" name="child_min_pax[]" class="form-control child_min_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Min">
                                        </div>
                                         <div class="col-md-3">
                                                 <input type="text" id="child_max_pax__1" name="child_max_pax[]" class="form-control child_max_pax" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Max">

                                        </div>
                                        <div class="col-md-3">
                                           <input type="text" id="child_pax_price__1" name="child_pax_price[]" class="form-control child_pax_price" style="margin-bottom: 1px" onkeypress="javascript:return validateNumber(event)" placeholder="Price">
                                        </div>
                                        <div class="col-md-2 add_more_childpricing_div">
                                            <img id="add_more_childpricing1" class="plus-icon add_more_childpricing" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endif

                    </div>
                    </div>





                    <div class="row mb-10">
                       @if($get_activity->availability_qty_details!="" && $get_activity->availability_qty_details!=null)
                       <div class="col-lg-12">
                        <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                          BOOKING AVAILABILITY <span class="asterisk">*</span></h4>
                          @php
                          $availability_qty_details=unserialize($get_activity->availability_qty_details);
                          @endphp
                          @for($availability_count=0;$availability_count< count($availability_qty_details);$availability_count++)
                          <div class="availability_div" id="availability_div__{{($availability_count+1)}}">
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="">From Date</label>
                                    <div class="form-group">
                                        <div class="input-group date">
                                            <input type="text" placeholder="FROM"
                                            class="form-control pull-right datepicker availability_dates availability_from" id="availability_from__{{($availability_count+1)}}" name="availability_from[]" value="{{$availability_qty_details[$availability_count]['availability_from']}}" readonly="readonly">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label for="">To Date</label>
                                    <div class="form-group">
                                        <div class="input-group date">
                                            <input type="text" placeholder="FROM"
                                            class="form-control pull-right datepicker availability_dates availability_to" id="availability_to__{{($availability_count+1)}}" name="availability_to[]" value="{{$availability_qty_details[$availability_count]['availability_to']}}" readonly="readonly">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6" style="border: 1px solid #f3f3f3;padding: 5px 10px;margin-bottom: 10px;border-radius: 10px;">
                                    @php
                                    if(!empty($availability_qty_details[$availability_count]['availability_time_from']))
                                    {
                                     $availability_time_from=$availability_qty_details[$availability_count]['availability_time_from'];
                                    $availability_time_to=$availability_qty_details[$availability_count]['availability_time_to'];
                                    $availability_no_of_bookings=$availability_qty_details[$availability_count]['availability_no_of_bookings'];   
                                    }
                                    else
                                    {
                                    $availability_time_from=array();
                                    $availability_time_to=array();
                                    $availability_no_of_bookings=array(); 
                                    }
                                    
                                    @endphp

                                @if(count($availability_time_from)>0)

                                @for($time_count=0;$time_count< count($availability_time_from);$time_count++)
                                 <div class="row availability_time_div{{($availability_count+1)}}" id="availability_time_div__{{($availability_count+1)}}__{{($time_count+1)}}">
                                    <div class="col-md-3">
                                        <label for="">FROM TIME <span class="asterisk">*</span></label>
                                            <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control availability_time_from timepicker1" id="availability_time_from__{{($availability_count+1)}}__{{($time_count+1)}}" name="availability_time_from[{{$availability_count}}][]" value="{{$availability_qty_details[$availability_count]['availability_time_from'][$time_count]}}" readonly="readonly">
                                                        <div class="input-group-addon" >
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-3">
                                          <label for="">TO TIME <span class="asterisk">*</span></label>
                                        <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control availability_time_to timepicker1" id="availability_time_to__{{($availability_count+1)}}__{{($time_count+1)}}" name="availability_time_to[{{$availability_count}}][]" value="{{$availability_qty_details[$availability_count]['availability_time_to'][$time_count]}}"  readonly="readonly">
                                                        <div class="input-group-addon" >
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">No. of Pax</label>
                                <div class="form-group">
                                    <select class="no_of_bookings form-control" name="no_of_bookings[{{$availability_count}}][]" id="no_of_bookings__{{($availability_count+1)}}__{{($time_count+1)}}">
                                        <option value="">Select No. of Pax</option>
                                        @for($i=0;$i<=500;$i++)
                                        <option value="{{$i}}" @if($availability_qty_details[$availability_count]['availability_no_of_bookings'][$time_count]==$i) selected @endif>{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                    </div>
                                     <div class="col-md-2 add_more_availability_time_div">
                                        <br>
                                         @if(count($availability_time_from)==1)
                                            <img id="add_more_availability_time__{{($availability_count+1)}}__{{($time_count+1)}}" class="plus-icon add_more_availability_time" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                            @else
                                           

                                             @if(count($availability_time_from)-1==$time_count)
                                               <img id="remove_more_availability_time__{{($availability_count+1)}}__{{($time_count+1)}}" class="minus-icon remove_more_availability_time" style="margin-left: auto;" src="{{ asset('assets/images/minus_icon.png') }}">
                                              <img id="add_more_availability_time__{{($availability_count+1)}}__{{($time_count+1)}}" class="plus-icon add_more_availability_time" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                            

                                             @endif

                                             @endif
                                         

                                    </div>
                                </div>
                                @endfor


                                @else
                                <div class="row availability_time_div{{($availability_count+1)}}" id="availability_time_div__{{($availability_count+1)}}__1">
                                    <div class="col-md-3">
                                        <label for="">FROM TIME <span class="asterisk">*</span></label>
                                            <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control availability_time_from timepicker1" id="availability_time_from__{{($availability_count+1)}}__1" name="availability_time_from[{{$availability_count}}][]" readonly="readonly">
                                                        <div class="input-group-addon" >
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-3">
                                          <label for="">TO TIME <span class="asterisk">*</span></label>
                                        <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control availability_time_to timepicker1" id="availability_time_to__{{($availability_count+1)}}__1" name="availability_time_to[{{$availability_count}}][]" readonly="readonly">
                                                        <div class="input-group-addon" >
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">No. of Pax</label>
                                <div class="form-group">
                                    <select class="no_of_bookings form-control" name="no_of_bookings[{{$availability_count}}][]" id="no_of_bookings__{{($availability_count+1)}}__1">
                                        <option value="">Select No. of Pax</option>
                                        @for($i=0;$i<=500;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                    </div>
                                     <div class="col-md-2 add_more_availability_time_div">
                                        <br>
                                         <img id="add_more_availability_time__{{($availability_count+1)}}__1" class="plus-icon add_more_availability_time" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                    </div>
                                </div>
                                @endif
                            </div>
                              {{--  <div class="col-md-3">
                                    <label for="">No. of Pax</label>
                                    <div class="form-group">
                                        <select id="no_of_bookings__{{($availability_count+1)}}" class="no_of_bookings form-control " name="no_of_bookings[]">
                                            <option value="">Select No. of Pax</option>
                                            @for($i=0;$i<=500;$i++)
                                            <option value="{{$i}}" @if($availability_qty_details[$availability_count]['no_of_bookings']==$i) selected @endif>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="col-md-1 add_more_availability_div">
                                    <br>
                                    <br>


                                            @if(count($availability_qty_details)==1)
                                            <img id="add_more_availability{{($availability_count+1)}}" class="plus-icon add_more_availability" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                            @else
                                           

                                             @if(count($availability_qty_details)-1==$availability_count)

                                              <img id="remove_more_availability{{($availability_count+1)}}" class="plus-icon remove_more_availability" style="margin-left: auto;" src="{{ asset('assets/images/minus_icon.png') }}">

                                              <img id="add_more_availability{{($availability_count+1)}}" class="plus-icon add_more_availability" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                             @endif
                                              @endif

                                  
                                </div>
                            </div>
                        </div>
                        @endfor
                    </div>
                    @else

                    <div class="col-lg-12">
                        <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                          BOOKING AVAILABILITY <span class="asterisk">*</span></h4>
                            <div class="availability_div" id="availability_div__1">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="">From Date</label>
                                <div class="form-group">
                                    <div class="input-group date">
                                        <input type="text" placeholder="FROM"
                                        class="form-control pull-right datepicker availability_dates availability_from" id="availability_from__1" name="availability_from[]" readonly="readonly">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="">To Date</label>
                                <div class="form-group">
                                    <div class="input-group date">
                                        <input type="text" placeholder="FROM"
                                        class="form-control pull-right datepicker availability_dates availability_to" id="availability_to__1" name="availability_to[]" readonly="readonly">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6" style="border: 1px solid #f3f3f3;padding: 5px 10px;margin-bottom: 10px;border-radius: 10px;">
                                <div class="row availability_time_div1" id="availability_time_div__1__1">
                                    <div class="col-md-3">
                                        <label for="">FROM TIME <span class="asterisk">*</span></label>
                                            <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control availability_time_from timepicker1" id="availability_time_from__1__1" name="availability_time_from[0][]" readonly="readonly">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-3">
                                          <label for="">TO TIME <span class="asterisk">*</span></label>
                                        <div class="form-group">
                                                <div class="bootstrap-timepicker">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control availability_time_to timepicker1" id="availability_time_to__1__1" name="availability_time_to[0][]" readonly="readonly">
                                                        <div class="input-group-addon">
                                                            <i class="fa fa-clock-o"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="">No. of Pax</label>
                                <div class="form-group">
                                    <select class="no_of_bookings form-control" name="no_of_bookings[0][]" id="no_of_bookings__1__1">
                                        <option value="">Select No. of Pax</option>
                                        @for($i=0;$i<=500;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                    </div>
                                     <div class="col-md-2 add_more_availability_time_div">
                                        <br>
                                         <img id="add_more_availability_time__1__1" class="plus-icon add_more_availability_time" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">

                                    </div>
                                </div>
                            </div>
                             <!-- <div class="col-md-3">
                                <label for="">No. of Pax</label>
                                <div class="form-group">
                                    <select class="no_of_bookings form-control " name="no_of_bookings[]" id="no_of_bookings__1">
                                        <option value="">Select No. of Pax</option>
                                        @for($i=0;$i<=500;$i++)
                                        <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div> -->
                            <div class="col-md-2 add_more_availability_div">
                                <br>
                                <br>
                                 <img id="add_more_availability1" class="plus-icon add_more_availability" style="margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">
                            </div>
                        </div>
                    </div>
                    </div>

                    @endif
                    </div>
        
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> INCLUSIONS <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                             <textarea class="form-control" id="activity_inclusions" name="activity_inclusions">{{$get_activity->activity_inclusions}}</textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> EXCLUSIONS <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <textarea class="form-control" id="activity_exclusions" name="activity_exclusions">{{$get_activity->activity_exclusions}}</textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                          <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> DESCRIPTION <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <textarea class="form-control" id="activity_description" name="activity_description">{{$get_activity->activity_description}}</textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> CANCELLATION POLICY <span class="asterisk">*</span></h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                           <textarea class="form-control" id="activity_cancellation" name="activity_cancellation">{{$get_activity->activity_cancel_policy}}</textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row mb-10">
                                <div class="col-sm-12">
                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">

                                    </div>
                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                        <i class="fa fa-plus-circle"></i> TERMS AND CONDITIONS <span class="asterisk">*</span> </h4>

                                </div>
                                <div class="col-sm-12">
                                    <div class="box">

                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <textarea class="form-control" id="activity_terms_conditions" name="activity_terms_conditions">{{$get_activity->activity_terms_conditions}}</textarea>
                                        </div>
                                    </div>
                                </div>





                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="img_group">
                            <label>IMAGES</label>
                           <!--  <div class="box1">
                                <input class="hide" type="file" id="upload_ativity_images"
                                accept="image/png,image/jpg,image/jpeg"
                                name="upload_ativity_images[]" multiple="multiple">

                                <button type="button"
                                onclick="document.getElementById('upload_ativity_images').click()"
                                id="upload_0" class="btn red btn-outline btn-circle">+

                            </button>
                        </div> -->
                        
                        <div class="input-group control-group increment" id="increment">
          <input type="file" name="upload_ativity_images[]" class="form-control">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_activity_image" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_ativity_images[]" class="form-control">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_activity_image" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>

                        <!-- ngRepeat: (itemindex,item) in temp_loop.enquiry_comment_attachment track by $index -->
                       
                        </div>

                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">

                    </div>
                     <div id="previewImg_new" class="row">
                        @php
                        $get_activity_images=unserialize($get_activity->activity_images);
                        for($images=0;$images< count($get_activity_images);$images++)
                        {
                           @endphp


                           <div class='col-md-3 already_images' id="already_images{{($images+1)}}">
                             <span class="pull-right remove_already_images" title="Delete Image" id="remove_already_images{{($images+1)}}" style="cursor:pointer"> X </span>
                            <input type="hidden" name="upload_ativity_already_images[]" value="{{$get_activity_images[$images]}}">
                            <img class='upload_ativity_images_preview' src='{{ asset("assets/uploads/activities_images") }}/{{$get_activity_images[$images]}}' width=150 height=150 class="img img-thumbnail" />

                        </div>
                           @php
                        }
                        @endphp
                        </div>
                         <div id="previewImg" class="row">
                        </div>





                </div>







                <div class="row mb-10">
                    <div class="col-md-12">
                        <div class="box-header with-border"
                            style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
                            <input type="hidden" name="activity_id" value="@php echo urlencode(base64_encode(base64_encode($get_activity->activity_id))) @endphp">
                            <button type="button" id="update_activity" class="btn btn-rounded btn-primary mr-10">Update</button>
                            <button type="button" id="discard_activity" class="btn btn-rounded btn-primary">Discard</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
    @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif


</div>
</div>
</div>
</div>
   @include('mains.includes.footer')

        @include('mains.includes.bottom-footer')
                     <script type="text/javascript">
    $(document).ready(function() {
       $(document).on("click",".add_more_activity_image",function(){ 
        var id=$(this).parent().parent().attr("id");
        var actual_id=id.split("increment");

        if(actual_id[1]=="")
        {
             var html = $("#clone").html();
          $("#increment").after(html);
        }
        else
        {
              var html = $("#clone"+actual_id[1]).html();
          $("#"+id).after(html);

        }
         
      });
      $("body").on("click",".remove_more_activity_image",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
<script>

$(document).ready(function()
{
    // document.getElementById('upload_ativity_images').addEventListener('change', handleFileSelect, false);
    CKEDITOR.replace('activity_exclusions');
    CKEDITOR.replace('activity_inclusions');
     CKEDITOR.replace('activity_description');
    CKEDITOR.replace('activity_cancellation');
    CKEDITOR.replace('activity_terms_conditions');
    $('.select2').select2();
    var date = new Date();
    date.setDate(date.getDate());
    $('#period_operation_from').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#period_operation_from").datepicker("getDate");
        var date_to = $("#period_operation_to").datepicker("getDate");

        if(!date_to)
        {
            $('#period_operation_to').datepicker("setDate",date_from);
        }
        else if(date_to<date_from)
        {
            $('#period_operation_to').datepicker("setDate",date_from);
        }
    });

    $('#period_operation_to').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#period_operation_from").datepicker("getDate");
        var date_to = $("#period_operation_to").datepicker("getDate");

        if(!date_from)
        {
            $('#period_operation_from').datepicker("setDate",date_to);
        }
        else if(date_to<date_from)
        {
            $('#period_operation_from').datepicker("setDate",date_to);
        }
    });

    $('#validity_operation_from').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#validity_operation_from").datepicker("getDate");
        var date_to = $("#validity_operation_to").datepicker("getDate");

        if(!date_to)
        {
            $('#validity_operation_to').datepicker("setDate",date_from);
        }
        else if(date_to<date_from)
        {
            $('#validity_operation_to').datepicker("setDate",date_from);
        }
    });

    $('#validity_operation_to').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#validity_operation_from").datepicker("getDate");
        var date_to = $("#validity_operation_to").datepicker("getDate");

        if(!date_from)
        {
            $('#validity_operation_from').datepicker("setDate",date_to);
        }
        else if(date_to<date_from)
        {
            $('#validity_operation_from').datepicker("setDate",date_to);
        }
    });
    
    $('#blackout_days').datepicker({
     multidate: true,
     todayHighlight: true,
     format: 'yyyy-mm-dd',
     startDate:date
 });
   $('.timepicker1').timepicker({
         defaultTime: 'current',
        showInputs: false,
        minuteStep: 5,
        timeFormat: 'HH:mm:ss',
         template: 'dropdown'
    });

     $('.availability_dates').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    });

});
    
</script>
<script>
    $(document).on("click",".remove_already_images",function()
    {
        var image=this.id;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this image !",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
              $("#"+image).parent().remove();
              swal("Deleted!", "Selected image has been deleted.", "success");
          } else {
            swal("Cancelled", "Your image is safe :)", "error");
        }
    });
    });
</script>
<script>
    $(document).on("click",".add_more_transport",function()
    {
        var clone_transport = $(".transport_div:last").clone();
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var newer_id = $(".transport_div:last").attr("id");
        new_id = newer_id.split('transport_div');
        old_id = parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;
        clone_transport.find("select[name='activity_transport_currency[]']").attr("id", "activity_transport_currency" + new_id)
        .val(0);
        clone_transport.find("select[name='activity_transport_currency[]']").parent().parent().parent().parent().attr("id",
            "transport_div" + new_id);
         clone_transport.find("select[name='activity_transport_currency[]']").select2();
         clone_transport.find(".select2-container").slice(1).remove();

        clone_transport.find("textarea[name='activity_transport_desc[]']").attr("id", "activity_transport_desc" + new_id).val("");
        clone_transport.find("input[name='activity_transport_cost[]']").attr("id", "activity_transport_cost" + new_id).val("");

        $("#transport_div"+old_id).find(".add_more_transport_div").html("");
        if(old_id>1)
        {
           $("#transport_div"+old_id).find(".add_more_transport_div").append('<img id="remove_more_transport'+old_id+'" class="remove_more_transport minus-icon" src="'+minus_url+'">');
       }
       clone_transport.find(".add_more_transport_div").html('');
       clone_transport.find(".add_more_transport_div").append(' <img id="remove_more_transport'+new_id+'" class="remove_more_transport minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_transport'+new_id+'" class="add_more_transport plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
        $(".transport_div:last").after(clone_transport);
    });

    $(document).on("click",".add_more_markup",function()
    {
        var clone_markup = $(".markup_div:last").clone();
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var newer_id = $(".markup_div:last").attr("id");
        new_id = newer_id.split('markup_div');
        old_id = parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;
        clone_markup.find("select[name='activity_nationality[]']").attr("id", "activity_nationality" + new_id)
        .val(0);
        clone_markup.find("select[name='activity_nationality[]']").parent().parent().parent().parent().attr("id",
            "markup_div" + new_id);
         clone_markup.find("select[name='activity_nationality[]']").select2();
         clone_markup.find(".select2-container").slice(1).remove();
        clone_markup.find("select[name='activity_markup[]']").attr("id", "activity_markup" + new_id).val("0");
        clone_markup.find("input[name='activity_amount[]']").attr("id", "activity_amount" + new_id).val("");

         $("#markup_div"+old_id).find(".add_more_markup_div").html("");
        if(old_id>1)
        {
           $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="remove_more_markup'+old_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'" style="margin-left: auto;">');
       }
       clone_markup.find(".add_more_markup_div").html('');
       clone_markup.find(".add_more_markup_div").append(' <img id="remove_more_markup'+new_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_markup'+new_id+'" class="add_more_markup plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
        $(".markup_div:last").after(clone_markup);
    });

     $(document).on("click",".add_more_availability",function()
    {
        var clone_markup = $(".availability_div:last").clone();
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var newer_id = $(".availability_div:last").attr("id");
        new_id = newer_id.split('__');
        old_id = parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;
        clone_markup.find(".availability_from").attr("id", "availability_from__" + new_id)
        .val("");
        clone_markup.find(".availability_from").parent().parent().parent().parent().parent().attr("id",
            "availability_div__" + new_id);
        clone_markup.find(".availability_to").attr("id", "availability_to__" + new_id)
        .val("");
        // clone_markup.find(".no_of_bookings").attr("id", "no_of_bookings__" + new_id).val("");
        clone_markup.find(".availability_time_div"+old_id).attr("id","availability_time_div__"+new_id+"__1").removeClass("availability_time_div"+old_id).addClass("availability_time_div"+new_id);
        clone_markup.find(".availability_time_div"+new_id).slice(1).remove();


         clone_markup.find(".availability_time_from").attr({"id":"availability_time_from__"+new_id+"__1","name":"availability_time_from["+parseInt(old_id)+"][]"})
        .val("");
        clone_markup.find(".availability_time_to").attr({"id":"availability_time_to__"+new_id+"__1","name":"availability_time_to["+parseInt(old_id)+"][]"})
        .val("");
        clone_markup.find(".no_of_bookings").attr({"id":"no_of_bookings__"+new_id+"__1","name":"no_of_bookings["+parseInt(old_id)+"][]"}).val("");


       clone_markup.find(".add_more_availability_time_div").html('<br><img id="add_more_availability_time__'+new_id+'__1" class="add_more_availability_time plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');


        $("#availability_div__"+old_id).find(".add_more_availability_div").html("");
       //  if(old_id>1)
       //  {
       //     $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="remove_more_markup'+old_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'" style="margin-left: auto;">');
       // }
       clone_markup.find(".add_more_availability_div").html('');
       clone_markup.find(".add_more_availability_div").html('<br><img id="remove_more_availability'+new_id+'" class="remove_more_availability minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_availability'+new_id+'" class="add_more_availability plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
        $(".availability_div:last").after(clone_markup);

         var date = new Date();
    date.setDate(date.getDate());

         $('.availability_dates').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    });
          $('.timepicker1').timepicker({
         defaultTime: 'current',
        showInputs: false,
        minuteStep: 5,
        timeFormat: 'HH:mm:ss',
         template: 'dropdown'
    });

    });

 $(document).on("click",".add_more_availability_time",function()
    {
        var add_more_id=this.id;
        var add_more_parent_id=add_more_id.split('__');
        var clone_markup = $(".availability_time_div"+add_more_parent_id[1]+":last").clone();
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var newer_id = $(".availability_time_div"+add_more_parent_id[1]+":last").attr("id");
       
        var new_id = newer_id.split('__');
        var parent_id=parseInt(new_id[1]);
        var old_id = parseInt(new_id[2]);
        var new_id = parseInt(new_id[2]) + 1;
        clone_markup.find(".availability_time_from").attr({"id":"availability_time_from__"+parent_id+"__"+new_id,"name":"availability_time_from["+parseInt(parent_id-1)+"][]"})
        .val("");
        clone_markup.find(".availability_time_from").parent().parent().parent().parent().parent().attr("id","availability_time_div__"+parent_id+"__"+new_id);
        clone_markup.find(".availability_time_to").attr({"id":"availability_time_to__"+parent_id+"__"+new_id,"name":"availability_time_to["+parseInt(parent_id-1)+"][]"})
        .val("");
        clone_markup.find(".no_of_bookings").attr({"id":"no_of_bookings__"+parent_id+"__"+new_id,"name":"no_of_bookings["+parseInt(parent_id-1)+"][]"}).val("");

         $("#availability_time_div__"+parent_id+"__"+old_id).find(".add_more_availability_time_div").html("");
       //  if(old_id>1)
       //  {
       //     $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="remove_more_markup'+old_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'" style="margin-left: auto;">');
       // }
       clone_markup.find(".add_more_availability_time_div").html('');
       clone_markup.find(".add_more_availability_time_div").html('<br><img id="remove_more_availability_time__'+parent_id+'__'+new_id+'" class="remove_more_availability_time minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_availability_time__'+parent_id+'__'+new_id+'" class="add_more_availability_time plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
        $(".availability_time_div"+add_more_parent_id[1]+":last").after(clone_markup);

         $('.timepicker1').timepicker({
         defaultTime: 'current',
        showInputs: false,
        minuteStep: 5,
        timeFormat: 'HH:mm:ss',
         template: 'dropdown'
    });

    });


    $(document).on("click",".add_more_adultpricing",function()
    {
        var clone_markup = $(".adultpricing_div:last").clone();
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var newer_id = $(".adultpricing_div:last").attr("id");
        new_id = newer_id.split('__');
        old_id = parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;
        clone_markup.find(".adult_min_pax").attr("id", "adult_min_pax__" + new_id)
        .val("");
        clone_markup.find(".adult_min_pax").parent().parent().parent().attr("id",
            "adultpricing_div__" + new_id);
        clone_markup.find(".adult_max_pax").attr("id", "adult_max_pax__" + new_id)
        .val("");
        clone_markup.find(".adult_pax_price").attr("id", "adult_pax_price__" + new_id).val("");

         $("#adultpricing_div__"+old_id).find(".add_more_adultpricing_div").html("");
       clone_markup.find(".add_more_adultpricing_div").html('');
       clone_markup.find(".add_more_adultpricing_div").html(' <img id="remove_more_adultpricing'+new_id+'" class="remove_more_adultpricing minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_adultpricing'+new_id+'" class="add_more_adultpricing plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
        $(".adultpricing_div:last").after(clone_markup);

    });

    $(document).on("click",".add_more_infantpricing",function()
    {
        var clone_markup = $(".infantpricing_div:last").clone();
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var newer_id = $(".infantpricing_div:last").attr("id");
        new_id = newer_id.split('__');
        old_id = parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;
        clone_markup.find(".infant_min_pax").attr("id", "infant_min_pax__" + new_id)
        .val("");
        clone_markup.find(".infant_min_pax").parent().parent().parent().attr("id",
            "infantpricing_div__" + new_id);
        clone_markup.find(".infant_max_pax").attr("id", "infant_max_pax__" + new_id)
        .val("");
        clone_markup.find(".infant_pax_price").attr("id", "infant_pax_price__" + new_id).val("");

         $("#infantpricing_div__"+old_id).find(".add_more_infantpricing_div").html("");
       clone_markup.find(".add_more_infantpricing_div").html('');
       clone_markup.find(".add_more_infantpricing_div").html('<img id="remove_more_infantpricing'+new_id+'" class="remove_more_infantpricing minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_infantpricing'+new_id+'" class="add_more_infantpricing plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
        $(".infantpricing_div:last").after(clone_markup);

    });

    $(document).on("click",".add_more_childpricing",function()
    {
        var clone_markup = $(".childpricing_div:last").clone();
        var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        var newer_id = $(".childpricing_div:last").attr("id");
        new_id = newer_id.split('__');
        old_id = parseInt(new_id[1]);
        new_id = parseInt(new_id[1]) + 1;
        clone_markup.find(".child_min_pax").attr("id", "child_min_pax__" + new_id)
        .val("");
        clone_markup.find(".child_min_pax").parent().parent().parent().attr("id",
            "childpricing_div__" + new_id);
        clone_markup.find(".child_max_pax").attr("id", "child_max_pax__" + new_id)
        .val("");
        clone_markup.find(".child_pax_price").attr("id", "child_pax_price__" + new_id).val("");

         $("#childpricing_div__"+old_id).find(".add_more_childpricing_div").html("");
       clone_markup.find(".add_more_childpricing_div").html('');
       clone_markup.find(".add_more_childpricing_div").html('<img id="remove_more_childpricing'+new_id+'" class="remove_more_childpricing minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_childpricing'+new_id+'" class="add_more_childpricing plus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
        $(".childpricing_div:last").after(clone_markup);

    });


    $(document).on("click", ".remove_more_transport", function () {
        var id = this.id;
        var split_id = id.split('remove_more_transport');
        $("#transport_div" + split_id[1]).remove();
         var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        $("#transport_div" + split_id[1]).remove();

         var last_id = $(".transport_div:last").attr("id");
         old_id = last_id.split('transport_div');
         old_id=parseInt(old_id[1]);


          if(old_id>1)
         {
           $("#transport_div"+old_id).find(".add_more_transport_div").html("");
           $("#transport_div"+old_id).find(".add_more_transport_div").append('<img id="remove_more_transport'+old_id+'" class="remove_more_transport minus-icon" src="'+minus_url+'"  style="margin-left: auto;"> <img id="add_more_transport'+old_id+'" class="add_more_transport plus-icon" src="'+add_url+'"  style="margin-left: auto;">');
       }
       else
       {

          $("#transport_div"+old_id).find(".add_more_transport_div").html("");
           $("#transport_div"+old_id).find(".add_more_transport_div").append('<img id="add_more_transport'+old_id+'" class="add_more_transport minus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
     }
    });
    $(document).on("click", ".remove_more_markup", function () {
        var id = this.id;
        var split_id = id.split('remove_more_markup');
         var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        $("#markup_div" + split_id[1]).remove();

         var last_id = $(".markup_div:last").attr("id");
         old_id = last_id.split('markup_div');
         old_id=parseInt(old_id[1]);


          if(old_id>1)
         {
           $("#markup_div"+old_id).find(".add_more_markup_div").html("");
           $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="remove_more_markup'+old_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_markup'+old_id+'" class="add_more_markup plus-icon" src="'+add_url+'" style="margin-left: auto;">');
       }
       else
       {

          $("#markup_div"+old_id).find(".add_more_markup_div").html("");
           $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="add_more_markup'+old_id+'" class="add_more_markup minus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
     }
    });

     $(document).on("click", ".remove_more_availability", function () {
        var id = this.id;
        var split_id = id.split('remove_more_availability');
         var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        $("#availability_div__" + split_id[1]).remove();

         var last_id = $(".availability_div:last").attr("id");
         old_id = last_id.split('__');
         old_id=parseInt(old_id[1]);


          if(old_id>1)
         {
           $("#availability_div__"+old_id).find(".add_more_availability_div").html("");
           $("#availability_div__"+old_id).find(".add_more_availability_div").append('<br><img id="remove_more_availability'+old_id+'" class="remove_more_availability minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_availability'+old_id+'" class="add_more_availability plus-icon" src="'+add_url+'" style="margin-left: auto;">');
       }
       else
       {

         $("#availability_div__"+old_id).find(".add_more_availability_div").html("");
         $("#availability_div__"+old_id).find(".add_more_availability_div").append('<br><img id="add_more_availability'+old_id+'" class="add_more_availability minus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
     }
    });
     
      $(document).on("click", ".remove_more_availability_time", function () {
        var id = this.id;
        var split_id = id.split('__');
         var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        $("#availability_time_div__" + split_id[1]+"__"+split_id[2]).remove();

         var last_id = $(".availability_time_div"+split_id[1]+":last").attr("id");
         old_id = last_id.split('__');
         old_id=parseInt(old_id[2]);


          if(old_id>1)
         {
           $("#availability_time_div__" + split_id[1]+"__"+old_id).find(".add_more_availability_time_div").html("");
           $("#availability_time_div__" + split_id[1]+"__"+old_id).find(".add_more_availability_time_div").append('<br><img id="remove_more_availability__'+ split_id[1]+'__'+old_id+'" class="remove_more_availability_time minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_availability__'+ split_id[1]+'__'+old_id+'" class="add_more_availability_time  plus-icon" src="'+add_url+'" style="margin-left: auto;">');
       }
       else
       {

         $("#availability_time_div__" + split_id[1]+"__"+old_id).find(".add_more_availability_time_div").html("");
         $("#availability_time_div__" + split_id[1]+"__"+old_id).find(".add_more_availability_time_div").append('<br><img id="add_more_availability__'+ split_id[1]+'__'+old_id+'" class="add_more_availability_time  minus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
     }
    });


      $(document).on("click", ".remove_more_adultpricing", function () {
        var id = this.id;
        var split_id = id.split('remove_more_adultpricing');
         var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        $("#adultpricing_div__" + split_id[1]).remove();

         var last_id = $(".adultpricing_div:last").attr("id");
         old_id = last_id.split('__');
         old_id=parseInt(old_id[1]);


          if(old_id>1)
         {
           $("#adultpricing_div__"+old_id).find(".add_more_adultpricing_div").html("");
           $("#adultpricing_div__"+old_id).find(".add_more_adultpricing_div").append('<img id="remove_more_adultpricing'+old_id+'" class="remove_more_adultpricing minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_adultpricing'+old_id+'" class="add_more_adultpricing plus-icon" src="'+add_url+'" style="margin-left: auto;">');
       }
       else
       {

         $("#adultpricing_div__"+old_id).find(".add_more_adultpricing_div").html("");
         $("#adultpricing_div__"+old_id).find(".add_more_adultpricing_div").append('<img id="add_more_adultpricing'+old_id+'" class="add_more_adultpricing minus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
     }
    });

       $(document).on("click", ".remove_more_childpricing", function () {
        var id = this.id;
        var split_id = id.split('remove_more_childpricing');
         var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        $("#childpricing_div__" + split_id[1]).remove();

         var last_id = $(".childpricing_div:last").attr("id");
         old_id = last_id.split('__');
         old_id=parseInt(old_id[1]);


          if(old_id>1)
         {
           $("#childpricing_div__"+old_id).find(".add_more_childpricing_div").html("");
           $("#childpricing_div__"+old_id).find(".add_more_childpricing_div").append('<img id="remove_more_childpricing'+old_id+'" class="remove_more_childpricing minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_childpricing'+old_id+'" class="add_more_childpricing plus-icon" src="'+add_url+'" style="margin-left: auto;">');
       }
       else
       {

         $("#childpricing_div__"+old_id).find(".add_more_childpricing_div").html("");
         $("#childpricing_div__"+old_id).find(".add_more_childpricing_div").append('<img id="add_more_childpricing'+old_id+'" class="add_more_childpricing minus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
     }
    });

         $(document).on("click", ".remove_more_infantpricing", function () {
        var id = this.id;
        var split_id = id.split('remove_more_infantpricing');
         var add_url= "{!! asset('assets/images/add_icon.png') !!}";
        var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
        $("#infantpricing_div__" + split_id[1]).remove();

         var last_id = $(".infantpricing_div:last").attr("id");
         old_id = last_id.split('__');
         old_id=parseInt(old_id[1]);


          if(old_id>1)
         {
           $("#infantpricing_div__"+old_id).find(".add_more_infantpricing_div").html("");
           $("#infantpricing_div__"+old_id).find(".add_more_infantpricing_div").append('<img id="remove_more_infantpricing'+old_id+'" class="remove_more_infantpricing minus-icon" src="'+minus_url+'" style="margin-left: auto;"> <img id="add_more_infantpricing'+old_id+'" class="add_more_infantpricing plus-icon" src="'+add_url+'" style="margin-left: auto;">');
       }
       else
       {

         $("#infantpricing_div__"+old_id).find(".add_more_infantpricing_div").html("");
         $("#infantpricing_div__"+old_id).find(".add_more_infantpricing_div").append('<img id="add_more_infantpricing'+old_id+'" class="add_more_infantpricing minus-icon" src="'+add_url+'" style="margin-left: auto;"> ');
     }
    });




    $(document).on("change","input[name='is_all_days']",function()
    {
        if($(this).is(":checked"))
        {
            if($("input[name='is_all_days']:checked").val()=="Yes")
            {
               $(".weekdays_yes").prop("checked",true);
            }
            else
            {
                $(".weekdays_no").prop("checked",true);
            }
        }


    });
</script>
<script>
    $(document).on("change","#supplier_name",function()
    {
        if($("#supplier_name").val()!="0")
        {
            var supplier_id=$(this).val();
            $.ajax({
                url:"{{route('search-supplier-country')}}",
                type:"GET",
                data:{"supplier_id":supplier_id},
                success:function(response)
                {
                    $("#activity_country").html(response);
                    $('#activity_country').select2();
                    $("#activity_country").prop("disabled",false);

                     $("#activity_city").html("");

                }
            });
        }
    });

    $(document).on("change","#activity_country",function()
    {
         if($("#activity_country").val()!="0")
        {
            var country_id=$(this).val();
            $.ajax({
                url:"{{route('search-country-cities')}}",
                type:"GET",
                data:{"country_id":country_id},
                success:function(response)
                {
                   
                    $("#activity_city").html(response);
                    $('#activity_city').select2();
                      $("#acitvity_city_div").show();
                   

                }
            });
        }

    });
</script>
<script>
    $(document).on("click","#update_activity",function()
    {
          var activity_type=$("#activity_type").val();
        var activity_name=$("#activity_name").val();
        var supplier_name=$("#supplier_name").val();
        var activity_location=$("#activity_location").val();
        var activity_country=$("#activity_country").val();
        var activity_city=$("#activity_city").val();
         var activity_duration=$("#activity_duration").val();
        var period_operation_from=$("#period_operation_from").val();
        var period_operation_to=$("#period_operation_to").val();
        var validity_operation_from=$("#validity_operation_from").val();
        var validity_operation_to=$("#validity_operation_to").val();
        var activity_time_from=$("#activity_time_from").val();
        var activity_time_to=$("#activity_time_to").val();
        var is_all_days = $("input[name='is_all_days']:checked").val();
        var week_monday = $("input[name='week_monday']:checked").val();
        var week_tuesday = $("input[name='week_tuesday']:checked").val();
        var week_wednesday = $("input[name='week_wednesday']:checked").val();
        var week_thursday = $("input[name='week_thursday']:checked").val();
        var week_friday = $("input[name='week_friday']:checked").val();
        var week_saturday = $("input[name='week_saturday']:checked").val();
        var week_sunday = $("input[name='week_sunday']:checked").val();
        var activity_currency=$("#activity_currency").val();
        // var activity_adult_cost=$("#activity_adult_cost").val();
        // var activity_child_cost=$("#activity_child_cost").val();
        // var for_all_ages = $("input[name='for_all_ages']:checked").val();
        // var child_allowed = $("input[name='child_allowed']:checked").val();
        // var child_age = $("#child_age").val();
        // var adult_allowed = $("input[name='adult_allowed']:checked").val();
        // var adult_age = $("#adult_age").val();
        var activity_inclusions= CKEDITOR.instances.activity_inclusions.getData();
        var activity_exclusions=CKEDITOR.instances.activity_exclusions.getData();
        var activity_description=CKEDITOR.instances.activity_description.getData();
        var activity_cancellation=CKEDITOR.instances.activity_cancellation.getData();
        var activity_terms_conditions=CKEDITOR.instances.activity_terms_conditions.getData();


        if (activity_type.trim() == "0")
        {
            $("#activity_type").css("border", "1px solid #cf3c63");

        } else

        {
            $("#activity_type").css("border", "1px solid #9e9e9e");
        }

        if (activity_name.trim() == "")
        {
            $("#activity_name").css("border", "1px solid #cf3c63");

        } else

        {
            $("#activity_name").css("border", "1px solid #9e9e9e");
        }

        if (supplier_name == "0")
        {
            $("#supplier_name").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

        } else

        {
           $("#supplier_name").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
       }
       if (activity_location.trim() == "")
       {
         $("#activity_location").css("border", "1px solid #cf3c63");

     } else

     {
      $("#activity_location").css("border", "1px solid #9e9e9e");
  }
  if (activity_country == "0")
  {
    $("#activity_country").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

} else

{
   $("#activity_country").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
}
if (activity_city == "0")
{
    $("#activity_city").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

} else

{
   $("#activity_city").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
}
if (activity_duration.trim() == "")
{
   $("#activity_duration").css("border", "1px solid #cf3c63");

} else

{
  $("#activity_duration").css("border", "1px solid #9e9e9e");
}
// if (period_operation_from.trim() == "")
// {
//     $("#period_operation_from").css("border", "1px solid #cf3c63");

// } else

// {
//     $("#period_operation_from").css("border", "1px solid #9e9e9e");
// }
// if (period_operation_to.trim() == "")
// {
//     $("#period_operation_to").css("border", "1px solid #cf3c63");

// } else

// {
//     $("#period_operation_to").css("border", "1px solid #9e9e9e");
// }
if (validity_operation_from.trim() == "")
{
    $("#validity_operation_from").css("border", "1px solid #cf3c63");

} else

{
    $("#validity_operation_from").css("border", "1px solid #9e9e9e");
}
if (validity_operation_to.trim() == "")
{
    $("#validity_operation_to").css("border", "1px solid #cf3c63");

} else

{
    $("#validity_operation_to").css("border", "1px solid #9e9e9e");
}
if (activity_time_from.trim() == "")
{
    $("#activity_time_from").css("border", "1px solid #cf3c63");

} else

{
    $("#activity_time_from").css("border", "1px solid #9e9e9e");
}
if (activity_time_to.trim() == "")
{
    $("#activity_time_to").css("border", "1px solid #cf3c63");

} else

{
    $("#activity_time_to").css("border", "1px solid #9e9e9e");
}
if (!is_all_days) {
    $("input[name='is_all_days']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='is_all_days']").parent().css("border", "1px solid white");
}
if (!week_monday) {
    $("input[name='week_monday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_monday']").parent().css("border", "1px solid white");
}
if (!week_tuesday) {
    $("input[name='week_tuesday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_tuesday']").parent().css("border", "1px solid white");
}
if (!week_wednesday) {
    $("input[name='week_wednesday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_wednesday']").parent().css("border", "1px solid white");
}
if (!week_thursday) {
    $("input[name='week_thursday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_thursday']").parent().css("border", "1px solid white");
}
if (!week_friday) {
    $("input[name='week_friday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_friday']").parent().css("border", "1px solid white");
}
if (!week_saturday) {
    $("input[name='week_saturday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_saturday']").parent().css("border", "1px solid white");
}
if (!week_sunday) {
    $("input[name='week_sunday']").parent().css("border", "1px solid #cf3c63");
} else {
    $("input[name='week_sunday']").parent().css("border", "1px solid white");
}
if (activity_currency == "0")
{
    $("#activity_currency").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

} else

{
   $("#activity_currency").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
}
// if (activity_adult_cost.trim() == "")
// {
//     $("#activity_adult_cost").css("border", "1px solid #cf3c63");

// } else

// {
//     $("#activity_adult_cost").css("border", "1px solid #9e9e9e");
// }
// if (activity_child_cost.trim() == "")
// {
//     $("#activity_child_cost").css("border", "1px solid #cf3c63");

// } else

// {
//     $("#activity_child_cost").css("border", "1px solid #9e9e9e");
// }

// if (!for_all_ages) {
//     $("input[name='for_all_ages']").parent().css("border", "1px solid #cf3c63");
// } else {
//     $("input[name='for_all_ages']").parent().css("border", "1px solid white");
// }

// if (for_all_ages=="No" && !child_allowed) {
//     $("input[name='child_allowed']").parent().css("border", "1px solid #cf3c63");
// } else {
//     $("input[name='child_allowed']").parent().css("border", "1px solid white");
// }
// if (for_all_ages=="No" && !adult_allowed) {
//     $("input[name='adult_allowed']").parent().css("border", "1px solid #cf3c63");
// } else {
//     $("input[name='adult_allowed']").parent().css("border", "1px solid white");
// }

// if (for_all_ages=="No" && child_allowed=="Yes" && child_age.trim()=="") {
//     $("input[name='child_age']").css("border", "1px solid #cf3c63");
// } else {
//     $("input[name='child_age']").css("border", "1px solid #9e9e9e");
// }

// if (for_all_ages=="No" && child_allowed=="Yes" && adult_age.trim()=="") {
//     $("input[name='adult_age']").css("border", "1px solid #cf3c63");
// } else {
//     $("input[name='adult_age']").css("border", "1px solid #9e9e9e");
// }


// var activity_nationality = 1;
// var activity_nationality_error = 0;
// $("select[name='activity_nationality[]']").each(function () {

//     if ($(this).val() == "0") {
//         $("#activity_nationality" + activity_nationality).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
//         $("#activity_nationality" + activity_nationality).parent().find(".select2-selection").focus();
//         activity_nationality_error++;
//     } else {
//         $("#activity_nationality" + activity_nationality).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
//     }
//     activity_nationality++;

// });

// var activity_markup = 1;
// var activity_markup_error = 0;
// $("select[name='activity_markup[]']").each(function () {

//     if ($(this).val()== "0") {
//         $("#activity_markup" + activity_markup).css("border", "1px solid #cf3c63");
//         $("#activity_markup" + activity_markup).focus();
//         activity_markup_error++;
//     } else {
//         $("#activity_markup" + activity_markup).css("border", "1px solid #9e9e9e");
//     }
//     activity_markup++;

// });

// var activity_markup_amt = 1;
// var activity_markup_amt_error = 0;
// $("input[name='activity_amount[]']").each(function () {



//     if ($(this).val().trim() == "") {
//         $("#activity_amount" + activity_markup_amt).css("border", "1px solid #cf3c63");
//         $("#activity_amount" + activity_markup_amt).focus();
//         activity_markup_amt_error++;
//     } else {
//         $("#activity_amount" + activity_markup_amt).css("border", "1px solid #9e9e9e");
//     }
//     activity_markup_amt++;

// });
// var activity_transport_currency = 1;
// var activity_transport_currency_error = 0;
// $("select[name='activity_transport_currency[]']").each(function () {

//     if ($(this).val() == "0") {
//         $("#activity_transport_currency" + activity_transport_currency).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
//         $("#activity_transport_currency" + activity_transport_currency).parent().find(".select2-selection").focus();
//         activity_transport_currency_error++;
//     } else {
//         $("#activity_transport_currency" + activity_transport_currency).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
//     }
//     activity_transport_currency++;

// });

// var activity_transport_desc = 1;
// var activity_transport_desc_error = 0;
// $("textarea[name='activity_transport_desc[]']").each(function () {



//     if ($(this).val().trim() == "") {
//         $("#activity_transport_desc" + activity_transport_desc).css("border", "1px solid #cf3c63");
//         $("#activity_transport_desc" + activity_transport_desc).focus();
//         activity_transport_desc_error++;
//     } else {
//         $("#activity_transport_desc" + activity_transport_desc).css("border", "1px solid #9e9e9e");
//     }
//     activity_transport_desc++;

// });

// var activity_transport_cost = 1;
// var activity_transport_cost_error = 0;
// $("input[name='activity_transport_cost[]']").each(function () {
//     if ($(this).val().trim() == "") {
//         $("#activity_transport_cost" + activity_transport_cost).css("border", "1px solid #cf3c63");
//         $("#activity_transport_cost" + activity_transport_cost).focus();
//         activity_transport_cost_error++;
//     } else {
//         $("#activity_transport_cost" + activity_transport_cost).css("border", "1px solid #9e9e9e");
//     }
//     activity_transport_cost++;

// });
var adults_age = $("input[name='adults_age']:checked").val();
var children_age = $("input[name='children_age']:checked").val();
var infant_age = $("input[name='infant_age']:checked").val();

var adults_min_age=$("#adults_min_age").val();
var child_min_age=$("#child_min_age").val();
var infant_min_age=$("#infant_min_age").val();

var adults_max_age=$("#adults_max_age").val();
var child_max_age=$("#child_max_age").val();
var infant_max_age=$("#infant_max_age").val();

if(adults_age)
{

if(adults_min_age.trim()=="")
{
    $("#adults_min_age").css("border", "1px solid #cf3c63");
}
else
{
 $("#adults_min_age").css("border", "1px solid #9e9e9e");
}
if(adults_max_age.trim()=="")
{
    $("#adults_max_age").css("border", "1px solid #cf3c63");
}
else
{
 $("#adults_max_age").css("border", "1px solid #9e9e9e");
}


var adult_min_pax = 1;
var adult_min_pax_error = 0;
$(".adult_min_pax").each(function () {
    if ($(this).val().trim() == "") {
        $("#adult_min_pax__" + adult_min_pax).css("border", "1px solid #cf3c63");
        $("#adult_min_pax__" + adult_min_pax).focus();
        adult_min_pax_error++;
    } else {
        $("#adult_min_pax__" + adult_min_pax).css("border", "1px solid #9e9e9e");
    }
    adult_min_pax++;

});

var adult_max_pax = 1;
var adult_max_pax_error = 0;
$(".adult_max_pax").each(function () {
    if ($(this).val().trim() == "") {
        $("#adult_max_pax__" + adult_max_pax).css("border", "1px solid #cf3c63");
        $("#adult_max_pax__" + adult_max_pax).focus();
        adult_max_pax_error++;
    } else {
        $("#adult_max_pax__" + adult_max_pax).css("border", "1px solid #9e9e9e");
    }
    adult_max_pax++;

});


var adult_pax_price = 1;
var adult_pax_price_error = 0;
$(".adult_pax_price").each(function () {
    if ($(this).val().trim() == "") {
        $("#adult_pax_price__" + adult_pax_price).css("border", "1px solid #cf3c63");
        $("#adult_pax_price__" + adult_pax_price).focus();
        adult_pax_price_error++;
    } else {
        $("#adult_pax_price__" + adult_pax_price).css("border", "1px solid #9e9e9e");
    }
    adult_pax_price++;

});

}
else
{

 $("#adults_min_age").css("border", "1px solid #9e9e9e");
 $("#adults_max_age").css("border", "1px solid #9e9e9e");


var adult_min_pax = 1;
$(".adult_min_pax").each(function () {
        $("#adult_min_pax__" + adult_min_pax).css("border", "1px solid #9e9e9e");
    adult_min_pax++;

});

var adult_max_pax = 1;
$(".adult_max_pax").each(function () {
        $("#adult_max_pax__" + adult_max_pax).css("border", "1px solid #9e9e9e");
    adult_max_pax++;
});


var adult_pax_price = 1;
$(".adult_pax_price").each(function () {
        $("#adult_pax_price__" + adult_pax_price).css("border", "1px solid #9e9e9e");
    adult_pax_price++;
});
}

if(children_age)
{

    if(child_min_age.trim()=="")
    {
        $("#child_min_age").css("border", "1px solid #cf3c63");
    }
    else
    {
       $("#child_min_age").css("border", "1px solid #9e9e9e");
   }
   if(child_max_age.trim()=="")
   {
    $("#child_max_age").css("border", "1px solid #cf3c63");
}
else
{
   $("#child_max_age").css("border", "1px solid #9e9e9e");
}

var child_min_pax = 1;
var child_min_pax_error = 0;
$(".child_min_pax").each(function () {
    if ($(this).val().trim() == "") {
        $("#child_min_pax__" + child_min_pax).css("border", "1px solid #cf3c63");
        $("#child_min_pax__" + child_min_pax).focus();
        child_min_pax_error++;
    } else {
        $("#child_min_pax__" + child_min_pax).css("border", "1px solid #9e9e9e");
    }
    child_min_pax++;

});

var child_max_pax = 1;
var child_max_pax_error = 0;
$(".child_max_pax").each(function () {
    if ($(this).val().trim() == "") {
        $("#child_max_pax__" + child_max_pax).css("border", "1px solid #cf3c63");
        $("#child_max_pax__" + child_max_pax).focus();
        child_max_pax_error++;
    } else {
        $("#child_max_pax__" + child_max_pax).css("border", "1px solid #9e9e9e");
    }
    child_max_pax++;

});


var child_pax_price = 1;
var child_pax_price_error = 0;
$(".child_pax_price").each(function () {
    if ($(this).val().trim() == "") {
        $("#child_pax_price__" + child_pax_price).css("border", "1px solid #cf3c63");
        $("#child_pax_price__" + child_pax_price).focus();
        child_pax_price_error++;
    } else {
        $("#child_pax_price__" + child_pax_price).css("border", "1px solid #9e9e9e");
    }
    child_pax_price++;

});

}
else
{

$("#child_min_age").css("border", "1px solid #9e9e9e");  
$("#child_max_age").css("border", "1px solid #9e9e9e");

var child_min_pax = 1;
$(".child_min_pax").each(function () {
 $("#child_min_pax__" + child_min_pax).css("border", "1px solid #9e9e9e");
    child_min_pax++;
});

var child_max_pax = 1;
$(".child_max_pax").each(function () {
    $("#child_max_pax__" + child_max_pax).css("border", "1px solid #9e9e9e");
    child_max_pax++;

});


var child_pax_price = 1;
$(".child_pax_price").each(function () {
    $("#child_pax_price__" + child_pax_price).css("border", "1px solid #9e9e9e");
    child_pax_price++;

});

}

if(infant_age)
{

     if(infant_min_age.trim()=="")
    {
        $("#infant_min_age").css("border", "1px solid #cf3c63");
    }
    else
    {
       $("#infant_min_age").css("border", "1px solid #9e9e9e");
   }
   if(infant_max_age.trim()=="")
   {
    $("#infant_max_age").css("border", "1px solid #cf3c63");
}
else
{
   $("#infant_max_age").css("border", "1px solid #9e9e9e");
}

var infant_min_pax = 1;
var infant_min_pax_error = 0;
$(".infant_min_pax").each(function () {
    if ($(this).val().trim() == "") {
        $("#infant_min_pax__" + infant_min_pax).css("border", "1px solid #cf3c63");
        $("#infant_min_pax__" + infant_min_pax).focus();
        infant_min_pax_error++;
    } else {
        $("#infant_min_pax__" + infant_min_pax).css("border", "1px solid #9e9e9e");
    }
    infant_min_pax++;

});

var infant_max_pax = 1;
var infant_max_pax_error = 0;
$(".infant_max_pax").each(function () {
    if ($(this).val().trim() == "") {
        $("#infant_max_pax__" + infant_max_pax).css("border", "1px solid #cf3c63");
        $("#infant_max_pax__" + infant_max_pax).focus();
        infant_max_pax_error++;
    } else {
        $("#infant_max_pax__" + infant_max_pax).css("border", "1px solid #9e9e9e");
    }
    infant_max_pax++;

});


var infant_pax_price = 1;
var infant_pax_price_error = 0;
$(".infant_pax_price").each(function () {
    if ($(this).val().trim() == "") {
        $("#infant_pax_price__" + infant_pax_price).css("border", "1px solid #cf3c63");
        $("#infant_pax_price__" + infant_pax_price).focus();
        infant_pax_price_error++;
    } else {
        $("#infant_pax_price__" + infant_pax_price).css("border", "1px solid #9e9e9e");
    }
    infant_pax_price++;

});

}
else
{
$("#infant_min_age").css("border", "1px solid #9e9e9e");
 $("#infant_max_age").css("border", "1px solid #9e9e9e");

var infant_min_pax = 1;
$(".infant_min_pax").each(function () {
        $("#infant_min_pax__" + infant_min_pax).css("border", "1px solid #9e9e9e");
    infant_min_pax++;
});

var infant_max_pax = 1;
$(".infant_max_pax").each(function () {
        $("#infant_max_pax__" + infant_max_pax).css("border", "1px solid #9e9e9e");
    infant_max_pax++;
});


var infant_pax_price = 1;
$(".infant_pax_price").each(function () {
        $("#infant_pax_price__" + infant_pax_price).css("border", "1px solid #9e9e9e");
    infant_pax_price++;

});

}

var availability_from = 1;
var availability_from_error = 0;
$(".availability_from").each(function () {
    if ($(this).val().trim() == "") {
        $("#availability_from__" + availability_from).css("border", "1px solid #cf3c63");
        $("#availability_from__" + availability_from).focus();
        availability_from_error++;
    } else {
        $("#availability_from__" + availability_from).css("border", "1px solid #9e9e9e");
    }
    availability_from++;

});

var availability_to = 1;
var availability_to_error = 0;
$(".availability_to").each(function () {
    if ($(this).val().trim() == "") {
        $("#availability_to__" + availability_to).css("border", "1px solid #cf3c63");
        $("#availability_to__" + availability_to).focus();
        availability_to_error++;
    } else {
        $("#availability_to__" + availability_to).css("border", "1px solid #9e9e9e");
    }
    availability_to++;

});

var availability_time_from = 1;
var availability_time_from_error = 0;
$(".availability_time_from").each(function () {
    var id=$(this).attr("id");
    if ($(this).val().trim() == "") {
        $("#"+id).css("border", "1px solid #cf3c63");
        $("#"+id).focus();
        availability_time_from_error++;
    } else {
        $("#"+id).css("border", "1px solid #9e9e9e");
    }
    availability_time_from++;

});

var availability_time_to = 1;
var availability_time_to_error = 0;
$(".availability_time_to").each(function () {
    var id=$(this).attr("id");
    if ($(this).val().trim() == "") {
        $("#"+id).css("border", "1px solid #cf3c63");
        $("#"+id).focus();
        availability_time_to_error++;
    } else {
        $("#"+id).css("border", "1px solid #9e9e9e");
    }
    availability_time_to++;

});


var no_of_bookings = 1;
var no_of_bookings_error = 0;
$(".no_of_bookings").each(function () {
     var id=$(this).attr("id");
    if ($(this).val().trim() == "") {
        $("#"+id).css("border", "1px solid #cf3c63");
        $("#"+id).focus();
        no_of_bookings_error++;
    } else {
        $("#"+id).css("border", "1px solid #9e9e9e");
    }
    no_of_bookings++;

});

if (activity_inclusions.trim() == "")
{
    $("#cke_activity_inclusions").css("border", "1px solid #cf3c63");

} else

{
    $("#cke_activity_inclusions").css("border", "1px solid #9e9e9e");
}
if (activity_exclusions.trim() == "")
{
    $("#cke_activity_exclusions").css("border", "1px solid #cf3c63");

} else

{
    $("#cke_activity_exclusions").css("border", "1px solid #9e9e9e");
}
if (activity_description.trim() == "")
{
    $("#cke_activity_description").css("border", "1px solid #cf3c63");

} else

{
    $("#cke_activity_description").css("border", "1px solid #9e9e9e");
}
if (activity_cancellation.trim() == "")
{
    $("#cke_activity_cancellation").css("border", "1px solid #cf3c63");

} else

{
    $("#cke_activity_cancellation").css("border", "1px solid #9e9e9e");
}
if (activity_terms_conditions.trim() == "")
{
    $("#cke_activity_terms_conditions").css("border", "1px solid #cf3c63");

} else

{
    $("#cke_activity_terms_conditions").css("border", "1px solid #9e9e9e");
}





if(activity_type.trim() == "0")
{
    $("#activity_type").focus();
}
else if(activity_name.trim() == "")
{
    $("#activity_name").focus();
}
else if(supplier_name=="0")
{
  $("#supplier_name").parent().find(".select2-selection").focus();  
} 
else if(activity_location.trim()=="")
{
  $("#activity_location").focus();  
}
else if(activity_country=="0")
{
  $("#activity_country").parent().find(".select2-selection").focus();  
} 
else if(activity_city=="0")
{
  $("#activity_city").parent().find(".select2-selection").focus();  
}
else if(activity_duration.trim()=="")
{
  $("#activity_duration").focus();  
}
/*else if(period_operation_from.trim()=="")
{
  $("#period_operation_from").focus();  
}
else if(period_operation_to.trim()=="")
{
  $("#period_operation_to").focus();  
}*/
else if(validity_operation_from.trim()=="")
{
  $("#validity_operation_from").focus();  
}
else if(validity_operation_to.trim()=="")
{
  $("#validity_operation_to").focus();  
}
else if(activity_time_from.trim()=="")
{
  $("#activity_time_to").focus();  
}
else if(activity_time_from.trim()=="")
{
  $("#activity_time_to").focus();  
} 
else if (!is_all_days) {
    $("input[name='is_all_days']").focus();
} 
else if (!week_monday) {
    $("input[name='week_monday']").focus();
} 
else if (!week_tuesday) {
    $("input[name='week_tuesday']").focus();
} 
else if (!week_wednesday) {
    $("input[name='week_wednesday']").focus();
} 
else if (!week_thursday) {
    $("input[name='week_thursday']").focus();
}
else if (!week_friday) {
    $("input[name='week_friday']").focus();
} 
else if (!week_saturday) {
    $("input[name='week_saturday']").focus();
} 
else if (!week_sunday) {
    $("input[name='week_sunday']").focus();
}
else if(activity_currency=="0")
{
  $("#activity_currency").parent().find(".select2-selection").focus();  
} 
else if(!adults_age && !children_age && !infant_age)
{
    alert("Please allow atleast one pax type");
    window.location.hash = '#allowed_pax_div';
}
else if(adults_age && adults_min_age.trim()=="")
{
    $("#adults_min_age").focus(); 
}
else if(adults_age && adults_max_age.trim()=="")
{
    $("#adults_max_age").focus(); 
}
else if(adults_age && adult_min_pax_error>0)
{

}
else if(adults_age && adult_max_pax_error>0)
{

}
else if(adults_age && adult_pax_price_error>0)
{

}
else if(children_age && child_min_age.trim()=="")
{
    $("#child_min_age").focus(); 
}
else if(children_age && child_max_age.trim()=="")
{
    $("#child_max_age").focus(); 
}
else if(children_age && child_min_pax_error>0)
{

}
else if(children_age && child_max_pax_error>0)
{

}
else if(children_age && child_pax_price_error>0)
{

}
else if(infant_age && infant_min_age.trim()=="")
{
    $("#infant_min_age").focus(); 
}
else if(infant_age && infant_max_age.trim()=="")
{
    $("#infant_max_age").focus(); 
}
else if(infant_age && infant_min_pax_error>0)
{

}
else if(infant_age && infant_max_pax_error>0)
{

}
else if(infant_age && infant_pax_price_error>0)
{

}
else if(availability_from_error>0)
{

}
else if(availability_to_error>0)
{

}
else if(availability_time_from_error>0)
{

}
else if(availability_time_to_error>0)
{

}
else if(no_of_bookings_error>0)
{

}

else if(activity_description.trim()=="")
{
  $("#cke_activity_description").attr("tabindex","100").focus();  
}
else if(activity_cancellation.trim()=="")
{
  $("#cke_activity_cancellation".attr("tabindex","100")).focus();  
}
else if(activity_terms_conditions.trim()=="")
{
  $("#cke_activity_terms_conditions").attr("tabindex","100").focus();  
}

else
{
    $("#update_activity").prop("disabled", true);
    var formdata=new FormData($("#activity_form")[0]);

    formdata.append("activity_inclusions",activity_inclusions);
    formdata.append("activity_exclusions",activity_exclusions);
    formdata.append("activity_description",activity_description);
    formdata.append("activity_cancellation",activity_cancellation);
    formdata.append("activity_terms_conditions",activity_terms_conditions);
    $.ajax({
        url:"{{route('update-activity')}}",
        enctype:"multipart/form-data",
        type:"POST",
        data:formdata,
        contentType: false,
        processData: false,
        success:function(response)
        {
            if (response.indexOf("exist") != -1)

            {

                swal("Already Exist!",
                    "Activity already exists");

            } else if (response.indexOf("success") != -1)

            {

                swal({
                    title: "Success",
                    text: "Activity Updated Successfully !",
                    type: "success"
                },

                function () {

                    location.reload();

                });

            } else if (response.indexOf("fail") != -1)

            {

                swal("ERROR", "Activity cannot be updated right now! ");

            }
            $("#update_activity").prop("disabled", false);

        }
    });
}
});
    $(document).on("click","#discard_activity",function()
    {
        window.history.back();

    });
</script>
<script>
    $(document).on("change",".weekdays_yes,.weekdays_no",function()
    {
        var count=0;
        $(".weekdays_yes").each(function()
        {
            if($(this).is(":checked"))
            {
               count++;
            }
        });

        if(count==7)
        {
            $("input[name=is_all_days][value='Yes']").prop("checked",true);
        }
        else
        {
             $("input[name=is_all_days][value='No']").prop("checked",true);
        }

    });

      $(document).on("change",".radio_allowed",function()
    {
        var radio_name=$(this).attr("name");

        if(radio_name=="for_all_ages")
        {
                  if($(this).val()=="No")
                  {
                    $("#"+radio_name+"_div").show();
                }
                else
                {
                 $("#"+radio_name+"_div").hide(); 
             }
         }
         else
         {
           if($(this).val()=="Yes")
           {
            $("#"+radio_name+"_div").show();
        }
        else
        {
         $("#"+radio_name+"_div").hide(); 
        } 
        }
        
        });
</script>
</body>


</html>
