@include('mains.includes.top-header')
<style>
.iti-flag {
width: 20px;
height: 15px;
box-shadow: 0px 0px 1px 0px #888;
background-image: url("{{asset('assets/images/flags.png')}}") !important;
background-repeat: no-repeat;
background-color: #DBDBDB;
background-position: 20px 0
}
div#cke_1_contents {
height: 250px !important;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Restaurant Menu</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Masters</li>
                                            <li class="breadcrumb-item active" aria-current="page">Add New Restaurant Menu Category
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            <!--  <div class="right-title">
                                <div class="dropdown">
                                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
                                    class="mdi mdi-dots-horizontal"></i></button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
                                        <div class="dropdown-divider"></div>
                                        <button type="button" class="btn btn-rounded btn-success">Submit</button>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    @if($rights['add']==1)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Add New Restaurant Menu Category</h4>
                                </div>
                                <div class="box-body">
                                    <form class="package_form" action="javascript:void()" method="POST" id="menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <label>Menu Category Name <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Name" id="restaurant_menu_category_name" name="restaurant_menu_category_name" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <label>Menu Category Description</label>
                                                    <textarea class="form-control" placeholder="Description" id="restaurant_menu_category_description" name="restaurant_menu_category_description"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-md-12">
                                                <button type="button"  id="save_restaurant_menu_category" class="btn btn-rounded btn-primary mr-10 pull-right">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">View Restaurant Menu Category</h4>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example1" class="table table-bordered table-striped datatable">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NAME</th>
                                                  <th>DESCRIPTION</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($fetch_restaurant_menu_category as $restaurant_menu_category)
                                            <tr>
                                                <td>{{$restaurant_menu_category->restaurant_menu_category_id}}</td>
                                                <td id="restaurant_menu_category_name_{{$restaurant_menu_category->restaurant_menu_category_id}}"> {{$restaurant_menu_category->restaurant_menu_category_name}}</td>
                                                <td id="restaurant_menu_category_description_{{$restaurant_menu_category->restaurant_menu_category_id}}"><?php echo nl2br($restaurant_menu_category->restaurant_menu_category_description); ?></td>
                                                 <td><button id="edit_{{$restaurant_menu_category->restaurant_menu_category_id}}" class="btn btn-default btn-rounded edit_restaurant"><i class="fa fa-pencil"></i></button></td>
                                            </tr>

                                            @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <h4 class="text-danger">No rights to access this page</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('mains.includes.footer')
    @include('mains.includes.bottom-footer')
    <div id="editModal" class="modal fade" role="dialog" style="z-index: 9999">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Restaurant Menu Category</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="package_form" action="javascript:void()" method="POST" id="edit_menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10" >
                                        <h5 style="color:red" id="edit_restaurant_menu_category_error"></h5>
                                        </div>
                                        <div class="row mb-10">
                                           <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                      <input type="hidden" id="edit_restaurant_menu_category_id" name="restaurant_menu_category_id">
                                                    <label>Restaurant Menu Category Name <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Name" id="edit_restaurant_menu_category_name" name="restaurant_menu_category_name" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label>Menu Category Description</label>
                                                    <textarea class="form-control" placeholder="Description" id="edit_restaurant_menu_category_description" name="restaurant_menu_category_description"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
            </div>
        </div>
        
      </div>
      <div class="modal-footer">
        
         <button type="button"  id="update_restaurant_menu_category" class="btn btn-rounded btn-primary mr-10 ">Update</button>
        <button type="button" class="btn btn-default mr-10 pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <script>
    $(document).ready(function()
    {
        $(".datatable").DataTable();
    });

    $(document).on("click","#save_restaurant_menu_category",function()
    {
    var restaurant_menu_category_name=$("#restaurant_menu_category_name").val();

    if(restaurant_menu_category_name.trim()=="")
    {
    $("#restaurant_menu_category_name").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#restaurant_menu_category_name").css("border","1px solid #9e9e9e");
    }
    
    
    if(restaurant_menu_category_name.trim()=="")
    {
    $("#restaurant_menu_category_name").focus();
    }
    else
    {
    $("#save_restaurant_menu_category").prop("disabled",true);
    var formdata=new FormData($("#menu_form")[0]);
    $.ajax({
    url:"{{route('restaurant-menu-category-insert')}}",
    data: formdata,
    type:"POST",
    processData: false,
    contentType: false,
    success:function(response)
    {
    if(response.indexOf("exist")!=-1)
    {
    swal("Already Exist!", "Restaurant Menu Category Name already exists");
    }
    else if(response.indexOf("success")!=-1)
    {
    swal({title:"Success",text:"Restaurant Menu Category Created Successfully !",type: "success"},
    function(){
    location.reload();
    });
    }
    else if(response.indexOf("fail")!=-1)
    {
    swal("ERROR", "Restaurant Menu Category cannot be inserted right now! ");
    }
    $("#save_restaurant_menu_category").prop("disabled",false);
    }
    });
    }
    });
    </script>
<script> 
    $(document).on("click",".edit_restaurant",function()
    {
        var id=this.id;

        var actual_id=id.split("_");
        var restaurant_menu_category_name=$("#restaurant_menu_category_name_"+actual_id[1]).text();
           var restaurant_menu_category_description=$("#restaurant_menu_category_description_"+actual_id[1]).text();
        $("#edit_restaurant_menu_category_name").val(restaurant_menu_category_name);
         $("#edit_restaurant_menu_category_description").val(restaurant_menu_category_description);
        $("#edit_restaurant_menu_category_id").val(actual_id[1]);


        $("#editModal").modal("show"); 
    });

    $(document).on("click","#update_restaurant_menu_category",function()
    {
    var restaurant_menu_category_name=$("#edit_restaurant_menu_category_name").val();
    var error=0;
    var errors_array=[];

    if(restaurant_menu_category_name.trim()=="")
    {
        errors_array.push("<li>Please Enter Restaurant Menu Category Name</li>");
            error++;

    $("#edit_restaurant_menu_category_name").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#edit_restaurant_menu_category_name").css("border","1px solid #9e9e9e");
    }

   
    if(error>0)
    {

            $("#edit_restaurant_menu_category_error").html("<ul>"+errors_array.join("")+"</ul>");
            $("#edit_restaurant_menu_category_error").focus();
    }
    else
    {
    $("#update_restaurant_menu_category").prop("disabled",true);
            var formdata=new FormData($("#edit_menu_form")[0]);
            $.ajax({
                url:"{{route('restaurant-menu-category-update')}}",
                data: formdata,
                type:"POST",
                processData: false,
                contentType: false,
                success:function(response)
                {
                    if(response.indexOf("exist")!=-1)
                    {
                        swal("Already Exist!", "Restaurant Menu Category already exists");
                    }
                    else if(response.indexOf("success")!=-1)
                    {
                        swal({title:"Success",text:"Restaurant Menu Category Update Successfully !",type: "success"},
                            function(){
                                location.reload();
                            });
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        swal("ERROR", "Restaurant Menu Category cannot be updated right now!");
                    }
                    $("#update_restaurant_menu_category").prop("disabled",false);
                }
            });
    }
    });
    </script>
</body>
</html>