<?php
use App\Http\Controllers\LoginController;
?>
@include('mains.includes.top-header')
<style>
.iti-flag {
width: 20px;
height: 15px;
box-shadow: 0px 0px 1px 0px #888;
background-image: url("{{asset('assets/images/flags.png')}}") !important;
background-repeat: no-repeat;
background-color: #DBDBDB;
background-position: 20px 0
}
div#cke_1_contents {
height: 250px !important;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Cities</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Masters</li>
                                            <li class="breadcrumb-item active" aria-current="page">Edit/View Cities
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            <!--  <div class="right-title">
                                <div class="dropdown">
                                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
                                    class="mdi mdi-dots-horizontal"></i></button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
                                        <div class="dropdown-divider"></div>
                                        <button type="button" class="btn btn-rounded btn-success">Submit</button>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    @if($rights['edit_delete']==1 && $rights['view']==1)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Edit/View Cities</h4>
                                </div>
                                <div class="box-body">
                                        <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <label>Country<span class="asterisk">*</span></label>
                                                
                                                    <select name="country" id="country" class="form-control select2">
                                                        <option value="0">Select Country</option>
                                                        @foreach($fetch_countries as $country)
                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                        @endforeach
                                                        
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">View Cities</h4>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive" id="cities_div">
    
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <h4 class="text-danger">No rights to access this page</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('mains.includes.footer')
    @include('mains.includes.bottom-footer')
        <div id="editModal" class="modal fade" role="dialog" style="z-index: 9999">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Cities</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="package_form" action="javascript:void()" method="POST" id="edit_menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10" >
                                        <h5 style="color:red" id="edit_cities_error"></h5>
                                        </div>
                                        <div class="row mb-10">
                                           <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                      <input type="hidden" id="cities_id" name="cities_id">
                                                    <label>City Name <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Name" id="cities_name" name="cities_name" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
            </div>
        </div>
        
      </div>
      <div class="modal-footer">
        
         <button type="button"  id="update_cities" class="btn btn-rounded btn-primary mr-10 ">Update</button>
        <button type="button" class="btn btn-default mr-10 pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <script>
    $(document).ready(function()
    {
        $(".datatable").DataTable();
        $(".select2").select2();
    });

    $(document).on("change","#country",function()
    {
        var country_id=$(this).val();
        if(country_id!=0)
        {
         
              
              

               setTimeout(function()
               {
                    $("#cities_div").html("<h4><b>LOADING..</b></h4>");
                },0);
               setTimeout(function()
               {
                   $("#cities_div").html("<h4><b>LOADING....</b></h4>");
                },300);
               setTimeout(function()
               {
                   $("#cities_div").html("<h4><b>LOADING.....</b></h4>");
                },600);
                 setTimeout(function()
               {
                    $("#cities_div").html("<h4><b>LOADING.......</b></h4>");
                },900);
                   setTimeout(function()
               {
                 $("#cities_div").html("<h4><b>LOADING..........</b></h4>");
                       $.ajax({
            url:"{{route('get-all-cities-tab')}}",
            data: {'country_id':country_id},
            type:"GET",
            success:function(response)
            {
                $("#cities_div").html("<h4><b>LOADING..........</b></h4>");
                $("#cities_div").html(response);
                 $(".datatable").DataTable();
        
            }
        });
                   },1000);
        

        }
        else
        {
            $("#cities_div").html("");
        }
    });
    </script>
    <script> 
    $(document).on("click",".edit_cities",function()
    {
        var id=this.id;

        var actual_id=id.split("_");
        var cities_name=$("#cities_name_"+actual_id[1]).text();
        $("#cities_name").val(cities_name);
        $("#cities_id").val(actual_id[1]);


        $("#editModal").modal("show"); 
    });

    $(document).on("click","#update_cities",function()
    {
        var cities_id=$("#cities_id").val();
    var cities_name=$("#cities_name").val();
    var error=0;
    var errors_array=[];

    if(cities_name.trim()=="")
    {
        errors_array.push("<li>Please Enter City Name</li>");
            error++;

    $("#cities_name").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#cities_name").css("border","1px solid #9e9e9e");
    }

   
    if(error>0)
    {

            $("#edit_cities_error").html("<ul>"+errors_array.join("")+"</ul>");
            $("#edit_cities_error").focus();
    }
    else
    {
    $("#update_cities").prop("disabled",true);
            var formdata=new FormData($("#edit_menu_form")[0]);
            $.ajax({
                url:"{{route('cities-update')}}",
                data: formdata,
                type:"POST",
                processData: false,
                contentType: false,
                success:function(response)
                {
                    if(response.indexOf("exist")!=-1)
                    {
                        swal("Already Exist!", "City already exists");
                    }
                    else if(response.indexOf("success")!=-1)
                    {
                        $("#cities_name_"+cities_id).text(cities_name);
                        swal({title:"Success",text:"City Name Update Successfully !",type: "success"},
                            function(){
                            
                            });
                        $("#editModal").modal("hide"); 
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        swal("ERROR", "City cannot be updated right now!");
                    }
                    $("#update_cities").prop("disabled",false);
                }
            });
    }
    });

    $(document).on("click",".delete_cities",function()
{
 var id=this.id;
        var actual_id=id.split("_");
       var cities_id=actual_id[1];

swal({
title: "Are you sure?",
text: "You will not be able to recover this city !",
type: "warning",
showCancelButton: true,
confirmButtonColor: "#DD6B55",
confirmButtonText: "Yes, delete it!",
cancelButtonText: "No, cancel!",
closeOnConfirm: false,
closeOnCancel: false
}, function(isConfirm) {
if (isConfirm) {
    $.ajax({
        url:"{{route('cities-delete')}}",
        data: {"_token":"{{csrf_token()}}","cities_id":cities_id},
        type:"POST",
        success:function(response)
        {

            if(response.indexOf("success")!=-1)
            {
                  $("#cities_id_"+cities_id).remove();
                swal("Deleted!", "Selected city has been deleted.", "success");
            }
            else if(response.indexOf("fail")!=-1)
            {
                swal("ERROR", "City cannot be deleted right now!");
            }

        }
    });


} else {
    swal("Cancelled", "Selected city is safe :)", "error");
}
});
});
</script>


</body>
</html>