@include('mains.includes.top-header')
<style>
    header.main-header {
          background: url("{{ asset('assets/images/color-plate/theme-purple.jpg') }}");
    }

    .iti-flag {
        width: 20px;
        height: 15px;
        box-shadow: 0px 0px 1px 0px #888;
        background-image: url("flags.png") !important;
        background-repeat: no-repeat;
        background-color: #DBDBDB;
        background-position: 20px 0
    }

    div#cke_1_contents {
        height: 250px !important;
    }

    table#calendar-demo {
        width: 100%;
        height: 275px !important;
        min-height: 275px !important;
        overflow: hidden;
    }

    .calendar-wrapper.load {
        width: 100%;
        height: 276px;
    }

    .calendar-date-holder .calendar-dates .date.month a {
        display: block;
        padding: 17px 0 !important;
    }

    .calendar-date-holder {
        width: 100% !important;
    }

    section.calendar-head-card {
        display: none;
    }

    .calendar-container {
        border: 1px solid #cccccc;
        height: 276px !important;
    }

    img.plus-icon {
        margin: 0 2px;
        display: inline !important;
    }

    @media screen and (max-width:400px) {
        .calendar-date-holder .calendar-dates .date a {
            text-decoration: none;
            display: block;
            color: inherit;
            padding: 3px !important;
            margin: 1px;
            outline: none;
            border: 2px solid transparent;
            transition: all .3s;
            -o-transition: all .3s;
            -moz-transition: all .3s;
            -webkit-transition: all .3s;
        }
    }
      div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
div#loaderModal {
    background: #0000005c;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

    <div class="wrapper">

        @include('mains.includes.top-nav')

        <div class="content-wrapper">

            <div class="container-full clearfix position-relative">

                @include('mains.includes.nav')

                <div class="content">

    <div class="content-header">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="page-title">Food / Drinks</h3>
                <div class="d-inline-block align-items-center">
                    <nav>
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                            <li class="breadcrumb-item" aria-current="page">Dashboard</li>
                            <li class="breadcrumb-item" aria-current="page">Food / Drinks</li>
                            <li class="breadcrumb-item active" aria-current="page">Create New Food / Drinks
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
           <!--  <div class="right-title">
                <div class="dropdown">
                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
                            class="mdi mdi-dots-horizontal"></i></button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Restaurant</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
                        <div class="dropdown-divider"></div>
                        <button type="button" class="btn btn-rounded btn-success">Submit</button>
                    </div>
                </div>
            </div> -->
        </div>
    </div>

  @if($rights['add']==1)
    <div class="row">
        <div class="col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h4 class="box-title">Create Food / Drinks</h4>
                </div>
                <div class="box-body">
                    <form id="restaurant_form" encytpe="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row mb-10">
                        <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                        <label for="restaurant">RESTAURANT<span class="asterisk">*</span></label>
                         <select id="restaurant" name="restaurant" class="form-control select2">
                            <option value="0">--SELECT RESTAURANT--</option>
                            @foreach($fetch_restaurants as $restaurant)
                            <option value="{{$restaurant->restaurant_id}}">{{$restaurant->restaurant_name}}</option>
                            @endforeach
                        </select>
                        </div>
                        </div>
                         <div class="col-sm-6 col-md-6">
                        <div class="form-group">
                        <label for="menu_category">CATEGORY<span class="asterisk">*</span></label>
                         <select id="menu_category" name="menu_category" class="form-control select2">
                            <option value="0">--SELECT CATEGORY--</option>
                            @foreach($menu_categories as $category)
                            <option value="{{$category->restaurant_menu_category_id}}">{{$category->restaurant_menu_category_name}}</option>
                            @endforeach
                        </select>
                        </div>
                        </div>
                        </div> 

                    <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="food_name">FOOD / DRINKS NAME <span class="asterisk">*</span></label>
                                <input type="text" id="food_name" name="food_name" class="form-control" placeholder="FOOD / DRINKS NAME">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                             <div class="form-group">
                                <label for="food_price">FOOD / DRINKS PRICE<span class="asterisk">*</span></label>
                                <input type="text" id="food_price" name="food_price" class="form-control" placeholder="FOOD / DRINKS PRICE" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">
                            </div>
                        </div>
                    </div>
                     <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="food_discounted_price">FOOD / DRINKS DISCOUNTED PRICE</label>
                                <input type="text" id="food_discounted_price" name="food_discounted_price" class="form-control" placeholder="DISCOUNTED PRICE" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">    
                                        <label for="food_unit">FOOD / DRINKS UNIT <span class="asterisk">*</span></label>
                                <input type="text" id="food_unit" name="food_unit" class="form-control" placeholder="For example: L, ml, Kg, g, plate">
                                    </div>
                        </div>
                    </div>
                     <div class="row mb-10">
                        <div class="col-sm-12 col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="food_unit">FOOD / DRINKS PACKAGE COUNT <span class="asterisk">*</span></label>
                                <input type="text" id="food_package_count" name="food_package_count" class="form-control" placeholder="1" value="1" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">
                                    </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-3">
                            <div class="form-group">
                                <br>
                                        <label for="available_for_delivery_label">AVAILABLE FOR DELIVERY</label>
                                        &nbsp;&nbsp;<input type="checkbox" name="food_available_for_delivery" id="food_available_for_delivery"  value="yes" class="checkbox-col-primary">
                                          <label for="food_available_for_delivery">&nbsp;</label>
                            </div>

                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-3">
                            <div class="form-group">
                                <br>
                                        <label for="available_for_delivery_label">FEATURED</label>
                                       
                                        &nbsp;&nbsp;<input type="checkbox" name="food_featured" id="food_featured"  value="yes" class="checkbox-col-primary">
                                          <label for="food_featured">&nbsp;</label>
                            </div>

                        </div>
                    </div>

                    <div class="row mb-10">
                         <div class="col-sm-12 col-md-12 col-lg-6">
                            <div class="form-group">
                                <label for="food_unit">FOOD / DRINKS INGREDIENTS ( , )</label>
                                <input type="text" id="food_ingredients" name="food_ingredients" class="form-control" placeholder="FOOD / DRINKS INGREDIENTS ">
                                    </div>
                        </div>
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group" style="display: none">
                                <label>FOOD / DRINKS AVAILABILITY<span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group date">
                                                <input type="text" placeholder="FROM"
                                                    class="form-control pull-right datepicker" id="validity_operation_from" name="validity_operation_from" readonly="readonly" value="{{date('Y-m-d')}}">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <div class="input-group date">
                                                <input type="text" placeholder="TO"
                                                    class="form-control pull-right datepicker" id="validity_operation_to" name="validity_operation_to" readonly="readonly" value="{{date('Y-m-d')}}">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                            <!-- /.input group -->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                </div>
                  
                     <div class="row mb-10" style="display: none">
                         <div class="col-sm-12 col-md-12">
                                        <div class="form-group">
                                              <label for="food_description">FOOD / DRINKS DESCRIPTION</label>
                                            <textarea class="form-control" id="food_description" name="food_description"></textarea>
                                        </div>
                                
                        </div>
                    </div>
                   


                    <div class="col-sm-12 col-md-12 col-lg-6">
                            <label>FOOD / DRINKS IMAGES</label>
                        <div class="input-group control-group increment" id="increment">
          <input type="file" name="upload_ativity_images[]" class="form-control upload_ativity_images">
          <div class="input-group-btn"> 
            <button class="btn btn-primary add_more_food_image" type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
          </div>
        </div>
        <div class="clone hide" style="display:none" id="clone">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="file" name="upload_ativity_images[]" class="form-control upload_ativity_images">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove_more_food_image" type="button"><i class="glyphicon glyphicon-remove"></i>  Remove</button>
            </div>
          </div>
        </div>
        <br>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                    </div>
                     <div id="previewImg" class="row">
                        </div>
                </div>
                <div class="row mb-10">
                    <div class="col-md-12">
                        <div class="box-header with-border"
                            style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
                            <button type="button" id="save_food" class="btn btn-rounded btn-primary mr-10">Save</button>
                            <button type="button" id="discard_food" class="btn btn-rounded btn-primary">Discard</button>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
 @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif

</div>
</div>
</div>
</div>
   @include('mains.includes.footer')

        @include('mains.includes.bottom-footer')
        <div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
                <script type="text/javascript">
    $(document).ready(function() {
       $(document).on("click",".add_more_food_image",function(){ 
        var id=$(this).parent().parent().attr("id");
        var actual_id=id.split("increment");

        if(actual_id[1]=="")
        {
             var html = $("#clone").html();
          $("#increment").after(html);
        }
        else
        {
              var html = $("#clone"+actual_id[1]).html();
          $("#"+id).after(html);

        }
         
      });
      $("body").on("click",".remove_more_food_image",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>
<script>

$(document).ready(function()
{
    // document.getElementById('upload_ativity_images').addEventListener('change', handleFileSelect, false);
     CKEDITOR.replace('food_description');
    $('.select2').select2();
    var date = new Date();
    date.setDate(date.getDate());

    $('#validity_operation_from').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#validity_operation_from").datepicker("getDate");
        var date_to = $("#validity_operation_to").datepicker("getDate");

        if(!date_to)
        {
            $('#validity_operation_to').datepicker("setDate",date_from);
        }
        else if(date_to<date_from)
        {
            $('#validity_operation_to').datepicker("setDate",date_from);
        }
    });

    $('#validity_operation_to').datepicker({
        autoclose:true,
        todayHighlight: true,
        format: 'yyyy-mm-dd',
        startDate:date
    }).on('changeDate', function (e) {
        var date_from = $("#validity_operation_from").datepicker("getDate");
        var date_to = $("#validity_operation_to").datepicker("getDate");

        if(!date_from)
        {
            $('#validity_operation_from').datepicker("setDate",date_to);
        }
        else if(date_to<date_from)
        {
            $('#validity_operation_from').datepicker("setDate",date_to);
        }
    });
    
});

</script>
<script>
    $(document).on("click","#save_food",function()
    {
        var restaurant=$("#restaurant").val();
        var menu_category=$("#menu_category").val();
        var food_name=$("#food_name").val();
        var food_price=$("#food_price").val();
        var food_unit=$("#food_unit").val();
        var food_package_count=$("#food_package_count").val();
        var validity_operation_from=$("#validity_operation_from").val();
        var validity_operation_to=$("#validity_operation_to").val();
          var food_description=CKEDITOR.instances.food_description.getData();


        if (restaurant.trim() == "0")
        {
            $("#restaurant").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

        } else

        {
            $("#restaurant").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
        }
        if (menu_category.trim() == "0")
        {
            $("#menu_category").css("border", "1px solid #cf3c63");

        } else

        {
            $("#menu_category").css("border", "1px solid #9e9e9e");
        }

        if (food_name.trim() == "")
        {
            $("#food_name").css("border", "1px solid #cf3c63");

        } else

        {
            $("#food_name").css("border", "1px solid #9e9e9e");
        }

        if (food_price.trim() == "")
        {
            $("#food_price").css("border", "1px solid #cf3c63");

        } else

        {
            $("#food_price").css("border", "1px solid #9e9e9e");
        }

  if (food_unit.trim() == "")
       {
         $("#food_unit").css("border", "1px solid #cf3c63");

     } else

     {
      $("#food_unit").css("border", "1px solid #9e9e9e");
  }
   if (food_package_count.trim() == "")
       {
         $("#food_package_count").css("border", "1px solid #cf3c63");

     } else

     {
      $("#food_package_count").css("border", "1px solid #9e9e9e");
  }

// if (validity_operation_from.trim() == "")
// {
//     $("#validity_operation_from").css("border", "1px solid #cf3c63");

// } else

// {
//     $("#validity_operation_from").css("border", "1px solid #9e9e9e");
// }
// if (validity_operation_to.trim() == "")
// {
//     $("#validity_operation_to").css("border", "1px solid #cf3c63");

// } else

// {
//     $("#validity_operation_to").css("border", "1px solid #9e9e9e");
// }

if(restaurant.trim() == "0")
{
    $("#restaurant").parent().find(".select2-selection").focus()
}
else if(menu_category.trim() == "0")
{
    $("#menu_category").parent().find(".select2-selection").focus()
}
else if(food_name.trim()=="")
{
  $("#food_name").focus();  
}
else if(food_price.trim()=="")
{
  $("#food_price").focus();  
}
else if(food_unit.trim()=="")
{
  $("#food_unit").focus();  
}
else if(food_package_count.trim()=="")
{
  $("#food_package_count").focus();  
}
// else if(validity_operation_from.trim()=="")
// {
//   $("#validity_operation_from").focus();  
// }
// else if(validity_operation_to.trim()=="")
// {
//   $("#validity_operation_to").focus();  
// }
else
{
    $("#save_restaurant").prop("disabled", true);
    $("#loaderModal").modal("show");
    var formdata=new FormData($("#restaurant_form")[0]);
     formdata.append("food_description",food_description);
    $.ajax({
        url:"{{route('insert-food')}}",
        enctype:"multipart/form-data",
        type:"POST",
        data:formdata,
        contentType: false,
        processData: false,
        success:function(response)
        {
            if (response.indexOf("exist") != -1)
            {
                swal("Already Exist!",
                    "Food / Drinks already exists");

            } else if (response.indexOf("success") != -1)

            {
                swal({
                    title: "Success",
                    text: "Food / Drinks Created Successfully !",
                    type: "success"
                },

                function () {

                    location.reload();

                });
            } else if (response.indexOf("fail") != -1)
            {
                swal("ERROR", "Food / Drinks cannot be inserted right now! ");
            }
            $("#save_food").prop("disabled", false);
             $("#loaderModal").modal("hide");
        }
    });
}
});
    $(document).on("click","#discard_food",function()
    {
        window.history.back();

    });
</script>
<script>
    $(document).on("change",".weekdays_yes,.weekdays_no",function()
    {
        var count=0;
        $(".weekdays_yes").each(function()
        {
            if($(this).is(":checked"))
            {
               count++;
            }
        });

        if(count==7)
        {
            $("input[name=is_all_days][value='Yes']").prop("checked",true);
        }
        else
        {
             $("input[name=is_all_days][value='No']").prop("checked",true);
        }

    });

     $(document).on("change",".upload_ativity_images",function()
        {      
            var imageSize = this;
            var count=0;
            for (var i = 0; i < imageSize.files.length; i++) {
                var image_Size = imageSize.files[i].size;
                    if(image_Size > 250000)
                    {
                       alert("Try to upload files less than 250KB!"); 
                        $(this).val("");
                        break;
                       count++;
                   }
               }
      });
</script>
</body>


</html>
