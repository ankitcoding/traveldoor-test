<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    div#cke_1_contents {

        height: 250px !important;

    }
div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('mains.includes.nav')

	<div class="content">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="page-title">Service Management</h3>
				<div class="d-inline-block align-items-center">
					<nav>
						<ol class="breadcrumb">
							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
							<li class="breadcrumb-item" aria-current="page">Dashboard</li>
							<li class="breadcrumb-item active" aria-current="page">Service Management
							</li>
						</ol>
					</nav>
				</div>
			</div>
			<!-- <div class="right-title">
				<div class="dropdown">
					<button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
							class="mdi mdi-dots-horizontal"></i></button>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
						<a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
						<a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
						<a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
						<div class="dropdown-divider"></div>
						<button type="button" class="btn btn-rounded btn-success">Submit</button>
					</div>
				</div>
			</div> -->
		</div>
	</div>


	<div class="row">


@if($rights['add']==1 || $rights['view']==1)
		<div class="col-12">
			<div class="box">

				<div class="box-body">
					<!-- Nav tabs -->
					<ul class="nav nav-pills mb-20">
						<li class=" nav-item"> <a href="#navpills-hotel" id="navpills-hotel-link" class="nav-link show active" data-toggle="tab"
								aria-expanded="false">Hotel</a> </li>
						<li class="nav-item"> <a href="#navpills-activity" id="navpills-activity-link" class="nav-link show" data-toggle="tab"
								aria-expanded="false">Activity</a> </li>
						<!-- <li class="nav-item"> <a href="#navpills-3" class="nav-link show" data-toggle="tab"
								aria-expanded="true">Transportation</a> </li> -->
								<li class="nav-item"> <a href="#navpills-guide" id="navpills-guide-link" class="nav-link show" data-toggle="tab"
								aria-expanded="true">Guides</a> </li>
								<li class="nav-item"> <a href="#navpills-sightseeing" id="navpills-sightseeing-link" class="nav-link show" data-toggle="tab"
								aria-expanded="true">Sightseeing</a> </li>
								<li class="nav-item"> <a href="#navpills-transfer" id="navpills-transfer-link" class="nav-link show" data-toggle="tab"
								aria-expanded="true">Transfers</a> </li>
								<li class="nav-item"> <a href="#navpills-driver" id="navpills-driver-link" class="nav-link show" data-toggle="tab"
								aria-expanded="true">Drivers</a> </li>
					</ul>
					<!-- Tab panes -->
					<div class="tab-content">
						<div id="navpills-hotel" class="tab-pane show active">
							<div class="row">
									@if($rights['add']==1)
								<div class="col-sm-6 col-md-3">
									<div class="form-group">

										<a href="{{route('create-hotel')}}"><button type="button"
												class="btn btn-rounded btn-success">Create New Hotel</button></a>
									</div>
								</div>
								@endif

								@if($rights['view']==1)		
								<div class="col-12">
									<div class="box">

										<!-- /.box-header -->
										<div class="box-body">
											<div class="table-responsive">
												<table  id="hotel_table" class="table table-bordered table-striped">
													<thead>
														<tr>
															<th>S. No.</th>
															<th style="display:none">Hotel ID</th>
															<th>Hotel Name</th>
															<th>Hotel Address</th>
															<th>City</th>
															<th>Country</th>
															<th>Supplier Name</th>
															@if($rights['edit_delete']==1)
															<th>Status</th>
															<th>Approval</th>
																<th>Best Seller</th>
															@endif
															@if($rights['edit_delete']==1 || $rights['view']==1)
															<th>Action</th>
															@endif
														</tr>
													</thead>
													<tbody>
														@php
														$srno=1;
														@endphp
															@foreach($get_hotels as $hotels)

												<tr id="tr_{{$hotels->hotel_id}}">
													<td style="cursor: all-scroll;">{{$srno}}</td>
													<td style="display:none">{{$hotels->hotel_id}}</td>
													<td>{{$hotels->hotel_name}}</td>
													<td>{{$hotels->hotel_address}}</td>
													<td>{{$hotels->getCity->name}}</td>
													<td>{{$hotels->getCountry->country_name}}</td>
													<td>{{$hotels->getSupplier->supplier_name}}</td>
														@if($rights['edit_delete']==1)
														@if(strpos($rights['admin_which'],'edit_delete')!==false)
														<td>
															@if($hotels->hotel_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_{{$hotels->hotel_id}}">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_{{$hotels->hotel_id}}">InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($hotels->hotel_approve_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($hotels->hotel_approve_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_{{$hotels->hotel_id}}">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_{{$hotels->hotel_id}}">Reject</button>
															@elseif($hotels->hotel_approve_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
															<td>
															
															@if($hotels->hotel_best_status==1)
															<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_{{$hotels->hotel_id}}" >Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_{{$hotels->hotel_id}}" >InActive</button>
															@endif
														</td>

														@elseif($hotels->hotel_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
														<td>
															@if($hotels->hotel_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_{{$hotels->hotel_id}}">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_{{$hotels->hotel_id}}">InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($hotels->hotel_approve_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($hotels->hotel_approve_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_{{$hotels->hotel_id}}">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_{{$hotels->hotel_id}}">Reject</button>
															@elseif($hotels->hotel_approve_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
															<td>
															
															@if($hotels->hotel_best_status==1)
															<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_{{$hotels->hotel_id}}" >Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_{{$hotels->hotel_id}}" >InActive</button>
															@endif
														</td>
														@elseif($hotels->hotel_created_by==Session::get('travel_users_id') && $hotels->hotel_create_role!="Supplier" && $rights['edit_delete']==1)
														<td>
															@if($hotels->hotel_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_{{$hotels->hotel_id}}">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_{{$hotels->hotel_id}}">InActive</button>
															@endif
														</td>
														
														
														<td style="white-space: nowrap;">
														@if($hotels->hotel_approve_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($hotels->hotel_approve_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_{{$hotels->hotel_id}}">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_{{$hotels->hotel_id}}">Reject</button>
															@elseif($hotels->hotel_approve_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
															<td>
															
															@if($hotels->hotel_best_status==1)
															<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_{{$hotels->hotel_id}}" >Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_{{$hotels->hotel_id}}" >InActive</button>
															@endif
														</td>
															@else
															<td>
															@if($hotels->hotel_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary" id="inactive_hotel_{{$hotels->hotel_id}}" disabled="disabled">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" id="active_hotel_{{$hotels->hotel_id}}" disabled="disabled">InActive</button>
															@endif
														</td>
														<td style="white-space: nowrap;">
														@if($hotels->hotel_approve_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
															@elseif($hotels->hotel_approve_status==0)
															<button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_hotel_{{$hotels->hotel_id}}" disabled="disabled">Approve</button>
															<button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_hotel_{{$hotels->hotel_id}}" disabled="disabled">Reject</button>
															@elseif($hotels->hotel_approve_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
															@endif


															</td>
															<td>
															
															@if($hotels->hotel_best_status==1)
															<button type="button" class="btn btn-sm btn-rounded best_seller_no btn-primary" id="bestsellerno_hotel_{{$hotels->hotel_id}}" disabled="disabled">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded best_seller_yes btn-default" id="bestselleryes_hotel_{{$hotels->hotel_id}}" disabled="disabled">InActive</button>
															@endif
														</td>
														@endif


														
															@endif

															@if($rights['edit_delete']==1 || $rights['view']==1)

													<td>
														@if(strpos($rights['admin_which'],'view')!==false)
														<a href="{{route('hotel-details',['hotel_id'=>$hotels->hotel_id])}}" target="_blank"><i class="fa fa-eye"></i></a>
														@elseif($hotels->hotel_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'view')!==false)
														<a href="{{route('hotel-details',['hotel_id'=>$hotels->hotel_id])}}"><i class="fa fa-eye"></i></a>
														@elseif($hotels->hotel_created_by==Session::get('travel_users_id') && $hotels->hotel_create_role!="Supplier" && $rights['view']==1)
														<a href="{{route('hotel-details',['hotel_id'=>$hotels->hotel_id])}}" target="_blank"><i class="fa fa-eye"></i></a>
														@endif

														@if(strpos($rights['admin_which'],'edit_delete')!==false)
														<a href="{{route('edit-hotel',['hotel_id'=>$hotels->hotel_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>
														<a href="{{route('edit-old-hotel',['hotel_id'=>$hotels->hotel_id])}}" target="_blank" style="color:red"><i class="fa fa-pencil"></i></a>
														@elseif($hotels->hotel_created_by!=Session::get('travel_users_id') &&  strpos($rights['admin_which'],'edit_delete')!==false)
														<a href="{{route('edit-hotel',['hotel_id'=>$hotels->hotel_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>
														<a href="{{route('edit-old-hotel',['hotel_id'=>$hotels->hotel_id])}}" target="_blank" style="color:red"><i class="fa fa-pencil"></i></a>
														@elseif($hotels->hotel_created_by==Session::get('travel_users_id') && $hotels->hotel_create_role!="Supplier" && $rights['edit_delete']==1)
														<a href="{{route('edit-hotel',['hotel_id'=>$hotels->hotel_id])}}" target="_blank"><i class="fa fa-pencil"></i></a>
														<a href="{{route('edit-old-hotel',['hotel_id'=>$hotels->hotel_id])}}" target="_blank"  style="color:red"><i class="fa fa-pencil"></i></a>
														@endif

														

													</td>
													@endif

												</tr>
												@php
														$srno++;
														@endphp

												@endforeach

													</tbody>

												</table>
											</div>
										</div>
										<!-- /.box-body -->
									</div>
								</div>
								@endif


							</div>
						</div>
						<div id="navpills-activity" class="tab-pane show">
						</div>
						<div id="navpills-3" class="tab-pane show">
						</div>
						<div id="navpills-guide" class="tab-pane show">
						</div>
						<div id="navpills-sightseeing" class="tab-pane show">
						</div>
						<div id="navpills-transfer" class="tab-pane show">
						</div>
						<div id="navpills-driver" class="tab-pane show">
						</div>
					</div>
				</div>


				<!-- /.row -->
			</div>
			<!-- /.box-body -->
		</div>
		@else
	<h4 class="text-danger">No rights to access this page</h4>
			
			@endif

		<!-- /.box -->

	</div>

</div>
</div>

</div>

</div>
@include('mains.includes.footer')

@include('mains.includes.bottom-footer')
<div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
<script>
	$(".datatable").DataTable();


    var table_hotel = $('#hotel_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[25, 50, 100,150,200,250,500,-1], [25, 50, 100,150,200,250,500,"All"]]
    });
 
    table_hotel.on( 'row-reorder', function ( e, diff1, edit1 ) {
        var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
            var rowData = table_hotel.row( diff1[i].node ).data();
 			new_data.push({hotel_id:rowData[1],new_order:diff1[i].newData});
        }

  // console.log(new_data);
        if(new_data.length>0)
        {
          $.ajax({
        	url:"{{route('sort-hotel')}}",
        	type:"POST",
        	data:{"_token":"{{csrf_token()}}",
        	"new_data":new_data},
        	success:function(response)
        	{
        		console.log(response);
        	}
        });	
        }
      
        
   
    });
	$(document).on("click",'.approve',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="hotel")
		{

			swal({
				title: "Are you sure?",
				text: "You want to approve this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-hotel-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');

								swal("Approved!", "Selected Hotel has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="transfer")
		{

			swal({
				title: "Are you sure?",
				text: "You want to approve the transfers of this transfer group !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transfer-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transfer_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');

								swal("Approved!", "Selected Transfer Group has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to approve this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-activity-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');

								swal("Approved!", "Selected Activity has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to approve this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');

								swal("Approved!", "Selected Transport has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to approve this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('admin-update-guide-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');

								swal("Approved!", "Selected Guide has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="driver")
		{
			swal({
				title: "Are you sure?",
				text: "You want to approve this driver !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Approve",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-driver-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"driver_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');

								swal("Approved!", "Selected Driver has been approved.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.reject',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="hotel")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-hotel-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');

								swal("Rejected!", "Selected Hotel has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="transfer")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject the transfers of this transfer group !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transfer-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transfer_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');

								swal("Rejected!", "Selected Transfer has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-activity-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');

								swal("Rejected!", "Selected Activity has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');

								swal("Rejected!", "Selected Transport has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('admin-update-guide-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');

								swal("Rejected!", "Selected Guide has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="driver")
		{
			swal({
				title: "Are you sure?",
				text: "You want to reject this driver !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Reject",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-driver-approval')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"driver_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');

								swal("Rejected!", "Selected Driver has been rejected.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
	});

	$(document).on("click",'.active',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="hotel")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-hotel-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_hotel_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Hotel has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="transfer")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate this transfer group !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transfer-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transfer_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_transfer_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Transfer Group has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-activity-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_activity_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Activity has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_transport_'+action_id+'" >Active</button>');

								swal("Activated!", "Selected Transport has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('admin-update-guide-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_guide_'+action_id+'" >Active</button>');

								swal("Activated!", "Selected Guide has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="driver")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate this driver !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-driver-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"driver_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_driver_'+action_id+'" >Active</button>');

								swal("Activated!", "Selected Driver has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="sightseeing")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate this sightseeing !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-sightseeing-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"sightseeing_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_sightseeing_'+action_id+'" >Active</button>');

								swal("Activated!", "Selected Sightseeing has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.inactive',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="hotel")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-hotel-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_hotel_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Hotel has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="transfer")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this transfer group !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transfer-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transfer_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_transfer_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Transfer Group has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});



				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-activity-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_activity_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Activity has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_transport_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Transport has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('admin-update-guide-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_guide_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Guide has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="driver")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this driver !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-driver-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"driver_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_driver_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Driver has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="sightseeing")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this sightseeing !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-sightseeing-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"sightseeing_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_sightseeing_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Sightseeing has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}

	});



$(document).on("click",'.best_seller_yes',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="hotel")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-hotel-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_hotel_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Hotel's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-activity-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_activity_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Activity's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_transport_'+action_id+'" >Active</button>');

								swal("Activated!", "Selected Transport has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('admin-update-guide-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_guide_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Guide's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="sightseeing")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this sightseing !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-sightseeing-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"sightseeing_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_sightseeing_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Sightseeing's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.best_seller_no',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="hotel")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-hotel-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_hotel_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Hotel's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-activity-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_activity_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Activity's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_transport_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Transport has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('admin-update-guide-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_guide_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Guide's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="sightseeing")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this sightseeing !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-sightseeing-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"sightseeing_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_sightseeing_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Sightseeing's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}

	});






$(document).on("click",'.popular_yes',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];

		if(action_name=="hotel")
		{

			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-hotel-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_hotel_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Hotel's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-activity-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_activity_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Activity's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary inactive" id="inactive_transport_'+action_id+'" >Active</button>');

								swal("Activated!", "Selected Transport has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate best seller status for this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('admin-update-guide-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary best_seller_no" id="bestsellerno_guide_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Guide's best seller status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}
		else if(action_name=="sightseeing")
		{
			swal({
				title: "Are you sure?",
				text: "You want to activate popular status for this sightseing !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Activate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {


					$.ajax({
						url:"{{route('update-sightseeing-popular')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"sightseeing_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary popular_no" id="bestsellerno_popular_'+action_id+'">Active</button>');

								swal("Activated!", "Selected Sightseeing's popular status has been activated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
							
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});

		}

	});
	$(document).on("click",'.popular_no',function()
	{
		var id=this.id;
		var actual_id=id.split("_");

		var action_name=actual_id[1];
		var action_perform=actual_id[0];
		var action_id=actual_id[2];
		if(action_name=="hotel")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this hotel !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-hotel-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"hotel_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_hotel_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Hotel's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="activity")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this activity !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-activity-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"activity_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_activity_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Activity's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});


		}
		else if(action_name=="transport")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate this transport !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-transport-active')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"transport_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default active" id="active_transport_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Transport has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});


					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="guide")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate best seller status for this guide !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('admin-update-guide-bestseller')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"guide_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default best_seller_yes" id="bestselleryes_guide_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Guide's best seller status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}
		else if(action_name=="sightseeing")
		{
			swal({
				title: "Are you sure?",
				text: "You want to inactivate popular status for this sightseeing !",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Inactivate",
				cancelButtonText: "No, cancel!",
				closeOnConfirm: false,
				closeOnCancel: false
			}, function(isConfirm) {
				if (isConfirm) {

					$.ajax({
						url:"{{route('update-sightseeing-popular')}}",
						type:"POST",
						data:{"_token":"{{csrf_token()}}",
						"action_perform":action_perform,
						"sightseeing_id":action_id},
						success:function(response)
						{
							if(response.indexOf("success")!=-1)
							{
								$("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-default popular_yes" id="popularyes_sightseeing_'+action_id+'">InActive</button>');

								swal("Inactivated!", "Selected Sightseeing's popular status has been inactivated.", "success");
							}
							else
							{
								swal("Error", "Unable to perform this operation", "error");

							}
						}
					});

					
				} else {
					swal("Cancelled", "Operation Cancelled", "error");
				}
			});
			
		}

	});


  $(document).on("click","#navpills-hotel-link",function()
    {
        $("#loaderModal").modal("show");
        $("#navpills-hotel").html("");
         $.ajax({
            url:"{{route('fetch-service-management-tab')}}",
            type:"GET",
            data:{"booking_type":"hotel"},
            success:function(response)
            {
                $("#navpills-hotel").html(response);
                 var table_hotel = $('#hotel_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[25, 50, 100,150,200,250,500,-1], [25, 50, 100,150,200,250,500,"All"]]
    });
 
    table_hotel.on( 'row-reorder', function ( e, diff1, edit1 ) {
        var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
            var rowData = table_hotel.row( diff1[i].node ).data();
 			new_data.push({hotel_id:rowData[1],new_order:diff1[i].newData});
        }

  // console.log(new_data);
        if(new_data.length>0)
        {
          $.ajax({
        	url:"{{route('sort-hotel')}}",
        	type:"POST",
        	data:{"_token":"{{csrf_token()}}",
        	"new_data":new_data},
        	success:function(response)
        	{
        		console.log(response);
        	}
        });	
        }
      
        
   
    });
                   $("#loaderModal").modal("hide");
            }
        }); 
    });


  $(document).on("click","#navpills-activity-link",function()
    {
        $("#loaderModal").modal("show");
        $("#navpills-activity").html("");
         $.ajax({
            url:"{{route('fetch-service-management-tab')}}",
            type:"GET",
            data:{"booking_type":"activity"},
            success:function(response)
            {
                $("#navpills-activity").html(response);
                 var table = $('#activity_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[25, 50, 100,150,200,250,500,-1], [25, 50, 100,150,200,250,500,"All"]]
    });
 
    table.on( 'row-reorder', function ( e, diff, edit ) {
        var result = 'Reorder started on row: '+edit.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            var rowData = table.row( diff[i].node ).data();
 			new_data.push({activity_id:rowData[1],new_order:diff[i].newData});
           
        }

        // console.log(new_data);
        if(new_data.length>0)
        {
          $.ajax({
        	url:"{{route('sort-activity')}}",
        	type:"POST",
        	data:{"_token":"{{csrf_token()}}",
        	"new_data":new_data},
        	success:function(response)
        	{
        		console.log(response);
        	}
        });	
        }
      
        
   
    });
                   $("#loaderModal").modal("hide");
            }
        }); 
    });

  $(document).on("click","#navpills-sightseeing-link",function()
    {
        $("#loaderModal").modal("show");
        $("#navpills-sightseeing").html("");
         $.ajax({
            url:"{{route('fetch-service-management-tab')}}",
            type:"GET",
            data:{"booking_type":'sightseeing'},
            success:function(response)
            {
                $("#navpills-sightseeing").html(response);
                var table_sightseeing = $('#sightseeing_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[10,25, 50, 100,150,200,250,500,-1], [10,25, 50, 100,150,200,250,500,"All"]]
    });
 
    table_sightseeing.on( 'row-reorder', function ( e, diff1, edit1 ) {
        var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
            var rowData = table_sightseeing.row( diff1[i].node ).data();
 			new_data.push({sightseeing_id:rowData[1],new_order:diff1[i].newData});
        }

  		// console.log(new_data);
        if(new_data.length>0)
        {
          $.ajax({
        	url:"{{route('sort-sightseeing')}}",
        	type:"POST",
        	data:{"_token":"{{csrf_token()}}",
        	"new_data":new_data},
        	success:function(response)
        	{
        		console.log(response);
        	}
        });	
        }

    });
                   $("#loaderModal").modal("hide");
            }
        }); 
    });

   $(document).on("click","#navpills-transfer-link",function()
    {
        $("#loaderModal").modal("show");
        $("#navpills-transfer").html("");
         $.ajax({
            url:"{{route('fetch-service-management-tab')}}",
            type:"GET",
            data:{"booking_type":"transfer"},
            success:function(response)
            {
                $("#navpills-transfer").html(response);
                var table_transfer = $('#transfer_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[10,25, 50, 100,150,200,250,500,-1], [10,25, 50, 100,150,200,250,500,"All"]]
    });
 
    table_transfer.on( 'row-reorder', function ( e, diff1, edit1 ) {
        var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
            var rowData = table_sightseeing.row( diff1[i].node ).data();
 			new_data.push({sightseeing_id:rowData[1],new_order:diff1[i].newData});
        }

  		// console.log(new_data);
        // if(new_data.length>0)
        // {
        //   $.ajax({
        // 	url:"{{route('sort-sightseeing')}}",
        // 	type:"POST",
        // 	data:{"_token":"{{csrf_token()}}",
        // 	"new_data":new_data},
        // 	success:function(response)
        // 	{
        // 		console.log(response);
        // 	}
        // });	
        // }

    });
                   $("#loaderModal").modal("hide");
            }
        }); 
    });


   $(document).on("click","#navpills-guide-link",function()
    {
        $("#loaderModal").modal("show");
        $("#navpills-guide").html("");
         $.ajax({
            url:"{{route('fetch-service-management-tab')}}",
            type:"GET",
            data:{"booking_type":"guide"},
            success:function(response)
            {
                $("#navpills-guide").html(response);
                 var table_guide = $('#guide_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[25, 50, 100,150,200,250,500,-1], [25, 50, 100,150,200,250,500,"All"]]
    });
 
    table_guide.on( 'row-reorder', function ( e, diff1, edit1 ) {
        var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
            var rowData = table_guide.row( diff1[i].node ).data();
 			new_data.push({guide_id:rowData[1],new_order:diff1[i].newData});
        }

  		// console.log(new_data);
        if(new_data.length>0)
        {
          $.ajax({
        	url:"{{route('sort-guide')}}",
        	type:"POST",
        	data:{"_token":"{{csrf_token()}}",
        	"new_data":new_data},
        	success:function(response)
        	{
        		console.log(response);
        	}
        });	
        }

    });
                   $("#loaderModal").modal("hide");
            }
        }); 
    });

   $(document).on("click","#navpills-driver-link",function()
    {
        $("#loaderModal").modal("show");
        $("#navpills-driver").html("");
         $.ajax({
            url:"{{route('fetch-service-management-tab')}}",
            type:"GET",
            data:{"booking_type":"driver"},
            success:function(response)
            {
                $("#navpills-driver").html(response);
               var table_driver = $('#driver_table').DataTable({
        rowReorder: true,
         "lengthMenu": [[10,25, 50, 100,150,200,250,500,-1], [10,25, 50, 100,150,200,250,500,"All"]]
    });
 
    table_driver.on( 'row-reorder', function ( e, diff1, edit1 ) {
        var result = 'Reorder started on row: '+edit1.triggerRow.data()[1]+'<br>';
 		var new_data=[];
        for ( var i=0, ien=diff1.length ; i<ien ; i++ ) {
            var rowData = table_driver.row( diff1[i].node ).data();
 			new_data.push({driver_id:rowData[1],new_order:diff1[i].newData});
        }

  		console.log(new_data);
        if(new_data.length>0)
        {
          $.ajax({
        	url:"{{route('sort-driver')}}",
        	type:"POST",
        	data:{"_token":"{{csrf_token()}}",
        	"new_data":new_data},
        	success:function(response)
        	{
        		console.log(response);
        	}
        });	
        }

    });

                   $("#loaderModal").modal("hide");
            }
        }); 
    });
</script>
</body>
</html>
