<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    .text-green
    {
        color:green;
    }
    .text-red
    {
        color:red;
    }
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Commissions</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item active" aria-current="page">Commissions</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                   @if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Commissions Details</h4>
                                </div>
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive" id="commission_data">
                                              <p>List of Booings that represents commissions of <strong>{{$user_name}}</strong> for {{$month_name}}, {{$yearname}}</p>
                                                <table class="table table-bordered datatable">
                                                            <thead>
                                                                <tr>
                                                                    <th>Sr. no</th>
                                                                    <th>Booking ID</th>
                                                                     <th>Booking Type</th>
                                                                    <th>Profit Earned</th>
                                                                    <th>Total Expenses</th>
                                                                    <th>Actual Profit( PROFIT - EXPENSES )</th>
                                                                    <th>Booking Portal</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                              @foreach($user_commissions_details as $commissions_details)
                                                              <tr>
                                                                <td>{{$loop->iteration}}</td>
                                                                <td>{{$commissions_details['booking_id']}}</td>
                                                                 <td>{{$commissions_details['booking_type']}}</td>
                                                                  <td>{{$commissions_details['booking_profit']}}</td>
                                                                  <td>{{$commissions_details['booking_expenses']}}</td>
                                                                  <td>{{$commissions_details['booking_actual_profit']}}</td>
                                                                   <td>{{$commissions_details['booking_role']}} PORTAL</td>
                                                              </tr>
                                                              @endforeach
                                                            </tbody>
                                                          </table>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                                   @else
                                    <h4 class="text-danger">No rights to access this page</h4>
                                   @endif
                                </div>
                            </div>
                        </div>
                        @include('mains.includes.footer')
                        @include('mains.includes.bottom-footer')
                        <script>
                            $(document).ready(function()
                            {
$(".datatable").DataTable();
                            });
                        </script>
                    </body>
                    </html>