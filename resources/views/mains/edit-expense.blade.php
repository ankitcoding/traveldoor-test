@include('mains.includes.top-header')
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative"> 

@include('mains.includes.nav')

    <div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">{{ucfirst($expense_type)}} Expense Management</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Home</li>

                            @if($expense_type=="booking")
    
                            <li class="breadcrumb-item" aria-current="page">Bookings</li>

                            <li class="breadcrumb-item active" aria-current="page">Edit Booking Expense

                            @else

                            <li class="breadcrumb-item" aria-current="page">Expenses</li>

                            <li class="breadcrumb-item active" aria-current="page">Edit Office Expense

                            @endif


                            </li>

                        </ol>

                    </nav>

                </div>

            </div>
        </div>

    </div>

@if($rights['add']==1)
    <div class="row">

        <div class="col-12">

            <div class="box">

                <div class="box-header with-border">

                    <h4 class="box-title">Edit Expense</h4>

                </div>

                <div class="box-body">

                      <form id="user_form" method="post" accept-charset="utf-8">

                         {{csrf_field()}}

        @if($expense_type=="booking")
                         <div class="row mb-10">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">

                                        <label>Booking ID<span class="asterisk">*</span></label>

                                        <select id="expense_booking_id" name="expense_booking_id" class="form-control select2" style="width: 100%;">
                                            <option selected="selected" value="0" hidden="hidden">--Select Booking ID--</option>
                                            @foreach($fetch_bookings as $booking)
                                                <option value="{{$booking->booking_sep_id}}"  @if($fetch_expenses->expense_booking_id==$booking->booking_sep_id) selected="selected" @endif>{{$booking->booking_sep_id}} ({{$booking->booking_type}})</option>
                                            @endforeach

                                        </select>
                            </div>
                        </div>
                    </div>
                    @else
                     <input type="hidden" name="expense_booking_id" id="expense_booking_id">
                    @endif

                    <div class="row mb-10">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">

                                        <label>Expense Category<span class="asterisk">*</span></label>

                                        <input type="hidden" name="expense_type" id="expense_type" value="{{$expense_type}}">
                                        <input type="hidden" name="expense_id" id="expense_id" value="{{$fetch_expenses->expenses_id}}">

                                        <select id="expense_category" name="expense_category" class="form-control select2" style="width: 100%;">
                                            <option selected="selected" value="0" hidden="hidden">--Select Category--</option>
                                            @foreach($fetch_expense_category as $expense_category)
                                                <option value="{{$expense_category->expense_category_id}}" @if($fetch_expenses->expense_category_id==$expense_category->expense_category_id) selected="selected" @endif>{{$expense_category->expense_category_name}}</option>
                                            @endforeach

                                        </select>
                            </div>
                        </div>
                    </div>

                    
                     <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>Occured On <span class="asterisk">*</span></label>

                            <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" placeholder="Select Date & Time" id="expense_occured" name="expense_occured" readonly="readonly" value="{{$fetch_expenses->expense_occured_on}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

                            </div>

                        </div>

                    </div>

                     <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>Amount <span class="asterisk">*</span></label>

                                <input type="text" class="form-control" placeholder="Enter Amount" id="expense_amount" name="expense_amount" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)" value="{{$fetch_expenses->expense_amount}}">

                            </div>

                        </div>

                    </div>

                       <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>Remarks <span class="text-info">(optional)</span></label>

                               <textarea name="expense_remarks" id="expense_remarks" class="form-control" placeholder="Enter Remarks Here...">{{$fetch_expenses->expense_remarks}}</textarea>

                            </div>

                        </div>

                    </div>


                    <div class="row mb-10">

                        <div class="col-md-12">

                            <div class="box-header with-border"

                                style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">

                                <button type="button"  id="update_expense" class="btn btn-rounded btn-primary mr-10">Update</button>

                                <button type="button" id="discard_expense" class="btn btn-rounded btn-primary">Discard</button>

                            </div>

                        </div>

                    </div>

                </form>

                </div>

            </div>

        </div>

    </div>
 @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif
</div>

</div>

</div>





@include('mains.includes.footer')

@include('mains.includes.bottom-footer')

<script>  
    $(document).ready(function()
    {
        $(".select2").select2();
        $("#datetimepicker1").datetimepicker({
            autoclose:true,
            format: 'yyyy-mm-dd hh:ii:00'
        });

    });
        $("#discard_expense").on("click",function()

        {

            window.history.back();

        });





        $(document).on("click","#update_expense",function()

        {

            var expense_category=$("#expense_category").val();

            var expense_type=$("#expense_type").val();

            var expense_booking_id=$("#expense_booking_id").val(); 

             var expense_occured=$("#expense_occured").val(); 

              var expense_amount=$("#expense_amount").val(); 



            if(expense_category.trim()=="0")

            {

                $("#expense_category").parent().find('.select2-selection').css("border","1px solid #cf3c63");

            }

            else

            {

             $("#expense_category").parent().find('.select2-selection').css("border","1px solid #9e9e9e"); 

            }


             if(expense_type.trim()=="booking" && expense_booking_id.trim()=="0")

            {

                $("#expense_booking_id").parent().find('.select2-selection').css("border","1px solid #cf3c63");

            }

            else

            {

             $("#expense_booking_id").parent().find('.select2-selection').css("border","1px solid #9e9e9e"); 

            }



             if(expense_amount.trim()=="")

            {

                $("#expense_amount").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#expense_amount").css("border","1px solid #9e9e9e"); 

            }

             if(expense_occured.trim()=="")

            {

                $("#expense_occured").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#expense_occured").css("border","1px solid #9e9e9e"); 

            }


        
            if(expense_category.trim()=="0")

            {

                $("#expense_category").parent().find('.select2-selection').focus();

            }
            else if(expense_type.trim()=="booking" && expense_booking_id.trim()=="0")

            {

                $("#expense_booking_id").parent().find('.select2-selection').focus();

            }
            else if(expense_occured.trim()=="")

            {

                $("#expense_occured").focus();

            }

             else if(expense_amount.trim()=="")

            {

                $("#expense_amount").focus();

            }
            else

            {

                $("#update_expense").prop("disabled",true);

                var formdata=new FormData($("#user_form")[0]);

                $.ajax({

                    url:"{{route('update-expenses')}}",

                    data: formdata,

                    type:"POST",

                    processData: false,

                    contentType: false,

                    success:function(response)

                    {

                      if(response.indexOf("success")!=-1)
                      {

                        swal({title:"Success",text:"Expense Updated Successfully !",type: "success"},

                         function(){ 

                             location.reload();

                         });

                      }

                      else if(response.indexOf("fail")!=-1)

                      {

                       swal("ERROR", "Expense cannot be updated right now! ");

                      }

                        $("#update_expense").prop("disabled",false);

                    }

                });  

            }







        });

</script>

</body>

</html>

