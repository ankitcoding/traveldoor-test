<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    .text-green
    {
        color:green;
    }
    .text-red
    {
        color:red;
    }
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Commissions</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item active" aria-current="page">Commissions</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                   @if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Commissions</h4>
                                </div>
                                <div class="box-body">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="comm_month">Month</label>
                                                <select class="form-control select2" id="comm_month" name="comm_month">
                                                    <option value="">--Select Month--</option>

                                                <?php
                                                 $month = strtotime(date('Y').'-'.date('m').'-'.date('j').' - 12 months');
                                                  $end = strtotime(date('Y').'-'.date('m').'-'.date('j').' + 0 months');
                                                  while($month < $end){
                                                      $selected = (date('F', $month)==date('F'))? ' selected' :'';
                                                      echo '<option'.$selected.' value="'.date('m', $month).'-'.date('F', $month).'">'.date('F', $month).'</option>'."\n";
                                                      $month = strtotime("+1 month", $month);
                                                  }

                                                ?>
                                            </select>
                                            </div>
                                        </div>
                                         <div class="col-md-3">
                                            <div class="form-group">
                                                 <label for="comm_year">Year</label>
                                                <select class="form-control select2" id="comm_year" name="comm_year">
                                                    <option value="">--Select Year--</option>
                                                    @for($year=2020;$year<=(date("Y")+5);$year++)
                                                   <option value="{{$year}}" @if($year==date('Y')) selected="selected" @endif>{{$year}}</option>
                                                    @endfor
                                                        
                                            </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive" id="commission_data">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                                   @else
                                    <h4 class="text-danger">No rights to access this page</h4>
                                   @endif
                                </div>
                            </div>
                        </div>
                        @include('mains.includes.footer')
                        @include('mains.includes.bottom-footer')
@if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
                        <script>
                            function get_commissions()
                            {
                                var comm_month=$("#comm_month").val();
                                     var comm_year=$("#comm_year").val();

                                     if(comm_month=="")
                                     {
                                        swal("Select Month first");
                                     }
                                     else if(comm_year=="")
                                     {
                                         swal("Select Year first");
                                     }
                                     else
                                     {
                                        $("#commission_data").html("<h3 class='text-center'><b>Loading....</b></h3>");
                                        $.ajax({
                                            url:"{{route('user-commissions-filter')}}",
                                            data:{"month":comm_month,
                                                 "year":comm_year},
                                            type:"GET",
                                                success:function(response)
                                                {
                                                    $("#commission_data").html(response);
                                                    $(".datatable").DataTable();
                                                }
                                        });
                                     }
                            }

                            function pay_commissions(user_id)
                            {
                                var user_actual_id=user_id;
                                var comm_month=$("#comm_month").val();
                                     var comm_year=$("#comm_year").val();

                                     if(comm_month=="")
                                     {
                                        swal("Select Month first");
                                     }
                                     else if(comm_year=="")
                                     {
                                         swal("Select Year first");
                                     }
                                     else
                                     {

                              swal({
                title: "Are you sure?",
                text: "You want to pay commission to this user",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Go Ahead",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                                        $.ajax({
                                            url:"{{route('pay-commissions')}}",
                                            data:{"_token":"{{csrf_token()}}",
                                                "month":comm_month,
                                                 "year":comm_year,
                                                    "user_id":user_actual_id},
                                            type:"POST",
                                                success:function(response)
                                                {
                                                    if(response.indexOf("success")!=-1)
                                                    {
                                                        swal("Paid","You have paid commission successfully","success");
                                                        get_commissions();
                                                    }
                                                    else if(response.indexOf("invaid_user")!=-1)
                                                    {
                                                             swal("Error","Invalid User","error");
                                                    }
                                                    else if(response.indexOf("fail")!=-1)
                                                    {
                                                        swal("Error!","Commissions cannot be paid right now.Try refreshing the page","error");
                                                    }
                                                }
                                        });
                                         } else {
                    swal("Cancelled", "Operation Cancelled", "error");
                }
            });

                                     }
                            }
                            $(document).ready(function()
                            {
                                $(".select2").select2();

                                get_commissions();

                                $(document).on("change","#comm_year,#comm_month",function()
                                {
                                    get_commissions();

                                });

                                 $(document).on("click",".pay_commission",function()
                                {
                                    var id=this.id;
                                    var actual_id=id.split("_")[2];
                                    pay_commissions(actual_id);

                                });




                            });
                        </script>
                        @endif
                    </body>
                    </html>