<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
  .text-green
  {
    color:green;
  }
  .text-red
  {
    color:red;
  }
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
  <div class="wrapper">
    @include('mains.includes.top-nav')
    <div class="content-wrapper">
      <div class="container-full clearfix position-relative">
        @include('mains.includes.nav')
        <div class="content">
          <div class="content-header">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="page-title">Monthly Report</h3>
                <div class="d-inline-block align-items-center">
                  <nav>
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                      <li class="breadcrumb-item" aria-current="page">Home</li>
                      <li class="breadcrumb-item active" aria-current="page">Monthly Report</li>
                    </ol>
                  </nav>
                </div>
              </div>
            </div>
          </div>
          @if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
          <div class="row">
            <div class="col-12">
              <div class="box">
                <div class="box-header with-border">
                  <h4 class="box-title">Monthly Report</h4>
                </div>
                <div class="box-body">

                  <div class="row">
                    <div class="col-md-3">
                      <div class="form-group">
                        <label for="comm_month">Month</label>
                        <select class="form-control select2" id="comm_month" name="comm_month">
                          <option value="">--Select Month--</option>

                          <?php
                          $month = strtotime(date('Y').'-'.date('m').'-'.date('j').' - 12 months');
                          $end = strtotime(date('Y').'-'.date('m').'-'.date('j').' + 0 months');
                          while($month < $end){
                            $selected = (date('F', $month)==date('F'))? ' selected' :'';
                            echo '<option'.$selected.' value="'.date('m', $month).'-'.date('F', $month).'">'.date('F', $month).'</option>'."\n";
                            $month = strtotime("+1 month", $month);
                          }

                          ?>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-3">
                      <div class="form-group">
                       <label for="comm_year">Year</label>
                       <select class="form-control select2" id="comm_year" name="comm_year">
                        <option value="">--Select Year--</option>
                        @for($year=2020;$year<=(date("Y")+5);$year++)
                        <option value="{{$year}}" @if($year==date('Y')) selected="selected" @endif>{{$year}}</option>
                        @endfor

                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <br>
                    <button onclick="printReport();" class="btn btn-primary">Print Report</button>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-12">
                    <h3>Bookings</h3>
                    <div class="table-responsive" id="booking_table">
                    </div>

                  </div>
                  <div class="col-md-12">
                    <h3>Office Incomes</h3>
                    <div class="table-responsive" id="office_incomes_table">
                    </div>

                  </div>

                  <div class="col-md-12">
                    <h3>Office Expenses</h3>
                    <div class="table-responsive" id="office_expenses_table">
                    </div>

                  </div>
                  <div class="col-md-12">
                    <h3>Final Calculation</h3>
                      <div class="table-responsive" id="office_expenses_table">
                        <table class="table table-bordered">
                          <tr>
                          <th>Particulars</th>
                          <th>Cr.</th>
                          <th>Dr.</th>
                        </tr>
                        <tr>
                          <td>Total Profit from Bookings</td>
                          <td> <span id="total_bookings_profit" class="text-green"></span></td>
                           <td></td>
                        </tr>
                        <tr>
                          <td>Total Office Incomes</td>
                          <td> <span id="total_office_incomes" class="text-green"></span></td>
                           <td></td>
                        </tr>
                        <tr>
                          <td>Total Office Expenses</td>
                           <td></td>
                          <td> <span id="total_office_expenses" class="text-red"></span></td>
                        </tr>
                         <tr>
                            <td></td>
                            <th>Profit</th>
                             <th>Loss</th>
                        </tr>
                          <tr>
                          <td><b>Total Profit/Loss for this month</b></td>
                          <td><span id="total_profit_loss_more"></span></td>
                           <td><span id="total_profit_loss_less"></span></td>
                        </tr>
                      </table>
                    </div>

                  </div>
                  <br>
                  <hr>
                  <h2>Advance Incomes/Expenses</h2>
                  <hr>
                   <div class="col-md-12">

                    <h3>Booking Incomes</h3>
                    <div class="table-responsive" id="booking_incomes_table">
                    </div>

                  </div>

                  <div class="col-md-12">
                    <h3>Booking Expenses</h3>
                    <div class="table-responsive" id="booking_expenses_table">
                    </div>

                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
        @else
        <h4 class="text-danger">No rights to access this page</h4>
        @endif
      </div>
    </div>
  </div>
  @include('mains.includes.footer')
  @include('mains.includes.bottom-footer')


  @if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
  <script>
    function get_report()
    {
      var comm_month=$("#comm_month").val();
      var comm_year=$("#comm_year").val();

      if(comm_month=="")
      {
        swal("Select Month first");
      }
      else if(comm_year=="")
      {
       swal("Select Year first");
     }
     else
     {
      $("#booking_table").html("<h5 class='text-center'><b>Loading....</b></h5>");
      $("#office_expenses_table").html("<h5 class='text-center'><b>Loading....</b></h5>");
      $("#office_incomes_table").html("<h5 class='text-center'><b>Loading....</b></h5>");
      $.ajax({
        url:"{{route('monthly-report')}}",
        data:{"_token":"{{csrf_token()}}",
        "month":comm_month,
        "year":comm_year},
        type:"POST",
        dataType:"JSON",
        success:function(response)
        {
          $("#booking_table").html(response.bookings_data);
          $("#office_expenses_table").html(response.office_expenses_data);
          $("#office_incomes_table").html(response.office_incomes_data);
           $("#booking_expenses_table").html(response.booking_expenses_data);
          $("#booking_incomes_table").html(response.booking_incomes_data);
          $("#total_bookings_profit").html("GEL "+response.total_bookings_profit);
          $("#total_office_incomes").html("GEL "+response.total_office_incomes);
          $("#total_office_expenses").html("GEL "+response.total_office_expenses);
        
          if(response.total_profit_loss>=0)
          {
            $("#total_profit_loss_more").addClass("text-green").removeClass('text-red').html("GEL "+Math.abs(response.total_profit_loss));
            $("#total_profit_loss_less").html("");
          }
          else
          {
            $("#total_profit_loss_less").addClass("text-red").removeClass('text-green').html("GEL "+Math.abs(response.total_profit_loss));
            $("#total_profit_loss_more").html("");
          }

        }
      });
    }
  }
  $(document).ready(function()
  {
    $(".select2").select2();

    get_report();

    $(document).on("change","#comm_year,#comm_month",function()
    {
      get_report();

    });
  });
  function printReport()
  {
   var comm_month=$("#comm_month").val();
   var comm_year=$("#comm_year").val();

   if(comm_month=="")
   {
    swal("Select Month first");
  }
  else if(comm_year=="")
  {
   swal("Select Year first");
 }
 else
 {
  window.location.href="{{route('monthly-report-print')}}?"+"month="+comm_month+"&year="+comm_year+"&method=print";

}
}
</script>
@endif
</body>
</html>