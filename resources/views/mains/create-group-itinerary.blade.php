@include('mains.includes.top-header')

<style>
	header.main-header {

		background: url("{{ asset('assets/images/color-plate/theme-purple.jpg') }}");

	}

	.days_div {
    border: none;
    background: #ffe7df;
    padding: 25px !important;
    margin-bottom: 30px !important;
    border-radius: 5px;
}



	div#cke_1_contents {

		height: 250px !important;

	}



	table#calendar-demo {

		width: 100%;

		height: 275px !IMPORTANT;

		min-height: 275px !important;

		overflow: hidden;

	}



	.calendar-wrapper.load {

		width: 100%;

		height: 276px;

	}



	.calendar-date-holder .calendar-dates .date.month a {

		display: block;

		padding: 17px 0 !important;

	}



	.calendar-date-holder {

		width: 100% !important;

	}



	section.calendar-head-card {

		display: none;

	}



	.calendar-container {

		border: 1px solid #cccccc;

		height: 276px !important;

	}



	img.plus-icon {

		margin: 0 2px;

		display: inline !important;

	}



	@media screen and (max-width:400px) {

		.calendar-date-holder .calendar-dates .date a {

			text-decoration: none;

			display: block;

			color: inherit;

			padding: 3px !important;

			margin: 1px;

			outline: none;

			border: 2px solid transparent;

			transition: all .3s;

			-o-transition: all .3s;

			-moz-transition: all .3s;

			-webkit-transition: all .3s;

		}

	}

	.select_hotel,
	.select_sightseeing,
	.select_activity {
		cursor: pointer !important;
	}

	.color-tiles-text {
		background: #573a9e85;
	}

	.grid-images img {
    width: 30%;
    margin: 10px;
    border: 1px solid #bdbdbd;
    padding: 4px;
    border-radius: 5px;
}
.grid-images {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;
}
p.para-h {
    color: #9C27B0;
    font-size: 20px;
    border-bottom: 1px dotted #9C27B0;
}
p.address {
    background: #fbe1ff;
    padding: 5px 10px;
    border-radius: 5px;
    color: #9C27B0;
    font-size: 13px;
}
p.star-p i {
    background: #ffccbc;
    color: #ff5722;
    padding: 7px;
    border-radius: 14px;
}
p.star-p {
    color: #FF5722;
    font-size: 15px;
    font-weight: 600;
    border-bottom: 1px solid lightgrey;
    padding-bottom: 8px;
}
.cstm-alert-notice{
	background: #e4e4e4 !important;
    color: #2f2f2f;
    border: none;
}
.box .overlay {
    z-index: 50;
    background: rgba(103, 58, 183, 0.39);
    border-radius: 5px;
}
p.color-tiles-text {
    background: #e1e1e1;
    position: absolute;
    width: 100%;
    top: 100%;
    left: 0;
    padding: 5px 10px;
    height: auto;
    display: flex;
    justify-content: center;
    color: #0088ff !important;
    border-radius: 5px;
    align-items: center;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
	font-size: 15px;
}
.bg-div {
    background: url('{{ asset("assets/images/vehicle-type.jpg") }}')!important;
    height: 130px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 65px;
    position: relative;
}
.theme-rosegold .alert-success {
    border-color: #ffffff;
    background-color: #e0e0e0 !important;
    color: #353535;
}
div.hotels_select .box.box-body.bg-dark.pull-up,.activity_select .box.box-body.bg-dark.pull-up,.sightseeing_select .box.box-body.bg-dark.pull-up,.transfer_select .box.box-body.bg-dark.pull-up{
    height: 115px;
    margin-bottom: 42px;
	margin-top: 20px;
}
.bg-dark {
   
    height: 130px;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
    margin-bottom: 65px;
    position: relative;
}
label.tile-label {
    position: absolute !important;
    top: 8px !important;
    left: 20px;
    border-color: white;
    z-index: 999999;
    color: red !important;
}
a.details.text-light {
    color: #673AB7 !important;
    font-weight: 600;
}
div#selectSightseeingModal .modal-content {
    height: 585px;
}
div#selectSightseeingModal .modal-dialog {
    margin-top: 1%;
}
div#selectSightseeingModal .modal-content .modal-body {
    padding: 10px;
}
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #fbe1ff;
}
div#selectSightseeingModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus {
    background: white;
    color: #9c27b0;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #9C27B0;
    border-radius: 10px;
}

/* Handle on hover */
div#selectSightseeingModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #555;
}
div#selectSightseeingModal .modal-lg, .modal-xl {
    max-width: 787px;
}
label.days_no {
    color: white;
    font-size: 15px;
    background: #FF5722;
    padding: 7px 20px;
    border-radius: 5px;
    width: 100px;
}
.days_div .select2-container--default .select2-selection--single {
    border: 1px solid #ffc5b2;
    border-radius: 20px;
    padding: 6px 12px;
    height: 34px;
}
.days_div p.color-tiles-text {
    background: #ff5722;
    position: absolute;
    width: 100%;
    top: 100%;
    left: 0;
    padding: 5px 10px;
    height: auto;
    display: flex;
    justify-content: center;
    color: #ffffff !important;
    border-radius: 5px;
    align-items: center;
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    font-size: 15px;
}
button.sightseeing_details,button.sightseeing_details:hover,button.sightseeing_details:focus {
    background: #9C27B0 !important;
    border: none;
    padding: 5px 9px;
    font-size: 11px;
    border-radius: 16px;
}
.select_sightseeing .card .card-body {
    background: #fbe4ff;
}
.select_sightseeing .card {
    border: 1px solid #f7caff;
    padding: 2px;
    min-height: 288px;
}

.select_activity .card .card-body {
    background: #ffe7df;
}

.select_activity .card {
    border: 1px solid #fdc4b1;
    padding: 2px;
    min-height: 270px;
}
.select_activity .card .card-body a{
	background: #FF5722 !important;
    border: none;
    padding: 5px 9px;
    font-size: 11px;
    border-radius: 16px;
}






div#selectActivityModal .modal-content {
    height: 585px;
}
div#selectActivityModal .modal-dialog {
    margin-top: 1%;
}
div#selectActivityModal .modal-content .modal-body {
    padding: 10px;
}
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #fdc4b1;
}
div#selectActivityModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus {
    background: white;
    color: #FF5722;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #FF5722;
    border-radius: 10px;
}

/* Handle on hover */
div#selectActivityModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #e83a00;
}
div#selectActivityModal .modal-lg, .modal-xl {
    max-width: 787px;
}
div#selectHotelModal .card-title {
    color: #8BC34A;
    text-transform: capitalize;
    font-size: 16px !important;
    border-bottom: 1px dotted #8BC34A;
}
table#room_type_table tr td, table#room_type_table tr th {
    padding: 5px;
    font-size: 13px;
}
table#room_type_table tr th:last-child {
    width: 100px !important;
}
table#room_type_table thead th {
    vertical-align: bottom;
    border-bottom: 1px solid #dee2e6;
}
.select_hotel .card .card-body {
    background: #edffd9 !IMPORTANT;
}
.select_hotel .card {
    border: 1px solid #d0e8b5;
    padding: 0px;
    height: 100%;
}
.select_hotel .card a {
    background: #8BC34A;
    border-color: #8BC34A;
    padding: 4px 10px;
    font-size: 13px;
}
table#room_type_table a {
    background: none;
    padding: 0;
}
.select_hotel {
    margin-bottom: 35px;
}



div#selectHotelModal .modal-content {
    height: 585px;
}
div#selectHotelModal .modal-dialog {
    margin-top: 1%;
}
div#selectHotelModal .modal-content .modal-body {
    padding: 10px;
}
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #edffd9;
}
div#selectHotelModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus {
    background: white;
    color: #8BC34A;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #8BC34A;
    border-radius: 10px;
}

/* Handle on hover */
div#selectHotelModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #8BC34A;
}
div#selectHotelModal .modal-lg, .modal-xl {
    max-width: 787px;
}

.modal-filter {
    background: #dbfdfa;
    padding: 10px;
    border-radius: 5px;
}
.modal-filter {
    background: #dbfdfa;
    padding: 10px;
    border: 1px solid #bef2ed;
    border-radius: 5px;
}
.modal-filter select,.modal-filter input,.modal-filter select:focus,.modal-filter input:focus {
    border: 1px solid #9ed9e2;
    border-radius: 50px;
}
button#search_transfer {
    background: #00BCD4;
    border: none;
    font-size: 14px;
    padding: 5px 15px;
    display: block;
	margin-top: 17px;
}
p.price {
    color: #9C27B0;
    font-size: 20px;
    padding: 0;
    text-align: right;
    border: 1px dashed;
    padding: 5px 10px;
    width: 130px;
    display: block;
    margin-left: auto;
}
select.type-select ,select.type-select:focus{
    border: 1px solid #f9d0ff;
    border-radius: 4px;
    width: 50%;
    margin-bottom: 20px;
    background: #fbe1ff;
    color: #9C27B0;
}
select.type-select:focu option{
   
    background: #fff;
    color: #9C27B0;
}
.modal-filter label {
    color: #00bcd4;
    margin-top: 10px;
}
div#transfer_div p:first-child {
    background: #ffe1e1;
    padding: 10px;
    color: #f44336;
    margin: 20px 0;
    border-radius: 5px;
}
.select_transfer .card .card-body {
    background: #dbfdfa;
}
.select_transfer .card {
    border: 1px solid #a6e3ea;
}

div#selectTransferModal .modal-content {
    height: 585px;
}
div#selectTransferModal .modal-dialog {
    margin-top: 1%;
}
div#selectTransferModal .modal-content .modal-body {
    padding: 10px;
}
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar {
  width: 8px;
}

/* Track */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-track {
    background: #a6e3ea;
}
div#selectTransferModal .modal-footer button,div#selectSightseeingModal .modal-footer button:hover,div#selectSightseeingModal .modal-footer button:focus {
    background: white;
    color: #00bcd4;
    border: none;
    border-radius: 50px;
    width: 100px;
    display: block;
    margin-left: auto;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.32);
    padding: 7px;
}

/* Handle */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-thumb {
    background: #00bcd4;
    border-radius: 10px;
}

/* Handle on hover */
div#selectTransferModal .modal-content .modal-body::-webkit-scrollbar-thumb:hover {
  background: #00b0ca;
}
div#selectTransferModal .modal-lg, .modal-xl {
    max-width: 787px;
}
.select_transfer .card .card-body h4.card-title {
    color: #00bcd4;
    font-weight: 600;
}

button.btn.btn-success.btn-rounded.change_date.pull-right {
    background: #FF5722;
    border: none;
    margin-top: 21px;
}
.days_div .form-group {
    margin-bottom: 0;
}


.sightseeing_remove,.transfer_remove,.activity_remove,.transfer_guide_remove
{
background-color: red !important; 
display:none; 
}

.days_div .form-group {
    margin-bottom: 12px;
}
</style>



<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">



	<div class="wrapper">



		@include('mains.includes.top-nav')



		<div class="content-wrapper">



			<div class="container-full clearfix position-relative">



				@include('mains.includes.nav')



				<div class="content">



					<div class="content-header">

						<div class="d-flex align-items-center">

							<div class="mr-auto">

								<h3 class="page-title">Create Group Itinerary </h3>

								<div class="d-inline-block align-items-center">

									<nav>

										<ol class="breadcrumb">

											<li class="breadcrumb-item"><a href="#"><i
														class="mdi mdi-home-outline"></i></a></li>

											<li class="breadcrumb-item" aria-current="page">Home</li>

											<li class="breadcrumb-item" aria-current="page">Create Group Itinerary</li>

										</ol>

									</nav>

								</div>

							</div>

						</div>

					</div>




					@if($rights['add']==1)
					<div class="row">

						<div class="col-12">

							<div class="box">

								<div class="box-header with-border">

									<h4 class="box-title">Create Group Itinerary</h4>

								</div>

								<div class="box-body">

									<form id="itinerary_form" encytpe="multipart/form-data">

										{{csrf_field()}}

										<div class="row mb-10">

											<div class="col-sm-6 col-md-6">

												<div class="form-group">

													<label for="tour_name">GROUP ITINERARY NAME <span
															class="asterisk">*</span></label>

													<input type="text" class="form-control" placeholder="GROUP ITINERARY NAME"
														id="tour_name" name="tour_name" maxlength="255">

												</div>

											</div>

											<div class="col-sm-6 col-md-6">



											</div>
                                        </div>

                                         {{--  add adult --}}
                                        <div class="row mb-10">

                                            <div class="col-sm-4 col-md-4">
                                                <div class="row mb-10">

                                                    <div class="col-sm-12 col-md-10">

                                                        <div class="form-group">

                                                            <label for="group_adults">Adults<span class="asterisk">*</span></label>
                                                            <select class="form-control" id="group_adults" name="group_adults">
                                                                <option value="">Select</option>
                                                                @for($i=1;$i<=50;$i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                                @endfor
                                                            </select>


                                                        </div>
                                                    </div>
                                                    

                                                </div>

                                            </div>
                                            <div class="col-sm-4 col-md-4">
                                                <div class="row mb-10">

                                                    <div class="col-sm-12 col-md-10">

                                                        <div class="form-group">
                                                            <label for="airport_child">Children<span class="asterisk">*</span></label>
                                                           <select class="form-control" id="airport_child" name="airport_child">
                                                                <option value="">Select</option>
                                                                @for($i=0;$i<=30;$i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                                @endfor
                                                            </select>


                                                        </div>
                                                    </div>
                                                    

                                                </div>

                                            </div>
                                            <div class="col-sm-4 col-md-4">
                                                <div class="row mb-10">

                                                    <div class="col-sm-12 col-md-10">

                                                        <div class="form-group">

                                                            <label for="airport_infant">Infant<span class="asterisk">*</span></label>
                                                            <select class="form-control" id="airport_infant" name="airport_infant">
                                                                <option value="">Select</option>
                                                                @for($i=1;$i<=50;$i++)
                                                                <option value="{{$i}}">{{$i}}</option>
                                                                @endfor
                                                            </select>


                                                        </div>
                                                    </div>
                                                    

                                                </div>

                                            </div>

                                        </div>
                                        {{-- end adults --}}
                                        {{-- children age --}}
                                        <div class="row mb-10">
                                        <div class="col-sm-3 col-md-3">
                                        </div>
                                            <div class="col-sm-6 col-md-6" id="showchildage" style="display: none">



                                                <div class="row mb-10">
                
                                                    <div class="col-sm-12 col-md-10">

                                                        <div class="form-group">

                                                            <label>Children Age</label>
                                                            <div id="airport_child_age_div" class="row">
                                                    </div>
                                                    

                                                        </div>
                                                     </div>
                                                    

                                                </div>

                                            </div>
                                            <div class="col-sm-3 col-md-3">
                                        </div>

                                        </div>
                                        {{-- end children age --}}
										


										<div class="row mb-10">

											<div class="col-sm-6 col-md-6">



												<div class="row mb-10">

													<div class="col-sm-12 col-md-6">

														<div class="form-group">

															<label>NO. OF NIGHTS</label>

															<select id="no_of_days" name="no_of_days"
																class="form-control select2" style="width: 100%;">
																<option value="0">SELECT NO. OF NIGHTS</option>
																@for($i=1;$i<=30;$i++) <option value="{{$i}}">{{$i}}
																	</option>
																	@endfor
															</select>
                                                            <input type="hidden" id="today_date" name="today_date" value="{{date('Y-m-d')}}">

														</div>





													</div>
													<div class="col-sm-12 col-md-6">
														<button type="button"
															class="btn btn-success btn-rounded change_date pull-right">Change
															Days</button>

													</div>

												</div>

											</div>

										</div>
                                  

            
										
										<div class="row">

											<div class="col-md-12 days_div" id="days_div" style="display: none;">
												<div class="form-group">
													<label class="days_no">DAY</label>
												</div>

												<div class="">
													<input type="hidden" class="days_number">
													<label class="days_title_label" for="tour_title">DAY TITLE</label>
													<input type="text" class="form-control days_title" maxlength="255">
													<br>
													<label class="days_description_label" for="tour_description">DAY
														DESCRIPTION</label>
													<textarea class="days_description"></textarea>

												</div>
												<div class="row">
													<div class="col-md-6 col-sm-12">
														<div class="form-group">

															<select class="form-control days_country select_country1 select2"
																style="width:100%;">
		
																<option value="0" selected="selected" disabled=""
																	hidden="hidden">SELECT COUNTRY</option>
		
																@foreach($countries as $country)
		
																<option value="{{$country->country_id}}">
																	{{$country->country_name}}</option>
		
																@endforeach
		
															</select>
		
														</div>
													</div>
													<div class="col-md-6 col-sm-12">
														<div class="form-group days_city_div" style="display:none">
															<input type="hidden" class="days_city_input">
															<select class="form-control select2 days_city" style="width:100%">
		
																<option value="0" selected="selected" hidden="hidden"
																	disabled="disabled">SELECT CITY</option>
		
															</select>
		
														</div>
													</div>
												</div>
												

												
												<div class="form-group">
													<div class="row">
														 <input type="hidden"  class="newhotel" value="1"> 
														<div class="col-xl-12 col-12 mainhoteldiv">
														<div class="col-xl-6 col-12 hotels_select">

															<div class="hotellength"  >
                                                                 
																<select class="form-control select_hotel"  style="width:100%">
        
                                                                <option value="0" selected="selected" hidden="hidden"
                                                                    disabled="disabled">SELECT Hotels</option>
        
                                                            </select>
																
															</div>
                                                            
															
														</div>
														<div class="col-xl-3 col-6">
															<button type="button" class="btn btn-primary add_hotel" id="add_hotel">+</button>
														</div>

                                                        <div class="col-xl-12 col-12 hotels_select_room showdata">
                                                           
                                                        </div>
                                                    </div>
														<div class="col-xl-6 col-12 sightseeing_select"
															style="cursor: pointer">
															<div class="">
                                                            <select class="form-control select2 select_sightseeing"  style="width:100%">
        
                                                                <option value="0" selected="selected" hidden="hidden"
                                                                    disabled="disabled">SELECT SIGHTSEEING</option>
        
                                                            </select>
																

															</div>
															
                                                              <button type="button" class="btn btn-sm sightseeing_remove">Remove</button>
															<p class="selected_sightseeing_name"></p>
															<input type="hidden" class="sightseeing_id">
															<input type="hidden" class="sightseeing_name">
                                                            <input type="hidden" class="sightseeing_tour_type">
															<input type="hidden" class="sightseeing_vehicle_type">
															<input type="hidden" class="sightseeing_guide_id">
															<input type="hidden" class="sightseeing_guide_name">
															<input type="hidden" class="sightseeing_guide_cost">
															<input type="hidden" class="sightseeing_driver_id">
															<input type="hidden" class="sightseeing_driver_name">
															<input type="hidden" class="sightseeing_driver_cost">
                                                            <input type="hidden" class="sightseeing_adult_cost">
                                                            <input type="hidden" class="sightseeing_additional_cost">
															<input type="hidden" class="sightseeing_cost cal_cost">
                                                            <input type="hidden" class="sightseeing_cities">
														</div>
                                                        <div class="col-xl-6 col-12 activity_select"
                                                            style="cursor: pointer">
                                                            <div>
                                                                <select class="form-control  select_activity"  style="width:100%">
        
                                                                <option value="0" selected="selected" hidden="hidden"
                                                                    disabled="disabled">SELECT ACTIVITY</option>
        
                                                            </select>
                                                            </div>
                                                            
                                                                     <button type="button" class="btn btn-sm activity_remove">Remove</button>
                                                            <p class="selected_activity_name"></p>
                                                            <input type="hidden" class="activity_id">
                                                            <input type="hidden" class="activity_name">
                                                            <input type="hidden"
                                                                class="activity_cost cal_activity_cost">
                                                        </div>
														<div class="col-xl-6 col-12 transfer_select">
															<div>
                                                                <select class="form-control select2 select_transfer"  style="width:100%">
        
                                                                <option value="0" selected="selected" hidden="hidden"
                                                                    disabled="disabled">SELECT Transfers</option>
        
                                                            </select>

																

															</div>
															
                                                            <button type="button" class="btn btn-sm transfer_remove">Remove</button>
															<p class="selected_transfer_name"></p>
															<input type="hidden" class="transfer_id">
															<input type="hidden" class="transfer_name">
															<input type="hidden" class="transfer_type">
															<input type="hidden" class="transfer_from_city">
															<input type="hidden" class="transfer_to_city">
															<input type="hidden" class="transfer_from_airport">
															<input type="hidden" class="transfer_to_airport">
															<input type="hidden" class="transfer_pickup">
															<input type="hidden" class="transfer_dropoff">
                                                            <input type="hidden" class="transfer_vehicle_type">
                                                            <input type="hidden" class="transfer_guide_id">
                                                            <input type="hidden" class="transfer_guide_name">
                                                            <input type="hidden" class="transfer_guide_cost">
															<input type="hidden" class="transfer_cost ">
                                                            <input type="hidden" class="transfer_total_cost cal_cost">

                                                            <button type="button" class="btn btn-sm btn-primary select_transfer_guide_btn" style="display:none">Select Guide</button>
                                                            <br>
                                                            <span class="selected_transfer_guide_label" style="display:none"><strong>Selected Guide :</strong></span>
                                                            <button type="button" class="btn btn-sm transfer_guide_remove"  style="display:none">Remove</button>
                                                            <p class="selected_transfer_guide_name" ></p>
														</div>

													</div>
												</div>





											</div>

										</div>



										<div class="row" id="itinerary_main_div">



										</div>

										<div class="row mb-10">
											<div class="col-sm-6 col-md-6">

												<div class="form-group">

													<label for="total_cost">TOTAL COST <span
															class="asterisk">*</span></label>

													<input type="text" class="form-control" placeholder="TOTAL COST"
														id="total_cost" name="total_cost" value="0" readonly="readonly">

												</div>

											</div>

										</div>

										
                                    </div>   

										{{-- <div class="row mb-10">

											<div class="col-md-12">

												<div class="box-header with-border"
													style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">

													<button type="button" id="save_itinerary"
														class="btn btn-rounded btn-primary mr-10">Save</button>

													<button type="button" id="discard_itinerary"
														class="btn btn-rounded btn-primary">Discard</button>

												</div>

											</div>

										</div> --}}



									</form>

								</div>

							</div>

						</div>





					</div>
					@else
					<h4 class="text-danger">No rights to access this page</h4>
					@endif

				</div>

			</div>

		</div>

		@include('mains.includes.footer')



		@include('mains.includes.bottom-footer')
		{{-- <div class="modal" id="selectHotelModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Select Hotel</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;">
						<input type="hidden" id="hotel_day_index">
						<label>No. of Nights</label>
						<select id="hotel_no_of_days" name="hotel_no_of_days" class="form-control">
						</select>
						<br>
						<div id="hotels_div">
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div>
		<div class="modal" id="selectActivityModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Select Activity</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;">
						<input type="hidden" id="activities_day_index">
						<div id="activities_div">
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div> --}}
	{{-- 	<div class="modal" id="selectSightseeingModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Select Sightseeing</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;">
						<input type="hidden" id="sightseeing_day_index">
						<div id="sightseeing_div">
						</div>
						<div id="sightseeing_details_div">
							<div class="row">
								<div class="col-12">
									<div class="box">
										<div class="box-body">
											<div class="row">
												<div class="col-md-12">
                                                    <input type="hidden" name="sightseeing_details_id" id="sightseeing_details_id">
                                                    <input type="hidden" name="sightseeing_cities" id="sightseeing_cities">
														<select class="form-control type-select" id="sightseeing_details_tour_type">
															<option value="private" selected="selected">PRIVATE TOUR</option>
															<option value="group">GROUP TOUR</option>													
														</select>
												</div>
												<div class="col-md-12">
													<div class="row">
														<div class="col-md-6 col-sm-12">

															<p class="para-h" id="sightseeing_details_name">Tbilisi - Mtskheta -Tbilisi</p>
												
														</div>
														<div class="col-md-6 col-sm-12">
                                                            <input type="hidden" name="sightseeing_adult_cost" id="sightseeing_adult_cost" value="">
                                                             <input type="hidden" name="sightseeing_additional_cost" id="sightseeing_additional_cost" value="">
                                                            <input type="hidden" name="sightseeing_group_cost" id="sightseeing_group_cost" value="">
                                                             <input type="hidden" name="sightseeing_total_cost" id="sightseeing_total_cost" value="">
                                                              <input type="hidden" name="selected_vehicle_type_id" id="selected_vehicle_type_id" value="0">
                                                               <input type="hidden" name="selected_vehicle_type_name"  id="selected_vehicle_type_name" value="">
                                                            <input type="hidden" name="selected_guide_id"  id="selected_guide_id" value="0">
                                                                <input type="hidden" name="selected_guide_name"  id="selected_guide_name" value="">
                                                             <input type="hidden" name="selected_guide_cost" id="selected_guide_cost" value="0">
                                                             <input type="hidden" name="selected_driver_id" id="selected_driver_id" value="0">
                                                                <input type="hidden" name="selected_driver_name"  id="selected_driver_name" value="">
                                                             <input type="hidden" name="selected_driver_cost" id="selected_driver_cost" value="0">

															<p class="price">Total Price
                                                        
																<span id="sightseeing_total_price_text">GEL 94</span></p>
														</div>
													</div>
													
												</div>
												<div class="col-md-3">
													<p class="address" id="sightseeing_details_address" ><i class="fa fa-map-marker"></i> (
														Tbilisi-Mtskheta )
													</p>
												</div>
												<div class="col-md-3">
													<p class="address" id="sightseeing_details_distance">
														<i class="fa fa-road" ></i> 51 KMS</p>


												</div>
												<div class="col-md-3">
													<p class="address" id="sightseeing_details_duration">
														<i class="fa fa-clock-o" ></i> 8 HOURS</p>

												</div>
											</div>

											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="col-lg-12 col-md-12">
															<div class="box">
																<div class="box-body" style="padding:0">
																	<div class="grid-images" id="sightseeing_detail_images">
																	</div>
																</div>
															</div>
														</div>

													</div>
													<div class="row">
														<div class="col-lg-12 col-md-12">
															<div class="section" id="">
																<p class="star-p"> <i class="fa fa-star star"></i>
																	Sightseeing Description
																</p>
                                                                <div id="sightseeing_detail_desc">
															
                                                            </div>


															</div>
															<div class="section">
																<p class="star-p"> <i class="fa fa-star star"></i>
																	Sightseeing Attractions
																</p>
                                                                  <div id="sightseeing_detail_attract">

                                                            </div>
															</div>
															<hr style="border-color: #ffc0c0;width: 100%;">
														</div>
													</div>
													<div class="row" style="margin-top:20px" id="vehicle_type_div">

                                                     @foreach($get_vehicles as $vehicles)
                                                     <!--  <i class="fa fa-user"></i> -->
                                                     <div class="col-md-4 text-center vehicle-type parent-tile" style="cursor: pointer">
                                                        <div class="box box-body bg-dark pull-up" @if($vehicles->vehicle_type_image!="")
                                        style="background: url({{ asset('assets/uploads/vehicle_type_images') }}/{{$vehicles->vehicle_type_image}});background-size: contain;background-repeat: no-repeat;" 
                                        @else 
                                        style="background: url({{asset('assets/images/vehicle-type.jpg')}});background-size: contain;background-repeat: no-repeat;" @endif>
                                                            <div class="overlay"></div>
                                                            <br>
                                                            <p class="font-size-26 text-center color-tiles-text"> <a href="#" class="details text-light" >
                                                              {{$vehicles->vehicle_type_name}}
                                                              <input type="hidden" id="vehcile_type_name_{{$vehicles->vehicle_type_id}}" name="vehcile_type_name_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_name}}"></a></p>
                                                              <br>
                                                          </div>
                                                          <input type="radio" class="with-gap radio-col-primary vehcile_type_show" name="vehcile_type" id="vehcile_type_{{$vehicles->vehicle_type_id}}" value="{{$vehicles->vehicle_type_id}}" data-minvalue="{{$vehicles->vehicle_type_min}}" data-maxvalue="{{$vehicles->vehicle_type_max}}">
                                                          <label for="vehcile_type_{{$vehicles->vehicle_type_id}}" class="tile-label"></label>
                                                      </div>

                                                      @endforeach           

													</div>
													<hr>
													<br>
													<p class="star-p" id="driver_div_head"> <i
															class="fa fa-star star"></i> Select Driver for Sightseeing
													</p>
													<div class="col-md-12" id="driver_div">
													</div>
													<br>
													<p class="star-p" id="guide_div_head"> <i
															class="fa fa-star star"></i> Select Guide for Sightseeing
													</p>
													<div class="col-md-12" id="guide_div">
													</div>
													<div class="text-center table-sightseeing-loader"
														style="display: none">
														<svg version="1.1" id="L5" xmlns="http://www.w3.org/2000/svg"
															xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
															viewBox="0 0 100 100" enable-background="new 0 0 0 0"
															xml:space="preserve">
															<circle fill="#F33D38" stroke="none" cx="6" cy="50" r="6"
																transform="translate(0 -3.75126)">
																<animateTransform attributeName="transform" dur="1s"
																	type="translate" values="0 15 ; 0 -15; 0 15"
																	repeatCount="indefinite" begin="0.1">
																</animateTransform>
															</circle>
															<circle fill="#F33D38" stroke="none" cx="30" cy="50" r="6"
																transform="translate(0 1.49916)">
																<animateTransform attributeName="transform" dur="1s"
																	type="translate" values="0 10 ; 0 -10; 0 10"
																	repeatCount="indefinite" begin="0.2">
																</animateTransform>
															</circle>
															<circle fill="#F33D38" stroke="none" cx="54" cy="50" r="6"
																transform="translate(0 2.74958)">
																<animateTransform attributeName="transform" dur="1s"
																	type="translate" values="0 5 ; 0 -5; 0 5"
																	repeatCount="indefinite" begin="0.3">
																</animateTransform>
															</circle>
														</svg>
													</div>

												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" id="include_sightseeing">Include</button>
					</div>

				</div>
			</div>
		</div> --}}
{{-- 
		<div class="modal" id="selectTransferModal">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Select Transfer</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;">

						<input type="hidden" id="transfer_day_index">
						<div class="modal-filter">
							<div class="row">
								<div class="col-md-3">
									<label>Transfer Type</label>
									<select id="transfer_type" name="transfer_type" class="form-control">
										<option value="">Select Transfer Type</option>
										<option value="from-airport">From Airport Transfer</option>
										<option value="to-airport">To Airport Transfer</option>
										<option value="city">City Transfer</option>
									</select>
								</div>
								<div class="col-md-3" id="from_airport_div" style="display:none">
									<label>From Airport</label>
									<select id="from_airport" name="from_airport" class="form-control">
										<option value="0">Select Airport</option>
										@foreach($fetch_airports as $airports)
										<option value="{{$airports->airport_master_id}}">{{$airports->airport_master_name}}
										</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-3" id="from_city_div" style="display:none">
									<label>From City</label>
									<select id="from_city" name="from_city" class="form-control">
	
									</select>
								</div>
								<div class="col-md-3" id="to_city_div" style="display:none">
									<label>To City</label>
									<select id="to_city" name="to_city" class="form-control">
	
									</select>
								</div>
								<div class="col-md-3" id="to_airport_div" style="display:none">
									<label>To Airport</label>
									<select id="to_airport" name="to_airport" class="form-control">
										<option value="0">Select Airport</option>
										@foreach($fetch_airports as $airports)
										<option value="{{$airports->airport_master_id}}">{{$airports->airport_master_name}}
										</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-3" id="pickup_div" style="display:none">
									<label>Pickup Location</label>
									<input type="text" id="pickup_location" name="pickup_location" class="form-control">
								</div>
								<div class="col-md-3" id="dropoff_div" style="display:none">
									<label>DropOff Location</label>
									<input type="text" id="dropoff_location" name="dropoff_location" class="form-control">
								</div>
								<div class="col-md-2" id="search_transfer_div">
									<br>
									<button class="btn btn-sm btn-primary" id="search_transfer">Search</button>
								</div>
	
							</div>
	
						</div>
					
						<div id="transfer_div">
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div> --}}


      {{--   <div class="modal" id="selectTransferGuideModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Select Transfer Guide</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;">

                        <input type="hidden" id="transfer_guide_day_index">
                       <div id="transfer_guide_div">
                           
                       </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                      <!--   <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
                        <button type="button" class="btn btn-primary" id="include_transfer_guide">Include</button>
                    </div>

                </div>
            </div>
        </div> --}}

		{{-- <div class="modal" id="getHotelDays">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">

					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title">Select Days</h4>
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>

					<!-- Modal body -->
					<div class="modal-body" style="height: 550px;
                overflow-y: auto;
            overflow-x: hidden;">
						<input type="hidden" id="hotel_day_index">
						<div id="sightseeing_div">
						</div>
					</div>

					<!-- Modal footer -->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
					</div>

				</div>
			</div>
		</div> --}}
{{-- 
            <div class="modal" id="vehicleModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Vehicle Images</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height: 550px;overflow-y: auto;overflow-x: hidden;" id="vehicle_view_images">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div> --}}

        {{-- <div class="modal" id="guideModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Guide Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height:600px;overflow-y: auto;overflow-x: hidden;" id="guide_details_div">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div> --}}
        {{-- <div class="modal" id="driverModal" style="z-index: 99999">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Driver Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body" style="height:600px;overflow-y: auto;overflow-x: hidden;" id="driver_details_div">
    
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                    
                    </div>

                </div>
            </div>
        </div> --}}
		<script>
			$(document).ready(function ()

				{

                     $(document).on('change','.upload_ativity_images1',function() { 
            if (this.files[0].size > 500000) { 
                alert("Try to upload file less than 500KB!"); 
                $(this).val("");
            }
        });

					// CKEDITOR.replace('tour_description');
					// CKEDITOR.replace('tour_exclusions');
					// CKEDITOR.replace('tour_terms_and_conditions');
					// CKEDITOR.replace('tour_cancellation');




					$('.select2').select2();




					$(document).on("change", "#no_of_days", function () {
						var no_of_days = $(this).val();
						$("#itinerary_main_div").html("");
						$("#change_date_div").show();


						for (loop = 0; loop <= no_of_days; loop++)

						{

							var clone_days = $("#days_div").clone();

							clone_days.find(".days_no").parent().parent().css("display", "block");

							clone_days.find(".days_no").parent().parent().attr({
								"id": "days_div__" + (loop + 1) + ""
							});

							clone_days.find(".days_no").text("DAY " + parseInt(loop + 1) + " :");


							clone_days.find(".days_number").attr({
								"id": "days_number__" + parseInt(loop + 1),
								"name": "days_number[]"
							}).val(parseInt(loop + 1));

							clone_days.find(".days_country").attr({
								"id": "days_country__" + parseInt(loop + 1),
								"name": "days_country[]"
							}).select2();

							clone_days.find(".days_country").siblings(".select2-container").slice(1).remove();

							clone_days.find(".days_city").attr({
								"id": "days_city__" + parseInt(loop + 1),
								"name": "days_city[]"
							});
							clone_days.find(".days_city_input").attr({
								"id": "days_city_input__" + parseInt(loop + 1),
								"name": "days_city_input[]"
							});
							clone_days.find(".days_description_label").attr({
								"for": "days_description__" + parseInt(loop + 1)
							}).text("DAY " + parseInt(loop + 1) + " DESCRIPTION").css({
								"display": "none"
							});

							clone_days.find(".days_title_label").attr({
								"for": "days_title_label__" + parseInt(loop + 1)
							}).text("DAY " + parseInt(loop + 1) + " TITLE").css({
								"display": "none"
							});

							if (loop != no_of_days) {
								clone_days.find(".hotels_select").attr({
									"id": "hotels_select__" + parseInt(loop + 1)
								});
                               
                               clone_days.find(".mainhoteldiv").attr({
									"id": "mainhoteldiv_" + parseInt(loop + 1)
								});

								clone_days.find(".selected_hotel_label").attr({
									"for": "selected_hotel_name__" + parseInt(loop + 1)
								});


								clone_days.find(".selected_hotel_name").attr({
									"id": "selected_hotel_name__" + parseInt(loop + 1)
								});
                                clone_days.find(".newhotel").attr({
                                    "id": "newhotel__" + parseInt(loop + 1),
                                    "name": "newhotel[]"
                                });
                                clone_days.find(".select_hotel").attr({
                                    "id": "select_hotel" + parseInt(loop + 1)+"-1",
                                    "name": "select_hotel[]"
                                });
                                
                                clone_days.find(".add_hotel").attr({
                                    "id": "add_hotel_" + parseInt(loop + 1)+"-1",
                                  
                                });
                                
                                 clone_days.find(".showdata").attr({
                                    "id": "showdata_" + parseInt(loop + 1)+"-1",
                                   
                                });
                                clone_days.find(".select_sightseeing").attr({
                                    "id": "select_sightseeing" + parseInt(loop + 1)+"-1",
                                    "name": "select_sightseeing[]"
                                });
                                clone_days.find(".select_activity").attr({
                                    "id": "select_activity" + parseInt(loop + 1)+"-1",
                                    "name": "select_activity[]"
                                });
                                clone_days.find(".select_transfer").attr({
                                    "id": "select_transfer" + parseInt(loop + 1)+"-1",
                                    "name": "select_transfer[]"
                                });


                                
								clone_days.find(".hotel_id").attr({
									"id": "hotel_id__" + parseInt(loop + 1),
									"name": "hotel_id[]"
								});
								clone_days.find(".hotel_name").attr({
									"id": "hotel_name__" + parseInt(loop + 1),
									"name": "hotel_name[]"
								});
								clone_days.find(".room_name").attr({
									"id": "room_name__" + parseInt(loop + 1),
									"name": "room_name[]"
								});
								clone_days.find(".hotel_cost").attr({
									"id": "hotel_cost__" + parseInt(loop + 1),
									"name": "hotel_cost[]"
								});
								clone_days.find(".hotel_no_of_days").attr({
									"id": "hotel_no_of_days__" + parseInt(loop + 1),
									"name": "hotel_no_of_days[]"
								});
							} else {
								clone_days.find(".hotels_select").remove();
							}



							clone_days.find(".activity_select").attr({
								"id": "activity_select__" + parseInt(loop + 1)
							});

							clone_days.find(".selected_activity_label").attr({
								"for": "selected_activity_name__" + parseInt(loop + 1)
							});

							clone_days.find(".selected_activity_name").attr({
								"id": "selected_activity_name__" + parseInt(loop + 1)
							});
							clone_days.find(".activity_id").attr({
								"id": "activity_id__" + parseInt(loop + 1),
								"name": "activity_id[]"
							});
							clone_days.find(".activity_name").attr({
								"id": "activity_name__" + parseInt(loop + 1),
								"name": "activity_name[]"
							});
							clone_days.find(".activity_cost").attr({
								"id": "activity_cost__" + parseInt(loop + 1),
								"name": "activity_cost[]"
							});
                             clone_days.find(".activity_remove").attr({
                                "id": "activity_remove__" + parseInt(loop + 1)
                            });




							clone_days.find(".sightseeing_select").attr({
								"id": "sightseeing_select__" + parseInt(loop + 1)
							});

							clone_days.find(".selected_sightseeing_label").attr({
								"for": "selected_sightseeing_name__" + parseInt(loop + 1)
							});

							clone_days.find(".selected_sightseeing_name").attr({
								"id": "selected_sightseeing_name__" + parseInt(loop + 1)
							});
							clone_days.find(".sightseeing_id").attr({
								"id": "sightseeing_id__" + parseInt(loop + 1),
								"name": "sightseeing_id[]"
							});
							clone_days.find(".sightseeing_name").attr({
								"id": "sightseeing_name__" + parseInt(loop + 1),
								"name": "sightseeing_name[]"
							});
                            clone_days.find(".sightseeing_cities").attr({
                                "id": "sightseeing_cities__" + parseInt(loop + 1)
                            });
							clone_days.find(".sightseeing_tour_type").attr({
								"id": "sightseeing_tour_type__" + parseInt(loop + 1),
								"name": "sightseeing_tour_type[]"
							});
                            clone_days.find(".sightseeing_vehicle_type").attr({
                                "id": "sightseeing_vehicle_type__" + parseInt(loop + 1),
                                "name": "sightseeing_vehicle_type[]"
                            });
                            clone_days.find(".sightseeing_guide_id").attr({
                                "id": "sightseeing_guide_id__" + parseInt(loop + 1),
                                "name": "sightseeing_guide_id[]"
                            });
                             clone_days.find(".sightseeing_guide_name").attr({
                                "id": "sightseeing_guide_name__" + parseInt(loop + 1),
                                "name": "sightseeing_guide_name[]"
                            });
                             clone_days.find(".sightseeing_guide_cost").attr({
                                "id": "sightseeing_guide_cost__" + parseInt(loop + 1),
                                "name": "sightseeing_guide_cost[]"
                            });

                              clone_days.find(".sightseeing_driver_id").attr({
                                "id": "sightseeing_driver_id__" + parseInt(loop + 1),
                                "name": "sightseeing_driver_id[]"
                            });
                             clone_days.find(".sightseeing_driver_name").attr({
                                "id": "sightseeing_driver_name__" + parseInt(loop + 1),
                                "name": "sightseeing_driver_name[]"
                            });
                             clone_days.find(".sightseeing_driver_cost").attr({
                                "id": "sightseeing_driver_cost__" + parseInt(loop + 1),
                                "name": "sightseeing_driver_cost[]"
                            });
                              clone_days.find(".sightseeing_adult_cost").attr({
                                "id": "sightseeing_adult_cost__" + parseInt(loop + 1),
                                "name": "sightseeing_adult_cost[]"
                            });

                                clone_days.find(".sightseeing_additional_cost").attr({
                                "id": "sightseeing_additional_cost__" + parseInt(loop + 1),
                                "name": "sightseeing_additional_cost[]"
                            });

                            clone_days.find(".sightseeing_cost").attr({
                                "id": "sightseeing_cost__" + parseInt(loop + 1),
                                "name": "sightseeing_cost[]"
                            });

                            clone_days.find(".sightseeing_remove").attr({
                                "id": "sightseeing_remove__" + parseInt(loop + 1)
                            });

							clone_days.find(".transfer_select").attr({
								"id": "transfer_select__" + parseInt(loop + 1)
							});

							clone_days.find(".selected_transfer_label").attr({
								"for": "selected_transfer_name__" + parseInt(loop + 1)
							});

                            clone_days.find(".selected_transfer_guide_label").attr({
                                "for": "selected_transfer_guide_name__" + parseInt(loop + 1),
                                "id": "selected_transfer_guide_label__" + parseInt(loop + 1)
                            });

                            clone_days.find(".selected_transfer_guide_name").attr({
                                "id": "selected_transfer_guide_name__" + parseInt(loop + 1)
                            });

							clone_days.find(".selected_transfer_name").attr({
								"id": "selected_transfer_name__" + parseInt(loop + 1)
							});
							clone_days.find(".transfer_id").attr({
								"id": "transfer_id__" + parseInt(loop + 1),
								"name": "transfer_id[]"
							});
							clone_days.find(".transfer_name").attr({
								"id": "transfer_name__" + parseInt(loop + 1),
								"name": "transfer_name[]"
							});
							clone_days.find(".transfer_type").attr({
								"id": "transfer_type__" + parseInt(loop + 1),
								"name": "transfer_type[]"
							});
							clone_days.find(".transfer_from_city").attr({
								"id": "transfer_from_city__" + parseInt(loop + 1),
								"name": "transfer_from_city[]"
							});
							clone_days.find(".transfer_to_city").attr({
								"id": "transfer_to_city__" + parseInt(loop + 1),
								"name": "transfer_to_city[]"
							});
							clone_days.find(".transfer_from_airport").attr({
								"id": "transfer_from_airport__" + parseInt(loop + 1),
								"name": "transfer_from_airport[]"
							});
							clone_days.find(".transfer_to_airport").attr({
								"id": "transfer_to_airport__" + parseInt(loop + 1),
								"name": "transfer_to_airport[]"
							});
							clone_days.find(".transfer_pickup").attr({
								"id": "transfer_pickup__" + parseInt(loop + 1),
								"name": "transfer_pickup[]"
							});
							clone_days.find(".transfer_dropoff").attr({
								"id": "transfer_dropoff__" + parseInt(loop + 1),
								"name": "transfer_dropoff[]"
							});

                             clone_days.find(".transfer_vehicle_type").attr({
                                "id": "transfer_vehicle_type__" + parseInt(loop + 1),
                                "name": "transfer_vehicle_type[]"
                            });
                            clone_days.find(".transfer_guide_id").attr({
                                "id": "transfer_guide_id__" + parseInt(loop + 1),
                                "name": "transfer_guide_id[]"
                            });
                             clone_days.find(".transfer_guide_name").attr({
                                "id": "transfer_guide_name__" + parseInt(loop + 1),
                                "name": "transfer_guide_name[]"
                            });
                             clone_days.find(".transfer_guide_cost").attr({
                                "id": "transfer_guide_cost__" + parseInt(loop + 1),
                                "name": "transfer_guide_cost[]"
                            });

							clone_days.find(".transfer_cost").attr({
								"id": "transfer_cost__" + parseInt(loop + 1),
								"name": "transfer_cost[]"
							});
                            clone_days.find(".transfer_total_cost").attr({
                                "id": "transfer_total_cost__" + parseInt(loop + 1),
                                "name": "transfer_total_cost[]"
                            });
                              clone_days.find(".select_transfer_guide_btn").attr({
                                "id": "select_transfer_guide_btn__" + parseInt(loop + 1),
                            });

                            clone_days.find(".transfer_remove").attr({
                                "id": "transfer_remove__" + parseInt(loop + 1)
                            });

                             clone_days.find(".transfer_guide_remove").attr({
                                "id": "transfer_guide_remove__" + parseInt(loop + 1)
                            });


							clone_days.find(".days_description").attr({
								"id": "days_description__" + parseInt(loop + 1),
								"name": "days_description[]"
							}).css({
								"display": "none"
							});
							clone_days.find(".days_title").attr({
								"id": "days_title__" + parseInt(loop + 1),
								"name": "days_title[]"
							}).css({
								"display": "none"
							}).css({
								"display": "none"
							});



							$("#itinerary_main_div").append(clone_days);
							// CKEDITOR.replace('days_description__'+parseInt(loop+1));
							// document.getElementById("cke_days_description__"+parseInt(loop+1)).style.visibility = 'hidden';
							// console.log("div#cke_days_description__"+parseInt(loop+1));



						}



					});
				});
		</script>

		<script>
			$(document).on("change", ".days_country", function ()

				{

					var country_id = $(this).val();

					var country_actual_id = $(this).attr("id");

					var actual_id = country_actual_id.split("__");

					$("#selected_hotel_name__" + actual_id[1]).text("");
					$("#hotel_id__" + actual_id[1]).val("");
					$("#hotel_name__" + actual_id[1]).val("");
					$("#room_name__" + actual_id[1]).val("");
					$("#hotel_cost__" + actual_id[1]).val("");
					$("#hotel_no_of_days__" + actual_id[1]).val("");

					$("#selected_activity_name__" + actual_id[1]).text("");
					$("#activity_id__" + actual_id[1]).val("");
					$("#activity_name__" + actual_id[1]).val("");
					$("#activity_cost__" + actual_id[1]).val("");

					$("#selected_sightseeing_name__" + actual_id[1]).text("");
					$("#sightseeing_id__" + actual_id[1]).val("");
					$("#sightseeing_name__" + actual_id[1]).val("");
					$("#sightseeing_cost__" + actual_id[1]).val("");

					var city_exist = $("#days_city_input__" + actual_id[1]).val();


					$.ajax({

						url: "{{route('search-country-cities')}}",

						type: "GET",

						data: {
							"country_id": country_id
						},

						success: function (response)

						{


                            $('#hotels_select__'+actual_id[1]).show();
							$("#" + country_actual_id).parent().parent().parent().find('#days_city__' + actual_id[1]).html(
								response).select2();

							$("#" + country_actual_id).parent().parent().parent().find('#days_city__' + actual_id[1])
							.siblings(".select2-container").slice(1).remove();

							$("#" + country_actual_id).parent().parent().parent().find('.days_city_div').show();
							if (city_exist != "") {
								$("#" + country_actual_id).parent().parent().parent().find('#days_city__' + actual_id[1]).val(
									city_exist).trigger('change');
							}




						}

					});



				});
		</script>

		<script>
			$(document).on("click", ".change_date", function ()

				{

					$("#no_of_days").val(0).trigger("change");

					$("#itinerary_main_div").html("");

					$("#change_date_div").hide();

					$("#total_cost").val(0);

				});
		</script>

		{{-- <script>
			$(document).on("click", ".hotels_select", function () {
				var id = this.id;
				var hotel_id = id.split("__")[1];
				var no_of_days = $("#no_of_days").val();
				no_of_days++;
				var calculate_days = parseInt(no_of_days - parseInt(hotel_id));

				var country_id = $("#days_country__" + hotel_id).val();
				var city_id = $("#days_city__" + hotel_id).val();
				$no_of_days_html = '<option value="">Select No. of Nights</option>';
				for ($i = 1; $i <= calculate_days; $i++) {
					$no_of_days_html += '<option value="' + $i + '">' + $i + '</option>';
				}

				if (country_id == null) {
					alert("Please select country first");
				} else if (city_id == 0) {
					alert("Please select city first");
				} else {
					$.ajax({
						url: "{{route('itinerary-get-hotels')}}",
						data: {
							"country_id": country_id,
							"city_id": city_id
						},
						type: "get",
						success: function (response) {
							$("#hotels_div").html(response);
							$("#hotel_day_index").val(hotel_id);
							$("#hotel_no_of_days").html($no_of_days_html);
							$("#selectHotelModal").modal("show");
						}
					});

				}



			});
		</script> --}}

		<script>
			$(document).on("click", ".activity_select", function (event) {

                 var event_id=event.target.id;
                if(event_id.indexOf("remove")!=-1)
                {
                    return false;
                }
                
				var id = this.id;
				var activity_id = id.split("__")[1];

				var country_id = $("#days_country__" + activity_id).val();
				var city_id = $("#days_city__" + activity_id).val();
                var cities = $("#sightseeing_cities__" + activity_id).val();
				if (country_id == null) {
					alert("Please select country first");
				} else if (city_id == 0) {
					alert("Please select city first");
				}
                else if(cities.trim()=="")
                {
                    alert("Please select sightseeing first");
                } else {
					$.ajax({
						url: "{{route('itinerary-get-activities')}}",
						data: {
							"country_id": country_id,
							"city_id": city_id,
                            "cities":cities
						},
						type: "get",
						success: function (response) {
							$("#activities_div").html(response);
							$("#activities_day_index").val(activity_id);

							$("#selectActivityModal").modal("show");
						}
					});
				}

			});
		</script>


		<script>
			$(document).on("click", ".sightseeing_select", function (event) {

                var event_id=event.target.id;
                if(event_id.indexOf("remove")!=-1)
                {
                    return false;
                }

                $("#sightseeing_details_div").hide();
				var id = this.id;
				var sightseeing_id = id.split("__")[1];
				var country_id = $("#days_country__" + sightseeing_id).val();
				var city_id = $("#days_city__" + sightseeing_id).val();
                if(parseInt(sightseeing_id)-1==0)
                {
                   var prev_city_id = "";  
                }
                else
                {
                     var prev_city_id = $("#days_city__" + parseInt(sightseeing_id-1)).val();  
                }
               
				if (country_id == null) {
					alert("Please select country first");
				} else if (city_id == 0) {
					alert("Please select city first");
				} else {
					$("#selectSightseeingModal").modal("show");
					$("#sightseeing_div").html("<h4 class='text-center'><b>Loading Content...</b></h4>");
					$("#sightseeing_div").html("<h4 class='text-center'><b>Loading Content....</b></h4>");
					$.ajax({
						url: "{{route('itinerary-get-sightseeing')}}",
						data: {
							"country_id": country_id,
							"city_id": city_id,
                            "prev_city_id":prev_city_id
						},
						type: "get",
						success: function (response) {
							$("#sightseeing_div").html(
								"<h4 class='text-center'><b>Loading Content......</b></h4>");
							$("#sightseeing_div").html(response).show();
							
							$("#sightseeing_day_index").val(sightseeing_id);

						}
					});
				}

			});
		</script>
		<script>
			$(document).on("click", ".sightseeing_details", function () {
				var id = $(this).attr("id").split("_")[2];

				$.ajax({
					url: "{{route('itinerary-get-sightseeing-details')}}",
					data: {
						"sightseeing_id": id
					},
					type: "get",
                    dataType:"JSON",
					success: function (response) {
						$("#sightseeing_div").hide();
                        $("#sightseeing_details_id").val(response.sightseeing_id);
                        $("#sightseeing_cities").val(response.sightseeing_cities);
                        $("#sightseeing_details_name").text(response.sightseeing_name);
                          $("#sightseeing_details_address").html('<i class="fa fa-map-marker"></i> '+response.sightseeing_route);
                           $("#sightseeing_details_distance").html('<i class="fa fa-road" ></i> '+response.sightseeing_distance_covered);
                           $("#sightseeing_details_duration").html('<i class="fa fa-clock-o" ></i> '+response.sightseeing_duration); 
                            $("#sightseeing_detail_images").html("");
                           var images=response.sightseeing_images; 

                           $.each( images, function( key, value ) {
                             $("#sightseeing_detail_images").append("<img src='"+value+"'>");
                            });
                           $("#sightseeing_detail_desc").html(response.sightseeing_tour_desc);
                            $("#sightseeing_detail_attract").html(response.sightseeing_attractions);

                            if(response.sightseeing_group_tour_status==0)
                            {
                                $("#sightseeing_details_tour_type").html('<option value="private" selected="selected">PRIVATE TOUR</option>');
                                $("#sightseeing_adult_cost").val(response.total_adult_cost);
                                  $("#sightseeing_additional_cost").val(response.sightseeing_additional_cost);
                                var total_cost=parseInt(response.total_adult_cost);
                                total_cost+=parseInt(response.sightseeing_additional_cost);
                                if(response.sightseeing_default_guide_price!="" && response.sightseeing_default_guide_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_guide_price);
                                }

                                 if(response.sightseeing_default_driver_price!="" && response.sightseeing_default_driver_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_driver_price);
                                }
                               
                                $("#sightseeing_total_price_text").text("GEL "+total_cost); 
                                 
                                 $("#sightseeing_group_cost").val(response.group_adult_cost);

                            }
                            else
                            {
                                $("#sightseeing_details_tour_type").html('<option value="private" selected="selected">PRIVATE TOUR</option><option value="group">GROUP TOUR</option>');
                                $("#sightseeing_adult_cost").val(response.total_adult_cost);
                                 $("#sightseeing_additional_cost").val(response.sightseeing_additional_cost);
                                var total_cost=parseInt(response.total_adult_cost);
                                 total_cost+=parseInt(response.sightseeing_additional_cost);
                                if(response.sightseeing_default_guide_price!="" && response.sightseeing_default_guide_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_guide_price);
                                }

                                 if(response.sightseeing_default_driver_price!="" && response.sightseeing_default_driver_price!=null)
                                {
                                    total_cost+=parseInt(response.sightseeing_default_driver_price);
                                }
                               
                                $("#sightseeing_total_price_text").text("GEL "+total_cost); 
                                 

                                 $("#sightseeing_group_cost").val(response.group_adult_cost);

                            }


                             $("#guide_div").hide();
                            $("#driver_div").hide();
                              $("#guide_div_head").hide();
                            $("#driver_div_head").hide();
                            $("#vehicle_type_div").show();
                            $("input[name='selected_guide_id']").val("");
                              $("input[name='selected_guide_cost']").val(0);
                              $("input[name='selected_guide_name']").val("");
                            $("input[name='selected_driver_id']").val("");
                              $("input[name='selected_driver_cost']").val(0);
                              $("input[name='selected_driver_name']").val("");
                              $("input[name='vehcile_type']").prop("checked",false);

                               var sightseeing_day_index=$("#sightseeing_day_index").val();
                              setTimeout(function()
                              {
                                 if($("#sightseeing_vehicle_type__"+sightseeing_day_index).val().trim()!="")
                                 {
                                    var vehicle_type_id=$("#sightseeing_vehicle_type__"+sightseeing_day_index).val().trim();
                                    $(".vehicle-type").each(function()
                                    {
                                        console.log("Vehicle :"+$(this).find(":radio").val());
                                        if($(this).find(":radio").val()==vehicle_type_id)
                                        {
                                            $(this).find(":radio").attr("checked","checked");
                                             $(this).find(":radio").prop("checked",true).trigger("change");
                                            // $("input[name='vehcile_type']")
                                        }
                                    });
                                }
                                
                        $("#sightseeing_details_div").show();

                            },1000);

          
				}
				});
			});
		</script>

		<script>
			$(document).on("click", "#transfer_type", function () {
				var type = $(this).val();

				if (type == "from-airport") {
					$("#from_airport_div").show();
					$("#to_city_div").show();
					$("#dropoff_div").show();

					$("#pickup_div").hide();
					$("#from_city_div").hide();
					$("#to_airport_div").hide();
				} else if (type == "to-airport") {
					$("#from_city_div").show();
					$("#to_airport_div").show();
					$("#pickup_div").show();

					$("#dropoff_div").hide();
					$("#from_airport_div").hide();
					$("#to_city_div").hide();

				} else if (type == "city") {
					$("#from_city_div").show();
					$("#to_city_div").show();
					$("#pickup_div").show();
					$("#dropoff_div").show();
					$("#to_airport_div").hide();
					$("#from_airport_div").hide();
				}

			});

			$(document).on("click", ".transfer_select", function (event) {

                 var event_id=event.target.id;
                if(event_id.indexOf("remove")!=-1 || event_id.indexOf("transfer_guide")!=-1)
                {
                    return false;
                }

				$("#transfer_type").val("");
				$("#from_airport_div").hide();
				$("#to_city_div").hide();
				$("#from_city_div").hide();
				$("#to_airport_div").hide();
				$("#pickup_div").hide();
				$("#dropoff_div").hide();

				var id = this.id;
				var transfer_id = id.split("__")[1];

				var country_id = $("#days_country__" + transfer_id).val();
				var city_id = $("#days_city__" + transfer_id).val();
				var error = 0;
				if (country_id !== undefined) {
					if (country_id == null) {
						alert("Please select country first");
						error++;
					} else if (city_id == 0) {
						alert("Please select city first");
						error++;
					}
				}

				if (error == 0) {

					if (country_id == undefined) {
						var transfer_previous_id = transfer_id - 1;
						var country_id = $("#days_country__" + transfer_previous_id).val();

					}

					$.ajax({

						url: "{{route('search-country-cities')}}",

						type: "GET",

						data: {
							"country_id": country_id
						},

						success: function (response) {
							$("#from_city").html(response);
							$("#to_city").html(response);
							$("#transfer_day_index").val(transfer_id);
							$("#transfer_div").html("");
							$("#pickup_location").val("");
							$("#dropoff_location").val("");
							$("#selectTransferModal").modal("show");

						}

					});

				}

			});

            $(document).on("click", ".select_transfer_guide_btn", function (event) {

                var id = this.id;
                var transfer_id = id.split("__")[1];


                var transfer_own_id = $("#transfer_id__" + transfer_id).val();
               var country_id = $("#days_country__" + transfer_id).val();
                var city_id = $("#days_city__" + transfer_id).val();
                 var transfer_date=$("#today_date").val();
                 var vehicle_type_id=$("#transfer_vehicle_type__"+transfer_id).val();

                  var transfer_type = $("#transfer_type__" + transfer_id).val();
                  if(transfer_type=="from-airport")
                  {
                    var airport_id=$("#transfer_from_airport__"+transfer_id).val();
                     var city_transfer=$("#transfer_to_city__"+transfer_id).val();
                  }
                  else if(transfer_type=="to-airport")
                  {
                    var airport_id=$("#transfer_to_airport__"+transfer_id).val();
                    var city_transfer=$("#transfer_from_city__"+transfer_id).val();
                  }
                var error = 0;
                    if (transfer_own_id == "") {
                        alert("Please select transfer first");
                        error++;
                    }



                if (error == 0) {

                    $.ajax({

                        url: "{{route('fetchGuidesTransfer')}}",

                        type: "GET",

                        data: {
                            "country_id": country_id,
                            "airport_id":airport_id,
                        "vehicle_type_id":vehicle_type_id,
                        "transfer_date":transfer_date,
                        "city_id":city_transfer,
                        "request_type":"admin",
                        },

                        success: function (response) {
                            $("#transfer_guide_day_index").val(transfer_id);
                            $("#transfer_guide_div").html(response);
                            $("#selectTransferGuideModal").modal("show");

                        }

                    }).done(function(){
                var guideDiv=$(".guide-div");
                guideDiv.on("click",function(){
                 
                    this.getElementsByClassName("tile-label")[0].click()
                })
            });

                }

            });



               $(document).on("click", "#include_transfer_guide", function (e) {

                    var error=0;
                     if ($("input[name='guide']:checked").val()=="" || $("input[name='guide']:checked").val() == undefined) {
                    e.preventDefault();
                    alert("Please Select Guide first");
                      error++;
                      $("#transfer_guide_div").css("border", "1px solid #cf3c63");
                      
                } else

                {
                     $("#transfer_guide_div").css("border","");
                }

                if(error==0)
                {
                      var transfer_guide_day_index = $("#transfer_guide_day_index").val();

                var guide_id=$("input[name='guide']").val();
            var guide_cost=$("#guide_price_"+guide_id).text();
             var guide_name=$("#guide_name_"+guide_id).text();

             var transfer_cost=$("#transfer_cost__"+transfer_guide_day_index).val();

             $("#transfer_guide_id__"+transfer_guide_day_index).val(guide_id);
             $("#transfer_guide_name__"+transfer_guide_day_index).val(guide_name);
             $("#transfer_guide_cost__"+transfer_guide_day_index).val(guide_cost);
             var total_cost=parseInt(parseInt(transfer_cost)+parseInt(guide_cost));
             $("#transfer_total_cost__"+transfer_guide_day_index).val(total_cost);

              $('#selected_transfer_guide_label__'+transfer_guide_day_index).show();
               $('#transfer_guide_remove__'+transfer_guide_day_index).show();
             $('#selected_transfer_guide_name__'+transfer_guide_day_index).text(guide_name).show();
             $('#select_transfer_guide_btn__'+transfer_guide_day_index).hide();



                var total_cost = 0;
                $(".cal_cost").each(function () {
                    if ($(this).val() != "") {
                        total_cost += parseInt($(this).val());
                    }

                });
                $(".cal_activity_cost").each(function () {
                    if ($(this).val() != "") {
                        var activity_cost_array = $(this).val().split("///");
                        var total = 0;
                        for (var i = 0; i < activity_cost_array.length; i++) {
                            total += activity_cost_array[i] << 0;
                        }

                            total_cost += parseInt(total);
                    }
                });
                $("#total_cost").val(total_cost);

                setTimeout(function () {
                    $("#selectTransferGuideModal").modal("hide");
                }, 500);
                }

               
              

            });


			$(document).on("click", "#search_transfer", function () {
				var transfer_type = $("#transfer_type").val();
				var transfer_id = $("#transfer_day_index").val();
				if (transfer_type != "") {
					$("#transfer_div").html("");

					var country_id = $("#days_country__" + transfer_id).val();
					var city_id = $("#days_city__" + transfer_id).val();
					if (country_id == undefined) {
						var transfer_previous_id = transfer_id - 1;
						country_id = $("#days_country__" + transfer_previous_id).val();
						city_id = $("#days_country__" + transfer_previous_id).val();

					}
					if (country_id == null) {
						alert("Please select country first");
					} else if (city_id == 0) {
						alert("Please select city first");
					} else {

						if (transfer_type == "from-airport") {

							$("#transfer_div").html(
								"<div class='jumbotron'><h3 class='text-center' style='margin: 60px;'><b>Searching....</b></h3></div>"
								);
							var from_airport = $("#from_airport").val();
							var to_city = $("#to_city").val();
							if (from_airport == 0) {
								alert("Please select airport");
							} else if (to_city == 0) {
								alert("Please select city");
							} else {
								$.ajax({
									url: "{{route('itinerary-get-transfer')}}",
									data: {
										"country_id": country_id,
										"city_id": city_id,
										"transfer_type": transfer_type,
										"from_airport": from_airport,
										"to_city": to_city,
									},
									type: "get",
									success: function (response) {
										$("#transfer_div").html(response);
									}
								});
							}
						} else if (transfer_type == "to-airport") {

							$("#transfer_div").html(
								"<div class='jumbotron'><h3 class='text-center' style='margin: 60px;'><b>Searching....</b></h3></div>"
								);
							var from_city = $("#from_city").val();
							var to_airport = $("#to_airport").val();
							if (from_city == 0) {
								alert("Please select city");
							} else if (to_airport == 0) {
								alert("Please select airport");
							} else {
								$.ajax({
									url: "{{route('itinerary-get-transfer')}}",
									data: {
										"country_id": country_id,
										"city_id": city_id,
										"transfer_type": transfer_type,
										"from_city": from_city,
										"to_airport": to_airport
									},
									type: "get",
									success: function (response) {
										$("#transfer_div").html(response);
									}
								});
							}

						} else {

							$("#transfer_div").html(
								"<div class='jumbotron'><h3 class='text-center' style='margin: 60px;'><b>Searching....</b></h3></div>"
								);
							var from_city = $("#from_city").val();
							var to_city = $("#to_city").val();
							if (from_city == 0) {
								alert("Please select from city");
							} else if (to_city == 0) {
								alert("Please select to city");
							} else {
								$.ajax({
									url: "{{route('itinerary-get-transfer')}}",
									data: {
										"country_id": country_id,
										"city_id": city_id,
										"transfer_type": transfer_type,
										"from_city": from_city,
										"to_city": to_city
									},
									type: "get",
									success: function (response) {
										$("#transfer_div").html(response);
									}
								});
							}

						}


					}

				} else {
					alert("Please select all fields");
				}
			});
		</script>


		<script>
			// $(document).on("click", ".select_hotel", function (e) {

			// 	var hotel_no_of_days = $("#hotel_no_of_days").val();
			// 	if (hotel_no_of_days != "") {

			// 		$(".select_hotel").each(function () {
			// 			$(this).find('.tick').css("display", "none");
			// 			$(this).removeClass('selected_hotel');

			// 		});
			// 		var hotel_day_index = $("#hotel_day_index").val();

			// 		var hotel_id = $(this).attr('id').split("__")[1];
			// 		var hotel_name = $(this).find('.card-title').text();
			// 		$("#hotel_id__" + hotel_day_index).val("");
			// 		$("#selected_hotel_name__" + hotel_day_index).text("");
			// 		$("#hotel_name__" + hotel_day_index).val("");
			// 		$("#room_name__" + hotel_day_index).val("");
			// 		$("#hotel_cost__" + hotel_day_index).val("");
			// 		$("#hotel_no_of_days__" + hotel_day_index).val("");
			// 		if ($("input[name='room_type_" + hotel_id + "']:checked").val()) {

			// 			$(".select_hotel").each(function () {
			// 				var id = this.id;
			// 				var hotel_select_id = $(this).attr('id').split("__")[1];

			// 				if (id != "hotel__" + hotel_id) {
			// 					$("input[name='room_type_" + hotel_select_id + "']").prop("checked", false);
			// 				}

			// 			});

			// 			$(this).find('.tick').css("display", "block");

			// 			$(this).addClass('selected_hotel');
			// 			var room_index = $("input[name='room_type_" + hotel_id + "']:checked").val();
			// 			// var hotel_day_index=$("#hotel_day_index").val();
			// 			// var hotel_id=$(this).attr('id').split("__")[1];
			// 			// var hotel_name=$(this).find('.card-title').text();
			// 			// var hotel_cost=$(this).find('.search_hotel_cost').val();
			// 			var room_name = $('input[name="room_name_' + room_index + '_' + hotel_id + '"]').val();
			// 			var hotel_cost = $('input[name="room_price_' + room_index + '_' + hotel_id + '"]').val();

			// 			var days_country = $("#days_country__" + hotel_day_index).val();
			// 			var days_city = $("#days_city__" + hotel_day_index).val();
			// 			for ($i = 0; $i < hotel_no_of_days; $i++) {


			// 				if ($i > 0) {
			// 					$("#days_city_input__" + hotel_day_index).val(days_city);
			// 					$("#days_country__" + hotel_day_index).val(days_country).trigger('change');

			// 				}

			// 				$("#hotel_id__" + hotel_day_index).val(hotel_id);
			// 				$("#selected_hotel_name__" + hotel_day_index).text(hotel_name);
			// 				$("#hotel_name__" + hotel_day_index).val(hotel_name);
			// 				$("#room_name__" + hotel_day_index).val(room_name);
			// 				$("#hotel_cost__" + hotel_day_index).val(hotel_cost);
			// 				if ($i == 0) {
			// 					$("#hotel_no_of_days__" + hotel_day_index).val(hotel_no_of_days);
			// 				} else {
			// 					$("#hotel_no_of_days__" + hotel_day_index).val(0);
			// 				}

			// 				$("#hotels_select__" + hotel_day_index).css({
			// 					'cursor': 'not-allowed',
			// 					'pointer-events': 'none'
			// 				});
			// 				$("#days_country__" + hotel_day_index).parent().find('.select2-selection').css({
			// 					'cursor': 'not-allowed',
			// 					'pointer-events': 'none'
			// 				});
			// 				if ($i == 0) {
			// 					$("#days_city__" + hotel_day_index).parent().find('.select2-selection').css({
			// 						'cursor': 'not-allowed',
			// 						'pointer-events': 'none'
			// 					});
			// 				}

			// 				hotel_day_index = parseInt(parseInt(hotel_day_index) + 1);


			// 			}


			// 			// $("#days_country__"+hotel_day_index).val(days_country).trigger('change');	
			// 			// $("#hotels_select__"+hotel_day_index).css({'cursor':'not-allowed','pointer-events':'none'});
			// 			// $("#days_country__"+hotel_day_index).parent().find('.select2-selection').css({'cursor':'not-allowed','pointer-events':'none'});
			// 			// $("#hotel_id__"+hotel_day_index).val(hotel_id);
			// 			// $("#selected_hotel_name__"+hotel_day_index).text(hotel_name);
			// 			// $("#hotel_name__"+hotel_day_index).val(hotel_name);
			// 			// $("#room_name__"+hotel_day_index).val(room_name);
			// 			// $("#hotel_cost__"+hotel_day_index).val(0);
			// 			// $("#hotel_no_of_days__"+hotel_day_index).val(0);


			// 			var total_cost = 0;
			// 			$(".cal_cost").each(function () {
			// 				if ($(this).val() != "") {
			// 					total_cost += parseInt($(this).val());
			// 				}

			// 			});
			// 			$(".cal_activity_cost").each(function () {
			// 				if ($(this).val() != "") {
			// 					var activity_cost_array = $(this).val().split("///");
			// 					var total = 0;
			// 					for (var i = 0; i < activity_cost_array.length; i++) {
			// 						total += activity_cost_array[i] << 0;
			// 					}

			// 					total_cost += parseInt(total);
			// 				}
			// 			});
			// 			$("#total_cost").val(total_cost);
			// 			setTimeout(function () {
			// 				$("#selectHotelModal").modal("hide");
			// 			}, 500);
			// 		} else {
			// 			// alert(" Please select room type");
			// 		}

			// 	} else {
			// 		alert("Please select no. of Days");
			// 	}

			// });

			// $(document).on("click", ".select_sightseeing1", function () {
			// 	$(".select_sightseeing").each(function () {
			// 		$(this).find('.tick').css("display", "none");
			// 		$(this).removeClass('selected_sightseeing');

			// 	});


			// 	$(this).find('.tick').css("display", "block");
			// 	$(this).addClass('selected_sightseeing');

			// 	var sightseeing_day_index = $("#sightseeing_day_index").val();

			// 	var sightseeing_id = $(this).attr('id').split("__")[1];

			// 	var sightseeing_name = $(this).find('.card-title').text();
			// 	var sightseeing_cost = $(this).find('.search_sightseeing_cost').val();

			// 	$("#sightseeing_id__" + sightseeing_day_index).val(sightseeing_id);
			// 	$("#selected_sightseeing_name__" + sightseeing_day_index).text(sightseeing_name);
			// 	$("#sightseeing_name__" + sightseeing_day_index).val(sightseeing_name);
			// 	$("#sightseeing_cost__" + sightseeing_day_index).val(sightseeing_cost);

			// 	var total_cost = 0;
			// 	$(".cal_cost").each(function () {
			// 		if ($(this).val() != "") {
			// 			total_cost += parseInt($(this).val());
			// 		}

			// 	});
			// 	$(".cal_activity_cost").each(function () {
			// 		if ($(this).val() != "") {
			// 			var activity_cost_array = $(this).val().split("///");
			// 			var total = 0;
			// 			for (var i = 0; i < activity_cost_array.length; i++) {
			// 				total += activity_cost_array[i] << 0;
			// 			}

			// 			total_cost += parseInt(total);
			// 		}
			// 	});
			// 	$("#total_cost").val(total_cost);

			// 	setTimeout(function () {
			// 		$("#selectSightseeingModal").modal("hide");
			// 	}, 500);

			// });



            $(document).on("click", "#include_sightseeing", function (e) {

                    var error=0;
                    if($('#sightseeing_details_tour_type').val()=="private")
                    {
                if (!$("input[name='vehcile_type']:checked").val()) {
                    e.preventDefault();
                    alert("Please Select Vehicle Type first");
                    error++;
                } else

                {
                    $("#vehcile_type_table").css("border", "1px solid #9e9e9e");
                     if ($("input[name='selected_guide_id']").val()==0) {
                    e.preventDefault();
                    alert("Please Select Guide first");
                      error++;
                } else

                {
                    $("#guide_div").css("border", "1px solid #9e9e9e");
                }

                if ($("input[name='selected_driver_id']").val()==0) {
                    e.preventDefault();
                    $("#driver_div").css("border", "1px solid #cf3c63");
                    alert("Please Select Driver first");
                      error++;
                } else

                {
                    $("#driver_div").css("border", "1px solid #9e9e9e");
                }
                }
            }

                if(error==0)
                {
                      var sightseeing_day_index = $("#sightseeing_day_index").val();

                var sightseeing_id = $("#sightseeing_details_id").val();

                var sightseeing_name = $("#sightseeing_details_name").text();
                var sightseeing_tour_type = $("#sightseeing_details_tour_type").val();
                if(sightseeing_tour_type=="private")
                {
                 var sightseeing_adult_cost = $("#sightseeing_adult_cost").val();
                }
                else
                {
                       var sightseeing_adult_cost = $("#sightseeing_group_cost").val();
                }


                var selected_vehicle_type_id = $("#selected_vehicle_type_id").val();
                var selected_vehicle_type_name = $("#selected_vehicle_type_name").val();
                var selected_guide_id = $("#selected_guide_id").val();
                var selected_guide_name = $("#selected_guide_name").val();
                var selected_guide_cost = $("#selected_guide_cost").val();
                var selected_driver_id = $("#selected_driver_id").val();
                var selected_driver_name = $("#selected_driver_name").val();
                var selected_driver_cost = $("#selected_driver_cost").val();
                var sightseeing_total_cost = $("#sightseeing_total_cost").val();
                var sightseeing_additional_cost = $("#sightseeing_additional_cost").val();
                var sightseeing_cities = $("#sightseeing_cities").val();



                $("#sightseeing_id__" + sightseeing_day_index).val(sightseeing_id);
                $("#selected_sightseeing_name__" + sightseeing_day_index).text(sightseeing_name);
                $("#sightseeing_name__" + sightseeing_day_index).val(sightseeing_name);
                 $("#sightseeing_tour_type__" + sightseeing_day_index).val(sightseeing_tour_type);
                $("#sightseeing_vehicle_type__" + sightseeing_day_index).val(selected_vehicle_type_id);
                $("#sightseeing_guide_id__" + sightseeing_day_index).val(selected_guide_id);
                $("#sightseeing_guide_name__" + sightseeing_day_index).val(selected_guide_name);  
                $("#sightseeing_guide_cost__" + sightseeing_day_index).val(selected_guide_cost); 
                $("#sightseeing_driver_id__" + sightseeing_day_index).val(selected_driver_id);   
                $("#sightseeing_driver_name__" + sightseeing_day_index).val(selected_driver_name);
                $("#sightseeing_driver_cost__" + sightseeing_day_index).val(selected_driver_cost);
                $("#sightseeing_adult_cost__" + sightseeing_day_index).val(sightseeing_adult_cost);
                $("#sightseeing_cost__" + sightseeing_day_index).val(sightseeing_total_cost);
                $("#sightseeing_additional_cost__" + sightseeing_day_index).val(sightseeing_additional_cost);
                $("#sightseeing_cities__" + sightseeing_day_index).val(sightseeing_cities);

                $(".sightseeing_guide_id").slice(sightseeing_day_index).each(function()
                {
                    var id=$(this).attr("id");
                    var actual_id=id.split("__")[1];

                    if(actual_id>sightseeing_day_index)
                    {
                        if($("#sightseeing_guide_id__"+actual_id).val()=="")
                        {
                             $("#sightseeing_vehicle_type__"+actual_id).val(selected_vehicle_type_id);
                            $("#sightseeing_guide_id__"+actual_id).val(selected_guide_id);
                    $("#sightseeing_guide_name__"+actual_id).val(selected_guide_name);

                    $("#sightseeing_driver_id__"+actual_id).val(selected_driver_id);
                    $("#sightseeing_driver_name__"+actual_id).val(selected_driver_name); 
                        }
                       
                    }
                    
                });

                var total_cost = 0;
                $(".cal_cost").each(function () {
                    if ($(this).val() != "") {
                        total_cost += parseInt($(this).val());
                    }

                });
                $(".cal_activity_cost").each(function () {
                    if ($(this).val() != "") {
                        var activity_cost_array = $(this).val().split("///");
                        var total = 0;
                        for (var i = 0; i < activity_cost_array.length; i++) {
                            total += activity_cost_array[i] << 0;
                        }

                            total_cost += parseInt(total);
                    }
                });
                $("#total_cost").val(total_cost);

                setTimeout(function () {
                    $("#selectSightseeingModal").modal("hide");
                }, 500);
                 $("#sightseeing_remove__"+sightseeing_day_index).show();
                }

               
              

            });


			// $(document).on("click", ".select_activity", function () {

			// 	if ($(this).find('.tick').css("display") == "block") {
			// 		$(this).find('.tick').css("display", "none");
			// 		$(this).removeClass('selected_activity');
			// 	} else {
			// 		$(this).find('.tick').css("display", "block");
			// 		$(this).addClass('selected_activity');

			// 	}

			// 	var activity_id = [];
			// 	var activity_name = [];
			// 	var activity_cost = [];

			// 	var activity_day_index = $("#activities_day_index").val();
			// 	var count = 0;
			// 	var total_activity_cost = 0;
			// 	$(".selected_activity").each(function () {
			// 		activity_id[count] = $(this).attr('id').split("__")[1];
			// 		activity_name[count] = $(this).find('.card-title').text();
			// 		activity_cost[count] = $(this).find('.search_activity_cost').val();

			// 		total_activity_cost += parseInt($(this).find('.search_activity_cost').val());

			// 		count++;
			// 	});

			// 	$("#activity_id__" + activity_day_index).val(activity_id.join("///"));
			// 	$("#selected_activity_name__" + activity_day_index).text(activity_name.join(" , "));
			// 	$("#activity_name__" + activity_day_index).val(activity_name.join("///"));
			// 	$("#activity_cost__" + activity_day_index).val(activity_cost.join("///"));

			// 	var total_cost = 0;
			// 	$(".cal_cost").each(function () {
			// 		if ($(this).val() != "") {
			// 			total_cost += parseInt($(this).val());
			// 		}

			// 	});
			// 	$(".cal_activity_cost").each(function () {
			// 		if ($(this).val() != "") {
			// 			var activity_cost_array = $(this).val().split("///");
			// 			var total = 0;
			// 			for (var i = 0; i < activity_cost_array.length; i++) {
			// 				total += activity_cost_array[i] << 0;
			// 			}

			// 			total_cost += parseInt(total);
			// 		}
			// 	});
			// 	$("#total_cost").val(total_cost);
   //              $("#activity_remove__"+activity_day_index).show();


			// });





			$(document).on("click", ".select_transfer", function () {
				$(".select_transfer").each(function () {
					$(this).find('.tick').css("display", "none");
					$(this).removeClass('selected_transfer');

				});


				$(this).find('.tick').css("display", "block");
				$(this).addClass('select_transfer');

				var transfer_day_index = $("#transfer_day_index").val();

				var transfer_id = $(this).attr('id').split("__")[1];

				var transfer_name = $(this).find('.card-title').text();
				var transfer_cost = $(this).find('.search_transfer_cost').val();
				var from_airport = $(this).find('.search_transfer_from_airport').val();
				var to_airport = $(this).find('.search_transfer_to_airport').val();
				var from_city = $(this).find('.search_transfer_from_city').val();
				var to_city = $(this).find('.search_transfer_to_city').val();
                var vehicle_type = $(this).find('.search_vehicle_type').val();


				var transfer_type = $("#transfer_type").val();
				var pickup_location = $("#pickup_location").val();
				var dropoff_location = $("#dropoff_location").val();



				$("#transfer_id__" + transfer_day_index).val(transfer_id);
				$("#transfer_name__" + transfer_day_index).val(transfer_name);
				$("#transfer_type__" + transfer_day_index).val(transfer_type);
                $("#transfer_vehicle_type__" + transfer_day_index).val(vehicle_type);
				$("#selected_transfer_name__" + transfer_day_index).text(transfer_name);
				if (transfer_type == "from-airport") {

					$("#transfer_from_airport__" + transfer_day_index).val(from_airport);
					$("#transfer_to_city__" + transfer_day_index).val(to_city);
					$("#transfer_dropoff__" + transfer_day_index).val(dropoff_location);
					$("#transfer_pickup__" + transfer_day_index).val("");

					$("#transfer_from_city__" + transfer_day_index).val("");
					$("#transfer_to_airport__" + transfer_day_index).val("");

                    $("#transfer_guide_id__"+transfer_day_index).val("");
                     $("#transfer_guide_name__"+transfer_day_index).val("");
                      $("#transfer_guide_cost__"+transfer_day_index).val("");


                    $("#select_transfer_guide_btn__"+transfer_day_index).show();

                    $('#selected_transfer_guide_label__'+transfer_day_index).hide();
                    $('#transfer_guide_remove__'+transfer_day_index).hide();
                    $('#selected_transfer_guide_name__'+transfer_day_index).text("").hide();


				} else if (transfer_type == "to-airport") {
					$("#transfer_from_airport__" + transfer_day_index).val("");
					$("#transfer_to_city__" + transfer_day_index).val("");
					$("#transfer_from_city__" + transfer_day_index).val(from_city);
					$("#transfer_to_airport__" + transfer_day_index).val(to_airport);

					$("#transfer_dropoff__" + transfer_day_index).val("");
					$("#transfer_pickup__" + transfer_day_index).val(pickup_location);

                      $("#transfer_guide_id__"+transfer_day_index).val("");
                     $("#transfer_guide_name__"+transfer_day_index).val("");
                      $("#transfer_guide_cost__"+transfer_day_index).val("");


                      $("#select_transfer_guide_btn__"+transfer_day_index).show();

                    $('#selected_transfer_guide_label__'+transfer_day_index).hide();
                    $('#transfer_guide_remove__'+transfer_day_index).hide();
                    $('#selected_transfer_guide_name__'+transfer_day_index).text("").hide();

				} else if (transfer_type == "city") {
					$("#transfer_from_airport__" + transfer_day_index).val("");
					$("#transfer_to_airport__" + transfer_day_index).val("");
					$("#transfer_to_city__" + transfer_day_index).val(to_city);
					$("#transfer_from_city__" + transfer_day_index).val(from_city);
					$("#transfer_dropoff__" + transfer_day_index).val(dropoff_location);
					$("#transfer_pickup__" + transfer_day_index).val(pickup_location);

                      $("#transfer_guide_id__"+transfer_day_index).val("");
                     $("#transfer_guide_name__"+transfer_day_index).val("");
                      $("#transfer_guide_cost__"+transfer_day_index).val("");

                      $("#select_transfer_guide_btn__"+transfer_day_index).hide();
                       $('#selected_transfer_guide_label__'+transfer_day_index).hide();
                    $('#transfer_guide_remove__'+transfer_day_index).hide();
                    $('#selected_transfer_guide_name__'+transfer_day_index).text("").hide();
				}


				$("#transfer_cost__" + transfer_day_index).val(transfer_cost);
                $("#transfer_total_cost__" + transfer_day_index).val(transfer_cost);

				var total_cost = 0;
				$(".cal_cost").each(function () {
					if ($(this).val() != "") {
						total_cost += parseInt($(this).val());
					}

				});
				$(".cal_activity_cost").each(function () {
					if ($(this).val() != "") {
						var activity_cost_array = $(this).val().split("///");
						var total = 0;
						for (var i = 0; i < activity_cost_array.length; i++) {
							total += activity_cost_array[i] << 0;
						}

						total_cost += parseInt(total);
					}
				});
				$("#total_cost").val(total_cost);
				setTimeout(function () {
					$("#selectTransferModal").modal("hide");
				}, 500);

                 $("#transfer_remove__"+transfer_day_index).show();

			});



            $(document).on("click",".sightseeing_remove",function()
            {
                if(confirm("Are you sure that you want to remove this sightseeing?")==true)
                {
                        var id=this.id
                    var sightseeing_day_index=id.split("__")[1];
                      $("#sightseeing_id__" + sightseeing_day_index).val("");
                $("#selected_sightseeing_name__" + sightseeing_day_index).text("");
                $("#sightseeing_name__" + sightseeing_day_index).val("");
                 $("#sightseeing_tour_type__" + sightseeing_day_index).val("");
                // $("#sightseeing_vehicle_type__" + sightseeing_day_index).val("");
                // $("#sightseeing_guide_id__" + sightseeing_day_index).val("");
                // $("#sightseeing_guide_name__" + sightseeing_day_index).val("");  
                $("#sightseeing_guide_cost__" + sightseeing_day_index).val(""); 
                // $("#sightseeing_driver_id__" + sightseeing_day_index).val("");   
                // $("#sightseeing_driver_name__" + sightseeing_day_index).val("");
                $("#sightseeing_driver_cost__" + sightseeing_day_index).val("");
                $("#sightseeing_adult_cost__" + sightseeing_day_index).val("");
                 $("#sightseeing_additional_cost__" + sightseeing_day_index).val("");
                $("#sightseeing_cost__" + sightseeing_day_index).val("");
                $("#sightseeing_cities__" + sightseeing_day_index).val("");

                    $("#"+id).css("display","none");

                    var total_cost = 0;
                $(".cal_cost").each(function () {
                    if ($(this).val() != "") {
                        total_cost += parseInt($(this).val());
                    }

                });
                $(".cal_activity_cost").each(function () {
                    if ($(this).val() != "") {
                        var activity_cost_array = $(this).val().split("///");
                        var total = 0;
                        for (var i = 0; i < activity_cost_array.length; i++) {
                            total += activity_cost_array[i] << 0;
                        }

                        total_cost += parseInt(total);
                    }
                });
                $("#total_cost").val(total_cost);

                }
                
            });

             $(document).on("click",".activity_remove",function()
            {
                if(confirm("Are you sure that you want to remove this activity?")==true)
                {
                        var id=this.id
                    var activity_day_index=id.split("__")[1];
                      $("#activity_id__" + activity_day_index).val("");
                $("#selected_activity_name__" + activity_day_index).text("");
                $("#activity_name__" + activity_day_index).val("");
                $("#activity_cost__" + activity_day_index).val("");

                    $("#"+id).css("display","none");

                    var total_cost = 0;
                $(".cal_cost").each(function () {
                    if ($(this).val() != "") {
                        total_cost += parseInt($(this).val());
                    }

                });
                $(".cal_activity_cost").each(function () {
                    if ($(this).val() != "") {
                        var activity_cost_array = $(this).val().split("///");
                        var total = 0;
                        for (var i = 0; i < activity_cost_array.length; i++) {
                            total += activity_cost_array[i] << 0;
                        }

                        total_cost += parseInt(total);
                    }
                });
                $("#total_cost").val(total_cost);

                }
                
            });


             $(document).on("click",".transfer_remove",function()
            {
                if(confirm("Are you sure that you want to remove this transfer?")==true)
                {
                    var id=this.id
                    var transfer_day_index=id.split("__")[1];
                    $("#transfer_id__" + transfer_day_index).val("");
                    $("#transfer_name__" + transfer_day_index).val("");
                    $("#transfer_type__" + transfer_day_index).val("");
                    $("#selected_transfer_name__" + transfer_day_index).text("");

                    $("#transfer_from_airport__" + transfer_day_index).val("");
                    $("#transfer_to_city__" + transfer_day_index).val("");
                    $("#transfer_dropoff__" + transfer_day_index).val("");
                    $("#transfer_pickup__" + transfer_day_index).val("");

                    $("#transfer_from_city__" + transfer_day_index).val("");
                    $("#transfer_to_airport__" + transfer_day_index).val("");

                    $("#transfer_vehicle_type__" + transfer_day_index).val("");
                    $("#transfer_guide_id__" + transfer_day_index).val("");
                     $("#transfer_guide_name__" + transfer_day_index).val("");
                      $("#transfer_guide_cost__" + transfer_day_index).val("");


                    $("#transfer_cost__" + transfer_day_index).val("");
                     $("#transfer_total_cost__" + transfer_day_index).val("");

                    $("#"+id).css("display","none");

                    $("#transfer_guide_remove__"+transfer_day_index).hide();
                     $("#selected_transfer_guide_name__"+transfer_day_index).hide();
                      $("#selected_transfer_guide_label__"+transfer_day_index).hide();

                    var total_cost = 0;
                $(".cal_cost").each(function () {
                    if ($(this).val() != "") {
                        total_cost += parseInt($(this).val());
                    }

                });
                $(".cal_activity_cost").each(function () {
                    if ($(this).val() != "") {
                        var activity_cost_array = $(this).val().split("///");
                        var total = 0;
                        for (var i = 0; i < activity_cost_array.length; i++) {
                            total += activity_cost_array[i] << 0;
                        }

                        total_cost += parseInt(total);
                    }
                });
                $("#total_cost").val(total_cost);

                }
                
            });




               $(document).on("click",".transfer_guide_remove",function()
            {
                if(confirm("Are you sure that you want to remove guide from this transfer?")==true)
                {
                    var id=this.id;
                    var transfer_day_index=id.split("__")[1];
                    $("#transfer_guide_id__" + transfer_day_index).val("");
                     $("#transfer_guide_name__" + transfer_day_index).val("");
                      $("#transfer_guide_cost__" + transfer_day_index).val("");

                    var transfer_cost=$("#transfer_cost__"+transfer_day_index).val();
                    var total_cost=parseInt(transfer_cost);
                     $("#transfer_total_cost__"+transfer_day_index).val(total_cost);

                    $("#"+id).css("display","none");

                     $("#select_transfer_guide_btn__"+transfer_day_index).show();

                    $("#transfer_guide_remove__"+transfer_day_index).hide();
                     $("#selected_transfer_guide_name__"+transfer_day_index).hide();
                      $("#selected_transfer_guide_label__"+transfer_day_index).hide();

                    var total_cost = 0;
                $(".cal_cost").each(function () {
                    if ($(this).val() != "") {
                        total_cost += parseInt($(this).val());
                    }

                });
                $(".cal_activity_cost").each(function () {
                    if ($(this).val() != "") {
                        var activity_cost_array = $(this).val().split("///");
                        var total = 0;
                        for (var i = 0; i < activity_cost_array.length; i++) {
                            total += activity_cost_array[i] << 0;
                        }

                        total_cost += parseInt(total);
                    }
                });
                $("#total_cost").val(total_cost);

                }
                
            });
		</script>

		<script>
			$(document).on("click", "#discard_itinerary", function ()

				{

					window.history.back();

				})
		</script>

		{{-- <script>
			$(document).on("click", "#save_itinerary", function () {
				var tour_name = $("#tour_name").val();
				var no_of_days = $("#no_of_days").val();
				var tour_description = CKEDITOR.instances.tour_description.getData();
				var tour_exclusions = CKEDITOR.instances.tour_exclusions.getData();
				var tour_terms_and_conditions = CKEDITOR.instances.tour_terms_and_conditions.getData();
				var tour_cancellation = CKEDITOR.instances.tour_cancellation.getData();
				var total_cost = $("#total_cost").val();

				if (tour_name.trim() == "")

				{

					$("#tour_name").css("border", "1px solid #cf3c63");

				} else

				{

					$("#tour_name").css("border", "1px solid #9e9e9e");

				}

				if (tour_description.trim() == "")

				{

					$("#cke_tour_description").css("border", "1px solid #cf3c63");

				} else

				{

					$("#cke_tour_description").css("border", "1px solid #9e9e9e");

				}


				if (no_of_days.trim() == "0")

				{

					$("#no_of_days").parent().find(".select2-selection").css("border", "1px solid #cf3c63");

				} else

				{

					$("#no_of_days").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");

				}

				var days_country_error = 0;

				$(".days_country").slice(1).each(function () {

					var days_country_id = $(this).attr("id");
					if ($(this).val() == null) {

						$("#" + days_country_id).parent().find(".select2-selection").css("border",
							"1px solid #cf3c63");

						$("#" + days_country_id).parent().find(".select2-selection").focus();

						days_country_error++;

					} else {

						$("#" + days_country_id).parent().find(".select2-selection").css("border",
							"1px solid #9e9e9e");

					}



				});



				var days_city_error = 0;

				$(".days_city").slice(1).each(function () {

					var days_city_id = $(this).attr("id");

					if ($(this).val() == "0") {

						$("#" + days_city_id).parent().find(".select2-selection").css("border",
							"1px solid #cf3c63");

						$("#" + days_city_id).parent().find(".select2-selection").focus();

						days_city_error++;

					} else {

						$("#" + days_city_id).parent().find(".select2-selection").css("border",
							"1px solid #9e9e9e");

					}



				});


				var hotels_id_error = 0;
				var activity_id_error = 0;
				var sightseeing_id_error = 0;
				var days_description_error = 0;
				var days_title_error = 0;

				days_description_array = [];
				for ($i = 1; $i <= no_of_days; $i++) {

					// var days_description=CKEDITOR.instances['days_description__'+$i].getData();

					// days_description_array[($i-1)]=CKEDITOR.instances['days_description__'+$i].getData();

					var days_description = $('#days_description__' + $i).val();

					days_description_array[($i - 1)] = $('#days_description__' + $i).val();


					// if (days_description == "") {

					//        $("#days_description__"+$i).parent().css("border", "1px solid #cf3c63");

					//        $("#days_description__"+$i).parent().focus();

					//        days_description_error++;

					//    } else {

					//         $("#days_description__"+$i).parent().css("border", "1px solid #fff");

					//    }

					// if ($("#days_title__" + $i).val() == "") {

					//     $("#days_title__" + $i).css("border", "1px solid #cf3c63");

					//     $("#days_title__" + $i).focus();

					//     days_title_error++;

					// } else {

					//      $("#days_title__" + $i).css("border", "1px solid #9e9e9e");

					// }

					if ($("#hotel_id__" + $i).val() == "") {

						$("#hotel_id__" + $i).parent().css("border", "1px solid #cf3c63");

						$("#hotel_id__" + $i).parent().focus();

						hotels_id_error++;

					} else {

						$("#hotel_id__" + $i).parent().css("border", "1px solid #fff");

					}



					// if ($("#activity_id__" + $i).val() == "") {

					//         $("#activity_id__" + $i).parent().css("border", "1px solid #cf3c63");

					//         $("#activity_id__" + $i).parent().focus();

					//         activity_id_error++;

					//     } else {

					//          $("#activity_id__" + $i).parent().css("border", "1px solid #fff");

					//     }



					// if ($("#sightseeing_id__" + $i).val() == "") {

					//         $("#sightseeing_id__" + $i).parent().css("border", "1px solid #cf3c63");

					//         $("#sightseeing_id__" + $i).parent().focus();

					//         sightseeing_id_error++;

					//     } else {

					//          $("#sightseeing_id__" + $i).parent().css("border", "1px solid #fff");

					//     }
				}

				// var days_description=CKEDITOR.instances['days_description__'+$i].getData();
				// 	 days_description_array[($i-1)]=CKEDITOR.instances['days_description__'+$i].getData();


				var days_description = $('#days_description__' + $i).val();

				days_description_array[($i - 1)] = $('#days_description__' + $i).val();


				// if (days_description == "") {

				//        $("#days_description__"+$i).parent().css("border", "1px solid #cf3c63");

				//        $("#days_description__"+$i).parent().focus();

				//        days_description_error++;

				//    } else {

				//         $("#days_description__"+$i).parent().css("border", "1px solid #fff");

				//    }

				// if ($("#days_title__" + $i).val() == "") {

				//     $("#days_title__" + $i).css("border", "1px solid #cf3c63");

				//     $("#days_title__" + $i).focus();

				//     days_title_error++;

				// } else {

				//      $("#days_title__" + $i).css("border", "1px solid #9e9e9e");

				// }

				if (total_cost.trim() == "")

				{

					$("#total_cost").css("border", "1px solid #cf3c63");

				} else

				{

					$("#total_cost").css("border", "1px solid #9e9e9e");

				}

				if (tour_exclusions.trim() == "")

				{

					$("#cke_tour_exclusions").css("border", "1px solid #cf3c63");

				} else

				{

					$("#cke_tour_exclusions").css("border", "1px solid #9e9e9e");

				}

				if (tour_terms_and_conditions.trim() == "")

				{

					$("#cke_tour_terms_and_conditions").css("border", "1px solid #cf3c63");

				} else

				{

					$("#cke_tour_terms_and_conditions").css("border", "1px solid #9e9e9e");

				}

				if (tour_cancellation.trim() == "")

				{

					$("#cke_tour_cancellation").css("border", "1px solid #cf3c63");

				} else

				{

					$("#cke_tour_cancellation").css("border", "1px solid #9e9e9e");

				}


				if (tour_name.trim() == "")

				{

					$("#tour_name").focus();

				} else if (tour_description.trim() == "")

				{

					$("#cke_tour_description").focus();
				} else if (no_of_days.trim() == "0")

				{

					$("#no_of_days").parent().find(".select2-selection").focus();
				} else if (days_country_error > 0) {} else if (days_city_error > 0) {} else if (days_title_error >
					0) {} else if (days_description_error > 0) {

				} else if (hotels_id_error > 0) {} else if (activity_id_error > 0) {} else if (sightseeing_id_error >
					0) {} else if (total_cost.trim() == "") {
					$("#total_cost").focus();

				} else if (tour_exclusions.trim() == "") {
					$("#cke_tour_exclusions").focus();
				} else if (tour_terms_and_conditions.trim() == "")

				{

					$("#cke_tour_terms_and_conditions").focus();

				} else if (tour_cancellation.trim() == "")

				{

					$("#cke_tour_cancellation").focus();

				} else

				{

					$("#save_itinerary").prop("disabled", true);

					var formdata = new FormData($("#itinerary_form")[0]);

					formdata.append("tour_description", tour_description);
					formdata.append("tour_exclusions", tour_exclusions);
					formdata.append("tour_terms_and_conditions", tour_terms_and_conditions);
					formdata.append("tour_cancellation", tour_cancellation);
					formdata.append("days_description", JSON.stringify(days_description_array));
					$.ajax({

						url: "{{route('insert-itinerary')}}",

						enctype: "multipart/form-data",

						type: "POST",

						data: formdata,

						contentType: false,

						processData: false,

						success: function (response)

						{

							if (response.indexOf("exist") != -1)



							{



								swal("Already Exist!",

									"Itinerary already exists");



							} else if (response.indexOf("success") != -1)



							{



								swal({

										title: "Success",

										text: "Itinerary Created Successfully !",

										type: "success"

									},



									function () {



										location.reload();



									});



							} else if (response.indexOf("fail") != -1)



							{



								swal("ERROR", "Itinerary cannot be inserted right now! ");



							}

							$("#save_itinerary").prop("disabled", false);



						}

					});

				}

			});
		</script> --}}

</body>
<script>
	var tile=document.getElementsByClassName("parent-tile")
	
	for(var i=0;i<tile.length;i++){
		tile[i].addEventListener("click",function(){
			this.lastElementChild.click()
		   
			
		})
	  
	   
	}
     $(document).on("change","input[name='vehcile_type']",function()
        {   
            var sightseeing_day_index=$("#sightseeing_day_index").val();
            var country_id = $("#days_country__" + sightseeing_day_index).val();
            var sightseeing_id=$("input[name='sightseeing_details_id']").val();
            var vehicle_type_id=$(this).val();
             var vehicle_type_name=$("#vehcile_type_name_"+vehicle_type_id).val();
             var sightseeing_date=$("#today_date").val();

            $("input[name='selected_vehicle_type_id']").val(vehicle_type_id);
              $("input[name='selected_vehicle_type_name']").val(vehicle_type_name);
              $(".table-sightseeing-loader").show();

                    $("#guide_div").html("");
                    $("#driver_div").html("");
            $.ajax({
                url:"{{route('fetchGuidesSightseeing')}}",
                data:{"sightseeing_id":sightseeing_id,
                        "vehicle_type_id":vehicle_type_id,
                        "country_id":country_id,
                        "sightseeing_date":sightseeing_date,
                        "request_type":"admin"},
                type:"GET",
                success:function(response)
                {

                    $("#guide_div").html(response);
                    $(".table-sightseeing-loader").hide();

                    //empty guide id and cost because guides data refreshed
                    $("input[name='selected_guide_id']").val("");
                    $("input[name='selected_guide_cost']").val(0);
                      $("input[name='selected_guide_name']").val("");
                       $("#guide_div").show();
                       $("#guide_div_head").show();




                }
            }).done(function(){
                var guideDiv=$(".guide-div");
                guideDiv.on("click",function(){
                 
                    this.getElementsByClassName("tile-label")[0].click()
                })
            });

           
              $.ajax({
                url:"{{route('fetchDriversSightseeing')}}",
                data:{"sightseeing_id":sightseeing_id,
                        "vehicle_type_id":vehicle_type_id,
                    "country_id":country_id,
                    "sightseeing_date":sightseeing_date,
                "request_type":"admin"},
                type:"GET",
                success:function(response)
                {
                    $("#driver_div").html(response);
                    $(".table-sightseeing-loader").hide();

                    //empty guide id and cost because guides data refreshed
                    $("input[name='selected_driver_id']").val("");
                    $("input[name='selected_driver_cost']").val(0);
                      $("input[name='selected_driver_name']").val("");

                        $("#driver_div").show();
                         $("#driver_div_head").show();

                      
                },
            }).done(function(){
               
                var driverDiv=$(".driver-div");
                
                driverDiv.on("click",function(){
                   
                    this.getElementsByClassName("tile-label")[0].click()
                })
               
        
            });

            setTimeout(function()
            {
                   if($("#sightseeing_guide_id__"+sightseeing_day_index).val().trim()!="")
            {
                var guide_id=$("#sightseeing_guide_id__"+sightseeing_day_index).val().trim();
                $(".guide-div").each(function()
                {
                    console.log("Guide :"+$(this).find(":radio").val());
                    if($(this).find(":radio").val()==guide_id)
                    {
                        $(this).find(":radio").attr("checked","checked");
                           $(this).find(":radio").prop("checked",true).trigger("change");
                    }
                });
            }

            if($("#sightseeing_driver_id__"+sightseeing_day_index).val().trim()!="")
            {
                var driver_id=$("#sightseeing_driver_id__"+sightseeing_day_index).val().trim();
                $(".driver-div").each(function()
                {
                    console.log("Driver :"+$(this).find(":radio").val());
                    if($(this).find(":radio").val()==driver_id)
                    {
                        $(this).find(":radio").attr("checked","checked");
                           $(this).find(":radio").prop("checked",true).trigger("change");
                    }
                });
            }


            },1000);

          

           
        });

   function roundLikePHP(num, dec){
  var num_sign = num >= 0 ? 1 : -1;
  return parseFloat((Math.round((num * Math.pow(10, dec)) + (num_sign * 0.0001)) / Math.pow(10, dec)).toFixed(dec));
}

     $(document).on("change","#sightseeing_details_tour_type",function()
     {
        var details_tour_type=$(this).val();

        if(details_tour_type=="group")
        {
            var adult_cost=$("#sightseeing_group_cost").val();
            $("#guide_div").hide();
            $("#driver_div").hide();
              $("#guide_div_head").hide();
            $("#driver_div_head").hide();
            $("#vehicle_type_div").hide();
        }
        else
        {
             var adult_cost=$("#sightseeing_adult_cost").val();
              $("#guide_div").show();
            $("#driver_div").show();
            $("#guide_div_head").show();
            $("#driver_div_head").show();
            $("#vehicle_type_div").show();

        }
        $("#guide_div").html("");
            $("#driver_div").html("");
            adult_cost+=parseInt($("#sightseeing_additional_cost").val());
        $("#sightseeing_total_cost").val(adult_cost);
          $("#sightseeing_total_price_text").text("GEL "+adult_cost);

           $("input[name='selected_guide_id']").val("");
              $("input[name='selected_guide_cost']").val(0);
              $("input[name='selected_guide_name']").val("");
            $("input[name='selected_driver_id']").val("");
              $("input[name='selected_driver_cost']").val(0);
              $("input[name='selected_driver_name']").val("");
              $("input[name='vehcile_type']").prop("checked",false);


     });
       $(document).on("change","input[name='guide']",function()
        {
            var guide_id=$(this).val();
            var guide_cost=$("#guide_price_"+guide_id).text();
             var guide_name=$("#guide_name_"+guide_id).text();

            

            $("input[name='selected_guide_id']").val(guide_id);
              $("input[name='selected_guide_cost']").val(guide_cost);
              $("input[name='selected_guide_name']").val(guide_name);

              var driver_cost=$("input[name='selected_driver_cost']").val();


            var adult_count = 1;


            var sightseeing_tour_type=$("#sightseeing_details_tour_type").val();
              var sightseeing_additional_cost=$("#sightseeing_additional_cost").val();
            if(sightseeing_tour_type=="private")
            {
            var adult_price = $("#sightseeing_adult_cost").val();
            }
            else
            {
            var adult_price = $("#sightseeing_group_cost").val();
            }
            

            if (!adult_count) {
                adult_count = 0;
            }

            if (!adult_price) {
                adult_price = 0;
            }
             if(!sightseeing_additional_cost)
            {
                sightseeing_additional_cost=0;
            }
            else if(sightseeing_additional_cost=="")
            {
              sightseeing_additional_cost=0;  
            }



            var adult_final_price = parseFloat(adult_price * adult_count);
            
           var total_hotel_foodcost = 0;

            var total_price = parseFloat( adult_final_price + total_hotel_foodcost);

            var total_price_after_driver = parseFloat(parseFloat(driver_cost) + parseFloat(total_price));

                var total_price_after_guide = parseFloat(parseFloat(guide_cost) + total_price_after_driver);
                total_price_after_guide+=parseFloat(sightseeing_additional_cost);
                var total_price_after_guide_1=roundLikePHP(total_price_after_guide,0);


            $("#sightseeing_total_price_text").text("GEL "+total_price_after_guide_1);
            $("#sightseeing_total_cost").val(total_price_after_guide_1);
          
        });


        //get the cost and driver that is selected
           $(document).on("change","input[name='driver']",function()
        {
            var driver_id=$(this).val();
            var driver_cost=$("#driver_price_"+driver_id).text();
             var driver_name=$("#driver_name_"+driver_id).text();


            $("input[name='selected_driver_id']").val(driver_id);
              $("input[name='selected_driver_cost']").val(driver_cost);
              $("input[name='selected_driver_name']").val(driver_name);

               var guide_cost=$("input[name='selected_guide_cost']").val();

                var adult_count = 1;


            var sightseeing_tour_type=$("#sightseeing_details_tour_type").val();
             var sightseeing_additional_cost=$("#sightseeing_additional_cost").val();
            if(sightseeing_tour_type=="private")
            {
            var adult_price = $("#sightseeing_adult_cost").val();
            }
            else
            {
            var adult_price = $("#sightseeing_group_cost").val();
            }
            

            if (!adult_count) {
                adult_count = 0;
            }

            if (!adult_price) {
                adult_price = 0;
            }

            if(!sightseeing_additional_cost)
            {
                sightseeing_additional_cost=0;
            }
            else if(sightseeing_additional_cost=="")
            {
              sightseeing_additional_cost=0;  
            }




            var adult_final_price = parseFloat(adult_price * adult_count);
            
           var total_hotel_foodcost = 0;

            var total_price = parseFloat( adult_final_price + total_hotel_foodcost);

           var total_price_after_guide = parseFloat(parseFloat(guide_cost) + parseFloat(total_price));
                var total_price_after_driver = parseFloat(parseFloat(driver_cost) + total_price_after_guide);
                total_price_after_driver+=parseFloat(sightseeing_additional_cost);
                var total_price_after_driver_1=roundLikePHP(total_price_after_driver,0);


            $("#sightseeing_total_price_text").text("GEL "+total_price_after_driver_1);
            $("#sightseeing_total_cost").val(total_price_after_driver_1);

        });


           $(document).on("click",".view_more",function()
           {
            var clone=$(this).parent().clone();
            clone.find(".view_more").remove();
            clone.find(".more-vehicle-image").css("display","block");
             clone.find(".more-vehicle-image").parent().css("display","block");
             clone.find("img").removeAttr("width height").attr({"width":"250","height":"200"}).css("padding","10px");
            $("#vehicle_view_images").html(clone);
            $("#vehicleModal").modal("show");
           });
              $(document).on("click",".view_guide",function()
             {
                var id=$(this).attr("id").split("_")[1];

                $.ajax({
                    url:"{{route('fetchGuidesDetails')}}",
                    type:"GET",
                    data:{"guide_id":id},
                    success:function(response)
                    {
                        $("#guide_details_div").html(response);

                        $("#guideModal").modal("show");
                        $('#guide_details_div').animate({
                            scrollTop: $('#guide_details_div').position().top
                        }, 'slow');
                    }

                });
             });
              $(document).on("click",".view_driver",function()
             {
                var id=$(this).attr("id").split("_")[1];

                $.ajax({
                    url:"{{route('fetchDriversDetails')}}",
                    type:"GET",
                    data:{"driver_id":id},
                    success:function(response)
                    {
                        $("#driver_details_div").html(response);

                        $("#driverModal").modal("show");
                        $('#driver_details_div').animate({
                            scrollTop: $('#driver_details_div').position().top
                        }, 'slow');
                    }

                });
             });
	</script>
<script>
     $(document).on("change","#outstation_child",function()
{
    var html_data="";
    var child_count=$(this).val();
    for($i=1;$i<=child_count;$i++)
    {
        html_data+='<div class="col-md-2"> <div class="form-group"><label for="outstation_child_age'+$i+'" style="color:black">Child Age '+$i+' <span class="asterisk" >*</span></label><input type="text" id="outstation_child_age'+$i+'" name="outstation_child_age[]" class="form-control outstation_child_age"  onkeypress="javascript:return validateNumber(event)" maxlength=2 style="color:black" required></div></div>'
    }
    $("#outstation_child_age_div").html(html_data);
    $("#outstation_child_age_div").show();
});
        $(document).on("change","#airport_child",function()
{
    var html_data="";
    var child_count=$(this).val();
    for($i=1;$i<=child_count;$i++)
    {
        html_data+='<div class="col-md-3"> <div class="form-group"><label for="outstation_child_age'+$i+'" style="color:black">Child Age '+$i+' <span class="asterisk" >*</span></label><input type="text" id="airport_child_age'+$i+'" name="airport_child_age[]" class="form-control airport_child_age"  onkeypress="javascript:return validateNumber(event)" maxlength=2 style="color:black" required></div></div>'
    }
    $("#airport_child_age_div").html(html_data);
    $("#airport_child_age_div").show();
    $('#showchildage').show();
});
</script>

{{-- new code --}}

<script>
    $(document).on('change','.days_city',function(){
        var city_id = $(this).val();
        var city_actual_id = $(this).attr("id");
        var actual_id = city_actual_id.split("__");
        var country_id=$("#days_country__" + actual_id[1]).val();
        var newhotel=$('#newhotel__'+actual_id[1]).val();
       
        if(country_id=="")
        {

        }
        else if(city_id=="")
        {

        }
        else
        {
            $.ajax({
                    url :"{{route('group-hotel-search')}}",
                    data : {'city_id':city_id,
                            'country_id':country_id,
                            },
                    type:'GET',
                    success:function(reponse)
                    {
                      
                       $('#select_hotel'+actual_id[1]+'-'+newhotel) .html(
                                reponse);
                    }

            });
        }


    })
</script>
<script>
    $(document).on('change','.select_hotel',function(){
       
       
        var select_hotel = $(this).val();
        
        var hotel_actual_id = $(this).attr("id");
       	var mainhotel=hotel_actual_id.split('select_hotel');
         var newhotel=$('#newhotel__'+hotel_actual_id[1]).val();
        
         var mainhotelid= $('#'+hotel_actual_id).val();
        
         if(mainhotelid=="")
         {

         }
         else
         {
            
            $.ajax({
                url:'{{route('hotel-group-room-search')}}',
            data :{'mainhotelid':mainhotelid,
            		'row_id':mainhotel[1],
        			},
            type:'GET',
            success:function(data)
            {
               
                $('#showdata_'+mainhotel[1]).html(data);
            }
        });
         }
    })
</script>
<script>
    $(document).on('change','.room_selected_qty',function(){
    	
        var select_room_qty_id = $(this).attr("id");
        var mainid=select_room_qty_id.split('room_selected_qty');
        var selectqty=$('#'+select_room_qty_id).val();
        
        var room_currency=$('#room_currency'+mainid[1]).val();
        var room_rate=$('#room_rate'+mainid[1]).val();
        var total_amount=parseInt(room_rate) * parseInt(selectqty);
       
        $('#total_room_rate'+mainid[1]).val(total_amount);
        
        $('.room_grand_price'+mainid[1]).html(total_amount);
    });
</script>
<script>
	$(document).on('click','.add_hotel',function(){
			var last_hotel_id = $(this).attr("id");
			// alert(last_hotel_id);
			var mainid=last_hotel_id.split('-');
			var newmainid=parseInt(mainid) + 1;
			var city_id = $("#days_city__" + mainid[1]).val();
       
        var country_id=$("#days_country__" + mainid[1]).val()
   //      	alert(city_id);
   //      	alert(country_id);
			// $('#mainhoteldiv_'+mainid[1]).append('<div class="col-xl-6 col-12 hotels_select"><div class="hotellength"  ><select class="form-control select_hotel"  style="width:100%"><option value="0" selected="selected" hidden="hidden" disabled="disabled">SELECT Hotels</option></select></div></div>');

	})
</script>

</html>
