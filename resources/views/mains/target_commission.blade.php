@include('mains.includes.top-header')

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Target Commission</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Masters</li>
                                            <li class="breadcrumb-item active" aria-current="page">Target Commission
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($rights['add']==1)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Add New Target</h4>
                                </div>
                                <div class="box-body">
                                    <form class="package_form" action="javascript:void()" method="POST" id="menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <input type="hidden" name="menu_pid" value="0">
                                                    <label>Monthly Target Amount <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Enter Target Amount" id="target_amount" name="target_amount" autofocus onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                        </div>
                                          <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <input type="hidden" name="menu_pid" value="0">
                                                    <label>Commission <small>(in %)</small> <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Enter Commission Percentage" id="target_commission" name="target_commission" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-md-12">
                                                <button type="button"  id="save_target_commission" class="btn btn-rounded btn-primary mr-10">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">View Expense Category</h4>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example1" class="table table-bordered table-striped datatable">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>TARGET AMOUNT</th>
                                                 <th>COMMISSION (in %)</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($fetch_target_commission as $target_commission)
                                            <tr>
                                                <td>{{$target_commission->st_commission_id}}</td>
                                                 <td id="target_amount_{{$target_commission->st_commission_id}}">{{$target_commission->st_amount}}</td>
                                                  <td id="target_commission_{{$target_commission->st_commission_id}}">{{$target_commission->st_commission_per}}</td>
                                                  <td><button id="edit_{{$target_commission->st_commission_id}}" class="btn btn-default btn-rounded edit_target_commission"><i class="fa fa-pencil"></i></button></td>
                                            </tr>

                                            @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <h4 class="text-danger">No rights to access this page</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('mains.includes.footer')
    @include('mains.includes.bottom-footer')
       <div id="editModal" class="modal fade" role="dialog" style="z-index: 9999">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Expense Category</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="package_form" action="javascript:void()" method="POST" id="edit_menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10" >
                                        <h5 style="color:red" id="edit_target_commission_error"></h5>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                       <input type="hidden" id="edit_target_commission_id" name="target_commission_id">
                                                    <label>Monthly Target Amount <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Enter Target Amount" id="edit_target_amount" name="target_amount" autofocus onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                        </div>
                                          <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <input type="hidden" name="menu_pid" value="0">
                                                    <label>Commission <small>(in %)</small> <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Enter Commission Percentage" id="edit_target_commission" name="target_commission" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
            </div>
        </div>
        
      </div>
      <div class="modal-footer">
        
         <button type="button"  id="update_target_commission" class="btn btn-rounded btn-primary mr-10 ">Submit</button>
        <button type="button" class="btn btn-default mr-10 pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <script>
    $(document).ready(function()
    {
        $(".datatable").DataTable();
    });

    $(document).on("click","#save_target_commission",function()
    {
    var target_amount=$("#target_amount").val();
     var target_commission=$("#target_commission").val();

    if(target_amount.trim()=="")
    {
    $("#target_amount").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#target_amount").css("border","1px solid #9e9e9e");
    }

    if(target_commission.trim()=="")
    {
    $("#target_commission").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#target_commission").css("border","1px solid #9e9e9e");
    }
    
    
    if(target_amount.trim()=="")
    {
    $("#target_amount").focus();
    }
    else if(target_commission.trim()=="")
    {
    $("#target_commission").focus();
    }
    else
    {
    $("#save_target_commission").prop("disabled",true);
    var formdata=new FormData($("#menu_form")[0]);
    $.ajax({
    url:"{{route('target-commission-insert')}}",
    data: formdata,
    type:"POST",
    processData: false,
    contentType: false,
    success:function(response)
    {
    if(response.indexOf("exist")!=-1)
    {
    swal("Already Exist!", "Target Amount already exists");
    }
    else if(response.indexOf("success")!=-1)
    {
    swal({title:"Success",text:"Target Commission Created Successfully !",type: "success"},
    function(){
    location.reload();
    });
    }
    else if(response.indexOf("fail")!=-1)
    {
    swal("ERROR", "Target Commission cannot be inserted right now! ");
    }
    $("#save_target_commission").prop("disabled",false);
    }
    });
    }
    });
    </script>
    <script> 
    $(document).on("click",".edit_target_commission",function()
    {
        var id=this.id;

        var actual_id=id.split("_");
        var target_amount=$("#target_amount_"+actual_id[1]).text();
        var target_comm=$("#target_commission_"+actual_id[1]).text();
        $("#edit_target_amount").val(target_amount);
          $("#edit_target_commission").val(target_comm);
        $("#edit_target_commission_id").val(actual_id[1]);


        $("#editModal").modal("show"); 

    });
    $(document).on("click","#update_target_commission",function()
    {
   var target_amount=$("#edit_target_amount").val();
     var target_commission=$("#edit_target_commission").val();
    var error=0;
    var errors_array=[];
    
    if(target_amount.trim()=="")
    {
        errors_array.push("<li>Please Enter Target Amount</li>");
            error++;
    $("#edit_target_amount").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#edit_target_amount").css("border","1px solid #9e9e9e");
    }
    if(target_commission.trim()=="")
    {
        errors_array.push("<li>Please Enter Commission</li>");
            error++;
    $("#edit_target_commission").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#edit_target_commission").css("border","1px solid #9e9e9e");
    }

   
    if(error>0)
    {

            $("#edit_expense_category_error").html("<ul>"+errors_array.join("")+"</ul>");
            $("#edit_expense_category_error").focus();
    }
    else
    {
    $("#update_target_commission").prop("disabled",true);
            var formdata=new FormData($("#edit_menu_form")[0]);
            $.ajax({
                url:"{{route('target-commission-update')}}",
                data: formdata,
                type:"POST",
                processData: false,
                contentType: false,
                success:function(response)
                {
                    if(response.indexOf("exist")!=-1)
                    {
                        swal("Already Exist!", "Target Commission already exists");
                    }
                    else if(response.indexOf("success")!=-1)
                    {
                        swal({title:"Success",text:"Target Commission Updated Successfully !",type: "success"},
                            function(){
                                location.reload();
                            });
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        swal("ERROR", "Target Commission cannot be updated right now!");
                    }
                    $("#update_target_commission").prop("disabled",false);
                }
            });
    }
    });
    </script>

</body>
</html>