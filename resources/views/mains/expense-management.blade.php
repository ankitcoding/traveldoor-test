@include('mains.includes.top-header')

<style>

    .iti-flag {

        width: 20px;

        height: 15px;

        box-shadow: 0px 0px 1px 0px #888;

        background-image: url("{{asset('assets/images/flags.png')}}") !important;

        background-repeat: no-repeat;

        background-color: #DBDBDB;

        background-position: 20px 0

    }



    div#cke_1_contents {

        height: 250px !important;

    }

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('mains.includes.nav')

	<div class="content">

	<!-- Content Header (Page header) -->

	<div class="content-header">

		<div class="d-flex align-items-center">

			<div class="mr-auto">

				<h3 class="page-title">{{ucfirst($expense_type)}} Expense Management</h3>

				<div class="d-inline-block align-items-center">

					<nav>

						<ol class="breadcrumb">

							<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

							<li class="breadcrumb-item" aria-current="page">Home</li>

							<li class="breadcrumb-item active" aria-current="page">{{ucfirst($expense_type)}} Expense Management

							</li>

						</ol>

					</nav>

				</div>

			</div>

			<!-- <div class="right-title">

				<div class="dropdown">

					<button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i

							class="mdi mdi-dots-horizontal"></i></button>

					<div class="dropdown-menu dropdown-menu-right">

						<a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>

						<a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>

						<a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>

						<a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>

						<div class="dropdown-divider"></div>

						<button type="button" class="btn btn-rounded btn-success">Submit</button>

					</div>

				</div>

			</div> -->

		</div>

	</div>





	<div class="row">

	 <div class="col-12">
	@if($rights['add']==1 || $rights['view']==1)	
			<div class="box">





				<div class="box-body">



					<div class="row">



	@if($rights['add']==1)
						<div class="col-sm-6 col-md-3">

							<div class="form-group">
								@if($expense_type=="booking")
								<a href="{{route('add-booking-expenses')}}">
								@else
								<a href="{{route('add-office-expenses')}}">
								@endif
									<button type="button"

										class="btn btn-rounded btn-success">Add {{ucfirst($expense_type)}} Expense</button>
								</a>
								

							</div>

						</div>
						@endif
			@if($rights['view']==1)
						<div class="col-12">
							<div class="box">
								<!-- /.box-header -->
								<div class="box-body" style="padding:0">


									<div class="table-responsive">

                                        <div class="row">

                                        <table id="expense_table" class="table table-bordered table-striped">

											<thead>

												<tr>
													<th>Sr. no</th>
													@if($expense_type=="booking")
													<th>Booking ID</th>
													@endif
													<th>Expense Category</th>
													<th>Occured on</th>
													<th>Amount</th>
													<th>Action</th>
												</tr>

											</thead>

											<tbody>

												@foreach($fetch_expenses as $expenses)
												<tr>
													<td>{{$loop->iteration}}</td>
													@if($expense_type=="booking")
													<td>{{$expenses->expense_booking_id}}</td>
													@endif
													<td>
														{{$expenses->get_expense_category['expense_category_name']}}
													</td>
													<td>
														{{date('d/m/Y h:i a',strtotime($expenses->expense_occured_on))}}
													</td>
													<td>
														GEL {{$expenses->expense_amount}}
													</td>	
														
													<td>
														<a href="{{route('expense-details',['expense_id'=>$expenses->expenses_id])}}"><i class="fa fa-eye"></i></a>
														@if($rights['edit_delete']==1)
														@if($expense_type=="booking")
														<a  href="{{route('edit-booking-expenses',['expense_id'=>$expenses->expenses_id])}}"><i class="fa fa-pencil"></i></a>
														@else
														<a  href="{{route('edit-office-expenses',['expense_id'=>$expenses->expenses_id])}}"><i class="fa fa-pencil"></i></a>

														@endif

														@endif

													</td>

												</tr>
												@endforeach

											</tbody>



										</table>

                                        </div>

									

									</div>

								</div>

								<!-- /.box-body -->

							</div>

						</div>
						@endif





					</div>



				</div>


				<!-- /.row -->

			</div>

			@else
	<h4 class="text-danger">No rights to access this page</h4>
			
			@endif

			<!-- /.box-body -->

		</div>



		<!-- /.box -->



	</div>



</div>

</div>

</div>

@include('mains.includes.footer')

@include('mains.includes.bottom-footer')

<script>

	$(document).ready(function()

	{
    $('#expense_table').DataTable({
		"responsive": true,
		"columnDefs": [
		            { responsivePriority: 1, targets: 0 },
		            { responsivePriority: 2, targets: 4 }
		        ]
    });
	});

</script>





</body>





</html>

