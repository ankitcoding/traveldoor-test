@include('mains.includes.top-header')
<style>
    header.main-header {
        background: url("{{ asset('assets/images/color-plate/theme-purple.jpg') }}");
    }
    img.plus-icon {
        margin: 0 2px;
        display: inline !important;
    }
    .preview_images
    {
        padding: 20px;
    }
    div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
div#loaderModal {
    background: #0000005c;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Service Management</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i
                                                class="mdi mdi-home-outline"></i></a></li>
                                                <li class="breadcrumb-item" aria-current="page">Home</li>
                                                <li class="breadcrumb-item active" aria-current="page">Create New Driver
                                                </li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
            
                        </div>
                    </div>
                    @if($rights['add']==1)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">CREATE DRIVER</h4>
                                </div>
                                <div class="box-body">
                                    <form id="driver_form" enctype="multipart/form-data" method="POST">
                                        {{csrf_field()}}
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label>FIRST NAME <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="FIRST NAME "
                                                    name="driver_first_name" id="driver_first_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label>LAST NAME <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="LAST NAME "
                                                    name="driver_last_name" id="driver_last_name">
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label>CONTACT NUMBER <span class="asterisk">*</span></label>
                                                    
                                                    <input type="text" class="form-control input-lg"
                                                    id="contact_number" name="contact_number" autocomplete="off"
                                                    placeholder="Enter Mobile Number">
                                                    
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label>ADDRESS<span class="asterisk">*</span></label>
                                                    <textarea rows="5" cols="5" class="form-control"
                                                    placeholder="ADDRESS" name="address" id="address"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <label for="driver_supplier_name">SUPPLIER <span class="asterisk">*</span></label>
                                                    <select id="driver_supplier_name" name="driver_supplier_name" class="form-control select2" style="width: 100%;">
                                                        <option value="0" hidden>Select Supplier</option>
                                                        @foreach($suppliers as $supplier)
                                                        <option value="{{$supplier->supplier_id}}">{{$supplier->supplier_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12 col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label>COUNTRY <span class="asterisk">*</span></label>
                                                            <select class="form-control select2" name="driver_country"
                                                            id="driver_country" style="width: 100%;">
                                                            <option selected="selected" hidden value="0">SELECT
                                                            COUNTRY</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                        </div>
                                    </div>
                                    <div class="row mb-10" id="city_div" style="display:none;">
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label for="driver_city">CITY <span class="asterisk">*</span></label>
                                                        <select id="driver_city" name="driver_city" class="form-control select2" style="width: 100%;">
                                                            <option selected="selected">SELECT CITY</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                        </div>
                                    </div>
                                    <div class="row mb-10" id="tour_div" style="display:none;">
                                    </div>
                                    <div class="row mb-10">
                                        <div class="col-sm-12 col-md-12 col-lg-6">
                                            <div class="form-group">
                                                <label>DRIVER LANGUAGE<span class="asterisk">*</span></label>
                                                <select class="form-control driver_language" name="driver_language[]"
                                                id="driver_language" style="width: 100%;" multiple="multiple">
                                                @foreach($languages as $lang)
                                                <option value="{{$lang->language_id}}">{{$lang->language_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>DRIVER PRICE <small>(PER DAY)</small></label>
                                            <input class="form-control" name="driver_price_per_day"
                                            id="driver_price_per_day" placeholder="Driver Price Per Day" onkeypress="javascript:return validateNumber(event)">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>DESCRIPTION<span class="asterisk">*</span></label>
                                            <textarea rows="5" cols="5" class="form-control"
                                            placeholder="DESCRIPTION" name="description" id="description"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="row mb-10">
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>IS ALL DAYS <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="radio_10"
                                                        class="with-gap radio-col-primary week_all_days"  name="is_all_days" value="Yes">
                                                        <label for="radio_10">Yes </label>
                                                        <input type="radio" id="radio_11"
                                                        class="with-gap radio-col-primary week_all_days" name="is_all_days" value="No">
                                                        <label for="radio_11">No</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>MONDAY <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input  type="radio" id="radio_20"
                                                        class="with-gap radio-col-primary weekdays_yes " name="week_monday" value="Yes">
                                                        <label for="radio_20">Yes </label>
                                                        <input type="radio" id="radio_21"
                                                        class="with-gap radio-col-primary weekdays_no " name="week_monday" value="No">
                                                        <label for="radio_21">No</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>TUESDAY <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="radio_30"
                                                        class="with-gap radio-col-primary weekdays_yes" name="week_tuesday" value="Yes">
                                                        <label for="radio_30">Yes </label>
                                                        <input type="radio" id="radio_31"
                                                        class="with-gap radio-col-primary weekdays_no" name="week_tuesday" value="No">
                                                        <label for="radio_31">No</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>WEDNESDAY <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="radio_40"
                                                        class="with-gap radio-col-primary weekdays_yes" name="week_wednesday" value="Yes">
                                                        <label for="radio_40">Yes </label>
                                                        <input type="radio" id="radio_41"
                                                        class="with-gap radio-col-primary weekdays_no" name="week_wednesday" value="No">
                                                        <label for="radio_41">No</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>THURSDAY <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="radio_50"
                                                        class="with-gap radio-col-primary weekdays_yes"  name="week_thursday" value="Yes">
                                                        <label for="radio_50">Yes </label>
                                                        <input type="radio" id="radio_51"
                                                        class="with-gap radio-col-primary weekdays_no"  name="week_thursday" value="No">
                                                        <label for="radio_51">No</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>FRIDAY <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="radio_60"
                                                        class="with-gap radio-col-primary weekdays_yes" name="week_friday" value="Yes">
                                                        <label for="radio_60">Yes </label>
                                                        <input type="radio" id="radio_61"
                                                        class="with-gap radio-col-primary weekdays_no" name="week_friday" value="No">
                                                        <label for="radio_61">No</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>SATURDAY <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="radio_70"
                                                        class="with-gap radio-col-primary weekdays_yes" name="week_saturday" value="Yes">
                                                        <label for="radio_70">Yes </label>
                                                        <input type="radio" id="radio_71"
                                                        class="with-gap radio-col-primary weekdays_no" name="week_saturday" value="No">
                                                        <label for="radio_71">No</label>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <label>SUNDAY <span class="asterisk">*</span></label>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="radio" id="radio_80"
                                                        class="with-gap radio-col-primary weekdays_yes" name="week_sunday" value="Yes">
                                                        <label for="radio_80">Yes </label>
                                                        <input type="radio" id="radio_81"
                                                        class="with-gap radio-col-primary weekdays_no" name="week_sunday" value="No">
                                                        <label for="radio_81">No</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                    </div>
                                </div>
                                <div class="row mb-10" style="display:none;">
                                    <div class="col-sm-12 col-md-12 col-lg-6 markup_div" id="markup_div1">
                                        <label>NATIONALITY & TRANSFER MARKUP <span class="asterisk">*</span></label>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select class="form-control select2" style="width: 100%;" id="driver_nationality1" name="driver_nationality[]">
                                                        <option selected="selected" value="0" hidden>SELECT NATIONALITY</option>
                                                        @foreach($countries as $country)
                                                        <option value="{{$country->country_id}}">{{$country->country_name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <select class="form-control" id="driver_markup1" name="driver_markup[]">
                                                        <option value="0" selected="selected">SELECT MARKUP TYPE</option>
                                                        <option>Markup Percentage</option>
                                                        <option>Markup Amount</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="Markup Amount" id="driver_amount1" name="driver_amount[]" onkeypress="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-12 add_more_markup_div">
                                            <img id="add_more_markup1" class="plus-icon add_more_markup" style="display: block !important;margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                    </div>
                                </div>
                                <div class="row mb-10">
                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                        <div class="form-group">
                                            <label>BLACKOUT DAYS</label>
                                            <div class="col-sm-12 col-md-12" style="padding:0">
                                                <button type="button" class="btn btn-rounded btn-primary mr-10"
                                                data-toggle="collapse" data-target="#demo2">Add
                                            Blackout Days</button>
                                            <div id="demo2" class="collapse">
                                                <div class="row mt-15 mb-10">
                                                    <div class="col-sm-12 col-md-12">
                                                        <div class="form-group">
                                                            <div class="input-group date">
                                                                <input type="text" placeholder="BLACKOUT DATES" class="form-control pull-right datepicker" id="blackout_days" name="blackout_days">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-calendar"></i>
                                                                </div>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-12 col-lg-6">
                                </div>
                            </div>
                            <div class="row mb-10" style="display:none;">
                                <div class="col-sm-12 col-md-12 col-lg-6 transport_div" id="transport_div1">
                                    <label for="driver_transport_currency1">
                                        DRIVER TARIFF <span class="asterisk">*</span></label>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-12"  >
                                                <div class="form-group">
                                                    <label for="validity_from">VALIDITY<span class="asterisk">*</span></label>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <div class="input-group date">
                                                                    <input type="text" placeholder="FROM"
                                                                    class="form-control pull-right datepicker driver_validity_from" id="driver_validity_from1" name="driver_validity_from[]">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <div class="input-group date">
                                                                    <input type="text" placeholder="TO"
                                                                    class="form-control pull-right datepicker driver_validity_to" id="driver_validity_to1" name="driver_validity_to[]">
                                                                    <div class="input-group-addon">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </div>
                                                                </div>
                                                                <!-- /.input group -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="TOUR NAME"  id="driver_tourname1" name="driver_tourname[]">
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="PRICE UPTO 4" id="driver_cost_four1" name="driver_cost_four[]" onkeypress="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="PRICE UPTO 7" id="driver_cost_seven1" name="driver_cost_seven[]" onkeypress="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="PRICE UPTO 20" id="driver_cost_twenty1" name="driver_cost_twenty[]" onkeypress="javascript:return validateNumber(event)">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" placeholder="DURATION"  id="driver_duration1" name="driver_duration[]">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-12 add_more_transport_div">
                                            <img id="add_more_transport1" class="plus-icon add_more_transport" style="display: block !important;margin-left: auto;" src="{{ asset('assets/images/add_icon.png') }}">
                                        </div>
                                    </div>
                                </div>



                                 <div class="row mb-10">

                                        <div class="col-sm-12 col-md-12 col-lg-6">

                                            <div class="form-group">

                                                <label>DRIVER FOOD COST<span class="asterisk">*</span></label>

                                                <input class="form-control" name="driver_food_cost"

                                                    id="driver_food_cost" placeholder="Driver Food Cost" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">

                                            </div>

                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-6">

                                        </div>

                                    </div>



                                 <div class="row mb-10">

                                        <div class="col-sm-12 col-md-12 col-lg-6">

                                            <div class="form-group">

                                                <label>DRIVER HOTEL COST<span class="asterisk">*</span></label>

                                                <input class="form-control" name="driver_hotel_cost"

                                                    id="driver_hotel_cost" placeholder="Driver Hotel Cost" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)">

                                            </div>

                                        </div>

                                        <div class="col-sm-12 col-md-12 col-lg-6">

                                        </div>

                                    </div>

                                <div class="row" style="display:none;">
                                    <div class="col-md-12">
                                        <div class="row mb-10">
                                            <div class="col-sm-12">
                                                <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                </div>
                                                <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                    <i class="fa fa-plus-circle"></i> INCLUSIONS <span class="asterisk">*</span></h4>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="box">
                                                        <!-- /.box-header -->
                                                        <div class="box-body">
                                                            <textarea class="form-control" id="driver_inclusions" name="driver_inclusions"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="row mb-10">
                                                <div class="col-sm-12">
                                                    <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                    </div>
                                                    <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                        <i class="fa fa-plus-circle"></i> EXCLUSIONS <span class="asterisk">*</span></h4>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        <div class="box">
                                                            <!-- /.box-header -->
                                                            <div class="box-body">
                                                                <textarea class="form-control" id="driver_exclusions" name="driver_exclusions"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row mb-10">
                                                    <div class="col-sm-12">
                                                        <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                        </div>
                                                        <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                            <i class="fa fa-plus-circle"></i> CANCELLATION POLICY <span class="asterisk">*</span></h4>
                                                        </div>
                                                        <div class="col-sm-12">
                                                            <div class="box">
                                                                <!-- /.box-header -->
                                                                <div class="box-body">
                                                                    <textarea class="form-control" id="driver_cancellation" name="driver_cancellation"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row mb-10">
                                                        <div class="col-sm-12">
                                                            <div class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;">
                                                            </div>
                                                            <h4 class="box-title" style="border-color: #c1c1c1;margin-top: 25px;">
                                                                <i class="fa fa-plus-circle"></i> TERMS AND CONDITIONS <span class="asterisk">*</span> </h4>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <div class="box">
                                                                    <!-- /.box-header -->
                                                                    <div class="box-body">
                                                                        <textarea class="form-control" id="driver_terms_conditions" name="driver_terms_conditions"></textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                 <div class="row mb-10">
                                            <div class="col-md-12">
                                             <div class="transfer_div" id="transfer_div__1">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <label>VEHICLE TYPE<span class="asterisk">*</span></label>
                                                     <select  class="form-control vehicle_type select2" style="width: 100%;" name="vehicle_type[]" id="vehicle_type__1">
                                                        <option value="0" hidden>SELECT VEHICLE TYPE</option>
                                                       @foreach($fetch_vehicle_type as $vehicle_type)
                                                        <option value="{{$vehicle_type->vehicle_type_id}}">{{$vehicle_type->vehicle_type_name}}</option>
                                                       @endforeach
                                                    </select>
                                                </div>
                                                 <div class="col-md-3">
                                                    <label>SELECT VEHICLE<span class="asterisk">*</span></label>
                                                     <select class="form-control vehicle select2" name="vehicle[]" style="width: 100%;" id="vehicle__1">
                                                        <option value="0" hidden>LIST OF VEHICLES</option>
                                                    </select>

                                                </div>
                                                 <div class="col-md-2">
                                                      <label>VEHICLE INFO <span class="asterisk">*</span></label>
                                                        <input type="text" class="form-control vehicle_info" placeholder="VEHICLE INFO "
                                                        name="vehicle_info[]" id="vehicle_info__1">
                                                </div>
                                                <div class="col-md-2">
                                                      <label>VEHICLE IMAGES <span class="asterisk">*</span></label>
                                                        <input type="file" class="form-control  vehicle_images" name="vehicle_images[0][]" id="vehicle_images__1" multiple="multiple" accept="image/jpeg,image/jpg,image/png">
                                                        <small>Max Limit : 4</small>
                                                </div>
                                                <div class="col-md-12 preview_images">
                                                </div>
                                                   <div class='col-sm-12 col-md-12 add_more_transfer_div'>
                                                  <img id='add_more_transfer1' class='add_more_transfer plus-icon'   style='margin-left: auto;' src='{{ asset('assets/images/add_icon.png') }}'>
                                              </div>
                                        
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                                <div class="row mb-10">
                                                    <div class="col-sm-12 col-md-12 col-lg-6">
                                                        <div class="img_group">
                                                            <label>DRIVER PICTURE</label>
                                                            <div class="box1">
                                                                <input class="hide" type="file" id="upload_logo"
                                                                accept="image/png,image/jpg,image/jpeg"
                                                                name="driver_logo_file" onchange="previewFile('logo')">
                                                                <button type="button"
                                                                onclick="document.getElementById('upload_logo').click()"
                                                                id="upload_0" class="btn red btn-outline btn-circle">+
                                                            </button>
                                                        </div>
                                                        <br>
                                                        <!-- ngRepeat: (itemindex,item) in temp_loop.enquiry_comment_attachment track by $index -->
                                                        <img id="logo_preview" src="" height="200" alt="LOGO Preview..."
                                                        style="display:none">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row mb-10">
                                                <div class="col-md-12">
                                                    <div class="box-header with-border"
                                                    style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
                                                    <button type="button" id="create_driver"
                                                    class="btn btn-rounded btn-primary mr-10">Save</button>
                                                    <button type="button" id="discard_driver"
                                                    class="btn btn-rounded btn-primary">Discard</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <h4 class="text-danger">No rights to access this page</h4>

                    @endif
                </div>
            </div>
        </div>
        @include('mains.includes.footer')
        @include('mains.includes.bottom-footer')
        <div class="modal" id="loaderModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
        <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
        <script>
            function previewFile(data) {
                if (data == "logo") {
                    var preview = document.getElementById('logo_preview');
                    var file = document.querySelector('input[name="driver_logo_file"]').files[0];
                } else {
                    var preview = document.getElementById('certificate_preview');
                    var file = document.querySelector('input[name="driver_certificate_file"]').files[0];
                }
                var reader = new FileReader();
                reader.onloadend = function () {
                    preview.src = reader.result;
                    preview.style.display = "block";
                }
                if (file) {
                    reader.readAsDataURL(file);
                } else {
                    preview.src = "";
                }
            }
        </script>
        <script>
            function dateshow()
            {
                var date = new Date();
                date.setDate(date.getDate());
                $('.driver_validity_from').datepicker({
                    autoclose:true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd',
                    startDate:date
                });
                $('.driver_validity_from').on('change', function () {
                    var date_from_id=this.id;
                    var date_id=date_from_id.split("driver_validity_from");
                    var date_from = $("#driver_validity_from"+date_id[1]).datepicker("getDate");
                    var date_to = $("#driver_validity_to"+date_id[1]).datepicker("getDate");
                    if(!date_to)
                    {
                        $("#driver_validity_to"+date_id[1]).datepicker("setDate",date_from);
                    }
                    else if(date_to.getTime()<date_from.getTime())
                    {
                        $("#driver_validity_to"+date_id[1]).datepicker("setDate",date_from);
                    }
                });
                $('.driver_validity_to').datepicker({
                    autoclose:true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd',
                    startDate:date
                });
                $('.driver_validity_to').on('change', function () {
                    var date_to_id=this.id;
                    var date_id=date_to_id.split("driver_validity_to");
                    var date_from = $("#driver_validity_from"+date_id[1]).datepicker("getDate");
                    var date_to = $("#driver_validity_to"+date_id[1]).datepicker("getDate");
                    if(!date_from)
                    {
                        $("#driver_validity_from"+date_id[1]).datepicker("setDate",date_to);
                    }
                    else if(date_to.getTime()<date_from.getTime())
                    {
                        $("#driver_validity_from"+date_id[1]).datepicker("setDate",date_to);
                    }
                });
            }
            $(document).ready(function()
            {
                CKEDITOR.replace('driver_exclusions');
                CKEDITOR.replace('driver_inclusions');
                CKEDITOR.replace('driver_cancellation');
                CKEDITOR.replace('driver_terms_conditions');
                $('.select2').select2();

                var date = new Date();
                date.setDate(date.getDate());
                $('#blackout_days').datepicker({
                    multidate: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd',
                    startDate:date
                });
                $(".driver_language").select2({placeholder:"SELECT LANGUAGE"});
                dateshow();
            });
            
        </script>
        <script>
            $(document).on("change","#driver_supplier_name",function()
            {
                if($("#driver_supplier_name").val()!="0")
                {
                    var supplier_id=$(this).val();
                    $.ajax({
                        url:"{{route('search-supplier-country')}}",
                        type:"GET",
                        data:{"supplier_id":supplier_id},
                        success:function(response)
                        {
                            $("#driver_country").html(response);
                            $('#driver_country').select2();
                            $("#driver_country").prop("disabled",false);
                            $("#driver_city").html("");
                        }
                    });
                }
            });
            $(document).on("change","#driver_country",function()
            {
                if($("#driver_country").val()!="0")
                {
                     $("#loaderModal").modal("show");
                    var country_id=$(this).val();
                    $.ajax({
                        url:"{{route('search-country-cities')}}",
                        type:"GET",
                        data:{"country_id":country_id},
                        success:function(response)
                        {
                            
                            $("#driver_city").html(response);
                            $('#driver_city').select2();
                            $("#city_div").show();
                             $("#loaderModal").modal("hide");
                        }
                    });

                    // $.ajax({
                    //     url:"{{route('searchSightseeingTour')}}",
                    //     type:"GET",
                    //     data:{"country_id":country_id,
                    //             "src":"driver"},
                    //     success:function(response)
                    //     {
                    //         $("#tour_div").html(response);
                    //         $("#tour_div").show();
                    //          $("#loaderModal").modal("hide");
                    //     }
                    // });
                }
            });
            $(document).on("change","#driver_city",function()
            {
                if($("#driver_city").val()!="0")
                {
                     $("#loaderModal").modal("show");
                    var city_id=$(this).val();
                    var country_id=$("#driver_country").val();
                    $.ajax({
                        url:"{{route('searchSightseeingTour')}}",
                        type:"GET",
                        data:{"city_id":city_id,"country_id":country_id,
                                "src":"driver"},
                        success:function(response)
                        {
                            $("#tour_div").html(response);
                            $("#tour_div").show();
                             $("#loaderModal").modal("hide");
                        }
                    });
                }
            });
        </script>
        <script>
            $("#driver_country").on("change", function () {
                if ($(this).val() != "0") {
                    $("#city_div").show();
                }
            });
            $("#discard_driver").on("click", function ()
            {
                window.history.back();
            });
            $(document).on("click", "#create_driver", function ()
            {
            // swal("Success","Dummy driver insertion process");
            var driver_first_name = $("#driver_first_name").val();
            var driver_last_name = $("#driver_last_name").val();
            var contact_number = $("#contact_number").val();
            var address = $("#address").val();
            var driver_supplier_name = $("#driver_supplier_name").val();
            var driver_country = $("#driver_country").val();
            var driver_city = $("#driver_city").val();
            var driver_description = $("#description").val();
            var driver_language = $("#driver_language").val();
            var driver_price_per_day = $("#driver_price_per_day").val();
             var driver_food_cost = $("#driver_food_cost").val();
          var driver_hotel_cost = $("#driver_hotel_cost").val();
            var driver_logo_file = $("#driver_logo_file").val();
            var is_all_days = $("input[name='is_all_days']:checked").val();
            var week_monday = $("input[name='week_monday']:checked").val();
            var week_tuesday = $("input[name='week_tuesday']:checked").val();
            var week_wednesday = $("input[name='week_wednesday']:checked").val();
            var week_thursday = $("input[name='week_thursday']:checked").val();
            var week_friday = $("input[name='week_friday']:checked").val();
            var week_saturday = $("input[name='week_saturday']:checked").val();
            var week_sunday = $("input[name='week_sunday']:checked").val();
            var driver_inclusions= CKEDITOR.instances.driver_inclusions.getData();
            var driver_exclusions=CKEDITOR.instances.driver_exclusions.getData();
            var driver_cancellation=CKEDITOR.instances.driver_cancellation.getData();
            var driver_terms_conditions=CKEDITOR.instances.driver_terms_conditions.getData();
            if (driver_first_name.trim() == "")
            {
                $("#driver_first_name").css("border", "1px solid #cf3c63");
            } else
            {
                $("#driver_first_name").css("border", "1px solid #9e9e9e");
            }
            if (driver_last_name.trim() == "")
            {
                $("#driver_last_name").css("border", "1px solid #cf3c63");
            } else
            {
                $("#driver_last_name").css("border", "1px solid #9e9e9e");
            }
            if (contact_number.trim() == "")
            {
                $("#contact_number").css("border", "1px solid #cf3c63");
            } else
            {
                $("#contact_number").css("border", "1px solid #9e9e9e");
            }
            if (address.trim() == "")
            {
                $("#address").css("border", "1px solid #cf3c63");
            } else
            {
                $("#address").css("border", "1px solid #9e9e9e");
            }
            if (driver_supplier_name.trim() == "0")
            {
                $("#driver_supplier_name").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#driver_supplier_name").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }
            if (driver_country.trim() == "0")
            {
                $("#driver_country").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#driver_country").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }
            if (driver_city.trim() == "0")
            {
                $("#driver_city").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#driver_city").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }
            if (driver_language == "")
            {
                $("#driver_language").parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            } else
            {
                $("#driver_language").parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            }

            // if (driver_price_per_day == "")
            // {
            //     $("#driver_price_per_day").css("border", "1px solid #cf3c63");
            // } else
            // {
            //     $("#driver_price_per_day").css("border", "1px solid #9e9e9e");
            // }

            if (driver_description.trim() == "")
            {
                $("#description").css("border", "1px solid #cf3c63");
            } else
            {
                $("#description").css("border", "1px solid #9e9e9e");
            }
            if (!is_all_days) {
                $("input[name='is_all_days']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='is_all_days']").parent().css("border", "1px solid white");
            }
            if (!week_monday) {
                $("input[name='week_monday']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='week_monday']").parent().css("border", "1px solid white");
            }
            if (!week_tuesday) {
                $("input[name='week_tuesday']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='week_tuesday']").parent().css("border", "1px solid white");
            }
            if (!week_wednesday) {
                $("input[name='week_wednesday']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='week_wednesday']").parent().css("border", "1px solid white");
            }
            if (!week_thursday) {
                $("input[name='week_thursday']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='week_thursday']").parent().css("border", "1px solid white");
            }
            if (!week_friday) {
                $("input[name='week_friday']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='week_friday']").parent().css("border", "1px solid white");
            }
            if (!week_saturday) {
                $("input[name='week_saturday']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='week_saturday']").parent().css("border", "1px solid white");
            }
            if (!week_sunday) {
                $("input[name='week_sunday']").parent().css("border", "1px solid #cf3c63");
            } else {
                $("input[name='week_sunday']").parent().css("border", "1px solid white");
            }
            // var driver_nationality = 1;
            // var driver_nationality_error = 0;
            // $("select[name='driver_nationality[]']").each(function() {
            //     if ($(this).val() == "0") {
            //         $("#driver_nationality" + driver_nationality).parent().find(".select2-selection").css("border", "1px solid #cf3c63");
            //         $("#driver_nationality" + driver_nationality).parent().find(".select2-selection").focus();
            //         driver_nationality_error++;
            //     } else {
            //         $("#driver_nationality" + driver_nationality).parent().find(".select2-selection").css("border", "1px solid #9e9e9e");
            //     }
            //     driver_nationality++;
            // });
            // var driver_markup = 1;
            // var driver_markup_error = 0;
            // $("select[name='driver_markup[]']").each(function() {
            //     if ($(this).val() == "0") {
            //         $("#driver_markup" + driver_markup).css("border", "1px solid #cf3c63");
            //         $("#driver_markup" + driver_markup).focus();
            //         driver_markup_error++;
            //     } else {
            //         $("#driver_markup" + driver_markup).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_markup++;
            // });
            // var driver_markup_amt = 1;
            // var driver_markup_amt_error = 0;
            // $("input[name='driver_amount[]']").each(function() {
            //     if ($(this).val().trim() == "") {
            //         $("#driver_amount" + driver_markup_amt).css("border", "1px solid #cf3c63");
            //         $("#driver_amount" + driver_markup_amt).focus();
            //         driver_markup_amt_error++;
            //     } else {
            //         $("#driver_amount" + driver_markup_amt).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_markup_amt++;
            // });
            //  var driver_validity_from = 1;
            // var driver_validity_from_error = 0;
            // $("input[name='driver_validity_from[]']").each(function() {
            //     if ($(this).val() == "") {
            //         $("#driver_validity_from" + driver_validity_from).css("border", "1px solid #cf3c63");
            //         $("#driver_validity_from" + driver_validity_from).focus();
            //         driver_validity_from_error++;
            //     } else {
            //         $("#driver_validity_from" + driver_validity_from).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_validity_from++;
            // });
            //   var driver_validity_to = 1;
            // var driver_validity_to_error = 0;
            // $("input[name='driver_validity_to[]']").each(function() {
            //     if ($(this).val() == "") {
            //         $("#driver_validity_to" + driver_validity_to).css("border", "1px solid #cf3c63");
            //         $("#driver_validity_to" + driver_validity_to).focus();
            //         driver_validity_to_error++;
            //     } else {
            //         $("#driver_validity_to" + driver_validity_to).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_validity_to++;
            // });
            //   var driver_tourname = 1;
            // var driver_tourname_error = 0;
            // $("input[name='driver_tourname[]']").each(function() {
            //     if ($(this).val() == "") {
            //         $("#driver_tourname" + driver_tourname).css("border", "1px solid #cf3c63");
            //         $("#driver_tourname" + driver_tourname).focus();
            //         driver_tourname_error++;
            //     } else {
            //         $("#driver_tourname" + driver_tourname).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_tourname++;
            // });
            //     var driver_cost_four = 1;
            // var driver_cost_four_error = 0;
            // $("input[name='driver_cost_four[]']").each(function() {
            //     if ($(this).val() == "") {
            //         $("#driver_cost_four" + driver_cost_four).css("border", "1px solid #cf3c63");
            //         $("#driver_cost_four" + driver_cost_four).focus();
            //         driver_cost_four_error++;
            //     } else {
            //         $("#driver_cost_four" + driver_cost_four).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_cost_four++;
            // });
            //     var driver_cost_seven = 1;
            // var driver_cost_seven_error = 0;
            // $("input[name='driver_cost_seven[]']").each(function() {
            //     if ($(this).val() == "") {
            //         $("#driver_cost_seven" + driver_cost_seven).css("border", "1px solid #cf3c63");
            //         $("#driver_cost_seven" + driver_cost_seven).focus();
            //         driver_cost_seven_error++;
            //     } else {
            //         $("#driver_cost_seven" + driver_cost_seven).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_cost_seven++;
            // });
            //     var driver_cost_twenty = 1;
            // var driver_cost_twenty_error = 0;
            // $("input[name='driver_cost_twenty[]']").each(function() {
            //     if ($(this).val() == "") {
            //         $("#driver_cost_twenty" + driver_cost_twenty).css("border", "1px solid #cf3c63");
            //         $("#driver_cost_twenty" + driver_cost_twenty).focus();
            //         driver_cost_twenty_error++;
            //     } else {
            //         $("#driver_cost_twenty" + driver_cost_twenty).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_cost_twenty++;
            // });
            //   var driver_duration = 1;
            // var driver_duration_error = 0;
            // $("input[name='driver_duration[]']").each(function() {
            //     if ($(this).val() == "") {
            //         $("#driver_duration" + driver_duration).css("border", "1px solid #cf3c63");
            //         $("#driver_duration" + driver_duration).focus();
            //         driver_duration_error++;
            //     } else {
            //         $("#driver_duration" + driver_duration).css("border", "1px solid #9e9e9e");
            //     }
            //     driver_duration++;
            // });

      if (driver_food_cost == "")
      {
        $("#driver_food_cost").css("border", "1px solid #cf3c63");
    } else
    {
        $("#driver_food_cost").css("border", "1px solid #9e9e9e");
    }

     if (driver_hotel_cost == "")
      {
        $("#driver_hotel_cost").css("border", "1px solid #cf3c63");
    } else
    {
        $("#driver_hotel_cost").css("border", "1px solid #9e9e9e");
    }
            if (driver_cancellation.trim() == "")
            {
                $("#cke_driver_cancellation").css("border", "1px solid #cf3c63");
            } else
            {
                $("#cke_driver_cancellation").css("border", "1px solid #9e9e9e");
            }
            if (driver_terms_conditions.trim() == "")
            {
                $("#cke_driver_terms_conditions").css("border", "1px solid #cf3c63");
            } else
            {
                $("#cke_driver_terms_conditions").css("border", "1px solid #9e9e9e");
            }
            
            if (driver_first_name.trim() == "") {
                $("#driver_first_name").focus();
            } else if (driver_last_name.trim() == "") {
                $("#driver_last_name").focus();
            } else if (contact_number.trim() == "") {
                $("#contact_number").focus();
            } else if (address.trim() == "") {
                $("#address").focus();
            } else if (driver_supplier_name.trim() == "0") {
                $("#driver_supplier_name").parent().find(".select2-selection").focus();
            } else if (driver_country.trim() == "0") {
                $("#driver_country").parent().find(".select2-selection").focus();
            } else if (driver_city.trim() == "0") {
                $("#driver_city").parent().find(".select2-selection").focus();
            } else if (driver_language == "") {
                $("#driver_language").parent().find(".select2-selection").focus();
            }
            // else if (driver_price_per_day == "") {
            //     $("#driver_price_per_day").focus();
            // }
            else if (driver_description.trim() == "") {
                $("#description").focus();
            } else if (!is_all_days) {
                $("input[name='is_all_days']").focus();
            } else if (!week_monday) {
                $("input[name='week_monday']").focus();
            } else if (!week_tuesday) {
                $("input[name='week_tuesday']").focus();
            } else if (!week_wednesday) {
                $("input[name='week_wednesday']").focus();
            } else if (!week_thursday) {
                $("input[name='week_thursday']").focus();
            } else if (!week_friday) {
                $("input[name='week_friday']").focus();
            } else if (!week_saturday) {
                $("input[name='week_saturday']").focus();
            } else if (!week_sunday) {
                $("input[name='week_sunday']").focus();
            }
            //  else if (driver_nationality_error > 0) {
            // } else if (driver_markup_error > 0) {
            // } else if (driver_markup_amt_error > 0) {
            // }
            // else if (driver_validity_from_error > 0) {
            // } else if (driver_validity_to_error > 0) {
            // }
            // else if (driver_tourname_error > 0) {
            // }
            // else if (driver_cost_four_error > 0) {
            // }
            // else if (driver_cost_seven_error > 0) {
            // }
            // else if (driver_cost_twenty_error > 0) {
            // }
            // else if (driver_duration_error > 0) {
            // }
            else if (driver_food_cost == "") {

                        $("#driver_food_cost").focus();

                    }
                     else if (driver_hotel_cost == "") {

                        $("#driver_hotel_cost").focus();

                    }
            else if(driver_cancellation.trim()=="")
            {
                $("#cke_driver_cancellation").attr("tabindex","100").focus();
            }
            else if(driver_terms_conditions.trim()=="")
            {
                $("#cke_driver_terms_conditions").attr("tabindex","100").focus();
            }
            else
            {
                $("#create_driver").prop("disabled",true);
                var formdata = new FormData($("#driver_form")[0]);
                formdata.append("driver_inclusions",driver_inclusions);
                formdata.append("driver_exclusions",driver_exclusions);
                formdata.append("driver_cancellation",driver_cancellation);
                formdata.append("driver_terms_conditions",driver_terms_conditions);
                $.ajax({
                    url: "{{route('insert-driver')}}",
                    enctype: 'multipart/form-data',
                    data: formdata,
                    type: "POST",
                    processData: false,
                    contentType: false,
                    success: function (response)
                    {
                        if (response.indexOf("exist") != -1)
                        {
                            swal("Already Exist!",
                                "Driver with this contact number already exists");
                        } else if (response.indexOf("success") != -1)
                        {
                            swal({
                                title: "Success",
                                text: "Driver Created Successfully !",
                                type: "success"
                            },
                            function () {
                                location.reload();
                            });
                        } else if (response.indexOf("fail") != -1)
                        {
                            swal("ERROR", "Driver cannot be inserted right now! ");
                        }
                        $("#create_driver").prop("disabled", false);
                    }
                });
            }
        });
    </script>
    <script>
        $(document).on("change","input[name='is_all_days']",function()
        {
            if($(this).is(":checked"))
            {
                if($("input[name='is_all_days']:checked").val()=="Yes")
                {
                    $(".weekdays_yes").prop("checked",true);
                }
                else
                {
                    $(".weekdays_no").prop("checked",true);
                }
            }
        });
        $(document).on("change",".weekdays_yes,.weekdays_no",function()
        {
            var count=0;
            $(".weekdays_yes").each(function()
            {
                if($(this).is(":checked"))
                {
                    count++;
                }
            });
            if(count==7)
            {
                $("input[name=is_all_days][value='Yes']").prop("checked",true);
            }
            else
            {
                $("input[name=is_all_days][value='No']").prop("checked",true);
            }
        });
        $(document).on("click",".add_more_transport",function()
        {
            var clone_transport = $(".transport_div:last").clone();
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            var newer_id = $(".transport_div:last").attr("id");
            new_id = newer_id.split('transport_div');
            old_id = parseInt(new_id[1]);
            new_id = parseInt(new_id[1]) + 1;
            clone_transport.find("input[name='driver_tourname[]']").attr("id", "driver_tourname" + new_id)
            .val("");
            clone_transport.find("input[name='driver_tourname[]']").parent().parent().parent().parent().attr("id",
                "transport_div" + new_id);
            clone_transport.find("input[name='driver_validity_from[]']").attr("id", "driver_validity_from" + new_id).val("");
            clone_transport.find("input[name='driver_validity_to[]']").attr("id", "driver_validity_to" + new_id).val("");
            clone_transport.find("input[name='driver_cost_four[]']").attr("id", "driver_cost_four" + new_id).val("");
            clone_transport.find("input[name='driver_cost_seven[]']").attr("id", "driver_cost_seven" + new_id).val("");
            clone_transport.find("input[name='driver_cost_twenty[]']").attr("id", "driver_cost_twenty" + new_id).val("");
            clone_transport.find("input[name='driver_vehicle[]']").attr("id", "driver_vehicle" + new_id).val("");
            clone_transport.find("input[name='driver_duration[]']").attr("id", "driver_duration" + new_id).val("");
            // clone_transport.find(".add_more_transport").attr("src", minus_url);
            // clone_transport.find(".add_more_transport").attr("id", "remove_more_transport" + new_id);
            // clone_transport.find(".add_more_transport").removeClass('plus-icon add_more_transport').addClass(
            //     'minus-icon remove_more_transport');
            $("#transport_div"+old_id).find(".add_more_transport_div").html("");
            if(old_id>1)
            {
                $("#transport_div"+old_id).find(".add_more_transport_div").append('<img id="remove_more_transport'+old_id+'" class="remove_more_transport minus-icon" src="'+minus_url+'">');
            }
            clone_transport.find(".add_more_transport_div").html('');
            clone_transport.find(".add_more_transport_div").append(' <img id="remove_more_transport'+new_id+'" class="remove_more_transport minus-icon" src="'+minus_url+'"> <img id="add_more_transport'+new_id+'" class="add_more_transport plus-icon" src="'+add_url+'"> ');
            $(".transport_div:last").after(clone_transport);
            dateshow();
        });
        $(document).on("click", ".remove_more_transport", function () {
            var id = this.id;
            var split_id = id.split('remove_more_transport');
            $("#transport_div" + split_id[1]).remove();
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            $("#transport_div" + split_id[1]).remove();
            var last_id = $(".transport_div:last").attr("id");
            old_id = last_id.split('transport_div');
            old_id=parseInt(old_id[1]);
            if(old_id>1)
            {
                $("#transport_div"+old_id).find(".add_more_transport_div").html("");
                $("#transport_div"+old_id).find(".add_more_transport_div").append('<img id="remove_more_transport'+old_id+'" class="remove_more_transport minus-icon" src="'+minus_url+'"> <img id="add_more_transport'+old_id+'" class="add_more_transport plus-icon" src="'+add_url+'">');
            }
            else
            {
                $("#transport_div"+old_id).find(".add_more_transport_div").html("");
                $("#transport_div"+old_id).find(".add_more_transport_div").append('<img id="add_more_transport'+old_id+'" class="add_more_transport minus-icon" src="'+add_url+'"> ');
            }
        });
        $(document).on("click",".add_more_markup",function()
        {
            var clone_markup = $(".markup_div:last").clone();
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            var newer_id = $(".markup_div:last").attr("id");
            new_id = newer_id.split('markup_div');
            old_id = parseInt(new_id[1]);
            new_id = parseInt(new_id[1]) + 1;
            clone_markup.find("select[name='driver_nationality[]']").attr("id", "driver_nationality" + new_id)
            .val(0);
            clone_markup.find("select[name='driver_nationality[]']").parent().parent().parent().parent().attr("id",
                "markup_div" + new_id);
            clone_markup.find("select[name='driver_nationality[]']").select2();
            clone_markup.find(".select2-container").slice(1).remove();
            clone_markup.find("select[name='driver_markup[]']").attr("id", "driver_markup" + new_id).val("0");
            clone_markup.find("input[name='driver_amount[]']").attr("id", "driver_amount" + new_id).val("");
            // clone_markup.find(".add_more_markup").attr("src", minus_url);
            // clone_markup.find(".add_more_markup").attr("id", "remove_more_markup" + new_id);
            // clone_markup.find(".add_more_markup").removeClass('plus-icon add_more_markup').addClass(
            //     'minus-icon remove_more_markup');
            $("#markup_div"+old_id).find(".add_more_markup_div").html("");
            if(old_id>1)
            {
                $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="remove_more_markup'+old_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'">');
            }
            clone_markup.find(".add_more_markup_div").html('');
            clone_markup.find(".add_more_markup_div").append(' <img id="remove_more_markup'+new_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'"> <img id="add_more_markup'+new_id+'" class="add_more_markup plus-icon" src="'+add_url+'"> ');
            $(".markup_div:last").after(clone_markup);
            
        });
        $(document).on("click", ".remove_more_markup", function () {
            var id = this.id;
            var split_id = id.split('remove_more_markup');
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            $("#markup_div" + split_id[1]).remove();
            var last_id = $(".markup_div:last").attr("id");
            old_id = last_id.split('markup_div');
            old_id=parseInt(old_id[1]);
            if(old_id>1)
            {
                $("#markup_div"+old_id).find(".add_more_markup_div").html("");
                $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="remove_more_markup'+old_id+'" class="remove_more_markup minus-icon" src="'+minus_url+'"> <img id="add_more_markup'+old_id+'" class="add_more_markup plus-icon" src="'+add_url+'">');
            }
            else
            {
                $("#markup_div"+old_id).find(".add_more_markup_div").html("");
                $("#markup_div"+old_id).find(".add_more_markup_div").append('<img id="add_more_markup'+old_id+'" class="add_more_markup minus-icon" src="'+add_url+'"> ');
            }
        });

        $(document).on("change",".vehicle_type",function()
{

    $("#loaderModal").modal("show");
    var vehicle_type_id=$(this).val();
    var id=$(this).attr("id").split("__")[1];

    $.ajax({
        url:"{{route('fetchVehicle')}}",
        type:"GET",
        data:{"vehicle_type_id":vehicle_type_id,},
        success:function(response)
        {
         $("#vehicle__"+id).html(response);   
          $("#loaderModal").modal("hide"); 
     }
 });
});

   $(document).on("click",".add_more_transfer",function()
        {
            var clone_policies = $(".transfer_div:last").clone();
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var newer_id = $(".transfer_div:last").attr("id");
            new_id = newer_id.split('__');
            old_id=parseInt(new_id[1]);
            new_id = parseInt(new_id[1]) + 1;
            clone_policies.find("select[name='vehicle_type[]']").attr("id", "vehicle_type__" + new_id).val("0").select2();
            clone_policies.find(".select2-container").slice(1).remove();
            clone_policies.find("select[name='vehicle_type[]']").parent().parent().parent().attr("id","transfer_div__" + new_id);
            clone_policies.find("select[name='vehicle[]']").attr("id", "vehicle__" + new_id).val("0").select2();
            clone_policies.find("input[name='vehicle_info[]']").attr("id","vehicle_info__" + new_id).val("");
            clone_policies.find(".vehicle_images").attr({"id":"vehicle_images__" + new_id,"name":"vehicle_images["+old_id+"][]"}).val("");
            $("#transfer_div__"+old_id).find(".add_more_transfer_div").html("");

                     clone_policies.find(".preview_images").html('');
            if(old_id>1)
            {
                // $("#transfer_div__"+old_id).find(".add_more_transfer_div").append('<img id="remove_transfer'+old_id+'" class="remove_transfer minus-icon" src="'+minus_url+'" style="margin-left: auto;">');
            }
            clone_policies.find(".add_more_transfer_div").html('');
    
            clone_policies.find(".add_more_transfer_div").append(' <img id="remove_transfer'+new_id+'" class="remove_transfer minus-icon"  src="'+minus_url+'"   style="margin-left: auto;"> <img id="add_more_transfer'+new_id+'" class="add_more_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;"> ');
            $(".transfer_div:last").after(clone_policies);
        });

        $(document).on("click", ".remove_transfer", function () {
            var id = this.id;
            var split_id = id.split('remove_transfer');
            $("#transfer_div__" + split_id[1]).remove();
            var minus_url = "{!! asset('assets/images/minus_icon.png') !!}";
            var add_url= "{!! asset('assets/images/add_icon.png') !!}";
            var last_id = $(".transfer_div:last").attr("id");
            old_id = last_id.split('__');
            old_id=parseInt(old_id[1]);
            if(old_id>1)
            {
                $("#transfer_div__"+old_id).find(".add_more_transfer_div").html("");
                $("#transfer_div__"+old_id).find(".add_more_transfer_div").append('<img id="remove_transfer'+old_id+'" class="remove_transfer minus-icon"  src="'+minus_url+'"   style="margin-left: auto;"> <img id="add_more_transfer'+old_id+'" class="add_more_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;">');
            }
            else
            {
                $("#transfer_div__"+old_id).find(".add_more_transfer_div").html("");
                $("#transfer_div__"+old_id).find(".add_more_transfer_div").append(' <img id="add_more_transfer'+old_id+'" class="add_more_transfer plus-icon"  src="'+add_url+'"   style="margin-left: auto;">');
            }
        });

          var filePreview = function(input, id) {
            if (input.files) {
               $('#'+id).parent().parent().find(".preview_images").empty();
               var filesAmount = input.files.length;

               for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();

                reader.onload = function(event) {
                    $('#'+id).parent().parent().find(".preview_images").append('<img src="'+event.target.result+'" width="150" height="150"/> &nbsp;');
                }

                reader.readAsDataURL(input.files[i]);
            }
        }
    };


        $(document).on("change",".vehicle_images",function()
        {      

            var id=this.id;
            if (parseInt($("#"+id).get(0).files.length) > 4){
              alert("You are only allowed to upload a maximum of 4 files");
              $("#"+id).val("");
              $('#'+id).parent().parent().find(".preview_images").empty();
          }
          else
          {

            var imageSize = document.getElementById(id);
            var count=0;
            for (var i = 0; i < imageSize.files.length; i++) {
                var image_Size = imageSize.files[i].size;
                    if(image_Size > 250000)
                    {
                       alert("Try to upload files less than 250KB!"); 
                        $("#"+id).val("");
                        $('#'+id).parent().parent().find(".preview_images").empty();
                        break;
                       count++;
                   }
               }
               if(count==0)
               {
                  filePreview(this,id);   
              }
          }
      });
    </script>
</body>
</html>
</body>
</html>