<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    .carousel-item img {
        height: 100%;
    }
    .carousel-item {
        height: 100%;
    }
    p.start_price {
        margin: 0;
    }
    p.country_name.ng-binding {
        font-size: 20px;
        margin: 0;
    }
    .book_card {
        /* padding: 15px; */
        background: #fefeff;
        box-shadow: 0 10px 15px -5px rgba(0, 0, 0, 0.07);
        margin-bottom: 50px;
        border-radius: 5px;
    }
    .hotel_detail {
        height: 150px;
    }
    a.moredetail.ng-scope {
        background: gainsboro;
        padding: 7px 10px;
        /* margin-bottom: 10px; */
    }
    .booking_label {
        background: #5d53ce;
        border-top-left-radius: 5px;
        border-top-right-radius: 5px;
        color: white;
        padding: 10px 15px;
    }
    .booking_detail {
        padding: 15px;
    }
    a.btn.btn-outline.btn-circle.book_btn1 {
        background: #E91E63;
        border-radius: 5px !IMPORTANT;
        padding: 5px 20px;
        width: auto !important;
        height: auto;
        line-height: 2;
        color: white;
    }
    td p {
        margin: 0;
    }
    td {
        background: gainsboro;
    }
    table {
        border-collapse: separate;
    }
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: 1px solid #59d25a;
    }
    a.panel-title {
        position: relative !important;
        background: #dfffe3;
        color: green !important;
        padding: 13px 20px 13px 85px !important;
        /* border-bottom: 1px solid #3ca23d; */
    }
    .panel-title {
        display: block;
        margin-top: 0;
        margin-bottom: 0;
        padding: 1.25rem;
        font-size: 18px;
        color: #4d4d4d;
        height: 48px;
    }
    .panel-group .panel-heading+.panel-collapse>.panel-body {
        border-top: none !important;
    }
    .panel-body {
        background: white;
        /* border: 1px solid #59d25a; */
        padding: 10px !important;
    }
    a.panel-title:before {
        content: attr(title) !important;
        position: absolute !important;
        top: 0px !important;
        opacity: 1 !important;
        left: 0 !important;
        padding: 12px 10px;
        width: auto;
        max-width: 250px;
        text-align: center;
        color: white;
        font-family: inherit !important;
        height: 48px;
        background: #279628;
        z-index: 999;
        transform: none !important;
    }
    .tab-content {
        margin-top: 10px;
    }
    div.panel-heading {
        border: 1px solid #59d25a !important;
    }
    .panel {
        border-top: none !important;
        margin-bottom: 5px !important;
    }
    div#carousel-example-generic-captions {
        width: 100%;
    }

    .table tr td, .table tr th {
        white-space: nowrap !important;
        padding: 10px 20px !important;
        background: white !important;
    }
    .table-condensed>thead>tr>th, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>tbody>tr>td, .table-condensed>tfoot>tr>td{
        padding: 2px;
    }

    div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
th
{
    text-align: center;
}

#actionModal
{
    z-index: 99999;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">B2B Bookings</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item active" aria-current="page">B2B Bookings
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="filters_div">
                        <div class="box">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group" id="sponser_div" style="display:block;">
                                            <label>From Date</label>
                                            <input type="text" id="from_date" name="from_date" class="form-control fromdatepicker" placeholder="From Date" autocomplete="off" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group" id="sponser_div" style="display:block;">
                                            <label>To Date</label>
                                            <input type="text" id="to_date" name="to_date" class="form-control todatepicker" placeholder="To Date" autocomplete="off" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-3">
                                        <div class="form-group">
                                            <label>Booking Type</label>
                                            <select class="form-control" id="booking_type" style="width: 100%;">
                                                <option selected="selected" value="0">All</option>
                                                <option value="hotel">Hotel</option>
                                                <option value="activity">Activity</option>
                                                <option value="sightseeing">Sightseeing</option>
                                                <option value="guide">Guide</option>
                                                <option value="driver">Driver</option>
                                                <option value="itinerary">Itinerary</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="box">
                            <div class="box-body">
                                <div class="row">
                                    <div class="col-md-12 table-responsive" id="showdata">
                                        <table id="booking_table" class="table table-condensed table-striped">
                                            <thead>
                                                <tr>
                                                    <td>Sr no.</td>
                                                    <td>Booking ID</td>
                                                    <td>Booked By</td>
                                                    <td>Type Name</td>
                                                    <td>Agent/Operator</td>
                                                    <td>Supplier</td>
                                                    <td>Supplier Number</td>
                                                    <td>Customer Name</td>
                                                    <td>Customer Email</td>
                                                    <td>Customer Mobile</td>
                                                    <td>Booking Date</td>
                                                    <td>Supplier Amount</td>
                                                    <td>Agent Amount</td>
                                                    <td>Total Customer Amount</td>
                                                    <td>Supplier's Approval</td>
                                                    <td>Supplier's Remarks</td>
                                                    <td>Admin Remarks</td>
                                                     <td>Booked From</td>
                                                    <td>Sub Bookings</td>
                                                    <td>Full Payment</td>
                                                    <td>Is Complete</td>
                                                    <td>Invoices</td>
                                                     <td>Installments</td>
                                                    <td>Action</td>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                                @foreach($fetch_bookings as $bookings)
                                                <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$bookings->booking_sep_id}}</td>
                                                    <td>{{ucwords($bookings->booking_type)}}</td>
                                                   
                                                    <td>@php
                                                        $booking_type_id=$bookings->booking_type_id;
                                                        if($bookings->booking_type=="activity")
                                                        {
                                                            $fetch_activity=ServiceManagement::searchActivity($booking_type_id);
                                                            echo $fetch_activity['activity_name'];
                                                        }
                                                        else if($bookings->booking_type=="hotel")
                                                        {
                                                            $fetch_hotel=ServiceManagement::searchHotel($booking_type_id);
                                                            echo $fetch_hotel['hotel_name'];
                                                        }
                                                        else if($bookings->booking_type=="guide")
                                                        {
                                                            $fetch_guide=ServiceManagement::searchGuide($booking_type_id);
                                                            echo $fetch_guide['guide_first_name']." ".$fetch_guide['guide_last_name'];
                                                        }
                                                        else if($bookings->booking_type=="driver")
                                                        {
                                                            $fetch_driver=ServiceManagement::searchDriver($booking_type_id);
                                                            echo $fetch_driver['driver_first_name']." ".$fetch_driver['driver_last_name'];
                                                        }
                                                        else if($bookings->booking_type=="sightseeing")
                                                        {
                                                            $fetch_sightseeing=ServiceManagement::searchSightseeingTourName($booking_type_id);
                                                            echo $fetch_sightseeing['sightseeing_tour_name'];
                                                        }
                                                        else if($bookings->booking_type=="itinerary")
                                                        {
                                                            $fetch_itinerary=ServiceManagement::searchItinerary($booking_type_id);
                                                            echo $fetch_itinerary['itinerary_tour_name'];
                                                        }
                                                         else if($bookings->booking_type=="restaurant")
                                                        {
                                                            $fetch_restaurant=ServiceManagement::searchRestaurant($booking_type_id);
                                                            echo $fetch_restaurant['restaurant_name'];
                                                        }
                                                        else if($bookings->booking_type=="transfer")
                                                        {
                                                            
                                                            echo $bookings->booking_subject_name;
                                                        }
                                                        
                                                    @endphp</td>
                                                    @if($bookings->booking_role=="AGENT")
                                                    <td>@php
                                                        $fetch_agent=ServiceManagement::searchAgent($bookings->booking_agent_id);
                                                        echo $fetch_agent['agent_name'];
                                                    @endphp</td>
                                                    @else
                                                    <td>@php
                                                        $fetch_user=ServiceManagement::searchUser($bookings->booking_agent_id);
                                                        echo $fetch_user['users_fname']." ".$fetch_user['users_lname'];
                                                    @endphp</td>

                                                    @endif
                                                   
                                                    @if($bookings->booking_type=="sightseeing" || $bookings->booking_type=="itinerary")
                                                        <td>NA</td><td>NA</td>
                                                       @else
                                                        @php
                                                        $fetch_supplier=ServiceManagement::searchSupplier($bookings->booking_supplier_id);
                                                        echo "<td>".$fetch_supplier['supplier_name']."</td><td>".$fetch_supplier['company_contact']."</td>";
                                                    @endphp
                                                       @endif
                                                    <td>{{$bookings->customer_name}}</td>
                                                    <td>{{$bookings->customer_email}}</td>
                                                    <td>{{$bookings->customer_contact}}</td>
                                                    <td>{{$bookings->booking_date}}</td>
                                                    <td>{{$bookings->booking_currency}} {{$bookings->booking_supplier_amount}}</td>
                                                    <td>{{$bookings->booking_currency}} {{$bookings->booking_agent_amount}}</td>
                                                    <td>{{$bookings->booking_currency}} {{$bookings->booking_amount}}</td>
                                                    <td id="status_{{$bookings->booking_id}}">
                                                        
                                                        @if($bookings->booking_supplier_status==0)
                                                        <button class="btn btn-sm btn-warning">Pending</button>
                                                        @elseif($bookings->booking_supplier_status==1)
                                                        <button class="btn btn-sm btn-success">Confirmed</button>
                                                        @elseif($bookings->booking_supplier_status==2)
                                                        <button class="btn btn-sm btn-danger">Rejected</button>
                                                        @endif
                                                    </td>
                                                    <td>{{$bookings->booking_supplier_message}}</td>
                                                    <td id="remarks_booking_{{$bookings->booking_id}}">{{$bookings->booking_admin_message}}</td>
                                                     <td>{{$bookings->booking_role}} PORTAL</td>
                                                      <td>
                                                        @if($bookings->booking_type=="itinerary" || $bookings->booking_type=="transfer" || ($bookings->booking_type=="sightseeing" && $bookings->booking_supplier_id!=null))
                                                        <a class="btn btn-info btn-sm" href="{{route('agent-sub-bookings',['booking_id'=>$bookings->booking_sep_id])}}" target="_blank">View Sub Bookings</a>
                                                        @else
                                                        No Sub Bookings
                                                    @endif</td>
                                                    <td>
                                                    	@if($bookings->itinerary_status==0)
                                                    	@if($bookings->booking_final_amount_status==0)
                                                    	<button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button>
                                                    	@elseif($bookings->booking_final_amount_status==1)
                                                    	<button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button>
                                                    	@endif
                                                    	@endif
                                                    </td>
                                                    <td>
                                                        @if($bookings->itinerary_status==0)
                                                        @if($bookings->booking_complete_status==0)
                                                        <button type="button" class="btn btn-sm btn-rounded btn-warning">Pending</button>
                                                        @elseif($bookings->booking_complete_status==1)
                                                        <button type="button" class="btn btn-sm btn-rounded btn-primary">Complete</button>
                                                        @endif
                                                        @endif
                                                    </td>
                                                    @if($bookings->itinerary_status==0 || $bookings->itinerary_status==null)
                                                    <td>
                                                         <a href="{{route('admin-agent-invoice',['booking_id'=>$bookings->booking_sep_id])}}" class="btn btn-rounded btn-default btn-sm">Agent Invoice</a> &nbsp;
                                                        <a href="{{route('admin-customer-invoice',['booking_id'=>$bookings->booking_sep_id])}}" class="btn btn-rounded btn-default btn-sm">Customer Invoice</a>
                                                        
                                                    </td>
                                                    @else
                                                    <td></td>
                                                    @endif
                                                    <td>
                                                         @if(($bookings->itinerary_status==0 || $bookings->itinerary_status==null))
                                                        <button id="installment_{{$bookings->booking_sep_id}}" class="btn btn-rounded btn-primary btn-sm create_installment" @if($bookings->booking_admin_status!=1) disabled @endif>Installments</button>
                                                        @elseif($bookings->booking_final_amount_status==1)
                                                            Completed 
                                                        @else
                                                        Not Available
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <span>
                                                            @if($bookings->booking_status==2)
                                                            <button type="button" class="btn btn-sm btn-rounded btn-info" disabled="disabled">Cancelled</button>
                                                            @elseif($bookings->booking_admin_status==1)
                                                            <button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>
                                                            @elseif($bookings->booking_admin_status==0)
                                                            <button type="button" class="btn btn-sm btn-rounded approve btn-primary" id="approve_booking_{{$bookings->booking_id}}">Approve</button>
                                                            <button type="button" class="btn btn-sm btn-rounded reject btn-danger" id="reject_booking_{{$bookings->booking_id}}">Reject</button>
                                                            @elseif($bookings->booking_admin_status==2)
                                                            <button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>
                                                            @endif
                                                        </span>
                                                       
                                                       

                                                        @if($bookings->itinerary_status==0 || $bookings->itinerary_status==null)
                                                        <a class="btn btn-rounded btn-success btn-sm" href="{{route('booking-details')}}?booking_id={{$bookings->booking_sep_id}}" target="_blank">View Details</a>
                                                        @endif
                                                    </td>



                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('mains.includes.footer')
        @include('mains.includes.bottom-footer')
        <div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
        <script>
            $(document).ready(function() {
                $('#agent_bookings').DataTable();
            });
        </script>
        <div class="modal" id="actionModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Peform Operation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="action_id" id="action_id">
                                 <input type="hidden" name="action_purpose" id="action_purpose">
                                  <div class="form-group" id="action_amount_div">
                                    <label for="action_amount" id="action_amount_label">Installment Amount</label>
                                    <input type="text" name="action_amount" id="action_amount" class="form-control"   onkeypress="javascript:return validateNumber(event)"  onpaste="javascript:return validateNumber(event)">
                                </div>
                                <div class="form-group">
                                    <label for="remarks" id="remarks_label">Remarks for Approval</label>
                                    <textarea name="remarks" id="remarks" class="form-control" rows="5"></textarea>
                                </div>
                                
                                
                                <button type="button" id="actionButton" class="btn btn-primary pull-right">Submit</button>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

            <div class="modal" id="installmentModal">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Installments</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                             <div class="col-md-12 mb-25">
                                <button data-toggle="collapse" data-target="#pre_install_table" class="btn btn-sm btn-primary">Existing Installments <i class="fa fa-caret-down"></i></button>
                                <div id="pre_install_table" class="collapse">
                                </div>
                             </div>
                            <div class="col-md-12 mb-25">
                                 <p>Payment Details</p>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><strong>Total Amount:</strong> <br> GEL <span id="total_amount_pay">0</span></p> 
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong>Remaining Amount:</strong> <br> GEL <span id="rem_amount_pay">0</span></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <p><strong>Recieved Payment:</strong> <br> GEL <span id="rec_amount_pay">0</span></p> 
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <p><strong>Pending Payment:</strong> <br> GEL <span id="pen_amount_pay">0</span></p>
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-12">
                             	<div class="row">
                             		<div class="col-md-6">
                             			<h3>Create New Installment</h3>
                             		</div>
                             	</div>
                               <div class="row">
                                   <div class="col-md-3">
                                       <label>Installment Amount</label>
                                   </div>
                                   <div class="col-md-6">
                                       <div class="form-group">
                                            <input type="text" name="pay_amount" id="pay_amount" placeholder="Enter Amount" class="form-control"  onkeypress="javascript:return validateNumber(event)"  onpaste="javascript:return validateNumber(event)">
                                            <span style="color:red" id="pay_amount_error"></span>

                                             <span style="color:green" id="pay_amount_success"></span>
                                       </div>
                                   </div>
                               </div>
                             </div>
                            <div class="col-md-12">
                                <input type="hidden" name="main_booking_id" id="main_booking_id">
                                <button type="button" id="createInstallmentButton" class="btn btn-sm btn-primary pull-right">Generate Installment</button>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <script>
            $(document).ready(function()
            {
        //Departure
        var date = new Date();
        date.setDate(date.getDate());
        $('.fromdatepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            endDate: date
        })
        //Date picker
        $('.todatepicker').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy',
            endDate: date
        });
        $('#booking_table').DataTable({ "scrollX": true});
        $(document).on("click",".approve",function()
        {
            var id=this.id;
            $("#action_id").attr("value",id);
            $("#action_purpose").val("booking");
            $("#action_amount_div").hide();
            $("#actionModal").find(".modal-title").text("Perform Approval Operation");
            $("#remarks_label").text("Remarks for Approval");
            $("#remarks").val("");
            $("#actionModal").modal("show");
        });
        $(document).on("click",".reject",function()
        {
            var id=this.id;
            $("#action_id").attr("value",id);
             $("#action_purpose").val("booking");
             $("#action_amount_div").hide();
            $("#actionModal").find(".modal-title").text("Perform Rejection Operation");
            $("#remarks_label").text("Remarks for Rejection");
            $("#remarks").val("");
            $("#actionModal").modal("show");
        });
         $(document).on("click",".approve_payment",function()
        {
            var id=this.id;
            var amount=$("#"+id+"_amount").text();
            var actual_amount=amount.split(" ");
            $("#action_id").attr("value",id);
            $("#action_purpose").val("payment");
            $("#action_amount_div").show();
            $("#action_amount").val(actual_amount[1]);
            $("#actionModal").find(".modal-title").text("Perform Approval Operation");
            $("#remarks_label").text("Remarks for Approval");
            $("#remarks").val("");
            $("#actionModal").modal("show");
        });
        $(document).on("click",".reject_payment",function()
        {
            var id=this.id;
             var amount=$("#"+id+"_amount").text();
            var actual_amount=amount.split(" ");
            $("#action_id").attr("value",id);
             $("#action_purpose").val("payment");
             $("#action_amount_div").hide();
              $("#action_amount").val("");
            $("#actionModal").find(".modal-title").text("Perform Rejection Operation");
            $("#remarks_label").text("Remarks for Rejection");
            $("#remarks").val("");
            $("#actionModal").modal("show");
        });
        $(document).on("click",'#actionButton',function()
        {
            var action_purpose=$("#action_purpose").val();

            if(action_purpose=="booking")
            {
                  var id=$("#action_id").val();
            var actual_id=id.split("_");
            var action_perform=actual_id[0];
            var action_id=actual_id[2];
            var remarks=$("#remarks").val();
            swal({
                title: "Are you sure?",
                text: "You want to "+action_perform+" this booking !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Go Ahead",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:"{{route('approve-booking')}}",
                        type:"POST",
                        data:{"_token":"{{csrf_token()}}",
                        "action_perform":action_perform,
                        "booking_id":action_id,
                        "remarks":remarks},
                        success:function(response)
                        {
                            if(action_perform=="approve")
                            {
                                if(response.indexOf("reload")!=-1)
                                {
                                    $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');
                                    $("#remarks_booking_"+action_id).text(remarks);
                                    $("#remarks_booking_"+action_id).parent().find(".create_installment").removeAttr("disabled");
                                    swal("Approved!", "Selected Booking has been approved.", "success");
                                }
                                else if(response.indexOf("success")!=-1)
                                {
                                    $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');
                                    $("#remarks_booking_"+action_id).text(remarks);
                                   $("#remarks_booking_"+action_id).parent().find(".create_installment").removeAttr("disabled");
                                    swal("Approved!", "Selected Booking has been approved.", "success");
                                }
                                else if(response.indexOf("cancel")!=-1)
                                {
                                    
                                    swal("Error", "This booking is cancelled by agent , so it cannot be approved", "error");
                                }
                                else
                                {
                                    swal("Error", "Unable to perform this operation", "error");
                                }
                            }
                            else
                            {
                                if(response.indexOf("reload")!=-1)
                                {
                                    $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');
                                    $("#remarks_booking_"+action_id).text(remarks);
                                    swal("Rejected!", "Selected Booking has been rejected.", "success");
                                }
                                else if(response.indexOf("success")!=-1)
                                {
                                    $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');
                                    $("#remarks_booking_"+action_id).text(remarks);
                                    swal("Rejected!", "Selected Booking has been rejected.", "success");
                                }
                                else if(response.indexOf("cancel")!=-1)
                                {
                                    
                                    swal("Error", "This booking is cancelled by agent , so it cannot be rejected", "error");
                                }
                                else
                                {
                                    swal("Error", "Unable to perform this operation", "error");
                                }
                            }
                            $("#actionModal").modal("hide");
                        }
                    });
                } else {
                    swal("Cancelled", "Operation Cancelled", "error");
                }
            });
     

            }
            else
            {
                  var id=$("#action_id").val();
                   var amount=$("#action_amount").val();
            var actual_id=id.split("_");
            var action_perform=actual_id[0];
            var action_id=actual_id[2];
            var remarks=$("#remarks").val();
            swal({
                title: "Are you sure?",
                text: "You want to "+action_perform+" this payment !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Go Ahead",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url:"{{route('approve-payment')}}",
                        type:"POST",
                        data:{"_token":"{{csrf_token()}}",
                        "action_perform":action_perform,
                        "amount":amount,
                        "installment_id":action_id,
                        "remarks":remarks},
                        success:function(response)
                        {
                            if(action_perform=="approve")
                            {
                                if(response.indexOf("reload")!=-1)
                                {
                                    location.reload();
                                }
                                else if(response.indexOf("success")!=-1)
                                {
                                     swal("Approved!", "Selected Payment has been approved.", "success");
                                    $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-primary" disabled="disabled">Approved</button>');
                                    $("td#remarks_booking_"+action_id).text(remarks);

                                    setTimeout(function()
                                    {
                                        $("#installmentModal").modal("hide");
                                    },500);
                                   
                                }
                                else
                                {
                                    swal("Error", "Unable to perform this operation", "error");
                                }
                            }
                            else
                            {
                                if(response.indexOf("reload")!=-1)
                                {
                                    location.reload();
                                }
                                else if(response.indexOf("success")!=-1)
                                {
                                   $("#"+id).parent().html('<button type="button" class="btn btn-sm btn-rounded btn-danger" disabled="disabled">Rejected</button>');
                                    $("td#remarks_booking_"+action_id).text(remarks);
                                    swal("Rejected!", "Selected Payment has been rejected.", "success");
                                    setTimeout(function()
                                    {
                                        $("#installmentModal").modal("hide");
                                    },500);
                                }
                                else
                                {
                                    swal("Error", "Unable to perform this operation", "error");
                                }
                            }
                            setTimeout(function()
                                    {
                                        $("#actionModal").modal("hide");
                                    },500);

                        }
                    });
                } else {
                    swal("Cancelled", "Operation Cancelled", "error");
                }
            });
     

            }

               });
          
});
</script>
<script>
    var mainStuff = function () {
        var to_date=$('#to_date').val();
        var from_date=$('#from_date').val();
        var booking_type=$('#booking_type').val();
        if(from_date!="" || to_date!="")
        {
            $('#showdata').html("<h4 class='text-center'><b>Loading....</b></h4>");
            $.ajax({
                url: "{{route('agent-bookings-filters')}}",
                data: {
                    'from_date':from_date,
                    'to_date':to_date,
                    'booking_type':booking_type,
                },
                type: 'GET',
                success: function (data)
                {
                    if(data=="")
                    {
                    }
                    else
                    {
                        $('#showdata').html(data);
                        $('#booking_table').DataTable({ "scrollX": true});
                    }
                }
            });
        }
    }
    $('#from_date').on('change',mainStuff);
    $('#to_date').on('change',mainStuff);
    $('#booking_type').on('change',mainStuff);



    $(document).on("click",".create_installment",function()
    {
    	$("#pay_amount_error").hide();
         $("#pay_amount_success").hide();
         $("#pay_amount").val("");
        var id=this.id;

        var actual_id=id.split("_")[1];

       
    $("#loaderModal").modal("show");
         $.ajax({
                url: "{{route('get-booking-installment-detail')}}",
                data: {
                    'booking_id':actual_id,
                },
                type: 'GET',
                dataType:"JSON",
                success: function (response)
                {

                    $("#main_booking_id").val(actual_id);
                    $("#total_amount_pay").text(response.total_amount);
                    $("#rem_amount_pay").text(response.remaining_amount);
                     $("#rec_amount_pay").text(response.received_amount);
                    $("#pen_amount_pay").text(response.pending_amount);
                     $("#pre_install_table").html(response.previous_installments_html);

                     // $('#installments_table').DataTable({ "scrollX": true});
                    $("#installmentModal").modal("show");


                    setTimeout(function()
                    {
                    	    $("#loaderModal").modal("hide");
                    },500);


                }
            });
       

    });


    $(document).on("click","#createInstallmentButton",function()
    {
    	var main_booking_id=$("#main_booking_id").val();
    	var total_amount=$("#total_amount_pay").text();
    	var remaning_amount=$("#rem_amount_pay").text();
    	var pay_amount=$("#pay_amount").val();


    	if(pay_amount.trim()=="")
    	{
			$("#pay_amount_error").text("Amount cannot be empty").show();
    	}
    	else if(pay_amount.trim()<0)
    	{
    		$("#pay_amount_error").text("Amount cannot be less than or equal to 0").show();
    	}
    	// else if(parseInt(pay_amount.trim())>parseInt(total_amount.trim()))
    	// {
    	// 	$("#pay_amount_error").text("Amount cannot be more than total amount i.e GEL "+total_amount).show();
    	// }
    	// else if(parseInt(pay_amount.trim())>parseInt(remaning_amount.trim()))
    	// {
    	// 	$("#pay_amount_error").text("Amount cannot be more than remaining amount i.e GEL "+remaning_amount).show();
    	// }
    	else
    	{
    		$("#pay_amount_error").hide();
    		$("#createInstallmentButton").prop("disabled",true);
    		 $.ajax({
                url: "{{route('insert-installment')}}",
                data: {
                    'booking_id':main_booking_id,
                    'pay_amount':pay_amount,
                    '_token':"{{csrf_token()}}",

                },
                type: 'POST',
                success: function (response)
                {

                	if(response.indexOf("success")!=-1)
                	{
                		$("#pay_amount_success").text("Installment created successfully").show();
                		$("#pay_amount_error").hide();

                		setTimeout(function()
                		{
                			$("#pay_amount").val("");
                			$("#installmentModal").modal("hide");
                		},1000);
                	}
                	else if(response.indexOf("fail")!=-1)
                	{
                		$("#pay_amount_error").text("Try Again Later").show();
                		$("#pay_amount_success").hide();
                	}
                	else if(response.indexOf("no_more")!=-1)
                    {
                        $("#pay_amount_error").text("No more installments can be created.").show();
                        $("#pay_amount_success").hide();
                    }
                     else if(response.indexOf("more_amount")!=-1)
                    {
                        $("#pay_amount_error").text("Amount cannot exceed remaining amount.").show();
                        $("#pay_amount_success").hide();
                    }

	$("#createInstallmentButton").prop("disabled",false);


                }
            });
    	}


    });
</script>
</body>
</html>