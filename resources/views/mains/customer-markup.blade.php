@include('mains.includes.top-header')
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Customer Markup Cost</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Masters</li>
                                            <li class="breadcrumb-item active" aria-current="page">Add New Customer Markup Cost
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                            <!--  <div class="right-title">
                                <div class="dropdown">
                                    <button class="btn btn-outline dropdown-toggle no-caret" type="button" data-toggle="dropdown"><i
                                    class="mdi mdi-dots-horizontal"></i></button>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-share"></i>Activity</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-email"></i>Messages</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-help-circle-outline"></i>FAQ</a>
                                        <a class="dropdown-item" href="#"><i class="mdi mdi-settings"></i>Support</a>
                                        <div class="dropdown-divider"></div>
                                        <button type="button" class="btn btn-rounded btn-success">Submit</button>
                                    </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    @if($rights['add']==1)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Add New Customer Markup Cost</h4>
                                </div>
                                <div class="box-body">
                                    <form class="package_form" action="javascript:void()" method="POST" id="menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <label>Customer Markup Name <span class="asterisk">*</span></label>
                                                    <select class="form-control" id="customer_markup_name" name="customer_markup_name" autofocus="autofocus">
                                                        <option value="0">SELECT CUSTOMER MARKUP</option>
                                                          <option value="Hotel">Hotel</option>
                                                        <option value="Activity">Activity</option>
                                                         <option value="Sightseeing">Sightseeing</option>
                                                        <option value="Guide">Guide</option>
                                                         <option value="Driver">Driver</option>
                                                          <option value="Transfer">Transfer</option>
                                                           <option value="Restaurant">Restaurant</option>
                                                           <option value="Itinerary">Itinerary</option>

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <label>Customer Markup Cost <span class="asterisk">*</span></label>
                                                    <input type="number" min=0 class="form-control" placeholder="Customer Markup Cost" id="customer_markup_cost" name="customer_markup_cost" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-md-12">
                                                <button type="button"  id="save_customer_markup" class="btn btn-rounded btn-primary mr-10 pull-right">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">View Customer Markup Cost</h4>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example1" class="table table-bordered table-striped datatable">
                                            <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NAME</th>
                                                <th>COST</th>
                                                  <th>ACTION</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($fetch_customer_markup as $customer_markup)
                                            <tr class="tr_{{$customer_markup->customer_markup_id}}">
                                                <td>{{$customer_markup->customer_markup_id}}</td>
                                                <td id="customer_markup_{{$customer_markup->customer_markup_id}}">{{$customer_markup->customer_markup}}</td>
                                                <td id="customer_markup_cost_{{$customer_markup->customer_markup_id}}">{{$customer_markup->customer_markup_cost}}</td>
                                                 <td><button id="edit_{{$customer_markup->customer_markup_id}}" class="btn btn-default btn-rounded edit_fuel"><i class="fa fa-pencil"></i></button></td>
                                            </tr>

                                            @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <h4 class="text-danger">No rights to access this page</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('mains.includes.footer')
    @include('mains.includes.bottom-footer')
       <!-- Modal -->
<div id="editModal" class="modal fade" role="dialog" style="z-index: 9999">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
       
        <h4 class="modal-title">Edit Customer Markup</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="package_form" action="javascript:void()" method="POST" id="edit_menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                     <input type="hidden" id="edit_customer_markup_id" name="customer_markup_id">
                                                    <label>Customer Markup Name <span class="asterisk">*</span></label>
                                                    <select class="form-control" id="edit_customer_markup_name" name="customer_markup_name">
                                                        <option value="Hotel">Hotel</option>
                                                        <option value="Activity">Activity</option>
                                                         <option value="Sightseeing">Sightseeing</option>
                                                        <option value="Guide">Guide</option>
                                                         <option value="Driver">Driver</option>
                                                          <option value="Transfer">Transfer</option>
                                                           <option value="Restaurant">Restaurant</option>
                                                           <option value="Itinerary">Itinerary</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                         <div class="row mb-10">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-group">
                                                    <label>Customer Markup Cost <span class="asterisk">*</span></label>
                                                    <input type="number" min=0 class="form-control" placeholder="Customer Markup Cost" id="edit_customer_markup_cost" name="customer_markup_cost">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
            </div>
        </div>
        
      </div>
      <div class="modal-footer">
        
         <button type="button"  id="update_customer_markup" class="btn btn-rounded btn-primary mr-10 ">Update</button>
        <button type="button" class="btn btn-default mr-10 pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <script>
    $(document).ready(function()
    {
        $(".datatable").DataTable();

        $(".select2").select2();
    });

    $(document).on("click","#save_customer_markup",function()
    {
        var customer_markup_name=$("#customer_markup_name").val();
        var customer_markup_cost=$("#customer_markup_cost").val();

        if(customer_markup_name.trim()=="0")
        {
            $("#customer_markup_name").css("border","1px solid #cf3c63");
        }
        else
        {
            $("#customer_markup_name").css("border","1px solid #9e9e9e");
        }

        if(customer_markup_cost.trim()=="")
        {
            $("#customer_markup_cost").css("border","1px solid #cf3c63");
        }
        else
        {
            $("#customer_markup_cost").css("border","1px solid #9e9e9e");
        }


        if(customer_markup_name.trim()=="0")
        {
            $("#customer_markup_name").focus();
        }
        else  if(customer_markup_cost.trim()=="")
        {
            $("#customer_markup_cost").focus();
        }
        else
        {
            $("#save_customer_markup").prop("disabled",true);
            var formdata=new FormData($("#menu_form")[0]);
            $.ajax({
                url:"{{route('customer-markup-insert')}}",
                data: formdata,
                type:"POST",
                processData: false,
                contentType: false,
                success:function(response)
                {
                    if(response.indexOf("exist")!=-1)
                    {
                        swal("Already Exist!", "Customer Markup Cost already exists");
                    }
                    else if(response.indexOf("success")!=-1)
                    {
                        swal({title:"Success",text:" New Customer Markup Cost Created Successfully !",type: "success"},
                            function(){
                                location.reload();
                            });
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        swal("ERROR", "Customer Markup Cost cannot be inserted right now! ");
                    }
                    $("#save_customer_markup").prop("disabled",false);
                }
            });
        }
    });
</script>

    <script> 
    $(document).on("click",".edit_fuel",function()
    {
        var id=this.id;

        var actual_id=id.split("_");

        var customer_markup_cost=$("#customer_markup_cost_"+actual_id[1]).text();
        var customer_markup_name=$("#customer_markup_"+actual_id[1]).text();
        $("#edit_customer_markup_cost").val(customer_markup_cost);
        $("#edit_customer_markup_name").val(customer_markup_name);
        $("#edit_customer_markup_id").val(actual_id[1]);

         $("#edit_customer_markup_name option").each(function(){
            if($(this).val()!=customer_markup_name)
            {
                $(this).attr("disabled","disabled");
            }
            else
            {
               $(this).removeAttr("disabled");  
            }
    });


        $("#editModal").modal("show"); 
    });

    $(document).on("click","#update_customer_markup",function()
    {
    var customer_markup_name=$("#edit_customer_markup_name").val();
    var customer_markup_cost=$("#edit_customer_markup_cost").val();

    if(customer_markup_name.trim()=="0")
    {
    $("#edit_customer_markup_name").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#edit_customer_markup_name").css("border","1px solid #9e9e9e");
    }

    if(customer_markup_cost.trim()=="")
    {
    $("#edit_customer_markup_cost").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#edit_customer_markup_cost").css("border","1px solid #9e9e9e");
    }
    
    
    if(customer_markup_name.trim()=="0")
    {
    $("#edit_customer_markup_name").focus();
    }
    else  if(customer_markup_cost.trim()=="")
    {
    $("#edit_customer_markup_cost").focus();
    }
    else
    {
    $("#update_customer_markup").prop("disabled",true);
            var formdata=new FormData($("#edit_menu_form")[0]);
            $.ajax({
                url:"{{route('customer-markup-update')}}",
                data: formdata,
                type:"POST",
                processData: false,
                contentType: false,
                success:function(response)
                {
                    if(response.indexOf("exist")!=-1)
                    {
                        swal("Already Exist!", "Customer Markup Cost already exists");
                    }
                    else if(response.indexOf("success")!=-1)
                    {
                        swal({title:"Success",text:"Customer Markup Cost Updated Successfully !",type: "success"},
                            function(){
                                location.reload();
                            });
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        swal("ERROR", "Customer Markup Cost cannot be updated right now! ");
                    }
                    $("#update_customer_markup").prop("disabled",false);
                }
            });
    }
    });
    </script>

</body>
</html>