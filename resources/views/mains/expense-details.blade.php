

@include('mains.includes.top-header')

<style>

    .iti-flag {

        width: 20px;

        height: 15px;

        box-shadow: 0px 0px 1px 0px #888;

        background-image: url("{{asset('assets/images/flags.png')}}") !important;

        background-repeat: no-repeat;

        background-color: #DBDBDB;

        background-position: 20px 0

    }



    div#cke_1_contents {

        height: 250px !important;

    }

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative">	

@include('mains.includes.nav')

	<div class="content">

	<!-- Content Header (Page header) -->

	<div class="content-header">

		<div class="d-flex align-items-center">

			<div class="mr-auto">

				<h3 class="page-title">{{ucfirst($fetch_expense_detail->expenses_type)}} Expense Management</h3>

				<div class="d-inline-block align-items-center">

					<nav>

						<ol class="breadcrumb">

							 <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Home</li>

                            @if($fetch_expense_detail->expenses_type=="booking")
    
                            <li class="breadcrumb-item" aria-current="page">Bookings</li>

                            <li class="breadcrumb-item active" aria-current="page">Booking Expense Details

                            @else

                            <li class="breadcrumb-item" aria-current="page">Expenses</li>

                            <li class="breadcrumb-item active" aria-current="page">Office Expense Details

                            @endif

						</ol>

					</nav>

				</div>

			</div>

		</div>

	</div>




@if($rights['view']==1)
	<div class="row">







		<div class="col-12">

			<div class="box">

				<div class="box-body">

					<div class="row">

						<div class="col-md-3">

							<label for="users_empcode"><strong>EXPENSE TYPE :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_empcode"> @if($fetch_expense_detail->expenses_type!="" && $fetch_expense_detail->expenses_type!="0" && $fetch_expense_detail->expenses_type!=null) {{strtoupper($fetch_expense_detail->expenses_type)}} EXPENSES @else No Data Available @endif </p>

						</div>

					</div>

					<div class="row">

						<div class="col-md-3">

							<label for="users_username"><strong>EXPENSE CATEGORY :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_username"> @if($fetch_expense_detail->expense_category_id!="" && $fetch_expense_detail->expense_category_id!="0" && $fetch_expense_detail->expense_category_id!=null){{$fetch_expense_detail->get_expense_category['expense_category_name']}} @else No Data Available @endif </p>

						</div>

					</div>

					<div class="row">

						<div class="col-md-3">

							<label for="users_empcode"><strong>OCCURED ON :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_empcode"> @if($fetch_expense_detail->expense_occured_on!="" && $fetch_expense_detail->expense_occured_on!="0" && $fetch_expense_detail->expense_occured_on!=null) {{date('d/m/Y h:i a',strtotime($fetch_expense_detail->expense_occured_on))}} @else No Data Available @endif </p>

						</div>

					</div>

					<div class="row">

						<div class="col-md-3">

							<label for="users_empcode"><strong>AMOUNT :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_empcode"> @if($fetch_expense_detail->expense_amount!="" && $fetch_expense_detail->expense_amount!=null) GEL {{$fetch_expense_detail->expense_amount}} @else No Data Available @endif </p>

						</div>

					</div>

						<div class="row">

						<div class="col-md-3">

							<label for="users_empcode"><strong>EXPENSE REMARKS :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_empcode"> @if($fetch_expense_detail->expense_remarks!="" && $fetch_expense_detail->expense_remarks!=null) {{$fetch_expense_detail->expense_remarks}} @else No Data Available @endif </p>

						</div>

					</div>

					

					<div class="row">

						<div class="col-md-3">

							<label for="users_empcode"><strong>CREATED BY :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_empcode"> @if($fetch_expense_detail->expense_created_by!="" && $fetch_expense_detail->expense_created_by!=null)
								@foreach($users as $user)
								@if($fetch_expense_detail->expense_created_by==$user->users_id)
								{{$user->users_fname}} {{$user->users_lname}}
								@endif
								@endforeach
							@else No Data Available @endif 
						</p>

						</div>

					</div>




					
					<div class="row">

						<div class="col-md-3">

							<label for="users_empcode"><strong>CREATED AT :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_empcode"> @if($fetch_expense_detail->created_at!="" && $fetch_expense_detail->created_at!="0" && $fetch_expense_detail->created_at!=null) {{date('d/m/Y h:i a',strtotime($fetch_expense_detail->created_at))}} @else No Data Available @endif </p>

						</div>

					</div>

					<div class="row">

						<div class="col-md-3">

							<label for="users_empcode"><strong>UPDATED AT :</strong></label>

						</div>

						<div class="col-md-9">

							<p class="" id="users_empcode"> @if($fetch_expense_detail->updated_at!="" && $fetch_expense_detail->updated_at!="0" && $fetch_expense_detail->updated_at!=null) {{date('d/m/Y h:i a',strtotime($fetch_expense_detail->updated_at))}} @else No Data Available @endif </p>

						</div>

					</div>




					  <div class="row mb-10">

                        <div class="col-md-12">

                            <div class="box-header with-border"

                                style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">

                                <button type="button"  id="back_btn" onclick="window.history.back()" class="btn btn-rounded btn-primary mr-10">Back</button>

                            </div>

                        </div>

                    </div>

					

					

					

				</div>









				<!-- /.row -->

			</div>

			<!-- /.box-body -->

		</div>



		<!-- /.box -->

	</div>
	      @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif

</div>

</div>

</div>

</div>



@include('mains.includes.footer')

@include('mains.includes.bottom-footer')



</body>





</html>

