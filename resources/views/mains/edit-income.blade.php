@include('mains.includes.top-header')
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

<div class="wrapper">

@include('mains.includes.top-nav')

<div class="content-wrapper">

    <div class="container-full clearfix position-relative"> 

@include('mains.includes.nav')

    <div class="content">



    <div class="content-header">

        <div class="d-flex align-items-center">

            <div class="mr-auto">

                <h3 class="page-title">{{ucfirst($incomes_type)}} Incomes Management</h3>

                <div class="d-inline-block align-items-center">

                    <nav>

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

                            <li class="breadcrumb-item" aria-current="page">Home</li>

                               @if($incomes_type=="booking")
    
                            <li class="breadcrumb-item" aria-current="page">Bookings</li>

                            <li class="breadcrumb-item active" aria-current="page">Edit Booking Income

                            @else

                              <li class="breadcrumb-item" aria-current="page">Incomes / Expenses</li>

                            <li class="breadcrumb-item active" aria-current="page">Edit Office Income</li>

                            @endif


                        </ol>

                    </nav>

                </div>

            </div>
        </div>

    </div>

@if($rights['add']==1)
    <div class="row">

        <div class="col-12">

            <div class="box">

                <div class="box-header with-border">

                    <h4 class="box-title">Edit Income</h4>

                </div>

                <div class="box-body">

                      <form id="user_form" method="post" accept-charset="utf-8">

                         {{csrf_field()}}

                         @if($incomes_type=="booking")
                         <div class="row mb-10">
                            <div class="col-sm-6 col-md-6">
                                <div class="form-group">

                                    <label>Booking ID<span class="asterisk">*</span></label>

                                    <select id="income_booking_id" name="income_booking_id" class="form-control select2" style="width: 100%;">
                                        <option selected="selected" value="0" hidden="hidden">--Select Booking ID--</option>
                                        @foreach($fetch_bookings as $booking)
                                        <option value="{{$booking->booking_sep_id}}" @if($fetch_incomes->incomes_booking_id==$booking->booking_sep_id) selected="selected" @endif>{{$booking->booking_sep_id}} ({{$booking->booking_type}})</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>
                        </div>
                        @else
                        <input type="hidden" name="income_booking_id" id="income_booking_id">
                        @endif


                    <div class="row mb-10">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">

                                        <label>Income Category<span class="asterisk">*</span></label>

                                         <input type="hidden" name="income_type" id="income_type" value="{{$incomes_type}}">
                                          <input type="hidden" name="income_id" id="income_id" value="{{$fetch_incomes->incomes_id}}">

                                        <select id="income_category" name="income_category" class="form-control select2" style="width: 100%;">
                                            <option selected="selected" value="0" hidden="hidden">--Select Category--</option>
                                            @foreach($fetch_income_category as $income_category)
                                                <option value="{{$income_category->expense_category_id}}" @if($fetch_incomes->incomes_category_id==$income_category->expense_category_id) selected="selected" @endif>{{$income_category->expense_category_name}}</option>
                                            @endforeach

                                        </select>
                            </div>
                        </div>
                    </div>

                    
                     <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>Occured On <span class="asterisk">*</span></label>
                            <div class='input-group date' id='datetimepicker1'>
                    <input type='text' class="form-control" placeholder="Select Date & Time" id="income_occured" name="income_occured" readonly="readonly"  value="{{$fetch_incomes->incomes_occured_on}}" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>

                            </div>

                        </div>

                    </div>

                     <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>Amount <span class="asterisk">*</span></label>

                                <input type="text" class="form-control" placeholder="Enter Amount" id="income_amount" name="income_amount" onkeypress="javascript:return validateNumber(event)" onpaste="javascript:return validateNumber(event)" value="{{$fetch_incomes->incomes_amount}}">

                            </div>

                        </div>

                    </div>

                       <div class="row mb-10">

                        <div class="col-sm-6 col-md-6">

                            <div class="form-group">

                                <label>Remarks <span class="text-info">(optional)</span></label>

                               <textarea name="income_remarks" id="income_remarks" class="form-control" placeholder="Enter Remarks Here...">{{$fetch_incomes->incomes_remarks}}</textarea>

                            </div>

                        </div>

                    </div>


                    <div class="row mb-10">

                        <div class="col-md-12">

                            <div class="box-header with-border"

                                style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">

                                <button type="button"  id="update_income" class="btn btn-rounded btn-primary mr-10">Update</button>

                                <button type="button" id="discard_income" class="btn btn-rounded btn-primary">Discard</button>

                            </div>

                        </div>

                    </div>

                </form>

                </div>

            </div>

        </div>

    </div>
 @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif
</div>

</div>

</div>





@include('mains.includes.footer')

@include('mains.includes.bottom-footer')

<script>  
    $(document).ready(function()
    {
        $(".select2").select2();
        $("#datetimepicker1").datetimepicker({
            autoclose:true,
            format: 'yyyy-mm-dd hh:ii:00'
        });

    });
        $("#discard_income").on("click",function()

        {

            window.history.back();

        });





        $(document).on("click","#update_income",function()

        {

            var income_category=$("#income_category").val();

            var income_type=$("#income_type").val();

            var income_booking_id=$("#income_booking_id").val(); 

             var income_occured=$("#income_occured").val(); 

              var income_amount=$("#income_amount").val(); 



            if(income_category.trim()=="0")

            {

                $("#income_category").parent().find('.select2-selection').css("border","1px solid #cf3c63");

            }

            else

            {

             $("#income_category").parent().find('.select2-selection').css("border","1px solid #9e9e9e"); 

            }


              if(income_type.trim()=="booking" && income_booking_id.trim()=="0")

            {

                $("#income_booking_id").parent().find('.select2-selection').css("border","1px solid #cf3c63");

            }

            else

            {

             $("#income_booking_id").parent().find('.select2-selection').css("border","1px solid #9e9e9e"); 

            }



             if(income_amount.trim()=="")

            {

                $("#income_amount").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#income_amount").css("border","1px solid #9e9e9e"); 

            }

             if(income_occured.trim()=="")

            {

                $("#income_occured").css("border","1px solid #cf3c63");

            }

            else

            {

             $("#income_occured").css("border","1px solid #9e9e9e"); 

            }


        
            if(income_category.trim()=="0")

            {

                $("#income_category").parent().find('.select2-selection').focus();

            }
             else if(income_type.trim()=="booking" && income_booking_id.trim()=="0")

            {

                $("#income_booking_id").parent().find('.select2-selection').focus();

            }
            else if(income_occured.trim()=="")

            {

                $("#income_occured").focus();

            }

             else if(income_amount.trim()=="")

            {

                $("#income_amount").focus();

            }
            else

            {

                $("#update_income").prop("disabled",true);

                var formdata=new FormData($("#user_form")[0]);

                $.ajax({

                    url:"{{route('update-incomes')}}",

                    data: formdata,

                    type:"POST",

                    processData: false,

                    contentType: false,

                    success:function(response)

                    {

                      if(response.indexOf("success")!=-1)
                      {

                        swal({title:"Success",text:"Income Updated Successfully !",type: "success"},

                         function(){ 

                             location.reload();

                         });

                      }

                      else if(response.indexOf("fail")!=-1)

                      {

                       swal("ERROR", "Income cannot be updated right now! ");

                      }

                        $("#update_income").prop("disabled",false);

                    }

                });  

            }







        });

</script>

</body>

</html>

