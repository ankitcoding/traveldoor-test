

@include('mains.includes.top-header')

<style>

	.iti-flag {

		width: 20px;

		height: 15px;

		box-shadow: 0px 0px 1px 0px #888;

		background-image: url("{{asset('assets/images/flags.png')}}") !important;

		background-repeat: no-repeat;

		background-color: #DBDBDB;

		background-position: 20px 0

	}



	div#cke_1_contents {

		height: 250px !important;

	}

</style>

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">

	<div class="wrapper">

		@include('mains.includes.top-nav')

		<div class="content-wrapper">

			<div class="container-full clearfix position-relative">	

				@include('mains.includes.nav')

				<div class="content">

					<!-- Content Header (Page header) -->

					<div class="content-header">

						<div class="d-flex align-items-center">

							<div class="mr-auto">

								<h3 class="page-title">Food / Drinks Management</h3>

								<div class="d-inline-block align-items-center">

									<nav>

										<ol class="breadcrumb">

											<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>

											<li class="breadcrumb-item" aria-current="page">Dashboard</li>

											<li class="breadcrumb-item active" aria-current="page">View Food / Drinks Details

											</li>

										</ol>

									</nav>

								</div>

							</div>
							</div>

						</div>




@if($rights['view']==1)
						<div class="row">







							<div class="col-12">

								<div class="box">

									<div class="box-body">

										<div class="row">

											<div class="col-md-3">

												<label for="food_name"><strong>FOOD / DRINKS NAME :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_name"> @if($get_food->food_name!="" && $get_food->food_name!="0" && $get_food->food_name!=null){{$get_food->food_name}} @else No Data Available @endif </p>

											</div>

										</div>


										<div class="row">

											<div class="col-md-3">

												<label for="restaurant_id_fk"><strong>RESTAURANT NAME:</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="restaurant_id_fk"> @if($get_food->restaurant_id_fk!="" && $get_food->restaurant_id_fk!="0" && $get_food->restaurant_id_fk!=null)
													{{$get_food->getRestaurant->restaurant_name}}
													 @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="menu_category_id_fk"><strong>MENU CATEGORY:</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="menu_category_id_fk"> @if($get_food->menu_category_id_fk!="" && $get_food->menu_category_id_fk!="0" && $get_food->menu_category_id_fk!=null)
													{{$get_food->getMenuCategory->restaurant_menu_category_name}}
													 @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="food_price"><strong>FOOD / DRINKS PRICE :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_price"> @if($get_food->food_price!="" && $get_food->food_price!=null) GEL {{$get_food->food_price}} @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="food_discounted_price"><strong>FOOD / DRINKS DISCOUNTED PRICE :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_discounted_price"> @if($get_food->food_discounted_price!="" && $get_food->food_discounted_price!=null) GEL {{$get_food->food_discounted_price}} @else No Data Available @endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="food_ingredients"><strong>FOOD / DRINKS INGREDIENTS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_ingredients"> @if($get_food->food_ingredients!="" && $get_food->food_ingredients!="0" && $get_food->food_ingredients!=null){{$get_food->food_ingredients}} @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="food_unit"><strong>FOOD / DRINKS UNIT :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_unit"> @if($get_food->food_unit!="" && $get_food->food_unit!="0" && $get_food->food_unit!=null){{$get_food->food_unit}} @else No Data Available @endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="food_package_count"><strong>FOOD / DRINKS PACKAGE COUNT :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_package_count"> @if($get_food->food_package_count!="" && $get_food->food_package_count!="0" && $get_food->food_package_count!=null){{$get_food->food_package_count}} @else No Data Available @endif </p>

											</div>

										</div>
								
										<div class="row">

											<div class="col-md-3">

												<label for="validity_fromdate"><strong>FOOD / DRINKS AVAILABILITY :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="validity_fromdate"> @if($get_food->validity_fromdate!="" && $get_food->validity_fromdate!="0" && $get_food->validity_fromdate!=null) @php echo date('d-F-Y',strtotime($get_food->validity_fromdate)) @endphp @else No Data Available @endif To  @if($get_food->validity_todate!="" && $get_food->validity_todate!="0" && $get_food->validity_todate!=null) @php echo date('d-F-Y',strtotime($get_food->validity_todate)) @endphp @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="food_available_for_delivery"><strong>AVAILABLE FOR DELIVERY :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_available_for_delivery"> @if($get_food->food_available_for_delivery!="" && $get_food->food_available_for_delivery!="0" && $get_food->food_available_for_delivery!=null){{strtoupper($get_food->food_available_for_delivery)}} @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="food_featured"><strong>FEATURED :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_featured"> @if($get_food->food_featured!="" && $get_food->food_featured!="0" && $get_food->food_featured!=null){{strtoupper($get_food->food_featured)}} @else No Data Available @endif </p>

											</div>

										</div>

										<div class="row">

											<div class="col-md-3">

												<label for="food_active"><strong>ACTIVE STATUS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_active">@if($get_food->food_status==1)
															<button type="button" class="btn btn-sm btn-rounded inactive btn-primary">Active</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded active btn-default" >InActive</button>
															@endif </p>

											</div>

										</div>
										<div class="row">

											<div class="col-md-3">

												<label for="food_approval_status"><strong>APPROVE STATUS :</strong></label>

											</div>

											<div class="col-md-9">

												<p class="" id="food_approval_status">@if($get_food->food_approval_status==1)
															<button type="button" class="btn btn-sm btn-rounded btn-primary">Approved</button>
															@elseif($get_food->food_approval_status==2)
															<button type="button" class="btn btn-sm btn-rounded btn-danger">Rejected</button>
															@else
															<button type="button" class="btn btn-sm btn-rounded btn-warning" >Pending</button>
															@endif </p>

											</div>

										</div>
				
							
										<div class="row mb-10">

											<div class="col-md-12">

												<h4 class="box-header with-border" style="padding: 10px;border-color: #c3c3c3;" >

													<i class="fa fa-minus-circle" id="restaurant_images"></i> FOOD / DRINKS IMAGES

												</h4>

											</div>



										</div>
										<div class="row" id="restaurant_images_details">

											@php
											$get_food_images=unserialize($get_food->food_images);
											for($images=0;$images< count($get_food_images);$images++)
											{
												@endphp
												<div class='col-md-3'>
													<img class='upload_ativity_images_preview' src='{{ asset("assets/uploads/food_images") }}/{{$get_food_images[$images]}}' width=150 height=150 class="img img-thumbnail" />

												</div>
												@php
											}
											@endphp

										</div>
										<br>
										<div class="row mb-10">
											<div class="col-md-12">
												<div class="box-header with-border"
												style="padding: 10px;border-bottom:none;border-radius:0;border-top:1px solid #c3c3c3">
												<button type="button" id="discard_food" class="btn btn-rounded btn-primary">BACK</button>
												<a href="{{route('edit-food',['food_id'=>$get_food->restaurant_food_id])}}" id="update_restaurant" class="btn btn-rounded btn-primary mr-10">EDIT</a>
											</div>
										</div>
									</div>





									<!-- /.row -->

								</div>

								<!-- /.box-body -->

							</div>
							<!-- /.box -->
						</div>
						 @else
<h4 class="text-danger">No rights to access this page</h4>
    @endif
					</div>
				</div>
			</div>
		</div>

		@include('mains.includes.footer')

		@include('mains.includes.bottom-footer')



		<script>

			$(document).on("click","#discard_food",function()
			{
				window.history.back();

			});
		</script>



	</body>





	</html>

