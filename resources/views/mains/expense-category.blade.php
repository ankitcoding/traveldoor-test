@include('mains.includes.top-header')

<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Expense Category</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Expenses</li>
                                            <li class="breadcrumb-item active" aria-current="page">Add New Expense Category
                                            </li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($rights['add']==1)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">Add New Expense Category</h4>
                                </div>
                                <div class="box-body">
                                    <form class="package_form" action="javascript:void()" method="POST" id="menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10">
                                            <div class="col-sm-4 col-md-4">
                                                <div class="form-group">
                                                    <input type="hidden" name="menu_pid" value="0">
                                                    <label>Expense Category Name <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Name" id="expense_category_name" name="expense_category_name" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-10">
                                            <div class="col-md-12">
                                                <button type="button"  id="save_expense_category" class="btn btn-rounded btn-primary mr-10 pull-right">Submit</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                     <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">View Expense Category</h4>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table id="example1" class="table table-bordered table-striped datatable">
                                            <thead>
                                            <tr>
                                                <th>SR NO</th>
                                                <th>NAME</th>
                                                <th>ACTION</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($fetch_expense_category as $expense_category)
                                            <tr>
                                                <td>{{$loop->iteration}}</td>
                                                 <td id="expense_category_name_{{$expense_category->expense_category_id}}">{{$expense_category->expense_category_name}}</td>
                                                  <td><button id="edit_{{$expense_category->expense_category_id}}" class="btn btn-default btn-rounded edit_activity"><i class="fa fa-pencil"></i></button></td>
                                            </tr>

                                            @endforeach
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @else
                    <h4 class="text-danger">No rights to access this page</h4>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @include('mains.includes.footer')
    @include('mains.includes.bottom-footer')
       <div id="editModal" class="modal fade" role="dialog" style="z-index: 9999">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Edit Expense Category</h4>
         <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-12">
                <form class="package_form" action="javascript:void()" method="POST" id="edit_menu_form">
                                        {{csrf_field()}}
                                        <div class="row mb-10" >
                                        <h5 style="color:red" id="edit_expense_category_error"></h5>
                                        </div>
                                        <div class="row mb-10">
                                           <div class="col-sm-6 col-md-6">
                                                <div class="form-group">
                                                      <input type="hidden" id="edit_expense_category_id" name="expense_category_id">
                                                    <label>Expense Category Name <span class="asterisk">*</span></label>
                                                    <input type="text" class="form-control" placeholder="Name" id="edit_expense_category_name" name="expense_category_name" autofocus>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
            </div>
        </div>
        
      </div>
      <div class="modal-footer">
        
         <button type="button"  id="update_expense_category" class="btn btn-rounded btn-primary mr-10 ">Submit</button>
        <button type="button" class="btn btn-default mr-10 pull-right" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <script>
    $(document).ready(function()
    {
        $(".datatable").DataTable();
    });

    $(document).on("click","#save_expense_category",function()
    {
    var expense_category_name=$("#expense_category_name").val();

    if(expense_category_name.trim()=="")
    {
    $("#expense_category_name").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#expense_category_name").css("border","1px solid #9e9e9e");
    }
    
    
    if(expense_category_name.trim()=="")
    {
    $("#expense_category_name").focus();
    }
    else
    {
    $("#save_expense_category").prop("disabled",true);
    var formdata=new FormData($("#menu_form")[0]);
    $.ajax({
    url:"{{route('expense-category-insert')}}",
    data: formdata,
    type:"POST",
    processData: false,
    contentType: false,
    success:function(response)
    {
    if(response.indexOf("exist")!=-1)
    {
    swal("Already Exist!", "Expense Category Name already exists");
    }
    else if(response.indexOf("success")!=-1)
    {
    swal({title:"Success",text:"Expense Category Created Successfully !",type: "success"},
    function(){
    location.reload();
    });
    }
    else if(response.indexOf("fail")!=-1)
    {
    swal("ERROR", "Expense Category cannot be inserted right now! ");
    }
    $("#save_expense_category").prop("disabled",false);
    }
    });
    }
    });
    </script>
    <script> 
    $(document).on("click",".edit_activity",function()
    {
        var id=this.id;

        var actual_id=id.split("_");
        var expense_category_name=$("#expense_category_name_"+actual_id[1]).text();
        $("#edit_expense_category_name").val(expense_category_name);
        $("#edit_expense_category_id").val(actual_id[1]);


        $("#editModal").modal("show"); 
    });

    $(document).on("click","#update_expense_category",function()
    {
    var expense_category_name=$("#edit_expense_category_name").val();
    var error=0;
    var errors_array=[];
    
    if(expense_category_name.trim()=="")
    {
        errors_array.push("<li>Please Enter Expense Category Name</li>");
            error++;

    $("#edit_expense_category_name").css("border","1px solid #cf3c63");
    }
    else
    {
    $("#edit_expense_category_name").css("border","1px solid #9e9e9e");
    }

   
    if(error>0)
    {

            $("#edit_expense_category_error").html("<ul>"+errors_array.join("")+"</ul>");
            $("#edit_expense_category_error").focus();
    }
    else
    {
    $("#update_expense_category").prop("disabled",true);
            var formdata=new FormData($("#edit_menu_form")[0]);
            $.ajax({
                url:"{{route('expense-category-update')}}",
                data: formdata,
                type:"POST",
                processData: false,
                contentType: false,
                success:function(response)
                {
                    if(response.indexOf("exist")!=-1)
                    {
                        swal("Already Exist!", "Expense Category already exists");
                    }
                    else if(response.indexOf("success")!=-1)
                    {
                        swal({title:"Success",text:"Expense Category Update Successfully !",type: "success"},
                            function(){
                                location.reload();
                            });
                    }
                    else if(response.indexOf("fail")!=-1)
                    {
                        swal("ERROR", "Expense Category cannot be updated right now!");
                    }
                    $("#update_expense_category").prop("disabled",false);
                }
            });
    }
    });
    </script>

</body>
</html>