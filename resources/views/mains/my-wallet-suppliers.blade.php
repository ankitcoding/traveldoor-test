<?php
use App\Http\Controllers\ServiceManagement;
?>
@include('mains.includes.top-header')
<style>
    .text-green
    {
        color:green;
    }
    .text-red
    {
        color:red;
    }
       div#loaderModal .modal-content {
    background: transparent;
    box-shadow: none;
}
div#loaderModal .modal-dialog {
    margin-top: 17%;
}
img.loader-img {
    display: block;
    margin: auto;
    width: auto;
    height: 50px;
}
    div#loaderModal {
    background: #0000005c;
}
</style>
<body class="hold-transition light-skin sidebar-mini theme-rosegold onlyheader">
    <div class="wrapper">
        @include('mains.includes.top-nav')
        <div class="content-wrapper">
            <div class="container-full clearfix position-relative">
                @include('mains.includes.nav')
                <div class="content">
                    <div class="content-header">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="page-title">Supplier's Wallet</h3>
                                <div class="d-inline-block align-items-center">
                                    <nav>
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
                                            <li class="breadcrumb-item" aria-current="page">Home</li>
                                            <li class="breadcrumb-item active" aria-current="page">Supplier's Wallet</li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                   @if($rights['view']==1 && strpos($rights['admin_which'],'view')!==false)
                    <div class="row">
                        <div class="col-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <h4 class="box-title">All Suppliers</h4>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="table-responsive" id="commission_data">
                                              <table id="supplier_wallet_table" class="table table-bordered">
                                                <thead>
                                                  <tr>
                                                    <th>Sr. no</th>
                                                    <th>Company Name</th>
                                                    <th>Supplier Name</th>
                                                    <th>Wallet Balance</th>
                                                     <th>Withdrawal Requests</th>
                                                      <th>Status</th>
                                                    <th>Action</th>
                                                  </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($suppliers_wallet_data as $wallet_data)
                                                  <tr>
                                                    <td>{{$loop->iteration}}</td>
                                                    <td>{{$wallet_data['supplier_company']}}</td>
                                                    <td>{{$wallet_data['supplier_fullname']}}</td>
                                                     <td id="wallet_amount_{{$wallet_data['supplier_id']}}">{{$wallet_data['supplier_total_wallet_amount']}}</td>
                                                     <td><button class="btn btn-success withdrawals" id="withdrawals_{{$wallet_data['supplier_id']}}">{{$wallet_data['get_commission_withdrawals_count']}}</button></td>
                                                      <td>@if($wallet_data['supplier_status']==1) <button class="btn btn-sm btn-secondary">Active</button> @else <button class="btn btn-sm btn-default">Inactive</button> @endif</td>
                                                      <td style="white-space: nowrap;">
                                                        <button class="btn btn-sm btn-primary add_money" id="add_money_{{$wallet_data['supplier_id']}}">Add / Deduct Money</button>
                                                        <a href="{{route('my-wallet-supplier',['supplier_id'=>$wallet_data['supplier_id']])}}" class="btn btn-sm btn-primary">Show Wallet</a>
                                                      </td>
                                                  </tr>

                                                  @endforeach

                                                </tbody>
                                              </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                                   @else
                                    <h4 class="text-danger">No rights to access this page</h4>
                                   @endif
                                </div>
                            </div>
                        </div>
                        @include('mains.includes.footer')
                        @include('mains.includes.bottom-footer')
                          <div class="modal" id="loaderModal">
    <div class="modal-dialog">
      <div class="modal-content">
  
       
  
        <!-- Modal body -->
        <div class="modal-body">
         <img src="{{ asset('assets/images/loading.gif')}}" class="loader-img">
        </div>
  
       
      </div>
    </div>
  </div>
   <div class="modal" id="actionModal" style="z-index: 99999;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Peform Operation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="hidden" name="action_id" id="action_id">
                                <div class="form-group">
                                    <label for="remarks" id="remarks_label">Remarks for Approval</label>
                                    <textarea name="remarks" id="remarks" class="form-control" rows="5"></textarea>
                                </div>
                                
                                
                                <button type="button" id="actionButton" class="btn btn-primary pull-right">Submit</button>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>


            <div class="modal" id="installmentModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Withdrawals Section</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                           <div class="col-md-12 mb-25">
                             <h3>Pending Withrawal Request</h3>
                             <div id="pending_withdrawal_table">
                             </div>
                             <button data-toggle="collapse" data-target="#history_table_div" class="btn btn-primary mb-20">Withrawals Hitory<i class="fa fa-caret-down"></i></button>

                             <div id="history_table_div" class="collapse">

                             </div>
                         </div>
                            <div class="col-md-12">
                                <input type="hidden" name="main_supplier_id" id="main_supplier_id">
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

         <div class="modal" id="actionModal2" style="z-index: 99999;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Peform Operation</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                              <form id="walletForm">
                                {{csrf_field()}}
                                <input type="hidden" name="supplier_action_id" id="supplier_action_id">
                               <div class="form-group">
                                    <label for="operation" id="operation_label">Select Operation</label>
                                    <select name="operation" id="operation" class="form-control">
                                      <option value="credit">Add Money</option>
                                      <option value="debit">Deduct Money</option>
                                    </select>
                                </div>
                                 <div class="form-group">
                                    <label for="operation_amount" id="operation_amount_label">Amount</label>
                                    <input type="text" class="form-control" id="operation_amount" name="operation_amount" placeholder="Enter Amount" onkeypress="javascript:return validateNumber(event)"  onpaste="javascript:return validateNumber(event)">
                                </div>
                                <div class="form-group">
                                    <label for="remarks" id="remarks_label">Remarks (if any)</label>
                                    <textarea name="remarks" id="supplier_remarks" class="form-control" rows="5"></textarea>
                                </div>
                                
                                
                                <button type="button" id="actionButton2" class="btn btn-primary pull-right">Submit</button>
                              </form>
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>

                        <script>
                          $(document).ready(function()
                          {
                            $("#supplier_wallet_table").DataTable({
                               "deferRender": true
                            });
                          });
                            function get_details(actual_id)
                            {
                               $("#loaderModal").modal("show");
                               $.ajax({
                                url: "{{route('get-withdrawals-supplier-detail')}}",
                                data: {
                                    'supplier_id':actual_id,
                                },
                                type: 'GET',
                                dataType:"JSON",
                                success: function (response)
                                {

                                    $("#main_supplier_id").val(actual_id);
                                    $("#pending_withdrawal_table").html(response.withdrawals_pending);
                                    $("#history_table_div").html(response.history_table);

                                     

                                    setTimeout(function()
                                    {
                                        $("#loaderModal").modal("hide");
                                         $('#history_table').DataTable();
                                    },500);


                                }
                            });

                           }

                           $(document).on("click",".withdrawals",function()
                           {

                            var id=this.id;

                            var actual_id=id.split("_")[1];
                            $("#pending_withdrawal_table").html("");
                            $("#history_table_div").html("");
                            get_details(actual_id);
                            $("#installmentModal").modal("show");




                        });

                           $(document).on("click",".approve",function()
                            {
                                var id=this.id;
                                $("#action_id").attr("value",id);
                                $("#actionModal").find(".modal-title").text("Perform Approval Operation");
                                $("#remarks_label").text("Remarks for Approval");
                                $("#remarks").val("");
                                $("#actionModal").modal("show");
                            });
                            $(document).on("click",".reject",function()
                            {
                                var id=this.id;
                                $("#action_id").attr("value",id);
                                $("#actionModal").find(".modal-title").text("Perform Rejection Operation");
                                $("#remarks_label").text("Remarks for Rejection");
                                $("#remarks").val("");
                                $("#actionModal").modal("show");
                            });


                           $(document).on("click","#actionButton",function()
                           {
                             var id=$("#action_id").val();

                              var actual_id=id.split("_");

                               var remarks=$("#remarks").val();
                              swal({
                              title: "Are you sure?",
                              text: "You want to "+actual_id[0]+" this withdrawal request !",
                              type: "warning",
                              showCancelButton: true,
                              confirmButtonColor: "#DD6B55",
                              confirmButtonText: "Yes, Go Ahead",
                              cancelButtonText: "No, cancel!",
                              closeOnConfirm: false,
                              closeOnCancel: false
                          }, function(isConfirm) {
                              if (isConfirm) {
                    

                            $.ajax({
                                url: "{{route('withdrawals-supplier-approval')}}",
                                data: {
                                    '_token':"{{csrf_token()}}",
                                    'supp_wallet_id':actual_id[1],
                                    'supp_wallet_answer':actual_id[0],
                                    "remarks":remarks
                                },
                                type: 'POST',
                                success: function (response)
                                {
                                    if(response.indexOf("success")!=-1)
                                    {
                                        if(actual_id[0]=="approve")
                                        swal("Success","Withdrawal Approved Successfully","success");
                                        else
                                        swal("Success","Withdrawal Rejected Successfully","success");

                                        var supplier_id=$("#main_supplier_id").val();
                                        get_details(supplier_id);   
                                    }
                                    else if(response.indexOf("fail")!=-1)
                                    {
                                          swal("Operation Fails","Operation cannot be done right now.","error");
                                        

                                    }
                                     else if(response.indexOf("already")!=-1)
                                    {
                                          swal("ALready!","Withdrawal request has been already updated with approval/rejection status","error");
                                        

                                    }
                                    else if(response.indexOf("answer")!=-1)
                                    {
                                         swal("Operation Fails","Incorrect or missing values passed","error");
                                    }
                                    else if(response.indexOf("invalid")!=-1)
                                    {
                                         swal("Invalid Session","User is invalid.Please Login again","error");
                                         window.location.replace="{{route('index')}}";
                                    }
                                    $("#actionModal").modal("hide");
                               
                                }
                            });
                } else {
                    swal("Cancelled", "Operation Cancelled", "error");
                }
            });

                            


                           });








                            $(document).on("click",'.add_money',function()
                          {
                            var id=this.id
                            var actual_id=id.split('_');

                            $("#supplier_action_id").val(actual_id[2]);
                            $("#actionModal2").modal("show");
                          });

                          $(document).on("click","#actionButton2",function()
                          {
                            var operation_amount=$("input[name='operation_amount']").val();
                            var supplier_id=$("#supplier_action_id").val();

                            if(operation_amount.trim()=="")
                            {
                              swal("Amount cannot be empty")
                            }
                            else
                            {
                              var formdata=$("#walletForm").serialize()
                              $.ajax({
                                url:"{{route('suppliers-operation')}}",
                                type:"POST",
                                data:formdata,
                                success:function(response)
                                {
                                  if(response.indexOf("success")!=-1)
                                  {
                                    swal("Operation Performed Successfully");
                                    $("#wallet_amount_"+supplier_id).text(response.split("_")[1]);

                                    setTimeout(function()
                                    {
                                      $("#operation_amount").val("");
                                      $("#actionModal2").modal("hide");
                                    },500);
                                  }
                                  else if(response.indexOf("fail")!=-1)
                                  {
                                    swal("Operation cannot be performed now.Try Again.");
                                  }

                                }
                              });

                            }

                          });

                        </script>
                    </body>
                    </html>